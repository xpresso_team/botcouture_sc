#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Build the dependencies
apt-get update -qq && apt-get install -y --no-install-recommends build-essential git-core && apt-get clean
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

pip install --upgrade pip
pip install awscli
pip install $(tail -n +2 ${ROOT_FOLDER}/requirements/requirements.txt)
pip install -r /dev-requirements.txt

/bin/bash -c "source environment_variables.txt"

python -m spacy download en
python setup.py install

