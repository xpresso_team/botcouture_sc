#! /bin/bash
## This script is used to setup the build environment. It is used setup the build and test environment. It performs
## either of these tasks
##   1. Install Linux Libraries
##   2. Setup environment
##   3. Download data required to perform build.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user

set -e

# Installing Python 3
if command -v python2.7 &> /dev/null; then
  echo "Python Found. Skipping Python installations"
else
  echo "Python not found. Installing python"
  apt-get update -y 
  apt-get install -y python-pip python-dev build-essential curl telnet wget rsyslog iputils-ping git

  
fi
apt-get update -y  && apt-get install -y python-pip
# Installing pytest
pip install -U pytest pytest-cov
