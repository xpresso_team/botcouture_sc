CREATE TABLE IF NOT EXISTS user(  
	user_id VARCHAR(300),  
	first_active_ts DATETIME NULL,  
	first_active_ts_date DATE NULL,  
	last_name VARCHAR(50) NULL,  
	first_name VARCHAR(50) NULL,  
	profile_pic_link VARCHAR(255) NULL,  
	locale VARCHAR(50) NULL,  
	timezone VARCHAR(50) NULL,  
	gender CHAR(1) NULL,  
	is_payment_enabled TINYINT(1) NULL,  
	marked_spam TINYINT(1) NULL,  
	has_blocked TINYINT(1) NULL,  
	PRIMARY KEY (user_id));

CREATE TABLE chatlog ( 
	user_id VARCHAR(300), 
	timestamp DATETIME NULL, 
	timestamp_date DATE NULL, 
	channel_id VARCHAR(20) NULL, 
	session_id VARCHAR(50) NULL, 
	message_number INT(11) NULL, 
	message_type VARCHAR(2) NULL, 
	message_chat VARCHAR(255) NULL, 
	message_type_flag VARCHAR(3) NULL, 
	visual_search INT(11) NULL, 
	product_response_list JSON NULL, 
	nlp_query_response VARCHAR(255) NULL, 
	vision_file_link VARCHAR(255) NULL, 
	vision_engine_response VARCHAR(255) NULL, 
	quick_reply_button varchar(255) NULL,
	feedback_type VARCHAR(20) NULL,
	feedback_img VARCHAR(255) NULL,
	sentiment VARCHAR(50) NULL,
	feedback_id VARCHAR(50) NULL 
	);

CREATE TABLE click ( 
	user_id VARCHAR(300), 
	timestamp DATETIME NULL, 
	timestamp_date DATE NULL, 
	session_id VARCHAR(50) NULL, 
	message_number int(11) NULL,
	click_item VARCHAR(50) NULL
	);

CREATE TABLE action(
	user_id VARCHAR(300),
	timestamp DATETIME NULL,
	timestamp_date DATE NULL,
	session_id VARCHAR(50) NULL,
	text VARCHAR(255) NULL,
	action_code VARCHAR(10) NULL,
	entity VARCHAR(20) NULL,
	filter VARCHAR(20) NULL
	);
