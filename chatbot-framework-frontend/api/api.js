const _       = require( 'lodash'     )
var nconf     = require( 'nconf'      )
const request = require( 'superagent' )
const express = require( 'express'    )
/**
 * Read text from Google sheet
 */
var SpreadSheetUtil = require( '../services/spreadsheet/SpreadSheetUtil' )
let SHEET_ID        = nconf.get( 'MESSAGE_TEXT_GOOGLE_SHEET_ID' )
let ACCESS_TOKEN = nconf.get('PAGE_ACCESS_TOKEN')

SpreadSheetUtil.getData( SHEET_ID,nconf.get( 'Messages_SHEET_NAME' ), function ( data ) {
  global.sheetData = data
  // console.log(nconf.get( 'Messages_SHEET_NAME' ), data)
})


const router  = express.Router()

console.log( 'MultiChannelChatBot url: ', nconf.get( 'MULTICHANNELCHATBOTSERVER' ) )
let CHATBOTSERVER = nconf.get( 'MULTICHANNELCHATBOTSERVER' )


/**
 * Get user object, mostly for gender
 */
router.post( '/getUser', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid ) {
    res.status( 404 ).send( 'UserId or BotId not provided.' )
    return
  }
  request.post( `${CHATBOTSERVER}/webview/getUser` ).send( { psid: params.psid, botid: params.botid } )
  .then( result => {
    res.send(  JSON.parse( result.text ) )
  })
  .catch( err => {
    res.status(err.status).send( 'Error fetching user from server.' )
  })  
})

/**
 * Save user's gender
 */
router.post( '/updateUser', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.gender ) {
    res.status( 404 ).send( 'UserId or BotId or Gender not provided.' )
    return
  }
  request.post( `${CHATBOTSERVER}/webview/updateUser` ).send( { psid: params.psid, botid: params.botid, gender : params.gender } )
  .then( result => {
    res.sendStatus( 200 )
  })
  .catch( err => {
    res.status(err.status).send( 'Error updating user.' )
  })
})

router.post('/sendFeedback', (req,res) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.feedback ) {
    res.status( 404 ).send( 'UserId or BotId or feedback not provided.' )
    return
  }
  request.post( `${CHATBOTSERVER}/webview/sendFeedback` ).send( { psid: params.psid, botid: params.botid, feedback : params.feedback } )
  .then( result => {
    // console.log(JSON.parse( result.text ))
    res.send( JSON.parse( result.text ))
  })
  .catch( err => {
    console.log("error in feedback is ",err)
    res.status(err.status).send( 'Error Sending Feedback happened')
  })
})
/**
 * Get all of users' saved preferences
 */
router.post( '/getUserPreferences', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid ) {
    res.status( 404 ).send( 'UserId or BotId not provided.' )
    return
  }  
  request.post( `${CHATBOTSERVER}/webview/getUserPreferences` ).send( { psid: params.psid, botid: params.botid } )
  .then( result => {
    // console.log( JSON.parse( result.text ) )
    res.send(  JSON.parse( result.text ) )
  })
  .catch( err => {
    res.status(err.status).send( 'Error fetching user from server.' )
  })  
})

/**
 * Get users' preferences for a given entity
 */
router.post( '/getUserPreferencesByEntity', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.entity ) {
    res.status( 404 ).send( 'UserId, BotId or entity not provided.' )
    return
  }  
  request.post( `${CHATBOTSERVER}/webview/getUserPreferencesByEntity` )
  .send( { psid: params.psid, botid: params.botid, entity: params.entity, gender : params.gender } )
  .then( result => {
    // console.log( 'getUserPreferencesByEntity -> ', JSON.parse( result.text ) )
    res.send(  JSON.parse( result.text ) )
  })
  .catch( err => {
    res.status(err.status).send( 'Error fetching user from server.' )
  })  
})

/**
 * Get users' preferences for a given multiple entities
 */
router.post( '/getUserPreferencesByEntities', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.quesArray ) {
    res.status( 404 ).send( 'UserId, BotId or questionsArray not provided.' )
    return
  }
  // console.log( 'getUserPreferencesByEntities', params )
  request.post( `${CHATBOTSERVER}/webview/getUserPreferencesByEntities` )
  .send( { psid: params.psid, botid: params.botid, quesArray: params.quesArray, gender : params.gender } )
  .then( result => {
    // console.log( 'getUserPreferencesByEntities -> ', JSON.parse( result.text ) )
    res.send(  JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error fetching user from server.' )
  })  
})
  
/**
 * Save user's preferences for multiple entities
 */
router.post( '/saveUserPreferencesByEntities', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.preferences ) {
    res.status( 404 ).send( 'UserId, BotId or Preferences not provided.' )
    return
  }
  // console.log( 'saveUserPreferencesByEntities', params )
  request.post( `${CHATBOTSERVER}/webview/saveUserPreferencesByEntities` )
  .send({
    psid   : params.psid, 
    botid  : params.botid,
    preferences : params.preferences,
  })
  .then( result => {
    // console.log('saveUserPreferencesByEntities', JSON.parse( result.text ) )
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( 'supby', err )
    res.status(err.status).send( 'Error saving user preferences.' )
  })
  // res.send( params )
  // return
})

/**
 * Get questionaire for profile or a single object for a given category
 */
router.post( '/getProfileQuestions', ( req, res ) => {

  /*
    let QUESTIONS = {
      'A' : [ 'women', 'Dresses', 'Dress', 'size', 'square' ],
      'B' : [ 'women', 'Tops', 'T-Shirt', 'size', 'square' ],
      'C' : [ 'women', 'Tops', 'Blouse', 'size', 'square' ],
      'D' : [ 'women', 'Skirts', 'Skirt', 'size', 'square' ],
      'E' : [ 'women', 'Pants', 'Pants', 'size', 'square' ],
      'F' : [ 'men', 'Outerwear', 'Coat', 'size', 'square' ],
      'G' : [ 'men', 'Shoe', 'Shoe', 'size', 'square' ],
      'H' : [ 'men'  , 'Pants'    , 'Pants', 'size', 'square' ]
    }
    let ques = _.groupBy(QUESTIONS, val => val[1])
    ques = _.mapValues( ques, (val, key) => _.uniqBy( val, el => el[1]+el[2]+el[3] ) )
    ques = _.map( ques, (val, key) => {
      return { category: key, ques : val }
    })
    
    Output:
    [
      {
          "category": "Dresses",
          "ques": [
            [
                "women",
                "Dresses",
                "Dress",
                "size",
                "square"
            ]
          ]
      },
      {
          "category": "Tops",
          "ques": [
            [
                "women",
                "Tops",
                "T-Shirt",
                "size",
                "square"
            ],
            [
                "women",
                "Tops",
                "Blouse",
                "size",
                "square"
            ]
          ]
      },
      {
          "category": "Skirts",
          "ques": [
            [
                "women",
                "Skirts",
                "Skirt",
                "size",
                "square"
            ]
          ]
      },
      {
          "category": "Pants",
          "ques": [
            [
                "women",
                "Pants",
                "Pants",
                "size",
                "square"
            ]
          ]
      },
      {
          "category": "Outerwear",
          "ques": [
            [
                "men",
                "Outerwear",
                "Coat",
                "size",
                "square"
            ]
          ]
      },
      {
          "category": "Shoe",
          "ques": [
            [
                "men",
                "Shoe",
                "Shoe",
                "size",
                "square"
            ]
          ]
      }
    ]
  */

  let QUESTIONS = {
    'A' : [ 'women', 'Dresses'  , 'dress'   , 'size', 'square', '/images/profile_icons/WomensDress.svg' ],
    'B' : [ 'women', 'Tops'     , 't-shirt' , 'size', 'square', '/images/profile_icons/WomensShirt.svg' ],
    'C' : [ 'women', 'Tops'     , 'blouse'  , 'size', 'square', '/images/profile_icons/WomensShirt.svg' ],
    'H' : [ 'men'  , 'Shoes' , 'footwear', 'size', 'square', '/images/profile_icons/MensShoes.svg' ],
    'I' : [ 'men'  , 'Tops'     , 't-shirt' , 'size', 'square', '/images/profile_icons/MensCasualShirt.svg' ],
  }

  QUESTIONS = global.sheetData
  delete QUESTIONS[ '1' ]       // Delete 1st headers row
// console.log( 'inside', QUESTIONS )
  let params   = req.body
  let gender   = params.gender
  let category = params.category
  Promise.resolve( QUESTIONS )
  .then( result => {
    let ques = result
    // Filter by gender
    if ( gender === 'men' || gender === 'women' ) {
      ques = _.filter( result, ( val, key ) => {
        return val[0] === gender
      })
    }
    // Group by Category
    ques = _.groupBy( ques, val => val[1] )
    // Remove duplicate questions (e.g. same questions for men and women if gender is no_preference, e.g. Pants in above example)
    ques = _.mapValues( ques, (val, key) => _.uniqBy( val, el => el[1]+el[2]+el[3] ) )
    // Put keys inside the arrays
    ques = _.map( ques, (val, key) => {
      return { category: key, ques : val }
    })
    // Return only the category object if specified
    // Else return the whole array of questions
    if ( category ) {
      ques = _.find( ques, val => val.category === category )
    }
    // console.log('ques', ques)
    res.send( ques )
  })
  .catch( err => {
    res.status(err.status).send( 'Error fetching qeustions from server.' )
  })

})

/**
 * Get attributes (e.g. size values) for given multiple entities
 */
router.post( '/getAttributesByEntities', ( req, res ) => {
  let params = req.body
  if ( ! params.quesArray ) {
    res.status( 404 ).send( 'QuestionsArray not provided.' )
    return
  }
  // console.log( 'getAttributesByEntities', params )
  request.post( `${CHATBOTSERVER}/webview/getAttributesByEntities` )
  .send( { quesArray: params.quesArray } )
  .then( result => {
    // console.log( 'getUserPreferencesByEntity -> ', JSON.parse( result.text ) )
    res.send(  JSON.parse( result.text ) )
  })
  .catch( err => {
    res.status(err.status).send( 'Error fetching user from server.' )
  })  
})

/*******************************************
************** Shopping Cart ***************
********************************************/
/**
 * Get all carts with count of items
 */
router.post( '/getUsersAllCarts', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid ) {
    res.status( 404 ).send( 'UserId or BotId not provided.' )
    return
  }
  // console.log( 'getUsersAllCarts -> ', params )
  request.post( `${CHATBOTSERVER}/webview/getUsersAllCarts` )
  .send({ psid: params.psid, botid: params.botid })
  .then( result => {
    // console.log( 'getUsersAllCarts -> ', JSON.parse( result.text ) )
    res.send(  JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error fetching user from server.' )
  })
})

/**
 * Clear all carts
 */
router.post( '/clearAllCarts', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid ) {
    res.status( 404 ).send( 'UserId or BotId not provided.' )
    return
  }
  // console.log( 'getUsersAllCarts -> ', params )
  request.post( `${CHATBOTSERVER}/webview/clearAllCarts` )
  .send({ psid: params.psid, botid: params.botid })
  .then( result => {
    // console.log( 'getUsersAllCarts -> ', JSON.parse( result.text ) )
    res.sendStatus( 200 )
    // res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error fetching user from server.' )
  })
})

/**
 * Clear cart for a given catalog
 */
router.post( '/clearCartByCatalog', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.catalog ) {
    res.status( 404 ).send( 'UserId, BotId or Catalog name not provided.' )
    return
  }
  // console.log( 'clearCartByCatalog -> ', params )
  request.post( `${CHATBOTSERVER}/webview/clearCartByCatalog` )
  .send({ psid: params.psid, botid: params.botid, catalog: params.catalog })
  .then( result => {
    // console.log( 'clearCartByCatalog -> ', JSON.parse( result.text ) )
    res.sendStatus( 200 )
    // res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error clearing cart from server.' )
  })
})

/**
 * Get a single cart
 */
router.post( '/getUsersCartByCatalog', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.catalog ) {
    res.status( 404 ).send( 'UserId, BotId or Catalog not provided.' )
    return
  }
  request.post( `${CHATBOTSERVER}/webview/getUsersCartByCatalog` )
  .send({ psid: params.psid, botid: params.botid, catalog: params.catalog })
  .then( result => {
    // console.log( 'getUsersAllCarts -> ', JSON.parse( result.text ) )
    // res.sendStatus( 200 )
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error fetching cart from server.' )
  })  
})

/**
 * Add an item from cart (by xc_xku)
 */
router.post( '/addItemToCart', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.xc_sku ) {
    res.status( 404 ).send( 'UserId, BotId or xc_xku not provided.' )
    return
  }
  // console.log( 'addItemToCart -> ', params )
  request.post( `${CHATBOTSERVER}/webview/addItemToCart` )
  .send({ 
    psid     : params.psid, 
    botid    : params.botid, 
    catalog  : params.catalog,
    color    : params.color,
    quantity : params.quantity,
    size     : params.size,
    xc_sku   : params.xc_sku 
  })
  .then( result => {
    // console.log( 'addItemToCart -> ', JSON.parse( result.text ) )
    // res.sendStatus( 200 )
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error adding item to cart from server.' )
  })  
})

/**
 * Remove an item from cart (by xc_xku)
 */
router.post( '/removeItemFromCart', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.catalog || ! params.xc_sku ) {
    res.status( 404 ).send( 'UserId, BotId, xc_xku or catalog not provided.' )
    return
  }
  // console.log( 'removeItemFromCart -> ', params )
  request.post( `${CHATBOTSERVER}/webview/removeItemFromCart` )
  .send({ psid: params.psid, botid: params.botid, catalog: params.catalog, xc_sku: params.xc_sku })
  .then( result => {
    // console.log( 'removeItemFromCart -> ', JSON.parse( result.text ) )
    // res.sendStatus( 200 )
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error fetching cart from server.' )
  })  
})

/**
 * Update cart item (change size, color, quantity)
 */
router.post( '/updateCartItem', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.catalog || ! params.xc_sku ) {
    res.status( 404 ).send( 'UserId, BotId, xc_xku or catalog not provided.' )
    return
  }
  // console.log( 'updateCartItem -> ', params )
  request.post( `${CHATBOTSERVER}/webview/updateCartItem` )
  .send({ 
    psid: params.psid, 
    botid: params.botid, 
    catalog: params.catalog, 
    xc_sku: params.xc_sku,
    quantity: params.quantity
   })
  .then( result => {
    // console.log( 'updateCartItem -> ', JSON.parse( result.text ) )
    // res.sendStatus( 200 )
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error fetching cart from server.' )
  })  
})

/**
 * Move an item to Cart by removing from wishlist
 */
 router.post( '/moveToCart', ( req, res) => {
  let params = req.body
  if( ! params.psid || ! params.botid || ! params.xc_sku) {
    res.status( 404 ).send('UserId, BotId or xc_sku not provided.')
    return
  }
  request.post(`${CHATBOTSERVER}/webview/moveToCart`)
  .send({ 
    psid     : params.psid, 
    botid    : params.botid, 
    catalog  : params.catalog,
    color    : params.color,
    quantity : !_.isUndefined(params.quantity)?params.quantity : 1,
    size     : params.size,
    xc_sku   : params.xc_sku
  })
  .then( result => {
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error moving item to Cart.')
  })
 })

 /*******************************************
    ************** Wishlist ***************
********************************************/

/**
 * Remove an item from cart (by xc_xku) and move to wishlist
 */
router.post( '/saveToWishlist', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.xc_sku ) {
    res.status( 404 ).send( 'UserId, BotId or xc_xku not provided.' )
    return
  }
  // console.log( 'saveToWishlist -> ', params )
  request.post( `${CHATBOTSERVER}/webview/saveToWishlist` )
  .send({ psid: params.psid, botid: params.botid, xc_sku: params.xc_sku, color: params.color, catalog : params.catalog, size : params.size })
  .then( result => {
    // console.log( 'saveToWishlist -> ', JSON.parse( result.text ) )
    // res.sendStatus( 200 )
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error adding to wishlist from server.' )
  })  
})

/**
 * Remove an item from cart (by xc_xku) and move to wishlist
 */
router.post( '/moveToWishlist', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.catalog || ! params.xc_sku ) {
    res.status( 404 ).send( 'UserId, BotId, xc_xku or Catalog not provided.' )
    return
  }
  console.log( 'moveToWishlist -> ', params )
  request.post( `${CHATBOTSERVER}/webview/moveToWishlist` )
  .send({ psid: params.psid, botid: params.botid, catalog: params.catalog, xc_sku: params.xc_sku, size: params.size, color: params.color })
  .then( result => {
    console.log( 'moveToWishlist -> ', JSON.parse( result.text ) )
    // res.sendStatus( 200 )
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error updating cart from server.' )
  })  
})


/**
 * Remove an item from wishlist
 */
router.post( '/removeItemFromWishlist', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.xc_sku || !params.catalog ) {
    res.status( 404 ).send( 'UserId, BotId or xc_xku or catalog not provided.' )
    return
  }
  // console.log( 'removeItemFromWishlist -> ', params )
  request.post( `${CHATBOTSERVER}/webview/removeItemFromWishlist` )
  .send({ psid: params.psid, botid: params.botid, xc_sku: params.xc_sku, catalog: params.catalog })
  .then( result => {
    // console.log( 'removeItemFromWishlist -> ', JSON.parse( result.text ) )
    // res.sendStatus( 200 )
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error updating wishlist from server.' )
  })  
})

/**
 *Get User Wishlist
 */
 router.post('/getUsersAllWishlists', (req, res) => {
  let params = req.body
  if ( ! params.psid || !params.botid ) {
    res.status( 404 ).send( 'UserID or BotId  not provided.' )
    return
  }
  request.post(`${CHATBOTSERVER}/webview/getUsersAllWishlists`)
  .send({ psid: params.psid, botid: params.botid })
  .then( result => {
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error getting wishlist from server.')
  })
 })

/**
 *Get User Wishlist by Catalog
 */
 router.post('/getUsersWishlistByCatalog', (req, res) => {
  let params = req.body
  if ( ! params.psid || !params.botid || !params.catalog ) {
    res.status( 404 ).send( 'UserID, BotId or catalog  not provided.' )
    return
  }
  request.post(`${CHATBOTSERVER}/webview/getUsersAllWishlistByCatalog`)
  .send({ psid: params.psid, botid: params.botid, catalog: params.catalog })
  .then( result => {
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error getting wishlist from server.')
  })
 })

 /**
 * Clear all wishlists
 */
router.post( '/clearAllWishlists', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid ) {
    res.status( 404 ).send( 'UserId or BotId not provided.' )
    return
  }
  // console.log( 'getUsersAllCarts -> ', params )
  request.post( `${CHATBOTSERVER}/webview/clearAllWishlists` )
  .send({ psid: params.psid, botid: params.botid })
  .then( result => {
    // console.log( 'getUsersAllCarts -> ', JSON.parse( result.text ) )
    res.sendStatus( 200 )
    // res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error fetching user from server.' )
  })
})

/**
 * Clear wishlist for a given catalog
 */
router.post( '/clearWishlistByCatalog', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.catalog ) {
    res.status( 404 ).send( 'UserId, BotId or Catalog name not provided.' )
    return
  }
  // console.log( 'clearCartByCatalog -> ', params )
  request.post( `${CHATBOTSERVER}/webview/clearWishlistByCatalog` )
  .send({ psid: params.psid, botid: params.botid, catalog: params.catalog })
  .then( result => {
    // console.log( 'clearCartByCatalog -> ', JSON.parse( result.text ) )
    res.sendStatus( 200 )
    // res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error clearing wishlist from server.' )
  })
})

/*******************************************
************** Product Details *************
********************************************/

/**
 * Get a product's details
 */
router.post( '/getProductDetails', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.xc_sku ) {
    res.status( 404 ).send( 'UserId, BotId or xc_xku not provided.' )
    return
  }
  // console.log( 'getProductDetails -> ', params )
  request.post( `${CHATBOTSERVER}/webview/getProductDetails` )
  .send({ psid: params.psid, botid: params.botid, xc_sku: params.xc_sku })
  .then( result => {
    // console.log( 'getProductDetails -> ', JSON.parse( result.text ) )
    // res.sendStatus( 200 )
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error getting product from server.' )
  })  
})

/**
 * Show similar products button
 */
router.post( '/showSimilarProducts', ( req, res ) => {
  let params = req.body
  if ( ! params.psid || ! params.botid || ! params.xc_sku ) {
    res.status( 404 ).send( 'UserId, BotId or xc_xku not provided.' )
    return
  }
  // console.log( 'showSimilarProducts -> ', params )
  request.post( `${CHATBOTSERVER}/webview/showSimilarProducts` )
  .send({ psid: params.psid, botid: params.botid, xc_sku: params.xc_sku })
  .then( result => {
    // console.log( 'showSimilarProducts -> ', JSON.parse( result.text ) )
    // res.sendStatus( 200 )
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error getting product from server.' )
  })  
})

/**
 *Show Recommended Products
 */
router.post( '/recommendProduct', (req, res) => {
  let params = req.body
  if( ! params.psid || ! params.botid || ! params.type) {
    res.status( 404 ).send( 'UserId or BotId or Type not provided.')
    return
  }
  request.post(`${CHATBOTSERVER}/webview/recommendProduct`)
  .send({ psid: params.psid, botid: params.botid, type: params.type })
  .then( result => {
    res.send( JSON.parse( result.text ) )
  })
  .catch( err => {
    console.log( err )
    res.status(err.status).send( 'Error getting Recommendations from server.' )
  })
})

/**
  *Get Group SKU
  */
  router.post( '/getGroupProductDetails', (req, res) => {
    let params = req.body
    if( ! params.psid || ! params.botid || !params.group_sku) {
      res.status(404).send( 'UserId, BotId or group_sku not provided.')
      return
    }
    request.post(`${CHATBOTSERVER}/webview/getGroupProductDetails`)
    .send({psid: params.psid, botid: params.botid, group_sku: params.group_sku })
    .then( result => {
      res.send( JSON.parse( result.text ) )
    })
    .catch( err => {
      console.log( err )
      res.status(err.status).send(' Error getting Group Product details from server.')
    })
  })

/*******************************************
************** Utility requests *************
********************************************/

/**
  *Get URL and AppID
  */
  router.post('/getConf', (req, res) => {
    let appID = nconf.get('appID')
    let URL = nconf.get('WebviewURL')
    let conf = {
      appID : appID,
      url : URL
    }
    if (appID && URL) {
      res.send(conf)
    } else {
      res.status(400).send('Error getting AppID and url')
    }
  })

/**
  *Encode PSID and ID
  */
  router.post('/getUniqueID', (req, res) => {
    let params = req.body
    let pArr = []
    let psid = params.psid
    let quesId = params.quesId
    if (quesId && psid && quesId.length === 6) {
      for (var i = 0; i < psid.length-1; i+=3) {
        pArr.push(psid.slice(i,i+3))
      }
      for (var j = 0; j < quesId.length-1; j++) {
        pArr[j] = quesId[j].concat(pArr[j])
      }
      pArr.push(quesId.charAt(quesId.length-1) + psid.charAt(psid.length-1))
      let finalID = {
        encodedID : pArr.join('')
      }
      if (finalID) {
        res.send(JSON.stringify(finalID))
        return
      } else {
        res.status(400).send('Error encoding')
        return
      }
    } else {
      res.status(400).send('No psid or id provided')
      return
    }
  })

/**
  *Decode PSID and ID
  */
  router.post('/decodeUniqueID', (req, res) => {
    let params = req.body
    let pArr = []
    let quesId = ''
    let uid = params.uid
    if (uid) {
      for (var i = 0; i < uid.length-2; i+=4) {
        let sliceArr = uid.slice(i,i+4)
        pArr.push(sliceArr.slice(1,4))
        quesId += sliceArr[0]
      }
      pArr.push(uid.charAt(uid.length-1))
      quesId += uid.charAt(uid.length-2)
      let finalIds = {
        psid : pArr.join(''),
        entryID : quesId
      }
      if (finalIds) {
        res.send(JSON.stringify(finalIds))
        return
      } else {
        res.status(400).send('Error decoding')
        return
      }
    } else {
      res.status(400).send('UniqueID not provided')
    }
  })

/**
  *Get Facebook UserProfile
  */
  router.post('/getUserInfo', (req, res) => {
    let params = req.body
    if (!params.psid) {
      res.status(400).send('UserID not provided')
      return
    }
    let psid = params.psid
    request.get('https://graph.facebook.com/v2.6/'+psid+'?access_token='+ACCESS_TOKEN)
    .then(result => {
      res.send( result.text )
    })
    .catch( err => {
      console.log(err)
      res.status(err.status).send('Error getting UserInfo from Facebook')
    })
  })

/*******************************************
************** Share with Friend ***********
********************************************/

/**
  *Retrieve Questions
  */
  router.post('/getAllPredefQuestions', (req, res) => {
    let params = req.body
    if (!params.psid || !params.botid) {
      res.status(400).send('UserID or BotId not provided')
      return
    }
    request.post(`${CHATBOTSERVER}/webview/getAllPredefQuestions`)
    .send({psid: params.psid, botid: params.botid})
    .then(result => {
      res.send( JSON.parse( result.text ) )
    })
    .catch( err => {
      console.log( err )
      res.status(err.status).send('Error getting PreDef Questions from server.')
    })
  })

/**
  *Get All User Selected Questions
  */
  router.post('/getUserQuestions', (req, res) => {
    let params = req.body
    if (!params.psid || !params.botid || !params.entryID) {
      res.status(400).send('UserID or BotId or UniqueID not provided.')
      return
    }
    request.post(`${CHATBOTSERVER}/webview/getUserQuestions`)
    .send({psid: params.psid, botid: params.botid, entryID : params.entryID})
    .then(result => {
      res.send( JSON.parse( result.text ))
    })
    .catch( err => {
      console.log(err)
      res.status(err.status).send('Error getting User filled Questions from server.')
    })
  })


  /**
    *Add User Questions
    */
    router.post('/addUserQuestions', (req, res) => {
      let params = req.body
      if (!params.psid || !params.botid || !params.questions || !params.sender_name) {
        res.status(400).send('UserID or pageid not provided')
        return
      }
      request.post(`${CHATBOTSERVER}/webview/addUserQuestions`)
      .send({psid : params.psid, botid : params.botid, 
        sender_name : params.sender_name, questions : params.questions})
      .then(result => {
        res.send(JSON.parse(result.text))
      })
      .catch(err => {
        console.log(err)
        res.status(err.status).send('Error Adding Questions to server')
      })
    })

  /**
    *Send Answers
    */
    router.post('/sendUserAnswers', (req, res) => {
      let params = req.body
      if (!params.senderPsid || !params.receiverPsid || (!params.answers) ) {
        res.status(400).send('UserID or answers not provided')
        return
      }
      request.post(`${CHATBOTSERVER}/webview/sendUserAnswers`)
      .send({psid : params.senderPsid, receiverPsid : params.receiverPsid,
        answers : JSON.stringify(params.answers), sender_name : params.sender_name, receiver_name : params.receiver_name,
        botid : params.botid, feedback_id : params.feedback_id})
      .then(result => {
        res.send(JSON.parse(result.text))
      })
      .catch( err => {
        console.log(err)
        res.status(err.status).send('Error sending answers to server.')
      })
    })

  /**
    *Retreive User Answers
    */
    router.post('/retrieveUserAnswer', (req, res) => {
      let params = req.body
      if (!params.psid || !params.entryID) {
        res.status(400).send('UserID or key not provided')
        return
      }
      request.post(`${CHATBOTSERVER}/webview/retrieveUserAnswer`)
      .send({psid : params.psid, entryID : params.entryID, botid : params.botid})
      .then(result => {
        res.send(JSON.parse(result.text))
      })
      .catch( err => {
        console.log(err)
        res.status(err.status).send('Error getting answers from server')
      })
    })

  /**
    *Retrieve All Answers
    */
    router.post('/retrieveAllAnswers', (req, res) => {
      let params = req.body
      if (!params.psid || !params.feedback_id) {
        res.status(400).send('UserID or key not provided')
        return
      }
      request.post(`${CHATBOTSERVER}/webview/retrieveAnswer`)
      .send({psid : params.psid, feedback_id : params.feedback_id, botid : params.botid})
      .then(result => {
        res.send(JSON.parse(result.text))
      })
      .catch( err => {
        res.status(err.status).send('Error getting all answers from server')
      })
    })

  /*******************************************
  ************** Retailers ***********
  ********************************************/

  // Get list of all the retailers present in DB
  router.post('/getallRetailers', (req,res) => {
    let params = req.body
    if ( ! params.psid || ! params.botid ) {
      res.status( 404 ).send( 'UserId or BotId not provided' )
      return
    }
    request.post( `${CHATBOTSERVER}/webview/getallRetailers` ).send( { psid: params.psid, botid: params.botid } )
      .then( result => {
        console.log(JSON.parse( result.text ))
        res.send( JSON.parse( result.text ))
      })
      .catch( err => {
        console.log("error in feedback is ",err)
        res.status( err.status ).send( 'Error getting all retailers')
     })  
  })

  // Get the User preference of all the retailers
  router.post('/getretailerPreference', (req,res) => {
    let params = req.body
    if ( ! params.psid || ! params.botid ) {
      res.status( 404 ).send( 'UserId or BotId not provided' )
      return
    }
    request.post( `${CHATBOTSERVER}/webview/getretailerPreference` ).send( { psid: params.psid, botid: params.botid } )
    .then( result => {
      console.log(JSON.parse( result.text ))
      res.send( JSON.parse( result.text ))
    })
    .catch( err => {
      console.log("error in feedback is ",err)
      res.status( err.status ).send( 'Error getting retailer preference')
    })  
  })

  // Set the User preference of all the retailers
  router.post('/setretailerPreference', (req,res) => {
    let params = req.body
    if ( ! params.psid || ! params.botid || ! params.selectedretailers) {
      res.status( 404 ).send( 'UserId or BotId not provided' )
      return
    }
    request.post( `${CHATBOTSERVER}/webview/setretailerPreference` ).send( { psid: params.psid, botid: params.botid, selectedretailers: params.selectedretailers} )
    .then( result => {
      console.log(JSON.parse( result.text ))
      res.send( JSON.parse( result.text ))
    })
    .catch( err => {
      console.log("error in feedback is ",err)
      res.status( err.status ).send( 'Error setting retailer preference')
    })  
  })    

module.exports = router
