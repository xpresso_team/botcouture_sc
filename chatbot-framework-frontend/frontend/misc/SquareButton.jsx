import React from 'react'
// var Loader = require('halogenium/DotLoader')
var classNames = require('classnames');

class SquareButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var btnClasses = classNames(
      'btn-sm', 'btn',
      {
        'btn-warning': this.props.selected,
      },
    )
    return (
    <button type='button' className={btnClasses} onMouseOver={this.props.mouseHandler} onClick={this.props.clickHandler} >
    {this.props.buttonValue }
    </button> );
  }
}
module.exports = SquareButton
