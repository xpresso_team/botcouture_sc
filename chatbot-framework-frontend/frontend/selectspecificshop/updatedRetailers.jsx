import React from 'react';
import _ from 'lodash';
import request from 'superagent'
import { Link } from 'react-router-dom'
import TopHeader from '../misc/TopHeader'
import classNames from 'classnames'
import Spinner from '../misc/SpinnerComponent'
import ButtonWide from '../misc/ButtonWide'
import ErrorComponent from '../misc/ErrorComponent'
import SubmitRetailers from './SubmitRetailers'
import RepeatCarousel from '../misc/RepeatCarousel'

class SelectRetailers extends React.Component {
    constructor(props) {
        super(props)
        this.state= {
            retailers : [{value:"https://images.google.com/images/branding/googleg/1x/googleg_standard_color_128dp.png",key:"image1",selected:false},
                         {value:"https://images.google.com/images/branding/googleg/1x/googleg_standard_color_128dp.png",key:"image2",selected:true},
                         {value:"https://images.google.com/images/branding/googleg/1x/googleg_standard_color_128dp.png",key:"image3",selected:false},
                         {value:"https://images.google.com/images/branding/googleg/1x/googleg_standard_color_128dp.png",key:"image5",selected:false}],
            selectedretailers : [],
            errorText : '',            
            showSpinner : true,
            clear_selection : false,
            select_all :true,
            arr : [0,1,2,3,4]
        }
    }

    componentDidMount() {
        request.post('/api/getallRetailers')
        .send({ 
            psid : this.props.query.psid, 
            botid : this.props.query.botid 
        })
        .then( res => {
            console.log( 'ALL RETAILERS', JSON.parse( res.text ) )
            let retailerlist = JSON.parse( res.text )
            let errorText = null
            if ( retailerlist.length === 0 ) {
                errorText = 'There are no retailers in the Database!'
            }
            this.setState({ 
                showSpinner : false,
                errorText   : errorText,
                totalpages  : Math.ceil(retailerlist.length/12),
            })
            let temp=retailerlist.map(retailer =>
                let tempret = {}
                tempret.key = retailer.key
                tempret.value = "https://s3.amazonaws.com/xpressobrand/images/"+retailer.key
                tempret.selected = false
                return tempret;
            )
            this.setState({
                retailers : temp
            })
            console.log(this.state.retailers)
        })
        .catch( err => {
            console.log( err )
            if ( err && err.status ) {
                this.setState( { showSpinner : false, errorText: err.response.text } )
            } 
            else {
                this.setState( { showSpinner : false, errorText: 'Server Error' } )
            }
        })

        request.post('/api/retailerPreference')
        .send({
            psid : this.props.query.psid,
            botid : this.props.query.botid
        })
        .then( res => {
            console.log('Retailers preference of User', JSON.parse(res))
            let resjson = JSON.parse(res)
            let retailerpreference = resjson.retailers
            if(retailerpreference.length===0) {
                console.log("there are no preferred retailers by the user")
            }
            let tempsel = retailerpreference.map(function(str) {
                    let sel = {}
                    sel.key = str
                    sel.value = "https://s3.amazonaws.com/xpressobrand/images/"+str
                    sel.selected = true
                    return sel
                })
            let temptotal = _.cloneDeep(this.state.retailers)
            let i,flag
            for(i=0;i<tempsel.length;i++) {
                flag= temptotal.findIndex(i => i.key === tempsel[i].key)
                temptotal[flag].selected = true
            }            
            this.setState({
                retailers : temptotal,
            })
            console.log(temptotal)
        })
        .catch( err => {
            console.log( err )
            if ( err && err.status ) {
                this.setState( { showSpinner : false, errorText: err.response.text } )
            } 
            else {
                this.setState( { showSpinner : false, errorText: 'Server Error' } )
            }
        })
        this.setState({
            showSpinner: false,
        })    
    }       

    gotoExit() {
        window.MessengerExtensions.requestCloseBrowser( function success() {
            return;
            }, function error(err) {
            console.error( err, 'Unable to close window.', 'You may be viewing outside of the Messenger app.' )
        })
    }

    gotoSelect(val) {
        let cp = this.state.currentpage
        let flag = this.state.arr.findIndex(i => i.key===val.key);
        let indexval = cp*12+flag
        let tempret1 = _.cloneDeep(this.state.retailers)
        let tempret2 = _.cloneDeep(this.state.arr)
        let tempret3 = _.cloneDeep(this.state.selectedretailers)
        let i
        let flag2 = -1
        for(i=0;i<tempret3.length;i++) {
            if(tempret3[i].key===val.key) {
                flag2 =i;
            }
        }
        if(flag2 === -1) { 
            tempret1[indexval].selected=true
            tempret2[flag].selected = true
            val.selected = true
            tempret3.push(val)
        }
        else {
            tempret1[indexval].selected = false
            tempret2[flag].selected =false
            tempret3.splice(flag2,1)
        }
        this.setState({
            retailers : tempret1,
            selectedretailers : tempret3,
            arr : tempret2
            }, console.log(tempret3))
    }

    render() {
        let page = null;
        page = <div>
                <div className='row'>
                    <div className='col-xs-12' style={{ marginTop: '15px', fontFamily:'Arial Black' }}>
                        <TopHeader headerText="Select Retailers"/>
                    </div>
                </div>
             

                <div id='productCarousel' className='carousel slide multi-item-carousel' data-interval='false'>
                    <div className='carousel-inner'>
                        { this.state.retailers && this.state.retailers.map( retailer =>
                            <RepeatCarousel key={retailer.key} product={recommendItem} psid={this.props.query.psid} botid={this.props.query.botid}/>
                        )}   
                    </div>
                    <a className="left carousel-control" href="#productCarousel" data-slide="prev" style={{background:'none'}}>
                        <span className="glyphicon glyphicon-chevron-left" style={{color:'#000'}}></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    
                    <a className="right carousel-control" href="#productCarousel" data-slide="next" style={{background:'none'}}>
                        <span className="glyphicon glyphicon-chevron-right" style={{color:'#000'}}></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>                

                <div style={{ position: 'fixed', bottom: '0', width: '100%', marginLeft: '-15px', padding: '15px 0', background: 'white', fontFamily:'Times New Roman' }} >
                    <div className='row center-block'>
                        <div className='col-xs-6'>
                            <ButtonWide buttonText="EXIT" selected={false} clickHandler={ e => this.gotoExit(e) }/>
                        </div>
                        <div className='col-xs-6'>
                            {
                            Boolean(this.state.selectedretailers.length)?       
                                <button type='button' className='btn-lg btn-block btn-warning' style={{ fontSize: '14px' }} >SHOP SELECTED</button> :
                                <button type='button' className='btn-lg btn-block' style={{ fontSize: '14px' }} disabled>SHOP SELECTED</button>
                            }
                        </div>
                    </div>
                </div>                                        
               
               </div>

        if ( this.state.showSpinner ) {
            page = <Spinner />
        }

        return( 
            <div>{page}</div>
        );
    }
}

module.exports= SelectRetailers