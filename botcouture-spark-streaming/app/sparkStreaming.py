"""
@author : Mohit Agrawal
@email  : mohit.agrawal@abzooba.com
"""
import traceback
import json
import sys
import pyinotify

import dateutil.parser as dp
import sqlalchemy
from sqlalchemy import exc

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

# from pyspark.streaming.kafka import TopicAndPartition

userQuery = 'INSERT INTO user (user_id, first_active_ts, first_active_ts_date, first_name, last_name, ' \
            'profile_pic_link, locale, timezone, gender, is_payment_enabled, marked_spam, has_blocked) values (%s,%s,' \
            '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) '
chatlogQuery = 'INSERT INTO chatlog (user_id, timestamp, timestamp_date, channel_id, session_id, message_number, ' \
               'message_type, message_chat, message_type_flag, visual_search, product_response_list, ' \
               'nlp_query_response, vision_file_link, vision_engine_response, quick_reply_button) VALUES (%s,%s,%s,' \
               '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) '
chatlogQueryWithFeedback = 'INSERT INTO chatlog (user_id, timestamp, timestamp_date, channel_id, session_id, ' \
                           'message_number, message_type, message_chat, message_type_flag, visual_search, ' \
                           'product_response_list, nlp_query_response, vision_file_link, vision_engine_response, ' \
                           'quick_reply_button, feedback_type, feedback_img, sentiment, feedback_id ) VALUES (%s,%s,' \
                           '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) '
clickQuery = 'INSERT INTO click (user_id, session_id, message_number, timestamp, timestamp_date, click_item) VALUES (' \
             '%s,%s,%s,%s,%s,%s) '

BOT_DB_MAP_FN = 'bot_list.json'
CONFIG_FN = 'config.json'
DB_SQL_FN = 'app/base.sql'


def loadConfig(filepath):
    with open(filepath) as f:
        cfg = json.loads(f.read())
    return cfg


def checkAndCreateDB():
    global dbName

    engine = sqlalchemy.create_engine(
        'mysql://' +
        config['DBUSER'] + ':' + config['DBPASS'] +
        '@' + config['DB_ADDRESS'])
    conn = engine.connect()

    existing_databases = conn.execute("SHOW DATABASES;")
    existing_databases = [_[0] for _ in existing_databases]

    for botID, db in dbName.items():
        if db not in existing_databases:
            conn.execute("CREATE DATABASE " + db + ';')
            conn.execute("USE " + db + ';')
            with open(DB_SQL_FN) as f:
                conn.execute(f.read())
    conn.close()


def loadDBNames(filepath):
    global dbName
    dbName = {}
    try:
        with open(filepath) as f:
            bot_list = json.loads(f.read())

        for data in bot_list:
            dbName[data['id']] = data['db']

        checkAndCreateDB()
    except Exception as e:
        pass


class EventHandler(pyinotify.ProcessEvent):
    def process_IN_MODIFY(self, event):
        if event.pathname.endswith(BOT_DB_MAP_FN):
            loadDBNames(event.pathname)
            # elif event.pathname.endswith('config.json'):
            #     loadConfig( event.pathname )


# return a tuple of arguments from the json data
def getUserArgs(data):
    address = data.get('address')
    if address:
        # channelId = address['channelId']
        user = address.get('user')
        if user:
            userId = user.get('id')
            userName = user.get('name', '').split()
            firstName = userName[0]
            if len(userName) > 1:
                lastName = userName[1]
            else:
                lastName = ''
        else:
            userId, userName, firstName, lastName = \
                None, None, None, None
    else:
        user, userId, userName, firstName, lastName = \
            None, None, None, None, None

    ts = dp.parse(data.get('timestamp'))
    if ts:
        firstActiveTs = ts.strftime('%Y-%m-%d %H:%M:%S')
        firstActiveTs_date = str(ts.date())
    else:
        firstActiveTs, firstActiveTs_date = None, None

    profilePicLink = data.get('profile_pic_link')
    locale = data.get('locale')
    timezone = data.get('timezone')
    gender = data.get('gender')
    isPaymentEnabled = data.get('is_payment_enabled')
    markedSpam = data.get('marked_spam')
    hasBlocked = data.get('has_blocked')

    return [userId, firstActiveTs, firstActiveTs_date,
            firstName, lastName, profilePicLink, locale,
            timezone, gender, isPaymentEnabled, markedSpam,
            hasBlocked]


def getChatlogArgs(data):
    address = data.get('address')
    if address:
        # bot = address.get('bot')
        channelId = address.get('channelId')
        user = address.get('user')
        if user:
            userId = user.get('id')
    else:
        channelId, user, userId = None, None, None

    ts = dp.parse(data.get('timestamp'))
    if ts:
        timestamp = ts.strftime('%Y-%m-%d %H:%M:%S')
        timestamp_date = str(ts.date())
    else:
        timestamp, timestamp_date = None, None

    sessionId = data.get('session_id')
    messageNumber = data.get('message_number')
    messageType = data.get('message_type')
    messageChat = data.get('message_chat')
    messageTypeFlag = data.get('message_type_flag')
    visualSearch = data.get('visual_search')
    if data.get('product_response_list', '') == '':
        productResponseList = json.dumps({})
    else:
        productResponseList = json.dumps(data.get('product_response_list'))
    nlpQueryResponse = str(data.get('nlp_query_response', ''))
    visionFileLink = data.get('vision_file_link')
    visionEngineResponse = str(data.get('vision_engine_response', ''))
    quickReplyButtons = data.get('quick_reply_buttons')

    feedback = data.get('feedback')
    if feedback:
        feedback_type = feedback.get('feedback_type')
        feedback_img = feedback.get('feedback_img')
        sentiment = feedback.get('sentiment')
        feedback_id = feedback.get('feedback_id')
    else:
        feedback_type = ''
        feedback_img = ''
        sentiment = ''
        feedback_id = ''

    return (userId, timestamp, timestamp_date, channelId,
            sessionId, messageNumber, messageType,
            messageChat, messageTypeFlag, visualSearch,
            productResponseList, nlpQueryResponse,
            visionFileLink, visionEngineResponse,
            quickReplyButtons, feedback_type, feedback_img,
            sentiment, feedback_id)


def getClickArgs(data):
    if data.get('click_item_sku', "") != "":
        clickItem = data.get('click_item_sku')
        address = data.get('address')
        if address:
            user = address.get('user')
            if user:
                userId = user.get('id')
            else:
                userId = None
        else:
            user, userId = None, None

        sessionNumber = data.get('session_id')
        messageNumber = data.get('message_number')
        ts = dp.parse(data.get('timestamp'))
        if ts:
            timestamp = ts.strftime('%Y-%m-%d %H:%M:%S')
            timestamp_date = str(ts.date())

        return (userId, sessionNumber, messageNumber,
                timestamp, timestamp_date, clickItem)

    return None


# submit query into db
def submitIntoUser(data, conn):
    try:
        args = getUserArgs(data)
        # for v in args: print(v)
        conn.execute(userQuery, args)
    except exc.IntegrityError:
        pass
    except Exception as e:
        print('\n\n')
        print(data)
        print('\n')
        print('Error in submit into user')
        print(type(e).__name__)
        print(e)
        print('\n')
        traceback.print_exc()


def submitIntoChatlog(data, conn):
    try:
        args = getChatlogArgs(data)
        # for v in args: print(v)
        # if feedback_type is empty
        if args[-4] == '':
            conn.execute(chatlogQuery, args[:-4])
        else:
            conn.execute(chatlogQueryWithFeedback, args)
    except Exception as e:
        print('\n\n')
        print(data)
        print('\n')
        print('Error in submit into chatlog')
        print(type(e).__name__)
        print(e)
        print('\n\n')
        traceback.print_exc()


def submitIntoClick(data, conn):
    try:
        args = getClickArgs(data)
        if args:
            conn.execute(clickQuery, args)
    except Exception as e:
        print('\n\n')
        print(data)
        print('\n')
        print('Error in submit into click')
        print(type(e).__name__)
        print(e)
        print('\n\n')


def sendPartition(iter, dbuser, dbpass, dbaddr):
    try:
        SQLengine = sqlalchemy.create_engine(
            'mysql://' +
            dbuser + ':' + dbpass + '@' + dbaddr +
            '/?charset=utf8&use_unicode=0')

        connection = SQLengine.connect()
        # transaction = connection.begin()
    except Exception as e:
        print('\nError, cannot connect to sql server')
        print(type(e).__name__)
        print(e)
        return

    for record in iter:
        try:

            # load data from json and
            # submit into various tables

            data = json.loads(record[1])

            botId = data['address']['bot']['id']
            if botId in dbName:
                connection.execute('use ' + dbName[botId] + ';')
                # print '\n',channelId
                # print data
                # print( 'processing...\n')
                submitIntoUser(data, connection)
                submitIntoChatlog(data, connection)
                submitIntoClick(data, connection)
        except Exception as e:
            print('\nError before inserting data')
            print(type(e).__name__)
            print(e)

    # return connection to connection pool
    if connection:
        # transaction.commit()
        connection.close()


def createContext(config):
    sc = SparkContext(appName='SparkStreamingFromKafka')
    sc.setLogLevel("ERROR")
    ssc = StreamingContext(sc, 2)  # 2 second window
    # consumerGroupId = 'spark-streaming-consumer-local-111'
    # zookeeper = '34.203.102.251:2181'

    directKafkaStream = \
        KafkaUtils.createDirectStream(ssc,
                                      [config['TOPIC']],
                                      {"metadata.broker.list": config[
                                          'BROKER']})
    # fromOffsets={TopicAndPartition(topic,0):0})

    # directKafkaStream = KafkaUtils.createStream(ssc,
    #                               zookeeper,
    #                               consumerGroupId,
    #                               {topic: 1})

    # put stream data into db
    directKafkaStream \
        .foreachRDD(lambda rdd:
                    rdd.foreachPartition(lambda it:
                                         sendPartition(it,
                                                       config['DBUSER'],
                                                       config['DBPASS'],
                                                       config['DB_ADDRESS'])))

    # store the stream onto HDFS
    '''
    directKafkaStream \
        .foreachRDD(lambda rdd:
                    rdd.isEmpty() == False
                    and rdd.saveAsTextFile(
                        'hdfs://' + config['HADOOP_ADDRESS'] + '/kafka-logs/' +
                        str(time.time()) + str(rdd.id())
                        )
                   )

    '''
    ssc.checkpoint(config['CHECKPOINT_DIRECTORY'])
    return ssc


def main():
    global config

    print(sys.argv)
    if len(sys.argv) >= 2:
        config_file_dir = sys.argv[1]
    else:
        print('ERROR')
        print('Provide configuration files\' directory')
        sys.exit(1)

    config = loadConfig(config_file_dir + CONFIG_FN)
    loadDBNames(config_file_dir + BOT_DB_MAP_FN)

    wm = pyinotify.WatchManager()
    notifier = pyinotify.ThreadedNotifier(wm, EventHandler())
    wdd = wm.add_watch(config_file_dir, pyinotify.IN_MODIFY, rec=True)
    notifier.start()

    ssc = StreamingContext \
        .getOrCreate(config['CHECKPOINT_DIRECTORY'],
                     lambda: createContext(config))

    ssc.start()
    ssc.awaitTermination()


if __name__ == '__main__':
    # botID: db_name( on MemSQL ) mapping
    dbName = {}
    config = {}
    main()
