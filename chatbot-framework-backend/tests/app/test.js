// commenting out as those are not in sync with the latest code base
// var assert = require('assert')
// var UtilService = require( '../lib/services/UtilService' )


// describe('UtilService', function() {
//   describe('pageList()', function() {
//     it('should return paged window of an array', function() {
//       let itemList = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 ]
//       let result   = null
//       let previousPageText = 'Previous Page'
//       let nextPageText     = 'Next Page'
//       // Page 0   
//       result = UtilService.pageList( itemList, 0, 11, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 0, 1, 2, 3, 4, 5, 6, 7, 8, nextPageText ], result )
    
//       result = UtilService.pageList( itemList, 0, 11, null, null )
//       assert.deepEqual( [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ], result )
      
//       result = UtilService.pageList( itemList, 0, 11, null, nextPageText )
//       assert.deepEqual( [ 0, 1, 2, 3, 4, 5, 6, 7, 8, nextPageText ], result )  
      
//       result = UtilService.pageList( itemList, 0, 11, previousPageText, null )
//       assert.deepEqual( [ previousPageText, 0, 1, 2, 3, 4, 5, 6, 7, 8 ], result )  
      
//       // Page 1
//       result = UtilService.pageList( itemList, 1, 11, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 9, 10, 11, 12, 13, 14, 15, 16, 17, nextPageText ], result )
    
//       result = UtilService.pageList( itemList, 1, 11, null, null )
//       assert.deepEqual( [ 9, 10, 11, 12, 13, 14, 15, 16, 17 ], result )
      
//       result = UtilService.pageList( itemList, 1, 11, null, nextPageText )
//       assert.deepEqual( [ 9, 10, 11, 12, 13, 14, 15, 16, 17, nextPageText ], result )  
      
//       result = UtilService.pageList( itemList, 1, 11, previousPageText, null )
//       assert.deepEqual( [ 'Previous Page', 9, 10, 11, 12, 13, 14, 15, 16, 17 ], result )  
//     })
//     it('Should handle when there are not enough elements in the array and should return partial last page', function() {
      
//       let itemList = [ 0, 1, 2, 3, 4, 5 ]
//       let result   = null
      
//       let previousPageText = 'Previous Page'
//       let nextPageText     = 'Next Page'
      
//       result = UtilService.pageList( itemList, 0, 11, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 0, 1, 2, 3, 4, 5, nextPageText ], result )

//       result = UtilService.pageList( itemList, 1, 6, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 4, 5, nextPageText ], result )

//     })
//     it('Should wrap around for page numbers outside array size', function() {
      
//       let itemList         = [ 0, 1, 2, 3, 4, 5 ]
//       let result           = null
//       let previousPageText = 'Previous Page'
//       let nextPageText     = 'Next Page'
      
//       result = UtilService.pageList( itemList, 3, 4, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 0, 1, nextPageText ], result )

//       result = UtilService.pageList( itemList, 3, 6, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 4, 5, nextPageText ], result )
      
//     })
//     it( 'Should work with empty arrays', function() {
//       let itemList         = [ ]
//       let result           = null
//       let previousPageText = 'Previous Page'
//       let nextPageText     = 'Next Page'
//       result = UtilService.pageList( itemList, 3, 4, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, nextPageText ], result )
//     })
//     it( 'Should throw error for null arrays')
//     /* it( 'Should throw error for null arrays'), function() {
//       // TODO: Figure out how to assert Errors
//       let itemList         = null
//       let result           = null
//       let previousPageText = 'Previous Page'
//       let nextPageText     = 'Next Page'
//       assert.throws( function ( ) { return UtilService.pageList( itemList, 3, 4, previousPageText, nextPageText ) } , Error, "Error: array null")
//     }) */
//     it( 'Should work with -ve page numbers', function() {

//       let itemList         = [ 0, 1, 2, 3, 4, 5, 6 ]
//       let result           = null
//       let previousPageText = 'Previous Page'
//       let nextPageText     = 'Next Page'

//       result = UtilService.pageList( itemList, -1, 4, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 6, nextPageText ], result )
      
//       result = UtilService.pageList( itemList, -2, 4, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 4, 5, nextPageText ], result )
      
//       result = UtilService.pageList( itemList, -3, 4, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 2, 3, nextPageText ], result )
      
//       result = UtilService.pageList( itemList, -4, 4, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 0, 1, nextPageText ], result )
      
//       result = UtilService.pageList( itemList, -5, 4, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 6, nextPageText ], result )
      
//       result = UtilService.pageList( itemList, -6, 4, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 4, 5, nextPageText ], result )
      
//       result = UtilService.pageList( itemList, -7, 4, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 2, 3, nextPageText ], result )
      
//       result = UtilService.pageList( itemList, -8, 4, previousPageText, nextPageText )
//       assert.deepEqual( [ previousPageText, 0, 1, nextPageText ], result )
      
//     })
//   })
// })