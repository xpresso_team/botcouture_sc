var request = require('superagent')
var Constants = require('../constants')
var url = require('url')
const nconf = require('nconf')
var UtilService = require('../services/UtilService')

// TODO: Rename to Image Upload Recognizer
module.exports = {

    recognize: function (context, callback) {

        let text = context.message.text || ''

        // TODO: try validUrl package http://stackoverflow.com/questions/30931079/validating-a-url-in-node-js

        UtilService.ifimgUrl(text, function (responseHeaders) {
            if (( ( responseHeaders ) && ( responseHeaders['content-type'] ) && ( responseHeaders['content-type'].match(/image/gi) ) )) {

                if (nconf.get("Visual Search")) {
                    let intent = {
                        intent: 'RESULTS.DIALOG',
                        score: 1,
                        entities: [{
                            entity: 'image',
                            entityType: 'UPLOAD.IMAGE',
                            data: {NODE: Constants.NODE.IMAGEUPLOAD, resetState: true, state: {imageUrl: text}}
                        }]
                    }
                    console.log('image-url-recognizer ->')

                    // ANALYTICS
                    context.userData.analytics.in.message_type_flag = 'II'
                    context.userData.analytics.in.vision_file_link = intent.entities[0].data.state.imageUrl

                    callback(null, intent)
                } else {

                    let intent = {
                        intent: 'FEATURE.DISABLE',
                        score: 1,
                        entities: [{
                            data: {feature: "Visual Search"}
                        }]
                    }
                    console.log('image-url-recognizer ->')
                    callback(null, intent)
                }
            } else {
                // Not an image
                console.log('image-url-recognizer <-')
                callback(null, {intent: null, score: 0})
            }
        })
    }

}
