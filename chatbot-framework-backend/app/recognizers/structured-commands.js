var Constants    = require( '../constants'             )
var StateService = require( '../services/StateService' )
var TextService  = require( '../services/TextService'  )

module.exports = {

    recognize: function ( context, callback ) {

        let text = context.message.text || ''

        text = text.toLowerCase()
        text = text.trim()
        console.log("text",text)


        let intent = null
        let entities = [{entity: null, data: {}}]


        switch (text) {
            case 'smooch:conversation:start' :
                intent = 'PERSONALIZATION.WELCOME';
                break;
            case 'Get Styled'.toLowerCase() :
                intent = 'PERSONALIZATION.WELCOME';
                break;
            case 'SPECIALIST'.toLowerCase()        :
                intent = 'HUMAN.AUGMENTATION';
                entities[0].data.user_type = "user";
                break;
            case Constants.Labels.Introduction.toLowerCase()        :
                intent = 'INTRODUCTION';
                break;
            case 'Start'.toLowerCase()                                 :
                intent = 'INTRODUCTION';
                break;
            case 'Restart'.toLowerCase()                                 :
                intent = 'INTRODUCTION';
                break;
            /*case 'Hi'.toLowerCase()                                 :
                intent = 'PERSONALIZATION.WELCOME';
                break;*/
            case 'GET_STARTED'.toLowerCase()                        :
                // intent = 'PERSONALIZATION.WELCOME';
                intent = 'INTRO_TO_BOT';
                break;
            case 'Get Started'.toLowerCase()                        :
                // intent = 'PERSONALIZATION.WELCOME';
                intent = 'INTRO_TO_BOT';
                break;
            case 'Ready'.toLowerCase()                        :
                intent = 'INTRO_Ready';
                break;
            case 'Wishlist FAQs'.toLowerCase()                      :
                intent = 'FAQ_CAT';
                entities[0].data.full_faq = true;
                break;
            case Constants.Labels.Tour.toLowerCase()                :
                intent = 'TOUR';
                break;
            case Constants.Labels.Inspiration.toLowerCase()         :
                intent = 'INSPIRATION';
                break;
            case 'Back To Search'.toLowerCase()                     :
                intent = 'SEARCH-ITEM';
                break;
            case 'BACK'.toLowerCase()                               :
                intent = 'RESULTS.DIALOG';
                break;
            case Constants.Labels.SearchItem.toLowerCase()          :
                intent = 'SEARCH-ITEM';
                break;
            case Constants.Labels.ExploreCollections.toLowerCase()  :
                intent = 'EXPLORE-COLLECTIONS';
                break;
            case Constants.Labels.ExploreDesignTrends.toLowerCase() :
                intent = 'EXPLORE-DESIGN-TRENDS';
                break;
            case Constants.Labels.ExploreGiftIdeas.toLowerCase()    :
                intent = 'EXPLORE-GIFT-IDEAS';
                break;
            case Constants.Labels.UploadImage.toLowerCase()         :
                intent = 'UPLOAD-IMAGE';
                break;
            case 'Back to Visual Search'.toLowerCase()              :
                intent = 'UPLOAD-IMAGE';
                break;
            case 'Back To Guided Search'.toLowerCase()              :
                intent = 'GUIDE-ME';
                break;
            case 'Back To Browse'.toLowerCase()              :
                intent = 'INSPIRATION';
                break;
            case Constants.Labels.GuideMe.toLowerCase()             :
                intent = 'GUIDE-ME';
                break;
            case 'New Query'.toLowerCase()                          :
                intent = 'TYPE.A.QUERY';
                entities[0].data.newQuery = true;
                break;
            case Constants.Labels.TypeQuery.toLowerCase()           :
                intent = 'TYPE.A.QUERY';
                entities[0].data.newQuery = true;
                break;
            case 'HELP'.toLowerCase()                               :
                intent = 'HELP_ASK';
                break;
            case 'Discover Features'.toLowerCase()                               :
                intent = 'HELP';
                break;
            case Constants.Labels.Help.toLowerCase()                :
                intent = 'HELP_ASK';
                break;
            case 'FAQ'.toLowerCase()                                :
                intent = 'FAQ_CAT';
                break;
            case 'Back to Videos'.toLowerCase()                     :
                intent = 'HELP_ASK';
                break;
            case 'Answer Not Found'.toLowerCase()                   :
                intent = 'NO_FAQ';
                break;
            case Constants.Labels.DemoCarousel.toLowerCase()        :
                intent = 'DEMO-CAROUSEL';
                break;
            case 'Profile'.toLowerCase()                            :
                intent = 'VIEW.PROFILE';
                break;
            case 'Start Shopping'.toLowerCase()                     :
                intent = 'INTRODUCTION';
                break;
            case 'Wishlist'.toLowerCase()                           :
                //intent = 'WISHLIST.VIEW.ALL.ITEMS';
                intent = 'VIEW.WISHLIST.WEBVIEW';
                break;
            case 'Back to Wishlist'.toLowerCase()                   :
                intent = 'WISHLIST.VIEW.ALL.ITEMS';
                break;
            case 'Feedback'.toLowerCase()                           :
                intent = 'Feedback';
                //intent = 'Feedback_New';
                break;
            case 'Give Feedback'.toLowerCase()                      :
                intent = 'Feedback';
                //intent = 'Feedback_New';
                break;
            case 'Back To Shopping'.toLowerCase()                      :
                intent = 'RESULTS.DIALOG';
                break;
            case 'Resume Shopping'.toLowerCase()                      :
                intent = 'RESULTS.DIALOG';
                break;
            case 'Shop mode'.toLowerCase()                      :
                intent = 'Shop-Mode';
                break;
            case  'Shop4Me'.toLowerCase()                      :
                // Shop for me
                intent = 'Shop-Option';
                entities[0].entity = "Shop4Me";
                break;
            case 'Shop4Other'.toLowerCase()                      :
                //shop for other
                intent = 'Shop-Option';
                entities[0].entity = 'Shop4Other';
                break;
            case '@login'.toLowerCase()                      :
                intent = 'AGENT.COMMANDS';
                entities[0].data.command = "login";
                break;
            case '@logout'.toLowerCase()                      :
                intent = 'AGENT.COMMANDS';
                entities[0].data.command = "logout";
                break;
            case '@register'.toLowerCase()                      :
                intent = 'AGENT.COMMANDS';
                entities[0].data.command = "register";
                break;
            case '@stop'.toLowerCase()                      :
                intent = 'HA.STOP';
                entities[0].data.mode = "agent";
                break;
            case '@status'.toLowerCase()                      :
                intent = 'AGENT.COMMANDS';
                entities[0].data.command = "status";
                break;
            case '@quit'.toLowerCase()                      :
                intent = 'AGENT.COMMANDS';
                entities[0].data.command = "quit";
                break;
            case '@end'.toLowerCase()                      :
                intent = 'AGENT.COMMANDS';
                entities[0].data.command = "end";
                break;

        }

        if (intent) {

            console.log("Structured-commands-recognizer ->", intent)
            
            // ANALYTICS
            context.userData.analytics.in.message_type_flag = 'IQR'    // User input is a quick reply press

            callback.call( null, null, {
                intent: intent,
                score: 1,
                entities: entities
            })

        } else {
            console.log("Structured-commands-recognizer <-")
            callback.call(null, null, {intent: null, score: 0});
        }

    }

}