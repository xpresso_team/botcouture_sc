var request = require('superagent')
var Constants = require('../constants')
var url = require('url')
const nconf = require('nconf')
var _ = require('lodash')
module.exports = {

    recognize: function (context, callback) {

        let socialDomains = ['www.instagram.com', 'www.pin.it', 'www.lookbook.nu']
        let text = context.message.text || ''
        let parsed = url.parse(context.message.text)

        if (parsed.hostname && _.find(socialDomains, domain => domain.match(parsed.hostname))) {

            let intent

            // Found a Social URL
            console.log('SOCIAL-url-recognizer ->')

            if (nconf.get("Visual Search")) {
                // ANALYTICS
                context.userData.analytics.in.message_type_flag = 'II'    // User input is a Postback press
                context.userData.analytics.in.vision_file_link = context.message.text
                intent = {
                    intent: "RESULTS.DIALOG",
                    score: 1,
                    entities: [{
                        entity: text,
                        entityType: 'SOCIAL.URL',
                        data: {
                            NODE: Constants.NODE.IMAGEUPLOAD,
                            resetState: true,
                            state: {imageUrl: text, isSocialUrl: true}
                        }
                    }]
                }
            } else {
                intent = {
                    intent: 'FEATURE.DISABLE',
                    score: 1,
                    entities: [{
                        data: {feature: "Visual Search"}
                    }]
                }

            }
            callback(null, intent)

        } else {
            console.log('SOCIAL-url-recognizer <-')
            callback(null, {intent: null, score: 0})
        }

    }
}