/**
 * Created by abzooba on 11/2/17.
 */

var fs = require('fs');
const HashMap = require('hashmap');   // Hashmap data structures
//var winston = require('./LoggerUtil');

var emojiRegex = require('emoji-regex');
var retext = require('retext');
var emoji = require('retext-emoji');
var contents = fs.readFileSync('app/recognizers/emoji/data/emoji-sentiment.csv').toString();
var emoticonMap = new HashMap();
contents.split('\n').forEach(function (line) {
    emoticonMap.set(line.split(',')[0],parseFloat(line.split(',')[1]));
});

function getEmojiJSON(text) {
    try{

        var altText = '';
        var delim = '';
        var sentiScore = 0 , sentiCnt = 0;
        // To Convert emojis to :emoji:
        Array.from(text).forEach(function (token) {
            try{
                if ((token.match(emojiRegex()) != null)) {
                    altText = altText + delim + retext().use(emoji, {convert: 'decode'}).process(token).contents;
                    delim = ' ';
                }else{
                    altText = altText + token;
                }
            }catch(err){
                winston.log('error',"Some error happened in getEmojiJSON "+err)
            }
        });
        // To convert any :) to :smiley:
        altText =  retext().use(emoji, {convert: 'decode'}).process(altText).contents;
        // console.log(altText)
        // To strip all :: and calculate emoticon sentiScore
        text = '';
        delim = '';
        Array.from(altText.split(' ')).forEach(function (token) {
            try{
                if ( token.charAt(0)==':' && token.charAt(token.length-1)==':' ) {
                    text = text + delim + token.slice(1, token.length - 1).replace("_"," ");
                    var emo = retext().use(emoji, {convert: 'encode'}).process(token).contents;
                    if(emoticonMap.has(emo)){
                        sentiScore = sentiScore + emoticonMap.get(emo);
                        sentiCnt = sentiCnt + 1;
                    }
                }else {
                    text = text + delim +token;
                }
                delim = ' ';
            }catch(err){}
        });
        // console.log(text);
        // console.log(sentiScore);
        // console.log(sentiCnt);
        var finalScore = null;
        if(sentiCnt > 0){
            finalScore = sentiScore/sentiCnt;
        }
        return { text:text , score : finalScore };
    }catch(err){
        return null;
    }
}

module.exports = {
    getEmojiJSON : getEmojiJSON
}