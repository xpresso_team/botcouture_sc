var request    = require( 'superagent'             )
var Constants  = require( '../../constants'           )
var ApiService = require( '../../services/ApiService' )
var EmojiUtil = require( './EmojiUtil' )

module.exports = {
  
  recognize : ( context, callback ) => {
    

    let text         = context.message.text
    console.log(text)
    text             = text.replace(/<[^>]+>/g, '')      // Skype wraps emoticons in tags
    let emoticonJSON = EmojiUtil.getEmojiJSON( text )

    // Found an Emoji
    if ( true || ( emoticonJSON && emoticonJSON.text && emoticonJSON.score != null ) ) {  
      //console.log( 'EMOJI--> ', emoticonJSON )
      // Make a wit.ai call
      ApiService.getEmotion( emoticonJSON.text,context )
      .then( emotionJSON => {
  
        let intent = {
          intent : "EMOJI.DIALOG",
          score : 1,
          entities : [ { entity : text, entityType: 'EMOJI', data: { emoticonJSON : emoticonJSON, emotionJSON : emotionJSON } } ]
        }
        console.log( 'Emoji-recognizer ->' )
        
        // ANALYTICS
        context.userData.analytics.in.message_type_flag = 'IT'      // User input is a Emoji

        callback( null, intent )
        
      })
      .catch( err => {
        console.log( err )
        console.log( 'Emoji Recognizer <-' )
        callback( err, null )
      })
    
    } else {
      console.log( 'Emoji Recognizer <-' )
      callback.call( null, null, { intent : null, score : 0 } )
    }

  }
}