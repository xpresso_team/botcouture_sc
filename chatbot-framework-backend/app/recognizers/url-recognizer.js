var request = require('superagent')
var Constants = require('../constants')
const nconf = require('nconf')
var url = require('url')

// TODO: Rename to Image Upload Recognizer
module.exports = {

    recognize: function (context, callback) {

        // TODO: try validUrl package http://stackoverflow.com/questions/30931079/validating-a-url-in-node-js
        let images = []

        // Filter only images
        if (context.message.attachments && context.message.attachments.length > 0) {

            // Check if its the facebook Thumbs up emoji
            if (context.message.attachments[0].contentUrl.match('t39.1997-6')) {
                let intent = {
                    intent: "EMOJI.DIALOG",
                    score: 1,
                    entities: [{
                        entity: "It is awesome",
                        entityType: 'EMOJI',
                        data: {
                            emoticonJSON: {score: '1'},
                            emotionJSON: {
                                entities: {
                                    intent: [{value: "appreciation"}]
                                }
                            }
                        }
                    }]
                }
                console.log('FB Thumbs Up emoji ( url-recognizer ) ->')
                callback(null, intent)
            } else {

                images = context.message.attachments.filter(attachment => {
                    return attachment.contentType.search(/^image/i) > -1 && attachment.contentUrl
                })

                if (images.length > 0) {

                    let intent

                    if (nconf.get("Visual Search")) {
                        //context.userData['IMAGEUPLOAD'].SS_Webview = 0
                        //context.userData['IMAGEUPLOAD'].SS_Carousel= 1
                        // Found images
                        images = images.map(attachment => {
                            attachment.url = url.parse(attachment.contentUrl)
                            return attachment
                        })

                        intent = {
                            intent: 'RESULTS.DIALOG',
                            score: 1,
                            entities: [{
                                entity: 'image',
                                entityType: 'UPLOAD.IMAGE',
                                data: {
                                    NODE: Constants.NODE.IMAGEUPLOAD,
                                    resetState: true,
                                    state: {imageUrl: images[0].contentUrl}
                                }
                            }]
                        }

                        console.log('url-recognizer ->')
                        context.userData.analytics.in.message_type_flag = 'II'    // User input is a Postback press
                        context.userData.analytics.in.vision_file_link = intent.entities[0].data.state.imageUrl
                    } else {
                        intent = {
                            intent: 'FEATURE.DISABLE',
                            score: 1,
                            entities: [{
                                data: {feature: "Visual Search"}
                            }]
                        }
                    }

                    callback(null, intent)

                } else {
                    // Not an image
                    console.log('url-recognizer <-')
                    callback(null, {intent: null, score: 0})
                }

            }

        } else {
            // Not an image
            console.log('url-recognizer <-')
            callback(null, {intent: null, score: 0})
        }

    }
}
