// This module should not require any other module.
// This is to prevent circular dependency.

module.exports = {
    // Lables that show on prompts (used in respective dialogues)
    Labels: {
        Introduction: 'Start Over',
        WelcomeBack: 'WelcomeBack',
        ShopMode: 'ShopMode',
        Tour: 'Take A Tour',
        Inspiration: 'Find Inspiration',
        SearchItem: 'Search For An Item',
        ExploreCollections: 'Explore Collections',
        ExploreDesignTrends: 'Explore Design Trends',
        ExploreGiftIdeas: 'Explore Gift Ideas',
        ViewCollecion: 'View Collection',
        ViewSimilarItems: 'View Similar Items',
        TypeQuery: 'Type A Query',
        UploadImage: 'Upload An Image',
        UploadImageSuccess: 'Upload Image Success',   // This dialog is only triggered if image upload is success
        UploadImageCategory: 'Upload Image Category',
        GuideMe: 'Guide Me',
        GuideMePickNext: 'Guide Me Pick Next',
        Help: 'Help',
        Help_Ask: 'Help_Ask',
        FAQ_CAT: 'FAQ_CAT',
        FAQ: 'FAQ',
        NO_FAQ: 'NO_FAQ',
        PLAY: 'PLAY',
        ViewItOn: 'ViewItOn',
        Feedback_New: 'Feedback_New',
        Feedback: 'Feedback',
        DemoCarousel: 'DEMO-CAROUSEL',
        Goodbye: 'goodbye',
        getStartedPayload : 'GET_STARTED',
        persistentMenuPayloadMsg: ["SPECIALIST", "Feedback", "HELP", "Wishlist", "Profile", "Discover Features","Start Over"],
        "Persistent Menu Options": ["Chat with Specialist", "Give Feedback", "Help", "View Wishlist", "View Profile", "Discover Features","Start Over"],
        INTRO_TO_BOT: 'INTRO_TO_BOT',
        INTRO_Ready: 'INTRO_Ready',
        Select_Shop: 'Select Shop',
        Apply_Retailer : 'Apply Retailer',   
        //Star_Labels: ["🌟"," 🌟🌟","🌟🌟🌟","🌟🌟🌟🌟","🌟🌟🌟🌟🌟"]
        //Star_Labels: ["1","2","“0xD83D 0xDC31","U+1F431","\ud83d\udc31"]
        //Star_Labels: ["1","2","3","4","5","6","7","8","9","10"]
        Star_Labels: ["1","2","3","4","5"]
        //Star_Labels: ["1*","2*","3*","4*","5*","6*","7*","8*","9*","10*"]
    },

    NODE: {
        GUIDEME: 'GUIDEME',        // this Text will be used as object key, don't use special characters
        NLP: 'NLP',
        IMAGEUPLOAD: 'IMAGEUPLOAD',
        IMAGEUPLOADVIEWMORE: 'IMAGEUPLOADVIEWMORE',
        IMAGEUPLOADCATEGORY: 'IMAGEUPLOADCATEGORY',
        COLLECTIONS: 'COLLECTIONS'
    },

    SYNONYMS: {
        SYN_DEFAULTS: {text: 'Synonym', isCaseSensitive: false, isPartialMatch: false},
        'Show More': ['Show More', {text: 'more'}],
        'Add Filters': ['add', {text: '(.*)add(.*)filter(.*)', isPartialMatch: true}, {
            text: '(.*)add(.*)filters(.*)',
            isPartialMatch: true
        }],
        'Edit Filters': ['edit', {
            text: '(.*)edit(.*)filter(.*)',
            isPartialMatch: true
        }, {text: '(.*)edit(.*)filters(.*)', isPartialMatch: true}],
        'Brand': ['brand'],
        'Price': ['price'],
        'Color': ['color', 'colour'],
        'Size': ['size'],
        'Save it!': [{text: '^save(.*)', isPartialMatch: true}],
        'No Thanks': ['no thanks', 'no'],
        'Cancel': ['cancel'],
        'Clear All': ['clear all', 'clear'],
        'Continue': ['continue'],
        'Men': ["Men's","Men","Man","Son","him","his","prince","boy"],
        'Women':["Women's","Women","Woman","Daughter","lady","Mistress","Princess","her","girl"]
    },

    user_address: {

        "id": "mid.$cAAI5p5HuWIpiZfLSuVcNK2Jtw1o9",
        "channelId": "facebook",
        "user": {
            "id": "1313298325359965",
            "name": "Jesse Samuel"
        },
        "conversation": {
            "isGroup": false,
            "id": "1313298325359965-672153249630080"
        },
        "bot": {
            "id": "672153249630080",
            "name": "AashishDevBot"
        },
        "serviceUrl": "https://facebook.botframework.com",
        "useAuth": true

    }

}