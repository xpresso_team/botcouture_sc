const NodeCache = require( 'node-cache' )

// TTL in seconds
const myCache   = new NodeCache( { stdTTL: 15 * 60, checkperiod: 10 * 60 } )

set = ( key, val ) => myCache.set( key, val )
get = ( key ) => myCache.get( key )
del = ( key ) => myCache.del( key )

module.exports = {
  set : set,
  get : get,
  del : del,
}