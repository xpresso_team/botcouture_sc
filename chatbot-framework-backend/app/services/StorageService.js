var nconf = require('nconf')
var redis = require('redis')
var redisClient = redis.createClient(nconf.get('redis_port'),'127.0.0.1',{db: 4});
var _ = require('lodash');


var feedbackClient = redis.createClient(nconf.get('redis_port'),'127.0.0.1',{db: 10})

getFeedback = (key, callback) => {
    console.log('*****storageKey in Storage Service getData***', nconf.get('storageKey'))
    redisClient.get(key, (err, reply) => {
        //console.log('\nKey in getData', JSON.stringify(data))
        // console.log('\nKey in getData', data)
        // console.log('\nReply in getData', reply)
        if (err || _.isNil(reply) || _.isEmpty(reply)) {
            callback(err, null)
        } else {
            callback(null, JSON.parse(reply))
        }
    })
}

saveFeedback = (key, value, callback) => {
    // console.log('\nKey in saveData', JSON.stringify(data))
    // console.log('\nvalue in saveData', JSON.stringify(value))
    console.log('*****storageKey in Storage Service*** saveData', nconf.get('storageKey'))
    //redisClient.set(data[nconf.get('storageKey')], JSON.stringify(value), (err, reply) => {
    // console.log("Value to be saved is ",value)

    redisClient.set(key, JSON.stringify(value), (err, reply) => {
        if (err) {
            callback(err)
        } else if (reply == 'OK') {
            callback(null)
        }
    })
}

getData = (data, callback) => {
    console.log('*****storageKey in Storage Service getData***', nconf.get('storageKey'))
    redisClient.get(data[nconf.get('storageKey')], (err, reply) => {

        //console.log('\nKey in getData', JSON.stringify(data))
        // console.log('\nKey in getData', data)
        // console.log('\nReply in getData', reply)
        if (err || _.isNil(reply) || _.isEmpty(reply)) {
            callback(err, null)
        } else {
            callback(null, JSON.parse(reply))
        }
    })
}

saveData = (data, value, callback) => {
    // console.log('\nKey in saveData', JSON.stringify(data))
    // console.log('\nvalue in saveData', JSON.stringify(value))
    console.log('*****storageKey in Storage Service*** saveData', nconf.get('storageKey'))

    redisClient.set(data[nconf.get('storageKey')], JSON.stringify(value), (err, reply) => {
        if (err) {
            callback(err)
        } else if (reply == 'OK') {
            callback(null)
        }
    })
}

module.exports = {
    getData: getData,
    saveData: saveData,
    getFeedback: getFeedback,
    saveFeedback: saveFeedback
}