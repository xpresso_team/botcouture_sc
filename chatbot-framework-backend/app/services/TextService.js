var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../constants')
var request = require('superagent')

module.exports = {

    /**
     * Returns message text read from google sheet
     */
    text: (key, data) => {

        // Error getting google sheet data
        if ( ! global.sheetData ) return [ key ]

        // Key does not exists
        if ( ! global.sheetData[key] ) {
            return [ key ]
            console.log( 'ERROR TEXTSEVICE : ', key )
        }

        // Make a copy, otherwise replaced values are cached
        let msgArr = global.sheetData[key].slice()

        // Replace placeholders
        _.forOwn(data, (value, name) => {
            _.each(msgArr, (msg, id, arr) => {
                arr[id] = msg.split('{{' + name + '}}').join(value)
                //arr[ id ] = msg.replace( '{{' + name + '}}', value )
            })
        })

        // Key was not present in the google sheet
        if (msgArr.length < 1) return [ key ]

        return msgArr

    },

    /**
     * Returns questions from personalization sheet
     */
    getPersonalizationQuestion: key => {

        // Error getting google sheet data
        if (!global.personalizationSheetData) return [key]

        return global.personalizationSheetData[key]

    },

    /**
     * Returns all the personalization questions
     */
    getAllPersonalizationQuestion: () => global.personalizationSheetData,

    FAQS: (key, data) => {

        if (!global.FAQS) return ['Server Error: Message Text Not Found']

        let msgArr = global.FAQS[key].slice()

        _.forOwn(data, (value, name) => {
            _.each(msgArr, (msg, id, arr) => {
                arr[id] = msg.replace('{{' + name + '}}', value)
            })
        })

        if (msgArr.length < 1) return ['Server Error: Message Text Not Found']

        return msgArr

    },
    FAQS_CAT: (key, data) => {

        if (!global.FAQS_CAT) return ['Server Error: Message Text Not Found']

        let msgArr = global.FAQS_CAT[key].slice()

        _.forOwn(data, (value, name) => {
            _.each(msgArr, (msg, id, arr) => {
                arr[id] = msg.replace('{{' + name + '}}', value)
            })
        })

        if (msgArr.length < 1) return ['Server Error: Message Text Not Found']

        return msgArr

    },

    FAQ_VID_TEXT: (key, data) => {

        if (!global.FAQ_VID_TEXT) return ['Server Error: Message Text Not Found']

        let msgArr = global.FAQ_VID_TEXT[key].slice()

        _.forOwn(data, (value, name) => {
            _.each(msgArr, (msg, id, arr) => {
                arr[id] = msg.replace('{{' + name + '}}', value)
            })
        })

        if (msgArr.length < 1) return ['Server Error: Message Text Not Found']

        return msgArr

    }

}
