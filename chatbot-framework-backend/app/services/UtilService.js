var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../constants')
var request = require('superagent')
var nconf = require('nconf')
var uniRest = require('unirest')
var toTitleCase = require('to-title-case')
var winston = require('./loggly/LoggerUtil')
var nconf = require('nconf')
var UserService = require('./UserService')
var TextService = require('./TextService')
const uuidV4 = require('uuid/v4')
// UtilService should not depend on other services,
// Other services should depend on UtilService


//let IMAGE_RESIZE_API_URL = 'vision.synaptica.abzooba.com'
let IMAGE_RESIZE_API_HOST = 'http://' + nconf.get('IMAGE_RESIZE_API_URL')
let Currency = nconf.get('Currency')

/**
 * Return a subset of an array
 */
paginate = (itemList, pageNo, pageSize) => {

    let startIndex = null
    let endIndex = null

    // Warp around  logic
    let numPages = Math.ceil(itemList.length / pageSize)
    // For -ve pages nums
    if (pageNo < 0) {
        pageNo = numPages - ( -pageNo % numPages )
        if (pageNo === numPages) pageNo = 0
    }
    // For +ve page nums
    pageNo = pageNo % numPages

    //startIndex = ( pageNo * pageSize ) % itemList.length
    startIndex = pageNo * pageSize
    endIndex = startIndex + pageSize

    let itemListWindow = itemList.slice(startIndex, endIndex)

    return itemListWindow

}

/**
 * Returns a subset of an array, to be used in a paged list
 * Adds Prev/Next to the list also
 */
pageList = (itemList, pageNo, pageSize, showPreviousText, showNextText) => {

    // Reduce page size by 2 elements for previous and next buttons
    // This is done even if these buttons are not actually shown
    pageSize = pageSize - 2

    let itemListWindow = paginate(itemList, pageNo, pageSize)

    if (showPreviousText) {
        itemListWindow.unshift(showPreviousText)
    }
    if (showNextText) {
        itemListWindow.push(showNextText)
    }
    return itemListWindow
}


/**
 * Creates cards from results
 */
createCards = (session, resultResults,suggested) => {

    if(!suggested){
        suggested = 0
    }

    if (session.message.source === "facebook") {

        let temp_node = session.userData.currentNode
        let state = session.userData[temp_node]
        let query_type = ""
        var NotRightUrl = nconf.get('WebView')["Url"]

        switch (temp_node) {
            case Constants.NODE.GUIDEME             :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]
                query_type = "Guide Me"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl += state.xc_category && state.xc_category.length ? '&entity=' + state.xc_category.join(',') : ''
                NotRightUrl += state.gender && state.gender.length ? '&gender=' + state.gender.join(',') : ''
                NotRightUrl += state.price && state.price.length ? '&price=' + state.price.join(',') : ''
                NotRightUrl += state.subcategory && state.subcategory.length ? '&subcategory=' + state.subcategory.join(',') : ''
                NotRightUrl += state.features && state.features.length ? '&feature=' + state.features.join(',') : ''
                NotRightUrl += state.brand && state.brand.length ? '&brand=' + state.brand.join(',') : ''
                NotRightUrl += state.details && state.details.length ? '&details=' + state.details.join(',') : ''
                NotRightUrl += state.colorsavailable && state.colorsavailable.length ? '&colorsavailable=' + state.colorsavailable.join(',') : ''
                NotRightUrl += state.size && state.size.length ? '&size=' + state.size.join(',') : ''

                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
                break;
            case Constants.NODE.NLP                 :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]

                query_type = "NLP"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl += state.entity? '&entity=' + state.entity : ''
                NotRightUrl += state.gender && state.gender.length ? '&gender=' + state.gender.join(',') : ''
                NotRightUrl += state.price && state.price.length ? '&price=' + state.price.join(',') : ''
                NotRightUrl += state.subcategory && state.subcategory.length ? '&subcategory=' + state.subcategory.join(',') : ''
                NotRightUrl += state.features && state.features.length ? '&feature=' + state.features.join(',') : ''
                NotRightUrl += state.brand && state.brand.length ? '&brand=' + state.brand.join(',') : ''
                NotRightUrl += state.details && state.details.length ? '&details=' + state.details.join(',') : ''
                NotRightUrl += state.colorsavailable && state.colorsavailable.length ? '&colorsavailable=' + state.colorsavailable.join(',') : ''
                NotRightUrl += state.size && state.size.length ? '&size=' + state.size.join(',') : ''

                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
                break;
            case Constants.NODE.IMAGEUPLOAD         :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]
                query_type = "VISION"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl = NotRightUrl +"&img_url="+encodeURIComponent(session.userData[temp_node].imageUrl)+"&upload_id="+session.userData[temp_node].upload_id
                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
                break;
            case Constants.NODE.IMAGEUPLOADVIEWMORE :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]
                query_type = "VISION"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl = NotRightUrl +"&img_url="+encodeURIComponent(session.userData[temp_node].imageUrl)+"&upload_id="+session.userData[temp_node].upload_id
                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='

                break;
            case Constants.NODE.IMAGEUPLOADCATEGORY :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]
                query_type = "VISION"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl = NotRightUrl +"&img_url="+encodeURIComponent(session.userData[temp_node].imageUrl)+"&upload_id="+session.userData[temp_node].upload_id
                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='

                break;
            case Constants.NODE.COLLECTIONS         :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]

                query_type = "COLLECTIONS"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl += state.xc_category && state.xc_category.length ? '&entity=' + state.xc_category.join(',') : ''
                NotRightUrl += state.collectionType ? '&collectionType=' + state.collectionType : ''
                NotRightUrl += state.collectionName  ? '&collectionName=' + state.collectionName : ''
                NotRightUrl += state.gender && state.gender.length ? '&gender=' + state.gender.join(',') : ''
                NotRightUrl += state.price && state.price.length ? '&price=' + state.price.join(',') : ''
                NotRightUrl += state.subcategory && state.subcategory.length ? '&subcategory=' + state.subcategory.join(',') : ''
                NotRightUrl += state.features && state.features.length ? '&feature=' + state.features.join(',') : ''
                NotRightUrl += state.brand && state.brand.length ? '&brand=' + state.brand.join(',') : ''
                NotRightUrl += state.details && state.details.length ? '&details=' + state.details.join(',') : ''
                NotRightUrl += state.colorsavailable && state.colorsavailable.length ? '&colorsavailable=' + state.colorsavailable.join(',') : ''
                NotRightUrl += state.size && state.size.length ? '&size=' + state.size.join(',') : ''

                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='

                break;
            default :
                break;
        }


        console.log("Sending for ", session.message.source)
        var elements=[];
        //${url}/viewproduct?sku=${sku}&xc_sku=${xc_sku}&psid=${session.message.address.user.id}&botid=${session.message.address.bot.id}`
        var detail_url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Details"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
        var expanded_url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Expanded_menu"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
        var shareWithFriend_url = nconf.get('WebView')["Url"] + nconf.get('WebView')["ShareWithFriend"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='

   // /expand?psid=${session.message.address.user.id}&botid=${session.message.address.bot.id}&xc_sku=${xc_sku}` + '&x=' + uuidV4(),
        for (let i = 0; i < resultResults.length && i < 10; i++) {
            let item = resultResults[i]
            let element = {
                title: toTitleCase(item.brand) + ' - ' + toTitleCase(item.productname),
                subtitle: 'Price: ' + Currency + ' ' + parseInt(item.price),
                image_url: IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + item.image + '&aspect='+nconf.get("image_aspect_ratio")[nconf.get("payload")["image_aspect_ratio"]],
                default_action: {
                    "type": "web_url",
                    "url": detail_url+item.xc_sku,
                    "messenger_extensions": true,
                    "webview_height_ratio": nconf.get('WebView_size')["Details"]
                },
                buttons: []

            };

            element.buttons.push({
                type: 'postback',
                title: TextService.text('Product carousel button text')[0],
                payload: 'VIEWSIMILAR::'+ item.xc_sku+"::"+suggested,
            });

            /*element.buttons.push(
                {
                    "type": "web_url",
                    "title": TextService.text('Product carousel button text')[1],
                    "url": detail_url+item.xc_sku ,
                    "messenger_extensions": true,
                    "webview_height_ratio": nconf.get('WebView_size')["Details"]
                }
            )*/
            if(nconf.get("Share_Friend_Enabled")) {
                element.buttons.push(
                    {
                        "type": "web_url",
                        "title": TextService.text('shareWithFriend Button')[0],
                        "url": shareWithFriend_url + item.xc_sku,
                        "messenger_extensions": true,
                        "webview_height_ratio": nconf.get('WebView_size')["shareWithFriend_url"]
                    }
                )
            }

            element.buttons.push(
                {
                    "type": "web_url",
                    "title": TextService.text('Product carousel button text')[2],
                    "url": NotRightUrl+item.xc_sku ,
                    "messenger_extensions": true,
                    "webview_height_ratio": nconf.get('WebView_size')["Expanded_menu"]
                }
            )
            elements.push(element)
        }

        return Promise.resolve(elements)
    } else {


        let cards = []
        let Wishlist_enabled = nconf.get('Wishlist')
        //console.log(Currency)
        if (Wishlist_enabled) {
// Get users wishlist
            return UserService.getUsersWishlist(session.message.address)
                .then(wishlist => {

                    if (!wishlist) wishlist = []
                    var wishlistSkus = _.map(wishlist, item => item.xc_sku)

                    for (let i = 0; i < resultResults.length && i < 10; i++) {
                        let item = resultResults[i]
                        let card = new builder.HeroCard(session)
                            .title(toTitleCase(item.brand) + ' - ' + toTitleCase(item.productname))
                            .subtitle('Price: ' + Currency + ' ' + parseInt(item.price))
                            //.text('Description text about exploring collections.')
                            .images([
                                builder.CardImage.create(session, IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + item.image + '&aspect=1.91x1')
                            ])

                        if (_.includes(wishlistSkus, item.xc_sku)) {
                            card.buttons([
                                //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                                builder.CardAction.postBack(session, 'VIEWITON::' + item.product_url + '::WARN::' + _.startCase(item.client_name) + "::" + item.xc_sku, TextService.text('Product_Details')[0]),
                                builder.CardAction.postBack(session, 'VIEWSIMILAR::' + item.xc_sku+"::"+suggested, 'View Similar'),
                                builder.CardAction.postBack(session, 'WISHLIST::' + item.xc_sku + '::' + item.productname + '::' + 'WISHLIST.REMOVE.ITEM', 'Remove From Wishlist')
                            ])

                        } else {
                            card.buttons([
                                //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                                builder.CardAction.postBack(session, 'VIEWITON::' + item.product_url + '::WARN::' + _.startCase(item.client_name) + "::" + item.xc_sku, TextService.text('Product_Details')[0]),
                                builder.CardAction.postBack(session, 'VIEWSIMILAR::' + item.xc_sku+"::"+suggested, 'View Similar'),
                                builder.CardAction.postBack(session, 'WISHLIST::' + item.xc_sku + '::' + item.productname + '::' + 'WISHLIST.SAVE.ITEM', 'Save To Wishlist')
                            ])

                        }
                        cards.push(card)
                    }
                    return cards
                })
        } else {

            for (let i = 0; i < resultResults.length && i < 10; i++) {
                let item = resultResults[i]
                let card = new builder.HeroCard(session)
                    .title(toTitleCase(item.brand) + ' - ' + toTitleCase(item.productname))
                    .subtitle('Price: ' + Currency + ' ' + parseInt(item.price))
                    //.text('Description text about exploring collections.')
                    .images([
                        builder.CardImage.create(session, IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + item.image + '&aspect=1.91x1')
                    ]).buttons([
                        //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                        builder.CardAction.postBack(session, 'VIEWITON::' + item.product_url + '::WARN::' + _.startCase(item.client_name) + "::" + item.xc_sku, TextService.text('Product_Details')[0]),
                        builder.CardAction.postBack(session, 'VIEWSIMILAR::' + item.xc_sku+"::"+suggested, 'View Similar'),
                    ])
                cards.push(card)

            }
            return Promise.resolve(cards)

        }
    }

}


/**
 * Creates cards from results
 */
createCards2 = (session, cards) => {

    cards = _.isPlainObject(cards) ? [cards] : cards

    let richCards = []

    _.each(cards, card => {

        let richCard = new builder.HeroCard(session)

        if (card.title) richCard = richCard.title(card.title)

        if (card.subtitle) richCard = richCard.subtitle(card.subtitle)

        if (card.text) richCard = richCard.text(card.text)

        // Images
        if (card.images && card.images.length > 0) {
            let images = []
            _.each(card.images, image => {
                images.push(builder.CardImage.create(session, image.url))
            })
            richCard = richCard.images(images)
        }

        // Buttons
        if (card.buttons && card.buttons.length > 0) {
            let buttons = []
            _.each(card.buttons, button => {
                buttons.push(builder.CardAction[button.type](session, button.value, button.title))
            })
            richCard = richCard.buttons(buttons)
        }

        richCards.push(richCard)

    })

    return richCards
},


    /**
     * Process result from soical urls and image upload (Vision) urls
     * TODO: Change the function name
     */
    createAndSendImageResult2 = (session, result, imageUploadState, upload_id) => {

        if (session.message.source === "facebook") {
            let temp_node = session.userData.currentNode
            let state = session.userData[temp_node]
            let query_type = ""
            var NotRightUrl = nconf.get('WebView')["Url"]

            switch (temp_node) {
                case Constants.NODE.GUIDEME             :
                    NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]

                    query_type = "Guide Me"
                    NotRightUrl += query_type ? '?query_type=' + query_type : ''
                    NotRightUrl += state.xc_category && state.xc_category.length ? '&entity=' + state.xc_category.join(',') : ''
                    NotRightUrl += state.gender && state.gender.length ? '&gender=' + state.gender.join(',') : ''
                    NotRightUrl += state.price && state.price.length ? '&price=' + state.price.join(',') : ''
                    NotRightUrl += state.subcategory && state.subcategory.length ? '&subcategory=' + state.subcategory.join(',') : ''
                    NotRightUrl += state.features && state.features.length ? '&feature=' + state.features.join(',') : ''
                    NotRightUrl += state.brand && state.brand.length ? '&brand=' + state.brand.join(',') : ''
                    NotRightUrl += state.details && state.details.length ? '&details=' + state.details.join(',') : ''
                    NotRightUrl += state.colorsavailable && state.colorsavailable.length ? '&colorsavailable=' + state.colorsavailable.join(',') : ''
                    NotRightUrl += state.size && state.size.length ? '&size=' + state.size.join(',') : ''

                    NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
                    break;
                case Constants.NODE.NLP                 :
                    NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]

                    query_type = "NLP"
                    NotRightUrl += query_type ? '?query_type=' + query_type : ''
                    NotRightUrl += state.entity? '&entity=' + state.entity : ''
                    NotRightUrl += state.gender && state.gender.length ? '&gender=' + state.gender.join(',') : ''
                    NotRightUrl += state.price && state.price.length ? '&price=' + state.price.join(',') : ''
                    NotRightUrl += state.subcategory && state.subcategory.length ? '&subcategory=' + state.subcategory.join(',') : ''
                    NotRightUrl += state.features && state.features.length ? '&feature=' + state.features.join(',') : ''
                    NotRightUrl += state.brand && state.brand.length ? '&brand=' + state.brand.join(',') : ''
                    NotRightUrl += state.details && state.details.length ? '&details=' + state.details.join(',') : ''
                    NotRightUrl += state.colorsavailable && state.colorsavailable.length ? '&colorsavailable=' + state.colorsavailable.join(',') : ''
                    NotRightUrl += state.size && state.size.length ? '&size=' + state.size.join(',') : ''

                    NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
                    break;
                case Constants.NODE.IMAGEUPLOAD         :
                    NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]
                    query_type = "VISION"
                    NotRightUrl += query_type ? '?query_type=' + query_type : ''
                    NotRightUrl = NotRightUrl +"&img_url="+encodeURIComponent(session.userData[temp_node].imageUrl)+"&upload_id="+session.userData[temp_node].upload_id
                    NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
                    break;
                case Constants.NODE.IMAGEUPLOADVIEWMORE :
                    NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]
                    query_type = "VISION"
                    NotRightUrl += query_type ? '?query_type=' + query_type : ''
                    NotRightUrl = NotRightUrl +"&img_url="+encodeURIComponent(session.userData[temp_node].imageUrl)+"&upload_id="+session.userData[temp_node].upload_id
                    NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='

                    break;
                case Constants.NODE.IMAGEUPLOADCATEGORY :
                    NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]
                    query_type = "VISION"
                    NotRightUrl += query_type ? '?query_type=' + query_type : ''
                    NotRightUrl = NotRightUrl +"&img_url="+encodeURIComponent(session.userData[temp_node].imageUrl)+"&upload_id="+session.userData[temp_node].upload_id
                    NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='

                    break;
                case Constants.NODE.COLLECTIONS         :
                    NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]

                    query_type = "COLLECTIONS"
                    NotRightUrl += query_type ? '?query_type=' + query_type : ''
                    NotRightUrl += state.xc_category && state.xc_category.length ? '&entity=' + state.xc_category.join(',') : ''
                    NotRightUrl += state.collectionType ? '&collectionType=' + state.collectionType : ''
                    NotRightUrl += state.collectionName  ? '&collectionName=' + state.collectionName : ''
                    NotRightUrl += state.gender && state.gender.length ? '&gender=' + state.gender.join(',') : ''
                    NotRightUrl += state.price && state.price.length ? '&price=' + state.price.join(',') : ''
                    NotRightUrl += state.subcategory && state.subcategory.length ? '&subcategory=' + state.subcategory.join(',') : ''
                    NotRightUrl += state.features && state.features.length ? '&feature=' + state.features.join(',') : ''
                    NotRightUrl += state.brand && state.brand.length ? '&brand=' + state.brand.join(',') : ''
                    NotRightUrl += state.details && state.details.length ? '&details=' + state.details.join(',') : ''
                    NotRightUrl += state.colorsavailable && state.colorsavailable.length ? '&colorsavailable=' + state.colorsavailable.join(',') : ''
                    NotRightUrl += state.size && state.size.length ? '&size=' + state.size.join(',') : ''

                    NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='

                    break;
                default :
                    break;
            }

            console.log("Sending for ", session.message.source)
            let cards = []
            var detail_url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Details"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
            var expanded_url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Expanded_menu"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
            var shareWithFriend_url = nconf.get('WebView')["Url"] + nconf.get('WebView')["ShareWithFriend"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
            _.forOwn(result, (value, key) => {
                if (key !== 'gender_flag' || key !== 'tagged_img') {
                    if (value.matches && value.matches.length > 0) {
                        let imageUrl = IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + value.matches[0].image + '&aspect='+nconf.get("image_aspect_ratio")[nconf.get("payload")["image_aspect_ratio"]];
                        value.matches[0].client_name = value.matches[0].client_name ? value.matches[0].client_name : 'Website';
                        let card = {
                            title: toTitleCase(key),
                            subtitle: 'Price: '+ Currency + value.matches[0].price,
                            image_url: imageUrl,
                            default_action: {
                                type: "web_url",
                                url: detail_url+value.matches[0].xc_sku,
                                messenger_extensions: true,
                                webview_height_ratio: nconf.get('WebView_size')["Details"]
                            },
                            buttons: []
                        }
                            /*card.buttons.push(
                            {
                                "type": "web_url",
                                "title": TextService.text('Product carousel button text')[1],
                                "url": detail_url+value.matches[0].xc_sku ,
                                "messenger_extensions": true,
                                "webview_height_ratio": nconf.get('WebView_size')["Details"]
                            }
                        )*/
                        if (imageUploadState.gender && imageUploadState.gender.length > 0) {
                            card.buttons.push({
                                type: 'postBack',
                                title: 'View More',
                                payload: 'VIEWMORE::' + imageUploadState.imageUrl + '::' + imageUploadState.gender[0] + '::' + key + '::' + imageUploadState.isSocialUrl + '::' + upload_id
                            })
                        } else {
                            card.buttons.push({
                                type: 'postBack',
                                title: 'View More',
                                payload: 'VIEWMORE::' + imageUploadState.imageUrl + '::' + key + '::' + imageUploadState.isSocialUrl + '::' + upload_id
                            })
                        }
                        if(nconf.get("Share_Friend_Enabled")) {
                            card.buttons.push(
                                {
                                    "type": "web_url",
                                    "title": TextService.text('shareWithFriend Button')[0],
                                    "url": shareWithFriend_url + value.matches[0].xc_sku,
                                    "messenger_extensions": true,
                                    "webview_height_ratio": nconf.get('WebView_size')["ShareWithFriend"]
                                }
                            )
                        }

                        card.buttons.push(
                            {
                                "type": "web_url",
                                "title": TextService.text('Product carousel button text')[2],
                                "url": NotRightUrl+value.matches[0].xc_sku ,
                                "messenger_extensions": true,
                                "webview_height_ratio": nconf.get('WebView_size')["Expanded_menu"]
                            }
                        )

                        cards.push(card)
                    }
                }
            })

            if (result.tagged_img) {
                let predictedCard = {
                    title: 'Predicted Items',
                    image_url: IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + result.tagged_img + '&aspect='+nconf.get("image_aspect_ratio")[nconf.get("payload")["image_aspect_ratio"]],
                }
                cards.unshift(predictedCard)
            }

            return Promise.resolve(cards)
        } else {


            let Wishlist_enabled = nconf.get('Wishlist')
            let cards = []
            if (Wishlist_enabled) {
                // Get users wishlist
                return UserService.getUsersWishlist(session.message.address)
                    .then(wishlist => {

                        if (!wishlist) wishlist = []
                        var wishlistSkus = _.map(wishlist, item => item.xc_sku)
                        _.forOwn(result, (value, key) => {
                            if (key !== 'gender_flag' || key !== 'tagged_img') {
                                if (value.matches && value.matches.length > 0) {
                                    let imageUrl = IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + value.matches[0].image + '&aspect=1.91x1'
                                    value.matches[0].client_name = value.matches[0].client_name ? value.matches[0].client_name : 'Website'
                                    let card = {
                                        title: toTitleCase(key),
                                        // subtitle: 'Price: '+ Currency + value.matches[0].price,
                                        images: [{url: imageUrl}],
                                        buttons: []
                                    }
                                    card.buttons.push({
                                        type: 'postBack',
                                        value: 'VIEWITON::' + value.matches[0].product_url + '::WARN::' + _.startCase(value.matches[0].client_name) + "::" + value.matches[0].xc_sku,
                                        title: TextService.text('Product_Details')[0]
                                    })
                                    if (imageUploadState.gender && imageUploadState.gender.length > 0) {
                                        card.buttons.push({
                                            type: 'postBack',
                                            value: 'VIEWMORE::' + imageUploadState.imageUrl + '::' + imageUploadState.gender[0] + '::' + key + '::' + imageUploadState.isSocialUrl + '::' + upload_id,
                                            title: 'View More'
                                        })
                                    } else {
                                        card.buttons.push({
                                            type: 'postBack',
                                            value: 'VIEWMORE::' + imageUploadState.imageUrl + '::' + key + '::' + imageUploadState.isSocialUrl + '::' + upload_id,
                                            title: 'View More'
                                        })
                                    }
                                    if (_.includes(wishlistSkus, value.matches[0].xc_sku)) {
                                        card.buttons.push({
                                            type: 'postBack',
                                            value: 'WISHLIST::' + value.matches[0].xc_sku + '::' + value.matches[0].productname + '::' + 'WISHLIST.REMOVE.ITEM',
                                            title: 'Remove From Wishlist'
                                        })
                                    } else {
                                        card.buttons.push({
                                            type: 'postBack',
                                            value: 'WISHLIST::' + value.matches[0].xc_sku + '::' + value.matches[0].productname + '::' + 'WISHLIST.SAVE.ITEM',
                                            title: 'Save To Wishlist'
                                        })

                                    }
                                    cards.push(card)
                                }
                            }
                        })

                        if (result.tagged_img) {
                            let predictedCard = {
                                title: 'Predicted Items',
                                images: [{url: IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + result.tagged_img + '&aspect=1.91x1'}],
                            }
                            cards.unshift(predictedCard)
                        }
                        let cards2 = createCards2(session, cards)
                        return cards2
                    })
            } else {
                _.forOwn(result, (value, key) => {
                    if (key !== 'gender_flag' || key !== 'tagged_img') {
                        if (value.matches && value.matches.length > 0) {
                            let imageUrl = IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + value.matches[0].image + '&aspect=1.91x1'
                            value.matches[0].client_name = value.matches[0].client_name ? value.matches[0].client_name : 'Website'
                            let card = {
                                title: toTitleCase(key),
                                // subtitle: 'Price: '+Currency + value.matches[0].price,
                                images: [{url: imageUrl}],
                                buttons: []
                            }
                            card.buttons.push({
                                type: 'postBack',
                                value: 'VIEWITON::' + value.matches[0].product_url + '::WARN::' + _.startCase(value.matches[0].client_name) + "::" + value.matches[0].xc_sku,
                                title: TextService.text('Product_Details')[0]
                            })
                            if (imageUploadState.gender && imageUploadState.gender.length > 0) {
                                card.buttons.push({
                                    type: 'postBack',
                                    value: 'VIEWMORE::' + imageUploadState.imageUrl + '::' + imageUploadState.gender[0] + '::' + key + '::' + imageUploadState.isSocialUrl + '::' + upload_id,
                                    title: 'View More'
                                })
                            } else {
                                card.buttons.push({
                                    type: 'postBack',
                                    value: 'VIEWMORE::' + imageUploadState.imageUrl + '::' + key + '::' + imageUploadState.isSocialUrl + '::' + upload_id,
                                    title: 'View More'
                                })
                            }
                            cards.push(card)
                        }
                    }
                })

                if (result.tagged_img) {
                    let predictedCard = {
                        title: 'Predicted Items',
                        images: [{url: IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + result.tagged_img + '&aspect=1.91x1'}],
                    }
                    cards.unshift(predictedCard)
                }
                let cards2 = createCards2(session, cards)
                return Promise.resolve(cards2)

            }
        }
    }

/**
 * Used in image-category-serivce
 * [ Depricated ]
 */
createAndSendImageResult = (session, argsEntitiesResults) => {
    let imgResultJSON = argsEntitiesResults
    let products = {imgResults: {}}

    for (var Key in imgResultJSON) {
        if (imgResultJSON[Key].matches && imgResultJSON[Key].matches.length > 0) {
            products.imgResults[Key] = {};
            products.imgResults[Key].category = Key;
            products.imgResults[Key].bbox = imgResultJSON[Key].bbox;
            products.imgResults[Key].segmented_bbox = imgResultJSON[Key].segmented_bbox;
            products.imgResults[Key].more = 0;
            products.imgResults[Key].results = imgResultJSON[Key].matches;
        }
        if (Key == "tagged_img") {
            products.imgResults[Key] = imgResultJSON[Key]
        }
    }

    imgResultJSON = products.imgResults

    let elements = []
    for (var Key in imgResultJSON) {
        let element
        if (Key == "tagged_img") {
            element = {
                title: 'Predicted items',
                image_url: IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + imgResultJSON[Key] + '&aspect=1.91x1',
                buttons: []
            }
            for (let k = elements.length - 1; k >= 0; k--) {
                elements[k + 1] = elements[k]
            }
            elements[0] = element;
        } else {
            // TODO :: Sort by score
            // TODO :: Impliment See More and Share buttons
            for (let j = 0; j < imgResultJSON[Key].results.length; j++) {
                let sku = imgResultJSON[Key].results[j].sku;
                let xc_sku = imgResultJSON[Key].results[j].xc_sku;
                let p = imgResultJSON[Key].results[j].price;
                let product_url = imgResultJSON[Key].results[j].product_url;
                let client_name = imgResultJSON[Key].results[j].client_name || 'Website';
                let price = imgResultJSON[Key].results[j].price;
                let productname = imgResultJSON[Key].results[j].productname;
                let brand = imgResultJSON[Key].results[j].brand;

                let im_url

                if (imgResultJSON[Key].results[j].cropped_match) {
                    im_url = imgResultJSON[Key].results[j].cropped_match

                } else if (imgResultJSON[Key].results[j].image) {
                    im_url = imgResultJSON[Key].results[j].image
                }

                element = {
                    title: imgResultJSON[Key].category,
                    //subtitle: StringUtil.capitalizeFirstLetter(imgResultJSON[Key].results[0].brand) + '-' + StringUtil.capitalizeFirstLetter(imgResultJSON[Key].results[0].productname)+" Price + Currency" + p.toString(),
                    subtitle: "Price " + Currency + ' ' + p.toString(),
                    image_url: IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + im_url + '&aspect=1.91x1',
                    buttons: [],
                    sku: sku,
                    xc_sku: xc_sku,
                    product_url: product_url,
                    client_name: client_name,
                    price: price,
                    productname: productname,
                    brand: brand
                }
                elements.push(element);

            }

        }   // else
    }


    if (session.message.source === "facebook") {
        console.log("Sending for ", session.message.source)


        var new_elements = [];
        //${url}/viewproduct?sku=${sku}&xc_sku=${xc_sku}&psid=${session.message.address.user.id}&botid=${session.message.address.bot.id}`
        var detail_url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Details"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4() + '&xc_sku='
        var expanded_url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Expanded_menu"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4() + '&xc_sku='
        var shareWithFriend_url = nconf.get('WebView')["Url"] + nconf.get('WebView')["ShareWithFriend"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
        let temp_node = session.userData.currentNode
        let state = session.userData[temp_node]
        let query_type = ""
        var NotRightUrl = nconf.get('WebView')["Url"]

        switch (temp_node) {
            case Constants.NODE.GUIDEME             :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]

                query_type = "Guide Me"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl += state.xc_category && state.xc_category.length ? '&entity=' + state.xc_category.join(',') : ''
                NotRightUrl += state.gender && state.gender.length ? '&gender=' + state.gender.join(',') : ''
                NotRightUrl += state.price && state.price.length ? '&price=' + state.price.join(',') : ''
                NotRightUrl += state.subcategory && state.subcategory.length ? '&subcategory=' + state.subcategory.join(',') : ''
                NotRightUrl += state.features && state.features.length ? '&feature=' + state.features.join(',') : ''
                NotRightUrl += state.brand && state.brand.length ? '&brand=' + state.brand.join(',') : ''
                NotRightUrl += state.details && state.details.length ? '&details=' + state.details.join(',') : ''
                NotRightUrl += state.colorsavailable && state.colorsavailable.length ? '&colorsavailable=' + state.colorsavailable.join(',') : ''
                NotRightUrl += state.size && state.size.length ? '&size=' + state.size.join(',') : ''

                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
                break;
            case Constants.NODE.NLP                 :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]

                query_type = "NLP"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl += state.entity? '&entity=' + state.entity : ''
                NotRightUrl += state.gender && state.gender.length ? '&gender=' + state.gender.join(',') : ''
                NotRightUrl += state.price && state.price.length ? '&price=' + state.price.join(',') : ''
                NotRightUrl += state.subcategory && state.subcategory.length ? '&subcategory=' + state.subcategory.join(',') : ''
                NotRightUrl += state.features && state.features.length ? '&feature=' + state.features.join(',') : ''
                NotRightUrl += state.brand && state.brand.length ? '&brand=' + state.brand.join(',') : ''
                NotRightUrl += state.details && state.details.length ? '&details=' + state.details.join(',') : ''
                NotRightUrl += state.colorsavailable && state.colorsavailable.length ? '&colorsavailable=' + state.colorsavailable.join(',') : ''
                NotRightUrl += state.size && state.size.length ? '&size=' + state.size.join(',') : ''

                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
                break;
            case Constants.NODE.IMAGEUPLOAD         :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]
                query_type = "VISION"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl = NotRightUrl +"&img_url="+encodeURIComponent(session.userData[temp_node].imageUrl)+"&upload_id="+session.userData[temp_node].upload_id
                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='
                break;
            case Constants.NODE.IMAGEUPLOADVIEWMORE :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]
                query_type = "VISION"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl = NotRightUrl +"&img_url="+encodeURIComponent(session.userData[temp_node].imageUrl)+"&upload_id="+session.userData[temp_node].upload_id
                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='

                break;
            case Constants.NODE.IMAGEUPLOADCATEGORY :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]
                query_type = "VISION"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl = NotRightUrl +"&img_url="+encodeURIComponent(session.userData[temp_node].imageUrl)+"&upload_id="+session.userData[temp_node].upload_id
                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='

                break;
            case Constants.NODE.COLLECTIONS         :
                NotRightUrl = NotRightUrl + nconf.get('WebView')["Expanded_menu"]

                query_type = "COLLECTIONS"
                NotRightUrl += query_type ? '?query_type=' + query_type : ''
                NotRightUrl += state.xc_category && state.xc_category.length ? '&entity=' + state.xc_category.join(',') : ''
                NotRightUrl += state.collectionType ? '&collectionType=' + state.collectionType : ''
                NotRightUrl += state.collectionName  ? '&collectionName=' + state.collectionName : ''
                NotRightUrl += state.gender && state.gender.length ? '&gender=' + state.gender.join(',') : ''
                NotRightUrl += state.price && state.price.length ? '&price=' + state.price.join(',') : ''
                NotRightUrl += state.subcategory && state.subcategory.length ? '&subcategory=' + state.subcategory.join(',') : ''
                NotRightUrl += state.features && state.features.length ? '&feature=' + state.features.join(',') : ''
                NotRightUrl += state.brand && state.brand.length ? '&brand=' + state.brand.join(',') : ''
                NotRightUrl += state.details && state.details.length ? '&details=' + state.details.join(',') : ''
                NotRightUrl += state.colorsavailable && state.colorsavailable.length ? '&colorsavailable=' + state.colorsavailable.join(',') : ''
                NotRightUrl += state.size && state.size.length ? '&size=' + state.size.join(',') : ''

                NotRightUrl = NotRightUrl +"&psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()+'&xc_sku='

                break;
            default :
                break;
        }

        // /expand?psid=${session.message.address.user.id}&botid=${session.message.address.bot.id}&xc_sku=${xc_sku}` + '&x=' + uuidV4(),
        for (let i = 0; i < elements.length; i++) {
            let item = elements[i]
            let new_element = {
                title: _.startCase(item.brand) + ' - ' + _.startCase(item.productname),
                subtitle: 'Price: ' + Currency + ' ' + parseInt(item.price),
                image_url: IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + item.image_url + '&aspect=' + nconf.get("image_aspect_ratio")[nconf.get("payload")["image_aspect_ratio"]],
                default_action: {
                    type: "web_url",
                    url: detail_url + item.xc_sku,
                    messenger_extensions: true,
                    webview_height_ratio: nconf.get('WebView_size')["Details"]
                },
                buttons: []

            };

            new_element.buttons.push({
                type: 'postback',
                title: TextService.text('Product carousel button text')[0],
                payload: 'VIEWSIMILAR::' + item.xc_sku,
            });

            /*new_element.buttons.push(
                {
                    "type": "web_url",
                    "title": TextService.text('Product carousel button text')[1],
                    "url": detail_url + item.xc_sku,
                    "messenger_extensions": true,
                    "webview_height_ratio": nconf.get('WebView_size')["Details"]
                }
            )*/
            if(nconf.get('Share_Friend_Enabled')) {
                new_element.buttons.push(
                    {
                        "type": "web_url",
                        "title": TextService.text('shareWithFriend Button')[0],
                        "url": shareWithFriend_url + item.xc_sku,
                        "messenger_extensions": true,
                        "webview_height_ratio": nconf.get('WebView_size')["ShareWithFriend"]
                    }
                )
            }

            new_element.buttons.push(
                {
                    "type": "web_url",
                    "title": TextService.text('Product carousel button text')[2],
                    "url": NotRightUrl + item.xc_sku,
                    "messenger_extensions": true,
                    "webview_height_ratio": nconf.get('WebView_size')["Expanded_menu"]
                }
            )



            new_elements.push(new_element)
        }

        return Promise.resolve(new_elements)


    } else {

        let Wishlist_enabled = nconf.get('Wishlist')
        let cards = []
        if (Wishlist_enabled) {
            // Get users wishlist
            return UserService.getUsersWishlist(session.message.address)
                .then(wishlist => {

                    if (!wishlist) wishlist = []
                    var wishlistSkus = _.map(wishlist, item => item.xc_sku)

                    for (let i = 0; i < elements.length; i++) {
                        let item = elements[i]

                        let card = new builder.HeroCard(session)
                            .title(_.startCase(item.brand) + ' - ' + _.startCase(item.productname))
                            .subtitle('Price: ' + Currency + ' ' + parseInt(item.price))
                            //.text('Description text about exploring collections.')
                            .images([
                                builder.CardImage.create(session, item.image_url)
                            ])

                        if (_.includes(wishlistSkus, item.xc_sku)) {
                            card.buttons([
                                //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                                builder.CardAction.postBack(session, 'VIEWITON::' + item.product_url + '::WARN::' + _.startCase(item.client_name) + "::" + item.xc_sku, TextService.text('Product_Details')[0]),
                                builder.CardAction.postBack(session, 'VIEWSIMILAR::' + item.xc_sku, 'View Similar'),
                                builder.CardAction.postBack(session, 'WISHLIST::' + item.xc_sku + '::' + item.productname + '::' + 'WISHLIST.REMOVE.ITEM', 'Remove From Wishlist')
                            ])

                        } else {
                            card.buttons([
                                //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                                builder.CardAction.postBack(session, 'VIEWITON::' + item.product_url + '::WARN::' + _.startCase(item.client_name) + "::" + item.xc_sku, TextService.text('Product_Details')[0]),
                                builder.CardAction.postBack(session, 'VIEWSIMILAR::' + item.xc_sku, 'View Similar'),
                                builder.CardAction.postBack(session, 'WISHLIST::' + item.xc_sku + '::' + item.productname + '::' + 'WISHLIST.SAVE.ITEM', 'Save To Wishlist')
                            ])

                        }

                        cards.push(card)

                    }

                    return cards

                })
        } else {
            for (let i = 0; i < elements.length; i++) {
                let item = elements[i]

                let card = new builder.HeroCard(session)
                    .title(_.startCase(item.brand) + ' - ' + _.startCase(item.productname))
                    .subtitle('Price: ' + Currency + ' ' + parseInt(item.price))
                    //.text('Description text about exploring collections.')
                    .images([
                        builder.CardImage.create(session, item.image_url)
                    ]).buttons([
                        //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                        builder.CardAction.postBack(session, 'VIEWITON::' + item.product_url + '::WARN::' + _.startCase(item.client_name) + "::" + item.xc_sku, TextService.text('Product_Details')[0]),
                        builder.CardAction.postBack(session, 'VIEWSIMILAR::' + item.xc_sku, 'View Similar'),
                    ])
                cards.push(card)

            }
            return Promise.resolve(cards)

        }

    }
}

/**
 * Returns if a filter should be shown for a given result set
 * returns true If there are all items have identical values for a given attribute
 * false if atleast one item has a different value for the given attribute
 */
hasDistinctAttributes = (result, attr) => {

    if (result.length === 1) return false

    let resArr = []

    if (attr === 'brand' || attr === 'price') {
        // For string attr: brand, price
        resArr = _(result).filter(item => item[attr])
            .groupBy(attr)
            .map((item, key) => {
                return {key: key, count: item.length}
            }).value()

    } else {
        // For attr is an array (i.e. colorsavailable, size)
        resArr = _(result).filter(item => item[attr])
            .map(item => {
                // Collections returns array, Structured search returns a string
                // console.log( 'BUG--> ' , item[attr], Array.isArray( item[attr] ), typeof item[attr] )
                if (Array.isArray(item[attr])) {
                    item[attr] = item[attr].sort()
                } else {
                    // TODO URGENT: Remove this hack, same in filters.js
                    // Vision api sends python unicode strings ( e.g: '[u'black']' )which is invalid json
                    /*
                     // Replace the u' with '
                     item[ attr ] = item[ attr ].replace( /u'(?=[^:]+')/g, "'" )
                     // Replace \' with TEMPORARY_STRING
                     item[ attr ] = item[ attr ].replace( /\\'/g, 'TEMPORARY_STRING' )
                     // Replace ' with "
                     item[ attr ] = item[ attr ].replace( /'/g, '"' )
                     // Replace TEMPORARY_STRING with \'
                     item[ attr ] = item[ attr ].replace( /TEMPORARY_STRING/g, "\\'" )
                     */
                    item[attr] = JSON.parse(item[attr]).sort()
                }
                return item
            })
            .groupBy(attr)
            .map((item, key) => {
                return {key: key, count: item.length}
            }).value()
    }

    return resArr.length > 1

}

/**
 * Returns parsed json if is a json string
 * else returns string
 */
toJSONIfJSON = str => {
    if (isJson(str)) {
        return JSON.parse(str)
    } else {
        return str
    }
},

    /**
     * Check if given string is a valid json
     */
    isJson = str => {
        try {
            JSON.parse(str);
        } catch (e) {
            return false
        }
        return true
    }


createPersistentMenu = function () {
    let token = nconf.get('FACEBOOK_PAGE_TOKEN', '')
    let persistentMenuMsg = Constants.Labels['Persistent Menu Options']
    let persistentMenuPayloadMsg = Constants.Labels.persistentMenuPayloadMsg
    let menu =[]
    let Human_augmentauion = nconf.get( 'Human_augmentauion' )
    if(Human_augmentauion){
        menu.push({
            type: "postback",
            title: persistentMenuMsg[0],
            payload: persistentMenuPayloadMsg[0]
        })

    }
    menu.push({
        type: "postback",
        title: persistentMenuMsg[1],
        payload: persistentMenuPayloadMsg[1]
    })
    let Help = nconf.get( 'Help' )
    if(Help) {
        menu.push({
            type: "postback",
            title: persistentMenuMsg[2],
            payload: persistentMenuPayloadMsg[2]
        })
    }
    let Wishlist_enabled = nconf.get( 'Wishlist' )
    if(Wishlist_enabled){
        menu.push({
            type: "postback",
            title: persistentMenuMsg[3],
            payload: persistentMenuPayloadMsg[3]
        })

    }

    let personalisation_enabled = nconf.get( 'Personalisation' )
    if(menu.length<=4){
        if(personalisation_enabled) {
            menu.push({
                type: "postback",
                title: persistentMenuMsg[4],
                payload: persistentMenuPayloadMsg[4]
            })

        }
    }

    let Discover_Features = nconf.get( 'Discover Features' )
    if(Discover_Features) {

        if (menu.length <= 4) {
            menu.push({
                type: "postback",
                title: persistentMenuMsg[5],
                payload: persistentMenuPayloadMsg[5]
            })

        }
    }
    if(menu.length<=4){
        menu.push({
            type: "postback",
            title: persistentMenuMsg[6],
            payload: persistentMenuPayloadMsg[6]
        })

    }
    console.log(menu)
    uniRest.post('https://graph.facebook.com/v2.6/me/thread_settings?access_token=' + token)
        .headers({'Content-Type': 'application/json'})
        .send({
            setting_type: "call_to_actions",
            thread_state: "existing_thread",
            call_to_actions: menu
        })
        .end(function (response) {
            console.log('info', 'Persistent Menu Button Added\t' + JSON.stringify(response.body));
        });

}

createGetStartedButton  = function ()  {
    let token = nconf.get('FACEBOOK_PAGE_TOKEN', '')
    let getStartedPayload = Constants.Labels['getStartedPayload']
    uniRest.post('https://graph.facebook.com/v2.6/me/thread_settings?access_token=' + token)
        .headers({'Content-Type': 'application/json'})
        .send({
            setting_type:"call_to_actions",
            thread_state:"new_thread",
            call_to_actions:[
                {
                    "payload":getStartedPayload
                }
            ]
        })
        .end(function (response) {
            winston.log('info','GetStarted Button Added\t'+ JSON.stringify(response.body));
        });

};


ifimgUrl = function (url, callback) {
    // console.log("checking if image url for url :- " + url)
    if (url) {
        uniRest.get(url)
            .end(function (response) {
                try {
                    // console.log("successfully checked if image url for url :- " + url)
                    callback(response.headers);
                } catch (err) {
                    winston.log('error', "Error Occurred in finding if the given url is an image url :-\t" + url);
                    winston.log('error', err);
                    //console.log(err)
                    callback({});
                }
            });
    } else {
        callback({})

    }
}



getRelaxationButtonText = function (combination) {

    let button_text = ""
    let delim = ""

    for(let i = 0;i<combination.length;i++){

        button_text = button_text + delim +combination[i].attribute_name
        delim = " & "
    }

    return button_text


}

getRelaxationMessage = function (session,filter,text) {

    let temp_node = session.userData.currentNode

    let userData = session.userData[temp_node]

    let attribute = {}

    let price = userData.price && userData.price.length ? + userData.price.join(',') : ''

    var numberPattern = /\d+/gi;

    let price_range = ""
    let test
    if(price) {
        let test = price.match(numberPattern)
        if (price.search('<>') >= 0) {
            price_range = "within"
            price = test[0] + " to " + test[1] + " " + Currency

        } else if (price.search('<') >= 0) {

            price_range = "below"

            price = test[0] + " " + Currency


        } else if (price.search('>') >= 0) {

            price_range = "above"

            price = test[0] + " " + Currency

        } else {

            price = ""

        }
    }else{
        price = ""
    }

    console.log("price is ",price)
    console.log("price_range is ",price_range)




    attribute.color = userData.colorsavailable && userData.colorsavailable.length ? userData.colorsavailable.join(',') : ''
    attribute.entity_name = userData.entity
    attribute.brand = userData.brand && userData.brand.length ? "from " + userData.brand.join(',') : ''
    attribute.price = price
    attribute.price_range = price_range
    attribute.filter = filter

    let msg = TextService.text( text,attribute)

    msg = msg[0].replace(/\s+/g, " ")

    return msg

}

module.exports = {

    /**
     * Return a subset of an array
     */
    paginate: paginate,

    /**
     * Returns a subset of an array, to be used in a paged list
     * Adds Prev/Next to the list also
     */
    pageList: pageList,

    /**
     * Create a prompt from a given string array
     */
    createPromptArray: promptStringsList => {
        if (!promptStringsList || promptStringsList.length < 1) return []
    },

    createCards: createCards,

    createCards2: createCards2,
    /**
     * Process result from soical urls and image upload (Vision) urls
     * TODO: Change the function name
     */
    createAndSendImageResult2: createAndSendImageResult2,

    createAndSendImageResult: createAndSendImageResult,

    hasDistinctAttributes: hasDistinctAttributes,

    /**
     * Returns parsed json if is a json string
     * else returns string
     */
    toJSONIfJSON: toJSONIfJSON,

    isJson: isJson,

    createPersistentMenu: createPersistentMenu,

    ifimgUrl: ifimgUrl,

    createGetStartedButton:createGetStartedButton,

    getRelaxationButtonText :getRelaxationButtonText,

    getRelaxationMessage : getRelaxationMessage

}
