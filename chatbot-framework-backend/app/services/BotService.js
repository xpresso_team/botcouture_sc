var _ = require('lodash')
var builder = require('botbuilder')
var nconf = require('nconf')
var TextService = require('./TextService')
var KafkaService = require('../../kafka-service')
var HumanAugmentationService = require('./human-augmentation-service')
var UserService = require('./UserService')


let HA_CLIENT_ORGANIZATION = nconf.get("HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION")


/**
 * Utility function for getting text from Google sheet
 * { text : 'Googel Key', index : 0, data : { replace : replaceStirng, } }
 */
getText = text => {

    console.log("Text in get text is ", text)

    if (_.isArray(text)) {
        return text
    }

    if (_.isString(text)) {

        text = {text: text}
    }

    if ((text)&&(_.isFinite(text.index))) {
        return TextService.text(text.text, text.data)[text.index]
    } else {
        if (text) {
            return TextService.text(text.text, text.data)
        }else{
            return text
        }
    }
}


getrandText = text => {
    if (_.isArray(text)) {
        let rand_int = Math.floor(Math.random() * (text.length));
        return text[rand_int]
    } else {
        return text
    }
}


/**
 * Bot sends a text message
 */
sendText = (session, text) => {

    //1252903224780060

    let promptText = getText(text)

    let HAText = promptText

    if (_.isArray(promptText)) {
        HAText = promptText[0]
    }

    //console.log(session)
    /*if(session.message.address.user.id==1313298325359965){
     let new_session
     //console.log("old session ",session)
     //new_session = UserService.return_user('♠1252903224780060')
     //console.log("New session 0 ",new_session)
     //new_session = UserService.return_user(1252903224780060)
     //console.log("New session 1 ",new_session)
     new_session = UserService.return_user('1252903224780060')
     console.log("New session 2 ",new_session)
     //console.log("dummy_list is  ",UserService.dummy_list)
     //new_session = UserService.dummy_list.get('♠1252903224780060')
     //console.log("New session3 ",new_session)
     //new_session = UserService.dummy_list.get('1252903224780060')
     //console.log("New session4 ",new_session)
     //new_session = UserService.dummy_list.get(1252903224780060)
     //console.log("New session5 ",new_session)
     if(new_session){
     session =new_session
     console.log("user replaced")
     }
     //session =UserService.return_user( session.message.address.user.id)

     }*/

    session.send(promptText)

    let event = {
        type: 'message',
        agent: 'botbuilder',
        source: session.message.source,
        timestamp: new Date().toISOString(),
        address: session.message.address,
        text: HAText,
        session_id: session.userData.session_id,
        message_number: '',
        message_type: 'OB',
        message_chat: HAText,
        message_type_flag: 'OT',
        visual_search: '',
        product_response_list: '',
        nlp_query_response: '',
        vision_file_link: '',
        vision_engine_response: '',
        click_item_sku: '',
    }

    event = _.merge(_.cloneDeep(session.userData.analytics.out), event)

    KafkaService.sendOutgoingMessage(session, event)

    // KafkaService.sendIncomingMessage( session )
    // console.log( 'Kafka sending outgoing -> ', event )
    // // Send to Kafka
    // let payload = [ { topic: 'ashleyexp', messages: JSON.stringify( event ), partition: 0 } ]
    // KafkaService.producer.send( payload, ( err, data ) => {
    //   if ( err ) {
    //     // winston.log( 'error', err )
    //     return
    //   }
    // })
    //   type: 'message',
    //   agent: 'botbuilder',
    //   source: 'facebook',
    //   address: {
    //     id: '79pdm11b72O',
    //     channelId: 'facebook',
    //     user: { id: '1306415456092513', name: 'Abhishek Dutt' },
    //     conversation: { isGroup: false, id: '1306415456092513-1101360899986891' },
    //     bot: { id: '1101360899986891', name: 'abhishek_mccb_2' },
    //     serviceUrl: 'https://facebook.botframework.com',
    //     useAuth: true
    //   },
    //   text: 'Here\'s what I\'ve found with the information so far.'
    // }

    let message = {
        session_id: session.userData.session_id,       // session id
        sender_type: 'bot',                             // one of [ 'user', 'agent', 'bot' ]
        user_psid: null,                              // If message sent by user, null otherwise
        agent_psid: null,                              // If message sent by agent, null otherwise
        bot_id: session.message.address.bot.id,    // If message sent by bot, as given by MS Bot FW, null otherwise
        channel_id: session.message.source,            // channel id
        client_organization: HA_CLIENT_ORGANIZATION,            // Client org id
        msg_timestamp: new Date().toISOString(),          // ISO 8601 with timezone, Z format
        text: HAText,                        // Text of text or quick reply message
        carousel: null,
        quick_reply: null
    }
    // Dont send message in case user is talking to an agent
    if (_.has(session, 'userData.user_engaged_queue.flag') && !session.userData.user_engaged_queue.flag) {
        HumanAugmentationService.sendMessage(message)
    } else if (!_.has(session, 'userData.user_engaged_queue.flag')) {
        HumanAugmentationService.sendMessage(message)
    }

}

/**
 * Bot sends a Carousel
 */
sendCard = (session, cards, fb_flag, template_type, text) => {

    var reply
    var carousel
    var attachments


    if(text) {
        text = getText(text)
        text = getrandText(text)
    }


    if (fb_flag) {

        //console.log("Sending facebook native card")

        if (template_type == "generic") {

            payload = {
                template_type: template_type,
                sharable: nconf.get('payload').sharable,
                image_aspect_ratio: nconf.get('payload').image_aspect_ratio,
                elements: cards
            }

        } else if (template_type == "button") {
            payload = {
                "template_type": template_type,
                "text": text,
                "buttons": cards
            }

        }

        reply = new builder.Message(session)
            .sourceEvent({
                "facebook": {
                    "attachment": {
                        "type": "template",
                        "payload": payload
                    }
                }
            })
        session.send(reply)

        //for kafka
        attachments = cards.map(card => card)

        carousel = _.map(cards, card => {
            return {
                title: card.title || null,
                subtitle: card.subtitle || null,
                images: [{image_url: card.image_url || null}],
                buttons: _.map(card.buttons, button => {
                    return {
                        button_type: button.type,
                        button_payload: button.payload,
                        button_title: button.title,
                    }
                }),
            }
        })
       // console.log(carousel)


    } else {

        console.log("Card is " + cards)
        reply = new builder.Message(session).attachmentLayout(builder.AttachmentLayout.carousel).attachments(cards);


        session.send(reply)

        //for kafka
        attachments = cards.map(card => card.data)


        carousel = _.map(cards, card => {
            return {
                title: card.data.content.title || null,
                subtitle: card.data.content.subtitle || null,
                text: card.data.content.text || null,
                images: _.map(card.data.content.images, image => {
                    return {image_url: image.url}
                }),
                buttons: _.map(card.data.content.buttons, button => {
                    return {
                        button_type: button.type,
                        button_value: button.value,
                        button_title: button.title,
                    }
                }),
            }
        })


    }

    let event = {
        type: 'message',
        agent: 'botbuilder',
        source: session.message.source,
        timestamp: new Date().toISOString(),
        address: session.message.address,
        attachmentLayout: 'carousel',
        attachments: attachments,
        session_id: session.userData.session_id,
        message_number: '',      //++session.userData.message_number,

        message_type: 'OB',
        message_chat: '',
        message_type_flag: session.userData.analytics.out.message_type_flag,
        visual_search: session.userData.analytics.out.visual_search,
        product_response_list: _.cloneDeep(session.userData.analytics.out.product_response_list),
        nlp_query_response: '',
        vision_file_link: '',
        vision_engine_response: _.cloneDeep(session.userData.analytics.out.vision_engine_response),
        click_item_sku: '',
    }

    event = _.merge(_.cloneDeep(session.userData.analytics.out), event)

    delete session.userData.analytics.out.product_response_list
    delete session.userData.analytics.out.vision_engine_response

    KafkaService.sendOutgoingMessage(session, event)

    let message = {
        session_id: session.userData.session_id,       // session id
        sender_type: 'bot',                             // one of [ 'user', 'agent', 'bot' ]
        user_psid: null,                              // If message sent by user, null otherwise
        agent_psid: null,                              // If message sent by agent, null otherwise
        bot_id: session.message.address.bot.id,    // If message sent by bot, as given by MS Bot FW, null otherwise
        channel_id: session.message.source,            // channel id
        client_organization: HA_CLIENT_ORGANIZATION,            // Client org id
        msg_timestamp: new Date().toISOString(),          // ISO 8601 with timezone, Z format
        text: null,                              // Text of text or quick reply message
        carousel: carousel,
        quick_reply: null,
    }
    HumanAugmentationService.sendMessage(message)

}

/**
 *
 */
sendQuickReplyButtons = (session, text, buttonsText) => {

    console.log("Text is ", text)

    let promptText = getText(text)


    let HAText = promptText

    if (_.isArray(promptText)) {
        HAText = promptText[0]
    }

    console.log("HAText is ", HAText)
    console.log("promptText is ", promptText)
    console.log("buttonsText is ", buttonsText)

    builder.Prompts.choice(session, promptText, buttonsText, {
        maxRetries: 0,
        listStyle: builder.ListStyle.button
    })

    let event = {
        type: 'message',
        agent: 'botbuilder',
        source: session.message.source,
        timestamp: new Date().toISOString(),
        address: session.message.address,
        text: HAText,
        attachments: [{
            contentType: 'application/vnd.microsoft.keyboard',
            content: {
                buttons: buttonsText.map(button => {
                    return {type: 'imBack', value: button, title: button}
                })
            }
        }],
        session_id: session.userData.session_id,
        message_number: '',      //++session.userData.message_number,

        message_type: 'OB',
        message_chat: HAText,
        message_type_flag: 'OQR',
        visual_search: '',
        product_response_list: '',
        nlp_query_response: '',
        vision_file_link: '',
        vision_engine_response: '',
        quick_reply_buttons: buttonsText.join(', '),
        click_item_sku: '',
    }

    event = _.merge(_.cloneDeep(session.userData.analytics.out), event)

    KafkaService.sendOutgoingMessage(session, event)


    let message = {
        session_id: session.userData.session_id,       // session id
        sender_type: 'bot',                             // one of [ 'user', 'agent', 'bot' ]
        user_psid: null,                              // If message sent by user, null otherwise
        agent_psid: null,                              // If message sent by agent, null otherwise
        bot_id: session.message.address.bot.id,    // If message sent by bot, as given by MS Bot FW, null otherwise
        channel_id: session.message.source,            // channel id
        client_organization: HA_CLIENT_ORGANIZATION,            // Client org id
        msg_timestamp: new Date().toISOString(),          // ISO 8601 with timezone, Z format
        text: HAText,                        // Text of text or quick reply message
        carousel: null,
        quick_reply: buttonsText.map(button => {
            return {type: 'imBack', value: button, title: button}
        })
    }
    buttonsText.map(button => {
        console.log("button is ",button)
        return {type: 'imBack', value: button, title: button}
    })
    HumanAugmentationService.sendMessage(message)

}

sendFbQuickReply = (session, text, quick_reply) => {

    if(text) {

        text = getText(text)
        text = getrandText(text)
    }
    
    var reply = new builder.Message(session)
        .sourceEvent({
            "facebook": {
                "text": text,
                "quick_replies": quick_reply
            }
        })

    console.log(reply)
    session.send(reply)

    let HAText = text

    if (_.isArray(text)) {
        HAText = text[0]
    }


    let event = {
        type: 'message',
        agent: 'botbuilder',
        source: session.message.source,
        timestamp: new Date().toISOString(),
        address: session.message.address,
        text: HAText,
        attachments: [{
            contentType: 'application/vnd.microsoft.keyboard',
            content: {
                buttons: quick_reply.map(element => {
                    return {type: 'imBack', value: element.payload, title: element.title || element.payload}
                })
            }
        }],
        session_id: session.userData.session_id,
        message_number: '',      //++session.userData.message_number,

        message_type: 'OB',
        message_chat: HAText,
        message_type_flag: 'OQR',
        visual_search: '',
        product_response_list: '',
        nlp_query_response: '',
        vision_file_link: '',
        vision_engine_response: '',
        quick_reply_buttons: quick_reply.map(element => {
            return element.title || element.payload
        }).join(', '),
        click_item_sku: '',
    }
    event = _.merge(_.cloneDeep(session.userData.analytics.out), event)

    KafkaService.sendOutgoingMessage(session, event)


    let message = {
        session_id: session.userData.session_id,       // session id
        sender_type: 'bot',                             // one of [ 'user', 'agent', 'bot' ]
        user_psid: null,                              // If message sent by user, null otherwise
        agent_psid: null,                              // If message sent by agent, null otherwise
        bot_id: session.message.address.bot.id,    // If message sent by bot, as given by MS Bot FW, null otherwise
        channel_id: session.message.source,            // channel id
        client_organization: HA_CLIENT_ORGANIZATION,            // Client org id
        msg_timestamp: new Date().toISOString(),          // ISO 8601 with timezone, Z format
        text: HAText,                        // Text of text or quick reply message
        carousel: null,
        quick_reply: quick_reply.map(element => {
            return {type: 'imBack', value: element.payload, title: element.title || element.payload}
        })
    }
    HumanAugmentationService.sendMessage(message)


}

module.exports = {
    getText: getText,
    getrandText: getrandText,
    sendText: sendText,
    sendCard: sendCard,
    sendQuickReplyButtons: sendQuickReplyButtons,
    sendFbQuickReply: sendFbQuickReply
}