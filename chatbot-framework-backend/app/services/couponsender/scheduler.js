var _         = require( 'lodash'       )
var builder   = require( 'botbuilder'   )
var Constants = require('../../constants')
var request   = require( 'superagent'   )
const uuidV4  = require( 'uuid/v4'      )
var nconf     = require( 'nconf'        )
var schedule = require('node-schedule');
var couponService = require( './couponService')






/**
 * Sends Coupon
 */
couponScheduler = (bot) => {



    let arg = nconf.get("Scheduler")["Second"]+' '+nconf.get("Scheduler")["Minute"]+' '+ nconf.get("Scheduler")["Hour"] +' * * *'

    console.log("Scheduler set  for " ,arg)

    schedule.scheduleJob(arg, function(){

        console.log("Running Scheduled Job")
        couponService.couponSender(bot)
    });
}


module.exports = {
    couponScheduler :couponScheduler
}