const _ = require('lodash')
const nconf = require('nconf')
const request = require('superagent')
const winston = require('./loggly/LoggerUtil')

const HUMAN_AUGMENTATION_SERVER = nconf.get('HUMAN_AUGMENTATION_SERVER')
let HA_CLIENT_ORGANIZATION = nconf.get("HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION")
let NLP_URL = nconf.get('NLP_URL')
let NLP_DB_PORT = nconf.get('NLP_DB_PORT')
let GENERIC_API_HOST = nconf.get('GENERIC_API_HOST')
let GENERIC_SEARCH_HOST = 'http://' + NLP_URL + ':' + NLP_DB_PORT+"/genericAPI/"

//let GENERIC_SEARCH_HOST = 'http://'+GENERIC_API_HOST+"/genericAPI/"

/**
 * Register a new agent
 * New agent account will be inactive by default
 * and will need to be made active seprately
 */

registerNewAgent = newAgent => {


    // Example argument :
    // let data = {
    //   agent_name          : 'James Bond'  ,
    //   channel_id          : 'facebook'    ,
    //   psid                : '123456XYZ'   ,
    //   client_organization : HA_CLIENT_ORGANIZATION,
    // }


    let url = HUMAN_AUGMENTATION_SERVER + '/AgentAccount/registerNewAgentAccount'

    console.log('Human Augmentation registerNewAgentAccount: ' + url)
    winston.log('info', 'Human Augmentation registerNewAgentAccount: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(newAgent).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation registerNewAgentAccount API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service-> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })

}


/**
 * Login an Agent and add him to AgentAvailable queue
 */

loginAgent = data => {


    // Example argument :
    // let data = {
    //   channel_id          : 'facebook'    ,
    //   psid                : '123456XYZ'   ,
    //   client_organization : 'Shopper Stop',
    // }

    // Example response (logged in agent object):
    // {
    //   "agent_queue": [],
    //   "agent_user_queue": [],
    //   "agent_name": "James Bond",
    //   "channel_id": "facebook",
    //   "psid": "123",
    //   "client_organization": "Shopper Stop",
    //   "account_active": true,
    //   "createdAt": "2017-04-22T10:22:47.309Z",
    //   "updatedAt": "2017-04-22T10:22:47.309Z",
    //   "id": 50
    // }

    let url = HUMAN_AUGMENTATION_SERVER + '/AgentAccount/loginAgentAccount'

    console.log('Human Augmentation loginAgentAccount: ' + url)
    winston.log('info', 'Human Augmentation loginAgentAccount: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation loginAgentAccount API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service-> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })

}


/**
 * Logout a user by removing him from all queues
 * TODO: logout using AgentAccount.id
 */

logoutAgent = data => {

    // Example argument :
    // let data = {
    //   channel_id          : 'facebook'    ,
    //   psid                : '123456XYZ'   ,
    //   client_organization : 'Shopper Stop',
    // }

    // Example response:
    // {
    //   "message": "Logout success"
    // }

    let url = HUMAN_AUGMENTATION_SERVER + '/AgentAccount/logoutAgentAccount'

    console.log('Human Augmentation logoutAgentAccount: ' + url)
    winston.log('info', 'Human Augmentation logoutAgentAccount: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation logoutAgentAccount API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service -> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })

}


/**
 * Returns a list of AgentAccounts available in AgentQueue
 */

getAllAvailableAgent = data => {

    // Example argument :
    // let data = {
    //   channel_id          : 'facebook'    ,
    //   client_organization : 'Shopper Stop',
    // }

    // Example response (array of agents):
    // [
    //   {
    //     "agent_queue": [       // Array with only 1 element
    //       {
    //         "agent_start_time": "2017-04-22T10:28:29.493Z",
    //         "createdAt": "2017-04-22T10:28:29.500Z",
    //         "updatedAt": "2017-04-22T10:28:29.510Z",
    //         "id": 28
    //       }
    //     ],
    //     "agent_name": "James Bond",
    //     "channel_id": "facebook",
    //     "psid": "123",
    //     "client_organization": "Shopper Stop",
    //     "account_active": true,
    //     "createdAt": "2017-04-22T10:22:47.309Z",
    //     "updatedAt": "2017-04-22T10:22:47.309Z",
    //     "id": 50
    //   },
    //   {
    //     Agent 2 details
    //   }
    //   ...

    // ]

    let url = HUMAN_AUGMENTATION_SERVER + '/AgentAccount/getAllAvailableAgentAccounts'

    console.log('Human Augmentation getAllAvailableAgentAccounts: ' + url)
    winston.log('info', 'Human Augmentation getAllAvailableAgentAccounts: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation getAllAvailableAgentAccounts API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service -> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })


}


/**
 * Returns Agent which ahs been free for the longest time
 */

getNextAvailableAgent = data => {


    // Example argument :
    // let data = {
    //   channel_id          : 'facebook'    ,
    //   client_organization : 'Shopper Stop',
    // }

    // Example response (agent object) :
    // {
    //   "agent_queue": [
    //     {
    //       "agent_start_time": "2017-04-22T10:38:25.353Z",
    //       "createdAt": "2017-04-22T10:38:25.359Z",
    //       "updatedAt": "2017-04-22T10:38:25.377Z",
    //       "id": 29
    //     }
    //   ],
    //   "agent_name": "James Bond",
    //   "channel_id": "facebook",
    //   "psid": "123",
    //   "client_organization": "Shopper Stop",
    //   "account_active": true,
    //   "createdAt": "2017-04-22T10:22:47.309Z",
    //   "updatedAt": "2017-04-22T10:22:47.309Z",
    //   "id": 50
    // }

    let url = HUMAN_AUGMENTATION_SERVER + '/AgentAccount/getNextAvailableAgent'

    console.log('Human Augmentation getNextAvailableAgent: ' + url)
    winston.log('info', 'Human Augmentation getNextAvailableAgent: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation getNextAvailableAgent API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service -> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    }).catch( err => {
        console.log( 'HA Error -> ', err )
    })



}

/**
 * Get all users waiting in queue
 */

getAllUsersWaitingInQueue = data => {

    // Example argument :
    // let data = {
    //   channel_id          : 'facebook'    ,
    //   client_organization : 'Shopper Stop',
    // }


    let url = HUMAN_AUGMENTATION_SERVER + '/UserAccount/getAllUsersWaitingInQueue'

    console.log('Human Augmentation getAllUsersWaitingInQueue: ' + url)
    winston.log('info', 'Human Augmentation getAllUsersWaitingInQueue: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation getAllUsersWaitingInQueue API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service -> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })

}


/**
 * Add a user to the waiting queue
 */

addUserToWaitingQueue = data => {

    // Example argument :
    // let data = {
    //   channel_id          : 'facebook'    ,
    //   psid                : '123456XYZ'   ,
    //   client_organization : 'Shopper Stop',
    // }


    let url = HUMAN_AUGMENTATION_SERVER + '/UserAccount/addUserToWaitingQueue'

    console.log('Human Augmentation addUserToWaitingQueue: ' + url)
    winston.log('info', 'Human Augmentation addUserToWaitingQueue: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation addUserToWaitingQueue API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service -> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })

}


/**
 * Remove a user from waiting queue
 */

removeUserFromWaitingQueue = data => {

    // Example argument :
    // let data = {
    //   channel_id          : 'facebook'    ,
    //   psid                : '123456XYZ'   ,
    //   client_organization : 'Shopper Stop',
    // }

    let url = HUMAN_AUGMENTATION_SERVER + '/UserAccount/removeUserFromWaitingQueue'

    console.log('Human Augmentation removeUserFromWaitingQueue: ' + url)
    winston.log('info', 'Human Augmentation removeUserFromWaitingQueue: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation removeUserFromWaitingQueue API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service -> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })

}


/**
 * Add User and an Agent to AgentUserQueue
 */


addUserAndAgentToEngagedQueue = data => {

    // Example argument :
    // let data = {
    //   channel_id          : 'facebook'    ,
    //   user_psid           : '123456XYZ'   ,
    //   agent_psid          : '987652142'   ,
    //   client_organization : 'Shopper Stop',
    // }

    // Example response:
    // {
    //   "message": "User and Agent added to the queue successfully."
    // }

    let url = HUMAN_AUGMENTATION_SERVER + '/AgentUserQueue/addUserAndAgentToEngagedQueue'

    console.log('Human Augmentation addUserAndAgentToEngagedQueue: ' + url)
    winston.log('info', 'Human Augmentation addUserAndAgentToEngagedQueue: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation addUserAndAgentToEngagedQueue API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service -> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })

}


/**
 * Add User and an Agent to AgentUserQueue
 */

removeUserAndAgentFromEngagedQueue = data => {

    // Example argument :
    // let data = {
    //   channel_id          : 'facebook'    ,
    //   user_psid           : '123456XYZ'   ,
    //   agent_psid          : '987652142'   ,
    //   client_organization : 'Shopper Stop',
    // }

    // Example response:
    // {
    //   "message": "User and Agent added to the queue successfully."
    // }

    let url = HUMAN_AUGMENTATION_SERVER + '/AgentUserQueue/removeUserAndAgentFromEngagedQueue'

    console.log('Human Augmentation removeUserAndAgentFromEngagedQueue: ' + url)
    winston.log('info', 'Human Augmentation removeUserAndAgentFromEngagedQueue: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation removeUserAndAgentFromEngagedQueue API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service -> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })
}


/**
 * Returns agent object with any queues he might be in
 */
getAgentAccountStatus = data => {
    // Example argument :
    // let data = {
    //   channel_id          : 'facebook'    ,
    //   psid                : '987652142'   ,
    //   client_organization : 'Shopper Stop',
    // }

    // Example response:
    // agent_queue, agent_user_queue will always be arrays
    // {
    //   "agent_queue": [
    //     {
    //       "agent_account": 17,
    //       "agent_start_time": "2017-05-17T19:28:37.391Z",
    //       "createdAt": "2017-05-17T19:28:37.395Z",
    //       "updatedAt": "2017-05-17T19:28:37.395Z",
    //       "id": 36
    //     }
    //   ],
    //   "agent_user_queue": [],
    //   "agent_name": "James Bond",
    //   "channel_id": "facebook",
    //   "psid": "1",
    //   "client_organization": "Shopper Stop",
    //   "account_active": true,
    //   "createdAt": "2017-05-17T19:28:30.336Z",
    //   "updatedAt": "2017-05-17T19:28:30.336Z",
    //   "id": 17
    // }

    let url = HUMAN_AUGMENTATION_SERVER + '/AgentAccount/getAgentAccountStatus'

    console.log('Human Augmentation getAgentAccountStatus: ' + url)
    winston.log('info', 'Human Augmentation getAgentAccountStatus: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation getAgentAccountStatus API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service -> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })

}


/**
 * Returns agent object with any queues he might be in
 */
getUserAccountStatus = data => {
    // Example argument :
    // let data = {
    //   channel_id          : 'facebook'    ,
    //   psid                : '987652142'   ,
    //   client_organization : 'Shopper Stop',
    // }

    // Example response:
    // user_queue, agent_user_queue will always be arrays
    // {
    //   "user_queue": [
    //     {
    //       "user_account": 40,
    //       "user_start_time": "2017-05-17T19:32:13.199Z",
    //       "createdAt": "2017-05-17T19:32:13.201Z",
    //       "updatedAt": "2017-05-17T19:32:13.201Z",
    //       "id": 24
    //     }
    //   ],
    //   "agent_user_queue": [],
    //   "user_name": "User Max",
    //   "channel_id": "facebook",
    //   "psid": "1",
    //   "client_organization": "Shopper Stop",
    //   "createdAt": "2017-05-17T19:32:04.056Z",
    //   "updatedAt": "2017-05-17T19:32:04.056Z",
    //   "id": 40
    // }

    let url = HUMAN_AUGMENTATION_SERVER + '/UserAccount/getUserAccountStatus'

    console.log('Human Augmentation getUserAccountStatus: ' + url)
    winston.log('info', 'Human Augmentation getUserAccountStatus: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation getUserAccountStatus API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service -> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })

}

/**
 * Get AgentUserQueue (i.e. list of agent and users that are talking to each other)
 */
getAgentUserQueue = data => {

    // Example argument :
    // {
    // 	"client_organization" : "Shopper Stop",
    // 	"channel_id" : "facebook"
    // }

    // Example response: Array of Objects with user_account and agent_account
    // [
    //   {
    //     "user_account": {
    //       "user_name": "User Max",
    //       "channel_id": "facebook",
    //       "psid": "1",
    //       "client_organization": "Shopper Stop",
    //       "createdAt": "2017-05-17T19:32:04.056Z",
    //       "updatedAt": "2017-05-17T19:37:49.239Z",
    //       "id": 40
    //     },
    //     "agent_account": {
    //       "agent_name": "James Bond",
    //       "channel_id": "facebook",
    //       "psid": "1",
    //       "client_organization": "Shopper Stop",
    //       "account_active": true,
    //       "createdAt": "2017-05-17T19:28:30.336Z",
    //       "updatedAt": "2017-05-17T19:37:49.255Z",
    //       "id": 17
    //     },
    //     "start_time": "2017-05-17T19:37:49.229Z",
    //     "createdAt": "2017-05-17T19:37:49.230Z",
    //     "updatedAt": "2017-05-17T19:37:49.230Z",
    //     "id": 26
    //   }
    //   ...
    //   ...
    // ]

    let url = HUMAN_AUGMENTATION_SERVER + '/AgentUserQueue/getAgentUserQueue'

    console.log('Human Augmentation getAgentUserQueue: ' + url)
    winston.log('info', 'Human Augmentation getAgentUserQueue: ' + url)
    let startTime = process.hrtime()
    let elapsed = null

    return request.post(encodeURI(url)).send(data).then(res => {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Human Augmentation getAgentUserQueue API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        console.log('Response from ha service -> ', JSON.parse(res.text))
        return JSON.parse(res.text)
    })

}



// GUIDE ME QUERIES
getProductdetails = urls => {
    let url = GENERIC_SEARCH_HOST+'?product_url=' + encodeURIComponent(urls) + '&returnField=image,product_url,gender,xc_sku,colorsavailable,size,price,productname,subcategory,sku,client_name,brand&productDetails=true'+'&client_name='+nconf.get('client_name')

    console.log('getProductdetails', url)
    winston.log('info', 'getProductdetails API :' + url)
    var startTime = process.hrtime();
    let elapsed = null

    return request.get( url ).then( res => {
        elapsed = process.hrtime(startTime)[1] / 1000000;
        winston.log('warn', "Time taken to get  getProductdetails " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
        let ret = JSON.parse( res.text )
        if (ret&&ret.Results){
            ret.Results = ret.Results.Results
        }else{
            ret.Results=[]
        }
        return ret.Results
    })


},


    /**
     *
     */
    sendMessage = message => {

        // console.log( 'Sending message ', message )

        // Example argument:
        // Set not relevant fields as null
        // let message = {
        //   session_id  : session.userData.session_id,          // session id
        //   sender_type : 'bot',                                // one of [ 'user', 'agent', 'bot' ]
        //   user_psid   : null,                                 // If message sent by user, null otherwise
        //   agent_psid  : null,                                 // If message sent by agent, null otherwise
        //   bot_id      : session.message.address.bot.id,       // If message sent by bot, as given by MS Bot FW, null otherwise
        //   channel_id  : session.message.source,               // channel id
        //   client_organization : CLIENT_ORGANIZATION,          // Client org id
        //   msg_timestamp : new Date().toISOString(),           // ISO 8601 : e.g. '2017-04-23T09:35:10.818Z'
        //   text : promptText,                                  // Text of text or quick reply message
        //   carousel : [                                        // Array of cards, if the message is a carousel
        //     {
        //       title: '',
        //       subtitle: '',
        //       text: '',
        //       images : [ { image_url : 'string' } ],    // Array
        //       buttons : [ { button_type : '', button_value : '', button_title : '' } ]
        //     }
        //   ],
        //   quick_reply : [                   // If message is a quick reply message
        //     { type : 'string', value : 'string', title : 'string' }
        //   ]
        // }


        let url = HUMAN_AUGMENTATION_SERVER + '/Message/createMessage'

        console.log('Human Augmentation createMessage: ' + url)
        winston.log('info', 'Human Augmentation createMessage: ' + url)
        let startTime = process.hrtime()
        let elapsed = null

        return request.post(encodeURI(url)).send(message).then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get Human Augmentation createMessage API ' + url + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            // console.log( 'Response from ha service -> ', JSON.parse( res.text ) )
            return JSON.parse(res.text)
        })
            .catch(err => {

                // console.log( 'Error ', err.response, err.status )
                // console.log( err.response.request._data )
                // console.log( _.keys( err ), _.keys( err.response ), _.keys( err.response.request ), _.keys( err.response.res ), _.keys( err.response.req ) )
                if (err && err.status) {
                    // console.log( 'oh no ', err.status, err.response.body, err.response.text, err.response.clientError, err.response.serverError, err.response.error );
                    console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
                    console.log('HUMAN AUGMENTATION API ERROR: ', err.status, err.response.body, err.response.text, err.response.error)
                    console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
                } else {
                    // all other error types we handle generically
                    console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
                    console.log('HUMAN AUGMENTATION API ERROR : Server Not Responding')
                    console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
                }

            })

    }


module.exports = {

    registerNewAgent: registerNewAgent,
    loginAgent: loginAgent,
    logoutAgent: logoutAgent,
    getAllAvailableAgent: getAllAvailableAgent,
    getNextAvailableAgent: getNextAvailableAgent,
    getAllUsersWaitingInQueue: getAllUsersWaitingInQueue,
    addUserToWaitingQueue: addUserToWaitingQueue,
    removeUserFromWaitingQueue: removeUserFromWaitingQueue,
    addUserAndAgentToEngagedQueue: addUserAndAgentToEngagedQueue,
    removeUserAndAgentFromEngagedQueue: removeUserAndAgentFromEngagedQueue,
    getUserAccountStatus: getUserAccountStatus,
    getAgentAccountStatus: getAgentAccountStatus,
    getAgentUserQueue: getAgentUserQueue,
    getProductdetails:getProductdetails,
    sendMessage: sendMessage,

}