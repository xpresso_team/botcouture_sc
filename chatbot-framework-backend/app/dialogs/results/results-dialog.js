var _              = require( 'lodash'                       )
var builder        = require( 'botbuilder'                   )
var Constants      = require( '../../constants'              )
var TextService    = require( '../../services/TextService'   )
var StateService2  = require( '../../services/StateService2' )
var StateService  = require( '../../services/StateService' )
var ApiService     = require( '../../services/ApiService'    )
var BotService     = require( '../../services/BotService'   )
var ResultsService = require( './results-service'            )

module.exports = {

  Label: Constants.Labels.GuideMe,
  Dialog: [
    function ( session, args, next ) {

      session.sendTyping()

        //data          : { NODE : Constants.NODE.COLLECTIONS, resetState : true, state : { collectionType : collectionType, collectionName : collectionName, gender : [ gender ], entity : category } }

      let NODE = session.userData.currentNode
        if(!NODE){
            NODE = Constants.NODE.NLP
            session.userData.currentNode = NODE
        }

        if ( ! session.userData[ NODE ] ) {
            session.userData[ NODE ] = ResultsService.getDefaultState( NODE )
        }

      // Use NODE if specified
      if ( _.has( args, 'entities[0].data.NODE' ) ) {
        NODE = args.entities[0].data.NODE
        session.userData.currentNode = NODE
      }

      let toggle_gender = {flag:false,Old_gender:null,New_gender:null};

        if(NODE == Constants.NODE.COLLECTIONS && args && _.has( args, 'entities[0].data.state' ) ) {
            let gender = session.userData[ NODE ].gender.join(',');

            let gender_asked

            let query_type = args.entities[0].data.query_type

            if (args.entities[0].data.state.gender && args.entities[0].data.state.gender.length>0) {
                console.log("Taking gender from user data")
                gender_asked = args.entities[0].data.state.gender.join(',')
            }

            console.log("gender is ", gender)

            console.log("gender_asked is ", gender_asked)


            if ((gender_asked) && (gender) && (gender != "men,women") && (query_type == "OLD") ) {

                if (gender_asked != gender) {

                    toggle_gender.flag  = true
                    toggle_gender.Old_gender =  gender
                    toggle_gender.New_gender =  gender_asked

                }

            }
        }


      // console.log( 'BEFORE ', session.userData[ NODE ])

      // Merge incoming state is available
      if ( _.has( args, 'entities[0].data.state' ) ) {
        // Reset state if needed
        if ( ! session.userData[ NODE ] || args.entities[0].data.resetState ) {
          session.userData[ NODE ] = ResultsService.getDefaultState( NODE )
        }
        // Merge incoming state with existing state
        _.merge( session.userData[ NODE ], args.entities[0].data.state )
        // console.log( 'After MERGE: ', session.userData[ NODE ], args.entities[0].data )
      }


      ResultsService.getResults( session, NODE, session.userData[ NODE ],toggle_gender)
      .then( results => {
          //console.log("result obtained in get result is ",results)
          if(results == "Skip"){

              return Promise.resolve(results)

          }else {
              return ResultsService.processResults(session, results)
          }
      })
      .then( results => {

          if(results != "Skip"){

              ResultsService.displayResults( session, results )

          }
        session.endDialog()
      })
      .catch( err => {
        // TODO : ResultsService.displayError( NDOE, err )
          let promptObj = { id : null, buttons : [] }
          if (StateService2.checkResumeShopping(session)) {
              promptObj.buttons.push({
                  buttonText: 'Back To Shopping',
                  triggerIntent: 'RESULTS.DIALOG',
                  entityType: 'WISHLIST.BACK.TO.SHOPPING',
                  data: {}
              })
          }
          StateService.addPrompt2(session, promptObj)
          //console.log("postback replied")

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          buttonsText.push(Constants.Labels.Introduction)
          buttonsText.push(Constants.Labels.Help)
          BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )



        //BotService.sendQuickReplyButtons( session, 'Api failure message', [ Constants.Labels.Introduction, Constants.Labels.Help ] )
        // builder.Prompts.choice( session, TextService.text( 'Api failure message' ), [ Constants.Labels.Introduction, Constants.Labels.Help ], {
        //   maxRetries: 0,
        //   listStyle: builder.ListStyle.button
        // })
        session.error( err )
        session.endDialog()
      })

      session.endDialog()

    }]

}