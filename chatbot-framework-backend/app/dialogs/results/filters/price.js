var _            = require( 'lodash'                         )
var builder      = require( 'botbuilder'                     )
var Constants    = require( '../../../constants'             )
var ApiService   = require( '../../../services/ApiService'   )
var StateService = require( '../../../services/StateService' )
var StateService2 = require( '../../../services/StateService2' )
var UtilService  = require( '../../../services/UtilService'  )
var TextService  = require( '../../../services/TextService'  )
var BotService     = require( '../../../services/BotService'    )

module.exports = {
  Label: Constants.Labels.GuideMe,
  Dialog: [
  function ( session, args, next ) {

    session.sendTyping()

    // Fall through
    if ( !args || !args.entities || !( args.entities.length > 0 ) ) { next(); return; }

    let entityType = args.entities[0].entityType

    if ( 'RESULT.FILTERER.PRICE.TEXT' === entityType ) {

      let userInput = args.entities[0].data.userInput

      ApiService.getIntent( userInput,session ).then( result => {

        if ( ! result.price || result.price.length === 0 ) {

          let promptObj = {
            id : null,
            buttons : [{
              buttonText    : 'Back To Results',
              triggerIntent : 'RESULTS.DIALOG',
              entityType    : 'PRICE.FILTER.BACK.TO.RESULTS',
              data          : { }
            }]
          }

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          StateService.addPrompt2( session, promptObj )
          builder.Prompts.choice( session, 'Sorry I did not understand that, please try entering price again, or you can go back to results by tapping the button below.', buttonsText, {
            maxRetries: 0,
            listStyle: builder.ListStyle.button
          })
          return
        } else {
          // Got valid Price after Intent API
          let state = { price : result.price }
          let args = { entities : [ { entity : userInput, entityType : 'RESULTS.FILTERER', data : { state : state } } ] }
          next( { resumed : builder.ResumeReason.forward, args : args } )
          return
        }

      })
      .catch( err => {
        //session.send('Server Error')
          let promptObj = { id : null, buttons : [] }
          // if (StateService2.checkResumeShopping(session)) {
          //     promptObj.buttons.push({
          //         buttonText: 'Back To Shopping',
          //         triggerIntent: 'RESULTS.DIALOG',
          //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
          //         data: {}
          //     })
          // }
          StateService.addPrompt2(session, promptObj)
          //console.log("postback replied")

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          buttonsText.push(Constants.Labels.Introduction)
          buttonsText.push(Constants.Labels.Help)
          BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )

        /*builder.Prompts.choice( session, 'What would you like to do?', [ Constants.Labels.Introduction, Constants.Labels.Help ], {
          maxRetries: 0,
          listStyle: builder.ListStyle.button
        })*/
        session.error( err )
      })


    }

  },
  function ( session, result , next ) {

    session.sendTyping()

    if ( result.resumed === builder.ResumeReason.completed ) {
      // User pressed 'Back To Results' prompt button, all info is in the button
      next()
      return
    }
    else if ( result.resumed === builder.ResumeReason.notCompleted ) {
      // User entered price text again, instead of 'Back To Results'
      if ( session.message.text ) {
        let args = { entities : [ { entity : session.message.text, entityType : 'RESULT.FILTERER.PRICE.TEXT', data : { userInput : session.message.text } } ] }
        session.replaceDialog( 'xpresso-bot:results-filters-price', args )
        return
      } else {
        // No user text, maybe user uploaded an image, let it go
        next()
        return
      }
      
    }
    else if ( result.resumed === builder.ResumeReason.forward ) {
      // console.log(result.args.entities, result.args.entities[0].data)
      // User entered a valid price text 
      session.replaceDialog( 'xpresso-bot:results-dialog', result.args )
      return
    }
    else if ( result.resumed === builder.ResumeReason.forward ) {
      // Falling trhough
      next()
      return
    }
  
  },
  function ( session, result ) {
    session.reset()
  }]

}