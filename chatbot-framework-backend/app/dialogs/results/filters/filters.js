var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../../constants')
var ApiService = require('../../../services/ApiService')
var StateService = require('../../../services/StateService')
var StateService2 = require('../../../services/StateService2')
var UtilService = require('../../../services/UtilService')
var TextService = require('../../../services/TextService')
var UserService = require('../../../services/UserService')
var BotService = require('../../../services/BotService')
var toTitleCase = require('to-title-case')
var ResultsService = require('../results-service')
var nconf = require('nconf')


module.exports = {
    Label: Constants.Labels.GuideMe,
    Dialog: [
        function (session, args, next) {

            // Fall through
            if (!args) {
                next();
                return;
            }
            session.sendTyping()

            let entity = args.entities[0].entity
            let entityType = args.entities[0].entityType

            let filterOn = args.entities[0].data.filterOn         // brand/color/price
            let NODE = args.entities[0].data.NODE
            let state = args.entities[0].data.state
            console.log("\n")
            console.log("the state is ",state)
            console.log("\n")
            let guestMode = args.entities[0].data.guestMode

            let pageSize = 9 + 2
            let pageNo = args.entities[0].data.pageNo || 0      // Page No of the quick reply buttons

            // Make the current attribute being filtered null
            let tmpState = _.cloneDeep(state)
            tmpState[filterOn] = null

            // Get results to get unique attributes from the results
            let structuredSearch = ResultsService.getResults(session, NODE, tmpState)

            // If this is BRAND or SIZE filter, get user's profile
            let getUserProfile = null

            console.log("In filter temo state is ",tmpState)
            console.log("In filter filterOn is ",filterOn)

            //if (!guestMode && ( filterOn === 'brand' || filterOn === 'size' )) {
            if (!guestMode && ((filterOn != 'price')||(filterOn != 'Shop'))) {
                getUserProfile = UserService.getUserProfileByEntity(session.message.address, state.entity,session.userData.Profile_gender)
            } else {
                getUserProfile = Promise.resolve(null)
            }

            Promise.all([structuredSearch, getUserProfile]).then(([result, userProfile]) => {

                // if(userProfile) {

                console.log("user Profile is ", userProfile)

                let profile_gender
                if (userProfile && userProfile.user){
                    profile_gender = userProfile.user.gender
                    profile_gender === 'men,women' ? "no_preference" : profile_gender
                }
                //}

                if(profile_gender) {
                    session.userData.Profile_gender = profile_gender
                }


                result.Results = result.Results || []
                if (result.Results.length === 0) {
                    result.Results = result["Suggested Results"] || []
                }

                if (result.Results.length > 0) {

                    //console.log("result.Results is ",result.Results)

                    // TODO: Refactor the following
                    let attributeList = []

                    // For string type attributes
                    if (filterOn === 'brand') {
                        attributeList = _(result.Results).filter(item => item[filterOn]).map(item => item[filterOn]).uniq().value()
                    }

                    // For Array type attributes
                    if (filterOn === 'colorsavailable' || filterOn === 'size') {
                        // filter falsy, flat map the attribute array, get uniques

                        //console.log("result.Results is ",result.Results[0],result.Results[1])

                        attributeList = _(result.Results).filter(item => item[filterOn]).flatMap(item => {

                            if (Array.isArray(item[filterOn])) {

                                //console.log("is array")
                                //console.log("item[filterOn] ",item[filterOn])
                                //console.log("item ",item)
                                //console.log("filterOn ",filterOn)
                                // Collections send Array
                                return item[filterOn]
                            } else {
                                // Structured Search sends json
                                // TODO URGENT: Remove this hack, same in UtilService.hasDistinctAttributes()
                                // Vision api sends python unicode strings ( e.g: '[u'black']' )which is invalid json
                                /*
                                 // Replace the u' with '
                                 item[ filterOn ] = item[ filterOn ].replace( /u'(?=[^:]+')/g, "'" )
                                 // Replace \' with TEMPORARY_STRING
                                 item[ filterOn ] = item[ filterOn ].replace( /\\'/g, 'TEMPORARY_STRING' )
                                 // Replace ' with "
                                 item[ filterOn ] = item[ filterOn ].replace( /'/g, '"' )
                                 // Replace TEMPORARY_STRING with \'
                                 item[ filterOn ] = item[ filterOn ].replace( /TEMPORARY_STRING/g, "\\'" )
                                 */
                                //console.log("item[filterOn] ",item[filterOn])
                                //console.log("item ",item)
                                //console.log("filterOn ",filterOn)
                                return JSON.parse(item[filterOn])
                            }
                        }).uniq().value()
                    }


                    let promptObj = {id: null, buttons: []}
                    // Redirect user to save filter dialog except if the filter is already saved in his profile
                    if (filterOn === 'brand') {
                        let pageSize = 10 + 2     // Brands are shown in a carousel (which has 10 slots) and not quick replies (which has 11 slots)

                        var cards = []
                        for (let i = 0; i < attributeList.length; i++) {
                            let postback = (userProfile && _.includes(['brand', 'colorsavailable','size'], filterOn) && !_.includes(userProfile[filterOn], attributeList[i]) ) ? 'RESULTS.SAVE.FILTER' : 'RESULTS.DIALOG'
                            let intent = {
                                intent: postback,
                                entities: [{
                                    entityType: 'RESULTS.FILTERER',
                                    data: {
                                        NODE: NODE,
                                        state: _.cloneDeep(state),
                                        filterOn: filterOn,
                                        attributeValue: attributeList[i],
                                        guestMode: guestMode
                                    }
                                }]
                            }
                            intent.entities[0].data.state[filterOn] = [attributeList[i]]    // <- This is where the filter is applied
                            intent.entities[0].data.state.pageNo = 0

                            let PostBack = 'POSTBACK::' + JSON.stringify(intent)
                            let card = new builder.HeroCard(session)
                                .title(toTitleCase(attributeList[i]))
                                .images([
                                    builder.CardImage.create(session, nconf.get('brand_url') + encodeURIComponent(attributeList[i]))
                                ])
                                .buttons([
                                    builder.CardAction.postBack(session, PostBack, toTitleCase(attributeList[i])),
                                ])
                            cards.push(card)
                            //console.log("\n this card is ",card)
                            //console.log("\n")
                        }

                        cards = UtilService.pageList(cards, pageNo, pageSize, null, null)
                        BotService.sendCard(session, cards)

                        let showNext = {
                            buttonText: 'Next Page',
                            triggerIntent: 'RESULTS.FILTERS',
                            entityType: 'RESULTS.FILTERER.NEXTPAGE',
                            data: {
                                filterOn: filterOn,
                                NODE: NODE,
                                state: _.cloneDeep(state),
                                pageNo: pageNo + 1,
                                guestMode: guestMode
                            }
                        }

                        let showPrevious = {
                            buttonText: 'Previous Page',
                            triggerIntent: 'RESULTS.FILTERS',
                            entityType: 'RESULTS.FILTERER.PREVIOUSPAGE',
                            data: {
                                filterOn: filterOn,
                                NODE: NODE,
                                state: _.cloneDeep(state),
                                pageNo: pageNo - 1,
                                guestMode: guestMode
                            }
                        }

                        showNext = ( pageSize - 2 ) * ( pageNo + 1) < attributeList.length ? showNext : null
                        showPrevious = pageNo !== 0 && session.message.address.channelId === 'facebook' ? showPrevious : null

                        if (showPrevious) {
                            promptObj.buttons.push(showPrevious)
                        }
                        if (showNext) {
                            promptObj.buttons.push(showNext)
                        }
                        if (showNext || showPrevious) {
                            StateService.addPrompt2(session, promptObj)
                            let buttons = _.map(promptObj.buttons, button => button.buttonText)
                            BotService.sendQuickReplyButtons(session, 'Guide me Filter message', buttons)
                        }

                    } else {

                     //  console.log("attributeList is", attributeList)
                        _.each(attributeList, attribute => {
                            let tmpButton = {
                                buttonText: toTitleCase(attribute),
                                triggerIntent: ( userProfile && _.includes(['brand', 'colorsavailable','size'], filterOn) && !_.includes(userProfile[filterOn], attribute) ) ? 'RESULTS.SAVE.FILTER' : 'RESULTS.DIALOG',
                                entityType: 'RESULTS.FILTERER',
                                data: {
                                    NODE: NODE,
                                    state: _.cloneDeep(state),
                                    filterOn: filterOn,
                                    attributeValue: attribute,
                                    guestMode: guestMode
                                }
                            }
                            //tmpButton.data.state[ filterOn ] = [ _.toLower( attribute ) ]    // <- This is where the filter is applied
                            tmpButton.data.state[filterOn] = [attribute]    // <- This is where the filter is applied
                            tmpButton.data.state.pageNo = 0
                            promptObj.buttons.push(tmpButton)
                        })

                        let showNext = {
                            buttonText: 'Next Page',
                            triggerIntent: 'RESULTS.FILTERS',
                            entityType: 'RESULTS.FILTERER.NEXTPAGE',
                            data: {
                                filterOn: filterOn,
                                NODE: NODE,
                                state: _.cloneDeep(state),
                                pageNo: pageNo + 1,
                                guestMode: guestMode
                            }
                        }

                        let showPrevious = {
                            buttonText: 'Previous Page',
                            triggerIntent: 'RESULTS.FILTERS',
                            entityType: 'RESULTS.FILTERER.PREVIOUSPAGE',
                            data: {
                                filterOn: filterOn,
                                NODE: NODE,
                                state: _.cloneDeep(state),
                                pageNo: pageNo - 1,
                                guestMode: guestMode
                            }
                        }

                        showNext = ( pageSize - 2 ) * ( pageNo + 1) < attributeList.length ? showNext : null
                        showPrevious = pageNo !== 0 && session.message.address.channelId === 'facebook' ? showPrevious : null

                        promptObj.buttons = UtilService.pageList(promptObj.buttons, pageNo, pageSize, showPrevious, showNext)

                        StateService.addPrompt2(session, promptObj)
                        let buttons = _.map(promptObj.buttons, button => button.buttonText)
                        BotService.sendQuickReplyButtons(session, 'Guide me Filter message', buttons)

                    }

                    session.endDialog()

                } else {
                    //No results found
                    BotService.sendText(session, 'No results found!')

                    let promptObj = {id: null, buttons: []}
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }
                    StateService.addPrompt2(session, promptObj)
                    //console.log("postback replied")

                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons(session, {text: 'Intro option'}, buttonsText)
                    session.endDialog()
                }

            })
                .catch(err => {

                    let promptObj = {id: null, buttons: []}
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }
                    StateService.addPrompt2(session, promptObj)
                    //console.log("postback replied")

                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)

                    session.error(err)
                    session.endDialog()
                })

            session.endDialog()

        }]

}