var _            = require( 'lodash'                         )
var builder      = require( 'botbuilder'                     )
var Constants    = require( '../../../constants'             )
var ApiService   = require( '../../../services/ApiService'   )
var StateService = require( '../../../services/StateService' )
var UtilService  = require( '../../../services/UtilService'  )
var TextService  = require( '../../../services/TextService'  )
var BotService   = require( '../../../services/BotService'   )
var toTitleCase  = require( 'to-title-case'                  )

/**
 * Prompts user to save the selected SIZE or BRAND filter
 */
module.exports = {
  Label: Constants.Labels.GuideMe,
  Dialog: [
  function ( session, args, next ) {

    // Fall through
    if ( !args ) { next(); return; }
    session.sendTyping()

    let entity         = args.entities[0].entity
    let entityType     = args.entities[0].entityType

    let NODE           = args.entities[0].data.NODE
    let state          = args.entities[0].data.state
    let filterOn       = args.entities[0].data.filterOn         // BRAND/SIZE
    let attributeValue = args.entities[0].data.attributeValue

    let entityName     = state.entity || 'product'

    let promptObj = { id : null, buttons : [] }
    let saveIt = {
        buttonText    : 'Save it!',
        triggerIntent : 'RESULTS.SAVE.FILTER.YES',
        entityType    : 'RESULTS.FILTERER',
        data          : { NODE : NODE, state : _.cloneDeep( state ), entityName : entityName, filterOn : filterOn, attributeValue : attributeValue }
    }
    let noThanks = {
      buttonText    : 'No Thanks',
      triggerIntent : 'RESULTS.SAVE.FILTER.NO',
      entityType    : 'RESULTS.FILTERER',
      data          : { NODE : NODE, state : _.cloneDeep( state ), filterOn : filterOn, attributeValue : attributeValue }
    }

    promptObj.buttons.push( saveIt   )
    promptObj.buttons.push( noThanks )
    StateService.addPrompt2( session, promptObj )

    let buttons = _.map( promptObj.buttons, button => button.buttonText )

    //BotService.sendQuickReplyButtons( session, { text : 'Type a Query Confirm Save Filter', data : { attributeValue : attributeValue , entityName : entityName, filter : "Shop" } }, buttons )
    BotService.sendQuickReplyButtons( session, { text : 'Type a Query Confirm Save Filter', data : { attributeValue : attributeValue , entityName : entityName, filter : filterOn } }, buttons )
    // builder.Prompts.choice( session, TextService.text( 'Type a Query Confirm Save Filter', { attributeValue : toTitleCase( attributeValue ), entityName : entityName, filter : filterOn } ) , buttons, {
    //   maxRetries: 0,
    //   listStyle: builder.ListStyle.button
    // })
    session.endDialog()

  }]

}
