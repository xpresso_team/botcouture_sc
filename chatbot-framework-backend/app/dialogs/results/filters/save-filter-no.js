var _            = require( 'lodash'                         )
var builder      = require( 'botbuilder'                     )
var Constants    = require( '../../../constants'             )
var ApiService   = require( '../../../services/ApiService'   )
var StateService = require( '../../../services/StateService' )
var StateService2 = require( '../../../services/StateService2' )
var UtilService  = require( '../../../services/UtilService'  )
var TextService  = require( '../../../services/TextService'  )
var UserService  = require( '../../../services/UserService'  )
var BotService   = require( '../../../services/BotService'   )

/**
 * Message after succesfully saving a filter 
 */
module.exports = {
  Label: Constants.Labels.GuideMe,
  Dialog: [
  function ( session, args, next ) {
    
    // Fall through
    if ( ! args ) { next(); return; }
    session.sendTyping()

    let entity         = args.entities[0].entity
    let entityType     = args.entities[0].entityType
    
    let NODE           = args.entities[0].data.NODE
    let state          = args.entities[0].data.state
    let filterOn       = args.entities[0].data.filterOn         // Brand/Size
    let attributeValue = args.entities[0].data.attributeValue

    // let entityType     = args.entities[0].entityType
    // let filterOn       = args.entities[0].data.filterOn         // Brand/Size
    // let attributeValue = args.entities[0].data.attributeValue
    // let userData       = args.entities[0].data.userData

    let userProfile = {}
    userProfile[ filterOn ] = [ attributeValue ]                 // SIZE and BRAND are arrays

    UserService.saveUsersNegativePreference( session.message.address, userProfile )
    .then( result => {
      
      let args = {
        intent   : 'RESULTS.DIALOG',
        score    : 1,
        entities : [ { 
          entityType : 'RESULTS.FILTERER', 
          data       : { NODE : NODE, state : state }
        } ]
      }

      session.replaceDialog( 'xpresso-bot:results-dialog', args )
      session.endDialog()

    })
    .catch( err => {

        let promptObj = { id : null, buttons : [] }
        if (StateService2.checkResumeShopping(session)) {
            promptObj.buttons.push({
                buttonText: 'Back To Shopping',
                triggerIntent: 'RESULTS.DIALOG',
                entityType: 'WISHLIST.BACK.TO.SHOPPING',
                data: {}
            })
        }
        StateService.addPrompt2(session, promptObj)
        //console.log("postback replied")

        let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
        buttonsText.push(Constants.Labels.Introduction)
        buttonsText.push(Constants.Labels.Help)
        BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
      session.error( err )
      session.endDialog()
    })
    
    session.endDialog()

  }]

}