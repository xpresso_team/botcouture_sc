/**
 * Created by aashish_amber on 13/3/17.
 */
var builder = require('botbuilder')
var Constants = require('../constants')
var TextService = require('../services/TextService')
var request = require('superagent')
var BotService = require('../services/BotService')
var _ = require('lodash')
var StateService = require('../services/StateService')
var StateService2 = require('../services/StateService2')
var UserService = require('../services/UserService')


module.exports = {

    Label: Constants.Labels.FAQ,

    Dialog: [
        function (session, args, next) {
            // Fall through
            if (!args || !args.entities || !( args.entities.length > 0 )) {
                next();
                return;
            }
            //UserService.setActiveState( session.message.address, true)

            BotService.sendText(session, "Question :" + args.entities[0].data.FAQ)
            BotService.sendText(session, "Ans :" + TextService.FAQS(args.entities[0].data.FAQ))
            let promptObj = {id: null, buttons: []}
            if (StateService2.checkResumeShopping(session)) {
                promptObj.buttons.push({
                    buttonText: 'Back To Shopping',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    data: {}
                })
            }
            StateService.addPrompt2(session, promptObj)
            console.log("postback replied")

            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
            buttonsText.push(Constants.Labels.Introduction)
            BotService.sendQuickReplyButtons(session, {text: 'Help option'}, buttonsText)

        },
        function (session, result) {
            session.reset()
            //session.replaceDialog( 'start' )
        }
    ]
}