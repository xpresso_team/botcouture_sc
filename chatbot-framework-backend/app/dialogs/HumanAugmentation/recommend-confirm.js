var _            = require( 'lodash'                      )
const nconf   = require( 'nconf'               )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var UserService = require( '../../services/UserService' )
var TextService  = require( '../../services/TextService'  )
var BotService   = require( '../../services/BotService'   )
var HumanAugmentationService = require('../../services/human-augmentation-service')
var HumanHelpService = require('./human-help-service')

module.exports = {
    Dialog: [
        function ( session,args,next ) {


            HumanHelpService.unsetWaitTimer(session.message.address)

            UserService.setActiveState( session.message.address, true)

            session.sendTyping()

            let userName    = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.address.channelId
            let firstName   = userName.split( ' ' )[0]
            let LastName    = userName.split( ' ' )[1]
            let HA_CLIENT_ORGANIZATION = nconf.get( "HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION" )
            let psid = session.message.address.user.id
            let data

            console.log("The dialog called by ",userName)

            console.log(JSON.stringify(args))

            if (_.has(args, 'entities[0].data.timeout')) {

                HumanHelpService.unsetWaitTimer(session.message.address)

                let promptObj = {id: null, buttons: []}
                promptObj.buttons.push({
                    buttonText: 'Yes',
                    triggerIntent: 'STOP.CONFIRM',
                    entityType: '',
                    data: {wait: false}
                })
                promptObj.buttons.push({
                    buttonText: 'No',
                    triggerIntent: 'STOP.CONFIRM',
                    entityType: '',
                    data: {wait: true}
                })
                StateService.addPrompt2(session, promptObj)
                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                BotService.sendQuickReplyButtons(session, {
                    text: 'Chat ending confirmation from agent'
                }, buttonsText)
                HumanHelpService.setWaitTimer(session.message.address, session, 'xpresso-bot:stop-confirm', nconf.get("HA_response_timeout"), {timeout: true})
                HumanHelpService.Add_user(psid, session)

                HumanHelpService.return_user(session.userData.user_engaged_queue.id).sendTyping()
                BotService.sendText( HumanHelpService.return_user(session.userData.user_engaged_queue.id), { text : 'Chat ending conf sent' } )
                session.endDialog()

            } else {

                let user_wait

                if (_.has(args, 'entities[0].data.wait')) {
                    user_wait = args.entities[0].data.wait
                    //user_wait = args.entities[0].data.user_wait
                    console.log("user match user wait is ",user_wait)
                } else {

                    console.log("********* NO DATA **************")
                    session.endDialog()
                    return;
                }


                if (user_wait) {

                    HumanHelpService.unsetWaitTimer(session.message.address)

                    let promptObj = {id: null, buttons: []}
                    promptObj.buttons.push({
                        buttonText: 'Yes',
                        triggerIntent: 'STOP.CONFIRM',
                        entityType: '',
                        data: {wait: false}
                    })
                    promptObj.buttons.push({
                        buttonText: 'No',
                        triggerIntent: 'STOP.CONFIRM',
                        entityType: '',
                        data: {wait: true}
                    })
                    StateService.addPrompt2(session, promptObj)
                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    BotService.sendQuickReplyButtons(session, {
                        text: 'Chat ending confirmation from agent'
                    }, buttonsText)

                    HumanHelpService.setWaitTimer(session.message.address, session, 'xpresso-bot:stop-confirm', nconf.get("HA_response_timeout"), {timeout: true})
                    HumanHelpService.Add_user(psid, session)

                    HumanHelpService.return_user(session.userData.user_engaged_queue.id).sendTyping()
                    BotService.sendText( HumanHelpService.return_user(session.userData.user_engaged_queue.id), { text : 'Chat ending conf sent'} )
                    session.endDialog()

                } else {

                    HumanHelpService.unsetWaitTimer(session.message.address)

                    BotService.sendText(session, 'Not ending chat message')

                    let postBackButtons = []
                    let intent = {
                        intent: 'HA.STOP',
                        entities: [{
                            data: {mode : "user"}
                        }]
                    }

                    let PostBack = 'POSTBACK::' + JSON.stringify(intent)

                    postBackButtons.push({
                        buttonText: 'STOP',
                        postback: PostBack
                    })
                    let cardButtons = _.map(postBackButtons, button => builder.CardAction.postBack(session, button.postback, button.buttonText))
                    let card = new builder.HeroCard(session).title(TextService.text('Button Template Title')[0]).buttons(cardButtons)
                    BotService.sendCard(session, [card])
                    BotService.sendText( HumanHelpService.return_user(session.userData.user_engaged_queue.id), { text : 'Conv resume msg to HA',data : { name : firstName+" "+LastName }} )

                    session.userData.user_engaged_queue.flag = true
                    session.userData.user_engaged_queue.wait = false
                    HumanHelpService.Add_user(psid, session)

                    let session2 = HumanHelpService.return_user(session.userData.user_engaged_queue.id )
                    session2.userData.agent_wait_queue = false
                    session2.userData.user_engaged_queue = {}
                    session2.userData.user_engaged_queue.flag = true
                    session2.userData.user_engaged_queue.wait = false
                    session2.userData.user_engaged_queue.id = session.message.address.user.id
                    session2.userData.user_engaged_queue.name = userName

                    console.log("data updated for agent")
                    //console.log(JSON.stringify(session2.userData))
                    //console.log(JSON.stringify(session2.message.address))
                    console.log("agent id is ",session.userData.user_engaged_queue.id)

                    HumanHelpService.Add_user(session.userData.user_engaged_queue.id , session2)

                    session.endDialog()

                }
                session.endDialog()
            }

            session.endDialog()

        }
    ]
};