var _            = require( 'lodash'                      )
const nconf   = require( 'nconf'               )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var UserService = require( '../../services/UserService' )
var TextService  = require( '../../services/TextService'  )
var BotService   = require( '../../services/BotService'   )
var HumanAugmentationService = require('../../services/human-augmentation-service')
var HumanHelpService = require('./human-help-service')

module.exports = {
    Dialog: [
        function ( session,args,next ) {

            UserService.setActiveState( session.message.address, true)

            let userName    = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.address.channelId
            let firstName   = userName.split( ' ' )[0]
            let LastName    = userName.split( ' ' )[1]
            let HA_CLIENT_ORGANIZATION = nconf.get( "HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION" )
            let psid = session.message.address.user.id
            let user_type
            let data


                if (_.has(args, 'entities[0].data.mode')) {
                    user_type = args.entities[0].data.mode
                } else {
                    console.log("********* NO DATA **************")
                    session.endDialog()
                    return;
                }


            if(_.has(session, 'userData.status')){
                if(session.userData.status == "agent"){
                    user_type = "agent"

                }else if ((session.userData.status == "user")&&(session.userData.user_engaged_queue.flag)){
                    user_type = "user"
                }else{
                    session.sendTyping()
                    let promptObj = {id: null, buttons: []}
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }
                    StateService.addPrompt2(session, promptObj)
                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons(session, {
                        text: 'Not authorised'
                    }, buttonsText)
                    session.endDialog()
                    return;
                }
            }



                if (user_type == "user") {
                    session.sendTyping()

                    let promptObj = {id: null, buttons: []}
                    promptObj.buttons.push({
                        buttonText: 'Yes',
                        triggerIntent: 'STOP.CONFIRM',
                        entityType: '',
                        data: {wait: true}
                    })
                    promptObj.buttons.push({
                        buttonText: 'No',
                        triggerIntent: 'STOP.CONFIRM',
                        entityType: '',
                        data: {wait: false}
                    })
                    StateService.addPrompt2(session, promptObj)
                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    BotService.sendQuickReplyButtons(session, {
                        text: 'Chat ending confirmation'
                    }, buttonsText)
                    HumanHelpService.setWaitTimer(session.message.address, session, 'xpresso-bot:stop-confirm', nconf.get("HA_response_timeout"), {timeout: true})
                    session.endDialog()

                }else if (user_type == "agent") {

                    data = {
                        channel_id: channelName,
                        psid:psid,
                        client_organization: HA_CLIENT_ORGANIZATION,
                    }

                    let getAgentAccountStatus = HumanAugmentationService.getAgentAccountStatus(data)

                    Promise.all([getAgentAccountStatus])
                        .then(([AgentAccountStatus]) => {
                            if (((AgentAccountStatus.agent_queue) && (AgentAccountStatus.agent_queue.length > 0)) ) {
                                session.sendTyping()
                                BotService.sendText(session, {
                                    text: 'Not a legal command stop'
                                })

                                session.endDialog()

                            }else if (((AgentAccountStatus.agent_user_queue) && (AgentAccountStatus.agent_user_queue.length > 0))){


                                let session2 = HumanHelpService.return_user(session.userData.user_engaged_queue.id)

                                session2.sendTyping()

                                let promptObj = {id: null, buttons: []}
                                promptObj.buttons.push({
                                    buttonText: 'Yes',
                                    triggerIntent: 'STOP.CONFIRM',
                                    entityType: '',
                                    data: {wait: false}
                                })
                                promptObj.buttons.push({
                                    buttonText: 'No',
                                    triggerIntent: 'STOP.CONFIRM',
                                    entityType: '',
                                    data: {wait: true}
                                })
                                StateService.addPrompt2(session2, promptObj)
                                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                                BotService.sendQuickReplyButtons(session2, {
                                    text: 'Chat ending confirmation from agent'
                                }, buttonsText)
                                HumanHelpService.setWaitTimer(session2.message.address, session2, 'xpresso-bot:stop-confirm', nconf.get("HA_response_timeout"), {timeout: true})
                                session2.endDialog()
                                console.log("session 2 dialog end")
                                HumanHelpService.Add_user(session.userData.user_engaged_queue.id, session2)

                                session.sendTyping()
                                BotService.sendText( session, { text : 'Chat ending conf sent'} )
                                session.endDialog()


                                // Code to directly end the chat

                               /* data = {
                                    channel_id          : channelName    ,
                                    user_psid           : session.userData.user_engaged_queue.id   ,
                                    agent_psid          : psid  ,
                                    client_organization : HA_CLIENT_ORGANIZATION,
                                }

                                //console.log("EngagedQueue data is ",JSON.stringify(data))

                                HumanAugmentationService.removeUserAndAgentFromEngagedQueue(data)
                                    .then(results => {

                                        let session2 = HumanHelpService.return_user(session.userData.user_engaged_queue.id)

                                        session2.sendTyping()
                                        BotService.sendText(session2, {
                                            text: 'Talk end',
                                            data: {name: session.userData.user_engaged_queue.name , name2: firstName+" "+LastName }
                                        })

                                        let cards =[]
                                        let card = new builder.HeroCard( session )
                                            .buttons([
                                                builder.CardAction.postBack( session, 'Feedback', 'Feedback' ),
                                            ])
                                        cards.push(card)
                                        BotService.sendCard( session2, cards )

                                        session2.userData.user_wait_queue = null
                                        session2.userData.user_engaged_queue = null
                                        session2.userData.status = ""
                                        HumanHelpService.Remove_user( session2.message.address.user.id)

                                        session.sendTyping()

                                        BotService.sendText( session, { text : 'Talk end agent msg',data : { name : session.userData.user_engaged_queue.name }} )

                                        session.userData.agent_wait_queue = true
                                        session.userData.user_engaged_queue = null
                                        HumanHelpService.Add_user(session.message.address.user.id, session)
                                        session.replaceDialog('xpresso-bot:human-augmentation', {entities:[{entity: null, data:{user_type :"agent"} }]})
                                        session.endDialog()

                                    })
                                    .catch(err => {

                                        session.sendTyping()

                                        //console.log("some error happened in addUserAndAgentToEngagedQueue login")
                                        let msg
                                        console.log(err)

                                        //if (err && err.status) {

//                                            msg = err.response.text

  //                                      } else {
                                            msg = TextService.text('Api failure message')
    //                                    }


                                        let buttonsText = []
                                        buttonsText.push(Constants.Labels.Help)
                                        BotService.sendQuickReplyButtons(session, msg
                                            , buttonsText)

                                        session.error(err)
                                        session.endDialog()
                                    })
*/

                            }else{
                                let msg
                                session.sendTyping()
                                msg = TextService.text('Not authorised')
                                let buttonsText = []
                                buttonsText.push(Constants.Labels.Help)
                                BotService.sendQuickReplyButtons(session, msg
                                    , buttonsText)

                                session.error(err)
                                session.endDialog()

                            }

                            session.endDialog()

                        })
                        .catch(err => {

                            console.log("some error happened in addUserAndAgentToEngagedQueue login",err)
                            session.sendTyping()

                            let msg
                            //console.log(msg)

                            //if (err && err.status) {

                            //msg = err.response.text

                            //} else {
                            msg = TextService.text('Not authorised')
                            //}


                            let buttonsText = []
                            buttonsText.push(Constants.Labels.Help)
                            BotService.sendQuickReplyButtons(session, msg
                                , buttonsText)

                            session.error(err)
                            session.endDialog()
                        })
                    session.endDialog()
                }

            session.endDialog()

        }
    ]
};