var _ = require('lodash')
const HashMap = require('hashmap')
var builder = require('botbuilder')
var Constants = require('../../constants')
var request = require('superagent')
var nconf = require('nconf')
const uuidV4  = require( 'uuid/v4' )
var winston = require('../../services/loggly/LoggerUtil')



let user_session_map = new HashMap()

let userWaitList = []



Add_user = (ID, session) => {



    //console.log("ID ", ID)
//console.log("session ", session)
//console.log("has id ", user_session_map.has(ID))

    //let id = user_session_map.get(ID);

    //if(!(user_session_map.has(ID))){

    user_session_map.set(ID, session);

    //}


    //console.log("has id ", user_session_map.has(ID))
    //console.log("ID ", ID)
    //console.log("session saved is ", user_session_map.get(ID))

}

return_user = (ID) => {




    //let id = dummy_list.get(ID);

    //console.log("list is  ",user_session_map)
    //console.log("has id ", user_session_map.has(ID))
    //console.log("retrived session ",user_session_map.get(ID))

    return user_session_map.get(ID);


}

Remove_user = (ID) => {




    //let id = dummy_list.get(ID);

    //console.log("list is  ",user_session_map)
    //console.log("has id ", user_session_map.has(ID))
    //console.log("retrived session ",user_session_map.get(ID))

     user_session_map.remove(ID);


}

has_user = (ID) => {



    return user_session_map.has(ID);


}





/**
 * Clear and reset timer, essentially extending feedback timeout after every user message
 */
setWaitTimer = (address, session,dialog,timer,data) => {

    let id = _.findIndex(userWaitList, {channelId: address.channelId, userId: address.user.id})

    //console.log("id is " ,id)

    if (id < 0) {

        let user = {
            channelId: address.channelId,
            userId: address.user.id,
            name: address.user.name || 'User',
            active: false,
            userWaitTimer: null,
        }

        userWaitList.push(user)
        id = userWaitList.length - 1
    }

    clearTimeout(userWaitList[id].userWaitTimer)



    userWaitList[id].userWaitTimer = setTimeout(function () {
        //bot.send( { address: address, text: 'YOLO!!!', type: 'message', user: address.user})
        console.log("response time out happened")
        session.beginDialog(dialog, {entities:[{entity: null, data:data }]})
    }, timer * 60000)

    //console.log("time out set for ",userWaitList[id])

}

/**
 * Remove feedback timer
 */
unsetWaitTimer = address => {
    let id = _.findIndex(userWaitList, {channelId: address.channelId, userId: address.user.id})

    //console.log("for timeout unset id is ",id)
    if (id > -1) {
        //console.log("timeout unset done")
        //console.log(userWaitList[id])
        clearTimeout(userWaitList[id].userWaitTimer)
    }
}

module.exports = {
    setWaitTimer: setWaitTimer,
    unsetWaitTimer: unsetWaitTimer,
    Add_user:Add_user,
    has_user:has_user,
    Remove_user:Remove_user,
    return_user :return_user,
    user_session_map :user_session_map
}
