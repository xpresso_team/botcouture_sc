const _ = require('lodash')
const nconf = require('nconf')
var builder = require('botbuilder')
var Constants = require('../../constants')
var TextService = require('../../services/TextService')
var StateService2 = require('../../services/StateService2')
var StateService = require('../../services/StateService')
var ApiService = require('../../services/ApiService')
var UserService = require('../../services/UserService')
var BotService = require('../../services/BotService')
var HumanAugmentationService = require('../../services/human-augmentation-service')
var HumanHelpService = require('./human-help-service')

module.exports = {

    Dialog: [
        function (session, args, next) {

            let command
            let data
            let user_type

            console.log("special command ", JSON.stringify(args))

            UserService.setActiveState(session.message.address, true)

            //console.log(JSON.stringify(args))


            if (_.has(args, 'entities[0].data.command')) {
                command = args.entities[0].data.command
            } else {
                next();
                return;
            }

            if (_.has(session, 'userData.status')) {
                if (session.userData.status == "agent") {
                    user_type = "agent"

                } else if (session.userData.status == "user") {
                    user_type = "user"
                } else {
                    user_type = null
                }
            }


            let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.address.channelId
            let firstName = userName.split(' ')[0]
            let LastName = userName.split(' ')[1]
            let HA_CLIENT_ORGANIZATION = nconf.get("HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION")
            let psid = session.message.address.user.id


            switch (command) {
                case 'register' :

                    if ((user_type === "user")) {
                        session.sendTyping()

                        if (session.userData.user_engaged_queue.flag || session.userData.user_engaged_queue.wait) {
                            BotService.sendText(session, {text: 'Not authorised'})
                            session.endDialog()
                        } else if (session.userData.user_wait_queue) {
                            let promptObj = {id: null, buttons: []}
                            if (StateService2.checkResumeShopping(session)) {
                                promptObj.buttons.push({
                                    buttonText: 'Back To Shopping',
                                    triggerIntent: 'RESULTS.DIALOG',
                                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                    data: {}
                                })
                            }
                            StateService.addPrompt2(session, promptObj)
                            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                            buttonsText.push(Constants.Labels.Introduction)
                            buttonsText.push(Constants.Labels.Help)
                            BotService.sendQuickReplyButtons(session, {
                                text: 'Not authorised'
                            }, buttonsText)
                            session.endDialog()
                        } else {
                            console.log("User should not have used this command")
                            BotService.sendText(session, {text: 'Not authorised'})
                            session.endDialog()
                        }
                        session.endDialog()
                    }
                    else {


                        data = {
                            agent_name: firstName + " " + LastName,
                            channel_id: channelName,
                            psid: psid,
                            client_organization: HA_CLIENT_ORGANIZATION,
                        }

                        //console.log("agent data is ", JSON.stringify(data))
                        session.sendTyping()

                        HumanAugmentationService.registerNewAgent(data)
                            .then(results => {
                                //console.log(results)
                                BotService.sendText(session, {
                                    text: 'Successfully registered as Agent',
                                    data: {user_first_name: firstName}
                                })
                                session.endDialog()
                            })
                            .catch(err => {

                                console.log("some error happened in HA registration")
                                let msg
                                //console.log(msg)

                                if (err && err.status) {

                                    msg = err.response.text

                                } else {
                                    msg = TextService.text('Api failure message')
                                }


                                let buttonsText = []
                                buttonsText.push(Constants.Labels.Help)
                                BotService.sendQuickReplyButtons(session, msg
                                    , buttonsText)

                                session.error(err)
                                session.endDialog()
                            })
                        session.endDialog()
                    }
                    session.endDialog()
                    break;
                case 'login' :
                    if ((user_type === "user")) {
                        session.sendTyping()

                        if (session.userData.user_engaged_queue.flag || session.userData.user_engaged_queue.wait) {
                            BotService.sendText(session, {text: 'Not authorised'})
                            session.endDialog()
                        } else if (session.userData.user_wait_queue) {
                            let promptObj = {id: null, buttons: []}
                            if (StateService2.checkResumeShopping(session)) {
                                promptObj.buttons.push({
                                    buttonText: 'Back To Shopping',
                                    triggerIntent: 'RESULTS.DIALOG',
                                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                    data: {}
                                })
                            }
                            StateService.addPrompt2(session, promptObj)
                            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                            buttonsText.push(Constants.Labels.Introduction)
                            buttonsText.push(Constants.Labels.Help)
                            BotService.sendQuickReplyButtons(session, {
                                text: 'Not authorised'
                            }, buttonsText)
                            session.endDialog()
                        } else {
                            console.log("User should not have used this command")
                            BotService.sendText(session, {text: 'Not authorised'})
                            session.endDialog()
                        }
                        session.endDialog()
                    }
                    else {
                        data = {
                            channel_id: channelName,
                            psid: psid,
                            client_organization: HA_CLIENT_ORGANIZATION,
                        }

                        //console.log("agent data is ", JSON.stringify(data))
                        session.sendTyping()

                        HumanAugmentationService.loginAgent(data)
                            .then(results => {
                                //console.log(results)
                                BotService.sendText(session, {
                                    text: 'Successfully logged in as Agent',
                                    data: {user_first_name: firstName}
                                })
                                //entities[0].data.user_type = "user";

                                //session.beginDialog(dialog, {entities:[{entity: null, data:{user_type :"agent"} }]})
                                //session.replaceDialog( 'introduction-active-user' )
                                session.userData.status = "agent"
                                session.userData.agent_wait_queue = true
                                session.userData.user_engaged_queue = null
                                HumanHelpService.Add_user(session.message.address.user.id, session)
                                console.log("Agent stored")
                                session.replaceDialog('xpresso-bot:human-augmentation', {
                                    entities: [{
                                        entity: null,
                                        data: {user_type: "agent"}
                                    }]
                                })
                                //session.endDialog()
                            })
                            .catch(err => {

                                //console.log("some error happened in HA login")
                                let msg
                                //console.log(msg)

                                if (err && err.status) {

                                    msg = err.response.text

                                } else {
                                    msg = TextService.text('Api failure message')
                                }


                                let buttonsText = []
                                buttonsText.push(Constants.Labels.Help)
                                BotService.sendQuickReplyButtons(session, msg
                                    , buttonsText)

                                session.error(err)
                                session.endDialog()
                            })
                        session.endDialog()
                    }
                    session.endDialog()
                    break;
                case 'logout':

                    if ((user_type === "user")) {
                        session.sendTyping()

                        if (session.userData.user_engaged_queue.flag || session.userData.user_engaged_queue.wait) {
                            BotService.sendText(session, {text: 'Not authorised'})
                            session.endDialog()
                        } else if (session.userData.user_wait_queue) {
                            let promptObj = {id: null, buttons: []}
                            if (StateService2.checkResumeShopping(session)) {
                                promptObj.buttons.push({
                                    buttonText: 'Back To Shopping',
                                    triggerIntent: 'RESULTS.DIALOG',
                                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                    data: {}
                                })
                            }
                            StateService.addPrompt2(session, promptObj)
                            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                            buttonsText.push(Constants.Labels.Introduction)
                            buttonsText.push(Constants.Labels.Help)
                            BotService.sendQuickReplyButtons(session, {
                                text: 'Not authorised'
                            }, buttonsText)
                            session.endDialog()
                        } else {
                            console.log("User should not have used this command")
                            BotService.sendText(session, {text: 'Not authorised'})
                            session.endDialog()
                        }
                        session.endDialog()
                    }
                    else {
                        if (((session) && (session.userData) && (session.userData.user_engaged_queue) && (session.userData.user_engaged_queue.flag)) || ((session) && (session.userData) && (session.userData.user_engaged_queue) && (session.userData.user_engaged_queue.wait))) {

                            BotService.sendText(session, {
                                text: 'Cant logout',
                                data: {
                                    name: firstName + " " + LastName,
                                    agent_name: session.userData.user_engaged_queue.name
                                }
                            })
                            session.endDialog()

                        } else {
                            data = {
                                agent_name: firstName + " " + LastName,
                                channel_id: channelName,
                                psid: psid,
                                client_organization: HA_CLIENT_ORGANIZATION,
                            }

                            //console.log("agent data is ", JSON.stringify(data))
                            session.sendTyping()

                            HumanAugmentationService.logoutAgent(data)
                                .then(results => {
                                    //console.log(results)
                                    session.userData.status = null
                                    session.userData.agent_wait_queue = null
                                    session.userData.user_engaged_queue = null
                                    HumanHelpService.Remove_user(session.message.address.user.id)
                                    BotService.sendText(session, {
                                        text: 'Successfully logged out as Agent',
                                        data: {user_first_name: firstName}
                                    })
                                    session.endDialog()
                                })
                                .catch(err => {

                                    console.log("some error happened in HA logoutAgent")
                                    let msg
                                    //console.log(msg)

                                    if (err && err.status) {

                                        msg = err.response.text

                                    } else {
                                        msg = TextService.text('Api failure message')
                                    }


                                    let buttonsText = []
                                    buttonsText.push(Constants.Labels.Help)
                                    BotService.sendQuickReplyButtons(session, msg
                                        , buttonsText)

                                    session.error(err)
                                    session.endDialog()
                                })
                            session.endDialog()
                        }
                        session.endDialog()
                    }
                    session.endDialog()
                    break;
                case 'status' :
                    if (user_type === "user") {
                        data = {
                            channel_id: channelName,
                            client_organization: HA_CLIENT_ORGANIZATION,
                        }

                        let AllUsersWaitingInQueue = HumanAugmentationService.getAllUsersWaitingInQueue(data)

                        data = {
                            channel_id: channelName,
                            client_organization: HA_CLIENT_ORGANIZATION,
                            psid: psid
                        }
                        let getUserAccountStatus = HumanAugmentationService.getUserAccountStatus(data)
                        session.sendTyping()

                        Promise.all([AllUsersWaitingInQueue, getUserAccountStatus])
                            .then(([UsersWaitingInQueue, UserAccountStatus]) => {

                                if (((UserAccountStatus.user_queue) && (UserAccountStatus.user_queue.length > 0))) {
                                    let queue_position = _.findIndex(UsersWaitingInQueue, {psid: psid})
                                    //console.log("queue_position is ", queue_position)

                                    let promptObj = {id: null, buttons: []}
                                    if (StateService2.checkResumeShopping(session)) {
                                        promptObj.buttons.push({
                                            buttonText: 'Back To Shopping',
                                            triggerIntent: 'RESULTS.DIALOG',
                                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                            data: {}
                                        })
                                    }
                                    StateService.addPrompt2(session, promptObj)
                                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                                    buttonsText.push(Constants.Labels.Introduction)
                                    buttonsText.push(Constants.Labels.Help)
                                    BotService.sendQuickReplyButtons(session, {
                                        text: 'Already in wait queue',
                                        data: {count: queue_position + 1}
                                    }, buttonsText)
                                    session.userData.status = "user"
                                    session.userData.user_wait_queue = true
                                    HumanHelpService.Add_user(session.message.address.user.id, session)
                                    session.endDialog()
                                } else if (((UserAccountStatus.agent_user_queue) && (UserAccountStatus.agent_user_queue.length > 0))) {

                                    BotService.sendText(session, {
                                        text: 'Already talking to agent',
                                        data: {
                                            name: firstName + " " + LastName,
                                            agent_name: session.userData.user_engaged_queue.name
                                        }
                                    })
                                    session.endDialog()

                                }
                                session.endDialog()


                            })
                            .catch(err => {

                                //console.log("some error happened in HA logoutAgent")
                                let msg
                                //console.log(msg)

                                if (err && err.status) {

                                    msg = err.response.text

                                } else {
                                    msg = TextService.text('Api failure message')
                                }


                                let buttonsText = []
                                buttonsText.push(Constants.Labels.Help)
                                BotService.sendQuickReplyButtons(session, msg
                                    , buttonsText)

                                session.error(err)
                                session.endDialog()
                            })
                        session.endDialog()


                    } else if (user_type === "agent") {


                        data = {
                            channel_id: channelName,
                            client_organization: HA_CLIENT_ORGANIZATION,
                            psid: psid
                        }
                        let getAgentAccountStatus = HumanAugmentationService.getAgentAccountStatus(data)
                        session.sendTyping()

                        Promise.all([getAgentAccountStatus])
                            .then(([AgentAccountStatus]) => {

                                if (((AgentAccountStatus.agent_queue) && (AgentAccountStatus.agent_queue.length > 0))) {

                                    BotService.sendText(session, {
                                        text: 'Agent inwait queue status'
                                    })
                                    session.userData.status = "agent"
                                    session.userData.agent_wait_queue = true
                                    session.userData.user_engaged_queue = null
                                    HumanHelpService.Add_user(session.message.address.user.id, session)
                                    session.endDialog()
                                } else if (((AgentAccountStatus.agent_user_queue) && (AgentAccountStatus.agent_user_queue.length > 0))) {

                                    BotService.sendText(session, {
                                        text: 'Agent helping status',
                                        data: {
                                            name: session.userData.user_engaged_queue.name
                                        }
                                    })
                                    session.endDialog()
                                }
                                session.endDialog()


                            })
                            .catch(err => {

                                //console.log("some error happened in HA logoutAgent")
                                let msg
                                //console.log(msg)

                                if (err && err.status) {

                                    msg = err.response.text

                                } else {
                                    msg = TextService.text('Api failure message')
                                }


                                let buttonsText = []
                                buttonsText.push(Constants.Labels.Help)
                                BotService.sendQuickReplyButtons(session, msg
                                    , buttonsText)

                                session.error(err)
                                session.endDialog()
                            })
                        session.endDialog()

                    } else {
                        session.sendTyping()
                        let promptObj = {id: null, buttons: []}
                        if (StateService2.checkResumeShopping(session)) {
                            promptObj.buttons.push({
                                buttonText: 'Back To Shopping',
                                triggerIntent: 'RESULTS.DIALOG',
                                entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                data: {}
                            })
                        }
                        StateService.addPrompt2(session, promptObj)
                        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                        buttonsText.push(Constants.Labels.Introduction)
                        buttonsText.push(Constants.Labels.Help)
                        BotService.sendQuickReplyButtons(session, {
                            text: 'Not authorised'
                        }, buttonsText)
                        session.endDialog()
                    }
                    session.endDialog()
                    break;

                case 'quit':
                    if ((user_type === "user") && (session.userData.user_wait_queue)) {
                        data = {
                            channel_id: channelName,
                            psid: psid,
                            client_organization: HA_CLIENT_ORGANIZATION,
                        }
                        session.sendTyping()
                        HumanAugmentationService.removeUserFromWaitingQueue(data)
                            .then(results => {

                                let promptObj = {id: null, buttons: []}
                                if (StateService2.checkResumeShopping(session)) {
                                    promptObj.buttons.push({
                                        buttonText: 'Back To Shopping',
                                        triggerIntent: 'RESULTS.DIALOG',
                                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                        data: {}
                                    })
                                }
                                StateService.addPrompt2(session, promptObj)
                                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                                buttonsText.push(Constants.Labels.Introduction)
                                buttonsText.push(Constants.Labels.Help)
                                BotService.sendQuickReplyButtons(session, {
                                    text: 'Wait queue rejected'
                                }, buttonsText)
                                session.userData.status = null
                                session.userData.user_wait_queue = null
                                session.userData.user_engaged_queue = null
                                HumanHelpService.Remove_user(session.message.address.user.id)
                                session.endDialog()

                            })
                            .catch(err => {

                                //console.log("some error happened in removing user from user queue")
                                let msg
                                //console.log(msg)

                                if (err && err.status) {

                                    msg = err.response.text

                                } else {
                                    msg = TextService.text('Api failure message')
                                }


                                let buttonsText = []
                                buttonsText.push(Constants.Labels.Help)
                                BotService.sendQuickReplyButtons(session, msg
                                    , buttonsText)

                                session.error(err)
                                session.endDialog()
                            })
                        session.endDialog()


                    } else {
                        session.sendTyping()

                        if ((user_type === "agent")) {
                            BotService.sendText(session, {text: 'Not authorised'})
                            session.endDialog()
                        } else {

                            let promptObj = {id: null, buttons: []}
                            if (StateService2.checkResumeShopping(session)) {
                                promptObj.buttons.push({
                                    buttonText: 'Back To Shopping',
                                    triggerIntent: 'RESULTS.DIALOG',
                                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                    data: {}
                                })
                            }
                            StateService.addPrompt2(session, promptObj)
                            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                            buttonsText.push(Constants.Labels.Introduction)
                            buttonsText.push(Constants.Labels.Help)
                            BotService.sendQuickReplyButtons(session, {
                                text: 'Not authorised'
                            }, buttonsText)
                            session.endDialog()

                        }
                        session.endDialog()

                    }
                    session.endDialog()
                    break;
                case 'end':

                    if ((user_type === "user")) {
                        session.sendTyping()

                        if (session.userData.user_engaged_queue.flag || session.userData.user_engaged_queue.wait) {
                            BotService.sendText(session, {text: 'Not authorised'})
                            session.endDialog()
                        } else if (session.userData.user_wait_queue) {
                            let promptObj = {id: null, buttons: []}
                            if (StateService2.checkResumeShopping(session)) {
                                promptObj.buttons.push({
                                    buttonText: 'Back To Shopping',
                                    triggerIntent: 'RESULTS.DIALOG',
                                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                    data: {}
                                })
                            }
                            StateService.addPrompt2(session, promptObj)
                            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                            buttonsText.push(Constants.Labels.Introduction)
                            buttonsText.push(Constants.Labels.Help)
                            BotService.sendQuickReplyButtons(session, {
                                text: 'Not authorised'
                            }, buttonsText)
                            session.endDialog()
                        } else {
                            console.log("User should not have used this command")
                            BotService.sendText(session, {text: 'Not authorised'})
                            session.endDialog()
                        }
                        session.endDialog()
                    }
                    else {
                        if (((session) && (session.userData) && (session.userData.user_engaged_queue) && (session.userData.user_engaged_queue.flag)) || ((session) && (session.userData) && (session.userData.user_engaged_queue) && (session.userData.user_engaged_queue.wait))) {

                            let session2 = HumanHelpService.return_user(session.userData.user_engaged_queue.id)

                            session2.sendTyping()

                            let promptObj = {id: null, buttons: []}
                            promptObj.buttons.push({
                                buttonText: 'Yes',
                                triggerIntent: 'RECOMMEND.CONFIRM',
                                entityType: '',
                                data: {wait: true}
                            })
                            promptObj.buttons.push({
                                buttonText: 'No',
                                triggerIntent: 'RECOMMEND.CONFIRM',
                                entityType: '',
                                data: {wait: false}
                            })
                            StateService.addPrompt2(session2, promptObj)
                            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                            BotService.sendQuickReplyButtons(session2, {
                                text: 'recommendation satisfaction msg from agent'
                            }, buttonsText)
                            HumanHelpService.setWaitTimer(session2.message.address, session2, 'xpresso-bot:recommend-confirm', nconf.get("HA_response_timeout"), {timeout: true})
                            session2.endDialog()
                            console.log("session 2 dialog end")
                            HumanHelpService.Add_user(session.userData.user_engaged_queue.id, session2)

                            session.sendTyping()
                            BotService.sendText( session, { text : 'recommendation satisfaction msg sent'} )
                            session.endDialog()

                        } else {
                            session.sendTyping()
                            BotService.sendText(session, {
                                text: 'Not a legal command stop'
                            })
                            session.endDialog()
                        }
                        session.endDialog()
                    }
                    session.endDialog()
                    break;
            }

            session.endDialog()

        }
    ]

}