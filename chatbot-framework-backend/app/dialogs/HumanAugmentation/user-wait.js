var _ = require('lodash')
const nconf = require('nconf')
var builder = require('botbuilder')
var Constants = require('../../constants')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UserService = require('../../services/UserService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')
var HumanAugmentationService = require('../../services/human-augmentation-service')
var HumanHelpService = require('./human-help-service')

module.exports = {
    Dialog: [
        function (session, args, next) {

            session.sendTyping()

            UserService.setActiveState( session.message.address, true)

            let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.address.channelId
            let firstName = userName.split(' ')[0]
            let LastName = userName.split(' ')[1]
            let HA_CLIENT_ORGANIZATION = nconf.get("HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION")
            let psid = session.message.address.user.id
            let data

            if (_.has(args, 'entities[0].data.timeout')) {

                HumanHelpService.unsetWaitTimer(session.message.address)

                data = {
                    channel_id: channelName,
                    psid: psid,
                    client_organization: HA_CLIENT_ORGANIZATION,
                }
                HumanAugmentationService.removeUserFromWaitingQueue(data)
                    .then(results => {

                        let promptObj = {id: null, buttons: []}
                        if (StateService2.checkResumeShopping(session)) {
                            promptObj.buttons.push({
                                buttonText: 'Back To Shopping',
                                triggerIntent: 'RESULTS.DIALOG',
                                entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                data: {}
                            })
                        }
                        StateService.addPrompt2(session, promptObj)
                        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                        buttonsText.push(Constants.Labels.Introduction)
                        buttonsText.push(Constants.Labels.Help)
                        BotService.sendQuickReplyButtons(session, {
                            text: 'No wait queue response'
                        }, buttonsText)
                        session.userData.status = null
                        session.userData.user_wait_queue = null
                        session.userData.user_wait_queue_accepted = null
                        session.userData.user_wait_queue_length = null
                        session.userData.user_wait_queue_text = null
                        session.userData.user_engaged_queue =null
                        HumanHelpService.Add_user(session.message.address.user.id, session)

                        session.endDialog()

                    })
                    .catch(err => {

                        console.log("some error happened in removing user from user queue")
                        let msg
                        //console.log(msg)

                        if (err && err.status) {

                            msg = err.response.text

                        } else {
                            msg = TextService.text('Api failure message')
                        }


                        let buttonsText = []
                        buttonsText.push(Constants.Labels.Help)
                        BotService.sendQuickReplyButtons(session, msg
                            , buttonsText)

                        session.error(err)
                        session.endDialog()
                    })

                session.endDialog()

            } else {

                let user_wait

                if (_.has(args, 'entities[0].data.wait')) {
                    user_wait = args.entities[0].data.wait
                } else {

                    console.log("********* NO DATA **************")
                    session.endDialog()
                    return;
                }
                console.log("user wait is ", user_wait)

                let queue_length = args.entities[0].data.queue_length

                if (user_wait) {
                    HumanHelpService.unsetWaitTimer(session.message.address)
                    //console.log(results)
                    //BotService.sendText( session, { text : 'Wait queue accepted', data : { length : queue_length } } )
                    let promptObj = {id: null, buttons: []}
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }
                    StateService.addPrompt2(session, promptObj)
                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons(session, {
                        text: 'Wait queue accepted',
                        data: {length: queue_length}
                    }, buttonsText)
                    session.userData.status = "user"
                    session.userData.user_wait_queue = true
                    session.userData.user_wait_queue_accepted = null
                    session.userData.user_wait_queue_length = null
                    session.userData.user_wait_queue_text = null
                    HumanHelpService.Add_user(session.message.address.user.id, session)
                    session.endDialog()

                } else {

                    HumanHelpService.unsetWaitTimer(session.message.address)

                    data = {
                        channel_id: channelName,
                        psid: psid,
                        client_organization: HA_CLIENT_ORGANIZATION,
                    }
                    HumanAugmentationService.removeUserFromWaitingQueue(data)
                        .then(results => {

                            HumanHelpService.unsetWaitTimer(session.message.address)
                            let promptObj = {id: null, buttons: []}
                            if (StateService2.checkResumeShopping(session)) {
                                promptObj.buttons.push({
                                    buttonText: 'Back To Shopping',
                                    triggerIntent: 'RESULTS.DIALOG',
                                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                    data: {}
                                })
                            }
                            StateService.addPrompt2(session, promptObj)
                            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                            buttonsText.push(Constants.Labels.Introduction)
                            buttonsText.push(Constants.Labels.Help)
                            BotService.sendQuickReplyButtons(session, {
                                text: 'Wait queue rejected'
                            }, buttonsText)
                            session.userData.status = null
                            session.userData.user_wait_queue = null
                            session.userData.user_wait_queue_accepted = null
                            session.userData.user_wait_queue_length = null
                            session.userData.user_wait_queue_text = null
                            session.userData.user_engaged_queue =null
                            HumanHelpService.Remove_user(session.message.address.user.id)
                            session.endDialog()
                        })
                        .catch(err => {

                            console.log("some error happened in HA login")
                            let msg
                            //console.log(msg)

                            if (err && err.status) {

                                msg = err.response.text

                            } else {
                                msg = TextService.text('Api failure message')
                            }


                            let buttonsText = []
                            buttonsText.push(Constants.Labels.Help)
                            BotService.sendQuickReplyButtons(session, msg
                                , buttonsText)

                            session.error(err)
                            session.endDialog()
                        })
                    session.endDialog()

                }
                session.endDialog()
            }

            session.endDialog()

        }
    ]
};