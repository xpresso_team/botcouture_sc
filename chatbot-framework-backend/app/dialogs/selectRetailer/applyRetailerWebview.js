var _               = require( 'lodash'                     )
var builder         = require( 'botbuilder'                 )
var Constants       = require( '../../constants'            )
var StateService    = require( '../../services/StateService')
var UserService     = require( '../../services/UserService' )
var TextService     = require( '../../services/TextService' )
var BotService      = require( '../../services/BotService'  )
var ApiService      = require( '../../services/ApiService'  )    
var nconf           = require( 'nconf'                      )
const uuidV4        = require( 'uuid/v4'                    )
var toTitleCase     = require( 'to-title-case'              )
var xpressobot     = require( '../../xpresso-bot')

module.exports = {
    Label : Constants.Labels.Apply_Retailer,
    Dialog : [
        function(session,args) {
            let address
            let userId
            let page_id
            let channel_id

            if (!_.isString(session)) {
                address     = session.message.address
                userId      = address.user.id
                page_id     = address.bot.id
                channel_id  = address.channelId
            } else {

                address     = ""
                userId      = ""
                page_id     = ""
                channel_id  = ""
            }

            session.sendTyping()

            let NODE = session.userData.currentNode
            let state = session.userData[NODE]
            let collectionType
            let collectionName
            let pageNo
            let gender
            let imageUrl
            if(NODE==='NLP') {
                gender = args.entities[0].data.gender
                entity = args.entities[0].entity
            } else if(NODE==='COLLECTIONS') {
                entity = args.entities[0].entity
                gender = args.entities[0].data.gender
                collectionType = args.entities[0].data.collectionType
                collectionType = collectionType.toUpperCase()
                collectionName = args.entities[0].data.collectionName
                console.log("\n"+collectionName+"\n"+collectionType+"\n"+gender)
            } else if(NODE==='GUIDEME') {
                entity = args.entities[0].entity
                gender = args.entities[0].data.gender
                pageNo = args.entities[0].data.pageNo
                console.log("\n"+gender)
            } else if(NODE==='IMAGEUPLOADVIEWMORE') {
                entity  = args.entities[0].entity
                gender = args.entities[0].data.gender
                imageUrl = args.entities[0].data.imageUrl
            }

            let retailerPreference = UserService.getretailerPreference(address)
                .then( res => {
                   let client_name 
                   let validity
                   if(res.retailers) {
                       client_name = res.retailers
                       validity = res.validity
                       console.log("\nclient_name saved is ",client_name)
                    }
                    else {
                       client_name = []
                       let msg = "Sorry, you havent saved any shops as your preference.Please select your shop and click on use your shop filter"
                       //BotService.sendText(session, msg)
                       let intent = {
                            intent : 'SELECT-RETAILER',
                            entities : [{
                                entity : entity,
                                entityType : 'SELECT-RETAILER',
                                data : args.entities[0].data,
                                flag : 1
                            }]
                       }
                       session.replaceDialog("xpresso-bot:select-retailer",intent)
                       return 
                    }
                    session.userData[NODE].client_name = client_name
                    session.userData.client_name = client_name
                    state.client_name = client_name
                    if(NODE==='NLP') {
                        console.log("\n entered NLP Shop apply filter",client_name)
                        let intent = {
                            intent: 'RESULTS.DIALOG',
                            entities: [{
                                entity : entity,
                                entityType: 'RESULTS.FILTERER',
                                data: {
                                    NODE: NODE,
                                    state: _.cloneDeep(state),
                                    guestMode: false
                                }
                            }]
                        }
                        session.replaceDialog( 'xpresso-bot:results-dialog', intent )
                    } else if(NODE==='COLLECTIONS') {
                        let intent = {
                            intent : 'VIEW.COLLECTIONS.CATEGORY',
                            entities : [{
                                entity : entity,
                                entityType : 'EXPLORE.INSPIRATION.TRIGGER',
                                data : { collectionType : collectionType, collectionName : collectionName, gender : gender, client_name : client_name }
                            }]
                        }
                        console.log("\n",client_name)
                        session.replaceDialog( 'xpresso-bot:view-collection-category', intent)
                    } else if(NODE==='GUIDEME') {
                        let intent = {
                            intent   : 'GUIDE.ME.CATEGORY',
                            score    : 1,
                            entities : [{ 
                                entity : entity, 
                                entityType : 'GUIDE.ME.CATEGORY.CLIENTNAME', 
                                data : { gender : gender, client_name:client_name } 
                            }]
                        }
                        session.replaceDialog( 'xpresso-bot:guide-me-category', intent)
                    } else if(NODE==='IMAGEUPLOADVIEWMORE') {
                            console.log("\n imageupload client_name is ", client_name)
                            let intent = {
                            intent   : 'RESULTS.DIALOG',
                            score    : 1,
                            entities : [{ 
                                entity : entity, 
                                entityType : 'IMAGE.UPLOAD.VIEW.MORE', 
                                data : { gender : gender, client_name:client_name, imageUrl : imageUrl } 
                            }]
                        }
                        session.replaceDialog( 'xpresso-bot:results-dialog', intent)   
                    }
                    session.endDialog()
                })
                .catch( err =>{
                    if( err.status ) {
                        let promptObj = { id : null, buttons : [] }
                        StateService.addPrompt2(session, promptObj)
                        let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
                        let button_arr = [ Constants.Labels.Introduction, Constants.Labels.Help]
                        for(let i = 0;i<button_arr.length;i++){
                            buttonsText.push(button_arr[i])
                        }
                        BotService.sendQuickReplyButtons( session, { text : 'Error Msg'}, buttonsText )
                    } else {
                        let promptObj = { id : null, buttons : [] }  
                    
                        StateService.addPrompt2(session, promptObj)

                        let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
                        let button_arr = [ Constants.Labels.Introduction, Constants.Labels.Help ]
                        for(let i = 0;i<button_arr.length;i++){
                            buttonsText.push(button_arr[i])
                        }
                        BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
                    }
                    session.error( err )
                    session.endDialog()
                })
            session.endDialog()    
        }        
    ]
}
