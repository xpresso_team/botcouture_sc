var _               = require( 'lodash'                     )
var builder         = require( 'botbuilder'                 )
var Constants       = require( '../../constants'            )
var StateService    = require( '../../services/StateService')
var UserService     = require( '../../services/UserService' )
var TextService     = require( '../../services/TextService' )
var BotService      = require( '../../services/BotService'  )
var ApiService      = require( '../../services/ApiService'  )    
var nconf           = require( 'nconf'                      )
const uuidV4        = require( 'uuid/v4'                    )
var toTitleCase     = require( 'to-title-case'              )

module.exports = {
    Label  : Constants.Labels.Select_Shop,
    Dialog : [
        function(session,args) {
            
            let address
            let userId
            let page_id
            let channel_id

            if (!_.isString(session)) {
                address     = session.message.address
                userId      = address.user.id
                page_id     = address.bot.id
                channel_id  = address.channelId
            } else {

                address     = ""
                userId      = ""
                page_id     = ""
                channel_id  = ""
            }

            session.sendTyping()

            try {
                let RETAILER_IMAGES=nconf.get('RETAILER_IMAGES')
                let NODE = session.userData.currentNode || NLP
                let state = _.merge( session.userData[ NODE ], args.entities[0].data.state ) || {}
                let gender = session.userData[NODE].gender
                let collectionType
                let collectionName
                let entityType
                let pageNo
                let flag
                let SS_Webview  = nconf.get("Select_Shop_Webview")
                let SS_Carousel = nconf.get("Select_Shop_Carousel")
                let remreg = NODE+'.REMOVE.SHOP'
                let usereg = NODE+'.USEYOUR.SHOP'
        
                if(typeof session.userData[NODE].client_name=='undefined') {
                    session.userData[NODE].client_name=[]
                }

                if(NODE==='NLP') {
                    entityType = args.entities[0].entityType
                } else if(NODE === 'COLLECTIONS') {
                    collectionType = args.entities[0].data.collectionType
                    collectionName = args.entities[0].data.collectionName
                    gender = args.entities[0].data.gender
                    entityType = args.entities[0].entityType
                } else if(NODE==='GUIDEME') {
                    entityType = args.entities[0].entityType
                    gender     = args.entities[0].data.gender
                    pageNo     = Number( args.entities[0].data.pageNo ) || 0
                }
                                           
                if(remreg === entityType) {
                    console.log("entered remove shop") 
                    if(session.userData[NODE].client_name) {
                        if(SS_Carousel) {
                            console.log("entered remove shop carousel part")
                            session.userData[NODE].SS_Carousel = 1
                            session.userData[NODE].client_name = []
                            state.client_name = []
                            let intent = {
                            intent: 'RESULTS.DIALOG',
                            score: 1,
                            entities: [{
                                entityType: 'REMOVE.RETAILERS',
                                data: {NODE : NODE, resetState : true, state : state}
                                }]
                            }
                            session.replaceDialog('xpresso-bot:results-dialog', intent)                     
                            
                        } else if(SS_Webview) {
                            console.log("entered remove shop webview part")
                            session.userData[NODE].SS_Webview = 1
                            session.userData[NODE].client_name = []
                            session.userData.client_name = []
                            state.client_name = []                    

                            UserService.removeretailerPreference(address)
                            .then( res => {
                                if(res) {
                                    //console.log(res)
                                    session.userData.client_name = []
                                }
                            })
                            .catch( err => {
                                console.log(err)
                            })
                            
                            let intent = {
                            intent: 'RESULTS.DIALOG',
                            score: 1,
                            entities: [{
                                entityType: 'REMOVE.RETAILERS',
                                data: {NODE : NODE, resetState : true, state : state}
                                }]
                            }
                            session.replaceDialog('xpresso-bot:results-dialog', intent)
                        } else {
                            if(session.userData[NODE].SS_Carousel==0 && session.userData[NODE].SS_Webview==0) {
                                session.userData[NODE].SS_Carousel = 1
                                session.userData[NODE].SS_Webview = 0
                                state.client_name = []
                                console.log('we have wrong data')
                            } else {
                                session.userData[NODE].SS_Carousel= 1
                                session.userData[NODE].SS_Webview = 0
                                session.userData[NODE].client_name = []  
                                state.client_name = []                
                                let intent = {
                                intent: 'RESULTS.DIALOG',
                                score: 1,
                                entities: [{
                                    entityType: 'REMOVE.RETAILERS',
                                    data: {NODE : NODE, resetState : true, state : state}
                                    }]
                                }
                                session.replaceDialog('xpresso-bot:results-dialog', intent)
                            }
                        }
                        session.endDialog()
                    } else {
                        console.log("this should ideally not happen as remove option comes if there is some client prefernce saved",session.userData[NODE].client_name)
                        session.endDialog()
                    }
                    session.endDialog()
                } else if(usereg=== entityType){
                    if(SS_Webview) {
                        let intent = {
                            intent : 'APPLY.RETAILER.WEBVIEW',
                            score  : 1,
                            entities : [{
                                entity : 'USEYOUR.SHOP',
                                entityType : 'SELECT-RETAILER',
                                data : args.entities[0].data
                            }]
                        }

                        session.replaceDialog('xpresso-bot:apply-retailer-webview', intent)

                    }
                    session.endDialog()
                } else {
                    if(SS_Webview) {                   
                        let msg
                        if(typeof args.entities[0].flag!== 'undefined'&&args.entities[0].flag===1) {
                            msg = "Sorry, you havent saved any shops as your preference.Please select your shop and click on use your shop filter"
                        } else {
                            msg= "Please Create your shop preference and click on use your shop filter to apply the filter"
                        }
                        let buttons = []
                        let client_name = []
                        
                        if(NODE==='NLP') {
                            buttons.push({
                                "type"    : "postback",
                                "title"   : "Use your Shop filter",
                                "payload" : "NLPSHOP::"+gender
                            })
                        } else if(NODE==='COLLECTIONS') {
                            buttons.push({
                                "type"    : "postback",
                                "title"   : "Use your Shop filter",
                                "payload" : "COLLECTIONSHOP::"+collectionType+"::"+collectionName+"::"+gender 
                            })
                        } else if(NODE==='GUIDEME') {
                            buttons.push({
                                "type"    : "postback",
                                "title"   : "Use your Shop filter",
                                "payload" : "GUIDEMESHOP::"+gender+"::"+pageNo  
                            })
                        } else if(NODE==='IMAGEUPLOADVIEWMORE') {
                            let gender = session.userData['IMAGEUPLOAD'].gender || [] 
                            let imageUrl = session.userData['IMAGEUPLOAD'].imageUrl
                            buttons.push({
                                "type"    : "postback",
                                "title"   : "Use your Shop filter",
                                "payload" : 'IMAGEUPLOADVIEWMORESHOP::'+imageUrl+'::'+gender 
                            })
                        }

                        console.log("\n client_name inside state is",session.userData.client_name)
                        let url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Select_Shop"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()
                        buttons.push({
                            "type"  : "web_url",
                            "title" : "Create Shop Preference",
                            "url"   : url,
                            "messenger_extensions": true,
                            "webview_height_ratio": nconf.get('WebView_size')["Select_Shop"]
                        })

                        if(NODE==='NLP') {
                            buttons.push({
                                "type"    : "postback",
                                "title"   : "Skip",
                                "payload" : "SKIP::RESULTS.DIALOG::WISHLIST.BACK.TO.SHOPPING"
                            })
                        } else if(NODE==='COLLECTIONS') {
                            buttons.push({
                                "type"    : "postback",
                                "title"   : "Skip",
                                "payload" : 'EXPLORE::' + collectionType.toUpperCase() + '::' + collectionName + '::' + gender+'::'+client_name
                            })
                        } else if(NODE==='GUIDEME') {
                            buttons.push({
                                "type"    : "postback",
                                "title"   : "Skip",
                                "payload" : 'GUIDEME::'+ gender+'::'+'CLIENT_NAME::'+client_name+'::'+pageNo 
                            })
                        } else if(NODE==='IMAGEUPLOADVIEWMORE') {
                            let gender = session.userData['IMAGEUPLOAD'].gender || [] 
                            let imageUrl = session.userData['IMAGEUPLOAD'].imageUrl
                            buttons.push({
                                "type"    : "postback",
                                "title"   : "Skip",
                                "payload" :  'IMAGEUPLOADVIEWMORE::[]::'+imageUrl+'::'+gender
                            })
                        }

                        BotService.sendCard(session, buttons, true, "button", msg)            
                        session.endDialog()
                    } 
                    else if(SS_Carousel) {
                        if(NODE==='NLP') {
                            let cards=[]   
                            let ret = session.userData['NLP'].retailers
                            console.log(ret)
                            for (let i = 0; i < ret.length; i++) {
                                state.client_name = ret[i]
                                let intent = {
                                    intent: 'RESULTS.DIALOG',
                                    entities: [{
                                        entityType: 'RESULTS.FILTERER',
                                        data: {
                                            NODE: NODE,
                                            state: _.cloneDeep(state),
                                            guestMode: false
                                        }
                                    }]
                                }
                                //intent.entities[0].data.state['client_name'] = [retailer[i].key]    // <- This is where the filter is applied

                                let PostBack = 'POSTBACK::' + JSON.stringify(intent)
                                let card = new builder.HeroCard(session)
                                    .title(toTitleCase(ret[i]))
                                    .images([builder.CardImage.create(session, "https://s3.amazonaws.com/xpressobrand/images/"+encodeURIComponent(ret[i]) )])
                                    .buttons([
                                        builder.CardAction.postBack(session, PostBack, toTitleCase(ret[i])),
                                    ])                                                                                        
                                cards.push(card)
                            }
                            BotService.sendCard(session, cards)
                        } else if(NODE==='IMAGEUPLOADVIEWMORE') {
                            let cards=[]
                            let ret = session.userData['IMAGEUPLOADVIEWMORE'].retailers
                            let client_name = ''
                            let gender = session.userData['IMAGEUPLOADVIEWMORE'].gender || [] 
                            let imageUrl = session.userData['IMAGEUPLOADVIEWMORE'].imageUrl 
                            for( let i=0;i < ret.length; i++) {   
                                let card = new builder.HeroCard(session)
                                    .title(toTitleCase(ret[i]))
                                    .images(
                                        builder.CardImage.create(session, "https://s3.amazonaws.com/xpressobrand/images/"+encodeURIComponent(ret[i]) )
                                    )
                                    .buttons([
                                        builder.CardAction.postBack(session, 'IMAGEUPLOADVIEWMORE::'+ret[i]+'::'+imageUrl+'::'+gender, toTitleCase(ret[i]))
                                    ])
                                    cards.push(card)
                                }
                            BotService.sendCard(session, cards)                                
                        } else {              
                        ApiService.getallRetailers(session)
                            .then(ret => {    
                                let retailer
                                if(ret){
                                    let retailerlist = JSON.parse(ret)                            
                                    console.log("\n"+retailerlist+"\n")
                                    let errorText = null
                                    if ( retailerlist.length === 0 ) {
                                        errorText = 'There are no retailers in the Database!'
                                    }
                                    let tempret = {}
                                    retailer=   retailerlist.map(ret => {
                                        tempret = {}                                
                                        tempret.key = ret
                                        console.log(tempret.key)
                                        tempret.value = "https://s3.amazonaws.com/xpressobrand/images/"+ret
                                        tempret.selected = false
                                        return tempret
                                    })
                                    console.log("\n"+Array.isArray(retailer)+"\n")
                                   // console.log("\n"+retailer[4].value+"\n")
                                }

                                let cards = []
                                if(retailer) {
                                    if(NODE==='COLLECTIONS') {
                                        let client_name = ''
                                        for( let i=0;i < retailer.length; i++) {
                                            client_name = retailer[i].key
                                            let card = new builder.HeroCard(session)
                                                .title(toTitleCase(retailer[i].key))
                                                .images(
                                                    builder.CardImage.create(session, "https://s3.amazonaws.com/xpressobrand/images/"+encodeURIComponent(retailer[i].key) )
                                                )
                                                .buttons([
                                                    builder.CardAction.postBack(session, 'EXPLORE::' + collectionType.toUpperCase() + '::' + collectionName + '::' + gender+'::'+client_name, toTitleCase(retailer[i].key))
                                                ])
                                                cards.push(card)
                                            }
                                    } else if(NODE==='GUIDEME') {
                                        let client_name = ''
                                        for( let i=0;i < retailer.length; i++) {
                                            client_name = retailer[i].key
                                            let card = new builder.HeroCard(session)
                                                .title(toTitleCase(retailer[i].key))
                                                .images(
                                                    builder.CardImage.create(session, "https://s3.amazonaws.com/xpressobrand/images/"+encodeURIComponent(retailer[i].key) )
                                                )
                                                .buttons([
                                                    builder.CardAction.postBack(session, 'GUIDEME::'+ gender+'::'+'CLIENT_NAME::'+client_name+'::'+pageNo, toTitleCase(retailer[i].key))
                                                ])
                                                cards.push(card)
                                            }
                                    } 
                                    BotService.sendCard(session, cards)
                                }
                                else {
                                    text = "Collection Api failure message"
                                    Botservice.sendText(session, text)
                                }
                               //   "brand_url": "http://34.195.150.71:9001/v1/util/brand/?brand=",
                               // console.log("cards sent are",cards)
                                session.endDialog()    
                            })
                            .catch(err =>{
                                  if ( err.status ) {
                                  let promptObj = { id : null, buttons : [] }
                                  StateService.addPrompt2(session, promptObj)
                                  let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
                                  let button_arr = [ Constants.Labels.Introduction, Constants.Labels.Help]
                                  for(let i = 0;i<button_arr.length;i++){
                                      buttonsText.push(button_arr[i])
                                  }
                                  BotService.sendQuickReplyButtons( session, { text : 'Error Msg'}, buttonsText )

                              }else {
                                  let promptObj = { id : null, buttons : [] }
                                
                                  StateService.addPrompt2(session, promptObj)

                                  let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
                                  let button_arr = [ Constants.Labels.Introduction, Constants.Labels.Help ]
                                  for(let i = 0;i<button_arr.length;i++){
                                      buttonsText.push(button_arr[i])
                                  }
                                  BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
                              }
                              session.error( err )
                              session.endDialog()                    
                            })

                            session.endDialog()
                        }

                        session.endDialog()
                    }
                    session.endDialog()
                }
                session.endDialog()
            } catch(err) {
                console.log(err)
                session.endDialog()
            }
            session.endDialog()
        }   
    ]

}
