var _           = require( 'lodash'      )
var builder     = require( 'botbuilder'              )
var Constants   = require( '../constants'            )
var TextService = require( '../services/TextService' )
var BotService  = require( '../services/BotService'  )
var UserService = require( '../services/UserService' )
var ResultsService = require( './results/results-service'  )

module.exports = {
  Label: Constants.Labels.UploadImage,
  Dialog: [   
  function ( session, args, next ) {

    // session.userData[Constants.NODE.NLP].nlp_reset_flag=1
    ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 )
    UserService.setActiveState( session.message.address, true)

    BotService.sendText( session, 'Upload a Picture Message' )
    // session.send( TextService.text( 'Upload a Picture Message' ) )
    session.endDialog()

  }]
};