var _ = require('lodash')
const nconf = require('nconf')
var builder = require('botbuilder')
var Constants = require('../../constants')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UserService = require('../../services/UserService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')
var CustomerSupportService = require('./customer-support-service')


module.exports = {
    Dialog: [
        function (session, args, next) {
            UserService.setActiveState(session.message.address, true)
            let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.address.channelId
            let firstName = userName.split(' ')[0]
            let LastName = userName.split(' ')[1]
            let psid = session.message.address.user.id
            let cards = []

            if (!( _.has(args, 'entities[0].data.text') )) {

                console.log("In customer support query , should not happen")
                session.endDialog();
                return;
            }

            let text = args.entities[0].data.text
            let current_id = ""
            if (args.entities[0].data.current_id) {
                current_id = args.entities[0].data.current_id
            }
            let prev_id = ""
            if (args.entities[0].data.prev_id) {
                prev_id = args.entities[0].data.prev_id
            }
            let multi_term = ""
            if (args.entities[0].data.multi_term) {
                multi_term = args.entities[0].data.multi_term
            }

            CustomerSupportService.getsupport(text, current_id, prev_id, multi_term, session, function (support) {
                console.log('Intent JSON\t' + JSON.stringify(support));
                if ((support) && (support["status_code"] === 200) && (support.responses) && (support.responses.length > 0)) {

                    console.log(support.responses.length,support["status_code"],"inside function")

                    let multi_term = support.multi_term.toLowerCase()
                    for (let i = 0; i < support.responses.length; i++) {
                        let msg
                        msg = support.responses[i].response_text

                        console.log("msg",msg)

                        console.log(multi_term)

                        if (multi_term === "no") {

                            let button_type = support.responses[i].button_type
                            let postback_type = support.responses[i].postback_type
                            let postback
                            postback = support.responses[i].postback

                            console.log("button_type",button_type)
                            console.log("postback",postback)

                            for (let i = 0; i < msg.length - 1; i++) {
                                BotService.sendText(session, msg[i])
                            }

                            let quick_reply_text_msg
                            if ((button_type === "postback") && (postback.length > 0)) {
                                quick_reply_text_msg = TextService.text('cs option for back to bot')[0]
                                console.log("postback",postback)
                                console.log("postback_type",postback_type)
                                let postback_text_msg = msg[msg.length - 1]
                                console.log("postback text",postback_text_msg)
                                BotService.sendText(session, postback_text_msg)
                                let cards = []
                                for (let i = 0; i < Math.min(postback.length,10); i++) {

                                    if (postback_type === "button") {

                                        let button = []

                                        let postback_button = postback[i].button
                                        console.log("postback_button",postback_button)

                                        for (let i = 0; i < Math.min(postback_button.length,3); i++) {


                                            console.log("value of i is",i)
                                            console.log("postback_button",postback_button[0])
                                            console.log("postback_button",postback_button[i])
                                            if (postback_button[i].type === "web_url") {
                                                button.push(builder.CardAction.openUrl(session, postback_button[i].url, postback_button[i].title))
                                            } else if (postback_button[i].type === "postback") {
                                                button.push(builder.CardAction.postBack(session, postback_button[i].payload, postback_button[i].title))
                                            }


                                        }

                                        console.log("button",button)

                                        let card = new builder.HeroCard(session)
                                            .title(TextService.text('Intro option')[0])
                                            .buttons(button)
                                        cards.push(card)

                                    } else if (postback_type === "generic") {


                                        let button = []

                                        let postback_button = postback[i].button

                                        for (let i = 0; i < Math.min(postback_button,3); i++) {

                                            if (postback_button[i].type === "web_url") {
                                                button.push(builder.CardAction.openUrl(session, postback_button[i].url, postback_button[i].title))
                                            } else if (postback_button[i].type === "postback") {
                                                button.push(builder.CardAction.postBack(session, postback_button[i].payload, postback_button[i].title))
                                            }


                                        }

                                        let card = new builder.HeroCard(session)
                                            .title(postback[i].title)
                                            .images([
                                                builder.CardImage.create(session, postback[i].image_url)])
                                            .buttons(button)
                                        if (postback[i].url) {
                                            card.tap(builder.CardAction.openUrl(session, postback[i].image_url))
                                        }

                                        cards.push(card)
                                    }
                                }
                                console.log("cards is ",cards)
                                BotService.sendCard(session, cards)
                            }else{
                                quick_reply_text_msg = msg[msg.length - 1]
                            }

                            let promptObj = {id: null, buttons: []}
                            if (StateService2.checkResumeShopping(session)) {
                                promptObj.buttons.push({
                                    buttonText: 'Back To Shopping',
                                    triggerIntent: 'RESULTS.DIALOG',
                                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                    data: {}
                                })
                            }
                            StateService.addPrompt2(session, promptObj)
                            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                            buttonsText.push(Constants.Labels.Introduction)
                            buttonsText.push(Constants.Labels.Help)
                            console.log("quick_reply_text_msg is ",quick_reply_text_msg)
                            console.log("buttonsText ",buttonsText)
                            BotService.sendQuickReplyButtons(session, quick_reply_text_msg, buttonsText)
                            session.endDialog()
                        } else if (multi_term === "yes") {

                            let current_id = support.current_id
                            let prev_id = support.prev_id
                            let multi_term_actual = support.multi_term
                            let button_type = support.responses[i].button_type
                            let postback_type = support.responses[i].postback_type

                            let quick_reply
                            quick_reply = support.responses[i].quick_reply

                            let postback
                            postback = support.responses[i].postback


                            for (let i = 0; i < msg.length - 1; i++) {
                                BotService.sendText(session, msg[i])
                            }

                            if ((button_type === "quick_reply") && (quick_reply.length > 0)) {

                                let quick_reply_text_msg = msg[msg.length - 1]
                                let promptObj = {id: null, buttons: []}

                                for (let j = 0; j < Math.min(quick_reply.length,10); j++) {
                                    promptObj.buttons.push({
                                        buttonText: quick_reply[j],
                                        triggerIntent: 'CUSTOMER-QUERY-RESPONSE',
                                        entityType: 'CUSTOMER.SUPPORT.QUICKREPLY',
                                        data: {
                                            text: quick_reply[j],
                                            current_id: current_id,
                                            prev_id: prev_id,
                                            multi_term: multi_term_actual
                                        }
                                    })
                                }
                                StateService.addPrompt2(session, promptObj)
                                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                                BotService.sendQuickReplyButtons(session, quick_reply_text_msg, buttonsText)
                                session.endDialog()

                            } else if ((button_type === "postback") && (postback.length > 0)) {
                                let postback_text_msg = msg[msg.length - 1]
                                BotService.sendText(session, postback_text_msg)
                                let cards = []
                                for (let i = 0; i < Math.min(postback.length,10); i++) {

                                    if (postback_type === "button") {

                                        let button = []

                                        let postback_button = postback[i].button

                                        for (let i = 0; i < Math.min(postback_button,3); i++) {

                                            if (postback_button[i].type === "web_url") {
                                                button.push(builder.CardAction.openUrl(session, postback_button[i].url, postback_button[i].title))
                                            } else if (postback_button[i].type === "postback") {
                                                button.push(builder.CardAction.postBack(session, postback_button[i].payload, postback_button[i].title))
                                            }

                                        }

                                        let card = new builder.HeroCard(session)
                                            .title(TextService.text('Intro option')[0])
                                            .buttons(button)
                                        cards.push(card)

                                    } else if (postback_type === "generic") {


                                        let button = []

                                        let postback_button = postback[i].button

                                        for (let i = 0; i < Math.min(postback_button,3); i++) {

                                            if (postback_button[i].type === "web_url") {
                                                button.push(builder.CardAction.openUrl(session, postback_button[i].url, postback_button[i].title))
                                            } else if (postback_button[i].type === "postback") {
                                                button.push(builder.CardAction.postBack(session, postback_button[i].payload, postback_button[i].title))
                                            }


                                        }

                                        let card = new builder.HeroCard(session)
                                            .title(postback[i].title)
                                            .images([
                                                builder.CardImage.create(session, postback[i].image_url)])
                                            .buttons(button)
                                        if (postback[i].url) {
                                            card.tap(builder.CardAction.openUrl(session, postback[i].image_url))
                                        }

                                        cards.push(card)
                                    }
                                }
                                BotService.sendCard(session, cards)
                            } else {

                                let text_msg = msg[msg.length - 1]

                                BotService.sendText(session, text_msg)
                                let quick_reply_text_msg = msg[msg.length - 1]
                                let promptObj = {id: null, buttons: []}
                                if (StateService2.checkResumeShopping(session)) {
                                    promptObj.buttons.push({
                                        buttonText: 'Back To Shopping',
                                        triggerIntent: 'RESULTS.DIALOG',
                                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                        data: {}
                                    })
                                }
                                StateService.addPrompt2(session, promptObj)
                                //console.log("postback replied")

                                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                                buttonsText.push(Constants.Labels.Introduction)
                                buttonsText.push(Constants.Labels.Help)
                                BotService.sendQuickReplyButtons(session, quick_reply_text_msg, buttonsText)
                                session.endDialog()
                            }
                            session.endDialog()
                        }
                        session.endDialog()
                    }
                    session.endDialog()
                } else {
                    session.sendTyping()

                    let promptObj = {id: null, buttons: []}
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }
                    StateService.addPrompt2(session, promptObj)
                    //console.log("postback replied")

                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons(session, {text: 'No answer available'}, buttonsText)
                    //BotService.sendText(session, {text: 'No answer available'});
                    session.endDialog()
                }
            });
            session.endDialog()
        }
    ]
};