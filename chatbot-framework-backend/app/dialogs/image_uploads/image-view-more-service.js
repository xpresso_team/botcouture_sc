var _ = require('lodash')
const nconf = require('nconf')
var builder = require('botbuilder')
var Constants = require('../../constants')
var ApiService = require('../../services/ApiService')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UtilService = require('../../services/UtilService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')
var toTitleCase = require('to-title-case')


getDefaultState = NODE => {
    let defaultState = {
        imageUrl: null,
        upload_id: null,
        entity: null,
        gender: [],
        pageNo: 0,
        pageSize: 10 + 2,
        isSocialUrl: false,
        // Filters
        price: [],
        subcategory: [],
        features: [],
        brand: [],
        details: [],
        colorsavailable: [],
        size: [],
        xc_category: [],
        client_name :[]
    }
    return _.cloneDeep(defaultState)
}


/**
 *
 */
getResults = (state, session) => {


    let prom = null


    if (state.isSocialUrl) {
        prom = ApiService.getSocialVisualSearchResult(state.imageUrl, state.gender,state.client_name, session)
    } else {
        prom = ApiService.getVisualSearchResult(state.imageUrl, state.gender, state.client_name, session)
    }

    // return ApiService.getVisualSearchResult( state.imageUrl, state.gender )
    return prom.then(results => {

        // Check for expiered images
        if (results && results.status_code === -1) {
            return results
        }

// console.log( 'RESULTS -> ', results )
// console.log( 'RESULTS -> ', results.results[ state.entity ].matches.length )


        if (!results.results[state.entity] || results.results[state.entity].length === 0) {
            results.results[state.entity] = {matches: []}
        }
        // Rearrange results to be compatible with results/filter.js
        results.Results = results.results[state.entity].matches
        delete results.results[state.entity]

        return results

    })
        .then(results => {


            // Check for expiered images
            if (results && results.status_code === -1) {
                return results
            }

            // Filter for brand, price, color, size

            // TODO: IMPLIMENT PRICE FILTER
            results.Results = _.filter(results.Results, product => {
                if (!state.brand || state.brand.length === 0) return true
                return _.includes(state.brand, product.brand)
            })

            results.Results = _.filter(results.Results, product => {
                if (!state.size || state.size.length === 0) return true
                return _.intersection(product.size, state.size).length
            })

            results.Results = _.filter(results.Results, product => {
                if (!state.colorsavailable || state.colorsavailable.length === 0) return true
                return _.intersection(product.colorsavailable, state.colorsavailable).length
            })
            //console.log('AFTER FILTER', results)
            return results

        })

}

/**
 *
 */
processResults = (session, results) => {
    if(nconf.get('Select_Shop_Carousel')) {
        let ret = ["express","forever21","lkbennett","kohl\'s","macy\'s","renttherunway","nordstorm"]
        let client_names
        let res
        try {
            res = _.filter(results.Results, val => {
                    let temp = val.client_name
                    let flag
                    flag=ret.indexOf(temp)
                    if(flag!==-1) {
                        console.log(flag)
                        ret.splice(flag,1)
                        console.log(ret)
                        return temp
                    }
            })
            client_names= res.map(val => {
                return val.client_name
            })
        } catch(err) {
            console.log(err)
            client_names=["express","forever21","lkbennett","kohl\'s","macy\'s","renttherunway","nordstorm"]
        }
        session.userData['IMAGEUPLOADVIEWMORE'].retailers=client_names
    }

    let pageSize = session.userData[session.userData.currentNode].pageSize || 10 + 2
    let pageNo = session.userData[session.userData.currentNode].pageNo || 0
    let entity = session.userData[session.userData.currentNode].entity
    let promptObj = {id: null, buttons: []}
    let gender_flag = null
    let status_code = null

    // Check for expiered images
    if (results && results.status_code === -1) {
        return {cards: [], promptObj: {}, status_code: results.status_code}
    } else if (results) {
        status_code = results.status_code
    }

    let pagedResults = UtilService.pageList(results.Results, pageNo, pageSize, null, null)
    return UtilService.createCards(session, pagedResults).then(cards => {

        // ANALYTICS
        session.userData.analytics.out.visual_search = 3    // 3 - multi object search result
        session.userData.analytics.out.vision_engine_response = pagedResults

        // The QuickReply buttons
        let onLastPage = ( pageSize - 2 ) * ( pageNo + 1 ) >= results.Results.length
        if (!onLastPage) {
            promptObj.buttons.push(
                {
                    buttonText: 'Show More',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'IMAGE.VIEW.MORE.SHOW.MORE',
                    data: {state: {pageNo: pageNo + 1}}
                }
            )
        }

        // Image match and categories found
        if (!results.results.gender_flag) {
            if (!session.userData[session.userData.currentNode].gender) {
                promptObj.buttons = promptObj.buttons.concat([
                    {
                        buttonText: TextService.text('Image Gender Filter')[1],
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'IMAGE.UPLOAD.WOMEN.FILTER',
                        data: {state: {gender: ['women']}}
                    },
                    {
                        buttonText: TextService.text('Image Gender Filter')[0],
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'IMAGE.UPLOAD.MEN.FILTER',
                        data: {state: {gender: ['men']}}
                    }
                ])
            }
        }

        promptObj.buttons = promptObj.buttons.concat(StateService2.getFiltersButtons(session, session.userData.currentNode, results.Results))
        return {cards: cards, promptObj: promptObj, status_code: results.status_code}

    }).catch(err => {

        console.log("some error happened in image view more service " + err)

    })

}

/**
 *
 */
displayResults = (session, results) => {

    let {cards, promptObj, status_code} = results

    if (status_code === -1) {

        let promptObj = {id: null, buttons: []}
        let buttonsText = []
        if (nconf.get('Human_augmentauion')) {
            promptObj.buttons.push({
                buttonText: "Chat with Specialist",
                triggerIntent: 'HUMAN.AUGMENTATION',
                data: {user_type: "user"}
            })

            StateService.addPrompt2(session, promptObj)

            buttonsText = _.map(promptObj.buttons, button => button.buttonText)
        }
        for (let i = 0; i < TextService.text('No Image Result Payload Msg').length; i++) {
            buttonsText.push(TextService.text('No Image Result Payload Msg')[i])
        }

        BotService.sendQuickReplyButtons(session, 'Image expired msg', buttonsText)

    } else if (cards.length > 0) {

        BotService.sendText(session, {
            text: 'Type a Query Result Message',
            data: {entity: session.userData[session.userData.currentNode].entity}
        })
        if (session.message.source === "facebook") {
            BotService.sendCard(session, cards, true, "generic")
        } else {
            BotService.sendCard(session, cards)
        }

    } else {

        let promptObj = {id: null, buttons: []}

        // if (StateService2.checkResumeShopping(session)) {
        //     promptObj.buttons.push({
        //         buttonText: 'Back To Shopping',
        //         triggerIntent: 'RESULTS.DIALOG',
        //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
        //         data: {}
        //     })
        // }

        if (nconf.get('Human_augmentauion')) {
            promptObj.buttons.push({
                buttonText: TextService.text('Chat with Specialist')[0],
                triggerIntent: 'HUMAN.AUGMENTATION',
                data: {user_type: "user"}
            })
        }

        StateService.addPrompt2(session, promptObj)

        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
        for (let i = 0; i < TextService.text('No Image Result Payload Msg').length; i++) {
            buttonsText.push(TextService.text('No Image Result Payload Msg')[i])
        }
        BotService.sendQuickReplyButtons(session, 'No results Message', buttonsText)
        session.endDialog()
        return
    }

    if(typeof session.userData['IMAGEUPLOADVIEWMORE'].client_name!=='undefined' && session.userData['IMAGEUPLOADVIEWMORE'].client_name!==null && session.userData['IMAGEUPLOADVIEWMORE'].client_name.length>0) {
        promptObj.buttons.push({
            buttonText: 'Remove Shop',
            triggerIntent: 'SELECT-RETAILER',
            entityType: 'IMAGEUPLOADVIEWMORE.REMOVE.SHOP',
            data: {state:session.userData['IMAGEUPLOADVIEWMORE']}
        })
    }  else {
        if(nconf.get('Select_Shop_Webview')) {
            let selectionflag = 1
            console.log("client_name flag inside typeaquery is ", session.userData.client_name)
            if(session.userData.client_name.length>0) {
                promptObj.buttons.push({
                buttonText: 'Use Your Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'IMAGEUPLOADVIEWMORE.USEYOUR.SHOP',
                data: {state:session.userData['IMAGEUPLOADVIEWMORE']}
                })    
            } else {
                promptObj.buttons.push({
                buttonText: 'Select Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'IMAGEUPLOADVIEWMORE.SELECT.SHOP',
                data: {state:session.userData['IMAGEUPLOADVIEWMORE']}
                })
            }
        } else {
            promptObj.buttons.push({
                buttonText: 'Select Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'IMAGEUPLOADVIEWMORE.SELECT.SHOP',
                data: {state:session.userData['IMAGEUPLOADVIEWMORE']}
            })
        }
    }


    // The QR buttons
    if (nconf.get('Human_augmentauion')) {
        promptObj.buttons.push({
            buttonText: TextService.text('Chat with Specialist')[0],
            triggerIntent: 'HUMAN.AUGMENTATION',
            data: {user_type: "user"}
        })
    }

    StateService.addPrompt2(session, promptObj)

    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
    for (let i = 0; i < TextService.text('No Image Result Payload Msg').length; i++) {
        buttonsText.push(TextService.text('No Image Result Payload Msg')[i])
    }

    BotService.sendQuickReplyButtons(session, 'Type a Query Add Filters', buttonsText)
    session.endDialog()
}


module.exports = {
    getDefaultState: getDefaultState,
    getResults: getResults,
    processResults: processResults,
    displayResults: displayResults
}

