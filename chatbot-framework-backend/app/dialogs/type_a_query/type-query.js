var _             = require( 'lodash'                       )
var builder       = require( 'botbuilder'                   )
var Constants     = require( '../../constants'              )
var ApiService    = require( '../../services/ApiService'    )
var StateService  = require( '../../services/StateService'  )
var StateService2 = require( '../../services/StateService2' )
var TextService   = require( '../../services/TextService'   )
var UtilService   = require( '../../services/UtilService'   )
var UserService   = require( '../../services/UserService'   )
var BotService    = require( '../../services/BotService'    )
var TypeAQueryService = require( '../type_a_query/type-a-query-service' )
var toTitleCase   = require( 'to-title-case'                )

module.exports = {
  Label: Constants.Labels.TypeQuery,
  Dialog: [
    function ( session, args, next ) {


      let NODE = Constants.NODE.NLP

      // Fall through
      if ( ! args ) { next(); return; }
      session.sendTyping()
        UserService.setActiveState( session.message.address, true)


        if ( args && args.entities && args.entities.length > 0 && args.entities[0].data && args.entities[0].api_failed) {
            let promptObj = { id : null, buttons : [] }
            if (StateService2.checkResumeShopping(session)) {
                promptObj.buttons.push({
                    buttonText: 'Back To Shopping',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    data: {}
                })
            }
            StateService.addPrompt2(session, promptObj)
            //console.log("postback replied")

            let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
            buttonsText.push(Constants.Labels.Introduction)
            buttonsText.push(Constants.Labels.Help)
            BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )

            /*builder.Prompts.choice( session, TextService.text( 'Api failure message' ), [ Constants.Labels.Introduction, Constants.Labels.Help ], {
                maxRetries: 0,
                listStyle: builder.ListStyle.button
            })*/
            next()
            return

        }


      let guestMode = session.userData.guestMode

      // Check New Query flag or maybe state is not initialized
      if ( args.entities[0].data.newQuery || ! session.userData[ NODE ] ) {
        session.userData[ NODE ] = TypeAQueryService.getDefaultState( NODE )
        // StateService2.resetState( NODE, session )
      }

      // console.log( 'NLP STATE ::> ', session.userData[ NODE ] )


      /**
       * Update State if user input had a new attribute
       */
      if ( args && args.entities && args.entities.length > 0 && args.entities[0].data && args.entities[0].data.userData ) {

        // Remove 'anything' from intent response
        args.entities[0].data.userData = StateService2.sanitizeIntentResults( args.entities[0].data.userData )

        // If entity changed ( apart from 'anything' ), RESET EVERYTHING
          if(session.userData[ NODE ].nlp_reset_flag){
              session.userData[NODE] = TypeAQueryService.getDefaultState(NODE)
              StateService2.overWriteState(NODE, session, args.entities[0].data.userData)
              session.userData[ NODE ].nlp_reset_flag = 0

          } else {

              if (session.userData[NODE].entity && args.entities[0].data.userData.entity && session.userData[NODE].entity !== args.entities[0].data.userData.entity) {
                  session.userData[NODE] = TypeAQueryService.getDefaultState(NODE)
                  // StateService2.resetState( NODE, session )
              }

              // Now only UPDATE new information
              StateService2.overWriteState(NODE, session, args.entities[0].data.userData)
              // console.log( 'AFTER OVERWRITE NLP STATE ::> ', session.userData[ NODE ], ' :: Overwritte By :: ', args.entities[0].data.userData )
          }

      }

      /**
       * Check if state has entity and gender
       */
      if ( ! session.userData[ NODE ].entity ) {

        // Ask for entity
        if( args.entities[0].data.newQuery ) {
          BotService.sendText( session, 'Type a Query Message' )
          // builder.Prompts.text( session, TextService.text( 'Type a Query Message' ) )
        } else {
          BotService.sendText( session, 'No entity message' )
          // builder.Prompts.text( session, TextService.text( 'No entity message' ) )
        }

      } else if ( !session.userData[ NODE ].gender || session.userData[ NODE ].gender.length == 0 ) {

        // Ask for gender
        session.replaceDialog( 'xpresso-bot:type-a-query-ask-gender', args )

      } else {

        let hasBrand = session.userData[ NODE ].brand && session.userData[ NODE ].brand.length > 0
        let hasSize  = session.userData[ NODE ].size  && session.userData[ NODE ].size.length  > 0

        let getUserProfile = null
        if ( ! guestMode ) {

            let user_pref_entity
            if((session.userData[ NODE ].xc_category) && session.userData[ NODE ].xc_category.length>0 ){
                user_pref_entity = session.userData[ NODE ].xc_category[0]
            }else{
                user_pref_entity = session.userData[ NODE ].entity
            }
          //getUserProfile = UserService.getUserProfileByEntity( session.message.address, session.userData[ NODE ].entity,session.userData.Profile_gender )
          getUserProfile = UserService.getUserProfileByEntity( session.message.address, user_pref_entity,session.userData.Profile_gender )
        } else {
          getUserProfile = Promise.resolve( null )
        }

        // UserService.getUserProfileByEntity( session.message.address, session.userData[ NODE ].entity )
        getUserProfile.then( userProfile => {

            console.log("user profile is " + userProfile)

          let brandToSave = []
          let sizeToSave  = []
          let valueToSave = []
          let filterOn    = null

          if ( ! guestMode ) {
            if ( ! userProfile.usersFilterNegative ) {
              userProfile.usersFilterNegative = {
                entity          : null,
                price           : [],
                subcategory     : [],
                features        : [],
                brand           : [],
                details         : [],
                colorsavailable : [],
                gender          : [],
                size            : [],
                xc_category     : []
              }
            }
            brandToSave = _( session.userData[ NODE ][ 'brand' ] ).difference( userProfile[ 'brand' ] ).difference( userProfile.usersFilterNegative[ 'brand' ] ).value()
            sizeToSave  = _( session.userData[ NODE ][ 'size'  ] ).difference( userProfile[ 'size'  ] ).difference( userProfile.usersFilterNegative[ 'size'  ] ).value()
          }

          if ( sizeToSave.length ) {
            filterOn = 'size'
            valueToSave = sizeToSave
          }
          if ( brandToSave.length ) {
            filterOn = 'brand'
            valueToSave = brandToSave
          }

            console.log("valueToSave is  " + valueToSave)

          if ( valueToSave.length > 0 ) {

            let userData = _.cloneDeep( session.userData[ NODE ] )
            let attributeName = valueToSave[0]

            let args = {
              intent     : 'RESULTS.SAVE.FILTER',
              entities   : [ {
                entity: attributeName,
                entityType: 'RESULTS.FILTERER',
                data: { NODE : NODE, state : StateService2.cloneState( NODE, session, { pageNo : 0 } ), filterOn: filterOn, attributeValue : attributeName }
              } ]
            }

            session.replaceDialog( 'xpresso-bot:results-save-filter', args )

          } else {

            if ( ! guestMode ) {
              // Auto apply filters form profile <NOPREFERENCE>
              let brandFilter = _.difference( userProfile[ 'brand' ], userProfile.usersFilterNegative[ 'brand' ] )
              if ( userProfile[ 'brand' ] && userProfile[ 'brand' ].length > 0 && ( ! session.userData[ NODE ].brand || session.userData[ NODE ].brand.length === 0 ) && ( brandFilter.length > 0 ) ) {
                if ( ! _.includes( userProfile[ 'brand' ], '<NOPREFERENCE>' ) && ! session.userData.guestMode ) {
                    //BotService.sendText(session, 'Adding Brand from your user profile: ' + toTitleCase( brandFilter.join( ', ' ) ) )
                  //session.userData[ NODE ].brand = brandFilter
                }
              }

              let sizeFilter = _.difference( userProfile[ 'size' ], userProfile.usersFilterNegative[ 'size' ] )
              if ( userProfile[ 'size'  ] && userProfile[ 'size'  ].length > 0 && ( ! session.userData[ NODE ].size || session.userData[ NODE ].size.length === 0 ) && ( sizeFilter.length > 0 ) ) {
                if ( ! _.includes( userProfile[ 'size' ], '<NOPREFERENCE>' ) && ! session.userData.guestMode ) {
                    //BotService.sendText(session, 'Adding Size from your user profile: ' + toTitleCase( sizeFilter.join( ', ' ) ) )
                 // session.userData[ NODE ].size = sizeFilter
                }
              }
            }

            // Entity and Gender present in the state, make a structured query
            // session.replaceDialog( 'type-a-query-results', args )
            session.userData.currentNode = NODE
            console.log(NODE)
            console.log("\n \n")
            session.replaceDialog( 'xpresso-bot:results-dialog' )

          }

        })
        .catch( err => {
            let promptObj = { id : null, buttons : [] }
            if (StateService2.checkResumeShopping(session)) {
                promptObj.buttons.push({
                    buttonText: 'Back To Shopping',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    data: {}
                })
            }
            StateService.addPrompt2(session, promptObj)
            //console.log("postback replied")

            let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
            buttonsText.push(Constants.Labels.Introduction)
            buttonsText.push(Constants.Labels.Help)
            BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
          session.error( err )
          session.endDialog()
        })

      }

    },
    function ( session, result ) {
      session.reset()
    }]
};
