var _ = require('lodash')
const nconf = require('nconf')
var builder = require('botbuilder')
var Constants = require('../../constants')
var ApiService = require('../../services/ApiService')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UtilService = require('../../services/UtilService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')
var UserService = require('../../services/UserService')

getDefaultState = NODE => {
    let defaultState = {
        entity: null,
        price: [],
        subcategory: [],
        features: [],
        brand: [],
        details: [],
        colorsavailable: [],
        gender: [],
        size: [],
        xc_category: [],
        pageNo: 0,
        pageSize: 10 + 2
    }
    return _.cloneDeep(defaultState)
}


/**
 *
 */
getResults = (state,session) => {
    console.log("the state is ",state)
   // console.log("the session is ", session)
    return ApiService.structuredSearch(state,session)
}

/**
 *
 */
processResults = (session, results) => {
    if(nconf.get('Select_Shop_Carousel')) {
        let ret = ["express","forever21","lkbennett","kohl\'s","macy\'s","renttherunway","nordstorm"]
        let client_names
        let res
        try {
            res = results.Results.filter( val => {
                    let temp = val.client_name
                    let flag
                    flag=ret.indexOf(temp)
                    if(flag!==-1) {
                        console.log(flag)
                        ret.splice(flag,1)
                        console.log(ret)
                        return temp
                    }
            })
            client_names= res.map(val => {
                return val.client_name
            })
        } catch(err) {
            console.log(err)
            client_names=["express","forever21","lkbennett","kohl\'s","macy\'s","renttherunway","nordstorm"]
        }
        session.userData['NLP'].retailers=client_names
    }else if(nconf.get('Select_Shop_Webview')) {

        console.log("saved user preferred client name is ",session.userData.client_name)

    }

    let pageSize = session.userData[session.userData.currentNode].pageSize || 10 + 2
    let pageNo = session.userData[session.userData.currentNode].pageNo || 0
    let suggested_results = 0
    let relaxation_attributes =[]


    //console.log("Result obtained is ",results)

    console.log("is_relaxed_combination_exist",results.is_relaxed_combination_exist)

    //console.log("before",results)
    results.Results = results.Results || []

    //console.log("Results before is ",results)
    console.log("relaxation_attributes is ",results.relaxation_attributes)


    if(results.is_relaxed_combination_exist){


        console.log("relaxation is ",results.relaxation)
        console.log("relaxation entity is ",results.relaxation.entity)
        console.log("relaxation suggested_combination is ",JSON.stringify(results.relaxation.suggested_combination))

        let entity = results.relaxation.entity

        let filter = "filter"

        if(results.relaxation.suggested_combination && results.relaxation.suggested_combination.length > 0 ){

            let relaxed_attribute = results.relaxation.suggested_combination

            if(relaxed_attribute[0] && relaxed_attribute[0].length>1){
                filter = "filter combination"
            }

            let cards = []
            let Postback = "RELAXATION::"
            let ButtonText = ""
            for( let i = 0; i < relaxed_attribute.length; i = i + 3 ) {
                let buttons = []
                if ( i     < relaxed_attribute.length )
                {
                    Postback = "RELAXATION::"
                    Postback = Postback +JSON.stringify(relaxed_attribute[ i])
                    ButtonText = UtilService.getRelaxationButtonText(relaxed_attribute[ i].combination)
                    buttons.push( builder.CardAction.postBack( session,Postback,ButtonText))
                }
                if ( i + 1 < relaxed_attribute.length ){
                    Postback = "RELAXATION::"
                    Postback = Postback +JSON.stringify(relaxed_attribute[ i+1])
                    ButtonText = UtilService.getRelaxationButtonText(relaxed_attribute[ i+1].combination)
                    buttons.push( builder.CardAction.postBack( session,Postback,ButtonText))
                }
                if ( i + 2 < relaxed_attribute.length ){
                    Postback = "RELAXATION::"
                    Postback = Postback +JSON.stringify(relaxed_attribute[ i+2])
                    ButtonText = UtilService.getRelaxationButtonText(relaxed_attribute[ i+2].combination)
                    buttons.push( builder.CardAction.postBack( session,Postback,ButtonText))

                }
                let card = new builder.HeroCard( session ).title( _.startCase(TextService.text( 'Attribute selection Title'))).buttons( buttons )
                cards.push( card )
            }

            if ( cards.length > 0 ) {
                let msg = UtilService.getRelaxationMessage(session,filter,'Relaxation_Message')
                BotService.sendText( session, msg)
                BotService.sendCard( session, cards )
            }else{

                BotService.sendText(session, {
                    text : 'Api failure message',
                    data: {entity: session.userData[session.userData.currentNode].entity}
                })

            }

            session.endDialog()



        }else{

            BotService.sendText(session, {
                text : 'Api failure message',
                data: {entity: session.userData[session.userData.currentNode].entity}
            })

            session.endDialog()



        }

        //Promise.all([retailerPreference]).then({
        return  Promise.resolve("Skip")
        //}).catch(err => {

        // console.log("Select shop promise error " + err)

        //})

        //return Promise.resolve("Skip");

    }else
    {
        //console.log("after",results)

        if (results.Results.length === 0 && results["Suggested Results"] && results["Suggested Results"].length > 0) {
            //session.send( 'No results found for the given search, showing some suggestions instead.' )
            results.Results = results["Suggested Results"] || []
            suggested_results = 1
            relaxation_attributes = results.relaxation_attributes
        }
        //result.Results = _.orderBy( result.Results, 'confidence', 'desc' )

        let pagedResults = UtilService.pageList(results.Results, pageNo, pageSize, null, null)
        return UtilService.createCards(session, pagedResults, suggested_results).then(cards => {

            //    console.log("cards",cards)

            // ANALYTICS
            session.userData.analytics.out.product_response_list = pagedResults

            // The QuickReply buttons
            let buttons = []
            let onLastPage = ( pageSize - 2 ) * ( pageNo + 1 ) >= results.Results.length
            if (!onLastPage) {
                buttons.push(
                    {
                        buttonText: 'Show More',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'NLP.SHOW.MORE',
                        data: {state: {pageNo: pageNo + 1}}
                    }
                )
            }

            buttons = buttons.concat(StateService2.getFiltersButtons(session, session.userData.currentNode, results.Results))

            let promptObj = {id: null, buttons: buttons}


            //Promise.all([retailerPreference]).then({
            return  {cards: cards, promptObj: promptObj, suggested_result: suggested_results,relaxation_attributes :relaxation_attributes}
            //}).catch(err => {

            //  console.log("Select shop promise error " + err)

            // })
        }).catch(err => {

            console.log("some error happened in type a query card " + err)

        })
    }

}

/**
 *
 */
displayResults = (session, results) => {

    let {cards, promptObj, suggested_result,relaxation_attributes} = results

    console.log(" sending result for of card")

    if (cards.length > 0) {

        if (suggested_result) {

            let msg = UtilService.getRelaxationMessage(session,'filter','Suggestion_Message')

            let postBackButtons =[]

            console.log("relaxation_attributes is ",relaxation_attributes)



            if (session.message.source === "facebook") {


                let intent = {
                    intent   : 'NLP-RELAXATION',
                    entities: [{
                        entity: 'Yes',
                        entityType: 'NLP-RELAXATION',
                        data: {

                            to_remove :relaxation_attributes

                        }
                    }]
                }
                let PostBack = 'POSTBACK::' + JSON.stringify(intent)


                postBackButtons.push(
                    {
                        "type": "postback",
                        "title": 'Yes',
                        "payload": PostBack
                    }
                )


                 intent = {
                    intent   : 'NLP-RELAXATION',
                    entities: [{
                        entity: 'No',
                        entityType: 'NLP-RELAXATION',
                        data: {

                            to_remove :relaxation_attributes

                        }
                    }]
                }
                PostBack = 'POSTBACK::' + JSON.stringify(intent)


                postBackButtons.push(
                    {
                        "type": "postback",
                        "title": 'No',
                        "payload": PostBack
                    }
                )

                let payload = {
                    "template_type": "button",
                    "text": msg,
                    "buttons": postBackButtons
                }

                BotService.sendCard(session, postBackButtons, true, "button", msg)
                session.endDialog()


            } else {


                postBackButtons.push({
                    buttonText: TextService.text('Profile Option')[3],
                    postback: PostBack
                })

                //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                let cardButtons = _.map(postBackButtons, button => builder.CardAction.postBack(session, button.postback, button.buttonText))
                cardButtons = cardButtons.concat(_.map(urlButtons, button => builder.CardAction.openUrl(session, button.buttonUrl, button.buttonText)))
                let card = new builder.HeroCard(session).title(TextService.text('Button Template Title')[0]).buttons(cardButtons)
                BotService.sendText(session, msg)
                BotService.sendCard(session, [card])
                session.endDialog()

            }

            session.endDialog()

            return

        } else {


            BotService.sendText(session, {
                text: 'Type a Query Result Message',
                data: {entity: session.userData[session.userData.currentNode].entity}
            })
        }
        if (session.message.source === "facebook") {

            console.log("sending  FB specific ")

            BotService.sendCard(session, cards, true, "generic")

        } else {
            BotService.sendCard(session, cards)
        }
        // session.send( TextService.text( 'Type a Query Result Message', { entity : session.userData[ session.userData.currentNode ].entity } ) )
        // var reply = new builder.Message( session ).attachmentLayout( builder.AttachmentLayout.carousel ).attachments( cards );
        // session.send( reply )

    } else {

        console.log("card is empty")
        //   let promptObj = { id : null, buttons : [] }
        if (StateService2.checkResumeShopping(session)) {
            promptObj.buttons.push({
                buttonText: 'Back To Shopping',
                triggerIntent: 'RESULTS.DIALOG',
                entityType: 'WISHLIST.BACK.TO.SHOPPING',
                data: {}
            })
        }

        if (nconf.get('Human_augmentauion')) {
            promptObj.buttons.push({
                buttonText: TextService.text('Chat with Specialist')[0],
                triggerIntent: 'HUMAN.AUGMENTATION',
                data: {user_type: "user"}
            })
        }

        StateService.addPrompt2(session, promptObj)

        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
        for (let i = 0; i < TextService.text('No Search Result Payload Msg').length; i++) {
            buttonsText.push(TextService.text('No Search Result Payload Msg')[i])
        }
        BotService.sendQuickReplyButtons(session, {text: 'No results Message'}, buttonsText)

        //BotService.sendQuickReplyButtons( session, 'No results Message', [ 'Back To Guided Search', 'Back To Search', Constants.Labels.Introduction, Constants.Labels.Help ] )

        // builder.Prompts.choice( session, TextService.text( 'No results Message' ), [ 'Back To Guided Search', 'Back To Search', Constants.Labels.Introduction, Constants.Labels.Help ], {
        //   maxRetries: 0,
        //   listStyle: builder.ListStyle.button
        // })

        session.endDialog()
        return
    }

    // The QR buttons

    let feedback_button = TextService.text('Product feedback')

    console.log(feedback_button)

    //let skus = _.map(cards, item => item.default_action.url)

    for(let i=0;i<feedback_button.length;i++){

        let triggerIntent

        if(i ==0){

            triggerIntent = 'PRODUCT-LIKE'

        }
        if(i ==1){

            triggerIntent = 'PRODUCT-DISLIKE'

        }

        if(i ==2){

            triggerIntent = 'PRODUCT-FEEDBACK'

        }

        promptObj.buttons.push({
            buttonText: feedback_button[i],
            triggerIntent: triggerIntent,
            entityType: 'Feedback',
            data: {emotion: feedback_button[i]}
        })

    }

    if(typeof session.userData['NLP'].client_name!=='undefined' && session.userData['NLP'].client_name!==null && session.userData['NLP'].client_name.length>0) {
        console.log("\n didnt entered the type a query service ")
        promptObj.buttons.push({
            buttonText: 'Remove Shop',
            triggerIntent: 'SELECT-RETAILER',
            entityType: 'NLP.REMOVE.SHOP',
            data: {state:session.userData['NLP']}
        })
    }  else {
        console.log("\nentered the type a query service ")
        if(nconf.get('Select_Shop_Webview')) {
            let selectionflag = 1
            console.log("client_name flag inside typeaquery is ", session.userData.client_name)
            if(session.userData.client_name.length>0) {
                promptObj.buttons.push({
                buttonText: 'Use Your Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'NLP.USEYOUR.SHOP',
                data: {state:session.userData['NLP']}
                })    
            } else {
                promptObj.buttons.push({
                buttonText: 'Select Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'NLP.SELECT.SHOP',
                data: {state:session.userData['NLP']}
                })
            }
        } else {
            promptObj.buttons.push({
                buttonText: 'Select Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'NLP.SELECT.SHOP',
                data: {state:session.userData['NLP']}
            })
        }        
    }    


    if (session.userData.guestMode) {

        promptObj.buttons.push({
            buttonText: 'Shop 4 me',
            triggerIntent: 'RESULT.TOGGLE',
            entityType: 'WISHLIST.BACK.TO.SHOPPING',
            data: {guest_mode: true}
        })


    } else {

        promptObj.buttons.push({
            buttonText: 'Shop 4 other',
            triggerIntent: 'RESULT.TOGGLE',
            entityType: 'WISHLIST.BACK.TO.SHOPPING',
            data: {guest_mode: false}
        })


    }
    /*
    if (nconf.get('Human_augmentauion')) {
        promptObj.buttons.push({
            buttonText: TextService.text('Chat with Specialist')[0],
            triggerIntent: 'HUMAN.AUGMENTATION',
            data: {user_type: "user"}
        })
    }
    */
    StateService.addPrompt2(session, promptObj)

    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
    for (let i = 0; i < TextService.text('No Search Result Payload Msg').length; i++) {
        buttonsText.push(TextService.text('No Search Result Payload Msg')[i])
    }
    BotService.sendQuickReplyButtons(session, {text: 'Type a Query Add Filters'}, buttonsText)
    
}


module.exports = {
    getDefaultState: getDefaultState,
    getResults: getResults,
    processResults: processResults,
    displayResults: displayResults
}

