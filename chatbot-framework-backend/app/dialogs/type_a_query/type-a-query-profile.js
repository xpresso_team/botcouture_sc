var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var ApiService = require('../../services/ApiService')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var TextService = require('../../services/TextService')
var UserService = require('../../services/UserService')
var BotService = require('../../services/BotService')
var nconf = require('nconf')
const uuidV4 = require('uuid/v4')

module.exports = {
    Dialog: [
        function (session, args, next) {

            session.sendTyping()

            UserService.setActiveState(session.message.address, true)

            let NODE = Constants.NODE.NLP

            let flow = false

            let nlp_reset_flag = session.userData[NODE].nlp_reset_flag

            console.log("session entity ", session.userData[NODE].entity)

            console.log("arg entity ", args.entities[0].data.userData.entity)

            console.log("guest mode is  ", session.userData.guestMode)

            console.log("Nlp reset flag is  ", nlp_reset_flag)

            let userData = args.entities[0].data.userData

            session.userData.currentNode = NODE

            session.userData[NODE].SS_Webview = 0
            session.userData[NODE].SS_Carousel= 1
            userData.SS_Webview = 0
            userData.SS_Carousel = 1
            if(typeof session.userData[NODE].client_name == 'undefined') {
                session.userData[NODE].client_name = []
                console.log("\nclient_name"+session.userData[NODE].client_name+"\n")
            }
            console.log("carousel flag is ",session.userData['NLP'].SS_Carousel)

            if ((session.userData[NODE].entity && args.entities[0].data.userData.entity && session.userData[NODE].entity !== args.entities[0].data.userData.entity) || nlp_reset_flag) {

                flow = true
            } else if (((!session.userData[NODE].entity) && (args.entities[0].data.userData.entity)) || nlp_reset_flag) {

                flow = true

            }

            let entity

            if (args.entities[0].data.userData.entity) {
                entity = args.entities[0].data.userData.entity
            } else {

                entity = session.userData[NODE].entity

            }

            console.log("Flow is ", flow)

            UserService.getUser(session.message.address)
                .then(userObj => {

                    if (!flow) {

                        let guest_mode = session.userData.guestMode

                        let skip_personalisation = false

                        let gender = userObj.gender;

                        session.userData.gender = gender


                        let gender_asked

                        if (userData.gender && userData.gender.length>0) {
                            console.log("Taking gender from user data")
                            //gender_asked = userData.gender.join(',')
                            gender_asked = userData.gender
                        } else {
                            console.log("Taking gender from user session")
                            //gender_asked = session.userData[NODE].gender.join(',')
                            gender_asked = session.userData[NODE].gender

                        }

                        console.log("Flow is ", flow)

                        console.log("gender is ", gender)

                        console.log("gender_asked is ", gender_asked)

                        console.log("NLP defined gender is ",gender_asked)
                        console.log("User Preference gender is ",gender)


                        if ((gender_asked) && (gender) && (gender != "men,women") && (!guest_mode)) {

                            console.log("NLP defined gender is ",gender_asked)
                            console.log("User Preference gender is ",gender)


                           /* if (gender_asked != gender) {
                                skip_personalisation = true
                            }

*/
                            if (!_.includes( gender_asked, gender )) {
                                skip_personalisation = true
                            }

                        }

                        console.log("skip_personalisation is ", skip_personalisation)

                        if (skip_personalisation) {

                            console.log("personalisation skipped")

                            BotService.sendText(session, {
                                text: 'Gender different from profile',
                                data: {
                                    entity: args.entities[0].data.userData.entity,
                                    gender: gender_asked,
                                    profile_gender: gender
                                }
                            })


                            let intent = {
                                intent: 'TYPE.A.QUERY.PERSONALISE',
                                score: 1,
                                entities: [{
                                    entity: entity,
                                    entityType: 'TYPE.A.QUERY.PROFILE',
                                    data: {
                                        gender: gender_asked,
                                        pageNo: 0,
                                        guest_mode: false,
                                        userData: userData,
                                        remove_personalisation: true
                                    }
                                }]
                            }
                            session.replaceDialog('xpresso-bot:type-query-personalise', intent)

                        } else {

                            session.replaceDialog('xpresso-bot:type-query', args)
                        }

                    } else {

                        let url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Profile"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()

                        let msg


                        if ((args) && (args.entities) && (args.entities.length > 0) && (args.entities[0].data) && (args.entities[0].data.NlpMsg) && (args.entities[0].data.NlpMsg == "new_user")) {
                            msg = TextService.text('New user Profile creation msg')[0]

                        } else {
                            msg = TextService.text('Profile NLP message')[0]
                        }


                        if ((!session.userData[NODE].entity) && (!args.entities[0].data.userData.entity)) {

                            console.log("entity missing")
                            BotService.sendText(session, 'No entity message')
                            session.endDialog()
                            return

                        }


                        // Get user's gender


                        let skip_personalisation = false

                        let gender = userObj.gender;

                        let guest_mode = session.userData.guestMode

                        session.userData.gender = gender

                        let gender_asked

                        if (userData.gender && userData.gender.length>0) {
                            console.log("Taking gender from user data")
                            //gender_asked = userData.gender.join(',')
                            gender_asked = userData.gender
                        }

                        if ((gender_asked) && (gender) && (gender != "men,women") ) {

                        /*    if (gender_asked != gender) {
                                skip_personalisation = true
                            }*/

                            if (!_.includes( gender_asked, gender )) {
                                skip_personalisation = true
                            }

                        }

                        console.log("Flow is ", flow)

                        console.log("gender is ", gender)

                        console.log("gender_asked is ", gender_asked)

                        console.log("skip_personalisation is ", skip_personalisation)


                        if (skip_personalisation) {

                            console.log("personalisation skipped")

                            if(!guest_mode) {

                                BotService.sendText(session, {
                                    text: 'Gender different from profile',
                                    data: {
                                        entity: args.entities[0].data.userData.entity,
                                        gender: gender_asked,
                                        profile_gender: gender
                                    }
                                })
                            }


                            let intent = {
                                intent: 'TYPE.A.QUERY.PERSONALISE',
                                score: 1,
                                entities: [{
                                    entity: entity,
                                    entityType: 'TYPE.A.QUERY.PROFILE',
                                    data: {gender: gender_asked, pageNo: 0, guest_mode: true, userData: userData}
                                }]
                            }
                            session.replaceDialog('xpresso-bot:type-query-personalise', intent)

                        } else if (gender && (args) && (args.entities) && (args.entities.length > 0) && (args.entities[0].entityType) && (args.entities[0].entityType === 'USE MY PROFILE')) {

                            let intent = {
                                intent: 'TYPE.A.QUERY.PERSONALISE',
                                score: 1,
                                entities: [{
                                    entity: entity,
                                    entityType: 'TYPE.A.QUERY.PROFILE',
                                    data: {gender: userObj.gender, pageNo: 0, guest_mode: false, userData: userData}
                                }]
                            }
                            session.replaceDialog('xpresso-bot:type-query-personalise', intent)


                        }
                        else if (userObj.gender && !session.userData.guestMode) {

                            let intent = {
                                intent: 'TYPE.A.QUERY.PERSONALISE',
                                score: 1,
                                entities: [{
                                    entity: entity,
                                    entityType: 'TYPE.A.QUERY.PROFILE',
                                    data: {gender: userObj.gender, pageNo: 0, guest_mode: false, userData: userData}
                                }]
                            }
                            session.replaceDialog('xpresso-bot:type-query-personalise', intent)

                        } else {

                            let postBackButtons = []
                            let urlButtons = []
                            if (gender) {
                                if (session.message.source === "facebook") {


                                    let intent = {
                                        intent: 'TYPE.A.QUERY.PERSONALISE',
                                        entities: [{
                                            entity: entity,
                                            entityType: 'TYPE.A.QUERY.PROFILE',
                                            data: {
                                                NlpMsg: null,
                                                gender: userObj.gender,
                                                guest_mode: false,
                                                pageNo: 0,
                                                userData: userData
                                            }
                                        }]
                                    }

                                    let PostBack = 'POSTBACK::' + JSON.stringify(intent)


                                    postBackButtons.push(
                                        {
                                            "type": "postback",
                                            "title": TextService.text('Profile Option')[1],
                                            "payload": PostBack
                                        }
                                    )

                                    postBackButtons.push(
                                        {
                                            "type": "web_url",
                                            "title": TextService.text('Profile Option')[2],
                                            "url": url,
                                            "messenger_extensions": true,
                                            "webview_height_ratio": nconf.get('WebView_size')["Profile"]
                                        }
                                    )


                                } else {


                                    let intent = {
                                        intent: 'TYPE.A.QUERY.PERSONALISE',
                                        entities: [{
                                            entity: entity,
                                            entityType: 'TYPE.A.QUERY.PROFILE',
                                            data: {
                                                NlpMsg: null,
                                                gender: userObj.gender,
                                                guest_mode: false,
                                                pageNo: 0,
                                                userData: userData
                                            }
                                        }]
                                    }


                                    let PostBack = 'POSTBACK::' + JSON.stringify(intent)

                                    postBackButtons.push({
                                        buttonText: TextService.text('Profile Option')[1],
                                        postback: PostBack
                                    })


                                    urlButtons.push({
                                        buttonUrl: url,
                                        buttonText: TextService.text('Profile Option')[2]
                                    })
                                }           


                            } else {

                                if (session.message.source === "facebook") {

                                    postBackButtons.push(
                                        {
                                            "type": "web_url",
                                            "title": TextService.text('Profile Option')[0],
                                            "url": url,
                                            "messenger_extensions": true,
                                            "webview_height_ratio": nconf.get('WebView_size')["Profile"]
                                        }
                                    )

                                    let intent = {
                                        intent: 'TYPE.A.QUERY.PROFILE',
                                        entities: [{
                                            entity: entity,
                                            entityType: "USE MY PROFILE",
                                            data: {
                                                NlpMsg: "new_user",
                                                gender: userObj.gender,
                                                guest_mode: false,
                                                pageNo: 0,
                                                userData: userData
                                            }
                                        }]
                                    }


                                    let PostBack = 'POSTBACK::' + JSON.stringify(intent)

                                    postBackButtons.push(
                                        {
                                            "type": "postback",
                                            "title": TextService.text('Profile Option')[1],
                                            "payload": PostBack
                                        }
                                    )

                                } else {


                                    urlButtons.push({
                                        buttonUrl: url,
                                        buttonText: TextService.text('Profile Option')[0]
                                    })

                                    let intent = {
                                        intent: 'TYPE.A.QUERY.PROFILE',
                                        entities: [{
                                            entity: entity,
                                            entityType: "USE MY PROFILE",
                                            data: {
                                                NlpMsg: "new_user",
                                                gender: userObj.gender,
                                                guest_mode: false,
                                                pageNo: 0,
                                                userData: userData
                                            }
                                        }]
                                    }

                                    let PostBack = 'POSTBACK::' + JSON.stringify(intent)

                                    postBackButtons.push({
                                        buttonText: TextService.text('Profile Option')[1],
                                        postback: PostBack
                                    })


                                }

                            }
                            // 3rd buttonn is common

                            let intent = {
                                intent: 'TYPE.A.QUERY.PERSONALISE',
                                entities: [{
                                    entity: entity,
                                    entityType: 'TYPE.A.QUERY.PROFILE',
                                    data: {
                                        NlpMsg: null,
                                        gender: userObj.gender,
                                        guest_mode: true,
                                        pageNo: 0,
                                        userData: userData
                                    }
                                }]
                            }

                            let PostBack = 'POSTBACK::' + JSON.stringify(intent)

                            if (session.message.source === "facebook") {


                                postBackButtons.push(
                                    {
                                        "type": "postback",
                                        "title": TextService.text('Profile Option')[3],
                                        "payload": PostBack
                                    }
                                )

                                let payload = {
                                    "template_type": "button",
                                    "text": msg,
                                    "buttons": postBackButtons
                                }

                                BotService.sendCard(session, postBackButtons, true, "button", msg)
                                session.endDialog()


                            } else {


                                postBackButtons.push({
                                    buttonText: TextService.text('Profile Option')[3],
                                    postback: PostBack
                                })

                                //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                                let cardButtons = _.map(postBackButtons, button => builder.CardAction.postBack(session, button.postback, button.buttonText))
                                cardButtons = cardButtons.concat(_.map(urlButtons, button => builder.CardAction.openUrl(session, button.buttonUrl, button.buttonText)))
                                let card = new builder.HeroCard(session).title(TextService.text('Button Template Title')[0]).buttons(cardButtons)
                                BotService.sendText(session, msg)
                                BotService.sendCard(session, [card])
                                session.endDialog()

                            }


                            session.endDialog()

                        }

                        session.endDialog()


                    }
                    session.endDialog()

                })
                .catch(err => {

                    let promptObj = {id: null, buttons: []}
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }
                    StateService.addPrompt2(session, promptObj)

                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    let button_arr = [Constants.Labels.Introduction, Constants.Labels.Help]
                    for (let i = 0; i < button_arr.length; i++) {
                        buttonsText.push(button_arr[i])
                    }
                    BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)
                    session.error(err)
                    session.endDialog()
                })

            session.endDialog()


        }]

}