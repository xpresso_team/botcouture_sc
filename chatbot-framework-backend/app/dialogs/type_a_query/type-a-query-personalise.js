var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var ApiService = require('../../services/ApiService')
var TextService = require('../../services/TextService')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UserService = require('../../services/UserService')
var BotService = require('../../services/BotService')
var nconf = require('nconf')
var toTitleCase = require('to-title-case')


module.exports = {

    Dialog: [

        function (session, args, next) {

            if (!args) {
                next();
                return;
            }
            session.sendTyping()
            UserService.setActiveState(session.message.address, true)

            let data = args.entities[0]
            let guest_mode = data.data.guest_mode
            session.userData.guestMode = guest_mode
            let NODE = Constants.NODE.NLP
            let entity = data.entity
            let gender = data.data.gender
            console.log("entity is ", entity)
            let userData = data.data.userData

            //console.log("userData is ",userData)
            //console.log("data is ",data)

            let user_pref_entity
            if((userData.xc_category) && userData.xc_category.length>0 ){
                user_pref_entity = userData.xc_category[0]
            }else{
                user_pref_entity = entity
            }



            if (guest_mode) {

                /*intent = {
                 intent : "TYPE.A.QUERY",
                 score : 1,
                 entities : [ { entity : query, entityType : 'FREE.FORM.TEXT', data : { userData : result } } ]
                 }*/

                BotService.sendText(session, {
                    text: 'Switch to guest mode',
                })

                console.log(" In guest mode NLP")

                let intent = {
                    intent: 'TYPE.A.QUERY',
                    score: 1,
                    entities: [{entity: entity, entityType: 'FREE.FORM.TEXT', data: {userData: userData}}]
                }
                session.replaceDialog('xpresso-bot:type-query', intent)

            } else {

                console.log(" In personalisation mode NLP")


                let userObjPromise = UserService.getUser(session.message.address)
                //let UserProfileByEntity = UserService.getUserProfileByEntity(session.message.address, entity, session.userData.Profile_gender)
                let UserProfileByEntity = UserService.getUserProfileByEntity(session.message.address, user_pref_entity, session.userData.Profile_gender)
                Promise.all([userObjPromise, UserProfileByEntity])
                    .then(([userObj, userProfile]) => {

                        let profile_gender = userObj.gender
                        console.log("In nlp process profile_gender is",profile_gender)
                        profile_gender === 'men,women' ? "no_preference" : profile_gender

                        if(profile_gender) {
                            session.userData.Profile_gender = profile_gender
                        }

                        console.log("In nlp process user profile gender is",session.userData.Profile_gender)

                        profile_gender = userObj.gender;

                        session.userData.gender = profile_gender

                        let brandFilter = _.difference(userProfile['brand'], userProfile.usersFilterNegative['brand'])
                        let sizeFilter = _.difference(userProfile['size'], userProfile.usersFilterNegative['size'])

                        if (_.has(data, 'data.remove_personalisation') && data.data.remove_personalisation) {
                            session.userData.guestMode = true;

                            session.userData[NODE].brand = _.difference(session.userData[NODE].brand, brandFilter)
                            session.userData[NODE].size = _.difference(session.userData[NODE].size, sizeFilter)
                            userData.gender = [gender]

                            let intent = {
                                intent: 'TYPE.A.QUERY',
                                score: 1,
                                entities: [{
                                    entity: entity,
                                    entityType: 'FREE.FORM.TEXT',
                                    data: {userData: userData}
                                }]
                            }
                            session.replaceDialog('xpresso-bot:type-query', intent)

                        } else {


                            //userData.brand = brandFilter
                            userData.size = sizeFilter
                            userData.entity = entity
                            userData.gender = [gender]

                            console.log(JSON.stringify(userProfile))

                            let brand_not_set_flag = true
                            let size_not_set_flag = true

                            let msg_txt = ""

                            if ((brandFilter.length > 0) || (sizeFilter.length > 0)) {

                                if (brandFilter.length > 0) {
                                    msg_txt = msg_txt + 'Adding Brand preference from your user profile for  ' + entity + " : " + toTitleCase(brandFilter.join(', '))
                                    brand_not_set_flag = false
                                }

                                if (sizeFilter.length > 0) {
                                    if (msg_txt) {
                                        msg_txt = msg_txt + "\n"
                                    }
                                    msg_txt = msg_txt + 'Adding Size preference from your user profile for  ' + entity + " : " + toTitleCase(sizeFilter.join(', '))
                                    size_not_set_flag = false
                                }

                            }

                            BotService.sendText(session, msg_txt)

                            msg_txt = ""

                            if ((brand_not_set_flag) || (size_not_set_flag)) {

                                if ((brand_not_set_flag) && (size_not_set_flag)) {
                                    msg_txt = msg_txt + TextService.text('No Preference saved', {
                                            choise: "brand/size",
                                            entity: entity
                                        })[0]

                                } else {

                                    if (brand_not_set_flag) {
                                        msg_txt = msg_txt + TextService.text('No Preference saved', {
                                                choise: "brand",
                                                entity: entity
                                            })[0]
                                    }

                                    if (size_not_set_flag) {
                                        if (msg_txt) {
                                            msg_txt = msg_txt + "\n"
                                        }
                                        msg_txt = msg_txt + TextService.text('No Preference saved', {
                                                choise: "size",
                                                entity: entity
                                            })[0]
                                    }
                                }

                            }

                            BotService.sendText(session, msg_txt)


                            let intent = {
                                intent: 'TYPE.A.QUERY',
                                score: 1,
                                entities: [{
                                    entity: entity,
                                    entityType: 'FREE.FORM.TEXT',
                                    data: {userData: userData}
                                }]
                            }
                            session.replaceDialog('xpresso-bot:type-query', intent)
                        }

                        session.endDialog()
                    })
                    .catch(err => {
                        let promptObj = {id: null, buttons: []}
                        if (StateService2.checkResumeShopping(session)) {
                            promptObj.buttons.push({
                                buttonText: 'Back To Shopping',
                                triggerIntent: 'RESULTS.DIALOG',
                                entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                data: {}
                            })
                        }
                        StateService.addPrompt2(session, promptObj)
                        //console.log("postback replied")

                        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                        buttonsText.push(Constants.Labels.Introduction)
                        buttonsText.push(Constants.Labels.Help)
                        BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)
                        session.error(err)
                        session.endDialog()
                    })
                session.endDialog()
            }
            session.endDialog()

        }]

}
