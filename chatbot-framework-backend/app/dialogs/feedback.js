var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../constants')
var StateService = require('../services/StateService')
var UserService = require('../services/UserService')
var TextService = require('../services/TextService')
var BotService = require('../services/BotService')
var nconf = require('nconf')
const uuidV4 = require('uuid/v4')

module.exports = {
    Label: Constants.Labels.Feedback,
    Dialog: [
        function (session) {


            let user_type

            if (_.has(session, 'userData.status')) {
                if (session.userData.status == "agent") {
                    user_type = "agent"

                } else if (session.userData.status == "user") {
                    user_type = "user"
                } else {
                    user_type = null
                }
            }

            if ((user_type === "user") || (user_type === "agent")) {
                console.log("Feedback reset called by ", user_type)
                //UserService.setBothTimer( session.message.address,null,session)
                UserService.unsetFeedbackTimer(session.message.address)
                UserService.unsetGoodbyeTimer(session.message.address)
                session.endDialog()
                return

            }

            session.sendTyping()

            // session.sendTyping()

            UserService.clearAllNegativeFilter(session.message.address)

            UserService.unsetFeedbackTimer(session.message.address);

            if (session.message.source === "facebook") {

                let urlButtons = []

                let msg = TextService.text('Feedback Solicitation Message')


                let url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Feedback"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()

                urlButtons.push(
                    {
                        "type": "web_url",
                        "title": TextService.text('Rate Bot')[0],
                        "url": url,
                        "messenger_extensions": true,
                        "webview_height_ratio": nconf.get('WebView_size')["Feedback"]
                    }
                )

                BotService.sendCard(session, urlButtons, true, "button", msg)
                session.endDialog()
            } else {

                BotService.sendText(session, 'Feedback Solicitation Message')
                let cards = []
                let card = new builder.HeroCard(session)
                    .buttons([
                        builder.CardAction.postBack(session, 'Feedback', 'Feedback'),
                    ])
                cards.push(card)
                BotService.sendCard(session, cards)
                session.endDialog()
            }
            //}


            session.endDialog()

        }]
};