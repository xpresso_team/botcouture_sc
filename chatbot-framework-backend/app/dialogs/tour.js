var builder      = require( 'botbuilder'              )
var Constants    = require( '../constants'            )
var TextService  = require( '../services/TextService' )
var nconf        = require( 'nconf'                   )
var BotService   = require( '../services/BotService'   )
var _            = require( 'lodash'                      )

//let DEMO_VIDEO_URL = 'http://exp.xpresso.abzooba.com/XC/demo.mp4'
let DEMO_VIDEO_URL = nconf.get( 'DEMO_VIDEO_URL' )

module.exports = {
  Label: Constants.Labels.Tour,
  Dialog: [   
  function ( session ) {
    
    session.sendTyping()

    let text = TextService.text( 'Take a tour Message' )

    var cards = [
      new builder.VideoCard( session )
          .title(TextService.text( 'Intro option')[0])
        .autoloop( true ).autostart( true )
        //.subtitle( text )   // this text is 'What would you like to do?' which will be displayed below
        //.text('Description text Description text Description text Description text Description text Description text.')
        //.image(builder.CardImage.create(session, 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Big_buck_bunny_poster_big.jpg/220px-Big_buck_bunny_poster_big.jpg'))
        .media([
          { url: DEMO_VIDEO_URL }
        ])
        .buttons([
            builder.CardAction.postBack(session, Constants.Labels.Inspiration, Constants.Labels.Inspiration),
            builder.CardAction.postBack(session, Constants.Labels.SearchItem, Constants.Labels.SearchItem),
            builder.CardAction.postBack(session, Constants.Labels.Introduction, Constants.Labels.Introduction),
        ])
    ]

    BotService.sendCard( session, cards[0] )
    session.endDialog()
    
  }]
};