/**
 * Created by aashish_amber on 15/3/17.
 */
var _ = require('lodash')
var builder = require('botbuilder')
const nconf   = require( 'nconf'               )
var Constants = require('../constants')
var StateService = require('../services/StateService')
var StateService2 = require('../services/StateService2')
var UserService = require('../services/UserService')
var TextService = require('../services/TextService')
var BotService = require('../services/BotService')


module.exports = {
    Label: Constants.Labels.Feedback,
    Dialog: [
        function (session, args, next) {

            session.sendTyping()

            UserService.unsetFeedbackTimer(session.message.address)

                session.userData.Feedback = {}

                if (args.entities[0].data.text === 'Thumbs Up') {
                    builder.Prompts.text(session, TextService.text('Positive Feedback Question'))
                    session.userData.Feedback.emotion = 'Positive'
                } else if (args.entities[0].data.text === 'Thumbs Down') {
                    builder.Prompts.text(session, TextService.text('Negative Feedback Question'))
                    session.userData.Feedback.emotion = 'Negative'
                } else {
                    //builder.Prompts.number( session, TextService.text( 'Rate your experience' ),{retryPrompt: TextService.text( 'Valid Rate your experience' )} )
                    session.userData.Feedback.emotion = 'Star Rating'
                    console.log(TextService.text('Rate your experience'))
                    console.log( Constants.Labels.Star_Labels)
                    BotService.sendQuickReplyButtons(session, TextService.text('Rate your experience'), Constants.Labels.Star_Labels)
                }
        },
        function (session, result, next) {

            session.sendTyping()

            console.log("outside this block",result)
            console.log("outside this block state",builder.ResumeReason)

            if (result && result.resumed === builder.ResumeReason.completed) {

                UserService.unsetFeedbackTimer(session.message.address)

                console.log("Inside this block",result)

                UserService.setUserFeedbacState(session.message.address, true)

                let promptObj = {id: null, buttons: []}
                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back To Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)
                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                buttonsText.push(Constants.Labels.Introduction)
                BotService.sendQuickReplyButtons(session, TextService.text('Feedback Response Message')
                , buttonsText)

                console.log("Feedback is ",session.message.text)
                session.userData.Feedback.text = session.message.text
                UserService.sendFeedback(session)
                session.endDialog()
                return

            } else {

                session.reset()

            }

        }
    ]
};