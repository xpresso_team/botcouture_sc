/**
 * Created by aashish_amber on 15/3/17.
 */
var _ = require('lodash')
var builder = require('botbuilder')
const nconf = require('nconf')
var Constants = require('../constants')
var StateService = require('../services/StateService')
var StateService2 = require('../services/StateService2')
var UserService = require('../services/UserService')
var TextService = require('../services/TextService')
var BotService = require('../services/BotService')


module.exports = {
    Label: Constants.Labels.Feedback,
    Dialog: [

        function (session, result, next) {

            // UserService.clearAllNegativeFilter( session.message.address )

            //console.log(session.message)
            //console.log(session.message.address)
            session.sendTyping()

            if (session.message.source === "facebook") {

                let promptObj = {id: null, buttons: []}
                let quick_replies = []

                let element = {
                    "content_type": "text",
                    "title": "",
                    "payload": "Thumbs Up",
                    "image_url": nconf.get("Thumbs_up")
                }

                promptObj.buttons.push({
                    buttonText: "Thumbs Up",
                    triggerIntent: 'FEEDBACK.FB',
                    data: {text: "Thumbs Up"}
                })

                quick_replies.push(element)

                element = {
                    "content_type": "text",
                    "title": "",
                    "payload": "Thumbs Down",
                    "image_url": nconf.get("Thumbs_down")
                }

                promptObj.buttons.push({
                    buttonText: "Thumbs Down",
                    triggerIntent: 'FEEDBACK.FB',
                    data: {text: "Thumbs Down"}
                })

                quick_replies.push(element)

                element = {
                    "content_type": "text",
                    "title": "Rate ",
                    "payload": "Rate your experience",
                    "image_url": nconf.get("Star")
                }

                promptObj.buttons.push({
                    buttonText: "Rate your experience",
                    triggerIntent: 'FEEDBACK.FB',
                    data: {text: "Rate your experience"}
                })

                quick_replies.push(element)
                StateService.addPrompt2(session, promptObj)
                BotService.sendFbQuickReply(session, TextService.text('Feedback Question'), quick_replies)
                session.endDialog()
            } else {


                BotService.sendQuickReplyButtons(session, 'Feedback Question', ['Thumbs Up', 'Thumbs Down', 'Rate your experience'])
            }


        },
        function (session, result, next) {

            session.sendTyping()

            UserService.unsetFeedbackTimer(session.message.address)

            //console.log("outside this block 1st", result)
            //console.log("outside this block state 1st", builder.ResumeReason)

            session.sendTyping()


            if (result && result.resumed === builder.ResumeReason.completed) {
                session.userData.Feedback = {}

                if (result.response.entity === 'Thumbs Up') {
                    builder.Prompts.text(session, TextService.text('Positive Feedback Question'))
                    session.userData.Feedback.emotion = 'Positive'
                } else if (result.response.entity === 'Thumbs Down') {
                    builder.Prompts.text(session, TextService.text('Negative Feedback Question'))
                    session.userData.Feedback.emotion = 'Negative'
                } else {
                    //builder.Prompts.number( session, TextService.text( 'Rate your experience' ),{retryPrompt: TextService.text( 'Valid Rate your experience' )} )
                    session.userData.Feedback.emotion = 'Star Rating'
                    BotService.sendQuickReplyButtons(session, TextService.text('Rate your experience'), Constants.Labels.Star_Labels)
                }

            } else {
                next()
            }

        },
        function (session, result, next) {

            session.sendTyping()

            console.log("outside this block", result)
            console.log("outside this block state", builder.ResumeReason)

            if (result && result.resumed === builder.ResumeReason.completed) {

                UserService.unsetFeedbackTimer(session.message.address)

                console.log("Inside this block", result)

                UserService.setUserFeedbacState(session.message.address, true)

                let promptObj = {id: null, buttons: []}
                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back To Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)
                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                buttonsText.push(Constants.Labels.Introduction)
                BotService.sendQuickReplyButtons(session,TextService.text('Feedback Response Message')
               , buttonsText)

                session.userData.Feedback.text = session.message.text
                UserService.sendFeedback(session)
                session.endDialog()
                return

            } else {

                session.reset()

            }

        }
    ]
};