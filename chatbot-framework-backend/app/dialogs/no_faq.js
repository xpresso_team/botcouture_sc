/**
 * Created by aashish_amber on 13/3/17.
 */
var builder = require('botbuilder')
var Constants = require('../constants')
var TextService = require('../services/TextService')
var UserService = require('../services/UserService')
var request = require('superagent')
var BotService = require('../services/BotService')
var _ = require('lodash')
var StateService = require('../services/StateService')
var StateService2 = require('../services/StateService2')


module.exports = {

    Label: Constants.Labels.FAQ,

    Dialog: [
        function (session, args, next) {
            // Fall through
            if (!args || !args.entities || !( args.entities.length > 0 )) {
                next();
                return;
            }
            BotService.sendText(session, {text: 'No FAQ Message'})
            session.userData.Feedback = {}
            session.userData.Feedback.emotion = 'Negative'

        },
        function (session, result) {

            session.userData.Feedback.text = "FAQ#" + session.message.text
            UserService.sendFeedback(session)


            let promptObj = {id: null, buttons: []}
            if (StateService2.checkResumeShopping(session)) {
                promptObj.buttons.push({
                    buttonText: 'Back To Shopping',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    data: {}
                })
            }
            StateService.addPrompt2(session, promptObj)
            console.log("postback replied")

            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
            buttonsText.push(Constants.Labels.Introduction)
            BotService.sendQuickReplyButtons(session, {text: 'FAQ_ASKED_RESPONSE'}, buttonsText)
            session.endDialog()
        }
    ]
}