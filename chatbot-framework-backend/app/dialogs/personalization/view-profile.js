var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var TextService = require('../../services/TextService')
var UserService = require('../../services/UserService')
var BotService = require('../../services/BotService')
var ResultsService = require('../results/results-service')
const uuidV4 = require('uuid/v4')
var nconf = require('nconf')

module.exports = {
    Label: Constants.Labels.Introduction,
    Dialog: [
        function (session,args) {

            session.sendTyping()

            UserService.setActiveState(session.message.address, true)

            let userObjPromise = UserService.getUser(session.message.address)

            let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.source ? 'on ' + _.startCase(_.toLower(session.message.source)) : ''
            let firstName = userName.split(' ')[0]
            let url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Profile"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()
            console.log("url is "+ url)

            let msg

            Promise.all([userObjPromise])
                .then(([userObj]) => {

                    let gender = userObj.gender
                    session.userData.gender = gender
                    let postBackButtons = []
                    let urlButtons = []

                    if (gender) {

                        msg = TextService.text( 'View profile msg')

                        if (session.message.source === "facebook") {



                            postBackButtons.push(
                                {
                                    "type": "web_url",
                                    "title": Constants.Labels['Persistent Menu Options'][4],
                                    "url": url,
                                    "messenger_extensions": true,
                                    "webview_height_ratio": nconf.get('WebView_size')["Profile"]
                                }
                            )

                            let payload = {
                                "template_type": "button",
                                "text":msg,
                                "buttons": postBackButtons
                            }

                            BotService.sendCard(session, postBackButtons,true,"button",msg)

                        } else {


                            urlButtons.push({
                                buttonUrl: url,
                                buttonText: TextService.text('Profile Option')[2]
                            })

                            //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                            let cardButtons = []
                            cardButtons = cardButtons.concat(_.map(urlButtons, button => builder.CardAction.openUrl(session, button.buttonUrl, button.buttonText)))
                            let card = new builder.HeroCard(session).title(TextService.text('Button Template Title')[0]).buttons(cardButtons)
                            BotService.sendText(session, msg)
                            BotService.sendCard(session, [card])

                        }

                    } else {

                        msg = TextService.text( 'Profile not created yet msg')

                        if (session.message.source === "facebook") {

                            postBackButtons.push(
                                {
                                    "type": "web_url",
                                    "title": TextService.text('Profile Option')[0],
                                    "url": url ,
                                    "messenger_extensions": true,
                                    "webview_height_ratio": nconf.get('WebView_size')["Profile"]
                                }
                            )

                            let payload = {
                                "template_type": "button",
                                "text":msg,
                                "buttons": postBackButtons
                            }

                            BotService.sendCard(session, postBackButtons,true,"button",msg)

                       } else {


                            urlButtons.push({
                                buttonUrl: url,
                                buttonText: TextService.text('Profile Option')[0]
                            })

                            //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                            let cardButtons = []
                            cardButtons = cardButtons.concat(_.map(urlButtons, button => builder.CardAction.openUrl(session, button.buttonUrl, button.buttonText)))
                            let card = new builder.HeroCard(session).title(TextService.text('Button Template Title')[0]).buttons(cardButtons)
                            BotService.sendText(session, msg)
                            BotService.sendCard(session, [card])

                        }

                    }

                    let buttonsText=[]
                    let promptObj = { id : null, buttons : [] }
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }
                    StateService.addPrompt2(session, promptObj)
                    buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons(session, {text: 'Profile not created option'}, buttonsText)
                    session.endDialog()

                })
                .catch(err => {
                    let promptObj = {id: null, buttons: []}
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }
                    StateService.addPrompt2(session, promptObj)
                    //console.log("postback replied")

                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)
                    session.error(err)
                    session.endDialog()
                })

            session.endDialog()

        }]

}