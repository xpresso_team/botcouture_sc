var _            = require( 'lodash'                   )
var builder      = require( 'botbuilder'               )
var Constants    = require( '../constants'             )
var StateService = require( '../services/StateService' )
var UserService  = require( '../services/UserService'  )
var TextService  = require( '../services/TextService'  )
var BotService   = require( '../services/BotService'   )
var ResultsService   = require( './results/results-service'   )

module.exports = {
  Label: Constants.Labels.Goodbye,
  Dialog: [   
  function ( session ) {

      session.sendTyping()

      // StateService.resetPrompt2( session )

      let user_type

      if(_.has(session, 'userData.status')){
          if(session.userData.status == "agent"){
              user_type = "agent"

          }else if (session.userData.status == "user"){
              user_type = "user"
          }else{
              user_type = null
          }
      }

      if((user_type ==="user")||(user_type ==="agent")){
          UserService.setBothTimer( session.message.address, null,session )
          session.endDialog()
          return

      }


      UserService.clearAllNegativeFilter( session.message.address )

    UserService.setActiveState( session.message.address, false )

    BotService.sendText( session, 'End of conversation' );
    session.userData.lastguestMode = session.userData.guestMode;
    ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 )
    //session.userData.guestMode = false;
    session.userData.client_name=[]
    UserService.endCurrentSession( session.message.address, session )

    session.endDialog()

  }]

};