var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var TextService  = require( '../../services/TextService'  )
var UserService  = require( '../../services/UserService'  )
var ApiService   = require( '../../services/ApiService'   )
var UtilService  = require( '../../services/UtilService'  )
var BotService   = require( '../../services/BotService'   )
var toTitleCase  = require( 'to-title-case'               )


module.exports = {
  Label: Constants.Labels.Introduction,
  Dialog: [   
  function ( session, args, next ) {

    if ( ! args ) { next(); return; }
    session.sendTyping()

    let entityType     = args.entities[0].entityType
    let xc_sku         = args.entities[0].data.xc_sku
    let productName    = args.entities[0].data.productName
    let REPLACE_DIALOG = args.entities[0].data.REPLACE_DIALOG

    // Get users wishlist
    UserService.saveToWishlist( session.message.address, xc_sku )
    .then( wishlist => {
      
      //session.replaceDialog( REPLACE_DIALOG, null )

      let promptObj = { id : null, buttons : [] }
      promptObj.buttons.push( { 
        buttonText    : 'View Wishlist',
        triggerIntent : 'WISHLIST.VIEW.ALL.ITEMS', 
        entityType    : 'WISHLIST.VIEW.ALL.ITEMS', 
        data          : { }
      } )

      if (StateService2.checkResumeShopping(session)) {
          promptObj.buttons.push({
              buttonText: 'Back To Shopping',
              triggerIntent: 'RESULTS.DIALOG',
              entityType: 'WISHLIST.BACK.TO.SHOPPING',
              data: {}
          })
      }
      
      promptObj.buttons.push( { 
        buttonText    : 'Back to Search',
        triggerIntent : 'SEARCH-ITEM',
        entityType    : 'SEARCH-ITEM', 
        data          : { }
      } )

      promptObj.buttons.push( { 
        buttonText    : 'Start Over',
        triggerIntent : 'INTRODUCTION',
        entityType    : 'INTRODUCTION', 
        data          : { }
      } )
      
      StateService.addPrompt2( session, promptObj )
      let buttonsText = _.map( promptObj.buttons, button => button.buttonText )

      BotService.sendQuickReplyButtons( session, { text : 'Wishlist Item Save Success', data : { productName : toTitleCase( productName ) } }, buttonsText )
      // builder.Prompts.choice( session, TextService.text( 'Wishlist Item Save Success', { productName : toTitleCase( productName ) } ), buttonsText, {
      //   maxRetries: 0,
      //   listStyle: builder.ListStyle.button
      // })

    })
    .catch( err => {
      console.log( 'WISHLIST ERROR ', err.text )

      let promptObj = { id : null, buttons : [] }
      if (StateService2.checkResumeShopping(session)) {
          promptObj.buttons.push({
              buttonText: 'Back To Shopping',
              triggerIntent: 'RESULTS.DIALOG',
              entityType: 'WISHLIST.BACK.TO.SHOPPING',
              data: {}
          })
      }
      
      promptObj.buttons.push( { 
        buttonText    : 'Back to Search',
        triggerIntent : 'SEARCH-ITEM',
        entityType    : 'SEARCH-ITEM', 
        data          : { }
      } )

      promptObj.buttons.push( { 
        buttonText    : 'Start Over',
        triggerIntent : 'INTRODUCTION',
        entityType    : 'INTRODUCTION', 
        data          : { }
      } )
       
      StateService.addPrompt2( session, promptObj )
      let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
      
      BotService.sendQuickReplyButtons( session, 'Sorry, were unable to save the item to your wishlist due to technical error.', buttonsText )
      session.error( err )
    })

  },
  function ( session, result ) {
    session.reset()
  }]
};