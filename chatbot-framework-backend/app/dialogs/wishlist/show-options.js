var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var TextService = require('../../services/TextService')
var UserService = require('../../services/UserService')
var ApiService = require('../../services/ApiService')
var UtilService = require('../../services/UtilService')
var BotService    = require( '../../services/BotService'    )

module.exports = {
  Label: Constants.Labels.Introduction,
  Dialog: [
    function (session, args, next) {

      if (!args) { next(); return; }
      session.sendTyping()

      let entityType  = args.entities[0].entityType
      let xc_sku      = args.entities[0].data.xc_sku
      let productName = args.entities[0].data.productName
      let type        = args.entities[0].data.type
      let imageUrl    = args.entities[0].data.imageUrl
        console.log(imageUrl)
        console.log("postback deduced")

      if ( _.includes( [ 'TYPE.A.QUERY.MORE.OPTIONS' ], entityType ) ) {

        // Get users wishlist
        UserService.getUsersWishlist(session.message.address)
        .then(wishlist => {

          if ( !wishlist ) wishlist = []
          let wishlistSkus          = _.map( wishlist, item => item.xc_sku )
          let itemAlreadyInWishlist = _.includes( wishlistSkus, xc_sku )

          let promptObj = { id : null, buttons : [] }
          if ( ! itemAlreadyInWishlist ) {
          promptObj.buttons.push({
            buttonText    : 'Save To Wishlist',
            triggerIntent : 'WISHLIST.SAVE.ITEM',
            entityType    : 'WISHLIST.SHOW.OPTIONS.SAVE' ,
            data          : { xc_sku : xc_sku, productName : productName }
            } )
          } else {
            promptObj.buttons.push({
              buttonText    : 'Remove From Wishlist',
              triggerIntent : 'WISHLIST.REMOVE.ITEM',
              entityType    : 'WISHLIST.SHOW.OPTIONS.REMOVE',
              data          : { xc_sku : xc_sku, productName : productName }
            })
          }

         if (StateService2.checkResumeShopping(session)) {
             promptObj.buttons.push({
                 buttonText: 'Back To Shopping',
                 triggerIntent: 'RESULTS.DIALOG',
                 entityType: 'WISHLIST.BACK.TO.SHOPPING',
                 data: {}
             })
         }

          if( type === 'image' ) {
            promptObj.buttons.push({
              buttonText    : 'Results Not Right',
              triggerIntent : 'WISHLIST.THUMBS.DOWN',
              entityType    : 'WISHLIST.THUMBS.DOWN.BUTTON',
              data          : { xc_sku : xc_sku, productName : productName, imageUrl : imageUrl }
            })
          }

            if( type === 'notimage' ) {
                promptObj.buttons.push({
                    buttonText    : 'Results Not Right',
                    triggerIntent : 'WISHLIST.RESULT.THUMBS.DOWN',
                    entityType    : 'WISHLIST.RESULT.THUMBS.DOWN.BUTTON',
                    data          : { xc_sku : xc_sku, productName : productName, imageUrl : imageUrl }
                })
            }

          StateService.addPrompt2(session, promptObj)
            console.log("postback replied")

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          BotService.sendQuickReplyButtons( session, 'Please select an option', buttonsText )

          // builder.Prompts.choice( session, 'Please select an option', buttonsText, {
          //   maxRetries: 0,
          //   listStyle: builder.ListStyle.button
          // })

        })
        .catch( err => {

            let promptObj = { id : null, buttons : [] }
            if (StateService2.checkResumeShopping(session)) {
                promptObj.buttons.push({
                    buttonText: 'Back To Shopping',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    data: {}
                })
            }
            StateService.addPrompt2(session, promptObj)
            //console.log("postback replied")

            let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
            buttonsText.push(Constants.Labels.Introduction)
            buttonsText.push(Constants.Labels.Help)
            BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
          session.error( err )
        })
      }

    },
      function (session, result) {
      session.reset()
    }
  ]
};