var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var TextService  = require( '../../services/TextService'  )
var UserService  = require( '../../services/UserService'  )
var ApiService   = require( '../../services/ApiService'   )
var UtilService  = require( '../../services/UtilService'  )
var BotService   = require( '../../services/BotService'   )
var toTitleCase  = require( 'to-title-case'               )
var nconf        = require( 'nconf'                       )


let IMAGE_RESIZE_API_HOST = nconf.get( 'IMAGE_RESIZE_API_URL' )

module.exports = {
  Label: Constants.Labels.Introduction,
  Dialog: [   
  function ( session, args, next ) {

    session.sendTyping()

    if ( ! args ) { next(); return; }
    let entityType     = args.entities[0].entityType
    let pageNo = 0
    let pageSize = 10
    // Get users wishlist
    UserService.getUsersWishlist( session.message.address )
    .then( wishlist => {
      
      if ( !wishlist ) wishlist = []
      _.reverse( wishlist )
      let pagedResults = UtilService.pageList( wishlist, pageNo, pageSize, null, null )

      let cards = _.map( pagedResults, item => {
        let card = new builder.HeroCard( session )
          .title( _.startCase( item.brand ) + ' - ' + _.startCase( item.productname ) )
          .subtitle('Price: $ ' + parseInt( item.price ) )
          .images([
              builder.CardImage.create(session, 'http://' + IMAGE_RESIZE_API_HOST + '/imagem/recenter_noseg/?url=' + item.image + '&aspect=1.91x1')
          ])
          .buttons([
            //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
            builder.CardAction.postBack(session, 'VIEWITON::'+item.product_url+'::WARN::'+_.startCase(item.client_name), 'View details'),
            builder.CardAction.postBack(session, 'VIEWSIMILAR::' + item.xc_sku, 'View Similar'),
            builder.CardAction.postBack(session, 'WISHLIST::' + item.xc_sku + '::' + item.productname + '::notimage::NULL', 'More Options')
          ])
        return card
      })


      let promptObj = { id : null, buttons : [] }

      if ( pagedResults.length > 0 ) {
        BotService.sendText( session, 'Your Wish List:' )
        BotService.sendCard( session, cards )
        // session.send( 'Your Wish List:' )
        // var reply = new builder.Message( session ).attachmentLayout( builder.AttachmentLayout.carousel ).attachments( cards );
        // session.send( reply )
        if (StateService2.checkResumeShopping(session)) {
            promptObj.buttons.push({
                buttonText: 'Back To Shopping',
                triggerIntent: 'RESULTS.DIALOG',
                entityType: 'WISHLIST.BACK.TO.SHOPPING',
                data: {}
            })
        }
      
        promptObj.buttons.push( { 
          buttonText    : 'Clear Entire Wishlist'           , 
          triggerIntent : 'WISHLIST.CLEAR.WISHLIST'  , 
          entityType    : 'WISHLIST.VIEW.ALL.ITEMS.CLEAR.WISHLIST' , 
          data          : {  }
        } )

      } else {
        BotService.sendText( session, 'Your wishlist is currently empty.' )
        // session.send( 'Your wishlist is currently empty.' )
      }

      promptObj.buttons.push( { 
        buttonText    : 'Continue Shopping'          , 
        triggerIntent : 'SEARCH-ITEM' , 
        entityType    : 'WISHLIST.VIEW.ALL.ITEMS.CONTINUE.SHOPPING'  , 
        data          : {  }
      } )

      StateService.addPrompt2( session, promptObj )

      let buttonsText = _.map( promptObj.buttons, button => button.buttonText );
      buttonsText.push("Wishlist FAQs");
      buttonsText.push("Help")

      BotService.sendQuickReplyButtons( session, 'Please select an option', buttonsText )
    })
    .catch( err => {
        let promptObj = { id : null, buttons : [] }
        if (StateService2.checkResumeShopping(session)) {
            promptObj.buttons.push({
                buttonText: 'Back To Shopping',
                triggerIntent: 'RESULTS.DIALOG',
                entityType: 'WISHLIST.BACK.TO.SHOPPING',
                data: {}
            })
        }
        StateService.addPrompt2(session, promptObj)
        //console.log("postback replied")

        let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
        buttonsText.push(Constants.Labels.Introduction)
        buttonsText.push(Constants.Labels.Help)
        BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
      session.error( err )
    })


  },
  function ( session, result ) {
    session.reset()
  }]
};