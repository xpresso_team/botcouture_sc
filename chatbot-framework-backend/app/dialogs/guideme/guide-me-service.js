var _ = require('lodash')
const nconf = require('nconf')
var builder = require('botbuilder')
var Constants = require('../../constants')
var ApiService = require('../../services/ApiService')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UtilService = require('../../services/UtilService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')

getDefaultState = NODE => {
    let defaultState = {
        entity: null,
        price: [],
        subcategory: [],
        features: [],
        brand: [],
        details: [],
        colorsavailable: [],
        gender: [],
        size: [],
        client_name : '',
        xc_category: [],
        pageNo: 0,
        pageSize: 10 + 2
    }
    return _.cloneDeep(defaultState)
}

/**
 *
 */
getResults = (state,session) => {

    //console.log("Guide me state",JSON.stringify(state))
    return ApiService.getGuideMeResults(state,session)
}

/**
 *
 */
processResults = (session, results) => {

    let NODE = Constants.NODE.GUIDEME;
    session.userData.currentNode = NODE;

    let pageSize = session.userData[session.userData.currentNode].pageSize || 10 + 2
    let pageNo = session.userData[session.userData.currentNode].pageNo || 0
    let suggested_results = 0

    //console.log("results",results)
    results.Results = results.Results || []
    //console.log("results",results)
    if (results.Results.length === 0 && results["Suggested Results"] && results["Suggested Results"].length > 0) {
        BotService.sendText(session, TextService.text('Suggested Results Message'))
        results.Results = results["Suggested Results"] || []
        suggested_results = 1
    }
    //console.log("results",results)
    //result.Results = _.orderBy( result.Results, 'confidence', 'desc' )

    let pagedResults = UtilService.pageList(results.Results, pageNo, pageSize, null, null)
    return UtilService.createCards(session, pagedResults, suggested_results).then(cards => {


        // ANALYTICS
        session.userData.analytics.out.product_response_list = pagedResults

        // The QuickReply buttons
        let buttons = []
        let onLastPage = ( pageSize - 2 ) * ( pageNo + 1 ) >= results.Results.length
        if (!onLastPage) {
            buttons.push(
                {
                    buttonText: 'Show More',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'GUIDE.ME.SHOW.MORE',
                    data: {state: {pageNo: pageNo + 1}}
                }
            )
        }

        buttons = buttons.concat(StateService2.getFiltersButtons(session, session.userData.currentNode, results.Results))

        let promptObj = {id: null, buttons: buttons, suggested_result: suggested_results}

        return {cards: cards, promptObj: promptObj}
    }).catch(err => {

        console.log("some error happened in guide me service card " + err)

    })

}

/**
 *
 */
displayResults = (session, results, suggested_result) => {

    let {cards, promptObj} = results

    if (cards.length > 0) {


        if (suggested_result) {

            BotService.sendText(session, {
                text: 'Guide Me suggested result Msg',
            })

        } else {


            BotService.sendText(session, {
                text: 'Guide Me Result display Msg',
            })
        }

        //BotService.sendText( session, 'Guide Me Result display Msg' )
        if (session.message.source === "facebook") {

            BotService.sendCard(session, cards, true, "generic")

        } else {
            BotService.sendCard(session, cards)
        }

    } else {

        //   let promptObj = { id : null, buttons : [] }

        if (StateService2.checkResumeShopping(session)) {
            promptObj.buttons.push({
                buttonText: 'Back To Shopping',
                triggerIntent: 'RESULTS.DIALOG',
                entityType: 'WISHLIST.BACK.TO.SHOPPING',
                data: {}
            })
        }

        if (nconf.get('Human_augmentauion')) {
            promptObj.buttons.push({
                buttonText: TextService.text('Chat with Specialist')[0],
                triggerIntent: 'HUMAN.AUGMENTATION',
                data: {user_type: "user"}
            })
        }

        StateService.addPrompt2(session, promptObj)

        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
        for (let i = 0; i < TextService.text('No Guide Me Result Payload Msg').length; i++) {
            buttonsText.push(TextService.text('No Guide Me Result Payload Msg')[i])
        }
        BotService.sendQuickReplyButtons(session, {text: 'No results Message'}, buttonsText)
        session.endDialog()
        return
    }

    let feedback_button = TextService.text('Product feedback')

    console.log(feedback_button)

    //let skus = _.map(cards, item => item.default_action.url)

    for(let i=0;i<feedback_button.length;i++){

        let triggerIntent

        if(i ==0){

            triggerIntent = 'PRODUCT-LIKE'

        }
        if(i ==1){

            triggerIntent = 'PRODUCT-DISLIKE'

        }

        if(i ==2){

            triggerIntent = 'PRODUCT-FEEDBACK'

        }

        promptObj.buttons.push({
            buttonText: feedback_button[i],
            triggerIntent: triggerIntent,
            entityType: 'Feedback',
            data: {emotion: feedback_button[i]}
        })

    }

    if(typeof session.userData['GUIDEME'].client_name!=='undefined' && session.userData['GUIDEME'].client_name!==null && session.userData['GUIDEME'].client_name.length>0) {
        console.log("\n didnt entered the type a query service ")
        promptObj.buttons.push({
            buttonText: 'Remove Shop',
            triggerIntent: 'SELECT-RETAILER',
            entityType: 'GUIDEME.REMOVE.SHOP',
            data: {state:session.userData['GUIDEME']}
        })
    }  else {
        if(nconf.get('Select_Shop_Webview')) {
            let selectionflag = 1
            console.log("client_name flag inside typeaquery is ", session.userData.client_name)
            if(session.userData.client_name.length>0) {
                promptObj.buttons.push({
                buttonText: 'Use Your Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'GUIDEME.USEYOUR.SHOP',
                data: {state:session.userData['GUIDEME']}
                })    
            } else {
                promptObj.buttons.push({
                buttonText: 'Select Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'GUIDEME.SELECT.SHOP',
                data: {state:session.userData['GUIDEME']}
                })
            }
        } else {
            promptObj.buttons.push({
                buttonText: 'Select Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'GUIDEME.SELECT.SHOP',
                data: {state:session.userData['GUIDEME']}
            })
        }        
    }


    if (session.userData.guestMode) {

        promptObj.buttons.push({
            buttonText: 'Shop 4 me',
            triggerIntent: 'RESULT.TOGGLE',
            entityType: 'WISHLIST.BACK.TO.SHOPPING',
            data: {guest_mode: true}
        })


    } else {

        promptObj.buttons.push({
            buttonText: 'Shop 4 other',
            triggerIntent: 'RESULT.TOGGLE',
            entityType: 'WISHLIST.BACK.TO.SHOPPING',
            data: {guest_mode: false}
        })
    }

    /*    
    }
    if (nconf.get('Human_augmentauion')) {
        promptObj.buttons.push({
            buttonText: TextService.text('Chat with Specialist')[0],
            triggerIntent: 'HUMAN.AUGMENTATION',
            data: {user_type: "user"}
        })
    }
    */

    StateService.addPrompt2(session, promptObj)
    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
    for (let i = 0; i < TextService.text('No Guide Me Result Payload Msg').length; i++) {
        buttonsText.push(TextService.text('No Guide Me Result Payload Msg')[i])
    }

    BotService.sendQuickReplyButtons(session, 'Guide Me result display Filter Msg', buttonsText)
}


module.exports = {
    getDefaultState: getDefaultState,
    getResults: getResults,
    processResults: processResults,
    displayResults: displayResults
}
