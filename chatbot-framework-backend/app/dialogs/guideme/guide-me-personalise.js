var _           = require( 'lodash'                     )
var builder     = require( 'botbuilder'                 )
var Constants   = require( '../../constants'            )
var ApiService  = require( '../../services/ApiService'  )
var TextService = require( '../../services/TextService' )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var UserService = require( '../../services/UserService' )
var BotService    = require( '../../services/BotService'    )
var ResultsService = require( '../results/results-service'  )
var nconf        = require( 'nconf')
var GuideMeService = require( './guide-me-service'           )
var toTitleCase = require( 'to-title-case'              )


module.exports = {

    Label: Constants.Labels.ExploreCollections,

    Dialog: [

        function ( session, args, next ) {

            if ( !args ) { next(); return; }
            session.sendTyping()
            ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 )
            UserService.setActiveState( session.message.address, true)
            let NODE = Constants.NODE.GUIDEME;
            session.userData.currentNode = NODE;

            let data1 = args.entities[0]
            let guest_mode = session.userData.guestMode
            let entity = data1.data.category
            let gender = data1.data.gender
            let client_name = data1.data.client_name
            console.log("entity is ",entity)

            //session.userData[ NODE ] = GuideMeService.getDefaultState( NODE )
            //session.userData[ NODE ].entity = entity
            //session.userData[ NODE ].xc_category = [ entity ]

            if(guest_mode) {

                BotService.sendText(session, {
                    text: 'Switch to guest mode guide me collection',
                })

                let intent = {
                    intent: 'GUIDE.ME.SIZE',
                    score: 1,
                    entities: [data1]
                }
                session.replaceDialog('xpresso-bot:guide-me-size', intent)
            }else {


                UserService.getUserProfileByEntity(session.message.address, entity,session.userData.Profile_gender).then(userProfile => {
                    let data = { xc_category: '', gender : '', size: '' }

                    let profile_gender
                    if (userProfile && userProfile.user){
                        profile_gender = userProfile.user.gender
                        profile_gender === 'men,women' ? "no_preference" : profile_gender
                    }
                    if(profile_gender) {
                        session.userData.Profile_gender = profile_gender
                    }


                    //console.log("Guide me ",JSON.stringify(userProfile))


                    let brandFilter = _.difference( userProfile[ 'brand' ], userProfile.usersFilterNegative[ 'brand' ] )
                    let sizeFilter = _.difference( userProfile[ 'size' ], userProfile.usersFilterNegative[ 'size' ] )

                    //data.brand = brandFilter
                    data.size = sizeFilter
                    data.xc_category = [ entity ]
                    data.gender = [ gender ]
                    data.client_name = [client_name]
                    //console.log(JSON.stringify(data))

                    let brand_not_set_flag = true
                    let size_not_set_flag = true

                    let msg_txt = ""

                    if((brandFilter.length>0)||(sizeFilter.length>0))
                    {

                        if(brandFilter.length>0)
                        {
                            msg_txt = msg_txt+ 'Adding Brand preference from your user profile for  ' + entity + " : "+ toTitleCase( brandFilter.join( ', ' ) )
                            brand_not_set_flag = false
                        }

                        if(sizeFilter.length>0)
                        {
                            if(msg_txt){
                                msg_txt = msg_txt +"\n"
                            }
                            msg_txt = msg_txt+'Adding Size preference from your user profile for  ' + entity + " : "+ toTitleCase( sizeFilter.join( ', ' ) )
                            size_not_set_flag = false
                        }

                    }

                    BotService.sendText(session,msg_txt)

                    msg_txt = ""

                    if((brand_not_set_flag)||(size_not_set_flag))
                    {

                        if((brand_not_set_flag)&&(size_not_set_flag)){
                            msg_txt = msg_txt + BotService.getrandText(TextService.text('No Preference saved', {
                                    choise: "brand/size",
                                    entity: entity
                                }))

                        }else {

                            if (brand_not_set_flag) {
                                msg_txt = msg_txt + BotService.getrandText(TextService.text('No Preference saved', {
                                        choise: "brand",
                                        entity: entity
                                    }))
                            }

                            if (size_not_set_flag) {
                                if (msg_txt) {
                                    msg_txt = msg_txt + "\n"
                                }
                                msg_txt = msg_txt + BotService.getrandText(TextService.text('No Preference saved', {
                                        choise: "size",
                                        entity: entity
                                    }))
                            }
                        }

                    }

                    BotService.sendText(session,msg_txt)




                    let intent = {
                        intent: 'GUIDE.ME.RESULTS',
                        score: 1,
                        entities: [{
                            entityType: 'GUIDE.ME.PERSONALISE',
                            data: data
                        }]
                    }
                    session.replaceDialog('xpresso-bot:guide-me-results', intent)


                })
                    .catch(err => {
                        let promptObj = {id: null, buttons: []}
                        // if (StateService2.checkResumeShopping(session)) {
                        //     promptObj.buttons.push({
                        //         buttonText: 'Back To Shopping',
                        //         triggerIntent: 'RESULTS.DIALOG',
                        //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        //         data: {}
                        //     })
                        // }
                        StateService.addPrompt2(session, promptObj)
                        //console.log("postback replied")

                        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                        buttonsText.push(Constants.Labels.Introduction)
                        buttonsText.push(Constants.Labels.Help)
                        BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)
                        session.error(err)
                        session.endDialog()
                    })
            }

        }]

}
