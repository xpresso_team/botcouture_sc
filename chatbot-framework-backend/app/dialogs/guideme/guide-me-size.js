var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var ApiService   = require( '../../services/ApiService'   )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var UtilService  = require( '../../services/UtilService'  )
var TextService  = require( '../../services/TextService'  )
var toTitleCase  = require( 'to-title-case'               )
var GuideMeService = require( './guide-me-service'           )
var BotService   = require( '../../services/BotService'   )
var UserService  = require( '../../services/UserService'  )
var ResultsService = require( '../results/results-service'  )


module.exports = {
  Label: Constants.Labels.GuideMe,
  Dialog: [
  function ( session, args, next,callback ) {


    if ( !args || !args.entities || !( args.entities.length > 0 ) ) { next(); return; }
    session.sendTyping()
      // session.userData[Constants.NODE.NLP].nlp_reset_flag=1
      ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 )
      UserService.setActiveState( session.message.address, true)
      let NODE = Constants.NODE.GUIDEME;
      session.userData.currentNode = NODE;

      let entityType = args.entities[0].entityType

      // StateService2.resetState( NODE, session )
      session.userData[ NODE ] = GuideMeService.getDefaultState( NODE )

      let gender   = args.entities[0].data.gender
      let category = args.entities[0].data.category
      let pageNo   = Number( args.entities[0].data.pageNo ) || 0
      let client_name = args.entities[0].data.client_name
      console.log("\n clientname is ",client_name)

      ApiService.getGuideMeSizes( gender, category,session )
      .then( sizeObj => {

        let sizeList = sizeObj.Results.size || []

        if ( sizeList.length > 0 ) {

          sizeList = sizeList.map( size => {
            return { text : size,  postback : 'GUIDEMESIZE::' + gender + '::' + category + '::' + size+'::'+client_name  }
          })

          let pageSize = 30                                  // Actual page size will be 2 less (left for Prev/Next buttons)
          pageSize     = pageSize < 30 ? pageSize : 30      // We can show 3 * 10 buttons on FB in total
          // -2 is the actual space available for items, +1 for zero based pageNos
          let showNext = ( pageSize - 2 ) * ( pageNo + 1) < sizeList.length ? { text : 'Next Page' , postback : 'GUIDEMESIZE::' + gender + '::' + category+'::'+client_name + '::NEXTPAGE::' + ( pageNo + 1 ) } : null
          let showPrevious = null
          let sizeListWindow = UtilService.pageList( sizeList, pageNo, pageSize, showPrevious, showNext )

          let cards = []
          for( let i = 0; i < sizeListWindow.length; i = i + 3 ) {
            let buttons = []
            if ( i     < sizeListWindow.length ) buttons.push( builder.CardAction.postBack( session, sizeListWindow[ i     ].postback, sizeListWindow[ i     ].text ) )      // i know
            if ( i + 1 < sizeListWindow.length ) buttons.push( builder.CardAction.postBack( session, sizeListWindow[ i + 1 ].postback, sizeListWindow[ i + 1 ].text ) )
            if ( i + 2 < sizeListWindow.length ) buttons.push( builder.CardAction.postBack( session, sizeListWindow[ i + 2 ].postback, sizeListWindow[ i + 2 ].text ) )
            let card = new builder.HeroCard( session ).title( _.startCase( gender ) ).buttons( buttons )
            cards.push( card )
          }

          if ( cards.length > 0 ) {
            BotService.sendText( session, { text : 'Product Size display msg', data : { gender : gender, product : category } } )
            BotService.sendCard( session, cards )
          }

          session.endDialog()

        } else {

            let arg = {
              intent   : 'GUIDE.ME.RESULTS',
              score    : 1,
              // entities : [ { entity: args.entities[0].entity, entityType : 'GUIDE.ME.SIZE', data : { userData: { gender : [ gender ], entity : category, xc_category: [ category ], } } } ]
                // entity no longer used in generic api
              entities : [ { entity: args.entities[0].entity, entityType : 'GUIDE.ME.SIZE', data : { gender : [ gender ], xc_category: [ category ] } } ]
            }

            session.replaceDialog( "xpresso-bot:guide-me-results", arg )
            return
        }

      })
      .catch( err => {

          let promptObj = { id : null, buttons : [] }
          // if (StateService2.checkResumeShopping(session)) {
          //     promptObj.buttons.push({
          //         buttonText: 'Back To Shopping',
          //         triggerIntent: 'RESULTS.DIALOG',
          //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
          //         data: {}
          //     })
          // }
          StateService.addPrompt2(session, promptObj)

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          let button_arr = [ Constants.Labels.Introduction, Constants.Labels.Help ]
          for(let i = 0;i<button_arr.length;i++){
              buttonsText.push(button_arr[i])
          }
          BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
        session.error( err )
        session.endDialog()
      })


  }]
}
