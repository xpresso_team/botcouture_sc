/**
 * Created by aashish_amber on 18/3/17.
 */
var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../constants'             )
var StateService = require( '../services/StateService' )
var TextService  = require( '../services/TextService'  )
var BotService   = require( '../services/BotService'   )
var UserService   = require( '../services/UserService'   )

module.exports = {
    Label: Constants.Labels.INTRO_TO_BOT,
    Dialog: [
        function ( session ) {

            session.sendTyping()

            // Reset state at start
            //StateService.resetState( session )
            //StateService.resetPrompt2( session )
            UserService.setActiveState( session.message.address, true)

            let userName    = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.source ? 'on ' + _.startCase( _.toLower( session.message.source ) ) : ''
            let firstName   = userName.split( ' ' )[0]

            console.log(session.message.source)

            if (session.message.source === "facebook") {
                console.log("Sending for ",session.message.source)

                let postBackButtons = []


                postBackButtons.push(
                    {
                        "type": "postback",
                        "title": TextService.text('First Shopping options')[0],
                        "payload": TextService.text('First Shopping postback')[0]
                    }
                )

                postBackButtons.push(
                    {
                        "type": "postback",
                        "title": TextService.text('First Shopping options')[1],
                        "payload": TextService.text('First Shopping postback')[1]
                    }
                )

                let payload = {
                    "template_type": "button",
                    "text":TextService.text('First Shopping Msg'),
                    "buttons": postBackButtons
                }

                BotService.sendCard(session, postBackButtons,true,"button",TextService.text('First Shopping Msg'))
                session.endDialog()

            }else {

                BotService.sendText(session, {text: 'First Shopping Msg'})
                let cards = []
                let card = new builder.HeroCard(session)
                //.title( TextService.text( 'Help option' )[0] )
                    .title(TextService.text('Button Template Title')[0])
                    .buttons([
                        builder.CardAction.postBack(session, TextService.text('First Shopping postback')[0], TextService.text('First Shopping options')[0]),
                        builder.CardAction.postBack(session, TextService.text('First Shopping postback')[1], TextService.text('First Shopping options')[1]),
                    ])
                cards.push(card)
                BotService.sendCard(session, cards)
                session.endDialog()
            }
            session.endDialog()
            console.log("Dialog Ended")

        }]
};

