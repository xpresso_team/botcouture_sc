var _           = require( 'lodash'                  )
var builder     = require( 'botbuilder'              )
var Constants   = require( '../constants'            )
var TextService = require( '../services/TextService' )
var UserService = require( '../services/UserService' )
var BotService  = require( '../services/BotService'  )

module.exports = {
  Label: Constants.Labels.Tour,
  Dialog: [   
  function ( session, args, next ) {

    if ( !args || !args.entities || !( args.entities.length > 0 ) ) { next(); return; }

    session.sendTyping()
    //let currentUser = { active: true }
    let currentUser   = UserService.getUserByAddress( session.message.address )
    // let userName      = currentUser.name
    let emoticonJSON  = args.entities[0].data.emoticonJSON
    let emotionJSON   = args.entities[0].data.emotionJSON
    let emoticonScore = emoticonJSON.score
    let text = args.entities[0].entity

      console.log("User entered quey is ",text)


let userName    = session.message.user && session.message.user.name ? session.message.user.name : 'User'
let channelName = session.message.source ? 'on ' + _.startCase( _.toLower( session.message.source ) ) : ''
let firstName   = userName.split( ' ' )[0]


    //let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
    console.log( "emot ICON JSON -> ", emoticonJSON )
    console.log( "emotionJSON    -> ", emotionJSON, emotionJSON.entities)
    
                    //var emoticonJSON = EmojiUtil.getEmojiJSON(text);
                    if ( true || ( emoticonJSON && emoticonJSON.score ) )  {
                       // if ((emotionJSON) && (emotionJSON.entities) && (emotionJSON.entities.intent) && (emotionJSON.entities.intent.length > 0) && (emotionJSON.entities.intent[0].value)) {
                        if ((emotionJSON) && (emotionJSON.intent) && (emotionJSON.intent.name)) {
                            //switch ( emotionJSON.entities.intent[0].value ) {
                            switch ( emotionJSON.intent.name ) {
                                case "appreciation" :
                                    BotService.sendText( session, { text : 'Response to an Appreciation'} )
                                    session.replaceDialog( 'xpresso-bot:introduction-active-user' )
                                    break;
                                case "criticism" :
                                    BotService.sendText( session, { text : 'Response to a Criticism' } )
                                    session.replaceDialog( 'xpresso-bot:introduction-active-user' )
                                    break;
                                case "greeting" :
                                    console.log("user status is "+currentUser.active)
                                    if (!currentUser.active) {
                                        UserService.setActiveState( session.message.address, true)
                                        currentUser.active = true;
                                        session.replaceDialog( 'xpresso-bot:introduction' )

                                    } else {
                                        BotService.sendText( session, { text : 'Response to a Greeting', data : { user_first_name : firstName } } )
                                        session.replaceDialog( 'xpresso-bot:introduction-active-user' )
                                    }
                                    break;
                                case "end":
                                    // Changed : always ask for feedback on user input such as 'bye'
                                    if ( true || !currentUser.askedFeedback ) {
                                        session.replaceDialog( 'xpresso-bot:feedback' )
                                    } else {
                                        session.replaceDialog( 'xpresso-bot:goodbye' )
                                    }
                                    break;
                                case "need" :
                                    // case when need is there and no intent : escalate to the QA ("I want a car")
                                    // This case is handled already since this recognizer is after the intent recognzier
                                    if (false && ( emoticonJSON && emoticonJSON.score != null ) ) {
                                        // handleTextMessage(startTime, senderID, emoticonJSON.text, emoticonJSON.score);
                                    } else {
                                      BotService.sendText( session, { text : 'Reply for no intent and no emotion'} )
                                        session.replaceDialog( 'xpresso-bot:introduction-active-user' )
                                    }
                                    break;
                                case "Personal" :
                                    BotService.sendText( session, { text : 'Who am I response', data : { user_first_name : firstName } } )
                                    break;
                                case "customer_support" :
                                    let intent = {
                                        intent: 'CUSTOMER-QUERY-RESPONSE',
                                        score: 1,
                                        entities: [{
                                            data: {text :text }
                                        }]
                                    }
                                    session.replaceDialog('xpresso-bot:customer-query-response', intent)
                                    break;
                                // TODO: Setup some mailing system here
                                default:
                                    BotService.sendText( session, { text : 'Reply for no intent and no emotion' } )
                                    break;
                            }

                        } else {
                            if (emoticonScore) {
                                if (emoticonScore > 0.01) {
                                    // Positive
                                    BotService.sendText( session, { text : 'Response to an Appreciation'} )
                                    session.replaceDialog( 'xpresso-bot:introduction-active-user' )
                                } else {
                                    // Negative
                                    BotService.sendText( session, { text : 'Response to a Criticism'} )
                                    session.replaceDialog( 'xpresso-bot:introduction-active-user' )
                                }
                            } else {
                                // console.log("in feedback help checking")
    
                                // THIS IS TAKEN CARE ALAREADY
                                if ( false && ( emoticonJSON && emoticonJSON.score != null) ) {
                                    //handleTextMessage(startTime, senderID, emoticonJSON.text, emoticonJSON.score);
                                } else {
                                    /*
                                    if (IntentUtil.hasIntent(intentJSON)){
                                        currentUser.updateIntent(intentJSON);
                                        quickReplyFlag = "Reply for intent and no emotion without entity"
                                        SendUtil.sendQuickReplyButtons(CreateUtil.createQuickReplyButtons(DataUtil.quickReplyOptions[quickReplyFlag)["tittleMsg"], null, DataUtil.quickReplyOptions[quickReplyFlag)["postbackPayloadMsg"]), DataUtil.quickReplyOptions[quickReplyFlag)["msg"], startTime, senderID);
                                    } else { */
                                        BotService.sendText( session, { text : 'Reply for no intent and no emotion'} )
                                   /*  } */
                                }
                            }
                        }
                    } else {
                        if ((emotionJSON) && (emotionJSON.entities) && (emotionJSON.entities.intent) && (emotionJSON.entities.intent.length > 0) && (emotionJSON.entities.intent[0].value)) {
                            switch (emotionJSON.entities.intent[0].value) {
                                case "greeting" :
                                    //console.log("it is a greeting")
                                    if (!currentUser.active) {
                                        UserService.setActiveState( session.message.address, true)
                                        currentUser.active = true;
                                        session.replaceDialog( 'xpresso-bot:introduction' )
                                    } else {
                                        BotService.sendText( session, { text : 'Response to a Greeting', data : { user_first_name : firstName } } )
                                        session.replaceDialog( 'xpresso-bot:introduction-active-user' )
                                    }
                                    break;
                                case "Personal" :
                                    BotService.sendText( session, { text : 'Who am I response', data : { user_first_name : firstName } } )
                                    break;
                            }
                        } else {
                            if (StringUtil.stringHasSubString(text, 'help')) {
                            } else{
                                BotService.sendText( session, { text : 'Reply for no intent and no emotion' } )
                          }
                        }
                    }

  },
  function ( session, result ) {
      UserService.setActiveState( session.message.address, true)
    session.reset()
  }]
};