/**
 * Created by aashish_amber on 13/3/17.
 */
var builder = require('botbuilder')
var Constants = require('../constants')
var TextService = require('../services/TextService')
var nconf = require('nconf')
var BotService = require('../services/BotService')
var StateService = require('../services/StateService')
var StateService2 = require('../services/StateService2')
var _ = require('lodash')
let HELP_VIDEO = nconf.get('HELP_VIDEO')
let DEMO_VIDEO_URL = nconf.get('DEMO_VIDEO_URL')


module.exports = {

    Label: Constants.Labels.PLAY,

    Dialog: [
        function (session, args, next) {

            // Fall through
            if (!args) {
                next();
                return;
            }
            session.sendTyping()

            console.log("Play")
            //console.log(args)
            let video = args.entities[0].data.video
            let flow = args.entities[0].data.flow
            let text = video + " Help Video"
            let cards = []

            if (flow == "NO_FAQ") {
                let text = video + " Video"
                BotService.sendText(session, text)
            } else {
                BotService.sendText(session, text)
                // session.send(text)
            }

            if (video == TextService.text('Demo video title')[0]) {
                cards = [new builder.VideoCard(session).autoloop(true).autostart(true).media([{url: DEMO_VIDEO_URL}])]
            } else {
                cards = [new builder.VideoCard(session).autoloop(true).autostart(true).media([{url: HELP_VIDEO[video]}])]
            }

            // TODO : Change the following
            let msg = new builder.Message(session).addAttachment(cards[0])
            session.send(msg)

            text = TextService.text('Help option')
            if (flow == "NO_FAQ") {
                // TODO : Change the following
                let promptObj = {id: null, buttons: []}
                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back To Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)

                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                //buttonsText.push(Constants.Labels.Introduction)
                for (let i = 0; i < TextService.text('First Shopping options').length; i++) {
                    buttonsText.push(TextService.text('First Shopping options')[i])
                }
                BotService.sendQuickReplyButtons(session, text, buttonsText)
                session.endDialog()

            } else {

                let promptObj = {id: null, buttons: []}
                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back To Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)
                //console.log("postback replied")

                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)

                let button_arr = ['FAQ', 'Back to Videos', Constants.Labels.Introduction]
                for (let i = 0; i < button_arr.length; i++) {
                    buttonsText.push(button_arr[i])
                }

                BotService.sendQuickReplyButtons(session, text, buttonsText)

                session.endDialog()
            }
            session.endDialog()

        }
    ]
}