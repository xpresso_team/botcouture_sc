var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var TextService  = require( '../../services/TextService'  )
var BotService   = require( '../../services/BotService'   )
var UserService   = require( '../../services/UserService'   )
var ResultsService = require( '../results/results-service'  )

module.exports = {
  Label: Constants.Labels.WelcomeBack,
  Dialog: [   
  function ( session ) {

    session.sendTyping()
    // session.userData[Constants.NODE.NLP].nlp_reset_flag=1
    ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 )
    UserService.setActiveState( session.message.address, true)

    // StateService.resetPrompt2( session )

    let userName    = session.message.user && session.message.user.name ? session.message.user.name : 'User'
    let channelName = session.message.source ? 'on ' + _.startCase( _.toLower( session.message.source ) ) : ''
    let firstName   = userName.split( ' ' )[0]


      if (session.message.source === "facebook") {
          console.log("Sending for ", session.message.source)

          let postBackButtons = []

          // Only show Resume shopping if user has currentNode
          if ( session.userData.currentNode ) {
            postBackButtons.push({
                "type": "postback",
                "title": TextService.text('Returning User option')[0],
                "payload": TextService.text('Returning User Postbacks')[0]
            })
          }

          postBackButtons.push(
              {
                  "type": "postback",
                  "title": TextService.text('Returning User option')[1],
                  "payload": TextService.text('Returning User Postbacks')[1]
              }
          )

          let payload = {
              "template_type": "button",
              "text": TextService.text('Returning User Introduction Message', {user_first_name: firstName})[0],
              "buttons": postBackButtons
          }

          BotService.sendCard(session, postBackButtons, true, "button", TextService.text('Returning User Introduction Message', {user_first_name: firstName})[0])

      } else {


          //BotService.sendText( session, { text : 'New User Introduction Message', data : { user_first_name : firstName } } )
          // session.send( TextService.text( 'New User Introduction Message', { user_first_name : firstName } ) )
          BotService.sendText(session, TextService.text('Returning User Introduction Message', {user_first_name: firstName}))
          let cards = []
    
          let postBackButtons = []
          // Only show Resume shopping if user has currentNode
          if ( session.userData.currentNode ) {
            postBackButtons.push( builder.CardAction.postBack(session, TextService.text('Returning User Postbacks')[0], TextService.text('Returning User option')[0]) )
          }
          postBackButtons.push( builder.CardAction.postBack(session, TextService.text('Returning User Postbacks')[1], TextService.text('Returning User option')[1]) )

          let card = new builder.HeroCard(session)
                    .title(TextService.text('Button Template Title')[0])
                    //.title( TextService.text( 'Intro option' )[0] )
                    //.text(TextService.text( 'Website redirection warning Message')[0].replace("{{client}}", client))
                    //   .buttons([
                    //       //builder.CardAction.postBack(session, Constants.Labels.Tour, Constants.Labels.Tour),
                    //       //builder.CardAction.postBack(session, Constants.Labels.Inspiration, Constants.Labels.Inspiration),
                    //       //builder.CardAction.postBack(session, Constants.Labels.SearchItem, Constants.Labels.SearchItem),
                    //       builder.CardAction.postBack(session, TextService.text('Returning User Postbacks')[0], TextService.text('Returning User option')[0]),
                    //       builder.CardAction.postBack(session, TextService.text('Returning User Postbacks')[1], TextService.text('Returning User option')[1]),
                    //   ])
                    .buttons( postBackButtons )

          cards.push(card)
          BotService.sendCard(session, cards)
      }
    // var reply = new builder.Message( session ).attachmentLayout( builder.AttachmentLayout.carousel ).attachments( cards );
    // session.send( reply )
    session.endDialog()

  }]
};