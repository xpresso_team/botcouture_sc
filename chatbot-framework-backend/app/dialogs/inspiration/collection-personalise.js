var _           = require( 'lodash'                     )
var builder     = require( 'botbuilder'                 )
var Constants   = require( '../../constants'            )
var ApiService  = require( '../../services/ApiService'  )
var TextService = require( '../../services/TextService' )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var UserService = require( '../../services/UserService' )
var BotService    = require( '../../services/BotService'    )
var ResultsService = require( '../results/results-service'  )
var nconf        = require( 'nconf')
var toTitleCase = require('to-title-case')


module.exports = {

    Label: Constants.Labels.ExploreCollections,

    Dialog: [

        function ( session, args, next ) {

            if ( !args ) { next(); return; }
            session.sendTyping()
            console.log('entered collection personalize')
            ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 )
            UserService.setActiveState( session.message.address, true)

            let data = args.entities[0].data
            console.log("\n",data.client_name)
            let guest_mode = session.userData.guestMode
            let NODE = Constants.NODE.COLLECTIONS
            let entity = data.state.entity
            session.userData.currentNode = NODE

            console.log("data is ",data)

            let user_pref_entity
            if((session.userData[NODE].xc_category) && session.userData[NODE].xc_category.length>0 ){
                user_pref_entity = session.userData[NODE].xc_category[0]
            }else{
                user_pref_entity = entity
            }


            if(guest_mode) {

                BotService.sendText(session, {
                    text: 'Switch to guest mode guide me collection',
                })

                let intent = {
                    intent: 'RESULTS.DIALOG',
                    score: 1,
                    entities: [{
                        entityType: 'INSPIRATION.VIEW.COLLECTIONS.PERSONALISE',
                        data: data
                    }]
                }
                session.replaceDialog('xpresso-bot:results-dialog', intent)
            }else {


                UserService.getUserProfileByEntity(session.message.address, user_pref_entity,session.userData.Profile_gender).then(userProfile => {

                    let profile_gender
                    if (userProfile && userProfile.user){
                        profile_gender = userProfile.user.gender
                        profile_gender === 'men,women' ? "no_preference" : profile_gender
                    }

                    if(profile_gender) {
                        session.userData.Profile_gender = profile_gender
                    }

                    let brandFilter = _.difference( userProfile[ 'brand' ], userProfile.usersFilterNegative[ 'brand' ] )
                    let sizeFilter = _.difference( userProfile[ 'size' ], userProfile.usersFilterNegative[ 'size' ] )

                    //data.state.brand = brandFilter
                    data.state.size = sizeFilter

                    //console.log(JSON.stringify(userProfile))


                    let brand_not_set_flag = true
                    let size_not_set_flag = true

                    let msg_txt = ""

                    if((brandFilter.length>0)||(sizeFilter.length>0))
                    {

                        if(brandFilter.length>0)
                        {
                            msg_txt = msg_txt+ 'Adding Brand preference from your user profile for  ' + entity + " : "+toTitleCase( brandFilter.join( ', ' ) )
                            brand_not_set_flag = false
                        }

                        if(sizeFilter.length>0)
                        {
                            if(msg_txt){
                                msg_txt = msg_txt +"\n"
                            }
                            msg_txt = msg_txt+'Adding Size preference from your user profile for  ' + entity + " : "+ toTitleCase( sizeFilter.join( ', ' ) )
                            size_not_set_flag = false
                        }

                    }

                    BotService.sendText(session,msg_txt)

                    msg_txt = ""

                    if((brand_not_set_flag)||(size_not_set_flag))
                    {

                        if((brand_not_set_flag)&&(size_not_set_flag)){
                            msg_txt = msg_txt + BotService.getrandText(TextService.text('No Preference saved', {
                                    choise: "brand/size",
                                    entity: entity
                                }))

                        }else {

                            if (brand_not_set_flag) {
                                msg_txt = msg_txt + BotService.getrandText(TextService.text('No Preference saved', {
                                        choise: "brand",
                                        entity: entity
                                    }))
                            }

                            if (size_not_set_flag) {
                                if (msg_txt) {
                                    msg_txt = msg_txt + "\n"
                                }
                                msg_txt = msg_txt + BotService.getrandText(TextService.text('No Preference saved', {
                                        choise: "size",
                                        entity: entity
                                    }))
                            }
                        }

                    }

                    BotService.sendText(session,msg_txt)



                    let intent = {
                        intent: 'RESULTS.DIALOG',
                        score: 1,
                        entities: [{
                            entityType: 'INSPIRATION.VIEW.COLLECTIONS.PERSONALISE',
                            data: data
                        }]
                    }
                    session.replaceDialog('xpresso-bot:results-dialog', intent)


                })
                    .catch(err => {
                        let promptObj = {id: null, buttons: []}

                        // if (StateService2.checkResumeShopping(session)) {
                        //     promptObj.buttons.push({
                        //         buttonText: 'Back To Shopping',
                        //         triggerIntent: 'RESULTS.DIALOG',
                        //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        //         data: {}
                        //     })
                        // }

                        StateService.addPrompt2(session, promptObj)
                        //console.log("postback replied")

                        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                        buttonsText.push(Constants.Labels.Introduction)
                        buttonsText.push(Constants.Labels.Help)
                        BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)
                        session.error(err)
                        session.endDialog()
                    })
            }

        }]

}
