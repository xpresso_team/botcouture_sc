var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var ApiService   = require( '../../services/ApiService'   )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var UtilService  = require( '../../services/UtilService'  )
var UserService  = require( '../../services/UserService'  )
var BotService   = require( '../../services/BotService'   )
var ResultsService = require( '../results/results-service'  )
var toTitleCase  = require( 'to-title-case'               )

module.exports = {
  Label: Constants.Labels.ViewCollecion,
  Dialog: [
    function ( session, args, next ) {

      // Fall through
      if ( !args ) { next(); return; }
      session.sendTyping()
      // session.userData[Constants.NODE.NLP].nlp_reset_flag=1
      ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 )
      UserService.setActiveState( session.message.address, true);
      let NODE = Constants.NODE.COLLECTIONS;
      session.userData.currentNode = NODE


      let collectionType = _.toLower( args.entities[0].data.collectionType )
      let collectionName = _.toLower( args.entities[0].data.collectionName )
      let gender         = _.toLower( args.entities[0].data.gender         )
      let client_name    =  args.entities[0].data.client_name
      console.log("\n",client_name)
      if(!(_.isArray(client_name))) {
        client_name=client_name.split(",")
        console.log("\n",client_name)
      }
      let pageNo         = args.entities[0].data.pageNo || 0
      let pageSize       = 9 + 2     // 9 for categories and + 2 for previous/next, FB allows only 11 quickreply buttons
      //let prompt2Id      = args.entities[0].prompt2Id
      let entityType     = args.entities[0].entityType

      ApiService.getCollectionCategories( collectionType, gender, collectionName,client_name,session)
      .then( result => {

          let categoryList = result.category_list || []

          // No categories found
          if ( categoryList.length === 0 ) {
            BotService.sendText( session, 'No categories found for the given collection.' )

              let promptObj = { id : null, buttons : [] }
              if (StateService2.checkResumeShopping(session)) {
                  promptObj.buttons.push({
                      buttonText: 'Back To Shopping',
                      triggerIntent: 'RESULTS.DIALOG',
                      entityType: 'WISHLIST.BACK.TO.SHOPPING',
                      data: {}
                  })
              }
              StateService.addPrompt2(session, promptObj)
              //console.log("postback replied")

              let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
              buttonsText.push(Constants.Labels.Introduction)
              buttonsText.push(Constants.Labels.Help)
              BotService.sendQuickReplyButtons( session, { text : 'Take a tour Message' }, buttonsText )
          }

          // Categories found
          if ( categoryList.length > 0 ) {

            // Set up prompt
            let promptObj = { id : null, buttons : [] }

            _.each( categoryList, category => {
              let tmpButton = {
                buttonText    : toTitleCase( category ),
                triggerIntent : 'INSPIRATION.COLLECTIONS.PERSONALISE',
                entityType    : 'VIEW.COLLECTION.FILTERER',
                data          : { NODE : Constants.NODE.COLLECTIONS, resetState : true, state : { collectionType : collectionType, collectionName : collectionName, gender : [ gender ], client_name: client_name, entity : category } }
              }
              promptObj.buttons.push( tmpButton )
            })

            let showNext = {
              buttonText    : 'Next Page',
              triggerIntent : 'VIEW.COLLECTION.CATEGORY',
              entityType    : 'VIEW.COLLECTION.FILTERER.NEXTPAGE',
              data          : { collectionType : collectionType, collectionName : collectionName, gender : gender,client_name : client_name,pageNo : pageNo + 1 }
            }

            let showPrevious = {
              buttonText    : 'Previous Page',
              triggerIntent : 'VIEW.COLLECTION.CATEGORY',
              entityType    : 'VIEW.COLLECTION.FILTERER.PREVIOUSPAGE',
              data          : { collectionType : collectionType, collectionName : collectionName, gender : gender,client_name : client_name, pageNo : pageNo - 1 }
            }

            showNext = ( pageSize - 2 ) * ( pageNo + 1) < categoryList.length ? showNext : null
            showPrevious = pageNo !== 0 && session.message.address.channelId === 'facebook' ? showPrevious : null

            promptObj.buttons = UtilService.pageList( promptObj.buttons, pageNo, pageSize, showPrevious, showNext )

            StateService.addPrompt2( session, promptObj )
            let buttons = _.map( promptObj.buttons, button => button.buttonText )

            BotService.sendQuickReplyButtons( session, 'Please select a category for the \'' + _.startCase( collectionName ) + '\' collection?', buttons )
            // builder.Prompts.choice( session, 'Please select a category for the \'' + _.startCase( collectionName ) + '\' collection?', buttons, {
            //   maxRetries: 0,
            //   listStyle: builder.ListStyle.button
            // } )
            session.endDialog()
          }
          session.endDialog()
      })
      .catch( err => {
          let promptObj = { id : null, buttons : [] }
          if (StateService2.checkResumeShopping(session)) {
              promptObj.buttons.push({
                  buttonText: 'Back To Shopping',
                  triggerIntent: 'RESULTS.DIALOG',
                  entityType: 'WISHLIST.BACK.TO.SHOPPING',
                  data: {}
              })
          }
          StateService.addPrompt2(session, promptObj)
          //console.log("postback replied")

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          buttonsText.push(Constants.Labels.Introduction)
          buttonsText.push(Constants.Labels.Help)
          BotService.sendQuickReplyButtons( session, { text : 'Api failure message' }, buttonsText )
        session.error( err )
      })

    }
  ]
};
