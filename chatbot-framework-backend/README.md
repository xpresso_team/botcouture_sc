# MultiChannel ChatBot

## Contents:
* Installation Guide
* Running Guide
* Coding Guide
* Whitelisting Webview url


## Installation guide
Bot needs to be setup at 3 places:

* Microsoft Bot Framework
* Facebook
* Local System

### Setup MS Bot Framework:
Register a new bot at [MS Bot portal](https://dev.botframework.com/bots/new) 
(Microsoft (outlook.com/live.com) account might be required)

* Name must be globally unique 
* Leave messaging end point blank for now (this will be the ngrok url generated later)
* Click on 'Create Microsoft App ID and password' button
* Click on 'Generate an app password to continue' button
* Copy & save the new password generated and click 'ok' (this will be required later in dev.json)
* Click 'Finish and go back to Bot Framework' button
* Click 'Register' button
* On the bot dashboard, click 'Add' link next to Facebook Messenger under 'Add another channel'
* Leave this new page open

This new page will have instructions about setting up Facebook App and Pages for the bot which are repeated briefly below.

### Setup Facebook:

* Create a [Facebook Page](https://www.facebook.com/pages/create/?ref_type=pages_you_admin)
* Create a [Facebook App](https://developers.facebook.com/) (Select 'Add a New App' from the top right menu)
* Enable Messenger for the FB App
![Enable messenger](https://facebook.botframework.com/Content/AddMessaging_1.png)
* Generate page access token
![Page Token](https://facebook.botframework.com/Content/TokenGeneration.png)
* Setup and validate webhook (Callback URL and Verify token are provided by MS Bot FW in the page opened above)
![Webhook image](https://facebook.botframework.com/Content/SetupWebhook_2.png)
* Enter Facebook credentials (i.e. Facebook Page Id, Facebook App Id, Facebook App Secret, Page Access Token) on MS Bot framework page opened above.
* All (except FB Page Id) are present on FB App dashboard. 
* FB Page Id is a number present at the bottom of the 'About' section in the FB Page
* After completing all of the above mentioned steps save your options by clicking on the save button at the bottom of the page.

### Setup Local System
* Clone and npm install this repository
* Download and run [ngrok](https://ngrok.com/) using following command:
```
ngrok http 6001     ## on Windows (6001 is the port number, can be changed)
./ngrok http 6001 ## on *unix
```
* Copy the url displayed (e.g. https://55c351ef.ngrok.io), the url is also found at http://localhost:4040
* Click 'Edit' in 'Details' section in MS Bot Framework dashboard
* Paste the ngrok url in Configuration > Messaging end point:
e.g.: https://55c351ef.ngrok.io/api/messages 
(also add /api/messages/ after the url)
* ngrok url changes after every restart, hence this Messaging end point must be updated after every ngrok restart.
* Copy config/exp.json to config/dev.json (dev.json is in .gitignore)
* Add your local bot filename(dev) to  Forever.js, so that whenever a restart event is triggered by your dev bot, it won't alert head developers.
* Update MICROSOFT_APP_ID and MICROSOFT_APP_PASSWORD in the dev.json from the MS Bot dashboard (password should be copied from one of the previous steps, or can be regenerated)
* Use the same BOT_PORT as used while running ngrok above (e.g. 6001)
* Update FACEBOOK_PAGE_TOKEN in the dev.json with your  own facebook page token created in previous steps from developers.facebook.com
* Done!


## Running guide
  
Create a 'logs' folder in the same directory as of multichannelchatbot. All of your localbot actions will be logged into this folder
Even though setup is same across all platforms. Running bot is a bit different on windows os

Windows:
Create a .env file in local chatbot directory, then add " --conf,dev " and save it. 
Open command prompt from this local repo and run the following command:
```
node MultiChannelChatBot.js --conf exp >> ./logs/date.log &
```
This will run the bot on your localhost sever and file the logs into date.log.


Click the 'Test' button on the MS Bot framework dashboard to test the connection, it displays a chatbox. You can check if your bot is working by typing any message in that box.

Linux:
To run bot as daemon process, echo your arguments to .env file through
```
echo "--conf,dev" >> .env
```
and run
```
npm start
```
The bot is up and running on your server

To check whether it is running as daemon , execute below command
```
npm run status
```
it should display 2 processes Forever.js and MultiChannelChatBot.js --conf dev running. 

To stop your bot, execute 
```
npm stop
```

## Coding guide
Best way to visualize the Bot codeflow is to think of it as a book.
In a book, Table of Contents is used to lookup and go to the corresponding chapter.

Similarly, in the bot, every single incoming message is translated into an 'intent'.
This intent is used to lookup and trigger the corresponding dialog that the user is shown.

### Config: 
The config subfolder is where we store all our bots whether production or stagin or developer. Be careful while working on files in this folder. 
* Never use any bot(file) other than yours i.e, dev.json in this case.
* Your local bot runs dev.json. Hence any changes you want to impelement in your bot you have to change in dev.json only. 
* If you add any other bot, make sure it has been also added to .gitignore.

### Recognizers:
Translation of message into 'intent' is done by a recognizer.
Recognizer is a function that checks the incoming message (or attachements in the message) and determines if the message is of certain type.
There are specialized recognizers for each type of message:

* Postback recognizer   : checks if the incoming message is a posback button press
* Quickreply recognizer : checks if the message is a quickreply button press
* ExactMatch recognizer : checks if the message is one of the standard keywords
* ImageURL recognizer   : checks if the message is an image url
* Intent recognzier     : checks if the message returns a non null result from the intent api
* Emoji recognizer      : checks if the message is an emoji
* etc.

Each message is passed through each of the recognizers one after the other (in order) untill a recognizer recognizes a message.
Once a recognizers recognizes a message, recognizers below it are not triggered.

Each 'intent' is mapped to a Dialog (like a Table of Contents in a book) which is triggered once a match is found by the recognizer. 
Recognizers sends information to the corresponding dialog using a standard format:

```javascript
{ 
  intent : 'INTENT.NAME',        // Intent name is many-to-one mapped to a dialog
  score  : 1,                    // Score should be either 1 or 0
  entities : [ {                 // 'entities' contains all the information required by the dialog to display messages to the user
    entity     : '',             // entity is generally the user input text (e.g. name of the category that user tapped)
    entityType : '',             // entityType is a collective noun for the type of user input (e.g. GUIDE.ME.CATEGORY if user pressed one of the many category button displayed)
    data       : {}              // Any arbitrary data, that may be passed by the preceding dialog or the recognizer to the next dialog
  }]
}
```
### Dialogs
Dialogs are an Array of function which contain the messages that are sent to the Users.
Each function in the Array waits for user input and triggers the next function in the Array, after recieveing the user's message.
Since we are allowing tha user to enter any message anywhere in the conversation flow, our dialogs are usually just 1 element long.

For creating dialogs, one needs to consider: 

* how many ways user can trigger this dialog (this is where entityType is checked)
* how many ways user can exit the dialog (user can always short circuit the dialog, but thats ok)

For e.g. a dialog that displays a carousel can be triggered by following ways:

* User entered a free form text that had an entity and gender
* User pressed the Show More button
* User is returning from a Brand, Price, Color or Size filter select dialog

### loggly
We use Loggly service for maintaining all our logs. Go to loggly(loggly.xpresso.com)
Each & every query made to the bot and the reults/errors replied by the bot are saved to to loggly for our inspection. You can check for any particular error/query by using the keywords of query. And the time is also logged for each and every query. Loggly is very useful in detecting the bugs & working on them efficiently.  

## Code style suggestions
* Indentation : Spaces not tabs
* 2 spaces for indentation


# Misc

## Whitelisting webview URL
Make a POST request to:
```
https://graph.facebook.com/v2.6/me/messenger_profile?access_token=<PAGE_ACCESS_TOKEN>
```
With header:
```
{"Content-Type":"application/json"}
```
And Body:
```
{
  "whitelisted_domains":[
    "https://1df1100b.ngrok.io"     // <-- Webview server url
  ]
}
```
Whitelisting can be verified by making a GET request to:
```
https://graph.facebook.com/v2.6/me/thread_settings?fields=whitelisted_domains&access_token=<PAGE_ACCESS_TOKEN>
```

Also, domain whitelisting has to be done for showing any of our products on facebook. Make the following curl request from postman(Chrome application).

You can also make a direct curl request from your terminal.
```
curl -X POST -H "Content-Type: application/json" -d '{
  "whitelisted_domains":[
    "https://3c4e96ee.ngrok.io",   // <-- MS bot webhook url
    "https://178883a0.ngrok.io"    // <-- webview server url
  ]
}' "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=<PAGE_ACCESS_TOKEN>" 
```