#!/usr/bin/env bash
# Subscribe an app to get updates for a page
#
# Documentation : https://developers.facebook.com/docs/messenger-platform/webhook-reference/#subscribe
#
# Usage: bash subscribe.sh
# -----------------------------------------------------------------------------

# This is for expAshley
# TODO: Add for EXP and PROD Consumer APP
curl -X POST "https://graph.facebook.com/v2.6/me/subscribed_apps?access_token=EAAYmYtnYyoQBAE4UljSCtaZA78ZC0dKhjQM06YVU4t7rhAg4MlDPDowSQ4eZBdIf2adHFh90nvdsjbFYwnownjYZCbqLY6xf1FRtGcsKWy7ZCuik1iVodnqtFOcZBFOCPLszuwqj4aBV3IgoJAaFk0CjSBJ3Cx2cjJy7oUzkwIFwZDZD"
curl -X POST "https://graph.facebook.com/v2.6/me/subscribed_apps?access_token=EAAUAELiM0hgBAG9zvMQT4VSE03dsv6UZAq2OmU1PIUQLrqlsoEdZCPHBcX0wOsJgo0B1LcdnOV8lkKB9HeCq7LPjuDZAV1x5ci1VxJB44HNDREZB5gZCp187amk8c6cgna4vu9SwAEzJZClmCAeCLUI80adZBnHQ9PBiJqqonXZA7QZDZD"
# This is for Harrods ( Comment the above one and uncomment the below one )
# curl -X POST "https://graph.facebook.com/v2.6/me/subscribed_apps?access_token=EAANnJNJ30QQBAOz8G2vdNZApf1zZCRsxAFnn9o9oWNZAvIZCo3z0DRWp9YALHZBOv9BtBZCAnY3Rzxh0SeuX2KNk7usdZCLWHtWZAwnM7Wuiv00J3sNl3UUgCzFigyWeVCWcHxQPbP9q2RLoEYzpuV5Y1g5ceZCUkD2NBpcyDblqhvQZDZD"

