#!/usr/bin/env bash
# TODO: Uncomment npm clean and install when deploying to server
echo "Starting the Redis server"
/etc/init.d/redis-server start
echo "Redis server started on 6379 port"
now="$(date +'%d_%m_%Y')"
node $ROOT_FOLDER/Forever.js >> ${ROOT_FOLDER}/logs/release_${now}_beta_1.log 2>&1
PID=$!
echo $PID > "scripts/forever.pid"
#printf "Forever's pid:$PID\nUse 'ps -fA | grep 'node src' | grep -v grep' to find node process's pid or \n"
#printf "bash status.sh\n"
