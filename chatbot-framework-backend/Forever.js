'use strict'; //Basically it enables the Javascript strict mode

var forever = require('forever-monitor');
var fs = require('fs');
var nodemailer = require("nodemailer");
var datetime = new Date();
var log_name = datetime.getDate()+'_0'+(datetime.getMonth()+1)+'_'+datetime.getFullYear()
const { exec } = require('child_process');
var args = null;
var mailconfig;
var hostname = '';

try {
    args = fs.readFileSync('.env','utf8');
    mailconfig = JSON.parse(fs.readFileSync('mailclient_secret.json','utf8'));
} catch (err) {
    console.log(err);
}

let authConfig = {
    type: 'OAuth2',
    user: mailconfig.user,
    clientId: mailconfig.client_id,
    clientSecret: mailconfig.client_secret,
    refreshToken: mailconfig.refresh_token,
    accessToken: mailconfig.access_token,
    expires: mailconfig.expires_in
}

let smtpConfig = {
    service: 'gmail',
    auth: authConfig,
    debug : true
}

let transport = nodemailer.createTransport(smtpConfig);

exec('hostname -I', (error, stdout, stderr) => {
   if (error) {
       console.error('Error getting hostname', error);
       return;
   }
    hostname = stdout
});

var child = new (forever.Monitor)('MultiChannelChatBot.js', {
    max: 10,
    silent: false,
    outFile: 'logs/release_'+log_name+'_beta_1_out',
    errFile: 'logs/release_'+log_name+'_beta_1_err',
    append: true,
    args: [args.split(",")[0],args.split(",")[1]],
    killTree: true,
    sourceDir: './'
});

child.on('start', function () {
    fs.writeFileSync('scripts/bot.pid', child.childData.pid, 'utf8');
});

child.on('exit', function () {
    console.log('Bot has exited after 10 restarts');
});

child.on('stderr', function (err) {
    let formattedTextError = err.toString('utf8').split(' ').join('\n');
    let formattedHtmlError = err.toString('utf8').split(' ').join('<br>');
    if( (child.childData.args.toString().split(",")[1]).trim() != "dev"){
        if ( child.times > 0 ) { 
            var texterrorMessage = 'Forever restarting script for ' + child.times + ' time(s)\nBot details: ' + child.childData.args.toString() + '\tHosted at -> '+ hostname.split(' ')[0] + '\nBot Error: ' + formattedTextError;
            var htmlerrorMessage = 'Forever restarting script for <b>' + child.times + '</b> time(s)<br>Bot details: <b>' + child.childData.args.toString() + '</b> Hosted at -> <b>'+ hostname.split(' ')[0] + '</b><br>Bot Error: <b>' + formattedHtmlError + '</b>';
            var emailTitle = "Chatbot restarted at "+ new Date() + ' Hosted on: ' + hostname.split(' ')[0]
        } else {
            var texterrorMessage = 'Error occured in Chatbot' +'\nBot details: ' + child.childData.args.toString() + '\tHosted at -> '+ hostname.split(' ')[0] + '\nBot Error: ' + formattedTextError;
            var htmlerrorMessage = 'Error occured in Chatbot' + '<br>Bot details: <b>' + child.childData.args.toString() + '</b> Hosted at -> <b>'+ hostname.split(' ')[0] + '</b><br>Bot Error: <b>' + formattedHtmlError + '</b>';
            var emailTitle = "Chatbot Error at "+ new Date() + ' Hosted on: ' + hostname.split(' ')[0]
        }
        transport.sendMail({
            from: mailconfig.user,
            to: "shrikrashna.kadam@abzooba.com,gopikrishna.kanugula@abzooba.com",
            subject: emailTitle,
            text: texterrorMessage,
            html: "<p>"+htmlerrorMessage+"</p>"
        }, console.error);
    }
});

child.on('restart', function() {
    // TODO: Write to file when the bot is restarted and delete when done
    // console.log(child);
    fs.writeFileSync('scripts/bot.pid', child.childData.pid, 'utf8');
});

child.start();
