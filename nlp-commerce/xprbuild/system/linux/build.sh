#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Build the dependencies
cd ${ROOT_FOLDER}
mvn clean process-classes package
