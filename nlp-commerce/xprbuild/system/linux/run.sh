#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Run the application
# change permissions of the files
echo "Changing the permissions of elasticsearch setup"
find /opt/nlp_dbs/elasticsearch/ -print | xargs --max-args=1 --max-procs=100 chmod 777
echo "changed the permissions of the elasticsearch setup"
id -u somename &>/dev/null || adduser --disabled-password --gecos "" abzooba
usermod -aG sudo abzooba
# Run the elasticsearch
cd /opt/nlp_dbs/elasticsearch/bin/
su -c "./elasticsearch &" abzooba
# Run neo4j server
cd /opt/nlp_dbs/neo4j/bin/
./neo4j start

# change app diretory
cd ${ROOT_FOLDER}
mvn play2:run

