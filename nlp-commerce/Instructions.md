Steps to set up the code on local system:

Prerequisites:

1. Download and install jdk ie. 1.7 or 1.8 or higher and set the path of the same.
2. Download and install git
3. Download and install mongoDB (3.0 and higher)
4. Download and install apache-maven (version 3 and higher).
5. Download IDE ie. Eclipse,IntelliJ etc.


Run following commands in order to clone the git repo :
1. Get the repository access rights. ie. bitbucket
2. git clone <http url for project> - It will ask for your username and password and will start cloning the project.
3. git branch - It will tell you on which branch you are.
4. git fetch - It will fetch all branch and tag info.
5. git checkout -b <branch name> - It will create a local branch for the branch mentioned and switch to the that branch
6. git checkout <branch name> - It will switch to the branch you mentioned.

After getting the local branch, go to the project folder and execute the commands
1. Execute any one of them, mvn clean install or mvn clean package
package: take the compiled code and package it in its distributable format, such as a JAR.
install: install the package into the local repository, for use as a dependency in other projects locally

Ideally it should set up the project in the local system, in case any error comes contact project owner.


