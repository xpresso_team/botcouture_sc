package controllers;

import java.util.Map;

import org.slf4j.Logger;

import com.abzooba.xcommerce.core.IntentAPI;
import com.abzooba.xcommerce.core.XCLogger;

import play.mvc.Controller;
import play.mvc.Result;

public class Application extends Controller {

	private static Logger logger = XCLogger.getServerLogger();

	public Result getIntent() {
		try {
			if (request().method().equals("GET")) {
				Map<String, String[]> queryParameters = request().queryString();
				IntentAPI intentObj = new IntentAPI();
				return ok(intentObj.intentAPIForChatBot(queryParameters).toString(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
		return ok("{}");
	}

	public Result getStructuredSearch() {
		try {
			if (request().method().equals("GET")) {
				Map<String, String[]> queryParameters = request().queryString();
				IntentAPI intentObj = new IntentAPI();
				return ok(intentObj.structuredSearch(queryParameters).toString(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
		return ok("{}");
	}
}