package controllers;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xcommerce.core.XCLogger;

@Singleton
public class OnStartup {
	private static Logger logger;

	@Inject
	public OnStartup() {
		//		String propsFolder = app.configuration().getString("propsFolder");
		//		System.out.println("Config file:" + propsFolder);
		//		XCEngine.init(propsFolder);
		XCEngine.init("config");
		logger = XCLogger.getServerLogger();
		logger.info("XpressoCommerce Engine Loaded!");
		logger.info("Started");
	}
}