package com.abzooba.xpresso.realtime;

import java.util.LinkedHashMap;
import java.util.Map;

public class TrendingTopicInfo {

    private Map<String, Integer> domainCountMap = new LinkedHashMap<String, Integer>();
    public String currentDomain;
    private double score;
    private int count = 0;

    public TrendingTopicInfo(double score) {
        this.score = score;
    }

    public TrendingTopicInfo(int count, double score) {
        this.count = count;
        this.score = score;
    }

    public TrendingTopicInfo(String domain, double score) {
        this.domainCountMap.put(domain, 1);
        this.score = score;
    }

    public TrendingTopicInfo(String domain, double score, int count) {
        this.domainCountMap.put(domain, 1);
        this.score = score;
        this.count = count;
    }

    public Map<String, Integer> getDomainCountMap() {
        return domainCountMap;
    }

    public void augmentDomainCountMap(String domain, int count) {
        Integer domainCount = domainCountMap.get(domain);
        if (domainCount != null) {
            domainCountMap.put(domain, domainCount.intValue() + count);
        } else {
            domainCountMap.put(domain, count);
        }
    }

    public boolean checkIfDomainExists(String domain) {
        //		System.out.println("DomainCountMap: " + this.domainCountMap);
        return this.domainCountMap.containsKey(domain);
    }

    public String getCurrentDomain() {
        return currentDomain;
    }

    public void setCurrentDomain(String currentDomain) {
        this.currentDomain = currentDomain;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void addCount(int count) {
        this.count += count;
    }

}