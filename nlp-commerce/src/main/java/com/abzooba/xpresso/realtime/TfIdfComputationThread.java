package  com.abzooba.xpresso.realtime;

public class TfIdfComputationThread implements Runnable {

    private String domain;
    private String entity;
    private int totalDocCount = 1;
    private EntityInfoClass entityInfo;

    public TfIdfComputationThread(String entity, int totalDocCount, String domain) {
        this.domain = domain;
        this.entity = entity;
        this.totalDocCount = totalDocCount;
    }

    public TfIdfComputationThread(String entity, EntityInfoClass entityInfo, int totalDocCount, String domain) {
        this.domain = domain;
        this.entity = entity;
        this.entityInfo = entityInfo;
        this.totalDocCount = totalDocCount;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        //		Integer entityDocCount = KeywordRelevanceComputation.getEntityDocCount(entity, domain);
        //		Integer entityFrequency = KeywordRelevanceComputation.getEntityFrequency(entity, domain);
        //
        //		if (entityDocCount != null && entityFrequency != null && totalDocCount > 0) {
        //			double idfScore = entityFrequency.intValue() * Math.log(totalDocCount * 1.0 / entityDocCount.intValue());
        //			KeywordRelevanceComputation.updateEntityScoreInCache(entity, idfScore, domain);
        //			KeywordRelevanceComputation.updateMeanIdfScore(domain, idfScore);
        //		}

        if (entityInfo != null) {
            int entityDocCount = entityInfo.getDocCount();
            int entityFrequency = entityInfo.getFrequency();

            if (entityDocCount > 0 && totalDocCount > 0) {
                double idfScore = entityFrequency * Math.log(totalDocCount * 1.0 / entityDocCount);
                KeywordRelevanceComputation.augmentEntityScore(domain, entity, idfScore);
                KeywordRelevanceComputation.updateMeanIdfScore(domain, idfScore);
            }
        }

    }

}