package com.abzooba.xpresso.realtime;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.abzooba.xpresso.engine.config.XpConfig.Annotations;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpExpression;

public class KeywordRelevanceThread implements Runnable {

    Languages language = null;
    String reviewID = null;
    Annotations annotation = null;
    String domainName = null;
    String subject = null;
    boolean isHistoricalDemoMode = false;
    String reviewStr = null;
    String source = null;
    //	boolean deleteTrendTopic = false;
    //	String trendField = null;

    public KeywordRelevanceThread(Languages language, String reviewID, Annotations annotation, String domainName, String subject, String reviewStr, String date, boolean isHistoricalDemoMode, String source) {
        this.language = language;
        this.reviewID = reviewID;
        this.annotation = annotation;
        this.domainName = domainName;
        this.subject = subject;
        this.isHistoricalDemoMode = isHistoricalDemoMode;
        this.reviewStr = reviewStr;
        this.source = source;
        //		this.deleteTrendTopic = deleteTrendTopic;
        //		this.trendField = trendField;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        XpExpression review = new XpExpression(language, annotation, isHistoricalDemoMode, reviewID, domainName, subject, reviewStr, "twitter".equalsIgnoreCase(source));
        Map<String, Integer> entityMap = review.getEntityMap();
        //		Set<String> alreadyFoundSet = new HashSet<String>();
        //		Set<String> reviewEntities = review.processPipelineForEntities(false);
        //		for (String topicField : reviewEntities) {
        //			topicField = topicField.replaceAll("[^\\w\\s\\-]", "").toLowerCase();
        //			if (!alreadyFoundSet.contains(topicField)) {
        //				KeywordRelevanceComputation.updateEntityDocCount(topicField, domainName);
        //
        //			}
        //			KeywordRelevanceComputation.updateEntityFrequency(topicField, domainName);
        //		}
        //		KeywordRelevanceComputation.updateEntityDocCount(KeywordRelevanceComputation.GENERIC_DOC_STR, domainName);
        //		System.out.println("Review Processed \t" + KeywordRelevanceComputation.updateReviewsProcessed());

        Set<String> alreadyFoundSet = new HashSet<String>();
        //		Set<String> reviewEntities = review.processPipelineForEntities(false);
        Set<String> reviewEntities = entityMap.keySet();
        for (String topic : reviewEntities) {
            topic = topic.replaceAll("[^\\w\\s\\-]", "").toLowerCase();
            if (topic.length() > 60)
                continue;
            if (!alreadyFoundSet.contains(topic.toLowerCase()))
                KeywordRelevanceComputation.augmentEntityDocCount(domainName, topic, 1);
            KeywordRelevanceComputation.augmentEntityFrequency(domainName, topic, 1);
            alreadyFoundSet.add(topic.toLowerCase());
        }
        KeywordRelevanceComputation.augmentEntityDocCount(domainName, KeywordRelevanceComputation.GENERIC_DOC_STR, 1);
        System.out.println("Review Processed \t" + KeywordRelevanceComputation.updateReviewsProcessed());
    }

}