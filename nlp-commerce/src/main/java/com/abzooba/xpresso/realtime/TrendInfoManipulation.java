package com.abzooba.xpresso.realtime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.eclipse.jetty.util.ConcurrentHashSet;

import com.abzooba.xpresso.aspect.domainontology.OntologyUtils;
import com.abzooba.xpresso.aspect.domainontology.XpOntology;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.utils.DatabaseFunctions;
import com.abzooba.xpresso.utils.FileIO;

public class TrendInfoManipulation {

    //	private static Map<String, Double> trendTopicMap = new HashMap<String, Double>();
    private static Map<String, TrendingTopicInfo> trendingTopicMap = new ConcurrentHashMap<String, TrendingTopicInfo>();
    protected static Map<String, DomainTrendingTopicInfo> domainTrendingTopicMap = new ConcurrentHashMap<String, DomainTrendingTopicInfo>();
    protected static Map<String, AspectTrendInfo> trendingAspectMap = new ConcurrentHashMap<String, AspectTrendInfo>();
    private static Set<String> inProcessTopicDomainSet = new ConcurrentHashSet<String>();
    protected static List<String> intermediateTopicList = new ArrayList<String>();
    //	private static double decayFactor = 0.001;
    //	private static double threshold = 0.5;
    //	private static double boostFactor = 2.0;

    public static void addToIntermediateList(String line) {
        synchronized (intermediateTopicList) {
            intermediateTopicList.add(line);
        }
    }

    public static void updateTopicDomainSet(String domain, boolean isDelete) {
        if (isDelete)
            inProcessTopicDomainSet.remove(domain);
        else
            inProcessTopicDomainSet.add(domain);
    }

    public static boolean isDomainPresent(String domain) {
        return inProcessTopicDomainSet.contains(domain);
    }

    //	public static void init() {
    //		if (DatabaseFunctions.isEstablishedConnection) {
    //			String sqlQuery = "SELECT topic, score FROM tbltrendtopic";
    //			ResultSet rs = XpEngine.dbFunction.getResultSet(sqlQuery);
    //			try {
    //				while (rs.next()) {
    //					String topic = rs.getString(1);
    //					String score = rs.getString(2);
    //					trendTopicMap.put(topic, Double.parseDouble(score));
    //				}
    //			} catch (SQLException e) {
    //				// TODO Auto-generated catch block
    //				e.printStackTrace();
    //			}
    //		}
    //	}

    public static void init() {
        if (XpConfig.TREND_BATCH_RUN)
            return;
        if (DatabaseFunctions.isEstablishedConnection) {
            String sqlQuery = "SELECT topic, count,score,domain FROM tbltrendtopic;";
            ResultSet rs = XpEngine.dbFunction.getResultSet(sqlQuery);
            try {
                while (rs.next()) {
                    String topic = rs.getString(1);
                    String topicCount = rs.getString(2);
                    String score = rs.getString(3);
                    String domain = rs.getString(4);
                    switch (XpConfig.TREND_TYPE.intValue()) {
                        case 1:
                            TrendingTopicInfo topicInfo = new TrendingTopicInfo(domain, Double.parseDouble(score));
                            trendingTopicMap.put(topic, topicInfo);
                            break;
                        case 3:
                            if (!domainTrendingTopicMap.containsKey(domain)) {
                                DomainTrendingTopicInfo domainTopicInfo = new DomainTrendingTopicInfo(topic, Integer.parseInt(topicCount), Double.parseDouble(score), domain);
                                domainTrendingTopicMap.put(domain, domainTopicInfo);
                            } else {
                                DomainTrendingTopicInfo domainTopicInfo = domainTrendingTopicMap.get(domain);
                                domainTopicInfo.addTrendTopic(topic, Integer.parseInt(topicCount), Double.parseDouble(score));
                            }
                            break;
                        default:
                            if (!domainTrendingTopicMap.containsKey(domain)) {
                                DomainTrendingTopicInfo domainTopicInfo = new DomainTrendingTopicInfo(topic, Integer.parseInt(topicCount), Double.parseDouble(score), domain);
                                domainTrendingTopicMap.put(domain, domainTopicInfo);
                            } else {

                            }
                    }

                    //					Map<String, TrendingTopicInfo> tempTrendTopicMap = domainTrendingTopicMap.get(domain);
                    //					if (tempTrendTopicMap != null) {
                    //						tempTrendTopicMap.put(topic, topicInfo);
                    //					} else {
                    //						tempTrendTopicMap = domainTrendingTopicMap.get(domain);
                    //						tempTrendTopicMap.put(topic, topicInfo);
                    //						domainTrendingTopicMap.put(domain, tempTrendTopicMap);
                    //					}

                }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    //	public static Double getTrendInfo(String topic) {
    //		if (topic == null)
    //			return null;
    //		return trendTopicMap.get(topic);
    //	}

    public static TrendingTopicInfo getTrendTopicInfo(String topic) {
        if (topic == null)
            return null;
        return trendingTopicMap.get(topic);
    }

    public static AspectTrendInfo getAspectTrendInfo(String aspect) {
        if (aspect == null)
            return null;
        return trendingAspectMap.get(aspect);
    }

    //	public static void updateTrendInfo(String topic, double value, boolean deleteTrendTopic) {
    //		if (topic != null && topic.equalsIgnoreCase("Overall"))
    //			return;
    //		boolean updatedTopic = false;
    //		synchronized (trendTopicMap) {
    //			Map<String, Double> tempTopicTrendMap = new HashMap<String, Double>();
    //			tempTopicTrendMap.putAll(trendTopicMap);
    //			for (Entry<String, Double> topicScorePair : tempTopicTrendMap.entrySet()) {
    //				double tempScore = topicScorePair.getValue().doubleValue();
    //				String topicStr = topicScorePair.getKey();
    //				tempScore *= (1 - XpConfig.DECAY_FACTOR);
    //				if (tempScore < XpConfig.TREND_THRESHOLD && deleteTrendTopic && !topicStr.equalsIgnoreCase(topic)) {
    //					trendTopicMap.remove(topicStr);
    //				} else {
    //					if (topicStr.equalsIgnoreCase(topic)) {
    //						updatedTopic = true;
    //						tempScore += value;
    //					}
    //					trendTopicMap.put(topicStr, Double.valueOf(tempScore));
    //				}
    //			}
    //			if (!updatedTopic)
    //				trendTopicMap.put(topic, Double.valueOf(value));
    //		}
    //	}

    public static void updateTrendInfo(String topic, double value, boolean deleteTrendTopic) {
        if (topic != null && topic.equalsIgnoreCase("Overall"))
            return;
        boolean updatedTopic = false;
        Map<String, TrendingTopicInfo> tempTopicTrendMap = new HashMap<String, TrendingTopicInfo>();
        tempTopicTrendMap.putAll(trendingTopicMap);
        for (Entry<String, TrendingTopicInfo> topicScorePair : tempTopicTrendMap.entrySet()) {
            double tempScore = topicScorePair.getValue().getScore();
            String topicStr = topicScorePair.getKey().toLowerCase();
            tempScore *= (1 - XpConfig.DECAY_FACTOR);
            if (tempScore < XpConfig.TREND_THRESHOLD && deleteTrendTopic && !topicStr.equalsIgnoreCase(topic)) {
                trendingTopicMap.remove(topicStr);
            } else {
                if (topicStr.equalsIgnoreCase(topic)) {
                    updatedTopic = true;
                    topicScorePair.getValue().addCount(1);
                    topicScorePair.getValue().augmentDomainCountMap("generic", 1);
                    tempScore += value;
                }
                topicScorePair.getValue().setScore(tempScore);
                trendingTopicMap.put(topicStr, topicScorePair.getValue());
            }
        }
        if (!updatedTopic) {
            //			TrendingTopicInfo topicInfo = new TrendingTopicInfo(null, value, 1);
            //			trendingTopicMap.put(topic.toLowerCase(), topicInfo);

            if (XpConfig.IDF_DOWNGRADE) {
                double entityAbundance = KeywordRelevanceComputation.computeEntityAbundance(topic, "generic");
                addToIntermediateList(topic.toLowerCase() + "\t" + entityAbundance + "\t" + (value / entityAbundance));
                //					System.out.println("entity : " + topic + " abundance : " + entityAbundance + " tempScore : " + value / entityAbundance);
                TrendingTopicInfo topicInfo = new TrendingTopicInfo("generic", value / entityAbundance, 1);
                //				topicInfo.augmentDomainMap(domain, 1);
                trendingTopicMap.put(topic.toLowerCase(), topicInfo);
            } else {
                TrendingTopicInfo topicInfo = new TrendingTopicInfo("generic", value, 1);
                //				topicInfo.augmentDomainMap(domain, 1);
                trendingTopicMap.put(topic.toLowerCase(), topicInfo);
            }
        }
    }

    //	public static void updateTrendInfo(String topic, double value, boolean deleteTrendTopic, String domain) {
    //		if (domain == null) {
    //			updateTrendInfo(topic, value, deleteTrendTopic);
    //			return;
    //		}
    //		if (topic != null && topic.equalsIgnoreCase("Overall"))
    //			return;
    //		boolean updatedTopic = false;
    //		synchronized (trendTopicMap) {
    //			Map<String, Double> tempTopicTrendMap = new HashMap<String, Double>();
    //			tempTopicTrendMap.putAll(trendTopicMap);
    //			for (Entry<String, Double> topicScorePair : tempTopicTrendMap.entrySet()) {
    //				double tempScore = topicScorePair.getValue().doubleValue();
    //				String topicStr = topicScorePair.getKey();
    //				tempScore *= (1 - XpConfig.DECAY_FACTOR);
    //
    //				if (tempScore < XpConfig.TREND_THRESHOLD && deleteTrendTopic && !topicStr.equalsIgnoreCase(topic)) {
    //					trendTopicMap.remove(topicStr);
    //				} else {
    //					if (topicStr.equalsIgnoreCase(topic)) {
    //						updatedTopic = true;
    //						if (XpConfig.IDF_DOWNGRADE) {
    //							//							Double idfScore = KeywordRelevanceComputation.computeIDFScoreForEntity(topic, domain);
    //							//							tempScore += value * idfScore.doubleValue();
    //							double entityAbundance = KeywordRelevanceComputation.computeEntityAbundance(topic, domain);
    //							tempScore += value / entityAbundance;
    //							addToIntermediateList(topic + "\t" + entityAbundance + "\t" + tempScore);
    //							//							System.out.println("entity : " + topic + " abundance : " + entityAbundance + " tempScore : " + tempScore);
    //						} else
    //							tempScore += value;
    //					}
    //					trendTopicMap.put(topicStr, Double.valueOf(tempScore));
    //				}
    //			}
    //			if (!updatedTopic) {
    //				if (XpConfig.IDF_DOWNGRADE) {
    //					double entityAbundance = KeywordRelevanceComputation.computeEntityAbundance(topic, domain);
    //					addToIntermediateList(topic + "\t" + entityAbundance + "\t" + (value / entityAbundance));
    //					//					System.out.println("entity : " + topic + " abundance : " + entityAbundance + " tempScore : " + value / entityAbundance);
    //					trendTopicMap.put(topic, Double.valueOf(value / entityAbundance));
    //				} else
    //					trendTopicMap.put(topic, Double.valueOf(value));
    //			}
    //		}
    //	}

    public static void updateTrendInfo(String topic, double value, boolean deleteTrendTopic, String domain) {
        if (domain == null) {
            updateTrendInfo(topic, value, deleteTrendTopic);
            return;
        }
        if (topic == null || topic.equalsIgnoreCase("Overall"))
            return;
        boolean updatedTopic = false;
        Map<String, TrendingTopicInfo> tempTopicTrendMap = new HashMap<String, TrendingTopicInfo>();
        tempTopicTrendMap.putAll(trendingTopicMap);
        for (Entry<String, TrendingTopicInfo> topicScorePair : tempTopicTrendMap.entrySet()) {
            double tempScore = topicScorePair.getValue().getScore();
            String topicStr = topicScorePair.getKey();
            tempScore *= (1 - XpConfig.DECAY_FACTOR);

            if (tempScore < XpConfig.TREND_THRESHOLD && deleteTrendTopic && !topicStr.equalsIgnoreCase(topic)) {
                trendingTopicMap.remove(topicStr);
            } else {
                TrendingTopicInfo currentTopicInfo = topicScorePair.getValue();
                if (topicStr.equalsIgnoreCase(topic)) {
                    updatedTopic = true;
                    currentTopicInfo.addCount(1);
                    currentTopicInfo.augmentDomainCountMap(domain, 1);
                    if (XpConfig.IDF_DOWNGRADE) {
                        //							Double idfScore = KeywordRelevanceComputation.computeIDFScoreForEntity(topic, domain);
                        //							tempScore += value * idfScore.doubleValue();
                        double entityAbundance = KeywordRelevanceComputation.computeEntityAbundance(topic, domain);
                        tempScore += value / entityAbundance;
                        addToIntermediateList(topic + "\t" + entityAbundance + "\t" + tempScore);
                        //							System.out.println("entity : " + topic + " abundance : " + entityAbundance + " tempScore : " + tempScore);
                    } else
                        tempScore += value;
                }
                currentTopicInfo.setScore(tempScore);
                trendingTopicMap.put(topicStr, currentTopicInfo);
            }
        }
        if (!updatedTopic) {
            if (XpConfig.IDF_DOWNGRADE) {
                double entityAbundance = KeywordRelevanceComputation.computeEntityAbundance(topic, domain);
                addToIntermediateList(topic.toLowerCase() + "\t" + entityAbundance + "\t" + (value / entityAbundance));
                //					System.out.println("entity : " + topic + " abundance : " + entityAbundance + " tempScore : " + value / entityAbundance);
                TrendingTopicInfo topicInfo = new TrendingTopicInfo(domain, value / entityAbundance, 1);
                //				topicInfo.augmentDomainMap(domain, 1);
                trendingTopicMap.put(topic.toLowerCase(), topicInfo);
            } else {
                TrendingTopicInfo topicInfo = new TrendingTopicInfo(domain, value, 1);
                //				topicInfo.augmentDomainMap(domain, 1);
                trendingTopicMap.put(topic.toLowerCase(), topicInfo);
            }
        }
    }

    public static void updateDomainTrendInfo(String topic, double value, boolean deleteTrendTopic, String domain) {

        if (topic != null && (topic.equalsIgnoreCase("Overall") || topic.equalsIgnoreCase("null") || topic.matches("[0-9]+") || OntologyUtils.isRejectEntity(topic)))
            return;
        String currentDomain = (domain != null) ? domain : "generic";
        DomainTrendingTopicInfo domainInfo = domainTrendingTopicMap.get(currentDomain);
        if (domainInfo != null) {
            domainInfo.updateTrendInfo(topic, value, deleteTrendTopic);
        } else {
            domainInfo = new DomainTrendingTopicInfo(topic, 1, value, currentDomain);
            domainTrendingTopicMap.put(currentDomain, domainInfo);
        }
    }

    //
    public static void updateAspectTrendInfo(String topic, double value, boolean deleteTrendTopic, String domain) {
        if (domain == null) {
            updateTrendInfo(topic, value, deleteTrendTopic);
            return;
        }
        if (topic != null) {
            if (topic.equalsIgnoreCase("Overall"))
                return;
            boolean updatedAspect = false;
            String topicAspect = XpOntology.getOntologyCategory(Languages.EN, domain, topic.toLowerCase(), null);
            if (topicAspect == null && domain != null)
                return;
            if (domain == null)
                domain = AspectTrendInfo.GENERIC_DOMAIN;
            if (topicAspect == null)
                topicAspect = AspectTrendInfo.GENERIC_ASPECT;

            Map<String, AspectTrendInfo> tempAspectTrendMap = new HashMap<String, AspectTrendInfo>();
            tempAspectTrendMap.putAll(trendingAspectMap);
            for (Entry<String, AspectTrendInfo> aspectDomainInfoPair : tempAspectTrendMap.entrySet()) {
                String aspectStr = aspectDomainInfoPair.getKey();
                AspectTrendInfo aspectInfo = aspectDomainInfoPair.getValue();
                if (aspectInfo.isDomainPresent(domain)) {
                    double tempScore = aspectInfo.getScore();
                    tempScore *= (1 - XpConfig.DECAY_FACTOR);

                    if (tempScore < XpConfig.TREND_THRESHOLD && deleteTrendTopic && !aspectStr.equalsIgnoreCase(topicAspect)) {
                        trendingAspectMap.remove(aspectStr);
                    } else {
                        if (aspectStr.equalsIgnoreCase(topicAspect)) {
                            updatedAspect = true;
                            if (XpConfig.IDF_DOWNGRADE) {
                                //							Double idfScore = KeywordRelevanceComputation.computeIDFScoreForEntity(topic, domain);
                                //							tempScore += value * idfScore.doubleValue();
                                double entityAbundance = KeywordRelevanceComputation.computeEntityAbundance(topic, domain);
                                tempScore += value / entityAbundance;
                                addToIntermediateList(topic + "\t" + entityAbundance + "\t" + tempScore);
                                //							System.out.println("entity : " + topic + " abundance : " + entityAbundance + " tempScore : " + tempScore);
                            } else
                                tempScore += value;
                            aspectInfo.augmentDomainCount(1, domain, false);
                            aspectInfo.augmentEntityCount(topic.toLowerCase(), 1, domain, false);
                        }
                        aspectInfo.setScore(tempScore);
                        trendingAspectMap.put(aspectStr, aspectInfo);
                    }
                }
            }
            //		}
            if (!updatedAspect) {
                double score = value;
                if (XpConfig.IDF_DOWNGRADE) {
                    double entityAbundance = KeywordRelevanceComputation.computeEntityAbundance(topic, domain);
                    score /= entityAbundance;
                    addToIntermediateList(topic + "\t" + entityAbundance + "\t" + score);
                }
                //					System.out.println("entity : " + topic + " abundance : " + entityAbundance + " tempScore : " + value / entityAbundance);
                //			if (!trendingAspectMap.containsKey(topicAspect)) {
                AspectTrendInfo aspectInfo = new AspectTrendInfo(topic.toLowerCase(), score, domain);
                trendingAspectMap.put(topicAspect, aspectInfo);
            }
        }
    }

    //	public static void updateTrendingInfo(String topic, double value, boolean deleteTrendTopic, String domain) {
    //		if (domain == null) {
    //			updateTrendInfo(topic, value, deleteTrendTopic);
    //			return;
    //		}
    //		if (topic != null && topic.equalsIgnoreCase("Overall"))
    //			return;
    //		boolean updatedTopic = false;
    //		synchronized (trendTopicMap) {
    //			Map<String, Double> tempTopicTrendMap = new HashMap<String, Double>();
    //			tempTopicTrendMap.putAll(trendTopicMap);
    //			for (Entry<String, Double> topicScorePair : tempTopicTrendMap.entrySet()) {
    //				double tempScore = topicScorePair.getValue().doubleValue();
    //				String topicStr = topicScorePair.getKey();
    //				tempScore *= (1 - XpConfig.DECAY_FACTOR);
    //
    //				if (tempScore < XpConfig.TREND_THRESHOLD && deleteTrendTopic && !topicStr.equalsIgnoreCase(topic)) {
    //					trendTopicMap.remove(topicStr);
    //				} else {
    //					if (topicStr.equalsIgnoreCase(topic)) {
    //						updatedTopic = true;
    //						if (XpConfig.IDF_DOWNGRADE) {
    //							//							Double idfScore = KeywordRelevanceComputation.computeIDFScoreForEntity(topic, domain);
    //							//							tempScore += value * idfScore.doubleValue();
    //							double entityAbundance = KeywordRelevanceComputation.computeEntityAbundance(topic, domain);
    //							tempScore += value / entityAbundance;
    //							addToIntermediateList(topic + "\t" + entityAbundance + "\t" + tempScore);
    //							//							System.out.println("entity : " + topic + " abundance : " + entityAbundance + " tempScore : " + tempScore);
    //						} else
    //							tempScore += value;
    //					}
    //					trendTopicMap.put(topicStr, Double.valueOf(tempScore));
    //				}
    //			}
    //			if (!updatedTopic) {
    //				if (XpConfig.IDF_DOWNGRADE) {
    //					double entityAbundance = KeywordRelevanceComputation.computeEntityAbundance(topic, domain);
    //					addToIntermediateList(topic + "\t" + entityAbundance + "\t" + (value / entityAbundance));
    //					//					System.out.println("entity : " + topic + " abundance : " + entityAbundance + " tempScore : " + value / entityAbundance);
    //					trendTopicMap.put(topic, Double.valueOf(value / entityAbundance));
    //				} else
    //					trendTopicMap.put(topic, Double.valueOf(value));
    //			}
    //		}
    //	}

    //	public static void outputTrendInfo(String outputFile) {
    //		Map<String, Double> sortedTrendMap = sortTrendTopicMapByValues();
    //		List<String> outputList = new ArrayList<String>();
    //		for (Entry<String, Double> entry : sortedTrendMap.entrySet()) {
    //			String line = entry.getKey() + "\t" + entry.getValue();
    //			outputList.add(line);
    //		}
    //		FileIO.write_file(outputList, outputFile, false);
    //		FileIO.write_file(intermediateTopicList, "./output/intermediate_topic.txt", false);
    //	}

    public static void outputTrendInfo(String outputFile) {
        Map<String, TrendingTopicInfo> sortedTrendMap = sortTrendingTopicByScores(null, trendingTopicMap);
        List<String> outputList = new ArrayList<String>();
        for (Entry<String, TrendingTopicInfo> entry : sortedTrendMap.entrySet()) {
            if (entry.getValue().getCount() <= XpConfig.MIN_TREND_TOPIC_COUNT)
                continue;
            String line = entry.getKey() + "\t" + entry.getValue().getScore() + "\t" + entry.getValue().getCount() + "\t" + entry.getValue().getDomainCountMap();
            outputList.add(line);
        }
        FileIO.write_file(outputList, outputFile, false);
        FileIO.write_file(intermediateTopicList, "./output/intermediate_topic.txt", false);
    }

    public static void outputTrendInfo(String outputFile, String domain) {

        switch (XpConfig.TREND_TYPE.intValue()) {
            case 1:
                if (domain == null) {
                    outputTrendInfo(outputFile);
                    return;
                }
                List<String> outputList = new ArrayList<String>();
                Map<String, TrendingTopicInfo> sortedTrendMap = sortTrendingTopicByScores(domain, trendingTopicMap);
                for (Entry<String, TrendingTopicInfo> entry : sortedTrendMap.entrySet()) {
                    if (entry.getValue().getCount() <= XpConfig.MIN_TREND_TOPIC_COUNT)
                        continue;
                    String line = entry.getKey() + "\t" + entry.getValue().getScore() + "\t" + entry.getValue().getCount() + "\t" + entry.getValue().getDomainCountMap();
                    outputList.add(line);
                }
                FileIO.write_file(outputList, outputFile, false);
                break;
            //				FileIO.write_file(intermediateTopicList, "./output/intermediate_topic.txt", false);
            case 2:
                outputAspectTrendInfo(outputFile, domain);
                break;
            case 3:
                String currentDomain = (domain == null) ? "generic" : domain;
                //				System.out.println("currentDomain : " + currentDomain);
                DomainTrendingTopicInfo domainTrendInfo = domainTrendingTopicMap.get(currentDomain);
                if (domainTrendInfo != null)
                    //					System.out.println("domainTrendInfo not null");
                    domainTrendInfo.outputTrendInfo(outputFile);
                break;
            //			case 3:
            //				String currentDomain = (domain != null) ? domain : "generic";
            //				outputList = new ArrayList<String>();
            //				Map<String, TrendingTopicInfo> tempTopicInfoMap = domainTrendingTopicMap.get(currentDomain);
            //				if (tempTopicInfoMap != null) {
            //					sortedTrendMap = sortTrendingTopicByScores(domain, tempTopicInfoMap);
            //					for (Entry<String, TrendingTopicInfo> entry : sortedTrendMap.entrySet()) {
            //						String line = entry.getKey() + "\t" + entry.getValue().getScore() + "\t" + entry.getValue().getCount() + "\t" + entry.getValue().getDomainMap();
            //						outputList.add(line);
            //					}
            //				}
            //				FileIO.write_file(outputList, outputFile, false);
            //				FileIO.write_file(intermediateTopicList, "./output/intermediate_topic.txt", false);
            default:
                if (domain == null) {
                    outputTrendInfo(outputFile);
                    return;
                }
                outputList = new ArrayList<String>();
                sortedTrendMap = sortTrendingTopicByScores(domain, trendingTopicMap);
                for (Entry<String, TrendingTopicInfo> entry : sortedTrendMap.entrySet()) {
                    String line = entry.getKey() + "\t" + entry.getValue().getScore() + "\t" + entry.getValue().getCount() + "\t" + entry.getValue().getDomainCountMap();
                    outputList.add(line);
                }
                FileIO.write_file(outputList, outputFile, false);
                //				FileIO.write_file(intermediateTopicList, "./output/intermediate_topic.txt", false);
        }

    }

    //	public static void outputAspectTrendInfo(String outputFile) {
    //		Map<String, AspectTrendInfo> sortedTrendMap = sortTrendingAspectByScores(null, trendingAspectMap);
    //		List<String> outputList = new ArrayList<String>();
    //		for (Entry<String, AspectTrendInfo> entry : sortedTrendMap.entrySet()) {
    //			String line = entry.getKey() + "\t" + entry.getValue().getScore() + "\t" + entry.getValue().getDomainCountMap();
    //			outputList.add(line);
    //		}
    //		FileIO.write_file(outputList, outputFile, false);
    //		//		FileIO.write_file(intermediateTopicList, "./output/intermediate_topic.txt", false);
    //	}

    public static void outputAspectTrendInfo(String outputFile, String domain) {
        //		if (domain == null) {
        //			outputAspectTrendInfo(outputFile);
        //			return;
        //		}
        Map<String, AspectTrendInfo> sortedTrendMap = sortTrendingAspectByScores(domain, trendingAspectMap);
        List<String> outputList = new ArrayList<String>();
        for (Entry<String, AspectTrendInfo> entry : sortedTrendMap.entrySet()) {
            String line = entry.getKey() + "\t" + entry.getValue().getScore() + "\t" + entry.getValue().getDomainCountMap();
            outputList.add(line);
        }
        FileIO.write_file(outputList, outputFile, false);
        //		FileIO.write_file(intermediateTopicList, "./output/intermediate_topic.txt", false);
    }

    //	private static Map<String, Double> getTrendTopicList() {
    //		Map<String, Double> sortedTrendMap = sortTrendTopicMapByValues();
    //		Map<String, Double> momentaryTrendTopicMap = new LinkedHashMap<String, Double>();
    //		int count = 0;
    //
    //		for (Entry<String, Double> topicPair : sortedTrendMap.entrySet()) {
    //			if (count >= XpConfig.MAX_TREND_TOPIC) {
    //				break;
    //			}
    //			String topic = topicPair.getKey();
    //			topic = topic.replaceAll("\\brt\\b", "");
    //			topic = topic.replaceAll("\\bhttp\\b", "");
    //			topic = topic.replaceAll("\\bhtt\\b", "");
    //			if (topic.length() > 0) {
    //				momentaryTrendTopicMap.put(topic, topicPair.getValue());
    //				count++;
    //			}
    //		}
    //		return momentaryTrendTopicMap;
    //	}

    //	private static Map<String, Double> getTrendTopicList(Map<String, TrendingTopicInfo> topicMap) {
    //		Map<String, TrendingTopicInfo> sortedTrendMap = sortTrendingTopicByScores(null, topicMap);
    //		Map<String, Double> momentaryTrendTopicMap = new LinkedHashMap<String, Double>();
    //		int count = 0;
    //
    //		for (Entry<String, TrendingTopicInfo> topicPair : sortedTrendMap.entrySet()) {
    //			if (count >= XpConfig.MAX_TREND_TOPIC) {
    //				break;
    //			}
    //			String topic = topicPair.getKey();
    //			topic = topic.replaceAll("\\brt\\b", "");
    //			topic = topic.replaceAll("\\bhttp\\b", "");
    //			topic = topic.replaceAll("\\bhtt\\b", "");
    //			if (topic.length() > 0) {
    //				momentaryTrendTopicMap.put(topic, topicPair.getValue().getScore());
    //				count++;
    //			}
    //		}
    //		return momentaryTrendTopicMap;
    //	}

    public static JSONObject prepareTrendJSON(String domain) {
        if (XpConfig.TREND_TYPE.intValue() == 3) {
            String currentDomain = (domain == null) ? "generic" : domain;
            DomainTrendingTopicInfo domainTrendInfo = domainTrendingTopicMap.get(currentDomain);
            if (domainTrendInfo != null)
                return domainTrendInfo.prepareTrendJSON();
            return null;
        }
        Map<String, Double> trendTopicMap = getTrendTopicList(domain, trendingTopicMap);
        String currentDomain = (domain != null) ? domain : "generic";
        JSONObject trendJson = new JSONObject();
        JSONArray trendArray = new JSONArray();
        for (Entry<String, Double> trendTopic : trendTopicMap.entrySet()) {
            JSONObject topicObj = new JSONObject();
            try {
                topicObj.put("topic", trendTopic.getKey());
                topicObj.put("score", trendTopic.getValue().doubleValue());
                trendArray.put(topicObj);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            trendJson.put("domain", currentDomain);
            trendJson.put("trending topics", trendArray);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return trendJson;
    }

    protected static Map<String, Double> getTrendTopicList(String domain, Map<String, TrendingTopicInfo> topicMap) {
        //		if (domain == null)
        //			return getTrendTopicList(topicMap);
        Map<String, TrendingTopicInfo> sortedTrendMap = sortTrendingTopicByScores(domain, topicMap);
        //		System.out.println("SortedTrendMap: " + sortedTrendMap);
        Map<String, Double> momentaryTrendTopicMap = new LinkedHashMap<String, Double>();
        int count = 0;

        for (Entry<String, TrendingTopicInfo> topicPair : sortedTrendMap.entrySet()) {

            if (count >= XpConfig.MAX_TREND_TOPIC) {
                break;
            }
            String topic = topicPair.getKey();
            topic = topic.replaceAll("\\brt\\b", "");
            topic = topic.replaceAll("\\bhttp\\b", "");
            topic = topic.replaceAll("\\bhtt\\b", "");

            if (topic.length() > 0) {
                momentaryTrendTopicMap.put(topic, topicPair.getValue().getScore());
                count++;
            }
        }
        return momentaryTrendTopicMap;
    }

    public static double round(double value, int places) {
        if (places > 0) {

            long factor = (long) Math.pow(10, places);
            value = value * factor;
            long tmp = Math.round(value);
            return (double) tmp / factor;
        } else {
            return value;
        }
    }

    //	public static String prepareQueryForTrendTopicUpdate(String tableName) {
    //		Map<String, Double> trendTopicMap = getTrendTopicList(null);
    //		StringBuilder qBuilder = new StringBuilder("INSERT INTO " + tableName + "(topic,category,score) VALUES ");
    //		int i = 0;
    //		if (trendTopicMap.size() > 1) {
    //			for (Entry<String, Double> topicPair : trendTopicMap.entrySet()) {
    //
    //				if (i != 0) {
    //					qBuilder.append(",");
    //				}
    //				qBuilder.append("(");
    //				String topic = topicPair.getKey();
    //				//				topic = topic.replaceAll("\\W+", "");
    //				qBuilder.append("'").append(topic).append("'");
    //				qBuilder.append(",NULL,");
    //				qBuilder.append(round(topicPair.getValue(), 2));
    //				qBuilder.append(") ");
    //				i++;
    //			}
    //			qBuilder.append(";");
    //			System.out.println(qBuilder.toString());
    //			return qBuilder.toString();
    //		} else {
    //			return null;
    //		}
    //	}

    public static String prepareQueryForTrendTopicUpdate(String tableName, String domain) {

        if (XpConfig.TREND_TYPE.intValue() == 3) {
            String currentDomain = (domain == null) ? "generic" : domain;
            DomainTrendingTopicInfo domainTrendInfo = domainTrendingTopicMap.get(currentDomain);
            if (domainTrendInfo != null)
                return domainTrendInfo.prepareQueryForTrendTopicUpdate(tableName);
            return null;
        }
        Map<String, Double> trendTopicMap = getTrendTopicList(domain, trendingTopicMap);
        StringBuilder qBuilder = new StringBuilder("INSERT INTO " + tableName + "(topic,category,score,domain) VALUES ");
        int i = 0;
        if (trendTopicMap.size() > 1) {
            for (Entry<String, Double> topicPair : trendTopicMap.entrySet()) {

                if (i != 0) {
                    qBuilder.append(",");
                }
                qBuilder.append("(");
                String topic = topicPair.getKey();
                //				topic = topic.replaceAll("\\W+", "");
                qBuilder.append("'").append(topic).append("'");
                qBuilder.append(",NULL,");
                qBuilder.append(round(topicPair.getValue().doubleValue(), 2));
                qBuilder.append(",");
                qBuilder.append("'").append(domain).append("'");
                qBuilder.append(") ");
                i++;
            }
            qBuilder.append(";");
            System.out.println(qBuilder.toString());
            return qBuilder.toString();
        }
        return null;
    }

    //	public static String prepareQueryForTrendTopicUpdate(String tableName) {
    //		Map<String, Double> trendTopicMap = getTrendTopicList(null);
    //		StringBuilder qBuilder = new StringBuilder("INSERT INTO " + tableName + "(topic,category,score,domain) VALUES ");
    //		int i = 0;
    //		if (trendTopicMap.size() > 1) {
    //			for (Entry<String, TrendingTopicInfo> topicPair : trendingTopicMap.entrySet()) {
    //
    //				if (i != 0) {
    //					qBuilder.append(",");
    //				}
    //				qBuilder.append("(");
    //				String topic = topicPair.getKey();
    //				//				topic = topic.replaceAll("\\W+", "");
    //				qBuilder.append("'").append(topic).append("'");
    //				qBuilder.append(",NULL,");
    //				qBuilder.append(round(topicPair.getValue().getScore(), 2));
    //				qBuilder.append(",");
    //				qBuilder.append(topicPair.getValue().getCurrentDomain());
    //				qBuilder.append(") ");
    //				i++;
    //			}
    //			qBuilder.append(";");
    //			System.out.println(qBuilder.toString());
    //			return qBuilder.toString();
    //		}
    //		return null;
    //	}

    public static void updateDatabaseWithRecentTopics() {
        String[] truncatedTables = { "TRUNCATE TABLE tbltrendtopicback;", "TRUNCATE TABLE tbltrendtopic;" };
        XpEngine.dbFunction.fireDDLQuery(truncatedTables[0], true);
        String sqlQuery = prepareQueryForTrendTopicUpdate("tbltrendtopicback", null);
        if (sqlQuery != null) {
            XpEngine.dbFunction.fireDDLQuery(sqlQuery, true);

            sqlQuery = prepareQueryForTrendTopicUpdate("tbltrendtopic", null);
            XpEngine.dbFunction.fireDDLQuery(truncatedTables[1], true);
            XpEngine.dbFunction.fireDDLQuery(sqlQuery, true);
            System.out.println("DataBase table Updated");
        }

        //		String truncatedTable = "TRUNCATE TABLE tbltrendtopic;";
        //		XpEngine.dbFunction.fireDDLQuery(truncatedTable, true);
        //		String sqlQuery = prepareQueryForTrendTopicUpdate("tbltrendtopic");
        //		if (sqlQuery != null) {
        //			XpEngine.dbFunction.fireDDLQuery(sqlQuery, true);
        //		}

    }

    //	private static Map<String, Double> sortTrendTopicMapByValues() {
    //		List<Entry<String, Double>> list = new LinkedList<Entry<String, Double>>(trendTopicMap.entrySet());
    //		// Defined Custom Comparator here
    //		Collections.sort(list, new Comparator<Entry<String, Double>>() {
    //			@Override
    //			public int compare(Entry<String, Double> o1, Entry<String, Double> o2) {
    //				// TODO Auto-generated method stub
    //				double diff = o1.getValue().doubleValue() - o2.getValue().doubleValue();
    //				int retVal = 0;
    //				if (diff < 0)
    //					retVal = 1;
    //				else if (diff == 0)
    //					retVal = 0;
    //				else
    //					retVal = -1;
    //				return retVal;
    //			}
    //		});
    //
    //		Map<String, Double> sortedHashMap = new LinkedHashMap<String, Double>();
    //		for (Iterator<Entry<String, Double>> it = list.iterator(); it.hasNext();) {
    //			Entry<String, Double> entry = (Entry<String, Double>) it.next();
    //			sortedHashMap.put(entry.getKey(), entry.getValue());
    //		}
    //		return sortedHashMap;
    //	}

    private static Map<String, TrendingTopicInfo> sortTrendingTopicByScores(Map<String, TrendingTopicInfo> topicInfoMap) {
        List<Entry<String, TrendingTopicInfo>> list = new LinkedList<Entry<String, TrendingTopicInfo>>(topicInfoMap.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator<Entry<String, TrendingTopicInfo>>() {
            @Override
            public int compare(Entry<String, TrendingTopicInfo> o1, Entry<String, TrendingTopicInfo> o2) {
                // TODO Auto-generated method stub
                double diff = o1.getValue().getScore() - o2.getValue().getScore();
                int retVal = 0;
                if (diff < 0)
                    retVal = 1;
                else if (diff == 0)
                    retVal = 0;
                else
                    retVal = -1;
                return retVal;
            }
        });

        Map<String, TrendingTopicInfo> sortedHashMap = new LinkedHashMap<String, TrendingTopicInfo>();
        for (Iterator<Entry<String, TrendingTopicInfo>> it = list.iterator(); it.hasNext();) {
            Entry<String, TrendingTopicInfo> entry = (Entry<String, TrendingTopicInfo>) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }

    private static Map<String, AspectTrendInfo> sortTrendingAspectByScores(Map<String, AspectTrendInfo> aspectDomainInfoMap) {
        // Defined Custom Comparator here
        List<Entry<String, AspectTrendInfo>> list = new ArrayList<Entry<String, AspectTrendInfo>>(aspectDomainInfoMap.entrySet());
        Collections.sort(list, new Comparator<Entry<String, AspectTrendInfo>>() {
            @Override
            public int compare(Entry<String, AspectTrendInfo> o1, Entry<String, AspectTrendInfo> o2) {
                // TODO Auto-generated method stub
                double diff = o1.getValue().getScore() - o2.getValue().getScore();
                int retVal = 0;
                if (diff < 0)
                    retVal = 1;
                else if (diff == 0)
                    retVal = 0;
                else
                    retVal = -1;
                return retVal;
            }
        });

        Map<String, AspectTrendInfo> sortedHashMap = new LinkedHashMap<String, AspectTrendInfo>();
        for (Iterator<Entry<String, AspectTrendInfo>> it = list.iterator(); it.hasNext();) {
            Entry<String, AspectTrendInfo> entry = (Entry<String, AspectTrendInfo>) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }

    private static List<Entry<String, TrendingTopicInfo>> filterDomainSpecificTrendingTopic(String domain, Map<String, TrendingTopicInfo> topicInfoMap) {
        List<Entry<String, TrendingTopicInfo>> list = new LinkedList<Entry<String, TrendingTopicInfo>>();
        for (Entry<String, TrendingTopicInfo> topicPair : topicInfoMap.entrySet()) {
            //			String topicDomain = topicPair.getValue().getDomain();
            //			if (topicDomain != null && topicDomain.equalsIgnoreCase(domain)) {
            //			System.out.println("Comparing: " + topicPair.getValue() + "\t" + domain);
            if (topicPair.getValue().checkIfDomainExists(domain)) {
                list.add(topicPair);
            }
        }
        return list;
    }

    private static List<Entry<String, AspectTrendInfo>> filterDomainSpecificTrendingAspect(String domain, Map<String, AspectTrendInfo> domainTrendAspectMap) {
        Map<String, AspectTrendInfo> domainInfoMap = new LinkedHashMap<String, AspectTrendInfo>();
        for (Entry<String, AspectTrendInfo> aspectPair : domainTrendAspectMap.entrySet()) {
            if (aspectPair.getValue().isDomainPresent(domain)) {
                domainInfoMap.put(aspectPair.getKey(), aspectPair.getValue());
            }
        }
        List<Entry<String, AspectTrendInfo>> entryList = new ArrayList<Entry<String, AspectTrendInfo>>(domainInfoMap.entrySet());
        return entryList;
    }

    protected static Map<String, TrendingTopicInfo> sortTrendingTopicByScores(String domain, Map<String, TrendingTopicInfo> topicInfoMap) {
        if (domain == null)
            return sortTrendingTopicByScores(topicInfoMap);

        //		System.out.println("TopicInfoMap in sortTrendingTopicByScores: " + topicInfoMap);
        List<Entry<String, TrendingTopicInfo>> list = filterDomainSpecificTrendingTopic(domain, topicInfoMap);
        //		System.out.println("FilterDomanSpecificList in sortTrendingTopicByScores: " + list);
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator<Entry<String, TrendingTopicInfo>>() {
            @Override
            public int compare(Entry<String, TrendingTopicInfo> o1, Entry<String, TrendingTopicInfo> o2) {
                // TODO Auto-generated method stub
                double diff = o1.getValue().getScore() - o2.getValue().getScore();
                int retVal = 0;
                if (diff < 0)
                    retVal = 1;
                else if (diff == 0)
                    retVal = 0;
                else
                    retVal = -1;
                return retVal;
            }
        });

        Map<String, TrendingTopicInfo> sortedHashMap = new LinkedHashMap<String, TrendingTopicInfo>();
        for (Iterator<Entry<String, TrendingTopicInfo>> it = list.iterator(); it.hasNext();) {
            Entry<String, TrendingTopicInfo> entry = (Entry<String, TrendingTopicInfo>) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }

    private static Map<String, AspectTrendInfo> sortTrendingAspectByScores(String domain, Map<String, AspectTrendInfo> aspectMap) {
        if (domain == null)
            return sortTrendingAspectByScores(aspectMap);
        List<Entry<String, AspectTrendInfo>> list = filterDomainSpecificTrendingAspect(domain, aspectMap);
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator<Entry<String, AspectTrendInfo>>() {
            @Override
            public int compare(Entry<String, AspectTrendInfo> o1, Entry<String, AspectTrendInfo> o2) {
                // TODO Auto-generated method stub
                double diff = o1.getValue().getScore() - o2.getValue().getScore();
                int retVal = 0;
                if (diff < 0)
                    retVal = 1;
                else if (diff == 0)
                    retVal = 0;
                else
                    retVal = -1;
                return retVal;
            }
        });

        Map<String, AspectTrendInfo> sortedHashMap = new LinkedHashMap<String, AspectTrendInfo>();
        for (Iterator<Entry<String, AspectTrendInfo>> it = list.iterator(); it.hasNext();) {
            Entry<String, AspectTrendInfo> entry = (Entry<String, AspectTrendInfo>) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }
}