package  com.abzooba.xpresso.realtime;

import java.util.LinkedHashMap;
import java.util.Map;

public class AspectTrendInfo {

    //	private Map<String, DomainInfo> domainEntityInfoMap = new LinkedHashMap<String, DomainInfo>();
    private Map<String, Map<String, Integer>> domainEntityCountMap = new LinkedHashMap<String, Map<String, Integer>>();
    private Map<String, Integer> domainCountMap = new LinkedHashMap<String, Integer>();
    public static String GENERIC_DOMAIN = "genericDomain";
    public static String GENERIC_ASPECT = "genericAspect";
    private double score = 0.0;

    public double getScore() {
        return this.score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public void addScore(double score) {
        this.score += score;
    }

    public AspectTrendInfo(String entity, double score) {
        this.score = score;
        Map<String, Integer> entityCountMap = new LinkedHashMap<String, Integer>();
        entityCountMap.put(entity, 1);
        domainEntityCountMap.put(GENERIC_DOMAIN, entityCountMap);
        domainCountMap.put(GENERIC_DOMAIN, 1);
    }

    public AspectTrendInfo(String entity, double score, String domain) {
        this.score = score;
        Map<String, Integer> entityCountMap = new LinkedHashMap<String, Integer>();
        entityCountMap.put(entity, 1);
        domainEntityCountMap.put(domain, entityCountMap);
        domainCountMap.put(domain, 1);
    }

    public void augmentEntityCount(String entity, int count, String domain, boolean overWrite) {
        //		String currentDomain = (domain != null) ? domain : GENERIC_DOMAIN;
        if (domain == null)
            return;
        Map<String, Integer> entityCountMap = domainEntityCountMap.get(domain);
        if (entityCountMap == null) {
            entityCountMap = new LinkedHashMap<String, Integer>();
            entityCountMap.put(entity, count);
            domainEntityCountMap.put(domain, entityCountMap);
        } else {
            if (!entityCountMap.containsKey(entity) || overWrite) {
                entityCountMap.put(entity, count);
            } else {
                entityCountMap.put(entity, entityCountMap.get(entity).intValue() + count);
            }
        }
    }

    public boolean isDomainPresent(String domain) {
        return this.domainCountMap.containsKey(domain);
    }

    public int getEntityCount(String entity, String domain) {
        int entityCount = 0;
        if (domain == null)
            return entityCount;
        Map<String, Integer> entityCountMap = domainEntityCountMap.get(domain);
        if (entityCountMap != null && entityCountMap.containsKey(entity)) {
            entityCount = entityCountMap.get(entity).intValue();
        }
        return entityCount;
    }

    public int getDomainCount(String domain) {
        int domainCount = 0;
        if (domain == null)
            return domainCount;
        if (domainCountMap.containsKey(domain)) {
            domainCount = domainCountMap.get(domain).intValue();
        }
        return domainCount;
    }

    public Map<String, Integer> getDomainCountMap() {
        return domainCountMap;
    }

    public void augmentDomainCount(int count, String domain, boolean overWrite) {
        if (!domainCountMap.containsKey(domain) || overWrite) {
            domainCountMap.put(domain, count);
        } else {
            domainCountMap.put(domain, domainCountMap.get(domain).intValue() + count);
        }
    }

    //	public class DomainInfo {
    //
    //		private Map<String, Integer> entityCountMap = new LinkedHashMap<String, Integer>();
    //		private String domain;
    //
    //		public DomainInfo(String entity) {
    //			entityCountMap.put(entity, 1);
    //		}
    //
    //		public DomainInfo(String entity, int count) {
    //			entityCountMap.put(entity, count);
    //		}
    //
    //		public DomainInfo(String entity, double score) {
    //			entityCountMap.put(entity, 1);
    //			//			this.score = score;
    //		}
    //
    //		public DomainInfo(String entity, int count, double score) {
    //			entityCountMap.put(entity, count);
    //			//			this.score = score;
    //		}
    //
    //		public void setEntity(String entity, int count) {
    //			entityCountMap.put(entity, count);
    //		}
    //
    //		public void setDomain(String domain) {
    //			this.domain = domain;
    //		}
    //
    //		public String getDomain() {
    //			return this.domain;
    //		}
    //
    //		public void addEntityCount(String entity, int count) {
    //			Integer currCount = entityCountMap.get(entity);
    //			if (currCount == null)
    //				setEntity(entity, count);
    //			else
    //				entityCountMap.put(entity, currCount.intValue() + count);
    //		}
    //
    //		public Integer getEntityCount(String entity) {
    //			if (entity == null)
    //				return null;
    //			return entityCountMap.get(entity);
    //		}
    //
    //		//		public double getScore() {
    //		//			return score;
    //		//		}
    //
    //		public boolean isEntityPresent(String entity) {
    //			return entityCountMap.containsKey(entity);
    //		}
    //
    //		public Map<String, Integer> getEntityCountMap() {
    //			return this.entityCountMap;
    //		}
    //	}

    //	public void augmentScore(double score, String domain, boolean overWrite) {
    //		//		String currentDomain = (domain != null) ? domain : GENERIC_DOMAIN;
    //		if (domain == null)
    //			return;
    //		DomainInfo domainInfo = domainEntityInfoMap.get(domain);
    //		if (domainInfo == null) {
    //			domainInfo = new DomainInfo(score);
    //			domainEntityInfoMap.put(domain, domainInfo);
    //		} else {
    //			if (overWrite) {
    //				domainInfo.setScore(score);
    //			} else
    //				domainInfo.addScore(score);
    //		}
    //	}

    //	public void setScoreForDomain(double score, String domain, boolean overWrite) {
    //		if (domain == null)
    //			return;
    //		DomainInfo domainInfo = domainEntityInfoMap.get(domain);
    //		if (domainInfo == null) {
    //			domainInfo = new DomainInfo(score);
    //			domainEntityInfoMap.put(domain, domainInfo);
    //		} else {
    //			if (overWrite) {
    //				domainInfo.setScore(score);
    //			} else
    //				domainInfo.addScore(score);
    //		}
    //	}

    //	public DomainInfo getDomainInfo(String domain) {
    //		return domainEntityInfoMap.get(domain);
    //	}
    //
    //	public Set<Entry<String, DomainInfo>> getDomainEntityInfoMap() {
    //		return this.domainEntityInfoMap.entrySet();
    //	}

}