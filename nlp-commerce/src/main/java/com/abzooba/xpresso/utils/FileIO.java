/**
 *
 */
package com.abzooba.xpresso.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import edu.stanford.nlp.io.IOUtils;

/**
 * @author Koustuv Saha
 * 10-Mar-2014 4:59:38 pm
 * XpressoV2 FileIO
 */
public class FileIO {

	//	public static void read_file(String fileName, Collection<String> collection) {
	//		try {
	//			//			BufferedReader br = new BufferedReader(new FileReader(fileName));
	//
	//			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
	//			String strLine;
	//			while ((strLine = br.readLine()) != null) {
	//				collection.add(strLine);
	//			}
	//			br.close();
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param fileName
	 * @return InputStream
	 * 
	 */
	public static InputStream getInputStreamFromFileName(String fileName) {

		InputStream is = null;

		/*This is applicable when the xpresso is used as jar*/
		is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
		if (is != null) {
			return is;
		}
		/*This is applicable when the xpresso is used as standalone application*/
		try {
			is = findStreamInClasspathOrFileSystem(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return is;
	}

	private static InputStream findStreamInClasspathOrFileSystem(String name) throws FileNotFoundException {
		try {
			/*This is applicable when the xpresso is used as jar*/
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
			if (is == null)
				is = new FileInputStream(name);
			if (name.endsWith(".gz")) {
				try {
					return new GZIPInputStream(is);
				} catch (IOException e) {
					System.err.println("Resource or file looks like a gzip file, but is not: " + name);
				}
			}
			return is;
		} catch (Exception ex) {
			InputStream is = IOUtils.class.getClassLoader().getResourceAsStream(name);
			if (is == null) {
				is = IOUtils.class.getClassLoader().getResourceAsStream(name.replaceAll("\\\\", "/"));
				if (is == null) {
					is = IOUtils.class.getClassLoader().getResourceAsStream(name.replaceAll("\\\\", "/").replaceAll("/+", "/"));
				}
			}
			if (is == null)
				is = new FileInputStream(name);
			if (name.endsWith(".gz")) {
				try {
					return new GZIPInputStream(is);
				} catch (IOException e) {
					System.err.println("Resource or file looks like a gzip file, but is not: " + name);
				}
			}
			return is;
		}
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param fileName
	 * @param collection
	 * 
	 */
	public static void read_file(String fileName, Collection<String> collection) {
		read_file(fileName, collection, false);
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param fileName
	 * @param collection
	 * @param ignoreHeader
	 * 
	 */
	public static void read_file(String fileName, Collection<String> collection, boolean ignoreHeader) {
		try {
			//			BufferedReader br = new BufferedReader(new FileReader(fileName));
			//			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
			BufferedReader br = new BufferedReader(new InputStreamReader(getInputStreamFromFileName(fileName), "UTF8"));

			//			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
			String strLine;
			int i = 0;

			while ((strLine = br.readLine()) != null) {
				if (i++ == 0) {
					if (ignoreHeader) {
						continue;
					}
				}
				collection.add(strLine);
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param fileName
	 * @param collection
	 * @param code-charset (eg. UTF-8)
	 * 
	 */
	public static void read_file(String fileName, Collection<String> collection, String charset) {
		try {
			//			BufferedReader br = new BufferedReader(new FileReader(fileName));

			//			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), charset));
			BufferedReader br = new BufferedReader(new InputStreamReader(getInputStreamFromFileName(fileName), charset));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				collection.add(strLine);
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param fileName
	 * @param collection
	 * @param ignoreHeader
	 * @param isLowerCase
	 * 
	 */
	public static void read_file(String fileName, Collection<String> collection, boolean ignoreHeader, boolean isLowerCase) {
		try {
			//			BufferedReader br = new BufferedReader(new FileReader(fileName));

			//			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
			BufferedReader br = new BufferedReader(new InputStreamReader(getInputStreamFromFileName(fileName), "UTF8"));

			String strLine;
			int i = 0;
			while ((strLine = br.readLine()) != null) {
				if (i++ == 0) {
					if (ignoreHeader) {
						continue;
					}
				}
				if (isLowerCase) {
					collection.add(strLine.toLowerCase());
				} else {
					collection.add(strLine);
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param fileName
	 * @param map
	 * 
	 */
	public static void read_file(String fileName, Map<String, String> map) {
		try {
			//			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
			BufferedReader br = new BufferedReader(new InputStreamReader(getInputStreamFromFileName(fileName), "UTF8"));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				String[] strArr = strLine.split("\\s+");
				if (strArr.length == 2) {
					map.put(strArr[1], strArr[0]);
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param fileName
	 * @param map
	 * @param delimiter
	 * @param keyIdx
	 * @param valueIdx
	 * 
	 */
	public static void read_file(String fileName, Map<String, String> map, String delimiter, int keyIdx, int valueIdx) {
		try {
			//			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
			BufferedReader br = new BufferedReader(new InputStreamReader(getInputStreamFromFileName(fileName), "UTF8"));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				String[] strArr = strLine.split(delimiter);
				if (strArr.length > keyIdx && strArr.length > valueIdx) {
					map.put(strArr[keyIdx], strArr[valueIdx]);
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Alix Melchy
	 * May 10, 2016
	 * @param fileName
	 * @param map
	 * @param delimiter
	 * 
	 */
	public static void read_file(String fileName, Map<String, Set<String>> map, String delimiter) {
		try {
			//			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
			BufferedReader br = new BufferedReader(new InputStreamReader(getInputStreamFromFileName(fileName), "UTF8"));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				String[] strArr = strLine.split(delimiter);
				if (strArr.length > 1) {
					Set<String> collection = new LinkedHashSet<String>();
					for (int i = 1; i < strArr.length; i++)
						collection.add(strArr[i]);
					map.put(strArr[0], collection);
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param collection
	 * @param fileName
	 * @param isAppend
	 * 
	 */
	public static void write_file(Collection<?> triples_strList, String fileName, boolean isAppend) {
		File file = new File(fileName);
		File parentFile = file.getParentFile();
		if (!parentFile.exists()) {
			parentFile.mkdir();
		}
		try {
			PrintWriter pw;
			pw = new PrintWriter(new FileWriter(file, isAppend));
			for (Object str : triples_strList) {
				pw.println(str.toString());
			}
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param strLine
	 * @param fileName
	 * @param isAppend
	 * 
	 */
	public static void write_file(String strLine, String fileName, boolean isAppend) {
		File file = new File(fileName);
		File parentFile = file.getParentFile();
		if (!parentFile.exists()) {
			parentFile.mkdir();
		}
		synchronized (file) {
			try {
				PrintWriter pw;
				pw = new PrintWriter(new FileWriter(file, isAppend));
				pw.println(strLine);
				pw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param map
	 * @param fileName
	 * @param isAppend
	 * @param delimiter
	 * 
	 */
	public static void write_file(Map<?, ?> map, String fileName, boolean isAppend, String delimiter) {
		List<String> keyValueList = new ArrayList<String>();
		for (Entry<?, ?> entry : map.entrySet()) {
			keyValueList.add(entry.getKey() + delimiter + entry.getValue());
		}
		write_file(keyValueList, fileName, isAppend);
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param line
	 * @param fileName
	 * @param isAppend
	 * 
	 */
	public static synchronized void write_fileNIO(String line, String fileName, boolean isAppend) {
		Path file = null;
		AsynchronousFileChannel asynchFileChannel = null;
		String filePath = fileName;
		try {

			if (fileName != null) {

				file = Paths.get(filePath);
				asynchFileChannel = AsynchronousFileChannel.open(file, StandardOpenOption.WRITE, StandardOpenOption.CREATE);

				CompletionHandler<Integer, Object> handler = new CompletionHandler<Integer, Object>() {
					@Override
					public void completed(Integer result, Object attachment) {
						//					System.out.println("Thread: " + Thread.currentThread().getName() + " File Write Completed with Result:" + result);
					}

					@Override
					public void failed(Throwable e, Object attachment) {
						//					System.err.println("File Write Failed Exception:");
						e.printStackTrace();
					}
				};
				asynchFileChannel.write(ByteBuffer.wrap(line.getBytes()), asynchFileChannel.size(), "", handler);
				asynchFileChannel.close();
			} else {
				System.out.println(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//		Future<FileLock> featureLock = asynchFileChannel.lock();
		//		FileLock lock = featureLock.get();
		//		if (lock.isValid()) {
		//			Future<Integer> featureWrite = asynchFileChannel.write(ByteBuffer.wrap(test), asynchFileChannel.size());
		//			lock.release();
		//		}
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param collection
	 * @param fileName
	 * @param isAppend
	 * 
	 */
	public static synchronized void write_fileNIO(Collection<?> collection, String fileName, boolean isAppend) {
		Path file = null;
		AsynchronousFileChannel asynchFileChannel = null;
		String filePath = fileName;
		try {
			StringBuilder sb = new StringBuilder();
			for (Object str : collection) {
				sb.append(str.toString()).append("\n");
			}
			if (fileName != null) {

				file = Paths.get(filePath);
				asynchFileChannel = AsynchronousFileChannel.open(file, StandardOpenOption.WRITE, StandardOpenOption.CREATE);

				CompletionHandler<Integer, Object> handler = new CompletionHandler<Integer, Object>() {
					@Override
					public void completed(Integer result, Object attachment) {
						//					System.out.println("Thread: " + Thread.currentThread().getName() + " File Write Completed with Result:" + result);
					}

					@Override
					public void failed(Throwable e, Object attachment) {
						//					System.err.println("File Write Failed Exception:");
						e.printStackTrace();
					}
				};
				//				System.out.println("Thread: " + Thread.currentThread().getName() + " Before write call");

				//			Future<FileLock> featureLock = asynchFileChannel.lock();
				//			//			FileLock featureLock = asynchFileChannel.tryLock();
				//			FileLock lock = featureLock.get();
				//
				//			if (lock.acquiredBy() == null) {
				//				lock = asynchFileChannel.tryLock();
				//			}
				//			if (lock.isValid()) {
				//			asynchFileChannel.write(ByteBuffer.wrap(sb.toString().getBytes()), asynchFileChannel.size(), "File Write", handler);

				//			if (lock.isValid()) {
				asynchFileChannel.write(ByteBuffer.wrap(sb.toString().getBytes()), asynchFileChannel.size(), "", handler);
				asynchFileChannel.close();
			} else {
				System.out.println(sb.toString());
			}
			//			}
			//			asynchFileChannel.force(true);
			//				lock.release();
			//			}

			//			System.out.println("Thread: " + Thread.currentThread().getName() + " After write call");
		} catch (Exception e) {
			e.printStackTrace();
		}
		//		Future<FileLock> featureLock = asynchFileChannel.lock();
		//		FileLock lock = featureLock.get();
		//		if (lock.isValid()) {
		//			Future<Integer> featureWrite = asynchFileChannel.write(ByteBuffer.wrap(test), asynchFileChannel.size());
		//			lock.release();
		//		}
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param collection
	 * @param asynchFileChannel
	 * @param isAppend
	 * 
	 */
	public static synchronized void write_fileNIO(Collection<?> collection, AsynchronousFileChannel asynchFileChannel, boolean isAppend) {
		try {
			StringBuilder sb = new StringBuilder();
			for (Object str : collection) {
				sb.append(str.toString()).append("\n");
			}
			if (asynchFileChannel != null) {

				CompletionHandler<Integer, Object> handler = new CompletionHandler<Integer, Object>() {
					@Override
					public void completed(Integer result, Object attachment) {
						//					System.out.println("Thread: " + Thread.currentThread().getName() + " File Write Completed with Result:" + result);
					}

					@Override
					public void failed(Throwable e, Object attachment) {
						//					System.err.println("File Write Failed Exception:");
						e.printStackTrace();
					}
				};
				//				System.out.println("Thread: " + Thread.currentThread().getName() + " Before write call");

				//			Future<FileLock> featureLock = asynchFileChannel.lock();
				//			//			FileLock featureLock = asynchFileChannel.tryLock();
				//			FileLock lock = featureLock.get();
				//
				//			if (lock.acquiredBy() == null) {
				//				lock = asynchFileChannel.tryLock();
				//			}
				//			if (lock.isValid()) {
				//			asynchFileChannel.write(ByteBuffer.wrap(sb.toString().getBytes()), asynchFileChannel.size(), "File Write", handler);

				//			if (lock.isValid()) {
				asynchFileChannel.write(ByteBuffer.wrap(sb.toString().getBytes()), asynchFileChannel.size(), "", handler);
				//				asynchFileChannel.close();
			} else {
				System.out.println(sb.toString());
			}
			//			}
			//			asynchFileChannel.force(true);
			//				lock.release();
			//			}

			//			System.out.println("Thread: " + Thread.currentThread().getName() + " After write call");
		} catch (Exception e) {
			e.printStackTrace();
		}
		//		Future<FileLock> featureLock = asynchFileChannel.lock();
		//		FileLock lock = featureLock.get();
		//		if (lock.isValid()) {
		//			Future<Integer> featureWrite = asynchFileChannel.write(ByteBuffer.wrap(test), asynchFileChannel.size());
		//			lock.release();
		//		}
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param inputFileXL
	 * @return listLines
	 * 
	 */
	public static List<String> getReviewListfromXL(String inputFile) {

		List<String> reviewList = new ArrayList<String>();
		try {
			FileInputStream file = new FileInputStream(new File(inputFile));
			// Create Workbook instance holding reference to .xlsx file
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);
			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			Map<String, String> reviewMap = new HashMap<>();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				int i = 1;
				String id = null;
				String srcText = null;
				while (cellIterator.hasNext()) {
					if (i <= 2) {
						Cell cell = (Cell) cellIterator.next();
						// Check the cell type and format accordingly
						// switch (cell.getCellType()) {
						// case Cell.CELL_TYPE_NUMERIC:
						// System.out.print(cell.getNumericCellValue() + "t");
						// break;
						// case Cell.CELL_TYPE_STRING:
						// System.out.print(cell.getStringCellValue() + "t");
						// break;
						// }
						if (i == 1) {
							id = ((Cell) cell).getStringCellValue();
						} else {
							srcText = cell.getStringCellValue();
						}
						i++;
					} else {
						break;
					}
				}

				if (!reviewMap.containsKey(id)) {
					reviewMap.put(id, srcText);
				}
			}
			file.close();

			for (Entry<String, String> entry : reviewMap.entrySet()) {
				// String id = entry.getKey();
				// System.out.println(id);
				String review = entry.getValue();
				// System.out.println(review);
				reviewList.add(review);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reviewList;
	}

	public static void main(String[] args) {
		List<String> justCheckList = Arrays.asList("ab", "bc");
		write_file(justCheckList, "./bullshitchalance/bull.txt", false);
	}
}
