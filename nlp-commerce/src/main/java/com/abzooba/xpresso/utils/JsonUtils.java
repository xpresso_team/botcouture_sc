/**
 * 
 */
package com.abzooba.xpresso.utils;

import java.util.Iterator;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * @author Koustuv Saha
 * Mar 8, 2015 4:41:58 PM
 * XpressoV2.8  JsonUtils
 */
public class JsonUtils {

	public static void toMap(JSONObject jsonObject, Map<String, String> collectionMap) {
		Iterator<?> keysItr = jsonObject.keys();
		while (keysItr.hasNext()) {
			String key = (String) keysItr.next();
			String value;
			try {
				value = jsonObject.getString(key);
				collectionMap.put(key, value);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}
