package  com.abzooba.xpresso.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.dbcp2.BasicDataSource;

public class DatabaseFunctions {

    private Connection dbConnection;
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    //	public static final String DB_URL = "jdbc:mysql://localhost/";
    private static final String DB_URL = "jdbc:mysql://104.197.92.31/dbsingapore";
    private static final String USER_NAME = "root";
    private static final String PASSWORD = "5dec1989";
    public static boolean isEstablishedConnection = false;
    public static BasicDataSource connectionPool = null;

    public DatabaseFunctions() {
        try {
            Class.forName(JDBC_DRIVER);
            dbConnection = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
            isEstablishedConnection = true;

        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Connecting to database...");
    }

    public Connection getDBconnection() {
        return dbConnection;
    }

    public static void init() {
        connectionPool = new BasicDataSource();
        connectionPool.setUsername(USER_NAME);
        connectionPool.setPassword(PASSWORD);
        connectionPool.setDriverClassName(JDBC_DRIVER);
        connectionPool.setUrl(DB_URL);
        connectionPool.setInitialSize(10);
    }

    public static BasicDataSource getDBConnectionPool() {
        return connectionPool;
    }

    //	public DatabaseFunctions(String db_Url, String userName, String passWord) throws SQLException {
    //		try {
    //			Class.forName("com.mysql.jdbc.Driver");
    //		} catch (ClassNotFoundException e) {
    //			// TODO Auto-generated catch block
    //			e.printStackTrace();
    //		}
    //		dbConnection = DriverManager.getConnection(DB_URL, userName, passWord);
    //		System.out.println("Connecting to database...");
    //	}

    public ResultSet getResultSet(String sqlQuery) {
        Statement statement = null;
        ResultSet rs = null;
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(sqlQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    public void fireDDLQuery(String sqlQuery, boolean isUpdate) {

        Statement statement = null;

        try {
            //			System.out.println("Calling CREATE PROCEDURE");
            statement = dbConnection.createStatement();
            if (statement != null) {
                if (isUpdate)
                    statement.executeUpdate(sqlQuery);
                else {
                    ResultSet rs = statement.executeQuery(sqlQuery);
                    if (rs != null) {
                        System.out.println("ResultSet returnd for " + sqlQuery);
                    }
                }
                System.out.println("Database created successfully...");
                statement.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}