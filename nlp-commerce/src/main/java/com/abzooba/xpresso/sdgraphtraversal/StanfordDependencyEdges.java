/**
 * @author Alix Melchy 
 * Jun 23, 2015 8:38:56 AM
 */
package com.abzooba.xpresso.sdgraphtraversal;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.aspect.domainontology.OntologyUtils;
import com.abzooba.xpresso.aspect.domainontology.XpOntology;
import com.abzooba.xpresso.engine.core.XpExpression;
import com.abzooba.xpresso.engine.core.XpText;
import com.abzooba.xpresso.engine.core.XpSentenceResources;
import com.abzooba.xpresso.expressions.intention.IntentEngine;
import com.abzooba.xpresso.expressions.sentiment.SentimentFunctions;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.textanalytics.PosTags;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.util.CoreMap;

public class StanfordDependencyEdges extends StanfordDependencyGraph {

    // List<SemanticGraphEdge> edges;
    private ConstituencyTree tree = null;

    /**
     *
     * @param text
     * @param tree
     * @param edges
     * @param xto
     * @param xe
     */
    public StanfordDependencyEdges(String text, ConstituencyTree tree, List<SemanticGraphEdge> edges, XpSentenceResources xto, XpExpression xe) {
        super(text, null, xto, xe);

        //		checkedEdge = new HashSet<SemanticGraphEdge>();

        this.text = text;

        this.tagSentimentMap = xto.getTagSentimentMap();
        this.wordPOSmap = xto.getWordPOSmap();
        this.tagAspectMap = xto.getTagAspectMap();

        this.xto = xto;

        this.dependencyGraph = null;
        entityOpinionMap = new HashMap<IndexedWord, List<String>>();
        equivalentEntityMap = new HashMap<String, List<String>>();
        negatedOpinionMap = new HashMap<String, String>();
        modifiedOpinionMap = new HashMap<String, String>();
        multiWordEntityMap = new HashMap<IndexedWord, String>();

        this.tree = tree;
        this.sortedEdges = edges;
        if (edges != null) {
            Collections.sort(this.sortedEdges, new DependencyRelationSorter());
            fireDependencyRules();
        }
    }

    public StanfordDependencyEdges(String text, XpSentenceResources xto, XpExpression xe) {
        this(text, null, null, xto, xe);

        XpText richText = new XpText(language, text, false);
        List<CoreMap> sentencesList = richText.getSentences();

        try {
            JSONObject parseTreeJSON = richText.getParseTree();
            String key;
            String treeStr;
            Iterator<?> keys = parseTreeJSON.keys();
            int sentenceIdx = 0;
            double tic = System.currentTimeMillis();
            double tac;
            while (keys.hasNext()) {
                key = (String) keys.next();
                tic = System.currentTimeMillis();
                treeStr = (String) parseTreeJSON.get(key);
                tac = System.currentTimeMillis();
                logger.info("Obtaining the parse tree in " + (tac - tic) + " ms\n" + treeStr);
                // System.out.println("Parse tree: " + treeStr);
                ConstituencyTree tree = new ConstituencyTree(sentencesList.get(sentenceIdx), treeStr);
                this.tree = tree;
                tic = System.currentTimeMillis();
                logger.info("Constituency tree obtained in " + (tic - tac) + " ms");
                sortedEdges = tree.extractAmod();
                sortedEdges.addAll(tree.extractNsubj());
                sortedEdges.addAll(tree.extractNsubjPass());
                sortedEdges.addAll(tree.extractNeg());
                sortedEdges.addAll(tree.extractDobj());
                sortedEdges.addAll(tree.extractNmod());

                //				if (sortedEdges == null) {
                //					sortedEdges = tree.extractNsubj();
                //				} else {
                //					sortedEdges.addAll(tree.extractNsubj());
                //				}
                //				if (sortedEdges == null) {
                //					sortedEdges = tree.extractNsubjPass();
                //				} else {
                //					sortedEdges.addAll(tree.extractNsubjPass());
                //				}
                sentenceIdx += 1;
            }
            tac = System.currentTimeMillis();
            logger.info("Initialized edges in " + (tac - tic) + " ms");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (sortedEdges != null) {
            Collections.sort(this.sortedEdges, new DependencyRelationSorter());
            fireDependencyRules();
        }
    }

    public void nsubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        logger.info("NSUBJ: " + dep + " nsubj " + gov);
        String depWord = dep.value();
        String govWord = gov.value();

        this.addToOpinionMap(dep, govWord);
        this.featureSet.setNsubjFeature(true);

        String govTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
        if (SentimentFunctions.isPolarTag(govTag)) {
            if (govTag.contains(XpSentiment.POSITIVE_TAG)) {
                this.featureSet.setNsubjPosFeature(true);
            } else if (govTag.contains(XpSentiment.NEGATIVE_TAG)) {
                this.featureSet.setNsubjNegFeature(true);
            }
        }

        String[] depAspect = XpOntology.findAspect(language, depWord, domainName, subject, this.xto);
        if (depAspect != null) {
            this.addToTagAspectMap(depWord, depAspect);
        }
        //		SentimentEngine.nsubjBasedSentimentClassifier(language, gov, dep, edgeRelation, dependencyGraph, wordPOSmap, tagSentimentMap, this.featureSet);

        //		try {
        //			iterativeMethodsCall(gov, dep, edgeRelation);
        //		} catch (IllegalArgumentException | SecurityException e) {
        //			e.printStackTrace();
        //		}
    }

    public void nsubjpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        logger.info("NsubjPass: " + dep + " nsubjPass " + gov);
        String depWord = dep.value();
        String govWord = gov.value();
        this.addToOpinionMap(dep, govWord);

        this.featureSet.setNsubjPassFeature(true);

        String govTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
        if (SentimentFunctions.isPolarTag(govTag)) {
            if (govTag.contains(XpSentiment.POSITIVE_TAG)) {
                this.featureSet.setNsubjPassPosFeature(true);
            } else if (govTag.contains(XpSentiment.NEGATIVE_TAG)) {
                this.featureSet.setNsubjPassNegFeature(true);
            }
        }
        String[] depAspect = XpOntology.findAspect(language, depWord, domainName, subject, this.xto);
        if (depAspect != null) {
            this.addToTagAspectMap(depWord, depAspect);
            this.featureSet.setNsubjPassAspectFeature(true);
        }
        //		SentimentEngine.nsubjBasedSentimentClassifier(language, gov, dep, edgeRelation, dependencyGraph, wordPOSmap, tagSentimentMap, this.featureSet);

        //		try {
        //			iterativeMethodsCall(gov, dep, edgeRelation);
        //			iterativeMethodsCall(dep, gov, edgeRelation);
        //		} catch (IllegalArgumentException | SecurityException e) {
        //			e.printStackTrace();
        //		}

    }

    public void neg_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

        String govWord = gov.value();
        String depWord = dep.value();

		/*Words can not express my disgust*/
        //		if (govWord.equalsIgnoreCase("express")) {
        //			IndexedWord dobjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
        //			if (dobjDep != null) {
        //				logger.info("Negation on express:\t" + depWord);
        //				this.addToTagSentimentMap(depWord.toLowerCase(), SentimentLexicon.NON_SENTI_TAG);
        //			}
        //		}

        boolean checkIsSentimentNullified = true;
        //		IndexedWord amodDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER);
        int govIdx = tree.getNegGov(govWord);
        logger.info("NEG rule: getNegGov: " + govIdx);
        Set<IndexedWord> amodDep = tree.getChildWithReln(govIdx, ConstituencyTree.amodRel);
        if (amodDep == null) {
            Set<IndexedWord> dobjDep = tree.getChildWithReln(govIdx, ConstituencyTree.dobjRel);
            if (dobjDep != null) {
                logger.info("NEG With a Word with DOBJ on AMOD: " + govWord);
                amodDep = new HashSet<IndexedWord>();
                for (IndexedWord dobjW : dobjDep) {
                    amodDep.addAll(tree.getChildWithReln(dobjW, ConstituencyTree.amodRel));
                }
            }
        }
        //		if (amodDep == null) {
        //			IndexedWord dobjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
        //			if (dobjDep != null) {
        //				/*I did not eat good food*/
        //				logger.info("NEG With a Word with DOBJ on AMOD: " + govWord);
        //				amodDep = this.dependencyGraph.getChildWithReln(dobjDep, UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER);
        //			}
        //		}
        //		IndexedWord xcompDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT);
        //		IndexedWord nsubjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT);
        Set<IndexedWord> nsubjDep = tree.getChildWithReln(govIdx, ConstituencyTree.nsubjRel);
        // Adding nsubjpass
        Set<IndexedWord> nsubjpassDep = tree.getChildWithReln(govIdx, ConstituencyTree.nsubjpassRel);
        if (XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap) != null) {
            checkIsSentimentNullified = false;
        } else if (amodDep != null && amodDep.size() > 0) {
            logger.info("NEG rule: amodDep: " + amodDep.toString());
            for (IndexedWord amodIW : amodDep) {
                if (XpSentiment.getLexTag(language, amodIW.word(), wordPOSmap, tagSentimentMap) != null) {
                    logger.info("NEG With a Word on SentimentBearing AMOD: " + govWord);
                    checkIsSentimentNullified = false;
                }
            }
        } else if (nsubjDep != null && nsubjDep.size() > 0) {
            logger.info("NEG rule: nsubj: " + nsubjDep.toString());
            for (IndexedWord nsubjIW : nsubjDep) {
                if ((PosTags.isMainVerb(language, gov.tag()) && XpSentiment.getLexTag(language, nsubjIW.word(), wordPOSmap, tagSentimentMap) != null)) {
                    logger.info("NEG With a Word on SentimentBearing NSUBJ: " + govWord);
                    checkIsSentimentNullified = false;
                }
            }
        } else if (nsubjpassDep != null && nsubjpassDep.size() > 0) {
            logger.info("NEG rule: nsubjpass: " + nsubjpassDep.toString());
            for (IndexedWord nsubjIW : nsubjpassDep) {
                if ((PosTags.isMainVerb(language, gov.tag()) && XpSentiment.getLexTag(language, nsubjIW.word(), wordPOSmap, tagSentimentMap) != null)) {
                    logger.info("NEG With a Word on SentimentBearing NSUBJPASS: " + govWord);
                    checkIsSentimentNullified = false;
                }
            }
        } else if (OntologyUtils.isWeakVerb(language, govWord)) {
            logger.info("NEG With a Weak Verb: " + govWord);
            checkIsSentimentNullified = false;
        }
        //	d	if (amodDep != null && XpSentiment.getLexTag(language, amodDep.word(), wordPOSmap, tagSentimentMap) != null) {
        //	d		/*This is not a good car: In this case we don't want to nullify the shifter*/
        //	d		/*it was not a good match*/
        //	d		logger.info("NEG With a Word on SentimentBearing AMOD: " + govWord);
        //	d		checkIsSentimentNullified = false;
        //		} else if (xcompDep != null && XpSentiment.getLexTag(language, xcompDep.word(), wordPOSmap, tagSentimentMap) != null) {
        //			/*Those sunglasses never looked good on you*/
        //			logger.info("NEG With a Word on SentimentBearing XCOMP: " + govWord);
        //			checkIsSentimentNullified = false;
        //	d	} else if (nsubjDep != null && PosTags.isVerb(language, gov.tag()) && XpSentiment.getLexTag(language, nsubjDep.word(), wordPOSmap, tagSentimentMap) != null) {
        //	d		/*help never came*/
        //	d		logger.info("NEG With a Word on SentimentBearing NSUBJ: " + govWord);
        //	d		checkIsSentimentNullified = false;
        //		} else if (this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.DETERMINER) != null) {
        //			/*Not a good car*/
        //			logger.info("NEG With a Word on Determiner: " + govWord);
        //			checkIsSentimentNullified = false;
        //	d	} else if (OntologyUtils.isWeakVerb(govWord)) {
        //	d		/*it is not a seamless well rounded app like citibank.*/
        //	d		logger.info("NEG With a Weak Verb: " + govWord);
        //	d		checkIsSentimentNullified = false;
        //		} else {
        //
        //			/*Food is not very good*/
        //			Set<IndexedWord> advmodGovList = this.dependencyGraph.getParentsWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
        //			if (advmodGovList != null && !advmodGovList.isEmpty()) {
        //				logger.info("NEG With a Word on AdvMod: " + govWord);
        //				checkIsSentimentNullified = false;
        //			}
        //		}
        logger.info("NEG: " + govWord + "\t checkIsSentimentNullified : " + checkIsSentimentNullified);
        addToNegatedOpinionMap(govWord, depWord, true, checkIsSentimentNullified);
        negBasedFeaturize(govWord.toLowerCase());

    }

    public void dobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

        String govWord = gov.word();
        String depWord = dep.word();

        logger.info("DOBJ: " + depWord + " is qualified by " + govWord);
        //		this.intentionClassification(gov, dep, edgeRelation, false);
        //		IntentEngine.intentionClassification(gov, dep, edgeRelation, this.dependencyGraph, false, this.featureSet, this.domainName, this.subject, this.negatedOpinionMap, this.tagAspectMap, tagSentimentMap, wordPOSmap, language, multiWordEntityMap);
        //		IntentEngine.intentionClassification(gov, dep, edgeRelation, this.dependencyGraph, false, this.featureSet, this.domainName, this.subject, this.negatedOpinionMap, this.tagAspectMap, tagSentimentMap, wordPOSmap, language, multiWordEntityMap);
        IntentEngine.intentionClassification(gov, dep, edgeRelation, this, false);

        String opinion = govWord;

        if (v2 != null) {
            String v2Word = v2.value();

            String[] v2Aspect = XpOntology.findAspect(language, v2Word, this.domainName, this.subject, this.xto);
            if (v2Aspect != null) {
                this.addToOpinionMap(v2, govWord + " " + depWord);
                this.addToTagAspectMap(v2Word, v2Aspect);
            } else {
                this.addToOpinionMap(dep, govWord);
            }
        }

        else {
            // String v4 = getTargetVertex(dep, RCMOD);
            //			IndexedWord v4 = this.dependencyGraph.getChildWithReln(dep, GrammaticalRelation.valueOf(DependencyRelationsLexicon.RCMOD));
            //			IndexedWord v4 = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.RELATIVE_CLAUSE_MODIFIER);
            //			if (v4 != null) {
            //				String v4Word = v4.value();
            //				logger.info("RCMOD: " + depWord + " which is  " + v4Word);
            //				opinion += " " + v4Word;
            //			}
            addToOpinionMap(dep, opinion);
        }
        //		boolean isClassified = needClassifier(gov, dep, edgeRelation);
        //		if (!isClassified) {
        //			//			System.out.println("Calling Possession Classifier");
        //			//			SentimentEngine.possessionClassifier(language, gov, dep, edgeRelation, dependencyGraph, wordPOSmap, tagSentimentMap, this.featureSet, negatedOpinionMap, domainName, subject, this.multiWordEntityMap, this.tagAspectMap);
        //			SentimentEngine.possessionClassifier(gov, dep, edgeRelation, this);
        //		}

		/* don't expect good service*/
        if (PosTags.isMainVerb(language, gov.tag())) {
            if (negatedOpinionMap.containsKey(govWord)) {
                addToNegatedOpinionMap(depWord, negatedOpinionMap.get(govWord), false, true);
            }
        }

        String govSentiment = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
        //		String depSentiment = SentimentLexicon.getLexTag(depWord.toLowerCase());

        if (SentimentFunctions.isPolarTag(govSentiment)) {
            if (govSentiment.contains(XpSentiment.POSITIVE_TAG)) {
                this.featureSet.setDobjPosFeature(true);
                //				if (depSentiment != null) {
                //					if (depSentiment.contains(SentimentLexicon.NEGATIVE_TAG)) {
                //						this.tagSentimentMap.put(govWord.toLowerCase(), SentimentLexicon.NEGATIVE_TAG);
                //						XpActivator.PW.println(this.text + "\tDobj\tPOS#NEG = Negative\t" + govWord + "#" + depWord);
                //					}
                //				}
            } else if (govSentiment.contains(XpSentiment.NEGATIVE_TAG)) {
                this.featureSet.setDobjNegFeature(true);
                //				if (depSentiment != null) {
                //					if (depSentiment.contains(SentimentLexicon.POSITIVE_TAG)) {
                //						this.tagSentimentMap.put(depWord.toLowerCase(), SentimentLexicon.NEGATIVE_TAG);
                //						XpActivator.PW.println(this.text + "\tDobj\tNEG#POS = Positive\t" + govWord + "#" + depWord);
                //					} else if (depSentiment.contains(SentimentLexicon.NEGATIVE_TAG)) {
                //						this.tagSentimentMap.put(depWord.toLowerCase(), SentimentLexicon.POSITIVE_TAG);
                //						this.tagSentimentMap.put(govWord.toLowerCase(), SentimentLexicon.POSITIVE_TAG);
                //						XpActivator.PW.println(this.text + "\tDobj\tNEG#NEG = Positive\t" + govWord + "#" + depWord);
                //					}
                //				}
            }
        }
        //		this.emotionVector.dobjVectorize(gov, dep, edgeRelation.getShortName());
    }

}