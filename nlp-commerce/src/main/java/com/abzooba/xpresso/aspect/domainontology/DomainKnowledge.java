/**
 *
 */
package com.abzooba.xpresso.aspect.domainontology;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.abzooba.xcommerce.core.XCLogger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.aspect.wordvector.FuzzyEntityRecognizer;
import com.abzooba.xpresso.aspect.wordvector.WordVectorAspect;
import com.abzooba.xpresso.aspect.wordvector.WordVectorBottomUp;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.controller.XpCacheWriterThread;
import com.abzooba.xpresso.engine.core.XpSentenceResources;
import com.abzooba.xpresso.expressions.sentiment.SentimentFunctions;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.PosTags;
import com.abzooba.xpresso.textanalytics.ProcessString;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;
import com.abzooba.xpresso.utils.FileIO;
import org.slf4j.Logger;


/**
 * @author Koustuv Saha
 * 10-Mar-2014 3:55:46 pm
 * XpressoV2 DomainConcepts
 */
public class DomainKnowledge {
    protected static final Logger logger = XCLogger.getSpLogger();

    private static JSONObject allDomainsJSONObject;
    private static Map<Languages, JSONObject> allDomainsNonSentimentWords = new HashMap<Languages, JSONObject>();
    protected static Map<Languages, Map<String, String>> allEntitiesDomainMap = new HashMap<Languages, Map<String, String>>();//new HashMap<String, String>();
    private static Map<Languages, Map<String, String>> implicitWordAttributeMap = new HashMap<Languages, Map<String, String>>();// new HashMap<String, String>();

    private JSONObject domainJSONObject;

    // private String conceptsFileName;
    // private String possibleAspectsFileName;
    // private String implicitConceptsMapFileName;
    private static Map<Languages, String> contextualSentimentFileName = new HashMap<Languages, String>();
    //	private static Map<Languages, String> conceptsClusterFileName = new HashMap<Languages, String>();

    // private String domainNonSentiFileName;
    private final String domainName;
    private final Languages language;

    // private final Map<String, String> conceptKBmap = new HashMap<String,
    // String>();
    // private final Map<String, String> secondaryConceptKBmap = new
    // HashMap<String, String>();
    private final Set<String> possibleAspectsHash = new HashSet<String>();

    //	private Map<String, String[]> cacheEntityMap = new ConcurrentHashMap<String, String[]>();
    private final Map<String, String> cacheEntityMap = new ConcurrentHashMap<String, String>();
    private final Map<String, String> cacheImplicitEntityMap = new ConcurrentHashMap<String, String>();
    //	private final Set<String> nullEntitiesSet = new ConcurrentHashSet<String>();
    //	private final Set<String> nullImplicitEntitiesSet = new ConcurrentHashSet<String>();

    private final Map<String, String> implicitConceptsMap = new HashMap<String, String>();

    private static Map<Languages, Map<String, Map<String, String>>> contextualSentimentMap = new HashMap<Languages, Map<String, Map<String, String>>>();
    //	private static Map<Languages, Map<String, String>> conceptClusterMap = new HashMap<Languages, Map<String, String>>();
    private final Set<String> domainNonSentimentConcepts = new HashSet<String>();
    private final Set<String> relevantCategoriesHash = new HashSet<String>();

    private final Map<String, String> aspectsHierarchyMap = new HashMap<String, String>();

    public static void init() {

        try {
            //			BufferedReader br = new BufferedReader(new FileReader(XpConfig.DOMAIN_CONFIG_FILE));
            StringBuilder sb = new StringBuilder();

            List<String> allLines = new ArrayList<String>();
            FileIO.read_file(XpConfig.DOMAIN_CONFIG_FILE, allLines);
            for (String line : allLines) {
                sb.append(line);
            }

            //	JSONObject allDomainsJSONObjectLang = new JSONObject(sb.toString());
            //	allDomainsJSONObject.put(lang, allDomainsJSONObjectLang);
            allDomainsJSONObject = new JSONObject(sb.toString());
            //			br.close();

            JSONObject allDomainsNonSentimentWordsLang;
            for (Languages lang : XpConfig.LANGUAGES) {
                //				br = new BufferedReader(new FileReader(XpConfig.getDomainNonSentimentFile(lang)));
                allLines.clear();
                FileIO.read_file(XpConfig.getDomainNonSentimentFile(lang), allLines);

                sb = new StringBuilder();
                //				while ((strLine = br.readLine()) != null) {
                for (String strLine : allLines) {
                    sb.append(strLine);
                }
                allDomainsNonSentimentWordsLang = new JSONObject(sb.toString());
                allDomainsNonSentimentWords.put(lang, allDomainsNonSentimentWordsLang);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (Languages lang : XpConfig.LANGUAGES) {
            //	try {
            //	BufferedReader br = new BufferedReader(new FileReader(XpConfig.DOMAIN_CONFIG_FILE));
            //	StringBuilder sb = new StringBuilder();
            //	String strLine;
            //	while ((strLine = br.readLine()) != null) {
            //		sb.append(strLine);
            //	}
            //	JSONObject allDomainsJSONObjectLang = new JSONObject(sb.toString());
            //	allDomainsJSONObject.put(lang, allDomainsJSONObjectLang);
            //	br.close();

            List<String> strLines = new ArrayList<String>();
            FileIO.read_file(XpConfig.getDomainConceptsFile(lang), strLines);
            Map<String, String> allEntitiesDomainMapLang = new HashMap<String, String>();
            for (String line : strLines) {
                String[] strArr = line.split("\t");
                if (strArr.length == 2) {
                    String entity = strArr[0].toLowerCase();
                    if (!allEntitiesDomainMapLang.containsKey(entity)) {
                        allEntitiesDomainMapLang.put(entity, strArr[1].toUpperCase().trim());
                    }
                }
            }
            allEntitiesDomainMap.put(lang, allEntitiesDomainMapLang);
            //	} catch (JSONException | IOException e) {
            //		e.printStackTrace();
            //	}

            List<String> implicitWordAttributeList = new ArrayList<String>();
            FileIO.read_file(XpConfig.getImplicitAspectLexicon(lang), implicitWordAttributeList);
            Map<String, String> implicitWordAttributeMapLang = new HashMap<String, String>();
            for (String strLine : implicitWordAttributeList) {
                String[] wordArr = strLine.split("\t");
                String word = wordArr[0].toLowerCase();
                String lemma = wordArr[1].toLowerCase();
                String attribute = wordArr[2].toUpperCase();
                implicitWordAttributeMapLang.put(word, attribute);
                implicitWordAttributeMapLang.put(lemma, attribute);
            }
            implicitWordAttributeMap.put(lang, implicitWordAttributeMapLang);
            contextualSentimentFileName.put(lang, XpConfig.getContextualSentiFilename(lang));
            //			conceptsClusterFileName.put(lang, XpConfig.getConceptsClusterFilename(lang));
        }
        loadContextualSentimentMap();
        //		loadConceptsClusterMap();
    }

    protected static String getWordVectorBottomUpCategory(Languages language, String entityStr, String domainName, boolean isProperNoun, boolean isTryEntity) {
        //	if (language == Languages.EN) {
        List<?> possibleCategories = (List<?>) WordVectorBottomUp.getCategory(language, entityStr, domainName, false, isProperNoun, isTryEntity);
        if (possibleCategories.size() == 0) {
            //				System.out.println("Returning category 0" + entityStr + "\tNULL");
            return null;
        } else if (possibleCategories.size() == 1) {
            //				System.out.println("Returning category 1" + entityStr + "\t" + possibleCategories.get(0));
            return (String) possibleCategories.get(0);
        } else {
            if (domainName == null || (domainName != null && domainName.equals(XpOntology.ALL_ASPECT))) {
                //					System.out.println("Returning category 2" + entityStr + "\t" + possibleCategories.get(0));
                return (String) possibleCategories.get(0);
            } else {
                //						centroidVectorList.parallelStream().forEach((centroidVector) -> {
                for (Object aspectCategory : possibleCategories) {
                    if (XpOntology.isDomainRelevantCategory(language, (String) aspectCategory, domainName)) {
                        return (String) aspectCategory;
                    }
                }
            }
        }
        //	}
        return null;
    }

    /**
     * @author Koustuv Saha
     * Aug 25, 2015
     * @param lang
     * @param entityStr
     * @return
     *
     */
    protected String getOntologyCategory(String entityStr, XpSentenceResources xto, boolean isProperNoun, boolean isContainsNonNounPOS, boolean isTryEntity) {
		/*isTryEntity= true: When it is likely a multiword entity: eg: french fries*/

        logger.info("GetOntologyCategory called:\t" + entityStr + "\tisProperNoun: " + isProperNoun + "\tisContainsNonNounPOS: " + isContainsNonNounPOS + "\tisTryEntity: " + isTryEntity);
        String returnType = allEntitiesDomainMap.get(language).get(entityStr);
        if (returnType != null) {
            return returnType;
        }

		/*We don't want to compute for isDirect. "bad youghurt". iPhone is a Proper Noun */
        //		if (!isDirect) {
        //		if (language == Languages.SP) {
        //			//		if (language == Languages.SP || isProperNoun) {
        //			return returnType;
        //		}
        String entityNER = null;
        if (xto != null) {
            entityNER = xto.getNerMap().get(entityStr);
        }
        if (entityNER != null && !entityNER.equals("ORGANIZATION") && !entityNER.equals("MISC")) {
            logger.info("NER contains " + entityStr + "\tNULL");
            return null;
        }

        //		System.out.println("NER MAP: " + xto.getNerMap());

        switch (XpConfig.IS_WORDVECTOR_ASPECTS_MATCH) {
            case (0):
                return allEntitiesDomainMap.get(language).get(entityStr);
            case (1):
                WordVectorAspect gloveAspect = new WordVectorAspect(entityStr, domainName, language);
                return gloveAspect.getSingleAspect();
            case (2):
                returnType = WordVector.getWordVectorCategory(language, entityStr);
                if (returnType != null) {
                    return returnType;
                }
                gloveAspect = new WordVectorAspect(entityStr, domainName, language);
                return gloveAspect.getSingleAspect();

            case (8):
                //				String columnName = domainName + "-category";
                //				return WordVectorBottomUp.getCategory(entityStr);

                String columnName = "all-category";
                returnType = WordVector.getWordVectorCategory(language, entityStr, columnName);
                //				if (returnType != null && !xto.getNerMap().containsKey(entityStr)) {
                if (returnType != null) {
                    //					System.out.println("BottomUp:\t" + entityStr + "\t" + returnType);
                    return returnType;
                }
                //				return null;
				/*Cases like "like denver" should not be considered*/
                if (!isContainsNonNounPOS) {
                    gloveAspect = new WordVectorAspect(entityStr, domainName, language);
                    return gloveAspect.getSingleAspect();
                } else {
                    return null;
                }
            case (9):
                columnName = "td-category";
                returnType = WordVector.getWordVectorCategory(language, entityStr, columnName);
                //				if (returnType != null && !xto.getNerMap().containsKey(entityStr)) {
                if (returnType != null) {
                    System.out.println("TopDown:\t" + entityStr + "\t" + returnType);
                    return returnType;
                }
				/*Cases like "like denver" should not be considered*/
                if (!isContainsNonNounPOS) {
                    gloveAspect = new WordVectorAspect(entityStr, domainName, language);
                    return gloveAspect.getSingleAspect();
                } else {
                    return null;
                }
            case (10):
                return getWordVectorBottomUpCategory(language, entityStr, domainName, isProperNoun, isTryEntity);
            //				return WordVectorBottomUp.getCategory(entityStr);

            default:
                returnType = FuzzyEntityRecognizer.entityCategoryEvaluation(entityStr, this.domainName, language);
                System.out.println("WV AspectClusterHybrid: " + entityStr + " " + returnType);
                return returnType;
            //			case (2):
            //				return FuzzyEntityRecognizer.entityCategoryEvaluation(entityStr, domain, lang);
            //			case (3):
            //				return FuzzyEntityRecognizer.entityCategoryEvaluation(entityStr, domain, lang);
            //			case (4):
            //				return FuzzyEntityRecognizer.entityCategoryEvaluation(entityStr, domain, lang);
        }

        //		this.addToCacheMap(entityStr, aspect, this.cacheEntityMap, this.nullEntitiesSet);

        //		}
        //	return null;

    }

    public DomainKnowledge(String domainName) throws IOException {
        this(Languages.EN, domainName);
    }

    public DomainKnowledge(Languages language, String domainName) throws IOException {
        this.domainName = domainName;
        this.language = language;
        if (!domainName.equals(XpOntology.ALL_ASPECT)) {
            try {
                this.domainJSONObject = allDomainsJSONObject.getJSONObject(this.domainName);
                JSONArray possibleAspectsArr = this.domainJSONObject.getJSONArray("Possible Aspects");
                for (int i = 0; i < possibleAspectsArr.length(); i++) {
                    this.possibleAspectsHash.add(possibleAspectsArr.getString(i));
                }
                JSONArray relevantCategoriesArr = this.domainJSONObject.getJSONArray("Relevant Domain Concepts");
                for (int i = 0; i < relevantCategoriesArr.length(); i++) {
                    this.relevantCategoriesHash.add(relevantCategoriesArr.getString(i));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            // this.conceptsFileName = XpConfig.DOMAIN_KB_DIR + "/" + domainName +
            // "/" + XpConfig.CONCEPTS_FILENAME;
            // this.possibleAspectsFileName = XpConfig.DOMAIN_KB_DIR + "/" +
            // domainName + "/" + XpConfig.POSSIBLE_ASPECTS_FILENAME;
            // this.implicitConceptsMapFileName = XpConfig.DOMAIN_KB_DIR + "/" +
            // domainName + "/" + XpConfig.IMPLICIT_CONCEPTS_MAP;

            // this.contextualSentimentFileName = XpConfig.DOMAIN_KB_DIR + "/" +
            // domainName + "/" + XpConfig.CONTEXTUAL_SENTI_FILENAME;
            // this.conceptsClusterFileName = XpConfig.DOMAIN_KB_DIR + "/" +
            // domainName + "/" + XpConfig.CONCEPTS_CLUSTER_FILENAME;

            // this.domainNonSentiFileName = XpConfig.DOMAIN_KB_DIR + "/" +
            // domainName + "/" + XpConfig.DOMAIN_NON_SENTI_FILENAME;
            // if (XpConfig.USE_EMBEDDED_GRAPHDB) {
            // // FileIO.read_file(this.possibleAspectsFileName,
            // // possibleAspectsHash);
            // // GraphDBLoader.loadAspectsFromGraph(conceptKBmap, false,
            // // domainName, possibleAspectsHash);
            // // GraphDBLoader.loadAspectsFromGraph(implicitConceptsMap, true,
            // // domainName, possibleAspectsHash);
            // //
            // GraphDBLoader.loadContextualSentimentFromGraph(contextualSentimentMap,
            // // domainName);
            // // GraphDBLoader.loadConceptClusterFromGraph(conceptClusterMap,
            // // domainName);
            //
            // // FileIO.read_file(this.possibleAspectsFileName,
            // possibleAspectsHash);
            // XpOntology.eGraphDBLoader.loadAspectsFromGraph(conceptKBmap, false,
            // domainName, possibleAspectsHash);
            // XpOntology.eGraphDBLoader.loadAspectsFromGraph(implicitConceptsMap,
            // true, domainName, possibleAspectsHash);
            // XpOntology.eGraphDBLoader.loadContextualSentimentFromGraph(contextualSentimentMap,
            // domainName);
            // XpOntology.eGraphDBLoader.loadConceptClusterFromGraph(conceptClusterMap,
            // domainName);
            //
            // System.out.println("implicit concepts size : " +
            // implicitConceptsMap.size());
            //
            // } else if (XpConfig.USE_GRAPH_DB) {
            // // FileIO.read_file(this.possibleAspectsFileName,
            // possibleAspectsHash);
            // XpOntology.rGraphDBLoader.loadAspectsFromGraph(conceptKBmap, false,
            // domainName, possibleAspectsHash);
            // XpOntology.rGraphDBLoader.loadAspectsFromGraph(implicitConceptsMap,
            // true, domainName, possibleAspectsHash);
            // XpOntology.rGraphDBLoader.loadContextualSentimentFromGraph(contextualSentimentMap,
            // domainName);
            // XpOntology.rGraphDBLoader.loadConceptClusterFromGraph(conceptClusterMap,
            // domainName);
            //
            // System.out.println("implicit concepts size : " +
            // implicitConceptsMap.size());
            // } else {
            this.loadConcepts();
            this.loadImplicitConcepts();
			/* for domainContextualSentiment */
            // this.loadContextualSentimentMap();
            // this.loadConceptsClusterMap();
            this.loadNonSentimentConcepts();

        }
        // }
    }

    //	public String[] getCacheEntityClass(String entity) {

    public boolean isValidAspectCategory(String category) {
        return this.possibleAspectsHash.contains(category);
    }

    protected Set<String> getPossibleAspects() {
        return this.possibleAspectsHash;
    }

    protected boolean isRelevantCategory(String category) {
        //		System.out.println("relevant categories hash : " + relevantCategoriesHash);
        //		System.out.println("category : " + category);
        //		System.out.println("Entered This method");
        boolean returnVal = this.relevantCategoriesHash.contains(category);
        if (!returnVal) {
            //			System.out.println("PossibleAspectsHash: " + this.possibleAspectsHash);
            returnVal = this.isValidAspectCategory(category);
            if (!returnVal) {
                String domainCategory = this.aspectsHierarchyMap.get(category);
                returnVal = this.isValidAspectCategory(domainCategory);
                //				System.out.println("DomainCategory in Hierarchy: " + domainCategory);
            }
        }
        //		System.out.println("checked if valid aspect : " + category + "\t" + returnVal);
        return returnVal;
    }

    /**
     * The method gets the aspect categories and the entities comprising it
     * @param language	Spanish or English
     * @return	map of aspect categories and the entities comprising it
     */
    public Map<String, List<String>> getDomainAspectEntities() {
        Map<String, List<String>> categoryEntityMap = new HashMap<String, List<String>>();
        Map<String, String> entityAspectMap = allEntitiesDomainMap.get(language);
        Map<String, Set<String>> filteredCategoryAspectMap = new HashMap<String, Set<String>>();
        for (Map.Entry<String, String> aspectCategoryPair : aspectsHierarchyMap.entrySet()) {
            String aspect = aspectCategoryPair.getKey();
            String aspectCategory = aspectCategoryPair.getValue();
            if (isValidAspectCategory(aspectCategory)) {
                if (filteredCategoryAspectMap.containsKey(aspectCategory)) {
                    filteredCategoryAspectMap.get(aspectCategory).add(aspect);
                } else {
                    Set<String> aspectList = new HashSet<String>();
                    aspectList.add(aspect);
                    filteredCategoryAspectMap.put(aspectCategory, aspectList);
                }
            }
        }

        for (String aspect : possibleAspectsHash) {
            if (filteredCategoryAspectMap.containsKey(aspect)) {
                filteredCategoryAspectMap.get(aspect).add(aspect);
            } else {
                Set<String> aspectSet = new HashSet<String>();
                aspectSet.add(aspect);
                filteredCategoryAspectMap.put(aspect, aspectSet);
            }
        }

        for (Map.Entry<String, String> entityAspectPair : entityAspectMap.entrySet()) {
            String entity = entityAspectPair.getKey();
            String aspect = entityAspectPair.getValue();
            String category = null;
            for (Map.Entry<String, Set<String>> categoryAspectPair : filteredCategoryAspectMap.entrySet()) {
                Set<String> aspectSet = categoryAspectPair.getValue();
                if (aspectSet.contains(aspect)) {
                    category = categoryAspectPair.getKey();
                    break;
                }
            }
            if (category != null) {
                if (categoryEntityMap.containsKey(category)) {
                    categoryEntityMap.get(category).add(entity);
                } else {
                    List<String> entityList = new ArrayList<String>();
                    entityList.add(entity);
                    categoryEntityMap.put(category, entityList);
                }
            }
        }
        return categoryEntityMap;
    }

    /**
     * The method gets the aspect categories and the entities comprising it
     * @param language	Spanish or English
     * @return	map of aspect categories and the entities comprising it
     */
    public static Map<String, List<String>> getGranularAspectEntities(Languages language) {
        Map<String, List<String>> categoryEntityMap = new HashMap<String, List<String>>();
        Map<String, String> entityAspectMap = allEntitiesDomainMap.get(language);

        for (Map.Entry<String, String> entityAspectPair : entityAspectMap.entrySet()) {
            String entity = entityAspectPair.getKey();
            String aspect = entityAspectPair.getValue();
            List<String> entityList = categoryEntityMap.get(aspect);

            if (entityList == null) {
                entityList = new ArrayList<String>();
            }
            entityList.add(entity);
            categoryEntityMap.put(aspect, entityList);
        }
        return categoryEntityMap;
    }

    /**
     * @author Koustuv Saha
     * 30-Apr-2014
     * @throws IOException
     * Load the concepts ontology. Domain Dependent
     */
    private void loadConcepts() {
        try {
            JSONObject aspectsHierarchyJSON = this.domainJSONObject.getJSONObject("Aspects Hierarchy");
            Iterator<?> keysItr = aspectsHierarchyJSON.keys();
            while (keysItr.hasNext()) {
                String subClassAspect = ((String) keysItr.next()).toUpperCase();
                String domainAspect;
                try {
                    domainAspect = aspectsHierarchyJSON.getString(subClassAspect).toUpperCase();
                    if (possibleAspectsHash.contains(domainAspect)) {
                        aspectsHierarchyMap.put(subClassAspect, domainAspect);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void loadImplicitConcepts() {
        JSONObject implicitAspectsJSON;
        try {
            implicitAspectsJSON = this.domainJSONObject.getJSONObject("Implicit Aspects");
            Iterator<?> keysItr = implicitAspectsJSON.keys();
            while (keysItr.hasNext()) {
                String attribute = (String) keysItr.next();
                String aspect;
                try {
                    aspect = implicitAspectsJSON.getString(attribute);
                    if (possibleAspectsHash.contains(aspect)) {
                        implicitConceptsMap.put(attribute.toUpperCase(), aspect);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

    }

    private void loadNonSentimentConcepts() {
        JSONArray nonSentimentWordsArr;
        try {
            //			nonSentimentWordsArr = allDomainsNonSentimentWords.get(language).getJSONObject(this.domainName).getJSONArray("Non Sentiment Concepts");

            JSONObject languageDomainNonSentimentMap = allDomainsNonSentimentWords.get(language);
            if (languageDomainNonSentimentMap.has(this.domainName)) {
                JSONObject nonSentimentJSONObject = allDomainsNonSentimentWords.get(language).getJSONObject(this.domainName);
                nonSentimentWordsArr = nonSentimentJSONObject.getJSONArray("Non Sentiment Concepts");
                for (int i = 0; i < nonSentimentWordsArr.length(); i++) {
                    String concept = nonSentimentWordsArr.getString(i);
                    this.domainNonSentimentConcepts.add(concept);
                    this.domainNonSentimentConcepts.add(CoreNLPController.lemmatizeToString(language, concept));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //		try {
        //			nonSentimentWordsArr = this.domainJSONObject.getJSONArray("Non Sentiment Concepts");
        //			for (int i = 0; i < nonSentimentWordsArr.length(); i++) {
        //				String concept = nonSentimentWordsArr.getString(i);
        //				this.domainNonSentimentConcepts.add(concept);
        //				// this.domainNonSentimentConcepts.add(CoreNLPController.lemmatizeToString(concept));
        //			}
        //		} catch (JSONException e) {
        //			e.printStackTrace();
        //		}

        // List<String> tempLines = new ArrayList<String>();
        // FileIO.read_file(this.domainNonSentiFileName, tempLines);
        // for (String line : tempLines) {
        // String[] strArr = line.split("\t");
        // if (strArr.length == 2) {
        // this.domainNonSentimentConcepts.add(strArr[0]);
        // this.domainNonSentimentConcepts.add(strArr[1]);
        // }
        // }
    }

    // public Map<String, String> getDomainMap() {
    // return conceptKBmap;
    // }

    // public Map<String, String> getSecondaryDomainMap() {
    // return secondaryConceptKBmap;
    // }

    //	public void addToCache(String entity, String[] entityClass) {
    private void addToCacheMap(String entity, String aspectCategory, Map<String, String> cacheMap, Map<String, String[]> tagAspectMap) {
        if (aspectCategory != null) {
            new XpCacheWriterThread<String, String>(entity, aspectCategory, cacheMap);
        } else {
            tagAspectMap.put(entity, new String[] { entity, XpOntology.NO_ASPECT_TAG });
        }
    }

    public void init(String domainName) throws IOException {
        this.loadConcepts();
		/* load secondary concepts */
        // loadSecondaryConcepts();
    }

    private String validateAndCorrectAspect(String aspectWord, String entity, String subject, boolean isDirect) {
        //		System.out.println("Received in ValidateAndCorrectAspect: " + entity + "\t" + aspectWord);
        if (domainName.equals(XpOntology.ALL_ASPECT)) {
            return aspectWord;
        }
        String returnAspect = null;
        //		System.out.println("Possible AspectsHash: " + this.possibleAspectsHash + " it contains " + this.possibleAspectsHash.contains("SOUND"));
        //		System.out.println("Trying to match " + aspectWord + " with " + this.possibleAspectsHash);
        if (aspectWord == null) {
            returnAspect = null;
        } else if (this.possibleAspectsHash.contains(aspectWord)) {
            returnAspect = aspectWord;
        } else {
            String domainAspect = this.aspectsHierarchyMap.get(aspectWord);
            if (domainAspect != null) {
                returnAspect = domainAspect;
            } else if (isDirect) {
				/*This includes cases when the entity is found in Ontology, but doesn't map to any possible aspect or higher aspect in the domain*/
                //				System.out.println("aspectWord : " + aspectWord + " entity : " + entity + " sublject : " + subject + " domain : " + this.domainName);
                logger.info("Aspect Changed to NO_ASPECT for Non Domain Relevant :\t" + entity + "\t" + aspectWord + "\t" + returnAspect);
                returnAspect = XpOntology.NO_ASPECT_TAG;
            }
        }

        //		System.out.println("Validate Aspect: " + aspectWord + "\t" + entity + "\t" + returnAspect);
        if (returnAspect != null && returnAspect.contains("COMPETITION") && subject == null) {
			/*Incase of Tmobile plans, we don't want to return Competition Telecom, if no subject*/
            logger.info("Competition Aspect Nullified for :\t" + entity + "\t" + aspectWord + "\t" + returnAspect);
            returnAspect = null;
        }
        //		System.out.println("Returning Return AspecT: " + returnAspect);
        logger.info("ReturnAspect :\t" + entity + "\t" + aspectWord + "\t" + returnAspect);
        return returnAspect;

    }

    private String[] generateAspectArr(String entityStr, String aspect, Map<String, String[]> tagAspectMap, boolean isImplicit) {

        //		if (!aspect.equals(XpOntology.NO_ASPECT_TAG)) {

        String[] retAspect = null;
        boolean addToCache = true;

        if (aspect != null) {
            retAspect = new String[2];
            retAspect[0] = entityStr;
            retAspect[1] = aspect;

            if (!aspect.equals(XpOntology.NO_ASPECT_TAG)) {
                addToCache = true;
            } else if (tagAspectMap != null && !tagAspectMap.containsKey(entityStr)) {
                tagAspectMap.put(entityStr, retAspect);
                addToCache = false;
            }
        }

		/*if aspect==null or !aspect.equals(NO_ASPECT) and not contained in tagAspectMap*/
        if (addToCache) {
            if (isImplicit) {
                this.addToCacheMap(entityStr, aspect, this.cacheImplicitEntityMap, tagAspectMap);
            } else {
                this.addToCacheMap(entityStr, aspect, this.cacheEntityMap, tagAspectMap);
            }
        }

        logger.info("EntityAspect Pair Returned:\t" + entityStr + "\t" + aspect);
        return retAspect;
    }

    public static boolean isExistingEntity(String entityStr, Languages language) {
        if (allEntitiesDomainMap.get(language).containsKey(entityStr)) {
            return true;
        } else if (allEntitiesDomainMap.get(language).containsKey(CoreNLPController.lemmatizeToString(language, entityStr))) {
            return true;
        }
        return false;
    }

    private String[] getWholeAspect(String entityStr, String subject, XpSentenceResources xto, boolean cacheCheck, boolean isTryEntity) {
        logger.info("Find Aspect For: " + entityStr);

        Map<String, String[]> tagAspectMap = xto.getTagAspectMap();
        Map<String, String> wordPOSmap = xto.getWordPOSmap();
        String[] retAspect = null;
        String aspect = null;

        if (cacheCheck) {
            //				retAspect = tagAspectMap.get(entityStr);
            if (tagAspectMap != null && tagAspectMap.containsKey(entityStr)) {
                retAspect = tagAspectMap.get(entityStr);
                if (retAspect != null) {
                    if (!retAspect[0].equals(entityStr)) {
                        retAspect[0] = entityStr;
                    }
                    logger.info("Aspect Match (Case1.1: TagAspectMap): " + entityStr + "\t" + Arrays.toString(retAspect));
                }
                return retAspect;
            }

            aspect = this.cacheEntityMap.get(entityStr);
            if (aspect != null) {
                logger.info("Aspect Match (Case1.2: Cache): " + entityStr);
                //				System.out.println("Called from Kuchi6");
                return generateAspectArr(entityStr, aspect, tagAspectMap, false);
            }

            //			if (nullEntitiesSet.contains(entityStr)) {
            //				logger.info("Aspect Match (Case1.3: NullEntitiesSet): " + entityStr);
            //				return null;
            //			}
        }

		/*LemmaCheck on multi word entities, or noun based single word Entities*/
        boolean isNounPresent = false;
        boolean isProperNoun = false;
        boolean isContainsNonNounPOS = false;
        if (wordPOSmap != null) {
            List<String> strArr = ProcessString.tokenizeString(language, entityStr);
            for (String str : strArr) {
                String posTag = wordPOSmap.get(str);
                //				System.out.println("String: " + str + "\tPosTag: " + posTag);
                if (posTag != null) {
                    if (PosTags.isProperNoun(language, posTag)) {
                        isNounPresent = true;
                        isProperNoun = true;
                    } else if ((PosTags.isNoun(language, posTag))) {
                        isNounPresent = true;
                        //						break;
                    }
                    //					else if (PosTags.isVerb(language, posTag) || PosTags.isPronoun(language, posTag) || PosTags.isAdverb(language, posTag)) {
                    //						isContainsNonNounPOS = true;
                    //					}
                    else if (PosTags.isMainVerb(language, posTag) || PosTags.isPersonalPronoun(language, posTag) || PosTags.isAdverb(language, posTag)) {
                        isNounPresent = false;
                        break;
                    } else {
                        isContainsNonNounPOS = true;
                    }
                } else {
					/*When it doesn't exist in the wordPOSmap*/
                    isNounPresent = true;
                }
            }
        } else {
            isNounPresent = true;
        }

        if (isNounPresent) {

            //			aspect = validateAndCorrectAspect(allEntitiesDomainMap.get(language).get(entityStr), entityStr, subject, true);
            //			System.out.println("Calling getOntologyCategory with " + entityStr);
            String ontologyCategory = getOntologyCategory(entityStr, xto, isProperNoun, isContainsNonNounPOS, isTryEntity);
            //			System.out.println("Ontology Category Received: " + ontologyCategory);
            aspect = validateAndCorrectAspect(ontologyCategory, entityStr, subject, true);

            if (aspect != null) {
                logger.info("Aspect Match (Case2: Direct Match): " + entityStr);
                return generateAspectArr(entityStr, aspect, tagAspectMap, false);
            }

            //			String entityLemma = CoreNLPController.lemmatizeToString(language, entityStr);
            //			aspect = validateAndCorrectAspect(getOntologyCategory(entityLemma, isProperNoun), entityLemma, subject, true);
            //			return generateAspectArr(entityStr, aspect, tagAspectMap, false);

            //			//			logger.info("Lemma for " + entityStr + " is " + entityLemma);
            //			//			System.out.println("Doing Lemma Check: " + entityLemma);
            //
            //			//			aspect = validateAndCorrectAspect(allEntitiesDomainMap.get(language).get(entityLemma), entityLemma, subject, true);
            //			if (aspect != null) {
            //				logger.info("Aspect Match (Case3: Direct Lemma): " + entityLemma);
            //				return generateAspectArr(entityStr, aspect, tagAspectMap, false);
            //			}
        } else if (!entityStr.contains(" ")) {
            String entityLemma = CoreNLPController.lemmatizeToString(language, entityStr);
            retAspect = this.getImplicitAspect(entityStr, entityLemma, subject, xto);
            if (retAspect != null) {
                logger.info("Aspect Match (Case3.1: Direct Implicit): " + entityStr + "\t" + entityLemma);
                return retAspect;
            } else {
                logger.info("Aspect Match (Null From Implicit): " + entityStr + "\t" + entityLemma);
            }

        }

        //		return generateAspectArr(entityStr, null, tagAspectMap, false);
        return null;
    }

    public String[] getAspect(String entityStr, String subject, XpSentenceResources xto) {
        return getAspect(entityStr, subject, xto, false, false);
    }

    public String[] getAspect(String entityStr, String subject, XpSentenceResources xto, boolean isTryEntity, boolean isOpinionString) {

        logger.info("Aspect Lookup for: " + entityStr);
        entityStr = entityStr.replaceAll("^s\\W", "").toLowerCase().trim();
        //		entityStr = entityStr.toLowerCase();

        //		System.out.println("Received in getAspect: " + entityStr);

        if (OntologyUtils.isRejectEntity(language, entityStr)) {
            logger.info("Aspect Match (Case0: REJECT_ENTITY): " + entityStr);
            return null;
        }

        //		String aspect = this.cacheEntityMap.get(entityStr);
        //		if (aspect != null) {
        //			logger.info("Aspect Match (Case1.2: Cache): " + entityStr);
        //			return generateAspectArr(entityStr, aspect, xto.getTagAspectMap(), false);
        //		}
        //
        //		if (nullEntitiesSet.contains(entityStr)) {
        //			logger.info("Aspect Match (Case1.3: NullEntitiesSet): " + entityStr);
        //			return null;
        //		}

		/*no need to check in the cache again*/
        String[] retAspect = this.getWholeAspect(entityStr, subject, xto, true, isTryEntity);
        //		String[] retAspect = this.getWholeAspect(entityStr, subject, xto, false, isTryEntity);

        if (retAspect != null) {
            if (retAspect[1].equals(XpOntology.NO_ASPECT_TAG)) {
                logger.info("Aspect Match (Case1 Whole Entity: NO_ASPECT): " + entityStr);
                return null;
            }
            return retAspect;
        }

        //		List<String> strArr = ProcessString.tokenizeString(entityLemma);
        //	List<String> strArr = ProcessString.tokenizeString(Languages.EN, entityStr);

        Map<String, String> wordPOSmap = xto.getWordPOSmap();
        Map<String, String[]> tagAspectMap = xto.getTagAspectMap();

        String tempEntity = entityStr.replaceAll(",", " ").replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("-", " ").replaceAll(",", " ");
        if (!tempEntity.equals(entityStr)) {
            retAspect = this.getWholeAspect(tempEntity, subject, xto, true, true);
            //						retAspect = this.getWholeAspect(bigramEntity, subject, xto, true, isTryEntity);
            if (retAspect != null) {
                if (retAspect[1].equals(XpOntology.NO_ASPECT_TAG)) {
                    logger.info("Aspect Match (Case2.1 Cleaned Entity: NO_ASPECT): " + tempEntity);
                    tagAspectMap.put(entityStr, new String[] { entityStr, XpOntology.NO_ASPECT_TAG });
                    return null;
                }
                logger.info("Aspect Match (Case2.2: Cleaned Entity Match): " + tempEntity);
                return retAspect;
            }
        }
        List<String> strArr = ProcessString.tokenizeString(language, entityStr);

        int numTokens = strArr.size();

        if (numTokens > 1 && !xto.getNerMap().containsKey(entityStr)) {
            boolean containsNonNoun = false;
            for (int i = 0; i < numTokens; i++) {
                String word = strArr.get(i);
				/*Incase it has 2 tokens, it should not again recheck for bigram->then redundant check*/
                if (numTokens > 2) {
                    if (i < numTokens - 1) {
                        String bigramEntity = word + " " + strArr.get(i + 1);
						/*For the case of bigram check: it is TryEntity=True. eg: Transformer Rescue Bot Action Figures, when Trying with Transformer Rescue*/
                        retAspect = this.getWholeAspect(bigramEntity, subject, xto, true, true);
                        //						retAspect = this.getWholeAspect(bigramEntity, subject, xto, true, isTryEntity);
                        if (retAspect != null) {
                            if (retAspect[1].equals(XpOntology.NO_ASPECT_TAG)) {
                                logger.info("Aspect Match (Case3.1.1 Bigram: NO_ASPECT): " + entityStr);
                                tagAspectMap.put(entityStr, new String[] { entityStr, XpOntology.NO_ASPECT_TAG });
                                return null;
                            }
                            logger.info("Aspect Match (Case3.2: Bigram Match): " + bigramEntity);
                            return retAspect;
                        }
                    }
                }
                if (word.length() < 2) {
                    continue;
                }
                String lemma = CoreNLPController.lemmatizeToString(language, word);

                String posTag = null;
                if (wordPOSmap != null) {
                    posTag = wordPOSmap.get(word);

                    if (posTag != null) {
						/*VB 'coz, cases like: "tastes"*/
                        if (PosTags.isAdjective(language, posTag) || PosTags.isAdverb(language, posTag) || PosTags.isMainVerb(language, posTag)) {
                            //	if (posTag.contains("JJ") || posTag.contains("RB") || posTag.contains("VB")) {
                            if (!containsNonNoun) {
                                containsNonNoun = true;
                            }

                            retAspect = this.getImplicitAspect(word, lemma, subject, xto);

                            if (retAspect != null) {
                                logger.info("Aspect Match (Case4: Implicit): " + word + "\t" + lemma);
                                return retAspect;
                            }
                            //							continue;
                        }
                        if (PosTags.isProperNoun(language, posTag)) {
                            //	if (posTag.equals("NNP")) {
                            continue;
                        }

                        if (!PosTags.isNoun(language, posTag)) {
                            //	if (!posTag.contains("NN")) {
                            if (!containsNonNoun) {
                                containsNonNoun = true;
                            }
                            continue;
                        }

                        //						System.out.println("Calling getWholeAspect for Word");
                        retAspect = this.getWholeAspect(word, subject, xto, true, isTryEntity);
                        if (retAspect != null) {
                            if (retAspect[1].equals(XpOntology.NO_ASPECT_TAG)) {
                                logger.info("Aspect Match (Case3.2.1 Individual Token: NO_ASPECT): " + entityStr);
                                return null;
                            }
							/*Tmobile plans should match with plans, before it tries partial matchings*/
                            logger.info("Aspect Match (Case3.3: Individual Word Match): " + word);

							/*if it is opinion string, then take the entityWord as it is mined. eg: I was told i only had seven days after placing my order to make a request for the exchange of my  expired medicines!!! (don't want exchange expired)*/
                            if (numTokens < 3 && !containsNonNoun && !isOpinionString) {
								/*cases like still room where still is RB, it should not be overall Entity*/
                                retAspect[0] = entityStr;
                            }
                            return retAspect;
                        }
                    }
                }

            }
        }
        return this.generateAspectArr(entityStr, null, tagAspectMap, false);
        //		return null;

    }

    protected String[] getImplicitAspect(String str, String lemmaStr, String subject, XpSentenceResources xto) {

        //		System.out.println("Checking implicit Aspect for: " + str + "\t" + lemmaStr);
        //		String[] retStr = cacheEntityMap.get(str);
        //		String[] retStr = null;
        String aspect = cacheImplicitEntityMap.get(str);

        Map<String, String[]> tagAspectMap = xto.getTagAspectMap();
        if (aspect != null) {
            //			System.out.println("Called from Kuchi3");
            return this.generateAspectArr(str, aspect, tagAspectMap, true);
        }

        //		if (this.nullImplicitEntitiesSet.contains(str)) {
        //			logger.info("Implicit Aspect Match (Case1: NullImplicitEntitiesSet): " + str);
        //			return null;
        //		}
        String domainConcept = null;

        String implicitConcept = implicitWordAttributeMap.get(language).get(str);
        //		System.out.println("implicitConcept: " + implicitConcept);

        if (implicitConcept == null) {
            implicitConcept = implicitWordAttributeMap.get(language).get(lemmaStr);
        }
        //		System.out.println("implicitConcept Lemma: " + implicitConcept);

        if (implicitConcept != null) {
            domainConcept = implicitConceptsMap.get(implicitConcept);

            if (domainConcept == null) {
                domainConcept = aspectsHierarchyMap.get(implicitConcept);
            }
        }
        //		System.out.println("ImplicitConceptsMap: " + implicitConceptsMap);
        //		System.out.println("implicitConcept's DomainConcept: " + domainConcept);

        // }

        if (domainConcept != null) {
            //			System.out.println("Called from Kuchi4");
            return this.generateAspectArr(str, domainConcept, tagAspectMap, true);
        }

        return this.generateAspectArr(str, null, tagAspectMap, true);
    }

    private static void loadContextualSentimentMap() {

        for (Languages lang : XpConfig.LANGUAGES) {
            List<String> linesList = new ArrayList<String>();
            FileIO.read_file(contextualSentimentFileName.get(lang), linesList);
            Map<String, Map<String, String>> contextualSentimentMapLang = new HashMap<String, Map<String, String>>();

            for (String line : linesList) {
                String[] wordArr = line.split("\t");
                String context = wordArr[0];

                Map<String, String> wordSentiMap = contextualSentimentMapLang.get(context);
                if (wordSentiMap == null) {
                    wordSentiMap = new HashMap<String, String>();
                }

                for (int i = 1; i < wordArr.length; i++) {
                    String[] currArr = wordArr[i].split("#");
                    // System.out.println(wordArr[i]);
                    if (currArr.length >= 2) {
                        String tag = null;
                        if (currArr[1].equals("p")) {
                            tag = XpSentiment.POSITIVE_TAG;
                        } else if (currArr[1].equals("n")) {
                            tag = XpSentiment.NEGATIVE_TAG;
                        } else if (currArr[1].equals("nn")) {
                            tag = XpSentiment.NN_TAG;
                        } else if (currArr[1].equals("nu")) {
                            tag = XpSentiment.NON_SENTI_TAG;
                        }
                        String aspect = null;
                        if (currArr.length == 3) {
                            aspect = currArr[2];
                        }
                        tag = tag + "#" + aspect;

                        if (tag != null) {
                            String word = currArr[0].toLowerCase();
                            // System.out.println("Putting in " + context + "\t" +
                            // word + "\t" + tag);
                            wordSentiMap.put(word, tag);
                            // wordSentiMap.put(CoreNLPController.lemmatizeToStringFromCache(word).toLowerCase(),
                            // tag);
                            wordSentiMap.put(CoreNLPController.lemmatizeToString(lang, word).toLowerCase(), tag);
                        }
                    }
                }
                contextualSentimentMapLang.put(context, wordSentiMap);
            }
            contextualSentimentMap.put(lang, contextualSentimentMapLang);
        }
    }

    private static String getContextualSentimentTag(Languages language, String entityLemma, String concept, String opinionWord) {
        String tag = null;
        Map<String, String> wordMap = null;

        if (concept != null) {
            concept = concept.toLowerCase();
            wordMap = contextualSentimentMap.get(language).get(concept);
        }
        if (wordMap == null) {
            //	logger.info("Using entityLemma: " + entityLemma);
            wordMap = contextualSentimentMap.get(language).get(entityLemma);
        }
        if (wordMap != null) {
            tag = wordMap.get(opinionWord);
            //	logger.info("opinionWord: " + opinionWord + "\ttag: " + tag);
            if (tag == null) {
                tag = wordMap.get(CoreNLPController.lemmatizeToString(language, opinionWord));
            }
        }
        return tag;
    }

    public static String getContextualSentimentTag(Languages language, String entity, String opinionWord, String domainName, String subject, XpSentenceResources xto) {
        entity = entity.toLowerCase();
        opinionWord = opinionWord.toLowerCase();

        String concept = null;
        String entityLemma = CoreNLPController.lemmatizeToString(language, entity);
        String[] conceptArr = XpOntology.findAspect(language, entity, domainName, subject, xto);
        if (conceptArr != null) {
            concept = conceptArr[1];
        }
        return getContextualSentimentTag(language, entityLemma, concept, opinionWord);
    }

    public static String getContextualSentimentTag(Languages language, String entity, String[] entityAspectArr, String opinionWord, String domainName, String subject, XpSentenceResources xto) {
        entity = entity.toLowerCase();
        opinionWord = opinionWord.toLowerCase();
        String entityLemma = CoreNLPController.lemmatizeToString(language, entity);
        String concept = null;
        if (entityAspectArr != null) {
            concept = entityAspectArr[1];
        }
        return getContextualSentimentTag(language, entityLemma, concept, opinionWord);
    }

    public boolean isDomainNonSentiment(String token) {
        boolean isPresent = this.domainNonSentimentConcepts.contains(token);
        if (!isPresent) {
            isPresent = this.domainNonSentimentConcepts.contains(CoreNLPController.lemmatizeToString(language, token));
        }

        return isPresent;

    }

    /**
     * @author Koustuv Saha
     * Apr 16, 2015
     * @param entity
     * @param aspect
     * @return boolean based on the successful augmentation
     *
     */
    public boolean augmentAspect(String entity, String aspect) {
        String validatedAspect = this.validateAndCorrectAspect(aspect, entity, null, false);
        if (validatedAspect != null) {
            allEntitiesDomainMap.get(language).put(aspect, entity);
            return true;
        }
        return false;
    }

    /**
     * @author Koustuv Saha
     * Apr 17, 2015
     * @param concept
     * @param opinionWord
     * @param sentiment
     * @return
     *
     */
    public static boolean augmentContextualSentiment(Languages lang, String context, String opinionWord, String sentiment) {
        String tag = SentimentFunctions.getTagFromSentiment(sentiment);
        if (tag != null) {
            Map<String, String> wordSentiMap = contextualSentimentMap.get(lang).get(context);
            if (wordSentiMap == null) {
                wordSentiMap = new HashMap<String, String>();
            }
            wordSentiMap.put(opinionWord, tag);
            wordSentiMap.put(CoreNLPController.lemmatizeToString(lang, opinionWord).toLowerCase(), tag);
            contextualSentimentMap.get(lang).put(context, wordSentiMap);
            return true;
        }
        return false;
    }

}