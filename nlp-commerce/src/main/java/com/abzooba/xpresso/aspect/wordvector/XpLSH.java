/**
 *
 */
package  com.abzooba.xpresso.aspect.wordvector;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.abzooba.xpresso.aspect.domainontology.DomainKnowledge;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;

import be.tarsos.lsh.Vector;
import be.tarsos.lsh.families.HashFunction;

/**
 * @author Koustuv Saha
 * Sep 10, 2015 11:44:39 AM
 * XpressoV3.0  WordVectorLSH
 */
public class XpLSH {

    private static final int numHashFunctions = 10;
    private static final int numHashBuckets = 200;
    // TODO: UPDATE FOR THE CLASS TO HANDLE DIFFERENT LANGUAGES
    private static final Languages language = Languages.EN;
    private static HashFunction[] hashFunctions;
    private static List<Vector> allVectors;

    private static List<Map<Integer, List<Vector>>> hashTableList;

    public static void init() {
        //		hashFamily = new CityBlockHashFamily(20, 300);

        //		hashFunctions = new CityBlockHash[numHashFunctions];
        //		hashFunctions = new EuclideanHash[numHashFunctions];
        hashFunctions = new CosineSimilarityHash[numHashFunctions];

        hashTableList = new ArrayList<Map<Integer, List<Vector>>>();

        for (int i = 0; i < numHashFunctions; i++) {
            Map<Integer, List<Vector>> hashTable = new HashMap<Integer, List<Vector>>();
            hashTableList.add(hashTable);
            //			hashFunctions[i] = new CityBlockHash(300, (i + 1) * 12);
            //			hashFunctions[i] = new EuclideanHash(300, 10000000);
            hashFunctions[i] = new CosineSimilarityHash(300, numHashBuckets);
            //			hashFunctions[i] = hashFamily.createHashFunction();
        }

        Map<String, List<String>> aspectsSeedVectorMap = DomainKnowledge.getGranularAspectEntities(language);
        allVectors = new ArrayList<Vector>();

        for (Map.Entry<String, List<String>> entry : aspectsSeedVectorMap.entrySet()) {
            List<String> currentEntitySeed = entry.getValue();

            Vector v = convertWVToTarsosVector(language, entry.getKey());
            if (v.getDimensions() == 300) {
                allVectors.add(v);
            }

            for (String seedEntity : currentEntitySeed) {
                v = convertWVToTarsosVector(language, seedEntity);
                if (v.getDimensions() == 300) {
                    allVectors.add(v);
                }
            }
        }
        try {
            preProcessing();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static Vector convertWVToTarsosVector(Languages language, String word) {
        List<?> wordVector = WordVector.getWordVector(language, word);
        Vector v = new Vector(wordVector.size());
        //				for(double seedEntityValue: seedEntityVector){
        v.setKey(word);
        for (int i = 0; i < wordVector.size(); i++) {
            v.set(i, (Double) wordVector.get(i));
        }

        return v;
    }

    private static void preProcessing() throws IOException {
        PrintWriter pw = new PrintWriter(new FileWriter("output/hashFunctionsLSH.txt"));

        for (int i = 0; i < numHashFunctions; i++) {
            Map<Integer, List<Vector>> currHashTable = hashTableList.get(i);
            for (int j = 0; j < allVectors.size(); j++) {
                Vector currVector = allVectors.get(j);
                int val = hashFunctions[i].hash(currVector);

                pw.println("HashValue:\t" + currVector.getKey() + "\t" + i + "\t" + val);
                //				System.out.println("PreProcessing on " + currVector.getKey());
                List<Vector> hashBucket = currHashTable.get(val);
                if (hashBucket == null) {
                    hashBucket = new ArrayList<Vector>();
                }
                hashBucket.add(currVector);
                currHashTable.put(val, hashBucket);
            }
        }
        pw.close();

    }

    public static String findNearestNeighbour(Languages language, String newPoint) {

        Vector newPointVector = convertWVToTarsosVector(language, newPoint);
        if (newPointVector.getDimensions() < 300) {
            System.out.println("Returning Null for: " + newPoint);
            return null;
        }

        Set<Vector> allBucketPoints = new HashSet<Vector>();
        for (int i = 0; i < numHashFunctions; i++) {
            int newPointHashValue = hashFunctions[i].hash(newPointVector);
            Map<Integer, List<Vector>> currHashTable = hashTableList.get(i);
            List<Vector> bucketPoints = currHashTable.get(newPointHashValue);
            if (bucketPoints != null) {
                allBucketPoints.addAll(bucketPoints);
            }
        }

        //		CosineDistance distanceMeasure = new CosineDistance();
        double maxSimilarity = 0;
        String maxSimilarStr = null;
        System.out.println("AllBucketPoints for: " + newPoint + "\t" + allBucketPoints.size());
        for (Vector v : allBucketPoints) {
            double currSimilarity = WordVector.computeSimilarityBetweenWords(language, newPoint, v.getKey());
            //			double currSimilarity = distanceMeasure.distance(newPointVector, v);
            if (currSimilarity > maxSimilarity) {
                maxSimilarStr = v.getKey();
                maxSimilarity = currSimilarity;
            }
            //			System.out.println("Present in bucket: " + v.getKey());
        }

        System.out.println("Maximum Similar: " + newPoint + "\t" + maxSimilarStr + "\t" + maxSimilarity);
        return maxSimilarStr;
    }

    public static void main(String[] args) throws IOException {

        XpEngine.init(true);

        init();
        //		preProcessing();

        for (int i = 0; i < hashTableList.size(); i++) {

            Map<Integer, List<Vector>> hashBucket = hashTableList.get(i);
            int j = 0;
            for (Map.Entry<Integer, List<Vector>> entry : hashBucket.entrySet()) {
                System.out.println("Number of members in: " + j++ + ": " + entry.getValue().size());
            }

            System.out.println("HashBucket Size: " + hashBucket.keySet().size());
        }

        Languages lang = Languages.EN;

        findNearestNeighbour(lang, "salami");
        findNearestNeighbour(lang, "executive");
        findNearestNeighbour(lang, "breakfast");
        findNearestNeighbour(lang, "chef");
        findNearestNeighbour(lang, "breakfast chef");
        findNearestNeighbour(lang, "chicken sandwich");
        findNearestNeighbour(lang, "actors");
        findNearestNeighbour(lang, "actor");

        //		HashFamily hashFamily = new CosineHashFamily(300);
        //		LSH lsh = new LSH(allVectors, hashFamily);
        //		lsh.buildIndex(100, 100);
        //		Vector newVector = convertWVToTarsosVector("actors");
        //
        //		System.out.println("Number of Dimensions for: " + newVector.getKey() + ":\t" + newVector.getDimensions());
        //		if (newVector.getDimensions() == 300) {
        //
        //			List<Vector> vList = lsh.query(newVector, 5);
        //			System.out.println("Top Neighbours Size: " + vList.size());
        //
        //			for (Vector v : vList) {
        //				System.out.println(v.getKey());
        //			}
        //		}
    }

}