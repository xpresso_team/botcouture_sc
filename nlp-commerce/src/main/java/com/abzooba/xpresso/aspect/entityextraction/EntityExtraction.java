package  com.abzooba.xpresso.aspect.entityextraction;

import java.util.ArrayList;
import java.util.List;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.PosTags;

import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.util.CoreMap;

public class EntityExtraction {

    private static Annotation document;

    public static void main(String[] args) throws Exception {
        CoreNLPController.init();
        Languages language = Languages.EN;
        String doc = "the staff is helpful but the wifi is slow";
        // System.out.println(extractEntityList(doc));
        annotateDocument(language, doc);
        List<CoreMap> sentencesList = document.get(SentencesAnnotation.class);
        for (CoreMap sentence : sentencesList) {
            System.out.println(extractEntityList(language, sentence));
        }
    }

    private static void annotateDocument(Languages language, String review) {
        document = new Annotation(review);
        CoreNLPController.getPipeline(language).annotate(document);
    }

    public static List<String> extractEntityList(Languages language, CoreMap sentence) throws Exception {
        List<String> entityList = new ArrayList<String>();
        String entity = "";
        for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
            String word = token.toString().toLowerCase();
            String pos = token.get(PartOfSpeechAnnotation.class);
            if (PosTags.isCommonNoun(language, pos)) {
                entity += " " + word;
            } else {
                if (!entity.equals("")) {
                    entityList.add(entity.trim());
                }
                entity = "";
            }
        }
        return entityList;
    }
}