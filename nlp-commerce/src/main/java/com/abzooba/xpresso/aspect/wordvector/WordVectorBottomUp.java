/**
 *
 */
package com.abzooba.xpresso.aspect.wordvector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.abzooba.xcommerce.core.XCLogger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.eclipse.jetty.util.ConcurrentHashSet;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.controller.XpCacheWriterThread;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;
import com.abzooba.xpresso.textanalytics.wordvector.WordVectorDB;
import com.abzooba.xpresso.textanalytics.wordvector.WordVectorMemsqlApp;
import com.abzooba.xpresso.utils.FileIO;
import com.abzooba.xpresso.utils.ThreadUtils;
import org.slf4j.Logger;


/**
 * @author Koustuv Saha
 * Sep 25, 2015 2:26:37 PM
 * XpressoV3.0  WordVectorBottomUp
 */
public class WordVectorBottomUp {

    protected static final Logger logger = XCLogger.getSpLogger();
    //	private static final String clustersFileName = "input/hospitality/Hospitality_Clusters_BottomUp.txt";
    //	private static final String clustersFileName = "input/clusters/Banking_Clusters_BottomUp.txt";
    //	private static final String clustersFileName = "domainKB/clusters/AllEntitiesClustersBottomUp.txt";
    private static final Map<Languages, String> clustersFileName = new ConcurrentHashMap<Languages, String>();
    private static final String DOMAIN_NAME = "all";
    private static Map<Languages, Set<String>> languagePossibleCategoriesSetMap = new ConcurrentHashMap<Languages, Set<String>>();
    //	private static Map<String, List<Double>> categoryCentroidVectorMap = new ConcurrentHashMap<String, List<Double>>();
    //	private static ConcurrentMap<String, List<List<Double>>> categoryCentroidVectorMap = new ConcurrentHashMap<String, List<List<Double>>>();
    private static Map<Languages, ConcurrentMap<String, Object>> categoryCentroidVectorMap = new ConcurrentHashMap<Languages, ConcurrentMap<String, Object>>();

    private static Map<Languages, Map<String, String>> languageEntityWordVectorCategoryHash = new ConcurrentHashMap<Languages, Map<String, String>>();

    private static final double PREDICTED_CATEGORY_THRESHOLD = 0.08;
    private static final int MAX_PREDICTED_CATEGORY = 5;

    class MongoUpdateThread implements Runnable {
        String line;
        int lineNo;
        Languages language;

        public MongoUpdateThread(Languages language, String line, int lineNo) {
            this.line = line;
            this.lineNo = lineNo;
            this.language = language;
        }

        /* (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        @SuppressWarnings("unchecked")
        @Override
        public void run() {
            String[] strArr = line.split("\t");
            if (strArr.length >= 2) {
                String categoryName = strArr[0];
                if (categoryName.equals("#N/A") || categoryName.equals("NA")) {
                    return;
                }
                if (categoryName.length() > 0) {

                    if (!languagePossibleCategoriesSetMap.get(language).contains(categoryName)) {
                        languagePossibleCategoriesSetMap.get(language).add(categoryName);
                        List<List<Double>> categoryCentroidVectorList = new ArrayList<List<Double>>();
                        categoryCentroidVectorMap.get(language).put(categoryName, categoryCentroidVectorList);
                    }

                    List<Double> categoryCentroidVector = new ArrayList<Double>();

                    String seedWord = strArr[1].toLowerCase();
                    Double seedConfidence = 1.0;

                    WordVectorDB wvd = XpEngine.getWordVectorDbApp();
                    String existingMongoCategory = (String) wvd.getColumnObjectForKey(language, seedWord, DOMAIN_NAME + "-category");
                    if (existingMongoCategory == null || !existingMongoCategory.equals(categoryName)) {
                        //						WordVectorMongoApp.updateDataInMongo(language, seedWord, DOMAIN_NAME + "-category", categoryName);
                        //						WordVectorMongoApp.updateDataInMongo(language, seedWord, DOMAIN_NAME + "-confidence", seedConfidence);
                        wvd.updateData(language, seedWord, DOMAIN_NAME + "-category", categoryName);
                        wvd.updateData(language, seedWord, DOMAIN_NAME + "-confidence", seedConfidence);
                    }
                    List<Double> seedWordVector = XpEngine.getWordVectorDbApp().getWordVectorForKey(language, seedWord);
                    WordVector.addVectors(categoryCentroidVector, seedWordVector);
                    System.out.println("Language: " + language + "\tProcessing: " + lineNo);

                    for (int i = 2; i < strArr.length; i++) {
                        //					for (int i = 2; i < strArr.length; i++) {
                        String[] wordArr = strArr[i].split(":");
                        if (wordArr.length == 2) {

                            String word = wordArr[0].toLowerCase();

                            //							List<Double> currWordVector = WordVectorMongoApp.getWordVectorForKey(language, word);
                            List<Double> currWordVector = XpEngine.getWordVectorDbApp().getWordVectorForKey(language, word);
                            WordVector.addVectors(categoryCentroidVector, currWordVector);

                            Double currConfidence = Double.parseDouble(wordArr[1]);
                            existingMongoCategory = (String) wvd.getColumnObjectForKey(language, word, DOMAIN_NAME + "-category");
                            if (existingMongoCategory != null && existingMongoCategory.equals(categoryName)) {
                                continue;
                            }
                            Object existingConf = wvd.getColumnObjectForKey(language, word, DOMAIN_NAME + "-confidence");
                            if (existingConf != null) {
                                Double existingConfidence = new Double(existingConf.toString());
                                if (existingConfidence != null) {
                                    if (currConfidence < existingConfidence) {
                                        continue;
                                    }
                                }
                            }
                            wvd.updateData(language, word, DOMAIN_NAME + "-category", categoryName);
                            wvd.updateData(language, word, DOMAIN_NAME + "-confidence", currConfidence);
                            System.out.println("Updated for word: " + word + "\t" + lineNo + "\t" + categoryName + "\t" + currConfidence);

                        }
                    }
                    //					System.out.println("Length of Vector: " + (strArr.length - 1));
                    WordVector.averageVector(categoryCentroidVector, strArr.length - 1);

                    //					Object categoryCentroidVectorList = WordVectorMongoApp.getColumnObjectForKey(categoryName, "centroid-vector");
                    //					if (categoryCentroidVectorList == null) {
                    //						categoryCentroidVectorList = new ArrayList<List<Double>>();
                    //					}
                    //					categoryCentroidVectorList.add(categoryCentroidVector);
                    //
                    //					WordVectorMongoApp.updateDataInMongo(categoryName, "centroid-vector", centroidVectorList);

                    List<List<?>> categoryCentroidVectorList = null;
                    categoryCentroidVectorList = (List<List<?>>) categoryCentroidVectorMap.get(language).get(categoryName);
                    categoryCentroidVectorList.add(categoryCentroidVector);
                    //					if (categoryName.equals("LOAN SERVICES")) {
                    //						System.out.println(categoryCentroidVector);
                    //						System.out.println("Loan Services size: " + categoryCentroidVectorList.size());
                    //					}
                    categoryCentroidVectorMap.get(language).put(categoryName, categoryCentroidVectorList);
                }
            }
        }

    }

    public static void init() {
        for (Languages language : XpConfig.LANGUAGES) {
            clustersFileName.put(language, XpConfig.getWordVectorClusterFile(language));
            Set<String> possibleCategoriesSetLang = new ConcurrentHashSet<String>();
            languagePossibleCategoriesSetMap.put(language, possibleCategoriesSetLang);
            ConcurrentMap<String, Object> categoryCentroidVectorMapLang = new ConcurrentHashMap<String, Object>();
            categoryCentroidVectorMap.put(language, categoryCentroidVectorMapLang);
            Map<String, String> entityCategoryMap = new ConcurrentHashMap<String, String>();
            languageEntityWordVectorCategoryHash.put(language, entityCategoryMap);
        }

        List<String> qaTaggedDomainEntities = new ArrayList<String>();
        FileIO.read_file(XpConfig.QA_TAGGED_ASPECT_FILE, qaTaggedDomainEntities);
        WordVectorDB wvd = XpEngine.getWordVectorDbApp();

        for (String currLine : qaTaggedDomainEntities) {
            String[] strArr = currLine.split("\t");
            if (strArr.length == 3) {
                String domainName = strArr[0];
                String entity = strArr[1].replaceAll(" ", "_").toLowerCase();
                String entityLemma = CoreNLPController.lemmatizeToString(entity);
                String aspectCategory = strArr[2];

                String columnName = domainName + "-category";
                wvd.updateData(Languages.EN, entity, columnName, aspectCategory);
                wvd.updateData(Languages.EN, entityLemma, columnName, aspectCategory);

                System.out.println("Updated for word: " + entity + "\t" + columnName + "\t" + aspectCategory);
            }
        }

        for (Languages language : XpConfig.LANGUAGES) {
            if (XpConfig.IS_WORDVECTOR_CATEGORIES_LOAD || (XpConfig.IS_WORDVECTOR_CATEGORIES_EN_LOAD && language == Languages.EN) || (XpConfig.IS_WORDVECTOR_CATEGORIES_SP_LOAD && language == Languages.SP)) {
                List<String> linesList = new ArrayList<String>();
                FileIO.read_file(clustersFileName.get(language), linesList, true);
                System.out.println("Successfully read clustersFile for " + language.toString());

                int lineNo = 0;
                ExecutorService executor = Executors.newFixedThreadPool(50);

                WordVectorBottomUp wb = new WordVectorBottomUp();

                //			int numLOANSERVICES = 0;
                for (String line : linesList) {
                    //				System.out.println("I am inside this loop!!!");
                    Runnable worker = wb.new MongoUpdateThread(language, line, lineNo++);
                    executor.execute(worker);
                }

                //			System.out.println("Num Loan Services: " + numLOANSERVICES);
                ThreadUtils.shutdownAndAwaitTermination(executor);
                //			System.out.println("PossibleCategoriesSet Before " + possibleCategoriesSet);

                for (Map.Entry<String, Object> entry : categoryCentroidVectorMap.get(language).entrySet()) {
                    //				for (Map.Entry<String, List<List<Double>>> entry : categoryCentroidVectorMap.entrySet()) {
                    //				System.out.println(entry.getKey() + "\t" + entry.getValue());
                    String categoryName = entry.getKey();
                    //				List<List<Double>> centroidVectorList = entry.getValue();
                    //				List<?> centroidVectorList = (List<?>) entry.getValue();
                    Object centroidVectorList = entry.getValue();
                    //				System.out.println("CategoryVectorList size: " + categoryName + "\t" + centroidVectorList.size());
                    wvd.updateData(language, categoryName, "centroid-vector", centroidVectorList);
                    //				System.out.println("Centroid Vector Calculated for Category: " + entry.getKey());
                    //	if (logger != null)
                    logger.info("Centroid Vector Calculated for Category: " + entry.getKey());
                }
                //			categoryCentroidVectorMap.clear();
                System.out.println("Loading all-category ");
                wvd.updateData(language, "xpresso-aspect-categories-list", "all-category", languagePossibleCategoriesSetMap.get(language));
            } else {
                List<?> categoriesList = null;
                switch (XpConfig.WV_DATABASE_USE) {
                    case 0:
                        categoriesList = (List<?>) wvd.getColumnObjectForKey(language, "xpresso-aspect-categories-list", "all-category");
                        break;
                    case 1:
                        List<String> catList = new ArrayList<String>();
                        @SuppressWarnings("unchecked")
                        List<List<String>> res = ((List<List<String>>) WordVectorMemsqlApp.executeQuery("select `all-category` from wordvectors where `all-category` is not null and name !='xpresso-aspect-categories-list' group by `all-category`;", 1));
                        for (int i = 1; i < res.size(); i++) {
                            catList.add(res.get(i).get(0));
                        }
                        categoriesList = catList;
                        break;
                    case 2:
                        categoriesList = (List<?>) Arrays.asList(((String) wvd.getColumnObjectForKey(language, "xpresso-aspect-categories-list", "all-category")).replaceAll("\\[", "").replaceAll("\\]", "").split(","));
                        break;
                }
                Set<String> languagePossibleCategoriesSet = languagePossibleCategoriesSetMap.get(language);
                if (languagePossibleCategoriesSet == null) {
                    languagePossibleCategoriesSet = new HashSet<String>();
                }

                if (categoriesList != null) {
                    for (Object obj : categoriesList) {
                        languagePossibleCategoriesSet.add((String) obj);
                    }
                }
                languagePossibleCategoriesSetMap.put(language, languagePossibleCategoriesSet);
            }
        }
        //				possibleCategoriesSet = (Set<String>) WordVectorMongoApp.getDistinctObjectForColumn("all-category");
        //		System.out.println("Possible Categories List: " + possibleCategoriesSet);

    }

    private static Object getCentroidVector(Languages language, String category) {
        Object centroidVectorList = categoryCentroidVectorMap.get(language).get(category);
        if (centroidVectorList == null) {
            centroidVectorList = (List<?>) XpEngine.getWordVectorDbApp().getColumnObjectForKey(language, category, "centroid-vector");
            categoryCentroidVectorMap.get(language).put(category, centroidVectorList);
        }
        return centroidVectorList;
    }

    private static String getWordVectorCategoryFromCacheAndDB(Languages language, String entityStr, String entityStrLemma, String domainName) {
        Map<String, String> wordVectorCategoryHash = languageEntityWordVectorCategoryHash.get(language);
        //		logger.info("check 1\twordVectorCategoryHash : " + wordVectorCategoryHash);
        String returnCategory = wordVectorCategoryHash.get(entityStr);
        //		logger.info("returnCategory : " + returnCategory);
        if (returnCategory == null) {
            returnCategory = wordVectorCategoryHash.get(entityStrLemma);
        }
        if (domainName != null && returnCategory == null) {
            returnCategory = WordVector.getWordVectorCategory(language, entityStr, entityStrLemma, domainName + "-category");
            //			logger.info("QA Checking 1 :\t" + entityStr + ":\t" + domainName + "-category:\t" + returnCategory);
            if (returnCategory != null && returnCategory.equalsIgnoreCase("OVERALL")) {
                return null;
            }
        }
        if (returnCategory == null) {
            returnCategory = WordVector.getWordVectorCategory(language, entityStr, entityStrLemma, DOMAIN_NAME + "-category");
            //			logger.info("QA Checking 2 :\t" + entityStr + ":\t" + DOMAIN_NAME + "-category:\t" + returnCategory);
            new XpCacheWriterThread<String, String>(entityStr, returnCategory, wordVectorCategoryHash);
        }
        return returnCategory;
    }

    public static Object getCategory(Languages language, String entityStr, boolean isJSON, boolean isProperNoun, boolean isTryEntity) {
        return getCategory(language, entityStr, null, isJSON, isProperNoun, isTryEntity);
    }

    public static Object getCategory(Languages language, String entityStr, String domainName, boolean isJSON, boolean isProperNoun, boolean isTryEntity) {

        List<String> returnCategoryList = new ArrayList<String>();
        String entityStrLemma = CoreNLPController.lemmatizeToString(language, entityStr);
        logger.info("entityStrLemma : " + entityStrLemma + " entityStr : " + entityStr);
        String returnCategory = getWordVectorCategoryFromCacheAndDB(language, entityStr, entityStrLemma, domainName);

        //		returnCategory = WordVector.getWordVectorCategory(language, entityStr, entityStrLemma, DOMAIN_NAME + "-category");
        //		logger.info("return Category in GetCategory for - " + entityStr + " category - " + returnCategory);
        int matchCase = -1;
        if (returnCategory != null) {
            matchCase = 1;
            returnCategoryList.add(returnCategory);
            //			System.out.println("WordVector BottomUp Category: " + entityStr + "\t" + returnCategory);
            //			logger.info("WordVector BottomUp Category: " + entityStr + "\t" + returnCategory);
            //			System.out.println("WordVector BottomUp Category: " + entityStr + "\t" + returnCategory);
        } else if (!isProperNoun) {
			/*No need to predict categories if ProperNoun*/
            //			double startTime = System.currentTimeMillis();
            predictCategories(language, entityStr, entityStrLemma, returnCategoryList, isTryEntity);
            if (returnCategoryList.size() > 0) {
                matchCase = 2;
                //				logger.info("WordVector BottomUp Category (Category Similarity): " + entityStr + "\t" + returnCategory);
                //				System.out.println("WordVector BottomUp Category (Category Similarity): " + entityStr + "\t" + returnCategory);
            }
        }
        //			System.out.println("Time taken to process this: " + (System.currentTimeMillis() - startTime) + " ms");

        logger.info("WordVector BottomUp Category (Match Case: " + matchCase + "): " + entityStr + "\t" + returnCategoryList);
        //		return returnCategory;
        if (!isJSON) {
            return returnCategoryList;
        } else {
            JSONObject outputJSON = new JSONObject();
            try {
                outputJSON.put("Match Type", matchCase);
                outputJSON.put("WordVectorCategories", returnCategoryList);
                outputJSON.put("Word", entityStr);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return outputJSON;
        }
    }

    private static void predictCategories(Languages language, String entityStr, String entityStrLemma, List<String> predictedCategories, boolean isTryEntity) {
        String returnCategory = null;
        //			List<?> entityVector = (List<?>) WordVector.getWordVector(entityStr);
        logger.info("entityStr: " + entityStr + " entityStrLemma: " + entityStrLemma);
        List<?> entityVector = (List<?>) WordVector.getWordVector(language, entityStrLemma);
        logger.info("WordVector BottomUp predictCategories entityStr:" + entityStr + "\tentityStrLemma: " + entityStrLemma + "\tisTryEntity: " + isTryEntity + "\tisEntityVectorNull: " + (entityVector == null ? true : false));
        if (entityVector != null && entityVector.size() > 0) {
            double maxSimilarity = 0;
            for (Object currCategory : languagePossibleCategoriesSetMap.get(language)) {
                String currCategoryStr = (String) currCategory;
                List<?> centroidVectorList = (List<?>) getCentroidVector(language, currCategoryStr);
                if (centroidVectorList != null) {
                    //						centroidVectorList.stream().forEach((centroidVector) -> {
                    //						int i = 0;
                    for (Object centroidVector : centroidVectorList) {

                        double currSimilarity = WordVector.cosSimilarity((List<?>) centroidVector, entityVector);

                        //						if (currCategoryStr.equals("BODY FLUIDS")) {
                        //							System.out.println("BLOOD BLOOD BLOOD!!!: " + currCategoryStr + "\t" + entityStr + "\t" + currSimilarity);
                        //						}

                        //							System.out.println("Checking: " + entityStrLemma + " with " + currCategoryStr + " " + i++ + "\tCurrSimilarity: " + currSimilarity + "\tMaxSimilarity: " + maxSimilarity);
                        if (currSimilarity > 0) {
                            if (currSimilarity > maxSimilarity) {
                                returnCategory = currCategoryStr;
                                maxSimilarity = currSimilarity;
                                //								System.out.println("returnCategory: " + returnCategory);
                            }
                        }
                        //						});
                    }

                }
            }

            final double maximumSimilarityFound = maxSimilarity;
            if (maximumSimilarityFound > 0 && returnCategory != null) {
                logger.info("WordVector BottomUp Category (Match Case: 2) isTryEntity: " + isTryEntity + "\t(First Match): " + entityStr + "\t" + returnCategory + "\tFirstMaxSimilarity: " + maximumSimilarityFound);
                predictedCategories.add(returnCategory);
                //				if (!isTryEntity) {
                for (String category : languagePossibleCategoriesSetMap.get(language)) {
                    List<?> centroidVectorList = (List<?>) getCentroidVector(language, category);
                    if (centroidVectorList != null) {
                        //						centroidVectorList.parallelStream().forEach((centroidVector) -> {
                        for (Object centroidVector : centroidVectorList) {
                            double currSimilarity = WordVector.cosSimilarity((List<?>) centroidVector, entityVector);

                            if (currSimilarity > 0) {
                                double absoluteDifference = Math.abs(currSimilarity - maximumSimilarityFound);
                                if (!predictedCategories.contains(category) && absoluteDifference < PREDICTED_CATEGORY_THRESHOLD) {
                                    predictedCategories.add(category);
                                    break;
                                }
                            }
                        }
                    }
                    //					}
                }
                if (predictedCategories.size() > MAX_PREDICTED_CATEGORY) {
                    logger.info("Predicted Categories Size > 5:\t" + entityStrLemma + ":\t0");
                    predictedCategories.clear();
                } else if (isTryEntity) {
					/*If it is a tryEntity, don't look for predictive match with multiple categories. eg: first episodes*/
                    predictedCategories.clear();
                    predictedCategories.add(returnCategory);
                }
            }
        }
    }

    public static void main(String[] args) {
        XpEngine.init(true);
    }

}