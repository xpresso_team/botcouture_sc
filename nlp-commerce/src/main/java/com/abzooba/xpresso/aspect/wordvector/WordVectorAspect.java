/**
 *
 */
package  com.abzooba.xpresso.aspect.wordvector;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.abzooba.xpresso.aspect.domainontology.DomainKnowledge;
import com.abzooba.xpresso.aspect.domainontology.XpOntology;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;

/**
 * @author Koustuv Saha
 * Aug 24, 2015 10:40:21 AM
 * XpressoV3.0  GloveAspect
 */
public class WordVectorAspect {

    /*Based on the score that we have calculated (tf-idf)*/
    Float[] scoreCategoryWiseVector = null;
    protected double maxSimilarity;
    //	List<String> suggestedAspects = null;
    String singleAspect = null;
    String entity;
    Languages language;
    String domainName;
    List<?> entityWordVector;

    //	protected static final double THRESHOLD_VALUE = 0.35D;

    static Map<String, List<String>> aspectsSeedVectorMap;

    public static Map<String, List<String>> getAspectsSeedVectorMap() {
        return aspectsSeedVectorMap;
    }

    public static void init() throws IOException {
        if (XpConfig.USE_LSH) {
            XpLSH.init();
        } else {
            aspectsSeedVectorMap = DomainKnowledge.getGranularAspectEntities(Languages.EN);
            for (Map.Entry<String, List<String>> entry : aspectsSeedVectorMap.entrySet()) {
                List<String> entitySeedList = entry.getValue();
                WordVector.getWordVector(Languages.EN, entry.getKey());
                for (String seedEntity : entitySeedList) {
                    WordVector.getWordVector(Languages.EN, seedEntity);
                }
            }
        }
    }

    public WordVectorAspect(String entity, String domainName, Languages language) {
        this.entity = entity;
        this.entityWordVector = WordVector.getWordVector(language, entity);
        //		this.computeGloveAspects(true);
        this.domainName = domainName;
        this.language = language;
        this.computeWordVectorAspects(true);
    }

    public WordVectorAspect(List<?> entityWordVector, String domainName, Languages language) {
        //		this.entity = entity;
        //		this.computeGloveAspects(true);

        this.entityWordVector = entityWordVector;
        this.domainName = domainName;
        this.language = language;
        this.computeWordVectorAspects(true);
    }

    protected void computeWordVectorAspects(boolean isLemma) {
        if (XpConfig.USE_LSH) {
            computeWordVectorAspectsLSH(isLemma);
        } else {
            computeWordVectorAspectsMaximal(isLemma);
        }
    }

    protected void computeWordVectorAspectsLSH(boolean isLemma) {

        //		double maxSimilarity = 0;

        //		ExecutorService executor = Executors.newFixedThreadPool(10);

        //		String maxSimilarityEntity = null;
        if (this.entity != null) {
            String nearestNeighbour = XpLSH.findNearestNeighbour(Languages.EN, entity);
            if (nearestNeighbour != null) {

                if (XpOntology.isDomainRelevantCategory(language, nearestNeighbour, domainName)) {
                    this.singleAspect = nearestNeighbour;
                } else {
                    this.singleAspect = XpOntology.getOntologyCategory(Languages.EN, this.domainName, nearestNeighbour, null);
                    if (!XpOntology.isDomainRelevantCategory(language, this.singleAspect, domainName)) {
                        this.singleAspect = null;
                    }
                }
            }
        }

        //		ThreadUtils.shutdownAndAwaitTermination(executor);
        //		if ("GENERAL".equals(this.singleAspect)) {
    }

    protected void computeWordVectorAspectsMaximal(boolean isLemma) {
        //		this.suggestedAspects = new ArrayList<String>();

        //		double maxSimilarity = 0;

        //		ExecutorService executor = Executors.newFixedThreadPool(10);

        //		String maxSimilarityEntity = null;
        for (Map.Entry<String, List<String>> entry : aspectsSeedVectorMap.entrySet()) {

            String category = entry.getKey();

            //			if (XpOntology.isDomainRelevantCategory(language, category, domainName)) {
            //			if (!XpOntology.isDomainRelevantCategory(Languages.EN, category, domainName)) {
            //				category = "GENERAL";
            //			}
            //			if (XpOntology.isDomainRelevantCategory(Languages.EN, category, domainName)) {

            //			double currSimilarityWithCategory = WordVector.computeSimilarityBetweenWords(entity, category);
            List<Double> categoryVector = WordVector.getWordVector(language, category);
            double currSimilarityWithCategory = WordVector.cosSimilarity(this.entityWordVector, categoryVector);
            if (currSimilarityWithCategory > maxSimilarity) {
                this.singleAspect = category;
                maxSimilarity = currSimilarityWithCategory;
                //				maxSimilarityEntity = category;
            }

            List<String> seedEntities = entry.getValue();

            for (String seedEntity : seedEntities) {

                //					double currSimilarity = GoogleW2V.getWord2Vec().similarity(entity, seedEntity);

                //				Runnable worker = new GloveAspectThread(entity, seedEntity, category, this);
                //				executor.execute(worker);

                List<Double> seedEntityVector = WordVector.getWordVector(language, seedEntity);
                double currSimilarity = WordVector.cosSimilarity(this.entityWordVector, seedEntityVector);

                //				double currSimilarity = WordVector.computeSimilarityBetweenWords(entity, seedEntity);
                //				System.out.println("Similarity between " + entity + "\t" + seedEntity + "\t" + currSimilarity);

                //				if (currSimilarity > THRESHOLD_VALUE) {
                if (currSimilarity > maxSimilarity) {
                    this.singleAspect = category;
                    maxSimilarity = currSimilarity;
                    //					maxSimilarityEntity = seedEntity;
                }
                //				}
                //				}
            }
        }

        //		if (this.entity != null) {
        //			System.out.println("Entity: " + entity + "\tMaxSimilarityEntity: " + maxSimilarityEntity + "\tMaxSimilarityCategory: " + this.singleAspect + "\tMaxSimilarity: " + maxSimilarity);
        //		}

        if (!XpOntology.isDomainRelevantCategory(language, this.singleAspect, domainName)) {
            this.singleAspect = null;
        }

        //		ThreadUtils.shutdownAndAwaitTermination(executor);
        //		if ("GENERAL".equals(this.singleAspect)) {
    }

    //	protected void computeWordVectorAspects(boolean isLemma) {
    //		this.suggestedAspects = new ArrayList<String>();
    //
    //		double maxSimilarity = 0;
    //		for (Map.Entry<String, List<String>> entry : aspectsSeedVectorMap.entrySet()) {
    //
    //			String category = entry.getKey();
    //
    //			if (!XpOntology.isDomainRelevantCategory(Languages.EN, category, domainName)) {
    //				category = "GENERAL";
    //			}
    //			List<String> seedEntities = entry.getValue();
    //			for (String seedEntity : seedEntities) {
    //				//					double currSimilarity = GoogleW2V.getWord2Vec().similarity(entity, seedEntity);
    //				double currSimilarity = Glove.computeSimilarityBetweenWords(entity, seedEntity);
    //				//				System.out.println("Similarity between " + entity + "\t" + seedEntity + "\t" + currSimilarity);
    //				if (currSimilarity > maxSimilarity) {
    //					this.singleAspect = category;
    //					maxSimilarity = currSimilarity;
    //				}
    //			}
    //		}
    //		if ("GENERAL".equals(this.singleAspect)) {
    //			this.singleAspect = null;
    //		}
    //	}

    /**

     /**
     * @return the singleAspect
     */
    public String getSingleAspect() {
        return singleAspect;
    }

}