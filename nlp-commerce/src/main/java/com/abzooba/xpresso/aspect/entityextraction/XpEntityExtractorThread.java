package com.abzooba.xpresso.aspect.entityextraction;

import java.util.Map;
import java.util.Set;

import com.abzooba.xpresso.engine.config.XpConfig.Annotations;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpExpression;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.ProcessString;

/**
 * @author Sumit Das
 * Jul 16, 2014 11:45:10 AM
 */
public class XpEntityExtractorThread implements Runnable {

    private String review;
    private Languages language;

    private XpEntityCommons xpEntity;

    public XpEntityExtractorThread(Languages language, String review, XpEntityCommons xpEntity) {
        this.review = review;
        this.language = language;
        this.xpEntity = xpEntity;
    }

    public void run() {
        try {
            Set<String> reviewEntitySet = null;
            int version = 1;
            if (version == 1) {
                EntityExtractionByPruning ep = new EntityExtractionByPruning();
                reviewEntitySet = ep.extractReviewEntities(language, review);
            } else {
                XpExpression xpExp = new XpExpression(language, Annotations.SENTI, false, "0", "", "", review, true);
                Map<String, Integer> tempMap = xpExp.getEntityMap();
                reviewEntitySet = tempMap.keySet();
            }
            Map<String, Integer> allEntityMap = xpEntity.getAllEntityMap();
            for (String entity : reviewEntitySet) {
                entity = entity.toLowerCase();
                if (ProcessString.isStopWord(language, entity) || entity.matches("\\W+")) {
                    continue;
                }
                String entityLemma = CoreNLPController.lemmatizeToString(language, entity);
                Integer count = allEntityMap.get(entityLemma);
                if (count == null) {
                    count = 0;
                }
                count += 1;
                allEntityMap.put(entityLemma, count);
            }
            xpEntity.reviewBatchSize++;
            xpEntity.processedReviewCount++;
            if (xpEntity.reviewBatchSize % 100 == 0) {
                xpEntity.writeMap();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}