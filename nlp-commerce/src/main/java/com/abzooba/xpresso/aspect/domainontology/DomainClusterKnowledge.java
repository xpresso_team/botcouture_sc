package com.abzooba.xpresso.aspect.domainontology;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DomainClusterKnowledge {
    private String domainName;
    private Map<String, List<double[]>> categoryCentroidMap;
    private Map<String, List<String>> categoryEntityMap;

    public DomainClusterKnowledge() {
        domainName = new String();
        categoryCentroidMap = new HashMap<String, List<double[]>>();
        categoryEntityMap = new HashMap<String, List<String>>();
    }

    public DomainClusterKnowledge(String domainName, Map<String, List<String>> categoryEntityMap, Map<String, List<double[]>> categoryCentroidMap) {
        this.domainName = domainName;
        this.categoryEntityMap = categoryEntityMap;
        this.categoryCentroidMap = categoryCentroidMap;

        //		this.categoryEntityMap = new HashMap<String, List<String>>();
        //		this.categoryEntityMap.putAll(categoryEntityMap);
        //		this.categoryEntityMap.putAll(categoryEntityMap);
        //		this.categoryCentroidMap = new HashMap<String, List<Double[]>>();
        //		this.categoryCentroidMap.putAll(categoryCentroidMap);
        //		this.categoryCentroidMap.putAll(categoryCentroidMap);
    }

    public String getDomainName() {
        return this.domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public Map<String, List<String>> getCategoryEntityMap() {
        return categoryEntityMap;
    }

    public Map<String, List<double[]>> getCategoryCentroidMap() {
        return this.categoryCentroidMap;
    }

}