/*
*      _______                       _        ____ _     _
*     |__   __|                     | |     / ____| |   | |
*        | | __ _ _ __ ___  ___  ___| |    | (___ | |___| |
*        | |/ _` | '__/ __|/ _ \/ __| |     \___ \|  ___  |
*        | | (_| | |  \__ \ (_) \__ \ |____ ____) | |   | |
*        |_|\__,_|_|  |___/\___/|___/_____/|_____/|_|   |_|
*                                                         
* -----------------------------------------------------------
*
*  TarsosLSH is developed by Joren Six at 
*  The School of Arts,
*  University College Ghent,
*  Hoogpoort 64, 9000 Ghent - Belgium
*  
* -----------------------------------------------------------
*
*  Info    : http://tarsos.0110.be/tag/TarsosLSH
*  Github  : https://github.com/JorenSix/TarsosLSH
*  Releases: http://tarsos.0110.be/releases/TarsosLSH/
* 
*/

package  com.abzooba.xpresso.aspect.wordvector;

import java.util.Random;

import be.tarsos.lsh.Vector;
import be.tarsos.lsh.families.CosineDistance;
import be.tarsos.lsh.families.DistanceMeasure;
import be.tarsos.lsh.families.HashFunction;

public class CosineSimilarityHash implements HashFunction {
    /**
     *
     */
    private static final long serialVersionUID = 778951747630668248L;
    private int numBuckets;
    final Vector randomProjection;

    public CosineSimilarityHash(int dimensions, int numBuckets) {
		/*the cosine similarity ranges between -1 to +1, so have to divide*/
        this.numBuckets = numBuckets;
        //		this.numBuckets = numBuckets / 2;
        Random rand = new Random();
        randomProjection = new Vector(dimensions);
        for (int d = 0; d < dimensions; d++) {
            //mean 0
            //standard deviation 1.0
            double val = rand.nextGaussian();
            randomProjection.set(d, val);
        }
    }

    @Override
    public int hash(Vector vector) {
        //calculate the dot product.
        DistanceMeasure dm = new CosineDistance();
        double result = 1 - dm.distance(vector, randomProjection);
        //returns a 'bit' encoded as an integer.
        //1 when positive or zero, 0 otherwise.
        //		System.out.println("Result Value: " + result);

        //		result = ((int) (result * 1000)) % numBuckets;
        result = (int) Math.ceil((result + 1) * this.numBuckets / 2);
        //		result = (int) (Math.round(result * numBuckets));

        //		result = Math.acos(1 - result) * (180.0 / 3.14);

        //		return result > 0 ? 1 : 0;
        return (int) result;
    }
}