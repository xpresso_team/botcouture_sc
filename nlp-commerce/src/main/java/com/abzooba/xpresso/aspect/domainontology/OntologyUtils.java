/**
 * 
 */
package com.abzooba.xpresso.aspect.domainontology;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.textanalytics.LuceneSpell;
import com.abzooba.xpresso.textanalytics.ProcessString;
import com.abzooba.xpresso.utils.FileIO;

/**
 * @author Koustuv Saha
 * 10-Mar-2014 4:50:29 pm
 * XpressoV2 NLPUtils
 */
public class OntologyUtils {
	private static final Map<Languages, Set<String>> pronounsHash = new HashMap<Languages, Set<String>>();
	private static final Map<Languages, Set<String>> weakVerbsHash = new HashMap<Languages, Set<String>>();
	private static final Map<Languages, Set<String>> abstractConcepts = new HashMap<Languages, Set<String>>();

	public static boolean isRejectEntity(String str) {
		return isRejectEntity(Languages.EN, str);
	}

	public static boolean isRejectEntity(Languages lang, String str) {
		if (str.matches("\\W+")) {
			return true;
		}
		if (pronounsHash.get(lang).contains(str) || weakVerbsHash.get(lang).contains(str) || ProcessString.isStopWord(lang, str)) {
			return true;
		}

		/*eg: gt*/
		if (str.length() <= 2 && !LuceneSpell.checkIfCorrectWord(lang, str, false)) {
			return true;
		}

		return false;
	}

	//	public static boolean isAbstractConcept(String str) {
	//		return isAbstractConcept(Languages.EN, str);
	//	}

	public static boolean isAbstractConcept(Languages lang, String str) {
		if (abstractConcepts.get(lang).contains(str.toLowerCase())) {
			return true;
		} else {
			return false;
		}
	}

	//	public static boolean isWeakVerb(String str) {
	//		return isWeakVerb(Languages.EN, str);
	//	}

	public static boolean isWeakVerb(Languages lang, String str) {
		Set<String> weakVerbsSet = weakVerbsHash.get(lang);
		if (weakVerbsSet != null && weakVerbsSet.contains(str)) {
			return true;
		} else {
			return false;
		}
	}

	public static void init() {

		for (Languages lang : XpConfig.LANGUAGES) {
			Set<String> proHash = new HashSet<String>();
			Set<String> weakHash = new HashSet<String>();
			Set<String> absHash = new HashSet<String>();

			FileIO.read_file(XpConfig.getPronounWords(lang), proHash);
			FileIO.read_file(XpConfig.getWeakVerbs(lang), weakHash);
			FileIO.read_file(XpConfig.getAbstractConcepts(lang), absHash);

			pronounsHash.put(lang, proHash);
			weakVerbsHash.put(lang, weakHash);
			abstractConcepts.put(lang, absHash);
		}

		//		RejectEntHash.addAll(WeakVerbsHash);
		//		RejectEntHash.addAll(pronounsHash);

	}

}
