package  com.abzooba.xpresso.aspect.wordvector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.abzooba.xpresso.aspect.domainontology.DomainClusterKnowledge;
import com.abzooba.xpresso.aspect.domainontology.DomainKnowledge;
import com.abzooba.xpresso.aspect.domainontology.XpOntology;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.machinelearning.XpClustering;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;

public class FuzzyEntityRecognizer {

    private static Map<Languages, Map<String, DomainClusterKnowledge>> langDomainKnowledgeMap = null;
    //	private static double MIN_SIMILARITY = 0.45;
    private static final String GENERIC_DOMAIN = "generic";

    public static void init(boolean readModelFromFile) {
        boolean isGranular = false;

        if (XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 3 || XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 4) {
            //			MIN_SIMILARITY = 0.2;
            isGranular = true;
        }

        //		else {
        //			MIN_SIMILARITY = 0.4;
        //		}
        generateDomainKnowledgeMap(readModelFromFile, isGranular);
    }

    //	public static void initDomain(boolean readModelFromFile) {
    //		langDomainKnowledgeMap = new HashMap<Languages, Map<String, DomainClusterKnowledge>>();
    //		for (Languages lang : XpConfig.LANGUAGES) {
    //			if (lang.equals(XpConfig.Languages.SP))
    //				continue;
    //			Map<String, DomainClusterKnowledge> langDomainMap = new HashMap<String, DomainClusterKnowledge>();
    //			String domainNames = XpConfig.DOMAIN_NAMES_LIST;
    //			String[] domainNamesArr = domainNames.split(",");
    //			for (int i = 0; i < domainNamesArr.length; i++) {
    //				//				if (!domainNamesArr[i].equals("banking"))
    //				//					continue;
    //				System.out.println("domainName - " + domainNamesArr[i]);
    //				System.out.println("domain index - " + XpOntology.domainNameIdxMap.get(domainNamesArr[i]));
    //				Map<String, List<String>> categoryEntityMap = new HashMap<String, List<String>>();
    //				categoryEntityMap.putAll(XpOntology.domainConceptsArr.get(lang)[XpOntology.domainNameIdxMap.get(domainNamesArr[i])].getdomainAspectEntities());
    //				Map<String, List<Double[]>> categoryCentroidMap = null;
    //				if (!readModelFromFile) {
    //					try {
    //						categoryCentroidMap = clusterEntitiesInDomain(categoryEntityMap);
    //						DomainClusterKnowledge domObj = new DomainClusterKnowledge(domainNamesArr[i], categoryEntityMap, categoryCentroidMap);
    //						langDomainMap.put(domainNamesArr[i], domObj);
    //					} catch (Exception e) {
    //						// TODO Auto-generated catch block
    //						e.printStackTrace();
    //					}
    //				}
    //
    //			}
    //			langDomainKnowledgeMap.put(lang, langDomainMap);
    //		}
    //
    //	}

    public static void generateDomainKnowledgeMap(boolean readModelFromFile, boolean isGranular) {
        System.out.println("Inside GenerateDomainKnowledgeMap");
        langDomainKnowledgeMap = new HashMap<Languages, Map<String, DomainClusterKnowledge>>();
        for (Languages lang : XpConfig.LANGUAGES) {
            if (lang.equals(XpConfig.Languages.SP)) {
                continue;
            }
            Map<String, DomainClusterKnowledge> languageDomainMap = new HashMap<String, DomainClusterKnowledge>();
            String domainNames = XpConfig.DOMAIN_NAMES_LIST;
            String[] domainNamesArr = domainNames.split(",");

            if (!isGranular) {
                for (int i = 0; i < domainNamesArr.length; i++) {

                    Map<String, List<String>> categoryEntityMap = null;

                    String domainName = null;
                    domainName = domainNamesArr[i];
                    categoryEntityMap = XpOntology.getDomainAspectEntities(lang, domainName);

                    Map<String, List<double[]>> categoryCentroidMap = new HashMap<String, List<double[]>>();

                    if (!readModelFromFile) {
                        try {
                            clusterEntitiesInDomain(lang, categoryEntityMap, categoryCentroidMap);
                            DomainClusterKnowledge domObj = new DomainClusterKnowledge(domainName, categoryEntityMap, categoryCentroidMap);
                            languageDomainMap.put(domainName, domObj);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            } else {
                Map<String, List<String>> categoryEntityMap = DomainKnowledge.getGranularAspectEntities(lang);
                String domainName = GENERIC_DOMAIN;
                Map<String, List<double[]>> categoryCentroidMap = new HashMap<String, List<double[]>>();

                if (!readModelFromFile) {
                    try {
                        clusterEntitiesInDomain(lang, categoryEntityMap, categoryCentroidMap);
                        DomainClusterKnowledge domObj = new DomainClusterKnowledge(domainName, categoryEntityMap, categoryCentroidMap);
                        languageDomainMap.put(domainName, domObj);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            langDomainKnowledgeMap.put(lang, languageDomainMap);
        }

    }

    //	public static void initGranular(boolean readModelFromFile) {
    //		langDomainKnowledgeMap = new HashMap<Languages, Map<String, DomainClusterKnowledge>>();
    //		for (Languages lang : XpConfig.LANGUAGES) {
    //			if (lang.equals(XpConfig.Languages.SP))
    //				continue;
    //			Map<String, DomainClusterKnowledge> langDomainMap = new HashMap<String, DomainClusterKnowledge>();
    //			//			Map<String, List<String>> categoryEntityMap = new HashMap<String, List<String>>();
    //			//			categoryEntityMap.putAll(DomainKnowledge.getGranularAspectEntities(lang));
    //			Map<String, List<String>> categoryEntityMap = DomainKnowledge.getGranularAspectEntities(lang);
    //
    //			Map<String, List<Double[]>> categoryCentroidMap = null;
    //			if (!readModelFromFile) {
    //				try {
    //					categoryCentroidMap = clusterEntitiesInDomain(categoryEntityMap);
    //					DomainClusterKnowledge domObj = new DomainClusterKnowledge(GENERIC_DOMAIN, categoryEntityMap, categoryCentroidMap);
    //					langDomainMap.put(GENERIC_DOMAIN, domObj);
    //				} catch (Exception e) {
    //					// TODO Auto-generated catch block
    //					e.printStackTrace();
    //				}
    //			}
    //			langDomainKnowledgeMap.put(lang, langDomainMap);
    //		}
    //
    //	}

    private static void clusterEntitiesInDomain(Languages language, Map<String, List<String>> categoryEntityMap, Map<String, List<double[]>> categoryCentroidList) throws Exception {
        //		System.out.println("Clustering Entities in Domain");
        for (Entry<String, List<String>> categoryEntityPair : categoryEntityMap.entrySet()) {
            //			System.out.println("cluster for category " + categoryEntityPair.getKey());
            String categoryEntity = categoryEntityPair.getKey();
            XpClustering clusterInstance = new XpClustering(language, categoryEntity, categoryEntityPair.getValue());
            //			System.out.println("Calling listClusterRepresentatives");
            List<double[]> centroidList = clusterInstance.listClusterRepresentatives();
            if (centroidList != null) {
                //				System.out.println("Putting : " + categoryEntity + "\t" + centroidList);
                categoryCentroidList.put(categoryEntity, centroidList);
            }
        }
    }

    //	public static List<String> entityCategoryEvaluation(List<String> entityList, String domain, Languages lang) {
    //		List<String> entitySimilarityTupleList = new ArrayList<String>();
    //		for (String entity : entityList) {
    //			if (XpOntology.isDomainEntity(lang, entity, domain)) {
    //				System.out.println("entity : " + entity + " already of domain : " + domain);
    //				continue;
    //			}
    //			String category =
    //				entitySimilarityTupleList.add(entity + "\t" + simCategory + "\t" + maxSim + "\t" + entityCategorySimilarityMap.toString());
    //		}
    //		return entitySimilarityTupleList;
    //	}

    public static String entityCategoryEvaluation(String entity, String domain, Languages lang) {
        Map<String, DomainClusterKnowledge> domainKnowledgeMap = langDomainKnowledgeMap.get(lang);
        String mapKey = (XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 2) ? domain : GENERIC_DOMAIN;
        Map<String, List<double[]>> domainCategoryCentroidMap = domainKnowledgeMap.get(mapKey).getCategoryCentroidMap();
        String simCategory = null;
        if (domainCategoryCentroidMap != null) {
            double maxSim = -1.0;
            for (Entry<String, List<double[]>> entry : domainCategoryCentroidMap.entrySet()) {
                //			System.out.println("key : " + entry.getKey() + " domain : " + domain);
                //				if ((XpConfig.IS_GLOVE_ASPECTS_MATCH == 3 || XpConfig.IS_GLOVE_ASPECTS_MATCH == 4) && !XpOntology.isDomainRelevantCategory(lang, entry.getKey(), domain)) {
                //					continue;
                //				}

                double simVal = computeEntityCategorySimilarity(lang, entity, entry.getValue());
                if (simVal > maxSim) {
                    maxSim = simVal;
                    simCategory = entry.getKey();
                }
            }
            System.out.println("Max Similarity: " + maxSim + "\tMaxSim AspectCategory: " + simCategory);

            if (simCategory != null && !XpOntology.isDomainRelevantCategory(lang, simCategory, domain)) {
                System.out.println("Language: " + lang + "\tEntity: " + entity + "\tSimCategory: " + simCategory + "\tDomainName: " + domain);
                simCategory = null;
            }
            //			if (maxSim < MIN_SIMILARITY) {
            //				return null;
            //			}
        }

        System.out.println("returning: " + simCategory);
        return simCategory;
    }

    //	private static double computeEntityCategorySimilarity(String entity, List<Double[]> centroidList) {
    //		double maxSimilarity = -1.0;
    //		List<Float> entityVector = Glove.getGloveVector(entity);
    //		Double[] entityVectorArr = new Double[entityVector.size()];
    //		for (int i = 0; i < entityVectorArr.length; i++) {
    //			double tempX = entityVector.get(i).floatValue();
    //			entityVectorArr[i] = tempX;
    //		}
    //		for (Double[] centroid : centroidList) {
    //			double simVal = Glove.cosSimilarity(entityVectorArr, centroid);
    //			if (simVal > maxSimilarity)
    //				maxSimilarity = simVal;
    //		}
    //		return maxSimilarity;
    //
    //	}

    private static double computeEntityCategorySimilarity(Languages language, String entity, List<double[]> centroidList) {
        double maxSimilarity = -1.0;
        double[] entityVectorArr = null;
        //		if (XpConfig.IS_GLOVE_ASPECTS_MATCH == 6) {
        //			entityVectorArr = GoogleW2V.getWordVector(entity);
        //		}
        //		else {
        List<Double> entityVector = WordVector.getWordVector(language, entity);
        entityVectorArr = new double[entityVector.size()];
        for (int i = 0; i < entityVectorArr.length; i++) {
            double tempX = entityVector.get(i).floatValue();
            entityVectorArr[i] = tempX;
        }
        //		}

        for (double[] centroid : centroidList) {
            double simVal = WordVector.cosSimilarity(entityVectorArr, centroid);
            if (simVal > maxSimilarity)
                maxSimilarity = simVal;
        }
        return maxSimilarity;

    }

    //	public static void main(String[] args) throws Exception {
    //		String propsFile = "./conf/xpressoConfig.properties";
    //		XpEngine.init(propsFile, true);
    //		Map<String, List<Double[]>> categoryCentroidList = FuzzyEntityRecognizer.clusterEntitiesInDomain();
    //		String newEntityFile = "./input/banking/extracted-entity-banking.txt";
    //		String outFile = "./output/entity_aspect_classification.txt";
    //		List<String> entityList = new ArrayList<String>();
    //		//		entityList.add("ravioli");
    //		FileIO.read_file(newEntityFile, entityList);
    //		List<String> resultsList = entityCategoryEvaluation(entityList, categoryCentroidList);
    //		FileIO.write_file(resultsList, outFile, false);
    //	}
}