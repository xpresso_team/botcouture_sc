/**
 *
 */
package com.abzooba.xpresso.machinelearning;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.textanalytics.ProcessString;

/**
 * @author Koustuv Saha
 * 30-May-2014 2:21:55 pm
 * XpressoV2.0.1  FeatureSet
 */
public class XpFeatureVector implements Cloneable {

    private static Map<String, Integer> conceptIndexMap = new ConcurrentHashMap<String, Integer>();

    //	public static void init() {
    //		conceptIndexMap.put("great food", 0);
    //	}

    private String targetClass;

    private boolean isNsubjFeature = false;
    private boolean isNsubjPosFeature = false;
    private boolean isNsubjNegFeature = false;
    private boolean isNsubjAspectFeature = false;

    private boolean isNsubjPassFeature = false;
    private boolean isNsubjPassPosFeature = false;
    private boolean isNsubjPassNegFeature = false;
    private boolean isNsubjPassAspectFeature = false;

    private boolean isNsubjShiftPronounSubjectFeature = false;

    private boolean isNsubjCopFeature = false;
    private boolean isNsubjCopPosFeature = false;
    private boolean isNsubjCopNegFeature = false;
    private boolean isNsubjCopAspectFeature = false;

    private boolean isAmodFeature = false;
    private boolean isAmodPosFeature = false;
    private boolean isAmodNegFeature = false;
    private boolean isAmodAspectFeature = false;

    private boolean isAdvmodFeature = false;
    private boolean isAdvmodIntsfFeature = false;

    private boolean isDobjPosFeature = false;
    private boolean isDobjNegFeature = false;

    private boolean isNegateFeature = false;
    private boolean isNegatePosFeature = false;
    private boolean isNegateNegFeature = false;
    private boolean isNegateNPIFeature = false;
    private boolean isNegatePPIFeature = false;

    private boolean isNeedFeature = false;
    private boolean isNeedNegFeature = false;
    private boolean isNeedPosFeature = false;

    private boolean isCouldNegFeature = false;
    private boolean isCouldPosFeature = false;
    private boolean isShouldNegFeature = false;

    private boolean isIntsfFeature = false;
    private boolean isIntsfNPIFeature = false;
    private boolean isIntsfPPIFeature = false;
    private boolean isIntsfPosFeature = false;
    private boolean isIntsfNegFeature = false;
    private boolean isIntsfUcPosFeature = false;
    private boolean isIntsfUcNegFeature = false;

    private boolean isIncrPosFeature = false;
    private boolean isIncrNegFeature = false;

    //	private boolean isIncrPreFeature = false;
    //	private boolean isDecrPreFeature = false;

    private boolean isShiftFeature = false;
    private boolean isShiftNegFeature = false;
    private boolean isShiftPosFeature = false;

    //	private boolean isIncrPostFeature = false;
    //	private boolean isDecrPostFeature = false;

    private boolean isUnconNegFeature = false;
    private boolean isUnconPosFeature = false;

    //	private boolean isPreIncrFeature = false;
    private boolean isPreIncrPPIFeature = false;
    private boolean isPreIncrNPIFeature = false;
    private boolean isPreIncrPosFeature = false;
    private boolean isPreIncrNegFeature = false;
    private boolean isPreIncrUcPosFeature = false;
    private boolean isPreIncrUcNegFeature = false;

    private boolean iConsumeIncrRsrcFeature = false;
    private boolean isConsumeDecrRsrcFeature = false;

    //	private boolean isPreDecrFeature = false;
    private boolean isPreDecrPPIFeature = false;
    private boolean isPreDecrNPIFeature = false;
    private boolean isPreDecrPosFeature = false;
    private boolean isPreDecrNegFeature = false;
    private boolean isPreDecrUcPosFeature = false;
    private boolean isPreDecrUcNegFeature = false;

    //	private boolean isPostIncrFeature = false;
    private boolean isPostIncrPPIFeature = false;
    private boolean isPostIncrNPIFeature = false;
    private boolean isPostIncrPosFeature = false;
    private boolean isPostIncrNegFeature = false;
    private boolean isPostIncrUcPosFeature = false;
    private boolean isPostIncrUcNegFeature = false;

    //	private boolean isPostDecrFeature = false;
    private boolean isPostDecrPPIFeature = false;
    private boolean isPostDecrNPIFeature = false;
    private boolean isPostDecrPosFeature = false;
    private boolean isPostDecrNegFeature = false;
    private boolean isPostDecrUcPosFeature = false;
    private boolean isPostDecrUcNegFeature = false;

    private boolean isPossnFeature = false;

    private boolean isPresentPosFeature = false;
    private boolean isPresentNegFeature = false;

    private boolean isHigherPosFeature = false;
    private boolean isHigherNegFeature = false;

    private boolean isAdvmodSentimentNullifierFeature = false;

    private int countPosFeature = 0;
    private int countNegFeature = 0;
    private int countStopWordsFeature = 0;

    private double percentagePositiveFeature = 0.0;
    private double percentageNegativeFeature = 0.0;
    //	private boolean isSubjectPresentFeature = false;
    private boolean isMainSentenceFeature = false;

    //	private boolean isPeriodPresentFeature = false;

    private int ruleBasedSentimentFeature = 2;
    private int stanfordSentimentFeature = 2;
    private boolean isStanfordHighIntensityFeature = false;
    private String stanfordSentimentStr = null;

    //	private int xpressoSentimentFeature = 2;
    private double sentiWNSentimentFeature = 0.0;

    private boolean isPresentPPIFeature = false;
    private boolean isPresentNPIFeature = false;

    private boolean isAdjFeature = false;
    private boolean isCompAdjFeature = false;
    private boolean isSupAdjFeature = false;

    private boolean isAdvbFeature = false;
    private boolean isCompAdvbFeature = false;
    private boolean isSupAdvbFeature = false;

    private boolean isPosEmoticonPresentFeature = false;
    private boolean isNegEmoticonPresentFeature = false;

    private boolean isPosIdiomPresentFeature = false;
    private boolean isNegIdiomPresentFeature = false;

    private boolean isPosPhrasalVerbPresentFeature = false;
    private boolean isNegPhrasalVerbPresentFeature = false;

    private boolean isAbusePresentFeature = false;
    private boolean isAdvocatePresentFeature = false;
    private boolean isSuggestionPresentFeature = false;
    private boolean isQuestionFeature = false;
    private boolean isQuestionPositiveFeature = false;
    private boolean isQuestionNegativeFeature = false;
    private boolean isStartsWithVBbaseFeature = false;
    private boolean isStartsWithWeakVerbFeature = false;

    private boolean isCCPresentFeature = false;
    private boolean isShiftedCCPresentFeature = false;

    private boolean isVeryNegPhrasePresentFeature = false;
    private boolean isNegPhrasePresentFeature = false;
    private boolean isNeutralPhrasePresentFeature = false;
    private boolean isPosPhrasePresentFeature = false;
    private boolean isVeryPosPhrasePresentFeature = false;

    private boolean isCapitalizedPresentFeature = false;
    private boolean isGreetingsSalutationPresentFeature = false;

    private boolean isSentimentPrimarilyStanford = false;

    private boolean isIntentionFeature = false;
    private Set<String> intentionObject = null;
    //	private XpSentenceResources xto = null;

    private boolean isSubjectFirstPerson = false;
    private boolean isSubjectSecondThirdPerson = false;
    private boolean isActionInFuture = false;

    public void setIsSubjectFirstPerson(boolean isSubjectFirstPerson) {
        this.isSubjectFirstPerson = isSubjectFirstPerson;
    }

    public boolean isSubjectFirstPerson() {
        return this.isSubjectFirstPerson;
    }

    public void setIsSubjectSecondThirdPerson(boolean isSubjectSecondThirdPerson) {
        this.isSubjectSecondThirdPerson = isSubjectSecondThirdPerson;
    }

    public boolean isSubjectSecondThirdPerson() {
        return this.isSubjectSecondThirdPerson;
    }

    public boolean isActionInFuture() {
        return this.isActionInFuture;
    }

    public void setIsActionInFuture(boolean isActionInFuture) {
        this.isActionInFuture = isActionInFuture;
    }

    //	private List<Integer> isDynamicConceptFeature = null;

    private static Map<String, String> phraseClassMap = new HashMap<String, String>();

    //	private Map<String, Boolean> unigramsFeature = new LinkedHashMap<String, Boolean>();
    //	public static List<String> ngramsList = new ArrayList<String>();

    //	public static void init() throws IOException {
    //		//		List<String> linesList = new ArrayList<String>();
    //		//		FileIO.read_file("output/ngrams.txt", linesList);
    //		//		for (String line : linesList) {
    //		//			String[] strArr = line.split("\t");
    //		//			String ngram = strArr[0];
    //		//			int count = Integer.parseInt(strArr[1]);
    //		//			if (count > 10) {
    //		//				ngramsList.add(ngram);
    //		//			}
    //		//		}
    //
    //		//		BufferedReader br = new BufferedReader(new FileReader("input/kaggle/importantPhrases.csv"));
    //		//		String strLine;
    //		//		while ((strLine = br.readLine()) != null) {
    //		//			String[] strArr = strLine.split(",");
    //		//			phraseClassMap.put(strArr[0], strArr[1]);
    //		//		}
    //		//		br.close();
    //	}

    /**
     * @return the conceptIndexMap
     */
    public static Map<String, Integer> getConceptIndexMap() {
        return conceptIndexMap;
    }

    /**
     * @param conceptIndexMap the conceptIndexMap to set
     */
    public static void setConceptIndexMap(Map<String, Integer> conceptIndexMap) {
        XpFeatureVector.conceptIndexMap = conceptIndexMap;
    }

    //	private static int getIndex(String concept) {
    //		Integer index;
    //		index = conceptIndexMap.get(concept);
    //		if (index == null) {
    //			index = conceptIndexMap.size();
    //			conceptIndexMap.put(concept, index);
    //		}
    //		return index;
    //	}

    /**
     * @return the targetClass
     */
    public String getTargetClass() {
        return targetClass;
    }

    /**
     * @param targetClass the targetClass to set
     */
    public void setTargetClass(String targetClass) {
        this.targetClass = targetClass;
    }

    //	/**
    //	 * @return the isDynamicConceptFeature
    //	 */
    //	public List<Integer> getIsDynamicConceptFeature() {
    //		return isDynamicConceptFeature;
    //	}
    //
    //	/**
    //	 * @param isDynamicConceptFeature the isDynamicConceptFeature to set
    //	 */
    //	public void setIsDynamicConceptFeature(String concept) {
    //		int index = getIndex(concept);
    //		if (this.isDynamicConceptFeature == null) {
    //			this.isDynamicConceptFeature = new ArrayList<Integer>();
    //		}
    //		this.isDynamicConceptFeature.add(index);
    //		//		this.isDynamicConceptFeature = isDynamicConceptFeature;
    //	}

    /**
     * @return the isNsubjFeature
     */
    public boolean isNsubjFeature() {
        return isNsubjFeature;
    }

    /**
     * @param isNsubjFeature the isNsubjFeature to set
     */
    public void setNsubjFeature(boolean isNsubjFeature) {
        this.isNsubjFeature = isNsubjFeature;
    }

    /**
     * @return the isNsubjPosFeature
     */
    public boolean isNsubjPosFeature() {
        return isNsubjPosFeature;
    }

    /**
     * @param isNsubjPosFeature the isNsubjPosFeature to set
     */
    public void setNsubjPosFeature(boolean isNsubjPosFeature) {
        this.isNsubjPosFeature = isNsubjPosFeature;
    }

    /**
     * @return the isNusbjShiftPronounSubject
     */
    public boolean isNsubjShiftPronounSubjectFeature() {
        return isNsubjShiftPronounSubjectFeature;
    }

    /**
     * @param isNusbjShiftPronounSubject the isNusbjShiftPronounSubject to set
     */
    public void setNsubjShiftPronounSubjectFeature(boolean isNusbjShiftPronounSubject) {
        this.isNsubjShiftPronounSubjectFeature = isNusbjShiftPronounSubject;
    }

    /**
     * @return the isNsubjNegFeature
     */
    public boolean isNsubjNegFeature() {
        return isNsubjNegFeature;
    }

    /**
     * @param isNsubjNegFeature the isNsubjNegFeature to set
     */
    public void setNsubjNegFeature(boolean isNsubjNegFeature) {
        this.isNsubjNegFeature = isNsubjNegFeature;
    }

    /**
     * @return the isNsubjAspectFeature
     */
    public boolean isNsubjAspectFeature() {
        return isNsubjAspectFeature;
    }

    /**
     * @param isNsubjAspectFeature the isNsubjAspectFeature to set
     */
    public void setNsubjAspectFeature(boolean isNsubjAspectFeature) {
        this.isNsubjAspectFeature = isNsubjAspectFeature;
    }

    /**
     * @return the isNsubjPassFeature
     */
    public boolean isNsubjPassFeature() {
        return isNsubjPassFeature;
    }

    /**
     * @param isNsubjPassFeature the isNsubjPassFeature to set
     */
    public void setNsubjPassFeature(boolean isNsubjPassFeature) {
        this.isNsubjPassFeature = isNsubjPassFeature;
    }

    /**
     * @return the isNsubjPassPosFeature
     */
    public boolean isNsubjPassPosFeature() {
        return isNsubjPassPosFeature;
    }

    /**
     * @param isNsubjPassPosFeature the isNsubjPassPosFeature to set
     */
    public void setNsubjPassPosFeature(boolean isNsubjPassPosFeature) {
        this.isNsubjPassPosFeature = isNsubjPassPosFeature;
    }

    /**
     * @return the isNsubjPassNegFeature
     */
    public boolean isNsubjPassNegFeature() {
        return isNsubjPassNegFeature;
    }

    /**
     * @param isNsubjPassNegFeature the isNsubjPassNegFeature to set
     */
    public void setNsubjPassNegFeature(boolean isNsubjPassNegFeature) {
        this.isNsubjPassNegFeature = isNsubjPassNegFeature;
    }

    /**
     * @return the isNsubjPassAspectFeature
     */
    public boolean isNsubjPassAspectFeature() {
        return isNsubjPassAspectFeature;
    }

    /**
     * @param isNsubjPassAspectFeature the isNsubjPassAspectFeature to set
     */
    public void setNsubjPassAspectFeature(boolean isNsubjPassAspectFeature) {
        this.isNsubjPassAspectFeature = isNsubjPassAspectFeature;
    }

    /**
     * @return the isNsubjCopFeature
     */
    public boolean isNsubjCopFeature() {
        return isNsubjCopFeature;
    }

    /**
     * @param isNsubjCopFeature the isNsubjCopFeature to set
     */
    public void setNsubjCopFeature(boolean isNsubjCopFeature) {
        this.isNsubjCopFeature = isNsubjCopFeature;
    }

    /**
     * @return the isNsubjCopPosFeature
     */
    public boolean isNsubjCopPosFeature() {
        return isNsubjCopPosFeature;
    }

    /**
     * @param isNsubjCopPosFeature the isNsubjCopPosFeature to set
     */
    public void setNsubjCopPosFeature(boolean isNsubjCopPosFeature) {
        this.isNsubjCopPosFeature = isNsubjCopPosFeature;
    }

    /**
     * @return the isNsubjCopNegFeature
     */
    public boolean isNsubjCopNegFeature() {
        return isNsubjCopNegFeature;
    }

    /**
     * @param isNsubjCopNegFeature the isNsubjCopNegFeature to set
     */
    public void setNsubjCopNegFeature(boolean isNsubjCopNegFeature) {
        this.isNsubjCopNegFeature = isNsubjCopNegFeature;
    }

    /**
     * @return the isNsubjCopAspectFeature
     */
    public boolean isNsubjCopAspectFeature() {
        return isNsubjCopAspectFeature;
    }

    /**
     * @param isNsubjCopAspectFeature the isNsubjCopAspectFeature to set
     */
    public void setNsubjCopAspectFeature(boolean isNsubjCopAspectFeature) {
        this.isNsubjCopAspectFeature = isNsubjCopAspectFeature;
    }

    /**
     * @return the isAmodFeature
     */
    public boolean isAmodFeature() {
        return isAmodFeature;
    }

    /**
     * @param isAmodFeature the isAmodFeature to set
     */
    public void setAmodFeature(boolean isAmodFeature) {
        this.isAmodFeature = isAmodFeature;
    }

    /**
     * @return the isAmodPosFeature
     */
    public boolean isAmodPosFeature() {
        return isAmodPosFeature;
    }

    /**
     * @param isAmodPosFeature the isAmodPosFeature to set
     */
    public void setAmodPosFeature(boolean isAmodPosFeature) {
        this.isAmodPosFeature = isAmodPosFeature;
    }

    /**
     * @return the isAmodNegFeature
     */
    public boolean isAmodNegFeature() {
        return isAmodNegFeature;
    }

    /**
     * @param isAmodNegFeature the isAmodNegFeature to set
     */
    public void setAmodNegFeature(boolean isAmodNegFeature) {
        this.isAmodNegFeature = isAmodNegFeature;
    }

    /**
     * @return the isAmodAspectFeature
     */
    public boolean isAmodAspectFeature() {
        return isAmodAspectFeature;
    }

    /**
     * @param isAmodAspectFeature the isAmodAspectFeature to set
     */
    public void setAmodAspectFeature(boolean isAmodAspectFeature) {
        this.isAmodAspectFeature = isAmodAspectFeature;
    }

    /**
     * @return the isAdvmodFeature
     */
    public boolean isAdvmodFeature() {
        return isAdvmodFeature;
    }

    /**
     * @param isAdvmodFeature the isAdvmodFeature to set
     */
    public void setAdvmodFeature(boolean isAdvmodFeature) {
        this.isAdvmodFeature = isAdvmodFeature;
    }

    /**
     * @return the isAdvmodIntsfFeature
     */
    public boolean isAdvmodIntsfFeature() {
        return isAdvmodIntsfFeature;
    }

    /**
     * @param isAdvmodIntsfFeature the isAdvmodIntsfFeature to set
     */
    public void setAdvmodIntsfFeature(boolean isAdvmodIntsfFeature) {
        this.isAdvmodIntsfFeature = isAdvmodIntsfFeature;
    }

    /**
     * @return the isDobjPosFeature
     */
    public boolean isDobjPosFeature() {
        return isDobjPosFeature;
    }

    /**
     * @param isDobjPosFeature the isDobjPosFeature to set
     */
    public void setDobjPosFeature(boolean isDobjPosFeature) {
        this.isDobjPosFeature = isDobjPosFeature;
    }

    /**
     * @return the isDobjNegFeature
     */
    public boolean isDobjNegFeature() {
        return isDobjNegFeature;
    }

    /**
     * @param isDobjNegFeature the isDobjNegFeature to set
     */
    public void setDobjNegFeature(boolean isDobjNegFeature) {
        this.isDobjNegFeature = isDobjNegFeature;
    }

    /**
     * @return the isNeedFeature
     */
    public boolean isNeedFeature() {
        return isNeedFeature;
    }

    /**
     * @param isNeedFeature the isNeedFeature to set
     */
    public void setNeedFeature(boolean isNeedFeature) {
        this.isNeedFeature = isNeedFeature;
    }

    /**
     * @return the isNeedNegFeature
     */
    public boolean isNeedNegFeature() {
        return isNeedNegFeature;
    }

    /**
     * @param isNeedNegFeature the isNeedNegFeature to set
     */
    public void setNeedNegFeature(boolean isNeedNegFeature) {
        this.isNeedNegFeature = isNeedNegFeature;
    }

    /**
     * @return the isNeedPosFeature
     */
    public boolean isNeedPosFeature() {
        return isNeedPosFeature;
    }

    /**
     * @param isNeedPosFeature the isNeedPosFeature to set
     */
    public void setNeedPosFeature(boolean isNeedPosFeature) {
        this.isNeedPosFeature = isNeedPosFeature;
    }

    /**
     * @return the isIntsfFeature
     */
    public boolean isIntsfFeature() {
        return isIntsfFeature;
    }

    /**
     * @param isIntsfFeature the isIntsfFeature to set
     */
    public void setIntsfFeature(boolean isIntsfFeature) {
        this.isIntsfFeature = isIntsfFeature;
    }

    /**
     * @return the isIntsfNPIFeature
     */
    public boolean isIntsfNPIFeature() {
        return isIntsfNPIFeature;
    }

    /**
     * @param isIntsfNPIFeature the isIntsfNPIFeature to set
     */
    public void setIntsfNPIFeature(boolean isIntsfNPIFeature) {
        this.isIntsfNPIFeature = isIntsfNPIFeature;
    }

    /**
     * @return the isIntsfPPIFeature
     */
    public boolean isIntsfPPIFeature() {
        return isIntsfPPIFeature;
    }

    /**
     * @param isIntsfPPIFeature the isIntsfPPIFeature to set
     */
    public void setIntsfPPIFeature(boolean isIntsfPPIFeature) {
        this.isIntsfPPIFeature = isIntsfPPIFeature;
    }

    /**
     * @return the isIntsfPosFeature
     */
    public boolean isIntsfPosFeature() {
        return isIntsfPosFeature;
    }

    /**
     * @param isIntsfPosFeature the isIntsfPosFeature to set
     */
    public void setIntsfPosFeature(boolean isIntsfPosFeature) {
        this.isIntsfPosFeature = isIntsfPosFeature;
    }

    /**
     * @return the isIntsfNegFeature
     */
    public boolean isIntsfNegFeature() {
        return isIntsfNegFeature;
    }

    /**
     * @param isIntsfNegFeature the isIntsfNegFeature to set
     */
    public void setIntsfNegFeature(boolean isIntsfNegFeature) {
        this.isIntsfNegFeature = isIntsfNegFeature;
    }

    /**
     * @return the isIntsfUcPosFeature
     */
    public boolean isIntsfUcPosFeature() {
        return isIntsfUcPosFeature;
    }

    /**
     * @param isIntsfUcPosFeature the isIntsfUcPosFeature to set
     */
    public void setIntsfUcPosFeature(boolean isIntsfUcPosFeature) {
        this.isIntsfUcPosFeature = isIntsfUcPosFeature;
    }

    /**
     * @return the isIntsfUcNegFeature
     */
    public boolean isIntsfUcNegFeature() {
        return isIntsfUcNegFeature;
    }

    /**
     * @param isIntsfUcNegFeature the isIntsfUcNegFeature to set
     */
    public void setIntsfUcNegFeature(boolean isIntsfUcNegFeature) {
        this.isIntsfUcNegFeature = isIntsfUcNegFeature;
    }

    /**
     * @return the isIncrPosFeature
     */
    public boolean isIncrPosFeature() {
        return isIncrPosFeature;
    }

    /**
     * @param isIncrPosFeature the isIncrPosFeature to set
     */
    public void setIncrPosFeature(boolean isIncrPosFeature) {
        this.isIncrPosFeature = isIncrPosFeature;
    }

    /**
     * @return the isIncrNegFeature
     */
    public boolean isIncrNegFeature() {
        return isIncrNegFeature;
    }

    /**
     * @param isIncrNegFeature the isIncrNegFeature to set
     */
    public void setIncrNegFeature(boolean isIncrNegFeature) {
        this.isIncrNegFeature = isIncrNegFeature;
    }

    //
    //	/**
    //	 * @return the isIncrPreFeature
    //	 */
    //	public boolean isIncrPreFeature() {
    //		return isIncrPreFeature;
    //	}
    //
    //	/**
    //	 * @param isIncrPreFeature the isIncrPreFeature to set
    //	 */
    //	public void setIncrPreFeature(boolean isIncrPreFeature) {
    //		this.isIncrPreFeature = isIncrPreFeature;
    //	}
    //
    //	/**
    //	 * @return the isDecrPreFeature
    //	 */
    //	public boolean isDecrPreFeature() {
    //		return isDecrPreFeature;
    //	}
    //
    //	/**
    //	 * @param isDecrPreFeature the isDecrPreFeature to set
    //	 */
    //	public void setDecrPreFeature(boolean isDecrPreFeature) {
    //		this.isDecrPreFeature = isDecrPreFeature;
    //	}

    /**
     * @return the isShftFeature
     */
    public boolean isShiftFeature() {
        return isShiftFeature;
    }

    /**
     * @param isShftFeature the isShftFeature to set
     */
    public void setShiftFeature(boolean isShftFeature) {
        this.isShiftFeature = isShftFeature;
    }

    /**
     * @return the isShftNegFeature
     */
    public boolean isShiftNegFeature() {
        return isShiftNegFeature;
    }

    /**
     * @param isShftNegFeature the isShftNegFeature to set
     */
    public void setShiftNegFeature(boolean isShftNegFeature) {
        this.isShiftNegFeature = isShftNegFeature;
    }

    /**
     * @return the isShftPosFeature
     */
    public boolean isShiftPosFeature() {
        return isShiftPosFeature;
    }

    /**
     * @param isShftPosFeature the isShftPosFeature to set
     */
    public void setShiftPosFeature(boolean isShftPosFeature) {
        this.isShiftPosFeature = isShftPosFeature;
    }

    /**
     * @return the isIncrPostFeature
     */
    //	public boolean isIncrPostFeature() {
    //		return isIncrPostFeature;
    //	}

    /**
     * @param isIncrPostFeature the isIncrPostFeature to set
     */
    //	public void setIncrPostFeature(boolean isIncrPostFeature) {
    //		this.isIncrPostFeature = isIncrPostFeature;
    //	}
    //
    //	/**
    //	 * @return the isDecrPostFeature
    //	 */
    //	public boolean isDecrPostFeature() {
    //		return isDecrPostFeature;
    //	}
    //
    //	/**
    //	 * @param isDecrPostFeature the isDecrPostFeature to set
    //	 */
    //	public void setDecrPostFeature(boolean isDecrPostFeature) {
    //		this.isDecrPostFeature = isDecrPostFeature;
    //	}

    /**
     * @return the isUnconNegFeature
     */
    public boolean isUnconNegFeature() {
        return isUnconNegFeature;
    }

    /**
     * @param isUnconNegFeature the isUnconNegFeature to set
     */
    public void setUnconNegFeature(boolean isUnconNegFeature) {
        this.isUnconNegFeature = isUnconNegFeature;
    }

    /**
     * @return the isUnconPosFeature
     */
    public boolean isUnconPosFeature() {
        return isUnconPosFeature;
    }

    /**
     * @param isUnconPosFeature the isUnconPosFeature to set
     */
    public void setUnconPosFeature(boolean isUnconPosFeature) {
        this.isUnconPosFeature = isUnconPosFeature;
    }

    //	/**
    //	 * @return the isPreIncrFeature
    //	 */
    //	public boolean isPreIncrFeature() {
    //		return isPreIncrFeature;
    //	}
    //
    //	/**
    //	 * @param isPreIncrFeature the isPreIncrFeature to set
    //	 */
    //	public void setPreIncrFeature(boolean isPreIncrFeature) {
    //		this.isPreIncrFeature = isPreIncrFeature;
    //	}

    /**
     * @return the isPreIncrPPIFeature
     */
    public boolean isPreIncrPPIFeature() {
        return isPreIncrPPIFeature;
    }

    /**
     * @param isPreIncrPPIFeature the isPreIncrPPIFeature to set
     */
    public void setPreIncrPPIFeature(boolean isPreIncrPPIFeature) {
        this.isPreIncrPPIFeature = isPreIncrPPIFeature;
    }

    /**
     * @return the isPreIncrNPIFeature
     */
    public boolean isPreIncrNPIFeature() {
        return isPreIncrNPIFeature;
    }

    /**
     * @param isPreIncrNPIFeature the isPreIncrNPIFeature to set
     */
    public void setPreIncrNPIFeature(boolean isPreIncrNPIFeature) {
        this.isPreIncrNPIFeature = isPreIncrNPIFeature;
    }

    //	/**
    //	 * @return the isPreDecrFeature
    //	 */
    //	public boolean isPreDecrFeature() {
    //		return isPreDecrFeature;
    //	}

    //	/**
    //	 * @param isPreDecrFeature the isPreDecrFeature to set
    //	 */
    //	public void setPreDecrFeature(boolean isPreDecrFeature) {
    //		this.isPreDecrFeature = isPreDecrFeature;
    //	}

    /**
     * @return the isPreDecrPPIFeature
     */
    public boolean isPreDecrPPIFeature() {
        return isPreDecrPPIFeature;
    }

    /**
     * @param isPreDecrPPIFeature the isPreDecrPPIFeature to set
     */
    public void setPreDecrPPIFeature(boolean isPreDecrPPIFeature) {
        this.isPreDecrPPIFeature = isPreDecrPPIFeature;
    }

    /**
     * @return the isPreDecrNPIFeature
     */
    public boolean isPreDecrNPIFeature() {
        return isPreDecrNPIFeature;
    }

    /**
     * @param isPreDecrNPIFeature the isPreDecrNPIFeature to set
     */
    public void setPreDecrNPIFeature(boolean isPreDecrNPIFeature) {
        this.isPreDecrNPIFeature = isPreDecrNPIFeature;
    }

    //	/**
    //	 * @return the isPostIncrFeature
    //	 */
    //	public boolean isPostIncrFeature() {
    //		return isPostIncrFeature;
    //	}

    //	/**
    //	 * @param isPostIncrFeature the isPostIncrFeature to set
    //	 */
    //	public void setPostIncrFeature(boolean isPostIncrFeature) {
    //		this.isPostIncrFeature = isPostIncrFeature;
    //	}

    /**
     * @return the isPostIncrPPIFeature
     */
    public boolean isPostIncrPPIFeature() {
        return isPostIncrPPIFeature;
    }

    /**
     * @param isPostIncrPPIFeature the isPostIncrPPIFeature to set
     */
    public void setPostIncrPPIFeature(boolean isPostIncrPPIFeature) {
        this.isPostIncrPPIFeature = isPostIncrPPIFeature;
    }

    /**
     * @return the isPostIncrNPIFeature
     */
    public boolean isPostIncrNPIFeature() {
        return isPostIncrNPIFeature;
    }

    /**
     * @param isPostIncrNPIFeature the isPostIncrNPIFeature to set
     */
    public void setPostIncrNPIFeature(boolean isPostIncrNPIFeature) {
        this.isPostIncrNPIFeature = isPostIncrNPIFeature;
    }

    //	/**
    //	 * @return the isPostDecrFeature
    //	 */
    //	public boolean isPostDecrFeature() {
    //		return isPostDecrFeature;
    //	}

    //	/**
    //	 * @param isPostDecrFeature the isPostDecrFeature to set
    //	 */
    //	public void setPostDecrFeature(boolean isPostDecrFeature) {
    //		this.isPostDecrFeature = isPostDecrFeature;
    //	}

    /**
     * @return the isPostDecrPPIFeature
     */
    public boolean isPostDecrPPIFeature() {
        return isPostDecrPPIFeature;
    }

    /**
     * @param isPostDecrPPIFeature the isPostDecrPPIFeature to set
     */
    public void setPostDecrPPIFeature(boolean isPostDecrPPIFeature) {
        this.isPostDecrPPIFeature = isPostDecrPPIFeature;
    }

    /**
     * @return the isPostDecrNPIFeature
     */
    public boolean isPostDecrNPIFeature() {
        return isPostDecrNPIFeature;
    }

    /**
     * @param isPostDecrNPIFeature the isPostDecrNPIFeature to set
     */
    public void setPostDecrNPIFeature(boolean isPostDecrNPIFeature) {
        this.isPostDecrNPIFeature = isPostDecrNPIFeature;
    }

    /**
     * @return the isPossnFeature
     */
    public boolean isPossnFeature() {
        return isPossnFeature;
    }

    /**
     * @param isPossnFeature the isPossnFeature to set
     */
    public void setPossnFeature(boolean isPossnFeature) {
        this.isPossnFeature = isPossnFeature;
    }

    /**
     * @return the isPresentPosFeature
     */
    public boolean isPresentPosFeature() {
        return isPresentPosFeature;
    }

    /**
     * @param isPresentPosFeature the isPresentPosFeature to set
     */
    public void setPresentPosFeature(boolean isPresentPosFeature) {
        this.isPresentPosFeature = isPresentPosFeature;
    }

    /**
     * @return the isPresentNegFeature
     */
    public boolean isPresentNegFeature() {
        return isPresentNegFeature;
    }

    /**
     * @param isPresentNegFeature the isPresentNegFeature to set
     */
    public void setPresentNegFeature(boolean isPresentNegFeature) {
        this.isPresentNegFeature = isPresentNegFeature;
    }

    /**
     * @return the isAdjFeature
     */
    public boolean isAdjFeature() {
        return isAdjFeature;
    }

    /**
     * @param isAdjFeature the isAdjFeature to set
     */
    public void setAdjFeature(boolean isAdjFeature) {
        this.isAdjFeature = isAdjFeature;
    }

    /**
     * @return the isCompAdjFeature
     */
    public boolean isCompAdjFeature() {
        return isCompAdjFeature;
    }

    /**
     * @param isCompAdjFeature the isCompAdjFeature to set
     */
    public void setCompAdjFeature(boolean isCompAdjFeature) {
        this.isCompAdjFeature = isCompAdjFeature;
    }

    /**
     * @return the isSupAdjFeature
     */
    public boolean isSupAdjFeature() {
        return isSupAdjFeature;
    }

    /**
     * @param isSupAdjFeature the isSupAdjFeature to set
     */
    public void setSupAdjFeature(boolean isSupAdjFeature) {
        this.isSupAdjFeature = isSupAdjFeature;
    }

    /**
     * @return the isAdvbFeature
     */
    public boolean isAdvbFeature() {
        return isAdvbFeature;
    }

    /**
     * @param isAdvbFeature the isAdvbFeature to set
     */
    public void setAdvbFeature(boolean isAdvbFeature) {
        this.isAdvbFeature = isAdvbFeature;
    }

    /**
     * @return the isCompAdvbFeature
     */
    public boolean isCompAdvbFeature() {
        return isCompAdvbFeature;
    }

    /**
     * @param isCompAdvbFeature the isCompAdvbFeature to set
     */
    public void setCompAdvbFeature(boolean isCompAdvbFeature) {
        this.isCompAdvbFeature = isCompAdvbFeature;
    }

    /**
     * @return the isSupAdvbFeature
     */
    public boolean isSupAdvbFeature() {
        return isSupAdvbFeature;
    }

    /**
     * @param isSupAdvbFeature the isSupAdvbFeature to set
     */
    public void setSupAdvbFeature(boolean isSupAdvbFeature) {
        this.isSupAdvbFeature = isSupAdvbFeature;
    }

    /**
     * @return the isCouldNegFeature
     */
    public boolean isCouldNegFeature() {
        return isCouldNegFeature;
    }

    /**
     * @param isCouldNegFeature the isCouldNegFeature to set
     */
    public void setCouldNegFeature(boolean isCouldNegFeature) {
        this.isCouldNegFeature = isCouldNegFeature;
    }

    /**
     * @return the isCouldPosFeature
     */
    public boolean isCouldPosFeature() {
        return isCouldPosFeature;
    }

    /**
     * @param isCouldPosFeature the isCouldPosFeature to set
     */
    public void setCouldPosFeature(boolean isCouldPosFeature) {
        this.isCouldPosFeature = isCouldPosFeature;
    }

    /**
     * @return the isShouldNegFeature
     */
    public boolean isShouldNegFeature() {
        return isShouldNegFeature;
    }

    /**
     * @param isShouldNegFeature the isShouldNegFeature to set
     */
    public void setShouldNegFeature(boolean isShouldNegFeature) {
        this.isShouldNegFeature = isShouldNegFeature;
    }

    /**
     * @return the isNegateFeature
     */
    public boolean isNegateFeature() {
        return isNegateFeature;
    }

    /**
     * @param isNegateFeature the isNegateFeature to set
     */
    public void setNegateFeature(boolean isNegateFeature) {
        this.isNegateFeature = isNegateFeature;
    }

    /**
     * @return the isNegatePosFeature
     */
    public boolean isNegatePosFeature() {
        return isNegatePosFeature;
    }

    /**
     * @param isNegatePosFeature the isNegatePosFeature to set
     */
    public void setNegatePosFeature(boolean isNegatePosFeature) {
        this.isNegatePosFeature = isNegatePosFeature;
    }

    /**
     * @return the isNegateNegFeature
     */
    public boolean isNegateNegFeature() {
        return isNegateNegFeature;
    }

    /**
     * @param isNegateNegFeature the isNegateNegFeature to set
     */
    public void setNegateNegFeature(boolean isNegateNegFeature) {
        this.isNegateNegFeature = isNegateNegFeature;
    }

    /**
     * @return the isNegateNPIFeature
     */
    public boolean isNegateNPIFeature() {
        return isNegateNPIFeature;
    }

    /**
     * @param isNegateNPIFeature the isNegateNPIFeature to set
     */
    public void setNegateNPIFeature(boolean isNegateNPIFeature) {
        this.isNegateNPIFeature = isNegateNPIFeature;
    }

    /**
     * @return the isNegatePPIFeature
     */
    public boolean isNegatePPIFeature() {
        return isNegatePPIFeature;
    }

    /**
     * @param isNegatePPIFeature the isNegatePPIFeature to set
     */
    public void setNegatePPIFeature(boolean isNegatePPIFeature) {
        this.isNegatePPIFeature = isNegatePPIFeature;
    }

    /**
     * @return the isPresentPPIFeature
     */
    public boolean isPresentPPIFeature() {
        return isPresentPPIFeature;
    }

    /**
     * @param isPresentPPIFeature the isPresentPPIFeature to set
     */
    public void setPresentPPIFeature(boolean isPresentPPIFeature) {
        this.isPresentPPIFeature = isPresentPPIFeature;
    }

    /**
     * @return the isPresentNPIFeature
     */
    public boolean isPresentNPIFeature() {
        return isPresentNPIFeature;
    }

    /**
     * @param isPresentNPIFeature the isPresentNPIFeature to set
     */
    public void setPresentNPIFeature(boolean isPresentNPIFeature) {
        this.isPresentNPIFeature = isPresentNPIFeature;
    }

    /**
     * @return the isPreIncrPosFeature
     */
    public boolean isPreIncrPosFeature() {
        return isPreIncrPosFeature;
    }

    /**
     * @param isPreIncrPosFeature the isPreIncrPosFeature to set
     */
    public void setPreIncrPosFeature(boolean isPreIncrPosFeature) {
        this.isPreIncrPosFeature = isPreIncrPosFeature;
    }

    /**
     * @return the isPreIncrNegFeature
     */
    public boolean isPreIncrNegFeature() {
        return isPreIncrNegFeature;
    }

    /**
     * @param isPreIncrNegFeature the isPreIncrNegFeature to set
     */
    public void setPreIncrNegFeature(boolean isPreIncrNegFeature) {
        this.isPreIncrNegFeature = isPreIncrNegFeature;
    }

    /**
     * @return the isPreDecrPosFeature
     */
    public boolean isPreDecrPosFeature() {
        return isPreDecrPosFeature;
    }

    /**
     * @param isPreDecrPosFeature the isPreDecrPosFeature to set
     */
    public void setPreDecrPosFeature(boolean isPreDecrPosFeature) {
        this.isPreDecrPosFeature = isPreDecrPosFeature;
    }

    /**
     * @return the isPreDecrNegFeature
     */
    public boolean isPreDecrNegFeature() {
        return isPreDecrNegFeature;
    }

    /**
     * @param isPreDecrNegFeature the isPreDecrNegFeature to set
     */
    public void setPreDecrNegFeature(boolean isPreDecrNegFeature) {
        this.isPreDecrNegFeature = isPreDecrNegFeature;
    }

    /**
     * @return the isPostIncrPosFeature
     */
    public boolean isPostIncrPosFeature() {
        return isPostIncrPosFeature;
    }

    /**
     * @param isPostIncrPosFeature the isPostIncrPosFeature to set
     */
    public void setPostIncrPosFeature(boolean isPostIncrPosFeature) {
        this.isPostIncrPosFeature = isPostIncrPosFeature;
    }

    /**
     * @return the isPostIncrNegFeature
     */
    public boolean isPostIncrNegFeature() {
        return isPostIncrNegFeature;
    }

    /**
     * @param isPostIncrNegFeature the isPostIncrNegFeature to set
     */
    public void setPostIncrNegFeature(boolean isPostIncrNegFeature) {
        this.isPostIncrNegFeature = isPostIncrNegFeature;
    }

    /**
     * @return the isPostDecrPosFeature
     */
    public boolean isPostDecrPosFeature() {
        return isPostDecrPosFeature;
    }

    /**
     * @param isPostDecrPosFeature the isPostDecrPosFeature to set
     */
    public void setPostDecrPosFeature(boolean isPostDecrPosFeature) {
        this.isPostDecrPosFeature = isPostDecrPosFeature;
    }

    /**
     * @return the isPostDecrNegFeature
     */
    public boolean isPostDecrNegFeature() {
        return isPostDecrNegFeature;
    }

    /**
     * @param isPostDecrNegFeature the isPostDecrNegFeature to set
     */
    public void setPostDecrNegFeature(boolean isPostDecrNegFeature) {
        this.isPostDecrNegFeature = isPostDecrNegFeature;
    }

    /**
     * @return the isHigherPosFeature
     */
    public boolean isHigherPosFeature() {
        return isHigherPosFeature;
    }

    /**
     * @param isHigherPosFeature the isHigherPosFeature to set
     */
    public void setHigherPosFeature(boolean isHigherPosFeature) {
        this.isHigherPosFeature = isHigherPosFeature;
    }

    /**
     * @return the isHigherNegFeature
     */
    public boolean isHigherNegFeature() {
        return isHigherNegFeature;
    }

    /**
     * @param isHigherNegFeature the isHigherNegFeature to set
     */
    public void setHigherNegFeature(boolean isHigherNegFeature) {
        this.isHigherNegFeature = isHigherNegFeature;
    }

    /**
     * @return the isAbusePresentFeature
     */
    public boolean isAbusePresentFeature() {
        return isAbusePresentFeature;
    }

    /**
     * @param isAbusePresentFeature the isAbusePresentFeature to set
     */
    public void setAbusePresentFeature(boolean isAbusePresentFeature) {
        this.isAbusePresentFeature = isAbusePresentFeature;
    }

    /**
     * @return the isAdvocatePresentFeature
     */
    public boolean isAdvocatePresentFeature() {
        return isAdvocatePresentFeature;
    }

    /**
     * @param isAdvocatePresentFeature the isAdvocatePresentFeature to set
     */
    public void setAdvocatePresentFeature(boolean isAdvocatePresentFeature) {
        this.isAdvocatePresentFeature = isAdvocatePresentFeature;
    }

    /**
     * @return the isSuggestionPresentFeature
     */
    public boolean isSuggestionPresentFeature() {
        return isSuggestionPresentFeature;
    }

    /**
     * @param isSuggestionPresentFeature the isSuggestionPresentFeature to set
     */
    public void setSuggestionPresentFeature(boolean isSuggestionPresentFeature) {
        this.isSuggestionPresentFeature = isSuggestionPresentFeature;
    }

    /**
     * @return the isStartsWithVBbaseFeature
     */
    public boolean isStartsWithVBbaseFeature() {
        return isStartsWithVBbaseFeature;
    }

    /**
     * @param isStartsWithVBbaseFeature the isStartsWithVBbaseFeature to set
     */
    public void setStartsWithVBbaseFeature(boolean isStartsWithVBbaseFeature) {
        this.isStartsWithVBbaseFeature = isStartsWithVBbaseFeature;
    }

    /**
     * @return the isPosEmoticonPresentFeature
     */
    public boolean isPosEmoticonPresentFeature() {
        return isPosEmoticonPresentFeature;
    }

    /**
     * @param isPosEmoticonPresentFeature the isPosEmoticonPresentFeature to set
     */
    public void setPosEmoticonPresentFeature(boolean isPosEmoticonPresentFeature) {
        this.isPosEmoticonPresentFeature = isPosEmoticonPresentFeature;
    }

    /**
     * @return the isNegEmoticonPresentFeature
     */
    public boolean isNegEmoticonPresentFeature() {
        return isNegEmoticonPresentFeature;
    }

    /**
     * @param isNegEmoticonPresentFeature the isNegEmoticonPresentFeature to set
     */
    public void setNegEmoticonPresentFeature(boolean isNegEmoticonPresentFeature) {
        this.isNegEmoticonPresentFeature = isNegEmoticonPresentFeature;
    }

    /**
     * @return the isCapitalizedPresentFeature
     */
    public boolean isCapitalizedPresentFeature() {
        return isCapitalizedPresentFeature;
    }

    /**
     * @param isCapitalizedPresentFeature the isCapitalizedPresentFeature to set
     */
    public void setCapitalizedPresentFeature(boolean isCapitalizedPresentFeature) {
        this.isCapitalizedPresentFeature = isCapitalizedPresentFeature;
    }

    /**
     * @return the isPosIdiomPresentFeature
     */
    public boolean isPosIdiomPresentFeature() {
        return isPosIdiomPresentFeature;
    }

    /**
     * @param isPosIdiomPresentFeature the isPosIdiomPresentFeature to set
     */
    public void setPosIdiomPresentFeature(boolean isPosIdiomPresentFeature) {
        this.isPosIdiomPresentFeature = isPosIdiomPresentFeature;
    }

    /**
     * @return the isNegIdiomPresentFeature
     */
    public boolean isNegIdiomPresentFeature() {
        return isNegIdiomPresentFeature;
    }

    /**
     * @param isNegIdiomPresentFeature the isNegIdiomPresentFeature to set
     */
    public void setNegIdiomPresentFeature(boolean isNegIdiomPresentFeature) {
        this.isNegIdiomPresentFeature = isNegIdiomPresentFeature;
    }

    /**
     * @return the isPosPhrasalVerbPresentFeature
     */
    public boolean isPosPhrasalVerbPresentFeature() {
        return isPosPhrasalVerbPresentFeature;
    }

    /**
     * @param isPosPhrasalVerbPresentFeature the isPosPhrasalVerbPresentFeature to set
     */
    public void setPosPhrasalVerbPresentFeature(boolean isPosPhrasalVerbPresentFeature) {
        this.isPosPhrasalVerbPresentFeature = isPosPhrasalVerbPresentFeature;
    }

    /**
     * @return the isNegPhrasalVerbPresentFeature
     */
    public boolean isNegPhrasalVerbPresentFeature() {
        return isNegPhrasalVerbPresentFeature;
    }

    /**
     * @param isNegPhrasalVerbPresentFeature the isNegPhrasalVerbPresentFeature to set
     */
    public void setNegPhrasalVerbPresentFeature(boolean isNegPhrasalVerbPresentFeature) {
        this.isNegPhrasalVerbPresentFeature = isNegPhrasalVerbPresentFeature;
    }

    /**
     * @return the isConjunctionPresentFeature
     */
    public boolean isConjunctionPresentFeature() {
        return isCCPresentFeature;
    }

    /**
     * @param isConjunctionPresentFeature the isConjunctionPresentFeature to set
     */
    public void setCCPresentFeature(boolean isConjunctionPresentFeature) {
        this.isCCPresentFeature = isConjunctionPresentFeature;
    }

    /**
     * @return the isShiftedConjunctionPresentFeature
     */
    public boolean isShiftedCCPresentFeature() {
        return isShiftedCCPresentFeature;
    }

    /**
     * @param isShiftedConjunctionPresentFeature the isShiftedConjunctionPresentFeature to set
     */
    public void setShiftedCCPresentFeature(boolean isShiftedConjunctionPresentFeature) {
        this.isShiftedCCPresentFeature = isShiftedConjunctionPresentFeature;
    }

    //	public String toString() {
    //		return "XpFeatureSet [isNsubjFeature=" + isNsubjFeature + ", isNsubjPosFeature=" + isNsubjPosFeature + ", isNsubjNegFeature=" + isNsubjNegFeature + ", isNsubjAspectFeature=" + isNsubjAspectFeature + ", isNsubjPassFeature=" + isNsubjPassFeature + ", isNsubjPassPosFeature=" + isNsubjPassPosFeature + ", isNsubjPassNegFeature=" + isNsubjPassNegFeature + ", isNsubjPassAspectFeature=" + isNsubjPassAspectFeature + ", isNsubjCopFeature=" + isNsubjCopFeature + ", isNsubjCopPosFeature=" + isNsubjCopPosFeature + ", isNsubjCopNegFeature=" + isNsubjCopNegFeature + ", isNsubjCopAspectFeature=" + isNsubjCopAspectFeature + ", isAmodFeature=" + isAmodFeature + ", isAmodPosFeature=" + isAmodPosFeature + ", isAmodNegFeature=" + isAmodNegFeature + ", isAmodAspectFeature=" + isAmodAspectFeature + ", isAdvmodFeature=" + isAdvmodFeature + ", isAdvmodIntsfFeature=" + isAdvmodIntsfFeature + ", isDobjPosFeature=" + isDobjPosFeature + ", isDobjNegFeature=" + isDobjNegFeature + ", isNegateFeature=" + isNegateFeature + ", isNegatePosFeature=" + isNegatePosFeature + ", isNegateNegFeature=" + isNegateNegFeature + ", isNegateNPIFeature=" + isNegateNPIFeature + ", isNegatePPIFeature=" + isNegatePPIFeature + ", isNeedFeature=" + isNeedFeature + ", isNeedNegFeature=" + isNeedNegFeature + ", isNeedPosFeature=" + isNeedPosFeature + ", isCouldNegFeature=" + isCouldNegFeature + ", isCouldPosFeature=" + isCouldPosFeature + ", isShouldNegFeature=" + isShouldNegFeature + ", isIntsfFeature=" + isIntsfFeature + ", isIntsfNPIFeature=" + isIntsfNPIFeature + ", isIntsfPPIFeature=" + isIntsfPPIFeature + ", isIntsfPosFeature=" + isIntsfPosFeature + ", isIntsfNegFeature=" + isIntsfNegFeature + ", isIntsfUcPosFeature=" + isIntsfUcPosFeature + ", isIntsfUcNegFeature=" + isIntsfUcNegFeature + ", isIncrPosFeature=" + isIncrPosFeature + ", isIncrNegFeature=" + isIncrNegFeature + ", isIncrPreFeature=" + isIncrPreFeature + ", isDecrPreFeature=" + isDecrPreFeature + ", isShftFeature=" + isShftFeature + ", isShftNegFeature=" + isShftNegFeature + ", isShftPosFeature=" + isShftPosFeature + ", isIncrPostFeature=" + isIncrPostFeature + ", isDecrPostFeature=" + isDecrPostFeature + ", isUnconNegFeature=" + isUnconNegFeature + ", isUnconPosFeature=" + isUnconPosFeature + ", isPreIncrFeature=" + isPreIncrFeature + ", isPreIncrPPIFeature=" + isPreIncrPPIFeature + ", isPreIncrNPIFeature=" + isPreIncrNPIFeature + ", isPreIncrPosFeature=" + isPreIncrPosFeature + ", isPreIncrNegFeature=" + isPreIncrNegFeature + ", isPreDecrFeature=" + isPreDecrFeature + ", isPreDecrPPIFeature=" + isPreDecrPPIFeature + ", isPreDecrNPIFeature=" + isPreDecrNPIFeature + ", isPreDecrPosFeature=" + isPreDecrPosFeature + ", isPreDecrNegFeature=" + isPreDecrNegFeature + ", isPostIncrFeature=" + isPostIncrFeature + ", isPostIncrPPIFeature=" + isPostIncrPPIFeature + ", isPostIncrNPIFeature=" + isPostIncrNPIFeature + ", isPostIncrPosFeature=" + isPostIncrPosFeature + ", isPostIncrNegFeature=" + isPostIncrNegFeature + ", isPostDecrFeature=" + isPostDecrFeature + ", isPostDecrPPIFeature=" + isPostDecrPPIFeature + ", isPostDecrNPIFeature=" + isPostDecrNPIFeature + ", isPostDecrPosFeature=" + isPostDecrPosFeature + ", isPostDecrNegFeature=" + isPostDecrNegFeature + ", isPossnFeature=" + isPossnFeature + ", isPresentPosFeature=" + isPresentPosFeature + ", isPresentNegFeature=" + isPresentNegFeature + ", isHigherPosFeature=" + isHigherPosFeature + ", isHigherNegFeature=" + isHigherNegFeature + ", isPresentPPIFeature=" + isPresentPPIFeature + ", isPresentNPIFeature=" + isPresentNPIFeature + ", isAdjFeature=" + isAdjFeature + ", isCompAdjFeature=" + isCompAdjFeature + ", isSupAdjFeature=" + isSupAdjFeature + ", isAdvbFeature=" + isAdvbFeature + ", isCompAdvbFeature=" + isCompAdvbFeature + ", isSupAdvbFeature=" + isSupAdvbFeature + ", isPosEmoticonPresentFeature=" + isPosEmoticonPresentFeature + ", isNegEmoticonPresentFeature=" + isNegEmoticonPresentFeature + ", isAbusePresentFeature=" + isAbusePresentFeature + ", isAdvocatePresentFeature=" + isAdvocatePresentFeature + ", isSuggestionPresentFeature=" + isSuggestionPresentFeature + ", isStartsWithVBbaseFeature=" + isStartsWithVBbaseFeature + ", isCapitalizedPresentFeature=" + isCapitalizedPresentFeature + ", isDynamicConceptFeature=" + Arrays.toString(isDynamicConceptFeature) + "]";
    //	}

    /**
     * @return the countPosFeature
     */
    public int getCountPosFeature() {
        return countPosFeature;
    }

    /**
     * @param countPosFeature the countPosFeature to set
     */
    public void setCountPosFeature(int posCount) {
        this.countPosFeature = posCount;
    }

    /**
     * @return the countNegFeature
     */
    public int getCountNegFeature() {
        return this.countNegFeature;
    }

    /**
     * @param countNegFeature the countNegFeature to set
     */
    public void setCountNegFeature(int negCount) {
        this.countNegFeature = negCount;
    }

    /**
     * @return the isCCPresentFeature
     */
    public boolean isCCPresentFeature() {
        return isCCPresentFeature;
    }

    private double getCategoricalPercentage(double percentage) {
        return (int) (percentage / 10);
    }

    /**
     * @return the percentagePositiveFeature
     */
    public double getPercentagePositiveFeature() {
        return percentagePositiveFeature;
    }

    /**
     * @param percentagePositiveFeature the percentagePositiveFeature to set
     */
    public void setPercentagePositiveFeature(int posCount, int totalCount, int totalSize) {
        if (totalSize == 0) {
            this.percentagePositiveFeature = 0;
        } else {
            //			this.percentagePositiveFeature = (double) posCount / (double) totalSize * (double) 100.0;
            if (posCount == 0) {
                this.percentagePositiveFeature = 0;
            } else {
                this.percentagePositiveFeature = calculatePercentage(posCount, totalCount, totalSize);
            } //			this.percentageNegativeFeature = Math.round((double) this.percentageNegativeFeature * (double) 100.0) / 100.0;

            //			this.percentagePositiveFeature = Math.round((double) this.percentagePositiveFeature * (double) 100.0) / 100.0;
            this.percentagePositiveFeature = this.getCategoricalPercentage(this.percentagePositiveFeature);
            //			System.out.println("Categorical percentage: " + percentagePositiveFeature);

        }
    }

    private double calculatePercentage(double categoryCount, double totalCount, int totalSize) {
        return 100 * ((double) categoryCount / (double) totalCount) * Math.log(1 + (double) categoryCount / (double) totalSize);
    }

    /**
     * @return the percentageNegativeFeature
     */
    public double getPercentageNegativeFeature() {
        return percentageNegativeFeature;
    }

    /**
     * @param percentageNegativeFeature the percentageNegativeFeature to set
     */
    public void setPercentageNegativeFeature(int negCount, int totalCount, int totalSize) {
        if (totalSize == 0) {
            this.percentageNegativeFeature = 0;
        } else {
            //			this.percentageNegativeFeature = (double) negCount / (double) totalSize * (double) 100.0;
            if (negCount == 0) {
                this.percentageNegativeFeature = 0;
            } else {
                this.percentageNegativeFeature = calculatePercentage(negCount, totalCount, totalSize);
                //				this.percentageNegativeFeature = ((double) negCount / (double) totalCount) * Math.log10(10 + (double) totalSize / (double) negCount) * (double) 100.0;
                //				System.out.println("Negative percentage: " + percentageNegativeFeature + "; counts: " + negCount + ", " + totalSize);
            } //			this.percentageNegativeFeature = Math.round((double) this.percentageNegativeFeature * (double) 100.0) / 100.0;
            this.percentageNegativeFeature = this.getCategoricalPercentage(this.percentageNegativeFeature);
            //			System.out.println("Categorical percentage: " + percentageNegativeFeature);
        }
        //		System.out.println("The percentage Negative Feature: " + percentageNegativeFeature);
        //		System.out.println("NegCount: " + negCount + ">>totalCount: " + totalCount + ">>PercentageNegativeFeature: " + percentageNegativeFeature);
    }

    /**
     * @return the isMainSentenceFeature
     */
    public boolean isMainSentenceFeature() {
        return isMainSentenceFeature;
    }

    /**
     * @param isMainSentenceFeature the isMainSentenceFeature to set
     */
    public void setMainSentenceFeature(boolean isMainSentenceFeature) {
        this.isMainSentenceFeature = isMainSentenceFeature;
    }

    /**
     * @return the ruleBaseSentimentFeature
     */
    public int getRuleBasedSentimentFeature() {
        return ruleBasedSentimentFeature;
    }

    /**
     * @return the stanfordSentimentStr
     */
    public String getStanfordSentimentStr() {
        return stanfordSentimentStr;
    }

    /**
     * @param stanfordSentimentStr the stanfordSentimentStr to set
     */
    public void setStanfordSentimentStr(String stanfordSentimentStr) {
        this.stanfordSentimentStr = stanfordSentimentStr;
    }

    private int getSentimentCategory(String rcvdSentiment, int previousSentiment, boolean isStanfordSentiment) {
        int newSentiment = previousSentiment;

        //		if (rcvdSentiment.contains(SentimentLexicon.POSITIVE_SENTIMENT)) {
        //			newSentiment = 3;
        //		} else if (rcvdSentiment.contains(SentimentLexicon.NEGATIVE_SENTIMENT)) {
        //			newSentiment = 1;
        //		}
        //
        //		if (rcvdSentiment.contains("Very")) {
        //			this.isStanfordHighIntensityFeature = true;
        //		}
        if (rcvdSentiment != null) {
            switch (rcvdSentiment) {
                //			case SentimentLexicon.NEUTRAL_SENTIMENT:
                //				if (previousSentiment == -1) {
                //					newSentiment = 2;
                //				}
                //				break;
                case XpSentiment.POSITIVE_SENTIMENT:
                    newSentiment = 3;
                    if (isStanfordSentiment) {
                        this.setStanfordSentimentStr(XpSentiment.POSITIVE_SENTIMENT);
                    }
                    break;
                case XpSentiment.NEGATIVE_SENTIMENT:
                    newSentiment = 1;
                    if (isStanfordSentiment) {
                        this.setStanfordSentimentStr(XpSentiment.NEGATIVE_SENTIMENT);
                    }
                    break;
                case XpSentiment.VERY_POSITIVE_SENTIMENT:
                    newSentiment = 3;
                    if (isStanfordSentiment) {
                        this.setStanfordSentimentStr(XpSentiment.POSITIVE_SENTIMENT);
                        this.setStanfordHighIntensityFeature(true);
                    }
                    break;
                case XpSentiment.VERY_NEGATIVE_SENTIMENT:
                    newSentiment = 1;
                    if (isStanfordSentiment) {
                        this.setStanfordSentimentStr(XpSentiment.NEGATIVE_SENTIMENT);
                        this.setStanfordHighIntensityFeature(true);
                    }
                    break;
                case XpSentiment.NEUTRAL_SENTIMENT:
                    if (isStanfordSentiment) {
                        this.setStanfordSentimentStr(XpSentiment.NEUTRAL_SENTIMENT);
                    }
                    break;
                //			case SentimentLexicon.VERY_POSITIVE_SENTIMENT:
                //				if (previousSentiment == 0) {
                //					newSentiment = 2;
                //				} else {
                //					newSentiment = 4;
                //				}
                //				break;
                //			case SentimentLexicon.VERY_NEGATIVE_SENTIMENT:
                //				if (previousSentiment == 4) {
                //					newSentiment = 2;
                //				} else {
                //					newSentiment = 0;
                //				}
                //				break;
            }
        }

        //		switch (rcvdSentiment) {
        //		//			case SentimentLexicon.NEUTRAL_SENTIMENT:
        //		//				if (previousSentiment == -1) {
        //		//					newSentiment = 2;
        //		//				}
        //		//				break;
        //			case SentimentLexicon.POSITIVE_SENTIMENT:
        //				if (previousSentiment == 1) {
        //					newSentiment = 2;
        //				} else if (previousSentiment == 2) {
        //					newSentiment = 3;
        //				} else if (previousSentiment == 3) {
        //					newSentiment = 4;
        //				}
        //				break;
        //			case SentimentLexicon.NEGATIVE_SENTIMENT:
        //				if (previousSentiment == 3) {
        //					newSentiment = 2;
        //				} else if (previousSentiment == 2) {
        //					newSentiment = 1;
        //				} else if (previousSentiment == 1) {
        //					newSentiment = 0;
        //				}
        //				break;
        //		//			case SentimentLexicon.VERY_POSITIVE_SENTIMENT:
        //		//				if (previousSentiment == 0) {
        //		//					newSentiment = 2;
        //		//				} else {
        //		//					newSentiment = 4;
        //		//				}
        //		//				break;
        //		//			case SentimentLexicon.VERY_NEGATIVE_SENTIMENT:
        //		//				if (previousSentiment == 4) {
        //		//					newSentiment = 2;
        //		//				} else {
        //		//					newSentiment = 0;
        //		//				}
        //		//				break;
        //		}
        return newSentiment;
    }

    /**
     * @param ruleBaseSentimentFeature the ruleBaseSentimentFeature to set
     */
    public void setRuleBasedSentimentFeature(String sentiment) {
        this.ruleBasedSentimentFeature = this.getSentimentCategory(sentiment, this.ruleBasedSentimentFeature, false);
        //		System.out.println("RuleBased Sentiment: " + this.ruleBasedSentimentFeature);
    }

    /**
     * @return the stanfordSentimentFeature
     */
    public int getStanfordSentimentFeature() {
        return stanfordSentimentFeature;
    }

    /**
     * @param stanfordSentimentFeature the stanfordSentimentFeature to set
     */
    public void setStanfordSentimentFeature(Map<String, String> stanfordSentiMap) {
        for (Map.Entry<String, String> entry : stanfordSentiMap.entrySet()) {
            this.stanfordSentimentFeature = this.getSentimentCategory(entry.getValue(), this.stanfordSentimentFeature, true);
        }
        //		System.out.println("Stanford Sentiment: " + this.stanfordSentimentFeature);
    }

    public void setStanfordSentimentFeature(String stanfordSentiment) {
        this.stanfordSentimentFeature = this.getSentimentCategory(stanfordSentiment, this.stanfordSentimentFeature, true);
        //		System.out.println("Stanford Sentiment: " + this.stanfordSentimentFeature);
    }

    /**
     * @return the isStanfordHighIntensityFeature
     */
    public boolean isStanfordHighIntensityFeature() {
        return isStanfordHighIntensityFeature;
    }

    /**
     * @param isStanfordHighIntensityFeature the isStanfordHighIntensityFeature to set
     */
    public void setStanfordHighIntensityFeature(boolean isStanfordHighIntensityFeature) {
        this.isStanfordHighIntensityFeature = isStanfordHighIntensityFeature;
    }

    /**
     * @return the countStopWordsFeature
     */
    public int getCountStopWordsFeature() {
        return countStopWordsFeature;
    }

    /**
     * @param countStopWordsFeature the countStopWordsFeature to set
     */
    public void setCountStopWordsFeature(int countStopWordsFeature) {
        this.countStopWordsFeature = countStopWordsFeature;
    }

    /**
     * @return the isPreIncrUcPosFeature
     */
    public boolean isPreIncrUcPosFeature() {
        return isPreIncrUcPosFeature;
    }

    /**
     * @param isPreIncrUcPosFeature the isPreIncrUcPosFeature to set
     */
    public void setPreIncrUcPosFeature(boolean isPreIncrUcPosFeature) {
        this.isPreIncrUcPosFeature = isPreIncrUcPosFeature;
    }

    /**
     * @return the isPreIncrUcNegFeature
     */
    public boolean isPreIncrUcNegFeature() {
        return isPreIncrUcNegFeature;
    }

    /**
     * @param isPreIncrUcNegFeature the isPreIncrUcNegFeature to set
     */
    public void setPreIncrUcNegFeature(boolean isPreIncrUcNegFeature) {
        this.isPreIncrUcNegFeature = isPreIncrUcNegFeature;
    }

    /**
     * @return the isPreDecrUcPosFeature
     */
    public boolean isPreDecrUcPosFeature() {
        return isPreDecrUcPosFeature;
    }

    /**
     * @param isPreDecrUcPosFeature the isPreDecrUcPosFeature to set
     */
    public void setPreDecrUcPosFeature(boolean isPreDecrUcPosFeature) {
        this.isPreDecrUcPosFeature = isPreDecrUcPosFeature;
    }

    /**
     * @return the isPreDecrUcNegFeature
     */
    public boolean isPreDecrUcNegFeature() {
        return isPreDecrUcNegFeature;
    }

    /**
     * @param isPreDecrUcNegFeature the isPreDecrUcNegFeature to set
     */
    public void setPreDecrUcNegFeature(boolean isPreDecrUcNegFeature) {
        this.isPreDecrUcNegFeature = isPreDecrUcNegFeature;
    }

    /**
     * @return the isPostIncrUcPosFeature
     */
    public boolean isPostIncrUcPosFeature() {
        return isPostIncrUcPosFeature;
    }

    /**
     * @param isPostIncrUcPosFeature the isPostIncrUcPosFeature to set
     */
    public void setPostIncrUcPosFeature(boolean isPostIncrUcPosFeature) {
        this.isPostIncrUcPosFeature = isPostIncrUcPosFeature;
    }

    /**
     * @return the isPostIncrUcNegFeature
     */
    public boolean isPostIncrUcNegFeature() {
        return isPostIncrUcNegFeature;
    }

    /**
     * @param isPostIncrUcNegFeature the isPostIncrUcNegFeature to set
     */
    public void setPostIncrUcNegFeature(boolean isPostIncrUcNegFeature) {
        this.isPostIncrUcNegFeature = isPostIncrUcNegFeature;
    }

    /**
     * @return the isPostDecrUcPosFeature
     */
    public boolean isPostDecrUcPosFeature() {
        return isPostDecrUcPosFeature;
    }

    /**
     * @param isPostDecrUcPosFeature the isPostDecrUcPosFeature to set
     */
    public void setPostDecrUcPosFeature(boolean isPostDecrUcPosFeature) {
        this.isPostDecrUcPosFeature = isPostDecrUcPosFeature;
    }

    /**
     * @return the isPostDecrUcNegFeature
     */
    public boolean isPostDecrUcNegFeature() {
        return isPostDecrUcNegFeature;
    }

    /**
     * @param isPostDecrUcNegFeature the isPostDecrUcNegFeature to set
     */
    public void setPostDecrUcNegFeature(boolean isPostDecrUcNegFeature) {
        this.isPostDecrUcNegFeature = isPostDecrUcNegFeature;
    }

    //	/**
    //	 * @param isDynamicConceptFeature the isDynamicConceptFeature to set
    //	 */
    //	public void setIsDynamicConceptFeature(List<Integer> isDynamicConceptFeature) {
    //		this.isDynamicConceptFeature = isDynamicConceptFeature;
    //	}

    /**
     * @return the sentiWNSentimentFeature
     */
    public double getSentiWNSentimentFeature() {
        return sentiWNSentimentFeature;
    }

    /**
     * @param sentiWNSentimentFeature the sentiWNSentimentFeature to set
     */
    public void setSentiWNSentimentFeature(double sentiWNSentimentFeature) {
        this.sentiWNSentimentFeature = sentiWNSentimentFeature;
    }

    //	/**
    //	 * @return the unigramsFeature
    //	 */
    //	public Map<String, Boolean> getNgramsFeature() {
    //		return unigramsFeature;
    //	}

    //	/**
    //	 * @param unigramsFeature the unigramsFeature to set
    //	 */
    //	public void setNgramsFeature(String reviewStr) {
    //		reviewStr = ProcessString.cleanStopWords(reviewStr.toLowerCase());
    //		for (String unigram : ngramsList) {
    //			if (reviewStr.contains(unigram)) {
    //				this.unigramsFeature.put(unigram, true);
    //			} else {
    //				this.unigramsFeature.put(unigram, false);
    //			}
    //		}
    //	}

    /**
     * @return the isVeryNegPhrasePresentFeature
     */
    public boolean isVeryNegPhrasePresentFeature() {
        return isVeryNegPhrasePresentFeature;
    }

    /**
     * @param isVeryNegPhrasePresentFeature the isVeryNegPhrasePresentFeature to set
     */
    public void setVeryNegPhrasePresentFeature(boolean isVeryNegPhrasePresentFeature) {
        this.isVeryNegPhrasePresentFeature = isVeryNegPhrasePresentFeature;
    }

    /**
     * @return the isNegPhrasePresentFeature
     */
    public boolean isNegPhrasePresentFeature() {
        return isNegPhrasePresentFeature;
    }

    /**
     * @param isNegPhrasePresentFeature the isNegPhrasePresentFeature to set
     */
    public void setNegPhrasePresentFeature(boolean isNegPhrasePresentFeature) {
        this.isNegPhrasePresentFeature = isNegPhrasePresentFeature;
    }

    /**
     * @return the isNeutralPhrasePresentFeature
     */
    public boolean isNeutralPhrasePresentFeature() {
        return isNeutralPhrasePresentFeature;
    }

    /**
     * @param isNeutralPhrasePresentFeature the isNeutralPhrasePresentFeature to set
     */
    public void setNeutralPhrasePresentFeature(boolean isNeutralPhrasePresentFeature) {
        this.isNeutralPhrasePresentFeature = isNeutralPhrasePresentFeature;
    }

    /**
     * @return the isPosPhrasePresentFeature
     */
    public boolean isPosPhrasePresentFeature() {
        return isPosPhrasePresentFeature;
    }

    /**
     * @param isPosPhrasePresentFeature the isPosPhrasePresentFeature to set
     */
    public void setPosPhrasePresentFeature(boolean isPosPhrasePresentFeature) {
        this.isPosPhrasePresentFeature = isPosPhrasePresentFeature;
    }

    /**
     * @return the isVeryPosPhrasePresentFeature
     */
    public boolean isVeryPosPhrasePresentFeature() {
        return isVeryPosPhrasePresentFeature;
    }

    /**
     * @param isVeryPosPhrasePresentFeature the isVeryPosPhrasePresentFeature to set
     */
    public void setVeryPosPhrasePresentFeature(boolean isVeryPosPhrasePresentFeature) {
        this.isVeryPosPhrasePresentFeature = isVeryPosPhrasePresentFeature;
    }

    public void setPhraseBasedFeatures(String reviewStr) {
        reviewStr = ProcessString.cleanStopWords(Languages.EN, reviewStr).toLowerCase();
        for (Map.Entry<String, String> entry : phraseClassMap.entrySet()) {
            if (reviewStr.contains(entry.getKey())) {
                switch (entry.getValue()) {
                    case (XpSentiment.NEGATIVE_SENTIMENT):
                        this.setNegPhrasePresentFeature(true);
                        break;
                    case (XpSentiment.NEUTRAL_SENTIMENT):
                        this.setNeutralPhrasePresentFeature(true);
                        break;
                    case (XpSentiment.POSITIVE_SENTIMENT):
                        this.setPosPhrasePresentFeature(true);
                        break;
                    //					case (SentimentLexicon.VERY_POSITIVE_SENTIMENT):
                    //						this.setVeryPosPhrasePresentFeature(true);
                    //						break;
                    //					case (SentimentLexicon.VERY_NEGATIVE_SENTIMENT):
                    //						this.setVeryNegPhrasePresentFeature(true);
                    //						break;

                }
            }
        }
    }

    /**
     * @return the iConsumeIncrRsrcFeature
     */
    public boolean isiConsumeIncrRsrcFeature() {
        return iConsumeIncrRsrcFeature;
    }

    /**
     * @param iConsumeIncrRsrcFeature the iConsumeIncrRsrcFeature to set
     */
    public void setiConsumeIncrRsrcFeature(boolean iConsumeIncrRsrcFeature) {
        this.iConsumeIncrRsrcFeature = iConsumeIncrRsrcFeature;
    }

    /**
     * @return the isConsumeDecrRsrcFeature
     */
    public boolean isConsumeDecrRsrcFeature() {
        return isConsumeDecrRsrcFeature;
    }

    /**
     * @param isConsumeDecrRsrcFeature the isConsumeDecrRsrcFeature to set
     */
    public void setConsumeDecrRsrcFeature(boolean isConsumeDecrRsrcFeature) {
        this.isConsumeDecrRsrcFeature = isConsumeDecrRsrcFeature;
    }

    /**
     * @return the isGreetingsSalutationPresentFeature
     */
    public boolean isGreetingsSalutationPresentFeature() {
        return isGreetingsSalutationPresentFeature;
    }

    /**
     * @param isGreetingsSalutationPresentFeature the isGreetingsSalutationPresentFeature to set
     */
    public void setGreetingsSalutationPresentFeature(boolean isGreetingsSalutationPresentFeature) {
        this.isGreetingsSalutationPresentFeature = isGreetingsSalutationPresentFeature;
    }

    /**
     * @return the isSentimentPrimarilyStanford
     */
    public boolean isSentimentPrimarilyStanford() {
        return isSentimentPrimarilyStanford;
    }

    /**
     * @param isSentimentPrimarilyStanford the isSentimentPrimarilyStanford to set
     */
    public void setSentimentPrimarilyStanford(boolean isSentimentPrimarilyStanford) {
        this.isSentimentPrimarilyStanford = isSentimentPrimarilyStanford;
    }

    /**
     * @return the isAdvmodSentimentNullifierFeature
     */
    public boolean isAdvmodSentimentNullifierFeature() {
        return isAdvmodSentimentNullifierFeature;
    }

    /**
     * @param isAdvmodSentimentNullifierFeature the isAdvmodSentimentNullifierFeature to set
     */
    public void setAdvmodSentimentNullifierFeature(boolean isAdvmodSentimentNullifierFeature) {
        this.isAdvmodSentimentNullifierFeature = isAdvmodSentimentNullifierFeature;
    }

    /**
     * @return the isStartsWithWeakVerbFeature
     */
    public boolean isStartsWithWeakVerbFeature() {
        return isStartsWithWeakVerbFeature;
    }

    /**
     * @param isStartsWithWeakVerbFeature the isStartsWithWeakVerbFeature to set
     */
    public void setStartsWithWeakVerbFeature(boolean isStartsWithWeakVerbFeature) {
        this.isStartsWithWeakVerbFeature = isStartsWithWeakVerbFeature;
    }

    /**
     * @return the isQuestionFeature
     */
    public boolean isQuestionFeature() {
        return isQuestionFeature;
    }

    /**
     * @param isQuestionFeature the isQuestionFeature to set
     */
    public void setQuestionFeature(boolean isQuestionFeature) {
        //		System.out.println("question feature set --------------------------------------");
        this.isQuestionFeature = isQuestionFeature;
    }

    /**
     * @return the isQuestionPositiveFeature
     */
    public boolean isQuestionPositiveFeature() {
        return isQuestionPositiveFeature;
    }

    /**
     * @param isQuestionPositiveFeature the isQuestionPositiveFeature to set
     */
    public void setQuestionPositiveFeature(boolean isQuestionPositiveFeature) {
        this.isQuestionPositiveFeature = isQuestionPositiveFeature;
    }

    /**
     * @return the isQuestionNegativeFeature
     */
    public boolean isQuestionNegativeFeature() {
        return isQuestionNegativeFeature;
    }

    /**
     * @param isQuestionNegativeFeature the isQuestionNegativeFeature to set
     */
    public void setQuestionNegativeFeature(boolean isQuestionNegativeFeature) {
        this.isQuestionNegativeFeature = isQuestionNegativeFeature;
    }

    /**
     * @return the isIntentionFeature
     */
    public boolean isIntentionFeature() {
        return isIntentionFeature;
    }

    /**
     * @param isIntentionFeature the isIntentionFeature to set
     */
    public void setIntentionFeature(boolean isIntentionFeature, String intentionObject, boolean isDirect) {
        this.isIntentionFeature = isIntentionFeature;
        if (intentionObject == null && !isIntentionFeature) {
			/*if it is false and intentionObject is sent as null, then set the feature to be null as well*/
            this.intentionObject = null;
        } else {
            if (this.intentionObject == null) {
                this.intentionObject = new HashSet<String>();
            }
			/*if it is isDirect, it should directly add, else only if size==0, it should add*/
            if (isDirect || (!isDirect && this.intentionObject.size() == 0)) {
                this.intentionObject.add(intentionObject);
            }
        }
    }

    /**
     * @return the intentionObject
     */
    public Set<String> getIntentionObject() {
        return intentionObject;
    }

    /**
     * @return the xto
     */
    //	public XpSentenceResources getXpTextObject() {
    //		return xto;
    //	}

    /**
     * @param xto the xto to set
     */
    //	public void setXpTextObject(XpSentenceResources xto) {
    //		this.xto = xto;
    //	}

    /**

     /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "XpFeatureVector [isNsubjFeature=" + isNsubjFeature + ", isNsubjPosFeature=" + isNsubjPosFeature + ", isNsubjNegFeature=" + isNsubjNegFeature + ", isNsubjAspectFeature=" + isNsubjAspectFeature + ", isNsubjPassFeature=" + isNsubjPassFeature + ", isNsubjPassPosFeature=" + isNsubjPassPosFeature + ", isNsubjPassNegFeature=" + isNsubjPassNegFeature + ", isNsubjPassAspectFeature=" + isNsubjPassAspectFeature + ", isNsubjShiftPronounSubjectFeature=" + isNsubjShiftPronounSubjectFeature + ", isNsubjCopFeature=" + isNsubjCopFeature + ", isNsubjCopPosFeature=" + isNsubjCopPosFeature + ", isNsubjCopNegFeature=" + isNsubjCopNegFeature + ", isNsubjCopAspectFeature=" + isNsubjCopAspectFeature + ", isAmodFeature=" + isAmodFeature + ", isAmodPosFeature=" + isAmodPosFeature + ", isAmodNegFeature=" + isAmodNegFeature + ", isAmodAspectFeature=" + isAmodAspectFeature + ", isAdvmodFeature=" + isAdvmodFeature + ", isAdvmodIntsfFeature=" + isAdvmodIntsfFeature + ", isDobjPosFeature=" + isDobjPosFeature + ", isDobjNegFeature=" + isDobjNegFeature + ", isNegateFeature=" + isNegateFeature + ", isNegatePosFeature=" + isNegatePosFeature + ", isNegateNegFeature=" + isNegateNegFeature + ", isNegateNPIFeature=" + isNegateNPIFeature + ", isNegatePPIFeature=" + isNegatePPIFeature + ", isNeedFeature=" + isNeedFeature + ", isNeedNegFeature=" + isNeedNegFeature + ", isNeedPosFeature=" + isNeedPosFeature + ", isCouldNegFeature=" + isCouldNegFeature + ", isCouldPosFeature=" + isCouldPosFeature + ", isShouldNegFeature=" + isShouldNegFeature + ", isIntsfFeature=" + isIntsfFeature + ", isIntsfNPIFeature=" + isIntsfNPIFeature + ", isIntsfPPIFeature=" + isIntsfPPIFeature + ", isIntsfPosFeature=" + isIntsfPosFeature + ", isIntsfNegFeature=" + isIntsfNegFeature + ", isIntsfUcPosFeature=" + isIntsfUcPosFeature + ", isIntsfUcNegFeature=" + isIntsfUcNegFeature + ", isIncrPosFeature=" + isIncrPosFeature + ", isIncrNegFeature=" + isIncrNegFeature + ", isShiftFeature=" + isShiftFeature + ", isShiftNegFeature=" + isShiftNegFeature + ", isShiftPosFeature=" + isShiftPosFeature + ", isUnconNegFeature=" + isUnconNegFeature + ", isUnconPosFeature=" + isUnconPosFeature + ", isPreIncrPPIFeature=" + isPreIncrPPIFeature + ", isPreIncrNPIFeature=" + isPreIncrNPIFeature + ", isPreIncrPosFeature=" + isPreIncrPosFeature + ", isPreIncrNegFeature=" + isPreIncrNegFeature + ", isPreIncrUcPosFeature=" + isPreIncrUcPosFeature + ", isPreIncrUcNegFeature=" + isPreIncrUcNegFeature + ", iConsumeIncrRsrcFeature=" + iConsumeIncrRsrcFeature + ", isConsumeDecrRsrcFeature=" + isConsumeDecrRsrcFeature + ", isPreDecrPPIFeature=" + isPreDecrPPIFeature + ", isPreDecrNPIFeature=" + isPreDecrNPIFeature + ", isPreDecrPosFeature=" + isPreDecrPosFeature + ", isPreDecrNegFeature=" + isPreDecrNegFeature + ", isPreDecrUcPosFeature=" + isPreDecrUcPosFeature + ", isPreDecrUcNegFeature=" + isPreDecrUcNegFeature + ", isPostIncrPPIFeature=" + isPostIncrPPIFeature + ", isPostIncrNPIFeature=" + isPostIncrNPIFeature + ", isPostIncrPosFeature=" + isPostIncrPosFeature + ", isPostIncrNegFeature=" + isPostIncrNegFeature + ", isPostIncrUcPosFeature=" + isPostIncrUcPosFeature + ", isPostIncrUcNegFeature=" + isPostIncrUcNegFeature + ", isPostDecrPPIFeature=" + isPostDecrPPIFeature + ", isPostDecrNPIFeature=" + isPostDecrNPIFeature + ", isPostDecrPosFeature=" + isPostDecrPosFeature + ", isPostDecrNegFeature=" + isPostDecrNegFeature + ", isPostDecrUcPosFeature=" + isPostDecrUcPosFeature + ", isPostDecrUcNegFeature=" + isPostDecrUcNegFeature + ", isPossnFeature=" + isPossnFeature + ", isPresentPosFeature=" + isPresentPosFeature + ", isPresentNegFeature=" + isPresentNegFeature + ", isHigherPosFeature=" + isHigherPosFeature + ", isHigherNegFeature=" + isHigherNegFeature + ", isAdvmodSentimentNullifierFeature=" + isAdvmodSentimentNullifierFeature + ", countPosFeature=" + countPosFeature + ", countNegFeature=" + countNegFeature + ", countStopWordsFeature=" + countStopWordsFeature + ", percentagePositiveFeature=" + percentagePositiveFeature + ", percentageNegativeFeature=" + percentageNegativeFeature + ", isMainSentenceFeature=" + isMainSentenceFeature + ", ruleBasedSentimentFeature=" + ruleBasedSentimentFeature + ", stanfordSentimentFeature=" + stanfordSentimentFeature + ", isStanfordHighIntensityFeature=" + isStanfordHighIntensityFeature + ", stanfordSentimentStr=" + stanfordSentimentStr + ", sentiWNSentimentFeature=" + sentiWNSentimentFeature + ", isPresentPPIFeature=" + isPresentPPIFeature + ", isPresentNPIFeature=" + isPresentNPIFeature + ", isAdjFeature=" + isAdjFeature + ", isCompAdjFeature=" + isCompAdjFeature + ", isSupAdjFeature=" + isSupAdjFeature + ", isAdvbFeature=" + isAdvbFeature + ", isCompAdvbFeature=" + isCompAdvbFeature + ", isSupAdvbFeature=" + isSupAdvbFeature + ", isPosEmoticonPresentFeature=" + isPosEmoticonPresentFeature + ", isNegEmoticonPresentFeature=" + isNegEmoticonPresentFeature + ", isPosIdiomPresentFeature=" + isPosIdiomPresentFeature + ", isNegIdiomPresentFeature=" + isNegIdiomPresentFeature + ", isPosPhrasalVerbPresentFeature=" + isPosPhrasalVerbPresentFeature + ", isNegPhrasalVerbPresentFeature=" + isNegPhrasalVerbPresentFeature + ", isAbusePresentFeature=" + isAbusePresentFeature + ", isAdvocatePresentFeature=" + isAdvocatePresentFeature + ", isSuggestionPresentFeature=" + isSuggestionPresentFeature + ", isQuestionFeature=" + isQuestionFeature + ", isQuestionPositiveFeature=" + isQuestionPositiveFeature + ", isQuestionNegativeFeature=" + isQuestionNegativeFeature + ", isStartsWithVBbaseFeature=" + isStartsWithVBbaseFeature + ", isStartsWithWeakVerbFeature=" + isStartsWithWeakVerbFeature + ", isCCPresentFeature=" + isCCPresentFeature + ", isShiftedCCPresentFeature=" + isShiftedCCPresentFeature + ", isVeryNegPhrasePresentFeature=" + isVeryNegPhrasePresentFeature + ", isNegPhrasePresentFeature=" + isNegPhrasePresentFeature + ", isNeutralPhrasePresentFeature=" + isNeutralPhrasePresentFeature + ", isPosPhrasePresentFeature=" + isPosPhrasePresentFeature + ", isVeryPosPhrasePresentFeature=" + isVeryPosPhrasePresentFeature + ", isCapitalizedPresentFeature=" + isCapitalizedPresentFeature + ", isGreetingsSalutationPresentFeature=" + isGreetingsSalutationPresentFeature + ", isSentimentPrimarilyStanford=" + isSentimentPrimarilyStanford + ", isIntentionFeature=" + isIntentionFeature + ", intentionObject=" + intentionObject + "]";
    }

    public String toStringTrue() {
        //		return (this.toString().replaceAll("(.? (\\w*?)=false)", ""));
        return (this.toString().replaceAll("(,?\\s?(\\w*?)=false)", ""));
    }

    @Override
    public XpFeatureVector clone() throws CloneNotSupportedException {
        return (XpFeatureVector) super.clone();
    }
}