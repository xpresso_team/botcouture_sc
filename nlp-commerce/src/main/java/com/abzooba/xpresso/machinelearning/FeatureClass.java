/**
 *
 */
package  com.abzooba.xpresso.machinelearning;

import java.util.Arrays;

/**
 * @author Koustuv Saha
 * 29-Jul-2014 10:37:22 am
 * XpressoV2.0.1  FeatureClass
 */
public class FeatureClass {

    private Double[] X;
    private String[] y;

    public FeatureClass(Double[] featureValues, String[] label) {
        this.X = featureValues;
        this.y = label;
    }

    public Double[] getX() {
        return this.X;
    }

    public String[] getY() {
        return this.y;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "FeatureClass [X=" + Arrays.toString(X) + ", y=" + Arrays.toString(y) + "]";
    }

    public String kaggleString() {
        //		return "FeatureClass [X=" + Arrays.toString(X) + ", y=" + Arrays.toString(y) + "]";
        StringBuilder sb = new StringBuilder(String.valueOf(y[0]));
        for (int i = 0; i < X.length; i++) {
            sb.append(",");
            if (X[i] != null && X[i] != 0.0) {
                sb.append(String.valueOf(X[i]));
            }
        }

        return sb.toString();
    }
}