/**
 *
 */
package  com.abzooba.xpresso.textanalytics.wordvector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.dbcp2.BasicDataSource;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;

/**
 * @author vivek aditya
 * 5:54:24 pm
 * 17-Nov-2015
 */

public class WordVectorMemsqlApp extends WordVectorDB {
    private static BasicDataSource connectionPool = null;

    public static Connection getConnection() throws SQLException {
        return connectionPool.getConnection();
    }

    public static void init() {
		/*
		 * CREATE TABLE wordvectors ( name VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL PRIMARY KEY, vector JSON DEFAULT NULL, `all-category` VARCHAR(256) DEFAULT NULL, `all-confidence` VARCHAR(256) DEFAULT NULL );
		 * CREATE INDEX seq_index ON wordvectors (name);
		 * ALTER TABLE `wordvectors` ADD COLUMN `centroid-vector` TEXT DEFAULT  NULL;
		 */
        try {
            String url = "130.211.187.156";
            //			String url = "192.168.104.184";
            long start = System.nanoTime();
            connectionPool = new BasicDataSource();
            connectionPool.setUsername("root");
            connectionPool.setPassword("");
            connectionPool.setDriverClassName("com.mysql.jdbc.Driver");
            connectionPool.setUrl("jdbc:mysql://" + url + ":3307/word2vec");
            connectionPool.setInitialSize(50);
            System.out.println("Memsql Connected in: " + (System.nanoTime() - start) * Math.pow(10, -9) + " seconds");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        init();
        //		if (args.length == 0) {
        //			populateDB("standalone");
        //		} else {
        //			populateDB(args[0]);
        //		}
        WordVectorDB a = new WordVectorMemsqlApp();
        Languages language = Languages.EN;
        //		System.out.println(WordVector.cosSimilarity(a.getWordVectorForKey(language, "dog"), a.getWordVectorForKey(language, "cat")));
        //		System.out.println(WordVector.cosSimilarity(a.getWordVectorForKey(language, "deer"), a.getWordVectorForKey(language, "cat")));
        //		a.updateData(language, "zdog", "all-category", "ZANIMALS");
        a.updateData(language, "dog", "all-category", "ANIMALS");
        System.out.println(a.getWordVectorCategoryForKey(language, "dog"));
        //		System.out.println(a.getWordVectorCategoryForKey(language, "zdog", "vector"));
        //		System.out.println(a.getColumnObjectForKey(language, "dog", "all-category"));
        //		List<?> rs = (List<?>) executeQuery("select `all-category` from wordvectors where `all-category` is not null and name !='xpresso-aspect-categories-list' group by `all-category`;", 1);
        //		System.out.println(rs.get(1));
    }

    public void updateData(Languages language, Set<String> entityList, String columnName, String category) {
        for (String entity : entityList) {
            updateData(language, entity, columnName, category);
        }
    }

    public void updateData(Languages language, String key, String columnName, Object value) {
        key = key.replaceAll("'", "");
        String strSQL = "select count(*) from wordvectors where name = '" + key + "';";
        List<?> rs = (List<?>) executeQuery(strSQL, 1);
        if (rs.size() != 0) {
            List<?> innerList = (List<?>) rs.get(1);
            if ((Long) innerList.get(0) == 1) {
                strSQL = "UPDATE wordvectors SET `" + columnName + "` = '" + value + "' where name = '" + key + "';";
                if ((int) executeQuery(strSQL, 0) != 1) {
                    System.out.println("Update Failed " + strSQL);
                }
            } else {
                strSQL = "INSERT INTO wordvectors (name, `" + columnName + "`) values ('" + key + "','" + value.toString() + "');";
                if ((int) executeQuery(strSQL, 0) != 1) {
                    System.out.println("Insert Failed " + strSQL);
                }
            }
        }
    }

    public List<Double> getWordVectorForKey(Languages language, String key) {
        key = key.replaceAll("'", "");
        String strSQL = "select * from wordvectors where name = '" + key + "';";
        List<Double> vector = null;
        List<?> rs = (List<?>) executeQuery(strSQL, 1);

        if (rs.size() > 1) {
            vector = new ArrayList<Double>();
            List<?> innerList = (List<?>) rs.get(1);
            if (innerList.get(1) != null) {
                JSONObject jsonObj;
                try {
                    jsonObj = new JSONObject((String) innerList.get(1));
                    JSONArray jsonArr = (JSONArray) jsonObj.get("vector");
                    int len = jsonArr.length();
                    for (int i = 0; i < len; i++) {
                        vector.add(Double.parseDouble((String) jsonArr.get(i)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return vector;

    }

    public static void populateDB(String type) throws Exception {
        String popFile = "config/xpressoConfig.properties";
        InputStream is = null;
        if (type.equals("standalone"))
            is = new FileInputStream(new File(popFile));
        else
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream(popFile);
        Properties XpressoProperties = new Properties();
        XpressoProperties.load(is);
        is.close();
        String line = null;
        BufferedReader br = null;
        System.out.println("Started Reading");

        long start = 0;
        br = new BufferedReader(new FileReader(XpressoProperties.getProperty("wv.db.data", "/home/abzooba/others/db/partsaa")));
        start = System.nanoTime();
        ExecutorService executorService = Executors.newFixedThreadPool(20);
        while ((line = br.readLine()) != null) {
            Runnable worker = new MemsqlThread(line);
            executorService.execute(worker);
        }
        br.close();
        shutdownAndAwaitTermination(executorService);
        System.out.println("Time Taken for insertion " + (System.nanoTime() - start) * Math.pow(10, -9));
    }

    public static void shutdownAndAwaitTermination(ExecutorService pool) {
        pool.shutdown();
        try {
            if (!pool.awaitTermination(90, TimeUnit.SECONDS)) {
                pool.shutdownNow();
                if (!pool.awaitTermination(90, TimeUnit.SECONDS))
                    System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            pool.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    public static Object executeQuery(String strSQL, int type) {
        Object result = null;
        ArrayList<List<Object>> resultArr = null;
        Connection connection = null;
        try {
            connection = getConnection();
            if (connection != null) {
                Statement stmt = connection.createStatement();
                switch (type) {
                    case (0):
                        result = stmt.executeUpdate(strSQL);
                        break;
                    case (1):
                        resultArr = new ArrayList<List<Object>>();
                        ResultSet resultSet = stmt.executeQuery(strSQL);
                        ResultSetMetaData rsmd = resultSet.getMetaData();
                        int numCols = rsmd.getColumnCount(), nameCols = 0, i = 0;
                        while (resultSet.next()) {
                            List<Object> row = new ArrayList<Object>();
                            for (i = 1; i <= numCols; i++) {
                                row.add(resultSet.getObject(i));
                            }
                            if (nameCols == 0) {
                                List<Object> colNames = new ArrayList<Object>();
                                for (i = 1; i <= numCols; i++) {
                                    colNames.add(rsmd.getColumnLabel(i));
                                }
                                resultArr.add(colNames);
                                nameCols = 1;
                            }
                            resultArr.add(row);
                        }
                        result = resultArr;
                        break;
                    case (2):
                        result = stmt.execute(strSQL);
                        break;
                    default:
                        System.out.println("Wrong sql choice");
                        break;
                }
                connection.close();
            }
        } catch (SQLException e) {
            try {
                e.printStackTrace();
                if (connection != null)
                    connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return result;
    }

    public String getWordVectorCategoryForKey(Languages language, String key) {
        key = key.replaceAll("'", "");
        return (String) getColumnObjectForKey(language, key, "all-category");
    }

    public String getWordVectorCategoryForKey(Languages language, String key, String columnName) {
        key = key.replaceAll("'", "");
        return (String) getColumnObjectForKey(language, key, columnName);
    }

    public Object getColumnObjectForKey(Languages language, String key, String columnName) {
        Object result = null;
        key = key.replaceAll("'", "");
        String strSQL = "select * from wordvectors where name = '" + key + "';";
        try {
            List<?> rs = (List<?>) executeQuery(strSQL, 1);
            if (rs.size() != 0) {
                List<?> colNames = (List<?>) rs.get(0);
                List<?> colValues = (List<?>) rs.get(1);
                for (int i = 0; i < colNames.size(); i++) {
                    String colName = (String) colNames.get(i);
                    if (colName.equals(columnName)) {
                        result = colValues.get(i);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}