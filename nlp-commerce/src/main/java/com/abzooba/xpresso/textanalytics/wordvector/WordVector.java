package com.abzooba.xpresso.textanalytics.wordvector;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.jetty.util.ConcurrentHashSet;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.controller.XpCacheWriterThread;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.textanalytics.CoreNLPController;

public class WordVector {

	private static Map<Languages, Map<String, List<Double>>> gloveVectorMap = new ConcurrentHashMap<Languages, Map<String, List<Double>>>();
	private static Map<Languages, Map<String, String>> gloveCategoryMap = new ConcurrentHashMap<Languages, Map<String, String>>();
	private static Map<Languages, Set<String>> nullGloveHash = new ConcurrentHashMap<Languages, Set<String>>();
	private static Map<Languages, Map<String, Double>> gloveSimilarityCache = new ConcurrentHashMap<Languages, Map<String, Double>>();
	private static Map<Languages, Map<String, Double>> euclideanDistanceCache = new ConcurrentHashMap<Languages, Map<String, Double>>();

	private static final float ZERO_SIMILARITY = (float) 0.0;
	private static final double INFINITE_DISTANCE = 999999.0;

	public static enum SimilarityType {
		COSINE, EUCLIDEAN
	}

	public static void init() {
		for (Languages language : XpConfig.LANGUAGES) {
			Map<String, List<Double>> gloveVectorMapLang = new ConcurrentHashMap<String, List<Double>>();
			gloveVectorMap.put(language, gloveVectorMapLang);
			Map<String, String> gloveCategoryMapLang = new ConcurrentHashMap<String, String>();
			gloveCategoryMap.put(language, gloveCategoryMapLang);
			Set<String> nullGloveHashLang = new ConcurrentHashSet<String>();
			nullGloveHash.put(language, nullGloveHashLang);
			Map<String, Double> gloveSimilarityCacheLang = new ConcurrentHashMap<String, Double>();
			gloveSimilarityCache.put(language, gloveSimilarityCacheLang);
			Map<String, Double> euclideanDistanceCacheLang = new ConcurrentHashMap<String, Double>();
			euclideanDistanceCache.put(language, euclideanDistanceCacheLang);
		}
	}

	//	private static String getCategoryFromCache(String key) {
	//		return gloveCategoryMap.get(key);
	//	}

	//	public static Double[] getGloveCategoryWiseVector(String concept, Map<String, List<String>> seedVectorMap, List<String> seedCategories, boolean isLemma) {
	//		return getGloveCategoryWiseVector(Languages.EN, concept, seedVectorMap, seedCategories, isLemma);
	//	}

	public static Double[] getGloveCategoryWiseVector(Languages language, String concept, Map<String, List<String>> seedVectorMap, List<String> seedCategories, boolean isLemma) {
		if (!XpConfig.MONGO_WV_READ) {
			return null;
		}
		Double[] gloveVector = new Double[seedCategories.size()];

		for (int i = 0; i < seedCategories.size(); i++) {

			List<String> seedVector = seedVectorMap.get(seedCategories.get(i));
			//			System.out.println("Seed Vector of " + seedCategories.get(i) + ":\t" + seedVector.toString());

			double maxSimilarity = 0;
			for (String seedConcept : seedVector) {
				double currSimilarity = computeSimilarityBetweenWords(language, concept, seedConcept);

				if (isLemma && currSimilarity == 0) {
					currSimilarity = computeSimilarityBetweenWords(language, CoreNLPController.lemmatizeToString(language, concept), seedConcept);
				}

				if (currSimilarity > maxSimilarity) {
					maxSimilarity = currSimilarity;
				}
			}
			gloveVector[i] = maxSimilarity;

		}
		//		System.out.println(Arrays.toString(gloveVector));
		return gloveVector;
	}

	private static String getHashValue(String primaryKey, String secondaryKey) {
		String compositeKey = null;
		if (primaryKey.compareToIgnoreCase(secondaryKey) < 0) {
			compositeKey = primaryKey + "\t" + secondaryKey;
		} else {
			compositeKey = secondaryKey + "\t" + primaryKey;
		}
		return compositeKey;
	}

	//	public static List<Float> getGloveVector(String key) {
	//		List<Float> gloveVector = gloveVectorMap.get(key);
	//
	//		if (gloveVector != null) {
	//			return gloveVector;
	//		} else if (nullGloveHash.contains(key)) {
	//			return null;
	//		} else {
	//			gloveVector = GloveMongoApp.getGloveVectorForKey(key);
	//			if (gloveVector == null) {
	//				nullGloveHash.add(key);
	//			} else {
	//				//				System.out.println("GloveVector for " + key + ":\t" + gloveVector);
	//				gloveVectorMap.put(key, gloveVector);
	//			}
	//		}
	//		return gloveVector;
	//	}

	private static void addToCache(Languages language, String key, List<Double> vector) {

		//		XpCacheWriterThread<String, List<Double>> xpCT = new XpCacheWriterThread<String, List<Double>>(key, vector, gloveVectorMap, nullGloveHash);
		//		xpCT.start();
		new XpCacheWriterThread<String, List<Double>>(key, vector, gloveVectorMap.get(language), nullGloveHash.get(language));
		//		if (vector == null) {
		//			nullGloveHash.add(key);
		//		} else {
		//			gloveVectorMap.put(key, vector);
		//		}
	}

	private static void addToCategoryCache(Languages language, String key, String category) {
		new XpCacheWriterThread<String, String>(key, category, gloveCategoryMap.get(language), nullGloveHash.get(language));
	}

	public static void addVectors(List<Double> netVector, List<Double> vector) {
		if (vector != null) {
			if (netVector.size() == 0) {
				netVector.addAll(vector);
				//			netVector = new ArrayList<Double>(vector);
			} else if (vector.size() > 0 && netVector.size() == vector.size()) {
				for (int i = 0; i < netVector.size(); i++) {
					netVector.set(i, netVector.get(i) + vector.get(i));
				}
			}
		}
	}

	public static void scalarMultiplyVectors(List<Double> netVector, double scalar) {
		if (netVector != null) {
			if (netVector.size() > 0) {
				for (int i = 0; i < netVector.size(); i++) {
					netVector.set(i, netVector.get(i) * scalar);
				}
			}
		}
	}

	public static void averageVector(List<Double> vector, int numVectors) {
		if (vector != null) {

			//			int vectorSize = vector.size();
			for (int i = 0; i < vector.size(); i++) {
				vector.set(i, (double) vector.get(i) / (double) numVectors);
			}
		}
	}

	private static String getWordVectorCategorySingleWord(Languages language, String key) {
		String category = gloveCategoryMap.get(language).get(key);

		if (category != null) {
			//			System.out.println("Returning from 1");
			return category;
		} else if (nullGloveHash.get(language).contains(key)) {
			//			System.out.println("Returning from 2");
			return null;
		} else {
			//			category = WordVectorMongoApp.getWordVectorCategoryForKey(language, key);
			category = XpEngine.getWordVectorDbApp().getWordVectorCategoryForKey(language, key);
			addToCategoryCache(language, key, category);
		}
		//		System.out.println("Returning from 3");
		return category;
	}

	private static String getWordVectorCategorySingleWord(Languages language, String key, String columnName) {
		//		String category = gloveCategoryMap.get(key);

		String category = XpEngine.getWordVectorDbApp().getWordVectorCategoryForKey(language, key, columnName);
		//		String category = WordVectorDB.getWordVectorCategoryForKey(language, key, columnName);
		//		System.out.println("Returning from 3");
		return category;
	}

	private static List<Double> getWordVectorSingleWord(Languages language, String key) {
		List<Double> gloveVector = gloveVectorMap.get(language).get(key);

		if (gloveVector != null) {
			//			System.out.println("Returning from 1");
			return gloveVector;
		} else if (nullGloveHash.get(language).contains(key)) {
			//			System.out.println("Returning from 2");
			return null;
		} else {
			//			gloveVector = WordVectorMongoApp.getWordVectorForKey(language, key);
			gloveVector = XpEngine.getWordVectorDbApp().getWordVectorForKey(language, key);
			//			System.out.println("Vector for " + key + ":\t" + gloveVector);
			addToCache(language, key, gloveVector);

			//			if (gloveVector == null) {
			//				nullGloveHash.add(key);
			//			} else {
			//				//				System.out.println("GloveVector for " + key + ":\t" + gloveVector);
			//				gloveVectorMap.put(key, gloveVector);
			//			}
		}
		//		System.out.println("Returning from 3");
		return gloveVector;
	}

	public static String getWordVectorCategory(Languages language, String key) {
		key = key.toLowerCase();
		if (nullGloveHash.get(language).contains(key)) {
			return null;
		}

		//		String netGloveCategory = getCategoryFromCache(key);

		String netGloveCategory = getWordVectorCategorySingleWord(language, key);
		//			if (!key.contains(" ")) {
		//			} else {
		//				String[] strArr = key.split(" ");
		//
		//				for (int i = strArr.length - 1; i >= 0; i--) {
		//					String word = strArr[i];
		//					String category = getGloveCategorySingleWord(word);
		//					if (category != null) {
		//						netGloveCategory = category;
		//						break;
		//					}
		//				}
		//
		//			}
		addToCategoryCache(language, key, netGloveCategory);
		//		System.out.println("Word Vector Categoty for " + key + " is " + netGloveCategory);

		return netGloveCategory;

	}

	public static String getWordVectorCategory(Languages language, String key, String keyLemma, String columnName) {
		String netGloveCategory = getWordVectorCategory(language, key, columnName);
		if (netGloveCategory == null) {
			netGloveCategory = getWordVectorCategory(language, keyLemma, columnName);
		}

		return netGloveCategory;

	}

	public static String getWordVectorCategory(Languages language, String key, String columnName) {
		key = key.toLowerCase();
		if (nullGloveHash.get(language).contains(key)) {
			return null;
		}

		//		String netGloveCategory = getCategoryFromCache(key, domainName);
		String netGloveCategory;
		if (key.contains(" ")) {
			key = key.replaceAll(" ", "_");
		}
		netGloveCategory = getWordVectorCategorySingleWord(language, key, columnName);
		//			if (!key.contains(" ")) {
		//			} else {
		//				String[] strArr = key.split(" ");
		//
		//				for (int i = strArr.length - 1; i >= 0; i--) {
		//					String word = strArr[i];
		//					String category = getGloveCategorySingleWord(word);
		//					if (category != null) {
		//						netGloveCategory = category;
		//						break;
		//					}
		//				}
		//
		//			}
		//			addToCategoryCache(key, netGloveCategory);
		//		System.out.println("Word Vector Categoty for " + key + " is " + netGloveCategory);

		return netGloveCategory;

	}

	public static List<Double> getWordVector(Languages language, String key) {
		key = key.toLowerCase();
		if (nullGloveHash.get(language).contains(key)) {
			return null;
		}

		List<Double> netGloveVector = gloveVectorMap.get(language).get(key);

		if (netGloveVector == null) {
			if (!key.contains(" ")) {
				netGloveVector = getWordVectorSingleWord(language, key);
			} else {
				String[] strArr = key.split(" ");

				//			String tryKey = key.replaceAll(" ", "_");
				//			netGloveVector = getGloveVectorSingleWord(tryKey);
				//			if (netGloveVector != null) {
				//				return netGloveVector;
				//			}

				int numVectors = 1;
				for (String word : strArr) {
					List<Double> gloveVector = getWordVectorSingleWord(language, word);
					if (gloveVector != null) {
						if (netGloveVector == null) {
							netGloveVector = new ArrayList<Double>();
						}
						addVectors(netGloveVector, gloveVector);
						numVectors++;
					}
				}
				averageVector(netGloveVector, numVectors);
			}
			addToCache(language, key, netGloveVector);
			//			addToCache(key, netGloveVector);

			//			XpCacheWriterThread<String, List<Float>> xpCT = new XpCacheWriterThread<String, List<Float>>(key, netGloveVector, gloveVectorMap);
			//			xpCT.start();
		}
		return netGloveVector;

	}

	public static double computeSimilarityBetweenWords(Languages language, String primaryKey, String secondaryKey) {
		String hashValue = getHashValue(primaryKey, secondaryKey);

		Double similarityScore = gloveSimilarityCache.get(language).get(hashValue);

		if (similarityScore == null) {
			List<Double> primaryVector = getWordVector(language, primaryKey);
			List<Double> secondaryVector = getWordVector(language, secondaryKey);

			if (primaryVector != null && secondaryVector != null) {
				similarityScore = cosSimilarity(primaryVector, secondaryVector);
			} else {
				similarityScore = 0.0D;
			}
			new XpCacheWriterThread<String, Double>(hashValue, similarityScore, gloveSimilarityCache.get(language));
			//			gloveSimilarityCache.put(hashValue, similarityScore);
		}

		return similarityScore;
	}

	public static double computeDistanceBetweenWords(Languages language, String primaryKey, String secondaryKey) {

		String hashValue = getHashValue(primaryKey, secondaryKey);
		Double distance = euclideanDistanceCache.get(language).get(hashValue);

		if (distance == null) {
			List<Double> primaryVector = getWordVector(language, primaryKey);
			List<Double> secondaryVector = getWordVector(language, secondaryKey);

			if (primaryVector != null && secondaryVector != null) {
				distance = euclideanDistance(primaryVector, secondaryVector);
			} else {
				distance = Double.valueOf(INFINITE_DISTANCE);
			}
			new XpCacheWriterThread<String, Double>(hashValue, distance, euclideanDistanceCache.get(language));
			//			gloveSimilarityCache.put(hashValue, similarityScore);
		}

		return distance.doubleValue();
	}

	public static double cosSimilarity(List<?> primaryVector, List<?> secondaryVector) {

		if (primaryVector == null || secondaryVector == null)
			return ZERO_SIMILARITY;
		else if (primaryVector.isEmpty() || secondaryVector.isEmpty() || primaryVector.size() != secondaryVector.size()) {
			return ZERO_SIMILARITY;
		}

		// System.out.println("Going for a proper computation of cos similarity");
		double product = 0.0;
		double denom_1 = 0.0;
		double denom_2 = 0.0;

		//		System.out.println("Vector Size: " + primaryVector.size());
		for (int i = 0; i < primaryVector.size(); i++) {
			//			double val1 = (double) primaryVector.get(i);
			//			double val2 = (double) secondaryVector.get(i);

			double val1 = ((Double) primaryVector.get(i)).doubleValue();
			double val2 = ((Double) secondaryVector.get(i)).doubleValue();

			product += val1 * val2;
			denom_1 += Math.pow(val1, 2);
			denom_2 += Math.pow(val2, 2);
		}
		if (denom_1 == 0.0 || denom_2 == 0.0) {
			return ZERO_SIMILARITY;
		}
		//		return (float) (product / (Math.sqrt(denom_1) * Math.sqrt(denom_2)));
		return (product / (Math.sqrt(denom_1 * denom_2)));
	}

	public static double cosSimilarity(double[] primaryVector, double[] secondaryVector) {

		if (primaryVector.length == 0 || secondaryVector.length == 0 || primaryVector.length != secondaryVector.length) {
			return ZERO_SIMILARITY;
		}
		// System.out.println("Going for a proper computation of cos similarity");
		double product = 0.0;
		double denom_1 = 0.0;
		double denom_2 = 0.0;

		//		System.out.println("Vector Size: " + primaryVector.size());
		for (int i = 0; i < primaryVector.length; i++) {
			product += primaryVector[i] * secondaryVector[i];
			denom_1 += Math.pow(primaryVector[i], 2);
			denom_2 += Math.pow(secondaryVector[i], 2);
		}
		if (denom_1 == 0.0 || denom_2 == 0.0) {
			return ZERO_SIMILARITY;
		}
		return (product / (Math.sqrt(denom_1 * denom_2)));
	}

	//	private static double euclideanDistance(List<Double> primaryVector, List<Double> secondaryVector) {
	//
	//		if (primaryVector.isEmpty() || secondaryVector.isEmpty() || primaryVector.size() != secondaryVector.size()) {
	//			return Double.valueOf(INFINITE_DISTANCE);
	//		} else if (primaryVector.size() == 0 || secondaryVector.size() == 0)
	//			return Double.valueOf(INFINITE_DISTANCE);
	//		// System.out.println("Going for a proper computation of cos similarity");
	//		double sum = 0.0;
	//		double diff = 0.0;
	//
	//		//		System.out.println("Vector Size: " + primaryVector.size());
	//		for (int i = 0; i < primaryVector.size(); i++) {
	//			diff = primaryVector.get(i).doubleValue() - secondaryVector.get(i).doubleValue();
	//			//			sum += Math.pow(diff, 2);
	//			sum += diff * diff;
	//		}
	//		return Math.sqrt(sum);
	//	}

	public static double euclideanDistance(List<?> primaryVector, List<?> secondaryVector) {

		if (primaryVector.isEmpty() || secondaryVector.isEmpty() || primaryVector.size() != secondaryVector.size()) {
			return INFINITE_DISTANCE;
		} else if (primaryVector.size() == 0 || secondaryVector.size() == 0)
			return INFINITE_DISTANCE;
		// System.out.println("Going for a proper computation of cos similarity");
		double sum = 0.0;
		double diff = 0.0;

		//		System.out.println("Vector Size: " + primaryVector.size());
		for (int i = 0; i < primaryVector.size(); i++) {
			//			diff = (double) primaryVector.get(i) - (double) secondaryVector.get(i);
			diff = ((Double) primaryVector.get(i)).doubleValue() - ((Double) secondaryVector.get(i)).doubleValue();
			//			sum += Math.pow(diff, 2);
			sum += diff * diff;
		}
		return Math.sqrt(sum);
	}

	public static double euclideanDistance(double[] primaryVector, double[] secondaryVector) {

		if (primaryVector.length == 0 || secondaryVector.length == 0 || primaryVector.length != secondaryVector.length) {
			return INFINITE_DISTANCE;
		}
		// System.out.println("Going for a proper computation of cos similarity");
		double sum = 0.0;
		double diff = 0.0;

		//		System.out.println("Vector Size: " + primaryVector.size());
		for (int i = 0; i < primaryVector.length; i++) {
			diff = primaryVector[i] - secondaryVector[i];
			sum += Math.pow(diff, 2);
		}
		return (Math.sqrt(sum));
	}

	public static void main(String[] args) {
		XpEngine.init(true);

		Languages language = Languages.EN;

		double similarityScore = computeSimilarityBetweenWords(language, "hotel", "motel");
		System.out.println(similarityScore);
		similarityScore = computeSimilarityBetweenWords(language, "price", "cost");
		System.out.println(similarityScore);
		similarityScore = computeSimilarityBetweenWords(language, "motel", "hotel");
		System.out.println(similarityScore);
		similarityScore = computeSimilarityBetweenWords(language, "vampire", "ghost");
		System.out.println(similarityScore);
		similarityScore = computeSimilarityBetweenWords(language, "pool", "staff");
		System.out.println(similarityScore);
		similarityScore = computeSimilarityBetweenWords(language, "cow", "animal");
		System.out.println(similarityScore);
		similarityScore = computeSimilarityBetweenWords(language, "car", "care");
		System.out.println(similarityScore);

	}

}
