package com.abzooba.xpresso.textanalytics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.utils.FileIO;

public class ProcessString {
	// public String stopwords[];
	// public int stopword_size;
	// static Pattern tag_pat;
	// static Pattern VB_pat;

	private static Map<Languages, Set<String>> stopWordsHashMap = new ConcurrentHashMap<Languages, Set<String>>();
	private static final Pattern COUNT_WORDS_PATTERN = Pattern.compile("\\b\\w+\\b", Pattern.UNICODE_CHARACTER_CLASS);;
	private static Map<String, String> replacePatternsMap;
	private static Pattern htTagRegexPattern = Pattern.compile("(<[/]*\\w>)");
	private static Pattern urlRegexPattern = Pattern.compile("((https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])");
	private static Pattern twitterUsernamePattern = Pattern.compile("(@[A-Za-z0-9_]{1,15})");

	public static void init() throws IOException {
		for (Languages lang : XpConfig.LANGUAGES) {
			Set<String> stopWordsHash = new HashSet<String>();
			FileIO.read_file(XpConfig.getStopWords(lang), stopWordsHash);
			stopWordsHashMap.put(lang, stopWordsHash);
		}
		replacePatternsMap = new HashMap<String, String>();
		List<String> tempList = new ArrayList<String>();
		FileIO.read_file(XpConfig.REPLACE_PATTERNS_FILE, tempList);
		for (String replaceLine : tempList) {
			String[] replaceArr = replaceLine.split("\t");
			if (replaceArr.length == 2) {
				/*(?i) means case-insensitive*/
				replacePatternsMap.put("(?i)" + replaceArr[0], replaceArr[1]);
			}
		}
	}

	public static List<String> tokenizeString(Languages language, String str) {
		List<String> tokList = new ArrayList<String>();
		StringTokenizer st;
		switch (language) {
			case SP:
				st = new StringTokenizer(str, " ,()-.;:?Â¿!Â¡Â«Â»â€œâ€�â€˜â€™'\"");
				break;
			default:
				st = new StringTokenizer(str, " ,()-.");
		}
		while (st.hasMoreTokens()) {
			tokList.add(st.nextToken());
		}
		return tokList;
	}

	public static String sentencifyList(List<String> tokenList) {
		StringBuilder sb = new StringBuilder();
		for (String str : tokenList) {
			sb.append(" ");
			sb.append(str);
		}
		return sb.toString().trim();
	}

	public static int calculateStringHashCode(String str) {
		str = str.replaceAll("[^\\w]", "");
		return str.hashCode();
	}

	public static int countStopWords(Languages language, String str) {
		StringTokenizer st = new StringTokenizer(str);
		int count = 0;
		while (st.hasMoreTokens()) {
			String currToken = st.nextToken();
			if (stopWordsHashMap.get(language).contains(currToken)) {
				count += 1;
			}
		}
		return count;
	}

	public static boolean isStopWord(Languages language, String token) {
		return stopWordsHashMap.get(language).contains(token.toLowerCase());
	}

	public static String cleanStopWords(Languages language, String str) {
		StringBuilder sb = new StringBuilder();
		List<String> tokList = tokenizeString(language, str);

		for (String token : tokList) {
			if (!stopWordsHashMap.get(language).contains(token)) {
				sb.append(token);
				sb.append(" ");
			}
		}
		return sb.toString().trim();
	}

	public static String resolveMultipleSpaces(String str) {
		String result = str;
		// change NA 02-25-2013
		result = result.replaceAll(" +", " ");
		// end
		return result;
	}

	public static String resolveApostrophe(Languages lang, String str) {
		String result = str;

		switch (lang) {
			case EN:
				// result = result.toLowerCase();
				result = result.replaceAll("\\bi\'m\\b", "i am");
				/*such that John's food = Johns food*/
				// result = result.replaceAll("'s", " is");
				result = result.replaceAll("\\bcan't\\b", "can not");
				result = result.replaceAll("\\bdidnt\\b", "did not").replaceAll("\\bcouldnt\\b", "could not").replaceAll("\\bdont\\b", "do not").replaceAll("\\bmightnt\\b", "might not").replaceAll("\\bmaynt\\b", "may not").replaceAll("\\bwasnt\\b", "was not").replaceAll("\\barent\\b", "are not").replaceAll("\\bisnt\\b", "is not");
				result = result.replaceAll("\\b've\\b", " have");
				break;
			case SP:
				break;
		}

		return result;
	}

	public static String resolveReplacePatterns(String str) {
		String result = str;
		for (Map.Entry<String, String> entry : replacePatternsMap.entrySet()) {
			Pattern pattern = Pattern.compile("\\b" + entry.getKey() + "\\b", Pattern.UNICODE_CHARACTER_CLASS);
			result = pattern.matcher(result).replaceAll(entry.getValue());
		}
		return result;
	}

	public static String removeVowels(String str) {
		String result = str;
		result = result.replaceAll("a", "").replaceAll("e", "").replaceAll("i", "").replaceAll("o", "").replaceAll("u", "").replaceAll("A", "").replaceAll("E", "").replaceAll("I", "").replaceAll("O", "").replaceAll("U", "");
		return result;
	}

	public static String toCamelCase(String s) {
		String[] parts = s.split("_");
		String camelCaseString = "";
		for (String part : parts) {
			camelCaseString = camelCaseString + toProperCase(part);
		}
		return camelCaseString;
	}

	private static String toProperCase(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}

	public static int countWords(String str) {
		int countWords = 0;
		Matcher m = COUNT_WORDS_PATTERN.matcher(str);
		while (m.find()) {
			++countWords;
		}
		return countWords;
	}

	//	public static boolean isAdj(Languages language, String posTag) {
	//		switch (language) {
	//			case SP:
	//				return posTag.startsWith("aq");
	//				// return posTag.contains("aq");
	//			default:
	//				return posTag.contains("JJ");
	//				// return posTag.startsWith("JJ")
	//		}
	//	}
	//
	//	public static boolean isAdv(Languages language, String posTag) {
	//		switch (language) {
	//			case SP:
	//				return (posTag.startsWith("rg") || posTag.startsWith("rn"));
	//			default:
	//				return posTag.contains("RB");
	//		}
	//	}
	//
	//	public static boolean isMainVerb(Languages language, String posTag) {
	//		switch (language) {
	//			case SP:
	//				return posTag.startsWith("vm");
	//			default:
	//				return posTag.contains("VB");
	//		}
	//	}

	public static String removeHypertextTags(String sentence) {
		String normalizedText = sentence;
		Matcher m = htTagRegexPattern.matcher(sentence);
		while (m.find()) {
			String tempPattern = m.group();
			normalizedText = normalizedText.replace(tempPattern, "");
		}
		return normalizedText;
	}

	public static String removeUrls(String sentence) {
		String normalizedText = sentence;
		//		System.out.println(normalizedText);
		Matcher m = urlRegexPattern.matcher(sentence);
		while (m.find()) {
			String tempPattern = m.group();
			//			System.out.println("tempPattern : " + tempPattern);
			normalizedText = normalizedText.replace(tempPattern, "");
		}
		normalizedText = normalizedText.replaceAll("(https?|ftp).*$", "");
		return normalizedText;
	}

	public static String removeTwitterUsernames(String sentence) {

		String normalizedText = sentence;
		//		System.out.println(normalizedText);
		Matcher m = twitterUsernamePattern.matcher(sentence);
		while (m.find()) {
			String tempPattern = m.group();
			//			System.out.println("tempPattern : " + tempPattern);
			normalizedText = normalizedText.replace(tempPattern, "");
		}
		return normalizedText;
	}

	public static void main(String[] args) throws Exception {
		// String propsFile = "./config/xdt.properties";
		// XpEngine.init(propsFile, true);
		// String str = "Ram is a good boy who was bad";
		// System.out.println(cleanStopWords(str));

		String str1 = "Although the food was good , the service was bad";
		String str2 = "Although the food was good, the service was bad";
		System.out.println("HashCode1: " + calculateStringHashCode(str1));
		System.out.println("HashCode2: " + calculateStringHashCode(str2));

	}
}