package com.abzooba.xpresso.textanalytics.wordvector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Level;

import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.utils.FileIO;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import org.slf4j.Logger;


public class WordVectorMongoApp extends WordVectorDB {

    private MongoClient mongoClient;
    //	private static MongoCollection<Document> mongoCollection;
    private Map<Languages, MongoCollection<Document>> mongoCollections = new HashMap<Languages, MongoCollection<Document>>();

    //	public void connect(String host, int port, String dbName, String collection) throws UnknownHostException {
    //		//		mongoClient = new MongoClient(host, port);
    //		//		//		System.out.println("DBNAME: " + dbName + "\tCollection:" + collection);
    //		//		for (Languages language : XpConfig.LANGUAGES) {
    //		//			mongoCollections.put(language, mongoClient.getDatabase(dbName).getCollection(getCollectionName(language, collection)));
    //		//		}
    //
    //		MongoClientOptions options = MongoClientOptions.builder().connectionsPerHost(600).socketTimeout(100000).build();
    //
    //		MongoClient mongoClient = new MongoClient(host + ":" + port, options);
    //		for (Languages language : XpConfig.LANGUAGES) {
    //			mongoCollections.put(language, mongoClient.getDatabase(dbName).getCollection(getCollectionName(language, collection)));
    //		}
    //
    //		//		MongoClient mongo = new MongoClient(
    //		//				  new MongoClientURI( "mongodb://abzAdmin:Abz00ba1nc@"+localhost/data" )
    //		//				);
    //
    //		//		List<ServerAddress> seeds = new ArrayList<ServerAddress>();
    //		//		seeds.add( new ServerAddress( "localhost" );
    //
    //		List<MongoCredential> credentials = new ArrayList<MongoCredential>();
    //		credentials.add(MongoCredential.createMongoCRCredential("abzAdmin", XpConfig.MONGO_DB, "Abz00ba1nc".toCharArray()));
    //
    //		MongoClient mongoClient = new MongoClient(host + ":" + port, options);
    //		for (Languages language : XpConfig.LANGUAGES) {
    //			mongoCollections.put(language, mongoClient.getDatabase(dbName).getCollection(getCollectionName(language, collection)));
    //		}
    //
    //	}

    private boolean connect() {

        //		mongoClient = new MongoClient(XpConfig.MONGO_HOST);
        //		boolean isConnectedAll = true;
        //		boolean loadLanguage = false;
        //		for (Languages language : XpConfig.LANGUAGES) {
        //			mongoCollections.put(language, mongoClient.getDatabase(XpConfig.MONGO_DB).getCollection(getCollectionName(language, XpConfig.MONGO_COLLECTION)));
        //			System.out.println("Loading for Mongo: " + language);
        //			Logger mongoLogger = Logger.getLogger("com.mongodb");
        //			mongoLogger.setLevel(Level.OFF);
        //			loadLanguage = true;
        //			isConnectedAll = isConnectedAll && (mongoCollections.get(language) != null);
        //		}
        //		return isConnectedAll && loadLanguage;

        MongoClientOptions options = MongoClientOptions.builder().connectionsPerHost(600).socketTimeout(100000).build();

        List<ServerAddress> seeds = new ArrayList<ServerAddress>();
        seeds.add(new ServerAddress(XpConfig.MONGO_HOST));
        List<MongoCredential> credentials = new ArrayList<MongoCredential>();
        //		MongoCredential credential = MongoCredential.createCredential("abzAdmin", XpConfig.MONGO_DB, "Abz00ba1nc".toCharArray());
        MongoCredential credential = MongoCredential.createCredential(XpConfig.MONGO_USERNAME, XpConfig.MONGO_DB, XpConfig.MONGO_PASSWORD.toCharArray());
        credentials.add(credential);
        mongoClient = new MongoClient(seeds, credentials, options);

        boolean isConnectedAll = true;
        boolean loadLanguage = false;
        System.out.println("DB: " + XpConfig.MONGO_DB + "\tCollection: " + XpConfig.MONGO_COLLECTION + "\tHost: " + XpConfig.MONGO_HOST);

        for (Languages language : XpConfig.LANGUAGES) {
            mongoCollections.put(language, mongoClient.getDatabase(XpConfig.MONGO_DB).getCollection(getCollectionName(language, XpConfig.MONGO_COLLECTION)));
            System.out.println("Loading for Mongo: " + language);
            loadLanguage = true;
            isConnectedAll = isConnectedAll && (mongoCollections.get(language) != null);
        }
        //		mongoClient.close();
        return isConnectedAll && loadLanguage;
    }

    public String getCollectionName(Languages language, String collection) {
        String collectionLanguage = collection;
        switch (language) {
            case EN:
                collectionLanguage = collection;
                break;
            case SP:
                collectionLanguage = collection + "_sp";
                break;
        }

        return collectionLanguage;
    }

    private void createTextIndex(String key) {
        for (Languages language : XpConfig.LANGUAGES) {
            createTextIndex(language, key);
        }
    }

    private void createTextIndex(Languages language, String key) {
        mongoCollections.get(language).createIndex(new BasicDBObject(key, 1).append("unique", true));
    }

    // public static void init(String host, String dbName, String collection,
    // boolean loadVectorFlag, String fileName) throws UnknownHostException {
    //	public void init() {
    public WordVectorMongoApp() {
        boolean isConnected = connect();

        if (isConnected) {
            if (XpConfig.MONGO_WV_LOAD) {
                //					deleteEntireDataFromMongo();
                if (XpConfig.MONGO_COLLECTION.equals("testCollection")) {
                    for (Languages language : XpConfig.LANGUAGES) {
                        loadGloveDataIntoMongo(language, XpConfig.getWordVectorFile(language));
                    }
                } //					loadGloveDataIntoMongo("input/word2vec/GoogleNews-vectors-negative300.txt");
                else {
                    loadGloveDataIntoMongo(Languages.EN, XpConfig.MONGO_FILE);
                }
            }
            if (XpConfig.MONGO_CREATE_INDEX) {
                createTextIndex("name");
            }
            System.out.println("Mongo Connected");
        }

    }

    // public static void loadGloveDataAllIntoMongo(String fileName) {
    // Stack<String> lineStack = new Stack<String>();
    // List<DBObject> objectList = new ArrayList<DBObject>();
    // FileIO.read_file(fileName, lineStack);
    // while (!lineStack.isEmpty()) {
    // String line = lineStack.pop();
    // String[] lineSplit = line.split(" ");
    // if (lineSplit.length < 2)
    // continue;
    // Vector<Float> gloveVector = new Vector<Float>();
    // for (int i = 1; i < lineSplit.length; i++)
    // gloveVector.add(Float.valueOf(lineSplit[i]));
    // BasicDBObject dbInstance = new BasicDBObject("name",
    // lineSplit[0]).append("vector", gloveVector);
    // objectList.add(dbInstance);
    // }
    // mongoCollection.insert(objectList);
    // System.out.println("All glove vectors loaded");
    // }

    public void loadGloveDataIntoMongo(Languages language, String fileName) {
        Stack<String> lineStack = new Stack<String>();
        FileIO.read_file(fileName, lineStack);
        if (lineStack.isEmpty())
            System.out.println("Empty lineStack for file " + fileName);
        while (!lineStack.isEmpty()) {
            String line = lineStack.pop();
            String[] lineSplit = line.split(" ");
            if (lineSplit.length < 2) {
                continue;
            }
            System.out.println(lineSplit[0]);
            List<Double> gloveVector = new ArrayList<Double>();
            for (int i = 1; i < lineSplit.length; i++) {
                gloveVector.add(Double.valueOf(lineSplit[i]));
            }
            Document dbInstance = new Document("name", lineSplit[0]).append("vector", gloveVector);
            mongoCollections.get(language).insertOne(dbInstance);
        }
        System.out.println("Glove Vectors loaded into Mongo");
    }

    //	public static void updateDataInMongo(Set<String> entityList, String columnName, String category) {
    //		updateDataInMongo(Languages.EN, entityList, columnName, category);
    //	}

    public void updateData(Languages language, Set<String> entityList, String columnName, String category) {
        for (String entity : entityList) {
            updateData(language, entity, columnName, category);
        }
    }

    //	public static void updateDataInMongo(String key, String columnName, Object value) {
    //		updateDataInMongo(Languages.EN, key, columnName, value);
    //	}

    public void updateData(Languages language, String key, String columnName, Object value) {
        BsonValue keyBSON = new BsonString(key);
        Bson query = new BsonDocument("name", keyBSON);
        FindIterable<Document> cursor = mongoCollections.get(language).find(query);
        if (cursor != null) {
            try {
                //				int i = 0;
                Document dbInstance = null;
                for (Document currentDoc : cursor) {
                    if (!currentDoc.containsKey(columnName)) {
                        //						System.out.println("I have entered here 1.1: " + key);
                        currentDoc.append(columnName, value);
                    } else {
                        //						System.out.println("I have entered here 1.2: " + key);
                        currentDoc.replace(columnName, value);
                    }
                    //					mongoCollections.get(language).replaceOne(query, currentDoc);
                    mongoCollections.get(language).deleteOne(query);
                    dbInstance = currentDoc;
                    break;
                    //					i++;
                }

                if (dbInstance == null) {
                    dbInstance = new Document("name", key).append(columnName, value);
                }

                //				if (i == 0) {
                //					System.out.println("I have entered here 1.2!! " + key);
                //					Document dbInstance = new Document("name", key).append(columnName, value);
                mongoCollections.get(language).insertOne(dbInstance);
                //				}

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //			System.out.println("I have entered here 2!! " + key);
            Document dbInstance = new Document("name", key).append(columnName, value);
            mongoCollections.get(language).insertOne(dbInstance);
        }
    }

    //	public static Object getFromMongo(String key, String columnName) {
    //		BsonValue keyBSON = new BsonString(key);
    //		Bson query = new BsonDocument("name", keyBSON);
    //		FindIterable<Document> cursor = mongoCollection.find(query);
    //		Object value = null;
    //		try {
    //			for (Document currentDoc : cursor) {
    //				value = currentDoc.get(columnName);
    //				if (value != null) {
    //					break;
    //				}
    //			}
    //		} catch (Exception ex) {
    //			ex.printStackTrace();
    //		}
    //		return value;
    //	}

    //	public static Object getColumnObjectForKey(String key, String columnName) {
    //		return getColumnObjectForKey(Languages.EN, key, columnName);
    //	}

    public Object getColumnObjectForKey(Languages language, String key, String columnName) {
        BsonValue value = new BsonString(key);
        Bson query = new BsonDocument("name", value);
        FindIterable<Document> cursor = mongoCollections.get(language).find(query);
        for (Document currObj : cursor) {
            return currObj.get(columnName);
        }
        return null;
    }

    //	public static void removeRowForKey(String key) {
    //		removeRowForKey(Languages.EN, key);
    //	}

    public void removeRowForKey(Languages language, String key) {
        BsonValue value = new BsonString(key);
        Bson query = new BsonDocument("name", value);
        mongoCollections.get(language).deleteOne(query);
    }

    //	public static List<String> getDistinctObjectForColumn(String columnName) {
    //		return getDistinctObjectForColumn(Languages.EN, columnName);
    //	}

    public List<String> getDistinctObjectForColumn(Languages language, String columnName) {
        DistinctIterable<String> distinctColumnNames = mongoCollections.get(language).distinct(columnName, String.class);
        List<String> columnNamesList = new ArrayList<String>();
        for (String distinctColumnName : distinctColumnNames) {
            System.out.println("DistinctColumnName: " + distinctColumnName);
            columnNamesList.add(distinctColumnName);
        }

        return columnNamesList;
    }

    //	public static List<Double> getWordVectorForKey(String key) {
    //		return getWordVectorForKey(Languages.EN, key);
    //	}

    //	public static List<Double> getWordVectorForKey(String key) {
    //		return getWordVectorForKey(Languages.EN, key);
    //	}

    @SuppressWarnings("unchecked")
    public List<Double> getWordVectorForKey(Languages language, String key) {
        // long startTime = System.currentTimeMillis();
        BsonValue value = new BsonString(key);
        Bson query = new BsonDocument("name", value);
        //		System.out.println("Query: " + query);
        //		FindIterable<Document> cursor = mongoCollection.find(query);
        FindIterable<Document> cursor = mongoCollections.get(language).find(query);
        //		MongoCursor<TDocument> cursor = mongoCollection.find(query).iterator();

        //		List<Double> gloveVector = new ArrayList<Double>();
        try {

            //			if (cursor.hasNext()) {
            for (Document totalObj : cursor) {
                //				Document totalObj = (Document) cursor.next();
                List<?> vectorList = (List<?>) totalObj.get("vector");
                return (List<Double>) vectorList;
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    //	public static List<Double> getWordVectorForKey(Languages language, String key) {
    //		// long startTime = System.currentTimeMillis();
    //		//		System.out.println("Ami method e dhukechhi!!!!");
    //		BsonValue value = new BsonString(key);
    //		Bson query = new BsonDocument("name", value);
    //		//		System.out.println("Query: " + query);
    //		//		FindIterable<Document> cursor = mongoCollection.find(query);
    //		FindIterable<Document> cursor = mongoCollections.get(language).find(query);
    //		//		MongoCursor<TDocument> cursor = mongoCollection.find(query).iterator();
    //
    //		List<Double> gloveVector = new ArrayList<Double>();
    //		try {
    //
    //			//			if (cursor.hasNext()) {
    //			for (Document totalObj : cursor) {
    //				//				Document totalObj = (Document) cursor.next();
    //				List<?> vectorList = (List<?>) totalObj.get("vector");
    //				//				System.out.println("VectorList: " + vectorList.toString());
    //				if (vectorList != null) {
    //					vectorList.stream().forEach((obj) -> {
    //						gloveVector.add((Double) obj);
    //					});
    //				}
    //				// for (Object obj : vectorList.toArray())
    //				// gloveVector.add((Float) obj);
    //			}
    //			// System.out.println("total time taken - " +
    //			// (System.currentTimeMillis() - startTime));
    //			return gloveVector;
    //		} catch (Exception ex) {
    //			ex.printStackTrace();
    //			return null;
    //		}
    //
    //	}

    //	protected static String getWordVectorCategoryForKey(String key) {
    //		return getWordVectorCategoryForKey(Languages.EN, key);
    //	}

    public String getWordVectorCategoryForKey(Languages language, String key) {
        BsonValue value = new BsonString(key);
        Bson query = new BsonDocument("name", value);
        FindIterable<Document> cursor = mongoCollections.get(language).find(query);

        String category = null;
        try {

            for (Document totalObj : cursor) {
                if (totalObj.containsKey("category")) {
                    category = totalObj.getString("category");
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return category;
    }

    //	protected static String getWordVectorCategoryForKey(String key, String columnName) {
    //		return getWordVectorCategoryForKey(Languages.EN, key, columnName);
    //	}

    public String getWordVectorCategoryForKey(Languages language, String key, String columnName) {
        BsonValue value = new BsonString(key);
        Bson query = new BsonDocument("name", value);
        FindIterable<Document> cursor = mongoCollections.get(language).find(query);
        //		System.out.println("Searching for " + key);

        String category = null;
        try {

            for (Document totalObj : cursor) {
                //				System.out.println(totalObj.toJson());
                if (totalObj.containsKey(columnName)) {
                    category = totalObj.getString(columnName);
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return category;
    }

    //	public static Set<String> getAllPhrasesWithVectors() {
    //		return getAllPhrasesWithVectors(Languages.EN);
    //	}

    public Set<String> getAllPhrasesWithVectors(Languages language) {
        Bson query = new BsonDocument();
        FindIterable<Document> cursor = mongoCollections.get(language).find(query);
        Set<String> phraseSet = new HashSet<String>();
        int counter = 0;
        try {
            for (Document singleObj : cursor) {
                phraseSet.add(singleObj.getString("name"));
                counter++;
                System.out.println("name : " + singleObj.getString("name") + "counter : " + counter);
            }
            return phraseSet;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    //	public static FindIterable<Document> getMongoDocuments() {
    //		return getMongoDocuments(Languages.EN);
    //	}

    public FindIterable<Document> getDocuments(Languages language) {
        Bson query = new BsonDocument();
        FindIterable<Document> cursor = mongoCollections.get(language).find(query);
        return cursor;
    }

    public boolean closeConnection() {
        if (mongoClient != null) {
            mongoClient.close();
            //			logger.info( "MongoDB Connection successfully closed");
            return true;
        } else {
            //			logger.info( "No MongoDB Connection to close");
            return false;
        }
    }

    public void deleteEntireDataFromMongo() {
        for (Languages language : mongoCollections.keySet()) {
            mongoCollections.get(language).drop();
        }
    }

    public void deleteEntireDataFromMongo(Languages language) {
        mongoCollections.get(language).drop();
    }

    public static void main(String[] args) throws Exception {
        // XpEngine.init("./config/xdt.properties", true);
        // GloveMongoApp.init("localhost", "mydb", "testCollection", false,
        // XpConfig.GLOVE_VECTOR_FILE);
        String propsFile = "./conf/xpressoConfig.properties";

        XpConfig.init(propsFile);
        //		XpEngine.init(propsFile, true);
        WordVectorMongoApp.init();
        WordVectorDB.init();
        WordVectorDB wvD = new WordVectorDB();

        System.out.println("Exit from init!");
        Languages language = Languages.EN;

        String str1 = "haze";
        String str2 = "pollution";
        String str3 = "car";

        //		int x = 1;
        //		String st;
        //		Scanner sc = new Scanner(System.in);
        //		while (x == 1) {
        //			st = sc.nextLine();
        //			System.out.println("Category " + st + "\t" + getColumnObjectForKey(st, "all-category"));
        //		}
        //		sc.close();
        //		System.out.println(getWordVectorForKey(str1));
        //		WordVectorMongoApp wMongo = new WordVectorMongoApp();
        System.out.println("Category for " + str1 + "\t" + WordVector.getWordVectorCategory(language, str1, "government"));
        System.out.println("Category " + str1 + "\t" + wvD.getColumnObjectForKey(language, str1, "all-category"));
        System.out.println("Category for " + str2 + "\t" + WordVector.getWordVectorCategory(language, str1, "government"));
        System.out.println("Category " + str2 + "\t" + wvD.getColumnObjectForKey(language, str1, "all-category"));
        System.out.println("Category for " + str3 + "\t" + WordVector.getWordVectorCategory(language, str1, "government"));
        System.out.println("Category " + str3 + "\t" + wvD.getColumnObjectForKey(language, str1, "all-category"));
        //		System.out.println("Category xpresso-aspect-categories-list" + "\t" + getColumnObjectForKey("xpresso-aspect-categories-list", "all-category"));
        //		System.out.println("Category " + "cricket" + "\t" + getColumnObjectForKey("cricket", "all-category"));
        //		System.out.println("Category " + "good" + "\t" + getColumnObjectForKey("good", "all-category"));
        //		getColumnObjectForKey("xpresso-aspect-categories-list", "all-category")
        //		getColumnObjectForKey("xpresso-aspect-categories-list", "all-category")
        //		getColumnObjectForKey("xpresso-aspect-categories-list", "all-category")

        //		System.exit(1);

        List<Double> vector1 = WordVector.getWordVector(language, str1);
        List<Double> vector2 = WordVector.getWordVector(language, str2);
        List<Double> vector3 = WordVector.getWordVector(language, str3);

        System.out.println("Euclidean Distance: " + WordVector.euclideanDistance(vector1, vector2));
        System.out.println("Cosine Similarity: " + WordVector.cosSimilarity(vector1, vector2));

        System.out.println("Euclidean Distance: " + WordVector.euclideanDistance(vector1, vector3));
        System.out.println("Cosine Similarity: " + WordVector.cosSimilarity(vector1, vector3));

        System.out.println("Euclidean Distance: " + WordVector.euclideanDistance(vector3, vector2));
        System.out.println("Cosine Similarity: " + WordVector.cosSimilarity(vector3, vector2));

        //		String str = "urine";
        //		WordVectorMongoApp.updateDataInMongo(str, "category", "BULLSHIT");
        //		System.out.println(str + ":\t" + WordVector.getWordVectorCategory(str));
        //
        //		str = "urine smell";
        //		System.out.println(str + ":\t" + Glove.getGloveVector(str));
        //
        //		String str1 = "breakfast";
        //		String str2 = "food";
        //
        //		System.out.println(str1 + " : " + str2 + "\t" + Glove.computeSimilarityBetweenWords(str1, str2));
        //
        //		str1 = "breakfast chef";
        //		str2 = "food";
        //		System.out.println(str1 + " : " + str2 + "\t" + Glove.computeSimilarityBetweenWords(str1, str2));
        //
        //		str1 = "chef";
        //		str2 = "staff";
        //		System.out.println(str1 + " : " + str2 + "\t" + Glove.computeSimilarityBetweenWords(str1, str2));
        //
        //		str1 = "breakfast chef";
        //		str2 = "staff";
        //		System.out.println(str1 + " : " + str2 + "\t" + Glove.computeSimilarityBetweenWords(str1, str2));
        //
        //		str1 = "breakfast";
        //		str2 = "cook";
        //		System.out.println(str1 + " : " + str2 + "\t" + Glove.computeSimilarityBetweenWords(str1, str2));
        //
        //		str1 = "chef";
        //		str2 = "cook";
        //		System.out.println(str1 + " : " + str2 + "\t" + Glove.computeSimilarityBetweenWords(str1, str2));
        //
        //		str1 = "breakfast chef";
        //		str2 = "cook";
        //		System.out.println(str1 + " : " + str2 + "\t" + Glove.computeSimilarityBetweenWords(str1, str2));
        //
        //		str1 = "health";
        //		str2 = "nutrition";
        //		System.out.println(str1 + " : " + str2 + "\t" + Glove.computeSimilarityBetweenWords(str1, str2));
        //
        //		str1 = "tip";
        //		str2 = "nutrition";
        //		System.out.println(str1 + " : " + str2 + "\t" + Glove.computeSimilarityBetweenWords(str1, str2));
        //
        //		str1 = "health tips";
        //		str2 = "nutrition";
        //		System.out.println(str1 + " : " + str2 + "\t" + Glove.computeSimilarityBetweenWords(str1, str2));
        //
        //		str1 = "executives";
        //		str2 = "executive";
        //		System.out.println(str1 + " : " + str2 + "\t" + Glove.computeSimilarityBetweenWords(str1, str2));

        //		System.out.println(str + ":\t" + GloveMongoApp.getGloveVectorForKey(str));
        //		str = "breakfast chef";
        //		System.out.println(str + ":\t" + GloveMongoApp.getGloveVectorForKey(str));
        //		str = "airport";
        //		System.out.println(str + ":\t" + GloveMongoApp.getGloveVectorForKey(str));
        //		str = "chicken";
        //		System.out.println(str + ":\t" + GloveMongoApp.getGloveVectorForKey(str));
        //		str = "chicken sandwich";
        //		System.out.println(str + ":\t" + GloveMongoApp.getGloveVectorForKey(str));
        //		str = "chicken teriyaki sandwich";
        //		System.out.println(str + ":\t" + GloveMongoApp.getGloveVectorForKey(str));

    }
}