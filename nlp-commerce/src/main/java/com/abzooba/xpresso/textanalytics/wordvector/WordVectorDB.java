/**
 * 
 */
package com.abzooba.xpresso.textanalytics.wordvector;

import java.util.List;
import java.util.Set;

import org.bson.Document;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.mongodb.client.FindIterable;

/**
 * @author vivek aditya
 * 5:16:15 pm
 * 18-Nov-2015
 */
public class WordVectorDB {

	static int databaseType;
	private static WordVectorDB[] databaseAppArr;

	//	private static WordVectorDB wordVectorDB = new WordVectorDB();

	//	/* A private Constructor prevents any other 
	//	 * class from instantiating.
	//	 */
	//	private WordVectorDB() {
	//	}
	//
	//	/* Static 'instance' method */
	//	public static WordVectorDB getInstance() {
	//		return wordVectorDB;
	//	}
	//
	//	/* Other methods protected by singleton-ness */
	//	protected static void demoMethod() {
	//		System.out.println("demoMethod for singleton");
	//	}
	//
	//	//	private static WordVectorDB getDbObject() {
	//	//		return databaseAppArr[databaseType];
	//	//	}

	public static void init() {

		int arrSize = XpConfig.WV_DATABASE_TYPES.split(",").length;
		databaseAppArr = new WordVectorDB[arrSize];
		databaseType = XpConfig.WV_DATABASE_USE;
		System.out.println("Database Chosen \t" + databaseType);
		switch (databaseType) {
			case 0:
				databaseAppArr[0] = new WordVectorMongoApp();
				break;
			case 1:
				databaseAppArr[1] = new WordVectorMemsqlApp();
				WordVectorMemsqlApp.init();
				break;
			case 2:
				databaseAppArr[2] = new WordVectorCassandraApp();
				WordVectorCassandraApp.init();
				break;
		}
		//		//		databaseType = type;
		//		className = str;
		//		databaseAppArr[0] = new WordVectorMongoApp();
		//		databaseAppArr[1] = new WordVectorMemsqlApp();
	}

	public List<Double> getWordVectorForKey(Languages language, String key) {
		return databaseAppArr[databaseType].getWordVectorForKey(language, key);
	}

	public String getWordVectorCategoryForKey(Languages language, String key) {
		return databaseAppArr[databaseType].getWordVectorCategoryForKey(language, key);
	}

	public String getWordVectorCategoryForKey(Languages language, String key, String columnName) {

		String returnCategory = databaseAppArr[databaseType].getWordVectorCategoryForKey(language, key, columnName);
		//		System.out.println("Returning from WordVectorDB: " + returnCategory);
		return returnCategory;
		//		return databaseAppArr[databaseType].getWordVectorCategoryForKey(language, key, columnName);
	}

	public String getWordVectorCategorySingleWord(Languages language, String key, String columnName) {
		//		String category = gloveCategoryMap.get(key);

		String category = databaseAppArr[databaseType].getWordVectorCategoryForKey(language, key, columnName);
		//		String category = WordVectorDB.getWordVectorCategoryForKey(language, key, columnName);
		//		System.out.println("Returning from 3");
		return category;
	}

	public Object getColumnObjectForKey(Languages language, String key, String columnName) {
		return databaseAppArr[databaseType].getColumnObjectForKey(language, key, columnName);
	}

	public void updateData(Languages language, Set<String> entityList, String columnName, String category) {
		databaseAppArr[databaseType].updateData(language, entityList, columnName, category);
	}

	public void updateData(Languages language, String key, String columnName, Object value) {
		databaseAppArr[databaseType].updateData(language, key, columnName, value);
	}

	public FindIterable<Document> getDocuments(Languages language) {
		return databaseAppArr[databaseType].getDocuments(language);
	}

	public Set<String> getAllPhrasesWithVectors(Languages language) {
		return databaseAppArr[databaseType].getAllPhrasesWithVectors(language);
	}
	//	public static Class<?>getDBClass(){
	//		switch(databaseType){
	//			case 0: return WordVectorDB.class.getClassLoader()
	//		}
	//	}

	//	/**
	//	 * @param language
	//	 * @param key
	//	 * @param columnName
	//	 * @return
	//	 */
	//	public static Object getDBResult(Languages language, String key, String columnName) {
	//		try {
	//			Class<?> aClass = WordVectorDB.class.getClassLoader().loadClass(className);
	//			Method m = aClass.getMethod("getWordVectorCategoryForKey", Languages.class, String.class, String.class);
	//			return (Object) m.invoke(aClass, language, key, columnName);
	//			//			return aClass.getWordVectorCategoryForKey(language, key, columnName);
	//		} catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//		return null;
	//
	//	}

	//	public static String getWordVectorCategoryForKey(Languages language, String key, String columnName) {
	//		switch(databaseType):
	//			case(0): return WordVectorMongoApp.getWordVectorCategoryForKey(language, key, columnName);
	//			
	//	}

	//
	//	public static getDatabaseType(){
	//		WordVectorMongoApp mongoApp = new WordVectorMongoApp();
	//	}

}
