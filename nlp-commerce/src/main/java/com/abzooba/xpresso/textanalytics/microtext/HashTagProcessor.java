package  com.abzooba.xpresso.textanalytics.microtext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.textanalytics.LuceneSpell;
import com.abzooba.xpresso.utils.FileIO;

public class HashTagProcessor {

    private static Pattern upperCasePattern = null;
    private static Pattern lowerCasePattern = null;
    private static Pattern camelCasePattern = null;
    private static Map<Languages, Set<String>> affixSet = new HashMap<Languages, Set<String>>();
    private static Set<String> entitySet = new HashSet<String>();
    String ING_STR = "ing";

    private static Set<String> hashtagTokenCache = new HashSet<String>();

    public static void init() {
        for (Languages lang : XpConfig.LANGUAGES) {
            Set<String> affixSetLang = new HashSet<String>();
            FileIO.read_file(XpConfig.getSuffix(lang), affixSetLang);
            affixSet.put(lang, affixSetLang);
        }

        //		FileIO.read_file(XpConfig.SUFFIX_FILE, affixSet);
        FileIO.read_file(XpConfig.UNLISTED_ENTITY_FILE, entitySet);
        //		upperCasePattern = Pattern.compile("[A-Z]+");
        upperCasePattern = Pattern.compile("\\p{Lu}+", Pattern.UNICODE_CHARACTER_CLASS);
        //		lowerCasePattern = Pattern.compile("[a-z]+");
        lowerCasePattern = Pattern.compile("\\p{Ll}+", Pattern.UNICODE_CHARACTER_CLASS);
        //		camelCasePattern = Pattern.compile("([A-Z][a-z]+)|([A-Z]+)");
        camelCasePattern = Pattern.compile("(\\p{Lu}\\p{Ll}+)|(\\p{Lu}+)", Pattern.UNICODE_CHARACTER_CLASS);
        addToHashtagTokenHash("costco");
        addToHashtagTokenHash("walmart");
    }

    private static boolean isValidEntity(String text) {
        return entitySet.contains(text.toLowerCase());
    }

    private static boolean isValidAffix(Languages language, String text) {
        return affixSet.get(language).contains(text);
    }

    public static void addToHashtagTokenHash(String str) {
        synchronized (hashtagTokenCache) {
            if (str.length() > 4) {
                hashtagTokenCache.add(str);
                if (!str.equals(str.toLowerCase()))
                    hashtagTokenCache.add(str.toLowerCase());
            }
        }
    }

    public static boolean isCacheToken(String str) {
        return hashtagTokenCache.contains(str);
    }

    private static List<String> parseHashTagSpecialCase(Languages language, String text) {
        List<String> hashStack = new ArrayList<String>();
        switch (language) {
            case EN:
                if (text.toLowerCase().startsWith("iam")) {
                    String latterStr = text.substring(3);
                    if (checkIfWord(language, latterStr)) {
                        hashStack.add("I");
                        hashStack.add("am");
                        hashStack.add(latterStr);
                    }
                }
                break;
            case SP:
                if (text.toLowerCase().startsWith("soy")) {
                    String latterStr = text.substring(3);
                    if (checkIfWord(language, latterStr)) {
                        hashStack.add("soy");
                        hashStack.add(latterStr);
                    }
                } else if (text.toLowerCase().startsWith("estoy")) {
                    String latterStr = text.substring(5);
                    if (checkIfWord(language, latterStr)) {
                        hashStack.add("estoy");
                        hashStack.add(latterStr);
                    }
                }
                break;
        }

        return hashStack;
    }

    public static List<String> parseHashTag(Languages language, String hashText) {

        if (hashText.startsWith("#")) {
            if (hashText.length() == 1)
                return null;
            hashText = hashText.substring(1);
        }
        if (checkIfWord(language, hashText) || checkIfWord(language, hashText.toLowerCase())) {
            //	System.out.println("Case 1: existing word");
            List<String> hashStack = new ArrayList<String>();
            HashTagProcessor.addToHashtagTokenHash(hashText);
            hashStack.add(hashText);
            return hashStack;
        }
        Stack<String> tokenStack = processCamelCase(language, hashText);
        if (tokenStack != null && !tokenStack.isEmpty()) {
            //	System.out.println("Case 2: camel case");
            List<String> tokenList = addTokensToList(tokenStack);
            return tokenList;
        }
        List<String> hashTokenList = parseHashTagSpecialCase(language, hashText);
        if (!hashTokenList.isEmpty()) {
            //	System.out.println("Case 3: special case");
            return hashTokenList;
        }
        //		System.out.println("Entering the parse left to right module");

        Stack<String> properWordStack = new Stack<String>();
        parseHashTagLeftToRightImproved(language, hashText, properWordStack);
        //	System.out.println("Case 4: left to right improved; properWordStack:\t" + properWordStack.toString());
        List<String> tokenList = addTokensToList(properWordStack);
        return tokenList;
    }

    private static List<String> addTokensToList(Stack<String> wordStack) {
        List<String> tokenList = new ArrayList<String>();
        wordStack.forEach((token) -> {
            HashTagProcessor.addToHashtagTokenHash(token);
            tokenList.add(token);
        });
        return tokenList;
    }

    public static void parseHashTagLeftToRight(Languages language, String hashText, Stack<String> properWordStack) {
        StringBuilder currentSB = new StringBuilder();
        StringBuilder lastSB = new StringBuilder();
        boolean isWord = false;
        boolean isSecondaryWord = false;
        String lastStr = null;
        boolean skipLast = false;
        boolean isValidAffix = false;
        for (int i = 0; i < hashText.length(); i++) {
            currentSB.append(hashText.charAt(i));
            isValidAffix = isValidAffix(language, currentSB.toString());
            if (isValidAffix && !lastSB.toString().isEmpty()) {
                lastSB.append(currentSB.toString());
                currentSB = new StringBuilder();
                continue;
            }

            isWord = checkIfWord(language, currentSB.toString());
            if (isWord) {
                // System.out.println("current changed laststr : " +
                // lastSB.toString() +
                // " currentstr : " + currentSB.toString());
                lastSB.append(hashText.charAt(i));
                isSecondaryWord = checkIfWord(language, lastSB.toString());
                if (isSecondaryWord) {
                    if (!properWordStack.isEmpty())
                        properWordStack.pop();
                    properWordStack.add(lastSB.toString());
                    currentSB = new StringBuilder();

                    lastStr = hashText.substring(i + 1, hashText.length());
                    if (checkIfWord(language, lastSB.toString() + lastStr) || isValidAffix(language, lastStr)) {
                        properWordStack.pop();
                        properWordStack.add(lastSB.toString() + lastStr);
                        // System.out.println("last word added - " +
                        // lastSB.toString() + " : " + lastStr);
                        skipLast = true;
                        break;
                    } else if (checkIfWord(language, lastStr)) {
                        properWordStack.add(lastStr);
                        skipLast = true;
                        break;
                    }
                } else {
                    properWordStack.add(currentSB.toString());
                    lastSB = new StringBuilder(currentSB);
                    currentSB = new StringBuilder();
                    lastStr = hashText.substring(i + 1, hashText.length());
                    if (checkIfWord(language, lastSB.toString() + lastStr) || isValidAffix(language, lastStr)) {
                        properWordStack.pop();
                        properWordStack.add(lastSB.toString() + lastStr);
                        // System.out.println("last word added - " +
                        // lastSB.toString() + " : " + lastStr);
                        skipLast = true;
                        break;
                    } else if (checkIfWord(language, lastStr)) {
                        properWordStack.add(lastStr);
                        skipLast = true;
                        break;
                    }
                }
                continue;
            }

            lastSB.append(hashText.charAt(i));
            isWord = checkIfWord(language, lastSB.toString());
            if (isWord) {
                // System.out.println("last changed laststr : " +
                // lastSB.toString() +
                // " currentstr : " + currentSB.toString());
                if (!properWordStack.isEmpty())
                    properWordStack.pop();
                properWordStack.add(lastSB.toString());
                currentSB = new StringBuilder();
            }

            // System.out.println("last unchanged laststr : " +
            // lastSB.toString() +
            // " currentstr : " + currentSB.toString());
        }
        if (skipLast) {
            // System.out.println("skip last fired");
            return;
        }
        // System.out.println("outside loop laststr : " + lastSB.toString() +
        // " currentstr : " + currentSB.toString());
        if ((isWord = checkIfWord(language, lastSB.toString()))) {
            if (!properWordStack.isEmpty())
                properWordStack.pop();
            properWordStack.add(lastSB.toString());
            // System.out.println("string added - " + lastSB.toString());
        } else if (!currentSB.toString().isEmpty() && !currentSB.toString().equals("")) {
            if (properWordStack.isEmpty()) {
                properWordStack.add(currentSB.toString());
                // System.out.println("string added - " + currentSB.toString());
            } else {
                lastStr = properWordStack.pop();
                String combo = lastStr + currentSB.toString();
                if (isValidAffix(language, combo)) {
                    combo = properWordStack.pop() + combo;
                    properWordStack.add(combo);
                } else if (checkIfWord(language, combo) || currentSB.toString().length() == 1) {
                    properWordStack.add(combo);
                    // System.out.println("string added - " + combo);
                } else {
                    properWordStack.add(lastStr);
                    properWordStack.add(currentSB.toString());
                    // System.out.println("dual string added - " + lastStr +
                    // " : " + currentSB.toString());
                }
            }
        }

    }

    private static boolean checkRemainingString(Languages language, String hashText, int startIndex, Stack<String> properWordStack, StringBuilder lastSB) {
        boolean skipLast = false;
        String lastStr = hashText.substring(startIndex, hashText.length());
        if (checkIfWord(language, lastSB.toString() + lastStr)) {
            properWordStack.pop();
            properWordStack.add(lastSB.toString() + lastStr);
            skipLast = true;
        } else if (checkIfWord(language, lastStr)) {
            properWordStack.add(lastStr);
            skipLast = true;
        }
        return skipLast;
    }

    public static boolean checkComboWithLastString(Languages language, Stack<String> properWordStack, StringBuilder lastSB) {
        String currentStr = null;
        String bufferStr = null;
        int count = 0;
        while (!properWordStack.isEmpty()) {
            bufferStr = properWordStack.pop();
            currentStr = (currentStr == null) ? (bufferStr + lastSB.toString()) : (bufferStr + currentStr);
            if (!checkIfWord(language, currentStr)) {
                currentStr = currentStr.substring(bufferStr.length());
                properWordStack.add(bufferStr);
                properWordStack.add(currentStr);
                break;
            }
            count++;
        }
        if (count == 0)
            properWordStack.add(lastSB.toString());
        else if (count > 0 && properWordStack.isEmpty())
            properWordStack.add(currentStr);
        return (count > 0);
    }

    public static void parseHashTagLeftToRightImproved(Languages language, String hashText, Stack<String> properWordStack) {
        StringBuilder currentSB = new StringBuilder();
        StringBuilder lastSB = new StringBuilder();
        boolean skipLast = false;
        for (int i = 0; i < hashText.length(); i++) {
            currentSB.append(hashText.charAt(i));
            //			System.out.println("currentSB : " + currentSB.toString());
            if (!lastSB.toString().isEmpty()) {
                //				System.out.println("lastSB before append : " + lastSB.toString());
                int lastSBIndex = lastSB.toString().length();
                lastSB.append(currentSB.toString());
                if (checkIfWord(language, lastSB.toString())) {
                    //					System.out.println("last SB verified as word : " + lastSB.toString());
                    if (!properWordStack.isEmpty())
                        properWordStack.pop();
                    while (!properWordStack.isEmpty()) {
                        String bufferStr = properWordStack.pop();
                        lastSB.insert(0, bufferStr);
                        //						System.out.println("SB compiled before checking : " + lastSB.toString());
                        if (!checkIfWord(language, lastSB.toString())) {
                            //							System.out.println("buffer added after check word failed : " + bufferStr);
                            properWordStack.add(bufferStr);
                            lastSB.delete(0, bufferStr.length());
                            break;
                        }
                        //						System.out.println("last sb removed - " + bufferStr);
                    }

                    //					System.out.println("string added last string non-empty - " + lastSB.toString());
                    properWordStack.add(lastSB.toString());
                    //					isPositiveCount = checkComboWithLastString(properWordStack, lastSB);
                    currentSB = new StringBuilder();
                    if (i + 1 < hashText.length()) {
                        skipLast = checkRemainingString(language, hashText, i + 1, properWordStack, lastSB);
                        if (skipLast)
                            break;
                    }
                    continue;
                }
                lastSB.delete(lastSBIndex, lastSB.toString().length());
            }
            if (checkIfWord(language, currentSB.toString())) {
                //				System.out.println("string added current string - " + currentSB.toString());
                properWordStack.add(currentSB.toString());
                lastSB = new StringBuilder(currentSB);
                currentSB = new StringBuilder();
                if (i + 1 < hashText.length()) {
                    skipLast = checkRemainingString(language, hashText, i + 1, properWordStack, lastSB);
                    if (skipLast)
                        break;
                }
            }
        }
        if (skipLast)
            return;
        properWordStack.add(currentSB.toString());
    }

    private static boolean checkIfWord(Languages language, String text) {
        boolean isWord = false;
        if (text.length() == 1 && !text.matches("[I0-9]"))
            return isWord;
        if (text.matches("[0-9]+"))
            return true;
        if (isCacheToken(text))
            return true;
        isWord = LuceneSpell.checkIfCorrectWord(language, text, false);
        System.out.println("");
        if (!isWord)
            isWord = isValidEntity(text);
        return isWord;
    }

    private static Stack<String> processCamelCase(Languages language, String text) {
        Matcher mU = upperCasePattern.matcher(text);
        Matcher mL = lowerCasePattern.matcher(text);
        Stack<String> tokenStack = new Stack<String>();
        if (mU.find() && !mL.find()) {
            tokenStack.add(text);
            return tokenStack;
        } else if ((!mU.find() && mL.find()) || text.matches("[0-9]+"))
            return null;
        Matcher camelM = camelCasePattern.matcher(text);
        int lastIdx = -1;
        int startIdx = -1;
        while (camelM.find()) {
            tokenStack.add(camelM.group());
            lastIdx = camelM.end();
            if (startIdx == -1)
                startIdx = camelM.start();
        }

        if (lastIdx >= 0 && lastIdx < text.length() - 1) {
            String lastStr = null;
            if (lastIdx > 0) {
                lastStr = text.substring(lastIdx - 1);
                if (checkIfWord(language, lastStr))
                    tokenStack.add(lastStr);
                else
                    tokenStack.add(text.substring(lastIdx));
            } else
                tokenStack.add(text.substring(lastIdx));
        }
        if (startIdx > 0)
            tokenStack.add(text.substring(0, startIdx));
        return tokenStack;
    }

    public static void main(String[] args) throws IOException {
        XpEngine.init(true);
        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter the hashtag : ");
            String[] lineArr = br.readLine().split("LANG=");
            String hashText = lineArr[0];
            Languages lang;
            if (lineArr.length == 2)
                lang = XpConfig.convertLanguage(lineArr[1]);
            else
                lang = Languages.EN;

            System.out.println(parseHashTag(lang, hashText));
        }

        // String inputFile = "./input/hashtags.txt";
        // Stack<String> collection = new Stack<String>();
        // FileIO.read_file(inputFile, collection);
        // List<String> splitTagCollection = new ArrayList<String>();
        // while (!collection.isEmpty()) {
        // String text = collection.pop().trim();
        // splitTagCollection.add(text + "\t" + parseHashtag(text));
        // }
        // String outputFile = "./output/hashtags_split.txt";
        // FileIO.write_file(splitTagCollection, outputFile, false);
    }
}