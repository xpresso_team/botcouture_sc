/**
 * 
 */
package com.abzooba.xpresso.textanalytics;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.abzooba.xpresso.engine.config.XpConfig;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Pointer;

/**
 * @author Sumit Das
 * Jun 20, 2014 12:25:22 PM 
 */
public class JWIController {

	/**
	 * @param args
	 */
	private static String wordNetDirectory;
	private static IDictionary dict;

	// private static IRAMDictionary dict;

	public static void init() throws IOException {
		// wordNetDirectory = "./resources/WordNet-3.0";
		wordNetDirectory = XpConfig.WORDNET_LOC;
		String path = wordNetDirectory + "/dict";
		URL url = new URL("file", null, path);
		dict = new Dictionary(url);
		dict.open();
	}

	// public static void init() throws Exception {
	// // wordNetDirectory = "./resources/WordNet-3.0";
	// wordNetDirectory = XpConfig.WORDNET_LOC;
	// String path = wordNetDirectory + File.separator + "dict";
	// // URL url = new URL("file", null, path);
	// // dict = new Dictionary(url);
	// // dict.open();
	//
	// // construct the dictionary object and open it
	// dict = new RAMDictionary(new File(path), ILoadPolicy.NO_LOAD);
	//
	// dict.open();
	// // do something
	// trek();
	// // now load into memory
	// System.out.println("Loading Wordnet into memory ... ");
	// long t = System.currentTimeMillis();
	// dict.load(true);
	// System.out.println(" done (%1 d msec )"
	// + (System.currentTimeMillis() - t));
	// // try it again , this time in memory
	// trek();
	// }

	public static void trek() {
		int tickNext = 0;
		int tickSize = 20000;
		int seen = 0;
		System.out.println(" Treking across Wordnet ");
		long t = System.currentTimeMillis();
		for (POS pos : POS.values())
			for (Iterator<IIndexWord> i = dict.getIndexWordIterator(pos); i
					.hasNext();)
				for (IWordID wid : i.next().getWordIDs()) {
					seen += dict.getWord(wid).getSynset().getWords().size();
					if (seen > tickNext) {
						System.out.print(",");
						tickNext = seen + tickSize;
					}
				}
		System.out.println("done (%1 d msec ) "
				+ (System.currentTimeMillis() - t));
		System.out.println(" In my trek I saw " + seen + " words ");
	}

	public static List<String> getSynonyms(String w1, POS pos) {
		List<String> synonymList = null;
		IIndexWord idxWord = dict.getIndexWord(w1, pos);
		if (idxWord != null) {
			synonymList = new ArrayList<String>();
			IWordID wordID = idxWord.getWordIDs().get(0); // 1 st meaning
			IWord word = dict.getWord(wordID);
			ISynset synset = word.getSynset();
			// iterate over words associated with the synset
			for (IWord w : synset.getWords()) {
				// System.out.println(w.getLemma());
				synonymList.add(w.getLemma().replaceAll("_", " "));
			}
		}
		return synonymList;
	}

	public static List<String> getHypernyms(String w1, POS pos) {
		// get the synset
		List<String> hypernymList = null;
		IIndexWord idxWord = dict.getIndexWord(w1, pos);
		// System.out.println("@@idxWord: " + idxWord);
		if (idxWord != null) {
			hypernymList = new ArrayList<String>();
			IWordID wordID = idxWord.getWordIDs().get(0); // 1 st meaning
			IWord word = dict.getWord(wordID);
			ISynset synset = word.getSynset();
			// get the hypernyms
			List<ISynsetID> hypernyms = synset
					.getRelatedSynsets(Pointer.HYPERNYM);
			// print out each h y p e r n y m s id and synonyms
			List<IWord> words;
			for (ISynsetID sid : hypernyms) {
				words = dict.getSynset(sid).getWords();
				for (Iterator<IWord> i = words.iterator(); i.hasNext();) {
					hypernymList.add(i.next().getLemma().replaceAll("_", " "));
				}
			}
		}
		return hypernymList;
	}

	public static List<String> getHyponyms(String w1, POS pos) {
		// System.out.println("getHyponyms");
		// System.out
		// .println("--------------------------------------------------");
		// get the synset
		List<String> hyponymList = null;
		IIndexWord idxWord = dict.getIndexWord(w1, pos);
		// System.out.println("@@idxWord: " + idxWord);
		if (idxWord != null) {
			hyponymList = new ArrayList<String>();
			IWordID wordID = idxWord.getWordIDs().get(0); // 1 st meaning
			IWord word = dict.getWord(wordID);
			ISynset synset = word.getSynset();
			// get the hyponyms
			List<ISynsetID> hyponym = synset.getRelatedSynsets(Pointer.HYPONYM);
			// print out each hypernyms id and synonyms
			List<IWord> words;
			for (ISynsetID sid : hyponym) {
				words = dict.getSynset(sid).getWords();
				for (Iterator<IWord> i = words.iterator(); i.hasNext();) {
					hyponymList.add(i.next().getLemma().replaceAll("_", " "));
				}
			}
		}
		return hyponymList;
	}

	public static void main(String[] args) throws Exception {
		String propsFile = "./config/xdt.properties";
		XpConfig.init(propsFile);
		JWIController.init();
		POS pos = POS.NOUN;
		String word = "counter";
		System.out.println(getSynonyms(word, pos));
		System.out.println(getHypernyms(word, pos));
		System.out.println(getHyponyms(word, pos));
		getHyponyms(word, pos);
	}

}
