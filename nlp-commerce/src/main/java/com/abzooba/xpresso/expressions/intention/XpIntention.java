package  com.abzooba.xpresso.expressions.intention;

import java.util.HashMap;
import java.util.Map;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpSentenceResources;

public class XpIntention {

    private static Map<Languages, IntentLexicon> intentLexicons;

    public static void init() {
        intentLexicons = new HashMap<Languages, IntentLexicon>();
        for (Languages lang : XpConfig.LANGUAGES) {
            IntentLexicon intentLexicon;
            switch (lang) {
                case SP:
                    intentLexicon = new IntentLexicon(Languages.SP);//SentimentLexiconSP();
                    break;
                default:
                    intentLexicon = new IntentLexicon(Languages.EN);//SentimentLexiconEN();
            }
            intentLexicons.put(lang, intentLexicon);
        }
        IntentEngine.init();
    }

    // public static enum SENTIMENT_TYPE {
    // POSITIVE_SENTIMENT, NEGATIVE_SENTIMENT, NEUTRAL_SENTIMENT
    // };
    //
    // public static enum SENTI_TAG {
    // POSITIVE_TAG, NEGATIVE_TAG, UNCONDITIONAL_NEGATIVE_TAG,
    // UNCONDITIONAL_POSITIVE_TAG, INTENSIFIER_TAG, INCREASER_TAG,
    // DECREASER_TAG, SHIFTER_TAG, NPI_TAG, PPI_TAG, POSSESSION_TAG,
    // NON_POSSESSION_TAG, NEED_TAG, NON_SENTI_TAG
    // };

    /**
     * 17-Apr-2014 Koustuv Saha
     * Loads the various opinion lexicons
     * @param fileName
     * @param value
     * @param basic_kbMap
     * @param lemma_kbMap
     * @throws IOException
     *
     */
    //	private static void loadLexiconsMap(String fileName, String value, Map<String, String> basic_kbMap, Map<String, String> lemma_kbMap) throws IOException {
    //		List<String> wordList = new ArrayList<String>();
    //		FileIO.read_file(fileName, wordList);
    //		for (String str : wordList) {
    //			// kbMap.put(strLine.toLowerCase().trim(), value);
    //			// System.out.println("Putting " + strLine);
    //			String[] strArr = str.split("\t");
    //			String word = null;
    //			String lemma = null;
    //			if (strArr.length > 1) {
    //				word = strArr[0].toLowerCase().trim();
    //				lemma = strArr[1].toLowerCase().trim();
    //				CoreNLPController.addToLemmaMap(word, lemma);
    //			} else {
    //				word = str.toLowerCase().trim();
    //				lemma = CoreNLPController.lemmatizeToString(word).trim();
    //			}
    //			// System.out.println("Adding " + word + ">>" + value);
    //			basic_kbMap.put(word, value);
    //			lemma_kbMap.put(lemma, value);
    //		}
    //		basic_kbMap.put("but", SWITCHER_TAG);
    //	}

    /**
     * 17-Apr-2014 Koustuv Saha
     * Returns the sentiment tag of word
     * @param word
     * @return tag of the word
     *
     */

    //	public static String getLexTag(String str) {
    //		Map<String, String> wordLemmaMap = CoreNLPController.getWordLemmaMap();
    //		String tag = basic_lexicalHashMap.get(str);
    //		if (tag == null) {
    //			String[] strArr = str.split(" ");
    //			String lemma = "";
    //			for (String word : strArr) {
    //				String wordLemma = wordLemmaMap.get(word);
    //				if (wordLemma == null) {
    //					wordLemma = CoreNLPController.lemmatizeToString(word);
    //				}
    //				lemma += " " + wordLemmaMap.get(word);
    //			}
    //			lemma = lemma.trim();
    //			if (lemma.length() > 0) {
    //				tag = lemmatized_lexicalHashMap.get(lemma);
    //			}
    //		}
    //		return tag;
    //	}

    //	public static String getLexTag(Languages language, String str, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap) {
    //		return sentLexicons.get(language).getLexTag(str, wordPOSmap, tagSentimentMap);
    //	}

    public static boolean isIntentActionWord(Languages language, String str, String domainName, String subject, XpSentenceResources xto, boolean isAlreadyStrong) {

        return intentLexicons.get(language).isIntentActionWord(str, domainName, subject, xto, isAlreadyStrong);
    }

    public static boolean isNormalIntentAction(Languages language, String str, String domainName, String subject, XpSentenceResources xto) {

        return intentLexicons.get(language).isNormalIntentAction(str, domainName, subject, xto);
    }

    //	public static boolean isRejectionWord(Languages language, String str, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, String domainName, boolean isAlreadyStrong) {
    //		return sentLexicons.get(language).isRejectionWord(str);
    //	}

    public static boolean isIntentObjectWord(Languages language, String str, String domainName, String subject, XpSentenceResources xto) {
        return intentLexicons.get(language).isIntentObjectWord(str, domainName, subject, xto);
    }

    //	public static List<String> getAbuseList(Languages language) {
    //		return sentLexicons.get(language).getAbuseList();
    //	}

    //	public static List<String> getAdvocateList(Languages language) {
    //		return sentLexicons.get(language).getAdvocateList();
    //	}

    //	public static List<String> getSuggestionList(Languages language) {
    //		return sentLexicons.get(language).getSuggestionList();
    //	}

    //	public static List<String> getGreetSalutationsRegExList(Languages language) {
    //		return sentLexicons.get(language).getGreetSalutationsRegexList();
    //	}

    //	public static Set<String> getGreetSalutationsSet() {
    //		return greetingsSalutationSet;
    //	}

    // public static List<String> getPositiveEmoticonsList() {
    // synchronized (positiveEmoticonsList) {
    // return positiveEmoticonsList;
    // }
    // }
    //
    // public static List<String> getNegativeEmoticonsList() {
    // synchronized (negativeEmoticonsList) {
    // return negativeEmoticonsList;
    // }
    // }
    //
    //	public static Set<String> getGreetSalutationsList(Languages language) {
    //		return sentLexicons.get(language).getGreetSalutationsList();
    //	}
    //
    //	public static Map<String, String> getEmoticonsMap(Languages language) {
    //		return sentLexicons.get(language).getEmoticonsMap();
    //	}
    //
    //	public static Map<String, String> getIdiomsMap(Languages language) {
    //		return sentLexicons.get(language).getIdiomsMap();
    //	}
    //
    //	public static Map<String, String> getPhrasalVerbMap(Languages language) {
    //		switch (language) {
    //			case EN:
    //				return ((SentimentLexiconEN) sentLexicons.get(language)).getPhrasalVerbMap();
    //			default:
    //				return null;
    //		}
    //	}

}