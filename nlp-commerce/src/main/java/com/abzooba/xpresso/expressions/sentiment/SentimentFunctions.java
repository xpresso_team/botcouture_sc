/**
 *
 */
package com.abzooba.xpresso.expressions.sentiment;

import java.util.List;

/**
 * @author Koustuv Saha
 * 21-Aug-2014 4:00:44 pm
 * XpressoV2.0.1  SentimentFuncions
 */
public class SentimentFunctions {

    public static boolean isOppositeSentiment(String str1, String str2) {
        if (str1.equals(XpSentiment.POSITIVE_SENTIMENT) && str2.equals(XpSentiment.NEGATIVE_SENTIMENT)) {
            return true;
        } else if (str2.equals(XpSentiment.POSITIVE_SENTIMENT) && str1.equalsIgnoreCase(XpSentiment.NEGATIVE_SENTIMENT)) {
            return true;
        }
        return false;
    }

    public static boolean isOppositeSentimentTag(String sentiTag1, String sentiTag2) {

        if (sentiTag1.contains(XpSentiment.POSITIVE_TAG) && sentiTag2.contains(XpSentiment.NEGATIVE_TAG)) {
            return true;
        } else if (sentiTag2.contains(XpSentiment.POSITIVE_TAG) && sentiTag1.contains(XpSentiment.NEGATIVE_TAG)) {
            return true;
        }

        return false;
    }

    public static String getTagFromSentiment(String sentiment) {

        if (XpSentiment.POSITIVE_SENTIMENT.equals(sentiment)) {
            return XpSentiment.POSITIVE_TAG;
        } else if (XpSentiment.NEGATIVE_SENTIMENT.equals(sentiment)) {
            return XpSentiment.NEGATIVE_TAG;
        }
        return null;
    }

    public static String getSentimentFromTag(String tag) {
        switch (tag) {
            case XpSentiment.POSITIVE_TAG:
                return XpSentiment.POSITIVE_SENTIMENT;
            case XpSentiment.NEGATIVE_TAG:
                return XpSentiment.NEGATIVE_SENTIMENT;
            case XpSentiment.UNCONDITIONAL_POSITIVE_TAG:
                return XpSentiment.POSITIVE_SENTIMENT;
            case XpSentiment.UNCONDITIONAL_NEGATIVE_TAG:
                return XpSentiment.NEGATIVE_SENTIMENT;
        }

        return null;
    }

    public static boolean isCombinationPolar(String wordSentiTag, String opinionSentiTag) {
        if (isPolarPotentialItemTag(wordSentiTag) && isTunerTag(opinionSentiTag)) {
            return true;
        } else if (XpSentiment.RESOURCES_TAG.equals(wordSentiTag) && XpSentiment.CONSUME_TAG.equals(opinionSentiTag)) {
            return true;
        }
        return false;
    }

    public static boolean isTunerTag(String sentiTag) {
        if (XpSentiment.INCREASER_TAG.equals(sentiTag) || XpSentiment.DECREASER_TAG.equals(sentiTag) || XpSentiment.INTENSIFIER_TAG.equals(sentiTag)) {
            return true;
        }
        return false;
    }

    public static boolean isPolarSentiment(String sentiment) {
        if (XpSentiment.POSITIVE_SENTIMENT.equals(sentiment) || XpSentiment.NEGATIVE_SENTIMENT.equals(sentiment)) {
            return true;
        }
        return false;
    }

    public static boolean isPolarTag(String sentiment) {
        if (XpSentiment.POSITIVE_TAG.equals(sentiment) || XpSentiment.UNCONDITIONAL_POSITIVE_TAG.equals(sentiment) || XpSentiment.NEGATIVE_TAG.equals(sentiment) || XpSentiment.UNCONDITIONAL_NEGATIVE_TAG.equals(sentiment)) {
            return true;
        }
        return false;
    }

    public static boolean isPolarPotentialItemTag(String sentimentTag) {
        if (XpSentiment.PPI_TAG.equals(sentimentTag) || XpSentiment.NPI_TAG.equals(sentimentTag)) {
            return true;
        }
        return false;
    }

    public static boolean isPolarFlexibleTag(String sentimentTag) {
        if (isPolarTag(sentimentTag) || isPolarPotentialItemTag(sentimentTag))
            return true;
        return false;
    }

    public static String switchSentiment(String sentiment) {
        if (XpSentiment.POSITIVE_SENTIMENT.equals(sentiment)) {
            return XpSentiment.NEGATIVE_SENTIMENT;
        } else if (XpSentiment.NEGATIVE_SENTIMENT.equals(sentiment)) {
            return XpSentiment.POSITIVE_SENTIMENT;
        }
        return XpSentiment.NEUTRAL_SENTIMENT;
        //		return SentiLex.NONE_SENTIMENT;
    }

    public static String switchSentimentTag(String sentimentTag) {
        // System.out.println(sentimentTag);
        String oppTag = sentimentTag;
        if (sentimentTag != null) {
            if (sentimentTag.equals(XpSentiment.UNCONDITIONAL_NEGATIVE_TAG) || sentimentTag.equals(XpSentiment.NEGATIVE_TAG)) {
                oppTag = XpSentiment.POSITIVE_TAG;
            } else if (sentimentTag.equals(XpSentiment.UNCONDITIONAL_POSITIVE_TAG) || sentimentTag.equals(XpSentiment.POSITIVE_TAG)) {
                oppTag = XpSentiment.NEGATIVE_TAG;
            }
        }
        // System.out.println(oppTag);
        return oppTag;
    }

    public static String calculateAggregateSentiment(List<String> sentimentList) {
        //		String returnSentiment = SentiLex.NEUTRAL_SENTIMENT;
        int posCount = 0;
        int negCount = 0;
        for (String sentiment : sentimentList) {
            switch (sentiment) {
                case (XpSentiment.POSITIVE_SENTIMENT):
                    posCount += 1;
                    break;
                case (XpSentiment.NEGATIVE_SENTIMENT):
                    negCount += 1;
                    break;
            }
        }
        if (posCount > negCount) {
            return XpSentiment.POSITIVE_SENTIMENT;
        } else if (negCount > posCount) {
            return XpSentiment.NEGATIVE_SENTIMENT;
        } else {
            return XpSentiment.NEUTRAL_SENTIMENT;
        }

    }

    public static boolean isContainSentimentBearingTag(String taggedString) {
        if (taggedString.contains(XpSentiment.POSITIVE_TAG) || taggedString.contains(XpSentiment.NEGATIVE_TAG) || taggedString.contains(XpSentiment.PPI_TAG) || taggedString.contains(XpSentiment.NPI_TAG)) {
            return true;
        }
        return false;
    }

    public static boolean isFlexibleDescriptor(String tag) {
        if (XpSentiment.DECREASER_TAG.equals(tag) || XpSentiment.INTENSIFIER_TAG.equals(tag) || XpSentiment.INCREASER_TAG.equals(tag))
            return true;
        return false;
    }

    //	public static String sentimentVoteAggregator(String rbSentiment, String mlSentiment, String senti_tagged_str) {
    //
    //		if (isPolarSentiment(mlSentiment) && !isPolarSentiment(rbSentiment)) {
    //			if (senti_tagged_str == null) {
    //				return mlSentiment;
    //			} else if (senti_tagged_str.split(" ").length <= 2) {
    //				return rbSentiment;
    //			} else {
    //				return mlSentiment;
    //			}
    //		} else {
    //			return rbSentiment;
    //		}
    //
    //		//		if (rbSentiment.equals(mlSentiment)) {
    //		//			return rbSentiment;
    //		//		}
    //		//
    //		//		else if (isPolarSentiment(rbSentiment)) {
    //		//			return rbSentiment;
    //		//		} else if (isPolarSentiment(mlSentiment)) {
    //		//			return mlSentiment;
    //		//		} else {
    //		//			return rbSentiment;
    //		//		}
    //
    //		//		else
    //		//			return SentiLex.NEGATIVE_SENTIMENT.equals(mlSentiment) ? mlSentiment : rbSentiment;
    //
    //		//		return rbSentiment;
    //
    //	}

}