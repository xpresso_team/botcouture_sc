/**
 *
 */
package  com.abzooba.xpresso.expressions.intention;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.sdgraphtraversal.StanfordDependencyGraph;
import com.abzooba.xpresso.textanalytics.VocabularyTests;
import com.abzooba.xpresso.utils.FileIO;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.UniversalEnglishGrammaticalRelations;
import org.slf4j.Logger;

/**
 * @author Koustuv Saha
 * Aug 14, 2015 2:40:53 PM
 * XpressoV3.0  IntentEngine
 */
public class IntentEngine {

    protected static final Logger logger = XCLogger.getSpLogger();
    private static Map<String, List<Method>> domainRuleMap;

    public static List<Method> getDomainIntentRules(String domain) {
        return domainRuleMap.get(domain);
    }

    public static void init() {
        domainRuleMap = new HashMap<String, List<Method>>();
        List<String> tempList = new ArrayList<String>();
        FileIO.read_file(XpConfig.getIntentRules(Languages.EN), tempList);
        Method[] intentMethods = DomainSpecificIntent.class.getDeclaredMethods();
        for (String line : tempList) {
            String[] fieldArr = line.split("\t");
            if (fieldArr.length < 2)
                continue;
            Method intentMethod = intentMethodFinder(fieldArr[0], intentMethods);
            if (intentMethod == null)
                continue;
            for (int i = 1; i < fieldArr.length; i++) {
                if (domainRuleMap.containsKey(fieldArr[i])) {
                    domainRuleMap.get(fieldArr[i]).add(intentMethod);
                } else {
                    List<Method> ruleList = new ArrayList<Method>();
                    ruleList.add(intentMethod);
                    domainRuleMap.put(fieldArr[i], ruleList);
                }
            }
        }
        //		System.out.println("Intent Engine Loaded");
    }

    public static void DomainRuleController(String domain) {

    }

    private static Method intentMethodFinder(String rule, Method[] methodArr) {
        Method intentMethod = null;
        for (int i = 0; i < methodArr.length; i++) {
            if (methodArr[i].getName().equals(rule)) {
                intentMethod = methodArr[i];
                break;
            }
        }
        return intentMethod;
    }

    //	public static void intentionClassification(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, SemanticGraph dependencyGraph, boolean isAlreadyStrong, XpFeatureVector featureSet, String domainName, String subject, Map<String, String> negatedOpinionHash, Map<String, String[]> tagAspectMap, Map<String, String> tagSentimentMap, Map<String, String> wordPOSmap, Languages language, Map<IndexedWord, String> multiWordEntityMap) {
    public static void genericIntentClassification(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

		/*isAlreadyStrong is via Xcomp: I want to buy... I want to get*/

        String govWord = gov.word();

        Map<String, String> negatedOpinionMap = sdg.getNegatedOpinionMap();
        if (negatedOpinionMap != null && negatedOpinionMap.containsKey(govWord.toLowerCase())) {
            logger.info("IntentionClassifier: verb " + govWord + " is negated");
            return;
        }
        //		else {
        //			Set<IndexedWord> xcompParentSet = dependencyGraph.getParentsWithReln(gov, UniversalEnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT);
        //			if (xcompParentSet != null) {
        //				for (IndexedWord word : xcompParentSet) {
        //					if (negatedOpinionHash.containsKey(word.word().toLowerCase())) {
        //						logger.info("IntentionClassifier: verb " + word.word() + " is negated");
        //						return;
        //					}
        //				}
        //			}
        //		}

        String depWord = dep.word();
        boolean isDepIO = false;

        Languages language = sdg.getLanguage();
        String domainName = sdg.getDomainName();
        String subject = sdg.getSubject();

        boolean isGovIA = XpIntention.isIntentActionWord(language, govWord, domainName, subject, sdg.getXpTextObject(), isAlreadyStrong);
        logger.info("IntentAction for " + gov + ":\t" + isGovIA);

        //		if(!isGovIA)
        //			isGovIA = XpSentiment.isRejectionWord(language, govWord, wordPOSmap, tagSentimentMap, domainName, isAlreadyStrong);
        if (isGovIA) {
			/*Even if the isDepIO is true because of aspect presence, it should be made false if npo. eg: help*/
            isDepIO = XpIntention.isIntentObjectWord(language, depWord, domainName, subject, sdg.getXpTextObject());
            logger.info("IntentObject for " + dep + ": " + domainName + ":\t" + isDepIO);
            IndexedWord nsubjDep = sdg.getDependencyGraph().getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT);
            if (isDepIO && (nsubjDep == null || !VocabularyTests.isSecondThirdPersonSubject(language, nsubjDep.word()))) {

                Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                XpFeatureVector xpf = sdg.getFeatureSet();
                if (multiWordEntityMap.containsKey(dep)) {
                    xpf.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                } else {
                    xpf.setIntentionFeature(true, depWord, true);
                }
                logger.info("IntentionClassifier (True):\t " + gov + "\t" + dep);
                return;
            }
        }
        logger.info("IntentionClassifier (False):\t" + gov + "\t" + dep);
    }

    public static void intentionClassification(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

		/*isAlreadyStrong is via Xcomp: I want to buy... I want to get*/

        String govWord = gov.word();

        Map<String, String> negatedOpinionMap = sdg.getNegatedOpinionMap();
        if (negatedOpinionMap != null && negatedOpinionMap.containsKey(govWord.toLowerCase())) {
            logger.info("IntentionClassifier: verb " + govWord + " is negated");
            return;
        }
        //		else {
        //			Set<IndexedWord> xcompParentSet = dependencyGraph.getParentsWithReln(gov, UniversalEnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT);
        //			if (xcompParentSet != null) {
        //				for (IndexedWord word : xcompParentSet) {
        //					if (negatedOpinionHash.containsKey(word.word().toLowerCase())) {
        //						logger.info("IntentionClassifier: verb " + word.word() + " is negated");
        //						return;
        //					}
        //				}
        //			}
        //		}

        String depWord = dep.word();
        boolean isDepIO = false;

        Languages language = sdg.getLanguage();
        String domainName = sdg.getDomainName();
        String subject = sdg.getSubject();

        boolean isGovIA = XpIntention.isIntentActionWord(language, govWord, domainName, subject, sdg.getXpTextObject(), isAlreadyStrong);
        logger.info("IntentAction for " + gov + ":\t" + isGovIA);

        //		if(!isGovIA)
        //			isGovIA = XpSentiment.isRejectionWord(language, govWord, wordPOSmap, tagSentimentMap, domainName, isAlreadyStrong);
        if (isGovIA) {
			/*Even if the isDepIO is true because of aspect presence, it should be made false if npo. eg: help*/
            isDepIO = XpIntention.isIntentObjectWord(language, depWord, domainName, subject, sdg.getXpTextObject());
            logger.info("IntentObject for " + dep + ": " + domainName + ":\t" + isDepIO);
            IndexedWord nsubjDep = sdg.getDependencyGraph().getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT);
            if (isDepIO && (nsubjDep == null || !VocabularyTests.isSecondThirdPersonSubject(language, nsubjDep.word()))) {

                Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                XpFeatureVector xpf = sdg.getFeatureSet();
                if (multiWordEntityMap.containsKey(dep)) {
                    xpf.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                } else {
                    xpf.setIntentionFeature(true, depWord, true);
                }
                logger.info("IntentionClassifier (True):\t " + gov + "\t" + dep);
                return;
                //				this.featureSet.setIntentionObject(depWord);

                //				boolean isRelevant = false;
                //				String[] aspect = XpOntology.findAspect(language, depWord, domainName, subject, wordPOSmap, tagAspectMap);
                //				if (aspect != null) {
                //					isRelevant = true;
                //				}
                //				if (!isRelevant) {
                //					String category = XpOntology.getOntologyCategory(language, depWord);
                //					if (XpOntology.isDomainRelevantCategory(language, category, depWord)) {
                //						isRelevant = true;
                //					}
                //				}
                //				if (isRelevant || domainName == null) {
                //					logger.info("IntentionClassifier (True): " + gov + "\t" + dep);
                //					if (multiWordEntityMap.containsKey(dep)) {
                //						featureSet.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                //					} else {
                //						featureSet.setIntentionFeature(true, depWord, true);
                //					} //				this.featureSet.setIntentionObject(depWord);
                //				}
            }
        }
        logger.info("IntentionClassifier (False):\t" + gov + "\t" + dep);
    }

    public static void intentionClassification(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong, String domainName) {
        //		System.out.println("Intention Classifier entry");
        genericIntentClassification(gov, dep, edgeRelation, sdg, isAlreadyStrong);
        XpFeatureVector xpf = sdg.getFeatureSet();
        if (!xpf.isIntentionFeature()) {
            //			System.out.println("Domain Specific Intent Rules fired");
            //			switch (domainName) {
            //				case "banking":
            //					DomainSpecificIntent.infoSeekingIntent(gov, dep, edgeRelation, sdg, isAlreadyStrong);
            //					break;
            //			}
            List<Method> domainIntentMethods = getDomainIntentRules(domainName);
            if (domainIntentMethods == null)
                return;
            for (Method intentMethod : domainIntentMethods) {
                try {
                    intentMethod.invoke(null, gov, dep, edgeRelation, sdg, isAlreadyStrong);
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (xpf.isIntentionFeature())
                    break;
            }
        }
    }

}