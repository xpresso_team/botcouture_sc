/**
 * @author Alix Melchy
 * Oct 7, 2015 4:18:17 PM
 */
package  com.abzooba.xpresso.expressions.emotion.languages;

public class EmotionLexiconEN {

    public static boolean isThanks(String sentence) {
        String currentSentence = sentence.toLowerCase();
        int index = currentSentence.indexOf("thanks for");
        if (index == -1)
            index = currentSentence.indexOf("thanks to");
        if (index == -1)
            index = currentSentence.indexOf("thank you");
        if (index > -1 && index < 4)
            return true;
        return false;
    }

    public static boolean isFirstPerson(String token) {
        return token.matches("(i)|(we)") ? true : false;

    }
}