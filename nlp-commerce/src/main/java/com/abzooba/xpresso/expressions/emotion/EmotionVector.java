/**
 *
 */
package  com.abzooba.xpresso.expressions.emotion;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.aspect.domainontology.XpOntology;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpSentenceResources;
import com.abzooba.xpresso.expressions.emotion.languages.EmotionLexiconEN;
import com.abzooba.xpresso.expressions.emotion.languages.EmotionLexiconSP;
import com.abzooba.xpresso.expressions.sentiment.SentimentFunctions;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.sdgraphtraversal.StanfordDependencyGraph;
import com.abzooba.xpresso.searchengine.PmiCalculator;
import com.abzooba.xpresso.searchengine.SearchEngine.SEARCH_ENGINE;
import com.abzooba.xpresso.searchengine.SearchEngineHitCounter;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.PosTags;
import com.abzooba.xpresso.textanalytics.ProcessString;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;
import com.abzooba.xpresso.utils.ThreadUtils;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.UniversalEnglishGrammaticalRelations;
import org.slf4j.Logger;

/**
 * @author Koustuv Saha
 * Jan 14, 2015 12:00:49 PM
 * XpressoV2.7  EmotionVector
 */
public class EmotionVector {

    // private static int numEmotionCat;
    // public static int COUNT = 0;
    protected static final Logger logger = XCLogger.getSpLogger();

    private static final Double MIN_CUTOFF_VALUE = (Double) 0.1;

    private Double[] emotionVector;

    private String domain = null;
    private Languages language;

    private Set<String> checkedWordHash;
    private int numAffectWords;
    private String sentiment = null;
    private List<String> tokenList = null;
    private boolean isModifierPresent = false;
    private Set<String> minedConcepts;
    private boolean isConjNegCcPresent = false;

    private String sentence;
    // private String manualEmotion;

    private boolean affectedDirectWord = false;
    // public static PrintWriter PW;

    // private Set<String> negationTokenSet = new HashSet<String>();
    private Set<String> firstPersonVerbSet = new HashSet<String>();
    private Map<String, String> tokenContextualSentimentMap;
    private Map<String, String> wordPOSmap;

    private Set<SemanticGraphEdge> checkedEdgeHash;
    private SemanticGraph dependencyGraph;
    private StanfordDependencyGraph sdg;

    private XpSentenceResources xto;

    //	private boolean isCouldBased;

    // private void addToNegationSet(String token) {
    // negationTokenSet.add(token);
    // }

    private void addToFirstPersonVerbSet(String token) {
        firstPersonVerbSet.add(token);
    }

    public static void init() throws Exception {
        // PW = new PrintWriter(new FileWriter("log/emotionDirectWord.tsv"));
        EmotionLexicon.init();
        // numEmotionCat = EmotionLexicon.getEmotionCategories().size();
        NRCemotion.init();
        if (XpConfig.ENABLE_BING_SEARCH) {
            PmiCalculator.init();
        }
    }

    //	public EmotionVector(String sentence, List<String> tokenList, SemanticGraph dependencyGraph, String sentiment, String manualEmotion) {
    //		// this.sentence = sentence;
    //		this.dependencyGraph = dependencyGraph;
    //		logger.info( "Dependency Graph: " + dependencyGraph.toString(SemanticGraph.OutputFormat.LIST));
    //
    //		this.sentence = sentence;
    //		checkedWordHash = new HashSet<String>();
    //		numAffectWords = 0;
    //		this.sentiment = sentiment;
    //		this.tokenList = tokenList;
    //		emotionVector = new Double[EmotionLexicon.getEmotionCategoriesCount()];
    //		this.initializeEmotionVector(this.emotionVector);
    //		minedConcepts = new HashSet<String>();
    //
    //		// this.manualEmotion = manualEmotion;
    //		// logger.info( "Text: " + sentence + "\tManualEmotion: " +
    //		// manualEmotion);
    //		// System.out.println("Text: " + sentence + "\tManualEmotion: " +
    //		// manualEmotion);
    //
    //		// ConceptsMiner cmi = new ConceptsMiner(dependencyGraph, null,
    //		// sentence, null);
    //		// List<ConceptsVector> conceptsMinedList = cmi.mineConcepts();
    //		// Set<String> conceptsStringSet = new HashSet<String>();
    //		// for (ConceptsVector cv : conceptsMinedList) {
    //		// String currConcept = cv.getConcept();
    //		// if (currConcept.contains(" NOUN") || currConcept.contains("NEG ")) {
    //		// continue;
    //		// }
    //		// currConcept = currConcept.replaceAll(" NOUN", "").replaceAll("NEG ",
    //		// "");
    //		// currConcept = currConcept.replaceAll("[^\\w\\s]", "");
    //		// conceptsStringSet.add(currConcept.trim());
    //		// }
    //		//
    //		// for (String concept : conceptsStringSet) {
    //		// try {
    //		// double currScore = LogOddRatioCalculator.getLogORRatio(concept,
    //		// SEARCH_ENGINE.BING_API);
    //		// System.out.println("Concept: " + concept + "\tScore: " + currScore);
    //		// } catch (Exception e) {
    //		// e.printStackTrace();
    //		// }
    //		// }
    //	}

    //	public EmotionVector(Languages language, String sentence, List<String> tokenList, SemanticGraph dependencyGraph, String sentiment, String manualEmotion, String domain, Map<String, String> wordPOSmap, boolean isCouldBased) {
    public EmotionVector(Languages language, String sentence, List<String> tokenList, String sentiment, StanfordDependencyGraph sdg) {
        this.sentence = sentence.toLowerCase();
        //		logger.info( "Dependency Graph: " + dependencyGraph.toString(SemanticGraph.OutputFormat.LIST));

        checkedWordHash = new HashSet<String>();
        numAffectWords = 0;
        this.sentiment = sentiment;
        this.tokenList = tokenList;
        emotionVector = new Double[EmotionLexicon.getEmotionCategoriesCount()];
        initializeEmotionVector(this.emotionVector);
        minedConcepts = new HashSet<String>();
        this.sdg = sdg;
        this.language = language;
        if (sdg != null) {
            this.domain = sdg.getDomainName();
            //			this.wordPOSmap = sdg.getWordPOSmap();
            this.dependencyGraph = sdg.getDependencyGraph();
            this.xto = sdg.getXpTextObject();
            this.wordPOSmap = this.xto.getWordPOSmap();
        }
    }

    public void setConjunctionNegCCPresent(boolean isConjunctionNegCCPresent) {
        this.isConjNegCcPresent = isConjunctionNegCCPresent;
    }

    // private void setEmotionVector(String emotion) {
    // Integer idx = EmotionLexicon.getIndex(emotion);
    // if (idx != null) {
    // emotionVector[idx] += 1;
    // }
    // }

    // private void setEmotionVector(Set<String> govEmotionList, Set<String>
    // depEmotionList) {
    // List<String> allEmotionList = EmotionLexicon.getEmotionCategories();
    // if (govEmotionList != null) {
    // this.numAffectWords += 2;
    //
    // /*
    // * Adding 1 for gov, 1 for dep (even if dep is not an affect word, it is
    // made so because of gov
    // */
    // for (String emotion : govEmotionList) {
    // Integer idx = EmotionLexicon.getIndex(emotion);
    // if (idx != null) {
    // emotionVector[idx] += 1;
    // }
    // }
    // }
    // for (String emotion : allEmotionList) {
    // Integer idx = EmotionLexicon.getIndex(emotion);
    // double score = 0;
    // if (idx != null) {
    // if (govEmotionList != null && govEmotionList.contains(emotion)) {
    // score += 1;
    // }
    // if (depEmotionList != null && depEmotionList.contains(emotion)) {
    // score += 1;
    // }
    // score /= 2;
    // emotionVector[idx] += score;
    // }
    // }
    //
    // }

    private void setEmotionVector(Double[] govEmotionList, Double[] depEmotionList) {
        // List<String> allEmotionList = EmotionLexicon.getEmotionCategories();

        for (int i = 0; i < EmotionLexicon.getEmotionCategoriesCount(); i++) {

            double govVal;
            if (govEmotionList == null) {
                govVal = 0;
            } else {
                govVal = govEmotionList[i];
                this.numAffectWords += 1;
            }

            double depVal;
            if (depEmotionList == null) {
                depVal = 0;
            } else {
                depVal = depEmotionList[i];
                this.numAffectWords += 1;
            }

            if (govVal != 0) {
                double tempVal = (depVal + govVal) / 2;
                depVal = (tempVal > depVal) ? tempVal : depVal;
                //				depVal += govVal;
                //				depVal /= 2;
            }

            this.emotionVector[i] += govVal;
            this.emotionVector[i] += depVal;

            // this.emotionVector[i]+=
        }

        // if (govEmotionList != null) {
        // this.numAffectWords += 2;
        //
        // /*
        // * Adding 1 for gov, 1 for dep (even if dep is not an affect word, it
        // is made so because of gov
        // */
        // for (String emotion : govEmotionList) {
        // Integer idx = EmotionLexicon.getIndex(emotion);
        // if (idx != null) {
        // emotionVector[idx] += 1;
        // }
        // }
        // }
        // for (String emotion : allEmotionList) {
        // Integer idx = EmotionLexicon.getIndex(emotion);
        // double score = 0;
        // if (idx != null) {
        // if (govEmotionList != null && govEmotionList.contains(emotion)) {
        // score += 1;
        // }
        // if (depEmotionList != null && depEmotionList.contains(emotion)) {
        // score += 1;
        // }
        // score /= 2;
        // emotionVector[idx] += score;
        // }
        // }

    }

    // private void setEmotionVector(String token, String emotion, boolean
    // fromGlove) {
    // if (fromGlove) {
    // Double value = GloveEmotionHandler.getEmotionValueForToken(token,
    // emotion);
    // Integer idx = EmotionLexicon.getIndex(emotion);
    // if (idx != null) {
    // if (value.floatValue() > 0)
    // emotionVector[idx] += value.floatValue();
    // }
    // return;
    // }
    // this.setEmotionVector(emotion);
    // return;
    // }

    private void setEmotionVector(Double[] tokenEmotionVector) {
        //		System.out.println("emotion vector is null ? " + (emotionVector == null));
        //		System.out.println("token emotion vector is null ? " + (tokenEmotionVector == null));
        for (int i = 0; i < tokenEmotionVector.length; i++) {
            emotionVector[i] += tokenEmotionVector[i];
        }

        // if (fromGlove) {
        // Double value = GloveEmotionHandler.getEmotionValueForToken(token,
        // emotion);
        // Integer idx = EmotionLexicon.getIndex(emotion);
        // if (idx != null) {
        // if (value.floatValue() > 0)
        // emotionVector[idx] += value.floatValue();
        // }
        // return;
        // }
        // this.setEmotionVector(emotion);
        // return;
    }

    private void setEmotionVector(String emotion, double val) {
        Integer idx = EmotionLexicon.getIndex(emotion);
        if (idx != null) {
            emotionVector[idx] += val;
        }
    }

    private void normalizeEmotionVector() {
        if (this.numAffectWords != 0) {
            for (int i = 0; i < emotionVector.length; i++) {
                emotionVector[i] = emotionVector[i] / this.numAffectWords;
            }
        }
    }

    // private void wordCompareBasedEmotions(String token, Set<String>
    // emotionList) {
    // List<String> allEmotions = EmotionLexicon.getEmotionCategories();
    // for (String emotion : allEmotions) {
    // if (EmotionLexicon.getEmotionSeedVector(emotion).contains(token)) {
    // if (emotionList == null) {
    // emotionList = new HashSet<String>();
    // }
    // affectedDirectWord = true;
    // emotionList.add(emotion);
    // logger.info( "Word Compare Based Emotion Detected for: " +
    // token + " Emotion: " + emotion);
    // // System.out.println("Adding " + emotion +
    // " by the new heuristic for token " + token);
    // }
    // }
    // }

    private static void initializeEmotionVector(Double[] emotionVec) {
        for (int i = 0; i < emotionVec.length; i++) {
            emotionVec[i] = 0.0;
        }
    }

    private boolean contextualSentimentBasedEmotion(String token, Double[] emotionVec) {
        if (this.tokenContextualSentimentMap != null) {
            String sentimentTag = this.tokenContextualSentimentMap.get(token);
            if (sentimentTag != null && SentimentFunctions.isPolarTag(sentimentTag)) {
                if (XpSentiment.POSITIVE_TAG.equals(sentimentTag)) {
                    emotionVec[EmotionLexicon.getIndex(EmotionLexicon.JOY_EMOTION)] += 1;
                } else if (XpSentiment.NEGATIVE_TAG.equals(sentimentTag)) {
                    if (emotionVec[EmotionLexicon.getIndex(EmotionLexicon.DISGUST_EMOTION)] > emotionVec[EmotionLexicon.getIndex(EmotionLexicon.SADNESS_EMOTION)])
                        emotionVec[EmotionLexicon.getIndex(EmotionLexicon.DISGUST_EMOTION)] += 1;
                    else if (emotionVec[EmotionLexicon.getIndex(EmotionLexicon.ANGER_EMOTION)] > emotionVec[EmotionLexicon.getIndex(EmotionLexicon.SADNESS_EMOTION)])
                        emotionVec[EmotionLexicon.getIndex(EmotionLexicon.ANGER_EMOTION)] += 1;
                    else
                        emotionVec[EmotionLexicon.getIndex(EmotionLexicon.SADNESS_EMOTION)] += 1;
                }

                logger.info("Contextual Sentiment in Emotion:\t" + token + "\t" + sentimentTag);
                return true;
            }
        }
        return false;
    }

    private Double[] wordCompareBasedEmotions(String token) {
        List<String> allEmotions = EmotionLexicon.getEmotionCategories();
        Double[] tokenEmotionVector = null;

        for (String emotion : allEmotions) {

            boolean isPresentToken = EmotionLexicon.getEmotionSeedVector(language, emotion).contains(token);

            String lemma = CoreNLPController.lemmatizeToString(language, token);
            if (!isPresentToken) {
                isPresentToken = EmotionLexicon.getEmotionSeedVector(language, emotion).contains(lemma);
            }

            if (isPresentToken) {
                if (tokenEmotionVector == null) {
                    tokenEmotionVector = new Double[EmotionLexicon.getEmotionCategoriesCount()];
                    initializeEmotionVector(tokenEmotionVector);
                }
                tokenEmotionVector[EmotionLexicon.getIndex(emotion)] += 2;

                // this.emotionVector[EmotionLexicon.getIndex(emotion)] += 1;

                this.affectedDirectWord = true;
                logger.info("Word Compare Based Emotion Detected for: " + token + " Emotion: " + emotion);
            }
        }
        return tokenEmotionVector;

    }

    // private Set<String> getEmotion(String token, boolean fromGlove) {
    // private List<String> getEmotion(String token, boolean fromGlove) {
    // List<String> emotionList;
    // // emotionList = NRCemotion.getEmotion(token);
    // emotionList = EmotionLexicon.getEmotionCategories();
    //
    // if (emotionList == null) {
    // // GloveEmotionHandler.computeEmotionForToken(token);
    // // emotionList = EmotionLexicon.getEmotionCategories();
    // emotionList = (List<String>)
    // GloveEmotionHandler.computeEmotionForToken(token);
    // // emotionList = new
    // HashSet<String>(GloveEmotionHandler.computeEmotionForToken(token));
    // }
    // // if (fromGlove) {
    // // emotionList = new
    // HashSet<String>(GloveEmotionHandler.computeEmotionForToken(token));
    // // } else {
    // // emotionList = NRCemotion.getEmotion(token);
    // // }
    //
    // this.wordCompareBasedEmotions(token, emotionList);
    //
    // return emotionList;
    // }

    //	private Double[] getTokenEmotionVector(String token) {
    //
    //		if (XpOntology.isDomainNonSentimentPhrase(token, this.domain)) {
    //			logger.info("Domain Non Sentiment Phrase\tToken: " + token + "\tDomain: " + this.domain);
    //			return null;
    //		}
    //
    //		Double[] emotionVec = this.wordCompareBasedEmotions(token);
    //		logger.info("Direct Word\tToken: " + token + "\tEmotion List: " + Arrays.toString(emotionVec));
    //		if (emotionVec == null && !ProcessString.isStopWord(Languages.EN, token)) {
    //			emotionVec = NRCemotion.getEmotion(token);
    //			logger.info("NRC Lexicon\tToken: " + token + "\tEmotion List: " + Arrays.toString(emotionVec));
    //
    //			//			if (emotionVec == null && XpConfig.EMOTION_GLOVE_FLAG) {
    //			if (emotionVec == null) {
    //				//				System.out.println("Entered inside Glove");
    //				//				emotionVec = GloveEmotionHandler.computeEmotionForToken(token);
    //
    //				emotionVec = Glove.getGloveVector(token, EmotionLexicon.getEmotionVectorMap(), EmotionLexicon.getEmotionCategories(), true);
    //
    //				logger.info("Glove Based\tToken: " + token + "\tEmotion List: " + Arrays.toString(emotionVec));
    //			}
    //		}
    //
    //		if (emotionVec != null) {
    //			boolean isContextualSentiBasedEmotion = this.contextualSentimentBasedEmotion(token, emotionVec);
    //			if (isContextualSentiBasedEmotion) {
    //				logger.info("ContextualSentimentBasedEmotion\tToken: " + token + "\tEmotion List: " + Arrays.toString(emotionVec));
    //			}
    //		}
    //
    //		return emotionVec;
    //	}

    private Double[] getTokenEmotionVector(String token, String pos) {

        if (XpOntology.isDomainNonSentimentPhrase(this.language, token, this.domain)) {
            logger.info("Domain Non Sentiment Phrase\tToken: " + token + "\tDomain: " + this.domain);
            return null;
        }

        Double[] emotionVec = this.wordCompareBasedEmotions(token);
        logger.info("Direct Word\tToken: " + token + "\tEmotion List: " + Arrays.toString(emotionVec));
        if (emotionVec == null && !ProcessString.isStopWord(language, token)) {
            emotionVec = NRCemotion.getEmotion(language, token, pos);
            logger.info("NRC Lexicon\tToken: " + token + "\tEmotion List: " + Arrays.toString(emotionVec));

            //			if (emotionVec == null && XpConfig.EMOTION_GLOVE_FLAG) {
            //			if ((emotionVec == null) && (language == Languages.EN)) {
            if (emotionVec == null) {
                //				System.out.println("Entered inside Glove");
                //				emotionVec = GloveEmotionHandler.computeEmotionForToken(token);

                emotionVec = WordVector.getGloveCategoryWiseVector(language, token, EmotionLexicon.getEmotionVectorMap(language), EmotionLexicon.getEmotionCategories(), true);

                logger.info("Glove Based\tToken: " + token + "\tEmotion List: " + Arrays.toString(emotionVec));
            }
        }

        if (emotionVec != null) {
            boolean isContextualSentiBasedEmotion = this.contextualSentimentBasedEmotion(token, emotionVec);
            if (isContextualSentiBasedEmotion) {
                logger.info("ContextualSentimentBasedEmotion\tToken: " + token + "\tEmotion List: " + Arrays.toString(emotionVec));
            }
        }

        return emotionVec;
    }

    private Double[] getConceptEmotionVector(String concept) {
        SearchAPIEmotion se = new SearchAPIEmotion();
        Double[] emotionVec = se.getEmotion(language, concept);
        return emotionVec;
    }

    private void roundUpValues() {
        for (int i = 0; i < emotionVector.length; i++) {
            if (emotionVector[i] > 0)
                emotionVector[i] = (Double) (Math.round((double) emotionVector[i] * (double) 100) / (double) 100.0);
        }
    }

    public String getEmotionVectorString() {
        StringBuilder vecString = new StringBuilder();
        for (int i = 0; i < emotionVector.length; i++) {
            vecString.append(emotionVector[i]).append("\t");
        }
        return vecString.toString().trim();
    }

    private void dependencyVectorize(IndexedWord gov, IndexedWord dep, String relName) {

        String govWord = gov.word().toLowerCase();
        String depWord = dep.word().toLowerCase();
        this.checkedWordHash.add(govWord);
        this.checkedWordHash.add(depWord);

        // Set<String> govEmotionList = this.getEmotion(govWord,
        // XpConfig.EMOTION_GLOVE_FLAG);

        String tag = XpOntology.getDomainContextualSentimentTag(language, govWord, depWord, domain, null, this.xto);
        if (tag != null) {
            String[] sentiTag = tag.split("#");
            if (sentiTag.length >= 1) {
                if (tokenContextualSentimentMap == null) {
                    this.tokenContextualSentimentMap = new HashMap<String, String>();
                }
                this.tokenContextualSentimentMap.put(govWord, sentiTag[0]);
            }
        }

        //		Double[] govEmotionList = this.getTokenEmotionVector(govWord);
        Double[] govEmotionList = this.getTokenEmotionVector(govWord, gov.tag());

        // if (govEmotionList == null) {
        // govEmotionList =
        // this.getTokenEmotionVector(CoreNLPController.lemmatizeToString(govWord));
        // }

        //		Double[] depEmotionList = this.getTokenEmotionVector(depWord);
        Double[] depEmotionList = this.getTokenEmotionVector(depWord, dep.tag());
        // if (depEmotionList == null) {
        // depEmotionList =
        // this.getTokenEmotionVector(CoreNLPController.lemmatizeToString(depWord));
        // }

        logger.info("Relation:" + relName + " Gov: " + govWord + "\tEmotion List: " + Arrays.toString(govEmotionList));
        logger.info("Relation:" + relName + " Dep: " + depWord + "\tEmotion List: " + Arrays.toString(depEmotionList));

        this.setEmotionVector(govEmotionList, depEmotionList);

        this.addToMinedConcepts(depWord, govWord);

    }

    //	public void dependencyVectorize(IndexedWord gov, IndexedWord dep, String relName) {
    //
    //		String govWord = gov.word().toLowerCase();
    //		String depWord = dep.word().toLowerCase();
    //		this.checkedWordHash.add(govWord);
    //		this.checkedWordHash.add(depWord);
    //
    //		// Set<String> govEmotionList = this.getEmotion(govWord,
    //		// XpConfig.EMOTION_GLOVE_FLAG);
    //
    //		String tag = SentimentEngine.getDomainContextualSentimentTag(language, govWord, depWord, domain, null, wordPOSmap, null);
    //		if (tag != null) {
    //			String[] sentiTag = tag.split("#");
    //			if (sentiTag.length >= 1) {
    //				if (tokenContextualSentimentMap == null) {
    //					this.tokenContextualSentimentMap = new HashMap<String, String>();
    //				}
    //				this.tokenContextualSentimentMap.put(govWord, sentiTag[0]);
    //			}
    //		}
    //
    //		//		Double[] govEmotionList = this.getTokenEmotionVector(govWord);
    //		Double[] govEmotionArr = this.getTokenEmotionVector(govWord, gov.tag());
    //
    //		// if (govEmotionList == null) {
    //		// govEmotionList =
    //		// this.getTokenEmotionVector(CoreNLPController.lemmatizeToString(govWord));
    //		// }
    //
    //		//		Double[] depEmotionList = this.getTokenEmotionVector(depWord);
    //		Double[] depEmotionArr = this.getTokenEmotionVector(depWord, dep.tag());
    //		// if (depEmotionList == null) {
    //		// depEmotionList =
    //		// this.getTokenEmotionVector(CoreNLPController.lemmatizeToString(depWord));
    //		// }
    //
    //		logger.info("Relation:" + relName + " Gov: " + govWord + "\tEmotion List: " + Arrays.toString(govEmotionArr));
    //		logger.info("Relation:" + relName + " Dep: " + depWord + "\tEmotion List: " + Arrays.toString(depEmotionArr));
    //
    //		this.setEmotionVector(govEmotionArr, depEmotionArr);
    //
    //		this.addToMinedConcepts(depWord, govWord);
    //
    //	}

    private void addToMinedConcepts(String firstWord, String secondWord) {
        if (firstWord.matches("\\w+") && secondWord.matches("\\w+")) {
            this.minedConcepts.add(firstWord + " " + secondWord);
        }
    }

    //	private void negationVectorizeBkup(IndexedWord gov, IndexedWord dep, String relName) {
    //		String govWord = gov.word().toLowerCase();
    //		String depWord = dep.word().toLowerCase();
    //		this.checkedWordHash.add(govWord);
    //		this.checkedWordHash.add(depWord);
    //		// Set<String> govEmotionList = this.getEmotion(govWord,
    //		// XpConfig.EemoMOTION_GLOVE_FLAG);
    //		//		Double[] govEmotionList = this.getTokenEmotionVector(govWord);
    //		Double[] govEmotionList = this.getTokenEmotionVector(govWord, gov.tag());
    //
    //		if (govEmotionList != null) {
    //			for (int i = 0; i < govEmotionList.length; i++) {
    //				if (govEmotionList[i] > 0) {
    //					// this.emotionVector[i] = 0.0F;
    //
    //					if (EmotionLexicon.getEmotionFromIdx(i).equals(EmotionLexicon.JOY_EMOTION)) {
    //						this.setEmotionVector(EmotionLexicon.SADNESS_EMOTION, govEmotionList[i]);
    //					} else if (EmotionLexicon.getEmotionFromIdx(i).equals(EmotionLexicon.SADNESS_EMOTION)) {
    //						this.setEmotionVector(EmotionLexicon.JOY_EMOTION, govEmotionList[i]);
    //					}
    //
    //				}
    //			}
    //		}
    //		if (this.tokenContextualSentimentMap != null) {
    //			String govContextualSentiTag = this.tokenContextualSentimentMap.get(govWord);
    //			if (govContextualSentiTag != null) {
    //				this.tokenContextualSentimentMap.put(govWord, SentimentFunctions.switchSentimentTag(govContextualSentiTag));
    //			}
    //		}
    //
    //		// if (govEmotionList != null) {
    //		// for (String emotion : govEmotionList) {
    //		// if (emotion.equals(EmotionLexicon.JOY_EMOTION)) {
    //		// this.setEmotionVector(EmotionLexicon.SADNESS_EMOTION);
    //		// this.numAffectWords += 1;
    //		// } else if (emotion.equals(EmotionLexicon.SADNESS_EMOTION)) {
    //		// this.setEmotionVector(EmotionLexicon.JOY_EMOTION);
    //		// this.numAffectWords += 1;
    //		// }
    //		// }
    //		// }
    //		// logger.info( "Relation:" + relName + " Gov: " + govWord +
    //		// "\tEmotion List: " + govEmotionList);
    //		// logger.info( "Relation:" + relName + " Dep: " + depWord +
    //		// "\tEmotion List: " + depEmotionList);
    //
    //	}

    public void negationVectorize(IndexedWord gov, IndexedWord dep, String relName) {
        String govWord = gov.word().toLowerCase();
        // Set<String> govEmotionList = this.getEmotion(govWord,
        // XpConfig.EemoMOTION_GLOVE_FLAG);
        //		Double[] govEmotionList = this.getTokenEmotionVector(govWord);
        Double[] govEmotionList = this.getTokenEmotionVector(govWord, gov.tag());

        if (govEmotionList != null) {
            for (int i = 0; i < govEmotionList.length; i++) {
                if (govEmotionList[i] > 0) {
                    // this.emotionVector[i] = 0.0F;

                    if (EmotionLexicon.getEmotionFromIdx(i).equals(EmotionLexicon.JOY_EMOTION)) {
                        this.setEmotionVector(EmotionLexicon.SADNESS_EMOTION, govEmotionList[i]);
                    } else if (EmotionLexicon.getEmotionFromIdx(i).equals(EmotionLexicon.SADNESS_EMOTION)) {
                        this.setEmotionVector(EmotionLexicon.JOY_EMOTION, govEmotionList[i]);
                    }

                }
            }
        }
        if (this.tokenContextualSentimentMap != null) {
            String govContextualSentiTag = this.tokenContextualSentimentMap.get(govWord);
            if (govContextualSentiTag != null) {
                this.tokenContextualSentimentMap.put(govWord, SentimentFunctions.switchSentimentTag(govContextualSentiTag));
            }
        }

        // if (govEmotionList != null) {
        // for (String emotion : govEmotionList) {
        // if (emotion.equals(EmotionLexicon.JOY_EMOTION)) {
        // this.setEmotionVector(EmotionLexicon.SADNESS_EMOTION);
        // this.numAffectWords += 1;
        // } else if (emotion.equals(EmotionLexicon.SADNESS_EMOTION)) {
        // this.setEmotionVector(EmotionLexicon.JOY_EMOTION);
        // this.numAffectWords += 1;
        // }
        // }
        // }
        // logger.info( "Relation:" + relName + " Gov: " + govWord +
        // "\tEmotion List: " + govEmotionList);
        // logger.info( "Relation:" + relName + " Dep: " + depWord +
        // "\tEmotion List: " + depEmotionList);

    }

    private void vectorizeSentence() {
        checkedEdgeHash = new HashSet<SemanticGraphEdge>();
        if (this.dependencyGraph != null) {

            //			List<SemanticGraphEdge> sortedEdgesList = this.dependencyGraph.edgeListSorted();
            //			Collections.sort(sortedEdgesList, new DependencyRelationSorter());

            List<SemanticGraphEdge> sortedEdgesList = sdg.getSortedEdges();

            for (SemanticGraphEdge edge : sortedEdgesList) {
                if (checkedEdgeHash.contains(edge)) {
                    continue;
                }

                IndexedWord gov = edge.getGovernor();
                IndexedWord dep = edge.getDependent();

                String govWord = gov.word().toLowerCase();
                String depWord = dep.word().toLowerCase();

                if ((this.sentence.contains(govWord) && this.sentence.contains(depWord))) {
                    GrammaticalRelation rel = edge.getRelation();
                    if (rel.equals(UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER)) {
                        dependencyVectorize(gov, dep, rel.getShortName());
                        this.isModifierPresent = true;
                    }
                    //					else if (rel.equals(UniversalEnglishGrammaticalRelations.ADJECTIVAL_COMPLEMENT)) {
                    //						dependencyVectorize(gov, dep, rel.getShortName());
                    //					}
                    else if (rel.equals(UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER)) {
                        dependencyVectorize(gov, dep, rel.getShortName());
                        // this.addToNegationSet(gov.word().toLowerCase());
                        this.isModifierPresent = true;
                    } else if (rel.equals(UniversalEnglishGrammaticalRelations.NEGATION_MODIFIER)) {
                        negationVectorize(gov, dep, rel.getShortName());
                    } else if (rel.equals(UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT) || rel.equals(UniversalEnglishGrammaticalRelations.NOMINAL_PASSIVE_SUBJECT)) {
                        nsubjVectorize(gov, dep, rel.getShortName());
                    } else if (rel.equals(UniversalEnglishGrammaticalRelations.DIRECT_OBJECT)) {
                        dobjVectorize(gov, dep, rel.getShortName());
                    } else if (rel.toString().equals("conj_negcc")) {
                        this.isConjNegCcPresent = true;
                    }
                }
            }
        }

    }

    private void nsubjVectorize(IndexedWord gov, IndexedWord dep, String relName) {

        String govWord = gov.word().toLowerCase();
        String depWord = dep.word().toLowerCase();

        if (!PosTags.isPersonalPronoun(language, dep.tag())) {
            this.addToMinedConcepts(govWord, depWord);
        } else if (isFirstPerson(depWord)) {
            logger.info("CheckFirstPersonPositive for " + depWord);
            String govTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, null);
            if (govTag != null && govTag.equals(XpSentiment.POSITIVE_TAG)) {
                this.addToFirstPersonVerbSet(govWord);
            }
        }

        String tag = XpOntology.getDomainContextualSentimentTag(language, depWord, govWord, domain, sdg.getSubject(), this.xto);
        if (tag != null) {
            String[] sentiTag = tag.split("#");
            if (sentiTag.length >= 1) {
                if (tokenContextualSentimentMap == null) {
                    this.tokenContextualSentimentMap = new HashMap<String, String>();
                }
                this.tokenContextualSentimentMap.put(depWord, sentiTag[0]);
            }
        }

    }

    //	public void nsubjVectorize(IndexedWord gov, IndexedWord dep, String relName) {
    //
    //		String govWord = gov.word().toLowerCase();
    //		String depWord = dep.word().toLowerCase();
    //
    //		if (!dep.tag().startsWith("PRP")) {
    //			this.addToMinedConcepts(govWord, depWord);
    //		} else if (isFirstPerson(depWord)) {
    //			logger.info("checkFirstPersonPositive for " + depWord);
    //			String govTag = XpSentiment.getLexTag(Languages.EN, govWord, wordPOSmap, null);
    //			if (govTag != null && govTag.equals(XpSentiment.POSITIVE_TAG)) {
    //				this.addToFirstPersonVerbSet(govWord);
    //			}
    //		}
    //
    //		String tag = SentimentEngine.getDomainContextualSentimentTag(language, depWord, govWord, domain, null, wordPOSmap, null);
    //		if (tag != null) {
    //			String[] sentiTag = tag.split("#");
    //			if (sentiTag.length >= 1) {
    //				if (tokenContextualSentimentMap == null) {
    //					this.tokenContextualSentimentMap = new HashMap<String, String>();
    //				}
    //				this.tokenContextualSentimentMap.put(depWord, sentiTag[0]);
    //			}
    //		}
    //
    //	}

    //	private void dobjVectorize(IndexedWord gov, IndexedWord dep, String relName) {
    //
    //		String govToken = gov.word().toLowerCase();
    //		String depToken = dep.word().toLowerCase();
    //		this.addToMinedConcepts(govToken, depToken);
    //
    //	}

    public void dobjVectorize(IndexedWord gov, IndexedWord dep, String relName) {

        String govToken = gov.word().toLowerCase();
        String depToken = dep.word().toLowerCase();
        this.addToMinedConcepts(govToken, depToken);

    }

    private boolean isFirstPerson(String token) {
        //		return token.matches("(i)|(we)") ? true : false;
        boolean answ = false;
        switch (language) {
            case EN:
                answ = EmotionLexiconEN.isFirstPerson(token);
                break;
            case SP:
                answ = EmotionLexiconSP.isFirstPerson(token);
                break;
        }

        return answ;
    }

    private void removeNegativeEmotions(Set<String> finalEmotionsHash) {
        if (finalEmotionsHash.contains(EmotionLexicon.SADNESS_EMOTION)) {
            if (this.emotionVector[EmotionLexicon.getIndex(EmotionLexicon.SADNESS_EMOTION)] < 11 * MIN_CUTOFF_VALUE) {
                finalEmotionsHash.remove(EmotionLexicon.SADNESS_EMOTION);
            }
            // PW.println("No Modifier\tSADNESS removed\t" + this.sentence);
        }
        if (finalEmotionsHash.contains(EmotionLexicon.ANGER_EMOTION)) {
            if (this.emotionVector[EmotionLexicon.getIndex(EmotionLexicon.ANGER_EMOTION)] < 11 * MIN_CUTOFF_VALUE) {
                finalEmotionsHash.remove(EmotionLexicon.ANGER_EMOTION);
            } // PW.println("No Modifier\tANGER removed\t" + this.sentence);
        }
        if (finalEmotionsHash.contains(EmotionLexicon.DISGUST_EMOTION)) {
            if (this.emotionVector[EmotionLexicon.getIndex(EmotionLexicon.DISGUST_EMOTION)] < 11 * MIN_CUTOFF_VALUE) {
                finalEmotionsHash.remove(EmotionLexicon.DISGUST_EMOTION);
            } // PW.println("No Modifier\tDISGUST removed\t" + this.sentence);
        }
        if (finalEmotionsHash.contains(EmotionLexicon.FEAR_EMOTION)) {
            if (this.emotionVector[EmotionLexicon.getIndex(EmotionLexicon.FEAR_EMOTION)] < 5 * MIN_CUTOFF_VALUE) {
                finalEmotionsHash.remove(EmotionLexicon.FEAR_EMOTION);
            } // PW.println("No Modifier\tDISGUST removed\t" + this.sentence);
        }
    }

    private void removePositiveEmotions(Set<String> finalEmotionsHash) {
        if (finalEmotionsHash.contains(EmotionLexicon.JOY_EMOTION)) {
            if (this.emotionVector[EmotionLexicon.getIndex(EmotionLexicon.JOY_EMOTION)] < 11 * MIN_CUTOFF_VALUE) {
                finalEmotionsHash.remove(EmotionLexicon.JOY_EMOTION);
                // PW.println("No Modifier\tJOY removed\t" + this.sentence);
            }
        }
        if (finalEmotionsHash.contains(EmotionLexicon.SURPRISE_EMOTION)) {
            if (this.emotionVector[EmotionLexicon.getIndex(EmotionLexicon.SURPRISE_EMOTION)] < 5 * MIN_CUTOFF_VALUE) {
                finalEmotionsHash.remove(EmotionLexicon.SURPRISE_EMOTION);
                // PW.println("No Modifier\tJOY removed\t" + this.sentence);
            }
        }
    }

    private void applyRules(Set<String> finalEmotionsHash) {
		/*If ifCouldBased is true, positiveEmotions should be removed, sentiment should get higher priority. Next comes the contextualSentiment's priority*/
        //		boolean removeSentimentPriorityCondition = (this.tokenContextualSentimentMap == null || isCouldBased);
        boolean removeSentimentPriorityCondition = this.tokenContextualSentimentMap == null;

        if (SentimentFunctions.isPolarSentiment(this.sentiment)) {
            // String emotionFromSentiment = this.getEmotionFromSentiment();

            // finalEmotionsHash.add(emotionFromSentiment);

            if (XpSentiment.POSITIVE_SENTIMENT.equals(this.sentiment)) {
                if (removeSentimentPriorityCondition) {
                    this.removeNegativeEmotions(finalEmotionsHash);
                    finalEmotionsHash.add(EmotionLexicon.JOY_EMOTION);
                }

            } else if (XpSentiment.NEGATIVE_SENTIMENT.equals(this.sentiment)) {
                //				System.out.println("isCouldBased: " + isCouldBased);
                //				System.out.println("tokenContextualMap: " + tokenContextualSentimentMap);

                if (removeSentimentPriorityCondition) {
                    this.removePositiveEmotions(finalEmotionsHash);
                }
                if (finalEmotionsHash.contains(EmotionLexicon.ANGER_EMOTION) || finalEmotionsHash.contains(EmotionLexicon.DISGUST_EMOTION)) {
                    if (this.emotionVector[EmotionLexicon.getIndex(EmotionLexicon.SADNESS_EMOTION)] > MIN_CUTOFF_VALUE / 2) {
                        finalEmotionsHash.add(EmotionLexicon.SADNESS_EMOTION);
                    }
                } else {
                    finalEmotionsHash.add(EmotionLexicon.SADNESS_EMOTION);
                } // if (!this.containsNegativeEmotions(finalEmotionsHash)) {

            }

            // }

        } else {

			/* We are giving higher priority to direct word match */
            //			if ((!isModifierPresent && !affectedDirectWord && removeSentimentPriorityCondition) || this.sentence.split(" ").length < 4) {
            if ((!isModifierPresent && !affectedDirectWord && removeSentimentPriorityCondition) || ProcessString.countStopWords(Languages.EN, this.sentence) < 4) {
                this.removePositiveEmotions(finalEmotionsHash);
                this.removeNegativeEmotions(finalEmotionsHash);
                logger.info("Sentiment Neutral: Remove Positive Negative Emotions");

            }

            // if (!isModifierPresent && affectedDirectWord) {
            // PW.println("Priority givent to Direct Word\t" + this.sentence);
            // }
        }

        if (finalEmotionsHash.contains(EmotionLexicon.JOY_EMOTION) && finalEmotionsHash.contains(EmotionLexicon.SADNESS_EMOTION) && finalEmotionsHash.size() > 2) {
            finalEmotionsHash.remove(EmotionLexicon.JOY_EMOTION);
            finalEmotionsHash.remove(EmotionLexicon.SADNESS_EMOTION);
            finalEmotionsHash.add(EmotionLexicon.SURPRISE_EMOTION);
        }

        else if (finalEmotionsHash.contains(EmotionLexicon.SADNESS_EMOTION) && (finalEmotionsHash.contains(EmotionLexicon.ANGER_EMOTION) || finalEmotionsHash.contains(EmotionLexicon.DISGUST_EMOTION)) && finalEmotionsHash.contains(EmotionLexicon.FEAR_EMOTION)) {
            finalEmotionsHash.remove(EmotionLexicon.FEAR_EMOTION);
        }
        // if (finalEmotionsHash.contains(EmotionLexicon.ANGER_EMOTION) ||
        // finalEmotionsHash.contains(EmotionLexicon.DISGUST_EMOTION)) {
        // finalEmotionsHash.remove(EmotionLexicon.SADNESS_EMOTION);
        // }

        if (this.isConjNegCcPresent) {
            if (finalEmotionsHash.contains(EmotionLexicon.JOY_EMOTION)) {
                finalEmotionsHash.add(EmotionLexicon.SADNESS_EMOTION);
            } else if (finalEmotionsHash.contains(EmotionLexicon.SADNESS_EMOTION) || finalEmotionsHash.contains(EmotionLexicon.DISGUST_EMOTION) || finalEmotionsHash.contains(EmotionLexicon.ANGER_EMOTION)) {
                finalEmotionsHash.add(EmotionLexicon.JOY_EMOTION);
            }
        }

        if (finalEmotionsHash.size() == 0) {
            finalEmotionsHash.add(XpSentiment.NEUTRAL_SENTIMENT);
        }

    }

    // private String neutralSentiEmotionProcess(Map<String, Double>
    // finalEmotionsHash) {
    // if (!isModifierPresent || finalEmotionsHash.size() == 0)
    // return SentimentLexicon.NEUTRAL_SENTIMENT;
    //
    // if (finalEmotionsHash.containsKey(EmotionLexicon.JOY_EMOTION) &&
    // finalEmotionsHash.containsKey(EmotionLexicon.SADNESS_EMOTION))
    // return EmotionLexicon.SURPRISE_EMOTION;
    //
    // Entry<String, Double> finalEmotion = null;
    // for (Entry<String, Double> emotion : finalEmotionsHash.entrySet()) {
    // if (finalEmotion == null || finalEmotion.getValue().doubleValue() <
    // emotion.getValue().doubleValue())
    // finalEmotion = emotion;
    // }
    //
    // return finalEmotion.getKey();
    // }

    private Set<String> computeEmotionsSet() {
        Set<String> finalEmotionsHash = new HashSet<String>();
        //		boolean nonNegateFirstPersonStatus = false;
        // for (String token : this.firstPersonVerbSet) {
        // if (!this.negationTokenSet.contains(token)) {
        // nonNegateFirstPersonStatus = true;
        // break;
        // }
        // }
        //		if (nonNegateFirstPersonStatus)
        //			finalEmotionsHash.add(EmotionLexicon.JOY_EMOTION);
        //		else {
        double max = 0;
        for (int i = 0; i < this.emotionVector.length; i++) {
            if (emotionVector[i] >= max) {
                max = emotionVector[i];
            }
        }
        // if (max != 0) {
        if (max >= MIN_CUTOFF_VALUE) {
            for (int i = 0; i < emotionVector.length; i++) {
                if (emotionVector[i] == max) {
                    String emotionFromIdx = EmotionLexicon.getEmotionFromIdx(i);
                    if (!XpSentiment.NEUTRAL_SENTIMENT.equals(emotionFromIdx)) {
                        finalEmotionsHash.add(emotionFromIdx);
                    }
                }
            }
        }

        logger.info("Final EmotionHash before Applying Rules:\t" + finalEmotionsHash);
        this.applyRules(finalEmotionsHash);
        //		}

        // finalEmotionsHash.stream().forEach((emotion) -> {
        // incrementBreakDownCount(emotion);
        // });

        return finalEmotionsHash;
    }

    class HitCounterThread implements Runnable {
        String phrase;
        String searchElement;
        SEARCH_ENGINE engineName;

        public HitCounterThread(String phrase, String searchElement, SEARCH_ENGINE searchEngine) {
            this.phrase = phrase;
            this.searchElement = searchElement;
            this.engineName = searchEngine;
        }

        public void run() {
            SearchEngineHitCounter.getHitCount(phrase, searchElement, engineName);
        }
    }

    private boolean thanksHandler(String sentence) {
        //		String currentSentence = sentence.toLowerCase();
        //		int index = currentSentence.indexOf("thanks for");
        //		if (index == -1)
        //			index = currentSentence.indexOf("thanks to");
        //		if (index == -1)
        //			index = currentSentence.indexOf("thank you");
        //		if (index > -1 && index < 4)
        //			return true;
        //		return false;
        boolean answ = false;
        switch (language) {
            case EN:
                answ = EmotionLexiconEN.isThanks(sentence);
                break;
            case SP:
                answ = EmotionLexiconSP.isThanks(sentence);
                break;
        }

        return answ;
    }

    public Set<String> getEmotionList() {

        this.vectorizeSentence();

        // List<String> tokensList =
        // ProcessString.tokenizeString(sentence.toLowerCase());

        if (tokenList != null) {
            ExecutorService executor = null;
            if (XpConfig.ENABLE_BING_SEARCH) {
                int loopCount = XpConfig.SEARCH_ENGINE_MAX_SEED;
                executor = Executors.newCachedThreadPool();
                for (String concept : minedConcepts) {
                    // System.out.println("MinedConcept: " + concept);
                    Runnable worker = new HitCounterThread(concept, null, SEARCH_ENGINE.BING_API);
                    executor.execute(worker);

                    for (String emotion : EmotionLexicon.getEmotionCategories()) {
                        List<String> seedWordsList = EmotionLexicon.getEmotionSeedVector(language, emotion);

                        for (int i = 0; i < loopCount; i++) {
                            worker = new HitCounterThread(seedWordsList.get(i), null, SEARCH_ENGINE.BING_API);
                            executor.execute(worker);

                            worker = new HitCounterThread(concept, seedWordsList.get(i), SEARCH_ENGINE.BING_API);
                            executor.execute(worker);
                        }
                    }
                }
            }

            for (String token : this.tokenList) {
                // token = token.replaceAll("\\W", "");
                token = token.toLowerCase();
                if (token.matches("[0-9]+") || checkedWordHash.contains(token) || ProcessString.isStopWord(language, token) || !token.matches("\\w+") || !this.sentence.contains(token)) {
                    continue;
                }
                String tokenPOS = wordPOSmap.get(token);
                if (PosTags.isProperNoun(language, tokenPOS)) {
                    continue;
                }
                // Set<String> emotionList = this.getEmotion(token,
                // XpConfig.EMOTION_GLOVE_FLAG);
                //				Double[] emotionVec = this.getTokenEmotionVector(token);
                Double[] emotionVec = this.getTokenEmotionVector(token, tokenPOS);

                if (emotionVec != null) {
                    logger.info("Token: " + token + "\tEmotion List: " + Arrays.toString(emotionVec));
                    this.setEmotionVector(emotionVec);

                    // for (String emotion : emotionList) {
                    // this.setEmotionVector(token, emotion,
                    // XpConfig.EMOTION_GLOVE_FLAG);
                    // }
                    this.numAffectWords += 1;
                }
                checkedWordHash.add(token);
            }

            if (XpConfig.ENABLE_BING_SEARCH) {
                ThreadUtils.shutdownAndAwaitTermination(executor);
                for (String concept : minedConcepts) {
                    Double[] emotionVec = this.getConceptEmotionVector(concept);

                    if (emotionVec != null) {
                        logger.info("Concept: " + concept + "\tEmotion List: " + Arrays.toString(emotionVec));
                        this.setEmotionVector(emotionVec);
                        this.numAffectWords += 1;
                    }
                }
            }
            //			System.out.println("Emotion vector before normalization : " + this.getEmotionVectorString());
            this.normalizeEmotionVector();
            this.roundUpValues();
            logger.info("Net EmotionVector\t" + this.getEmotionVectorString());
            logger.info("Sentiment\t" + this.sentiment);
        }

        Set<String> finalEmotionsHash = this.computeEmotionsSet();

        // String emotionResult = this.sentence + "\t" + this.sentiment + "\t" +
        // finalEmotionsHash.toString();
        // NRCemotion.addToNRCresultHash(emotionResult);
        // if (this.affectedDirectWord) {
        // PW.println(this.sentence + "\t" + finalEmotionsHash);
        // }
        boolean foundEmotion = false;
        if (finalEmotionsHash.size() > 1) {
            foundEmotion = true;
        } else if (finalEmotionsHash.size() == 1) {
            for (String emotion : finalEmotionsHash) {
                if (!emotion.equals(XpSentiment.NEUTRAL_SENTIMENT))
                    foundEmotion = true;
            }
        }
        if (!foundEmotion) {
            boolean hasThanks = this.thanksHandler(sentence.toString());
            if (hasThanks) {
                if (finalEmotionsHash.size() == 1)
                    finalEmotionsHash.clear();
                finalEmotionsHash.add(EmotionLexicon.JOY_EMOTION);
            }
        }
        return finalEmotionsHash;

    }

    public static void main(String[] args) {

    }
}