/**
 * @author Alix Melchy
 * Oct 7, 2015 4:23:06 PM
 */
package  com.abzooba.xpresso.expressions.emotion.languages;

/**
 * @author Alix Melchy
 *
 */
public class EmotionLexiconSP {

    public static boolean isThanks(String sentence) {
        String currentSentence = sentence.toLowerCase();
        int index = currentSentence.indexOf("gracias");
        if (index > -1 && index < 4)
            return true;
        return false;
    }

    public static boolean isFirstPerson(String token) {
        return token.matches("(yo)|(me)|(m\u00ED)") ? true : false;

    }
}