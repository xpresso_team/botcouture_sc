/**
 *
 */
package  com.abzooba.xpresso.expressions.statementtype;

/**
 * @author Koustuv Saha
 * 03-Jul-2014 11:27:44 pm
 * XpressoV2.0.1  StatementLex
 */
public class StatementTypeLexicon {

    public static final String COMPLAINT_STATEMENT = "Complaint";
    public static final String ADVOCACY_STATEMENT = "Advocacy";
    public static final String SUGGESTION_STATEMENT = "Suggestion";
    public static final String OPINION_STATEMENT = "Opinion";

}