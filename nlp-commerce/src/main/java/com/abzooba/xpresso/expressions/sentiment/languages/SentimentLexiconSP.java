/**
 * @author Alix Melchy Jun 12, 2015 12:14:19 PM
 */
package  com.abzooba.xpresso.expressions.sentiment.languages;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.expressions.sentiment.SentimentLexicon;
import com.abzooba.xpresso.expressions.sentiment.SentimentPhraseInfo;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.utils.FileIO;

public class SentimentLexiconSP extends SentimentLexicon {

    private Set<String> nonSentiExpressions = new HashSet<String>();
    private final Pattern INTSF_SUFFIX = Pattern.compile("^(\\S*?)(ísimo|ísimos|ísima|ísimas|isimo|isimos|isima|isimas)\\b");

    public SentimentLexiconSP() {
        this.language = Languages.SP;
        try {
            FileIO.read_file(XpConfig.getNonSentiWords(language), nonSentiExpressions, false, true);
            init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* (non-Javadoc)
     * @see com.abzooba.xpresso.expressions.sentiment.SentLexicon#addSwitcher(java.util.Map)
     */
    @Override
    protected void addSwitcher(Map<String, SentimentPhraseInfo> basic_kbMap) {
        SentimentPhraseInfo phraseObj = new SentimentPhraseInfo();
        phraseObj.setLexTag(XpSentiment.SWITCHER_TAG);
        basic_kbMap.put("pero", phraseObj);
    }

    private String dropSuffixIntensifier(String str) {
        Matcher m = INTSF_SUFFIX.matcher(str);
        String lemma = str;
        Pattern hardenedC = Pattern.compile("\\b(.*)qu\\b");

        if (m.find()) {
            //	System.out.println("Group 0: " + m.group());
            //	System.out.println("Number of groups: " + m.groupCount());
            String stem = m.group(1).trim();
            Matcher m2 = hardenedC.matcher(stem);
            if (m2.find()) {
                stem = m2.group(1) + "c";
            }
            lemma = stem + "o";
        }

        return lemma;
    }

    /* (non-Javadoc)
     * @see com.abzooba.xpresso.expressions.sentiment.SentLexicon#lookUpString(java.lang.String)
     */
    protected String lookUpString(String str, String pos) {
        String stem = dropSuffixIntensifier(str);
        return super.lookUpString(stem, pos);
    }

    //	@Override
    //	protected String getLexTag(String str, Map<String, String> wordPOSmap) {
    //		str = str.toLowerCase();
    //		if (greetingsSalutationSet.contains(str)) {
    //			return SentimentLexicon.NON_SENTI_TAG;
    //		}
    //		if (nonSentiExpressions.contains(str)) {
    //			return SentimentLexicon.NON_SENTI_TAG;
    //		}
    //
    //		String posTag = (wordPOSmap == null) ? null : wordPOSmap.get(str);
    //		String sentiTag = lookUpString(str, posTag);
    //
    //		if (sentiTag == null) {
    //
    //			if (!str.contains(" ")) {
    //				if (wordPOSmap == null || (wordPOSmap != null && !("np00000").equals(wordPOSmap.get(str)))) {
    //					String spellSuggestedStr = LuceneSpell.correctSpellingWord(str);
    //					if (spellSuggestedStr != null) {
    //						sentiTag = lookUpString(spellSuggestedStr, posTag);
    //					}
    //				}
    //			}
    //		}
    //
    //		return sentiTag;
    //	}

	/* (non-Javadoc)
	 * @see com.abzooba.xpresso.expressions.sentiment.SentLexicon#isNP(java.lang.String)
	 */
    //	@Override
    //	protected boolean isNP(String tag) {
    //		return ((tag != null) && tag.startsWith("np"));
    //	}

}