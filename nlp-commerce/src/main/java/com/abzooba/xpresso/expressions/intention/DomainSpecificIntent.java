package  com.abzooba.xpresso.expressions.intention;

import java.util.Map;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.aspect.domainontology.XpOntology;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpSentenceResources;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.sdgraphtraversal.StanfordDependencyGraph;
import com.abzooba.xpresso.textanalytics.VocabularyTests;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.UniversalEnglishGrammaticalRelations;
import org.slf4j.Logger;

public class DomainSpecificIntent {

    protected static final Logger logger = XCLogger.getSpLogger();

    public static void infoSeekingIntent(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

		/*requirements for an intent object - e.g. Wanted to know the requirements of a loan*/
        //		System.out.println("infoSeekingIntent Fired");
        String govWord = gov.word();
        String depWord = dep.word();

        XpSentenceResources xto = sdg.getXpTextObject();
        //		Map<String, String> wordPOSmap = sdg.getWordPOSmap();
        //		Map<String, String> tagSentimentMap = sdg.getTagSentimentMap();
        //		Map<String, String[]> tagAspectMap = sdg.getTagAspectMap();
        Languages language = sdg.getLanguage();
        String domainName = sdg.getDomainName();
        String subject = sdg.getSubject();
        XpFeatureVector xpf = sdg.getFeatureSet();

        if (VocabularyTests.isRequireNeed(language, govWord)) {

			/*Even if the isDepIO is true because of aspect presence, it should be made false if npo. eg: help*/
            boolean isIntent = XpIntention.isIntentObjectWord(language, depWord, domainName, subject, xto);
            logger.info("IntentObject for " + dep + ": " + domainName + ":\t" + isIntent);
            boolean isInfo = (!isIntent && VocabularyTests.isInfo(language, depWord)) ? true : false;
            IndexedWord nomDep = null;
            if (isInfo) {
                Map<String, String> negatedOpinionMap = sdg.getNegatedOpinionMap();
                if (!isNegated(negatedOpinionMap, govWord.toLowerCase()))
                    return;

                nomDep = sdg.getDependencyGraph().getChildWithReln(dep, UniversalEnglishGrammaticalRelations.NOMINAL_MODIFIER);
                if (nomDep != null) {
                    String nomDepWord = nomDep.word().toLowerCase();
                    isIntent = XpIntention.isIntentObjectWord(language, nomDepWord, domainName, subject, xto);
                }
            }
            if (isIntent) {
                Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                IndexedWord intentObj = (isInfo) ? nomDep : dep;
                if (multiWordEntityMap.containsKey(intentObj)) {
                    xpf.setIntentionFeature(true, multiWordEntityMap.get(intentObj), true);
                } else {
                    xpf.setIntentionFeature(true, intentObj.toString().toLowerCase(), true);
                }
                logger.info("InfoSeekingIntent (True):\t " + gov + "\t" + dep);
            }
        } else if (VocabularyTests.isKnow(language, govWord) && (xpf.isSubjectFirstPerson() || !xpf.isSubjectSecondThirdPerson())) {
			/*I want to know about loans*/
            Map<String, String> negatedOpinionMap = sdg.getNegatedOpinionMap();
            if (!isNegated(negatedOpinionMap, govWord.toLowerCase()))
                return;

            boolean isIntent = XpIntention.isIntentObjectWord(language, depWord, domainName, subject, xto);
            logger.info("IntentObject for " + dep + ": " + domainName + ":\t" + isIntent);
            if (isIntent) {
                Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                if (multiWordEntityMap.containsKey(dep)) {
                    xpf.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                } else {
                    xpf.setIntentionFeature(true, depWord, true);
                }
                logger.info("InfoSeekingIntent (True):\t " + gov + "\t" + dep);
            }
        }
    }

    public static void intentActionCombination(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

        //		System.out.println("intentActionCombination Fired");
        String govWord = gov.word();
        String depWord = dep.word();
        XpFeatureVector xpf = sdg.getFeatureSet();
        XpSentenceResources xto = sdg.getXpTextObject();
        Languages language = sdg.getLanguage();
        String domainName = sdg.getDomainName();
        String subject = sdg.getSubject();
        //		System.out.println("is gov " + govWord + " intent action? - " + XpIntention.isIntentActionWord(language, govWord, domainName, subject, xto, false));
        //		System.out.println("is dep " + depWord + " intent action? - " + XpIntention.isIntentActionWord(language, depWord, domainName, subject, xto, false));
        if (xpf.isSubjectFirstPerson() || !xpf.isSubjectSecondThirdPerson()) {
            if (XpIntention.isIntentActionWord(language, govWord, domainName, subject, xto, isAlreadyStrong) && XpIntention.isIntentActionWord(language, depWord, domainName, subject, xto, isAlreadyStrong)) {
                IndexedWord dObj = sdg.getDependencyGraph().getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
                if (dObj != null) {
                    String dObjWord = dObj.word().toLowerCase();

                    Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                    String multiWordEntity = multiWordEntityMap.get(dObj);

                    if (multiWordEntity != null) {
                        xpf.setIntentionFeature(true, multiWordEntity, true);
                    } else {
                        xpf.setIntentionFeature(true, dObjWord, true);
                    }
                } else
                    xpf.setIntentionFeature(true, null, true);
            }
        }
    }

    public static void opinionAboutIntentObject(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

        String govWord = gov.word().toLowerCase();
        String depWord = dep.word().toLowerCase();

        //		System.out.println("opinionAboutIntentObject Fired with gov - " + govWord + " dep - " + depWord);
        //		logger.info("opinionAboutIntentObject Fired");
        XpSentenceResources xto = sdg.getXpTextObject();
        XpFeatureVector xpf = sdg.getFeatureSet();
        //		Map<String, String> wordPOSmap = xto.getWordPOSmap();
        //		Map<String, String> tagSentimentMap = xto.getTagSentimentMap();
        //		Map<String, String[]> tagAspectMap = xto.getTagAspectMap();
        Languages language = sdg.getLanguage();
        String domainName = sdg.getDomainName();
        String subject = sdg.getSubject();

        if (govWord.matches("(suggest[eds]*)|(recommend)|(advice)|(advise)") && !xpf.isSubjectFirstPerson()) {
            boolean isIntentObj = XpIntention.isIntentObjectWord(language, depWord, domainName, subject, xto);
            if (isIntentObj) {
                Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                if (multiWordEntityMap.containsKey(dep)) {
                    xpf.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                } else {
                    xpf.setIntentionFeature(true, depWord, true);
                }
                logger.info("OpinionAboutIntentObject (True):\t " + gov + "\t" + dep);
            }
        } else if (xpf.isSubjectFirstPerson() || !xpf.isSubjectSecondThirdPerson()) {
            IndexedWord auxWord = sdg.getDependencyGraph().getChildWithReln(gov, UniversalEnglishGrammaticalRelations.AUX_MODIFIER);
            if (auxWord != null && auxWord.word().matches("('d)|(would)") && govWord.matches("(like)")) {
                boolean isIntentObj = XpIntention.isIntentObjectWord(language, depWord, domainName, subject, xto);
                if (isIntentObj) {
                    Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                    if (multiWordEntityMap.containsKey(dep)) {
                        xpf.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                    } else {
                        xpf.setIntentionFeature(true, depWord, true);
                    }
                    logger.info("OpinionAboutIntentObject (True):\t " + gov + "\t" + dep);
                } else {
                    IndexedWord dObjWord = sdg.getDependencyGraph().getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
                    if (dObjWord != null) {
                        isIntentObj = XpIntention.isIntentObjectWord(language, dObjWord.value(), domainName, subject, xto);
                        if (isIntentObj) {
                            Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                            if (multiWordEntityMap.containsKey(dObjWord)) {
                                xpf.setIntentionFeature(true, multiWordEntityMap.get(dObjWord), true);
                            } else {
                                xpf.setIntentionFeature(true, dObjWord.value(), true);
                            }
                            logger.info("OpinionAboutIntentObject (True):\t " + gov + "\t" + dep);
                        }
                    }
                }
            } else if (govWord.matches("(look[ing]*)") && depWord.matches("forward")) {
                //				System.out.println("Entered with gov - " + govWord + " dep - " + depWord);
                IndexedWord dObj = sdg.getDependencyGraph().getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOMINAL_MODIFIER);
                //				System.out.println("dobj - " + dObj);
                if (dObj != null && XpIntention.isIntentObjectWord(language, dObj.word().toLowerCase(), domainName, subject, xto)) {
                    String dObjWord = dObj.word();
                    Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                    String multiWordWord = multiWordEntityMap.get(dObj);
                    if (multiWordWord != null) {
                        xpf.setIntentionFeature(true, multiWordWord, true);
                    } else {
                        xpf.setIntentionFeature(true, dObjWord, true);
                    }
                    logger.info("OpinionAboutIntentObject (True):\t " + gov + "\t" + dep);
                }
            }
        }
    }

    public static void questionBasedInfoSeeking(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

        //		System.out.println("questionBasedInfoSeeking Fired");
        XpSentenceResources xto = sdg.getXpTextObject();
        XpFeatureVector xpf = sdg.getFeatureSet();
        //		System.out.println("is the sentence question - " + xpf.isQuestionFeature());
        if (xpf.isQuestionFeature() && (xpf.isSubjectFirstPerson() || !xpf.isSubjectSecondThirdPerson())) {
            String govWord = gov.word();
            String depWord = dep.word();
            Languages language = sdg.getLanguage();
            String domainName = sdg.getDomainName();
            String subject = sdg.getSubject();
            //			System.out.println("gov word - " + govWord + " dep word - " + depWord);
            if ((xpf.isSubjectFirstPerson() || !xpf.isSubjectSecondThirdPerson()) && VocabularyTests.isBuy(language, govWord) && XpIntention.isIntentObjectWord(language, depWord, domainName, subject, xto)) {
                Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                if (multiWordEntityMap.containsKey(dep)) {
                    xpf.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                } else {
                    xpf.setIntentionFeature(true, depWord, true);
                }
                logger.info("questionBasedInfoSeeking (True):\t " + gov + "\t" + dep);
            }
        }
    }

    public static void autoLoanIntentDetection(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

        //		System.out.println("autoLoanIntentDetection Fired");
        String govWord = gov.word();
        String depWord = dep.word();
        Languages language = sdg.getLanguage();
        String domainName = sdg.getDomainName();
        String subject = sdg.getSubject();
        XpSentenceResources xto = sdg.getXpTextObject();
        XpFeatureVector xpf = sdg.getFeatureSet();

        //		System.out.println("autoLoanIntentDetection Fired with gov - " + govWord + " and dep - " + depWord);

        if (XpIntention.isIntentActionWord(language, govWord, domainName, subject, xto, isAlreadyStrong)) {
            //			String rawCategory = XpOntology.getRawOntologyCategory(language, depWord);
            //			if (rawCategory != null && rawCategory.matches("(CAR)|(CAR MODEL)|(CAR TYPE)|(CAR COMPANY)")) {
            //			System.out.println(govWord + " is intent action ");
            if (isEntityOfCategoryDesired(language, depWord, "(CAR)|(CAR MODEL)|(CAR TYPE)|(CAR COMPANY)")) {
                xpf.setIntentionFeature(true, "auto loan", true);
                logger.info("autoLoanIntentDetection (True):\t " + gov + "\t" + dep);
            } else {
                IndexedWord dObjWord = sdg.getDependencyGraph().getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
                if (dObjWord != null && isEntityOfCategoryDesired(language, dObjWord.word(), "(CAR)|(CAR MODEL)|(CAR TYPE)|(CAR COMPANY)")) {
                    //					rawCategory = XpOntology.getRawOntologyCategory(language, dObjWord.word());
                    //					if (rawCategory != null && rawCategory.matches("(CAR)|(CAR MODEL)|(CAR TYPE)|(CAR COMPANY)")) {
                    xpf.setIntentionFeature(true, "auto loan", true);
                    logger.info("autoLoanIntentDetection (True):\t " + gov + "\t" + dep);
                    //				}
                }

            }
        } else if ((xpf.isSubjectFirstPerson() || !xpf.isSubjectSecondThirdPerson()) && xpf.isActionInFuture()) {
            //			System.out.println("Entered futuristic purchase");
            if ((XpIntention.isNormalIntentAction(language, govWord, domainName, subject, xto) || VocabularyTests.isSellBuy(language, govWord)) && isEntityOfCategoryDesired(language, depWord, "(CAR)|(CAR MODEL)|(CAR TYPE)|(CAR COMPANY)")) {
                xpf.setIntentionFeature(true, "auto loan", true);
                logger.info("autoLoanIntentDetection (True):\t " + gov + "\t" + dep);
            } else if (XpIntention.isNormalIntentAction(language, depWord, domainName, subject, xto) || VocabularyTests.isSellBuy(language, depWord)) {
                IndexedWord dObjWord = sdg.getDependencyGraph().getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
                if (dObjWord != null && isEntityOfCategoryDesired(language, dObjWord.word(), "(CAR)|(CAR MODEL)|(CAR TYPE)|(CAR COMPANY)")) {
                    xpf.setIntentionFeature(true, "auto loan", true);
                    logger.info("autoLoanIntentDetection (True):\t " + gov + "\t" + dep);
                }
            }
        } else if (xpf.isQuestionFeature() && (xpf.isSubjectFirstPerson() || !xpf.isSubjectSecondThirdPerson())) {
            //			String rawCategory = XpOntology.getRawOntologyCategory(language, depWord);
            //			if (rawCategory != null && rawCategory.matches("(CAR)|(CAR MODEL)|(CAR TYPE)|(CAR COMPANY)")) {
            if (isEntityOfCategoryDesired(language, depWord, "(CAR)|(CAR MODEL)|(CAR TYPE)|(CAR COMPANY)")) {
                if (VocabularyTests.isRecommendSuggest(language, govWord) && xpf.isSubjectSecondThirdPerson()) {
                    xpf.setIntentionFeature(true, "auto loan", true);
                    logger.info("autoLoanIntentDetection (True):\t " + gov + "\t" + dep);
                } else if (VocabularyTests.isBuy(language, govWord) && xpf.isSubjectFirstPerson()) {
                    xpf.setIntentionFeature(true, "auto loan", true);
                    logger.info("autoLoanIntentDetection (True):\t " + gov + "\t" + dep);
                }
            }
        }
    }

    public static void houseLoanIntentDetection(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

        //		System.out.println("houseLoanIntentDetection Fired");
        String govWord = gov.word();
        String depWord = dep.word();
        Languages language = sdg.getLanguage();
        String domainName = sdg.getDomainName();
        String subject = sdg.getSubject();
        XpSentenceResources xto = sdg.getXpTextObject();
        XpFeatureVector xpf = sdg.getFeatureSet();

        //		System.out.println("houseLoanIntentDetection Fired with gov - " + govWord + " and dep - " + depWord);

        if (XpIntention.isIntentActionWord(language, govWord, domainName, subject, xto, isAlreadyStrong)) {
            //			String rawCategory = XpOntology.getRawOntologyCategory(language, depWord);
            //			if (rawCategory != null && rawCategory.matches("(CAR)|(CAR MODEL)|(CAR TYPE)|(CAR COMPANY)")) {
            //			System.out.println(govWord + " is intent action ");
            if (VocabularyTests.isHouse(language, depWord)) {
                xpf.setIntentionFeature(true, "house loan", true);
                logger.info("houseLoanIntentDetection (True):\t " + gov + "\t" + dep);
            } else {
                IndexedWord dObjWord = sdg.getDependencyGraph().getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
                if (dObjWord != null && VocabularyTests.isHouse(language, dObjWord.word())) {
                    //					rawCategory = XpOntology.getRawOntologyCategory(language, dObjWord.word());
                    //					if (rawCategory != null && rawCategory.matches("(CAR)|(CAR MODEL)|(CAR TYPE)|(CAR COMPANY)")) {
                    xpf.setIntentionFeature(true, "house loan", true);
                    logger.info("houseLoanIntentDetection (True):\t " + gov + "\t" + dep);
                    //				}
                }

            }
        } else if ((xpf.isSubjectFirstPerson() || !xpf.isSubjectSecondThirdPerson()) && xpf.isActionInFuture()) {
            //			System.out.println("Entered futuristic purchase");
            if ((XpIntention.isNormalIntentAction(language, govWord, domainName, subject, xto) || VocabularyTests.isSellBuy(language, govWord)) && VocabularyTests.isHouse(language, depWord)) {
                xpf.setIntentionFeature(true, "house loan", true);
                logger.info("houseLoanIntentDetection (True):\t " + gov + "\t" + dep);
            } else if (XpIntention.isNormalIntentAction(language, depWord, domainName, subject, xto) || VocabularyTests.isSellBuy(language, depWord)) {
                IndexedWord dObjWord = sdg.getDependencyGraph().getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
                if (dObjWord != null && VocabularyTests.isHouse(language, dObjWord.word())) {
                    xpf.setIntentionFeature(true, "house loan", true);
                    logger.info("houseLoanIntentDetection (True):\t " + gov + "\t" + dep);
                }
            }
        } else if (xpf.isQuestionFeature()) {
            //			String rawCategory = XpOntology.getRawOntologyCategory(language, depWord);
            //			if (rawCategory != null && rawCategory.matches("(CAR)|(CAR MODEL)|(CAR TYPE)|(CAR COMPANY)")) {
            if (VocabularyTests.isHouse(language, depWord)) {
                if (VocabularyTests.isRecommendSuggest(language, govWord) && xpf.isSubjectSecondThirdPerson()) {
                    xpf.setIntentionFeature(true, "house loan", true);
                    logger.info("houseLoanIntentDetection (True):\t " + gov + "\t" + dep);
                } else if (VocabularyTests.isBuy(language, govWord) && xpf.isSubjectFirstPerson()) {
                    xpf.setIntentionFeature(true, "house loan", true);
                    logger.info("houseLoanIntentDetection (True):\t " + gov + "\t" + dep);
                }
            }
        }
    }

    public static void personalLoanIntentDetection(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

        //		System.out.println("personalLoanIntentDetection Fired");
        String govWord = gov.word();
        String depWord = dep.word();
        //		Languages language = sdg.getLanguage();
        //		String domainName = sdg.getDomainName();
        //		String subject = sdg.getSubject();
        Map<String, String> nerMap = sdg.getXpTextObject().getNerMap();
        XpFeatureVector xpf = sdg.getFeatureSet();
        Languages language = sdg.getLanguage();
        //		System.out.println("personalLoanIntentDetection fired with gov - " + govWord + " dep - " + depWord);
        if ((xpf.isSubjectFirstPerson() || !xpf.isSubjectSecondThirdPerson())) {
            if (xpf.isActionInFuture()) {
                //	if (govWord.matches("(renovate)") && VocabularyTests.isHouse(language, depWord)) {
                if (VocabularyTests.isRenovate(language, govWord) && VocabularyTests.isHouse(language, depWord)) {
                    xpf.setIntentionFeature(true, "personal loan", true);
                    logger.info("personalLoanIntentDetection (True):\t " + gov + "\t" + dep);
                    //	} else if (depWord.matches("(renovate)")) {
                } else if (VocabularyTests.isRenovate(language, depWord)) {
                    IndexedWord dObjWord = sdg.getDependencyGraph().getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
                    if (dObjWord != null && VocabularyTests.isHouse(language, dObjWord.word())) {
                        xpf.setIntentionFeature(true, "personal loan", true);
                        logger.info("personalLoanIntentDetection (True):\t " + gov + "\t" + dep);
                    }
                    //	} else if (govWord.matches("(move)|(shift)|(settle)")) {
                } else if (VocabularyTests.isMove(language, govWord)) {
                    String nerTag = nerMap.get(depWord);
                    if (nerTag != null && nerTag.equalsIgnoreCase("location")) {
                        xpf.setIntentionFeature(true, "personal loan", true);
                        logger.info("personalLoanIntentDetection (True):\t " + gov + "\t" + dep);
                    }
                }
            } else {
                //	if (govWord.matches("(renovating)") && VocabularyTests.isHouse(language, depWord)) {
                if (VocabularyTests.isRenovate(language, govWord) && VocabularyTests.isHouse(language, depWord)) {
                    xpf.setIntentionFeature(true, "personal loan", true);
                    logger.info("personalLoanIntentDetection (True):\t " + gov + "\t" + dep);
                    //	} else if (depWord.matches("(renovating)")) {
                } else if (VocabularyTests.isRenovate(language, depWord)) {
                    IndexedWord dObjWord = sdg.getDependencyGraph().getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
                    if (dObjWord != null && VocabularyTests.isHouse(language, dObjWord.word())) {
                        xpf.setIntentionFeature(true, "personal loan", true);
                        logger.info("personalLoanIntentDetection (True):\t " + gov + "\t" + dep);
                    }
                    //	} else if (govWord.matches("(moving)|(shifting)")) {
                } else if (VocabularyTests.isMove(language, govWord)) {
                    String nerTag = nerMap.get(depWord);
                    if (nerTag != null && nerTag.equalsIgnoreCase("location")) {
                        xpf.setIntentionFeature(true, "personal loan", true);
                        logger.info("personalLoanIntentDetection (True):\t " + gov + "\t" + dep);
                    }
                }
                //				else if (depWord.matches("(renovat[inge)")) {
                //
                //				}
            }
        }
    }

    public static void studentLoanIntentDetection(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

        String govWord = gov.word();
        String depWord = dep.word();
        //		Languages language = sdg.getLanguage();
        //		String domainName = sdg.getDomainName();
        //		String subject = sdg.getSubject();
        //		XpTextObject xto = sdg.getXpTextObject();
        XpFeatureVector xpf = sdg.getFeatureSet();
        Languages language = sdg.getLanguage();
        //		System.out.println("studentLoanIntentDetection fired with gov - " + govWord + " dep - " + depWord);
        if ((xpf.isSubjectFirstPerson() || !xpf.isSubjectSecondThirdPerson())) {
            if (VocabularyTests.isJoinStart(language, govWord) && VocabularyTests.isCollege(language, depWord)) {
                xpf.setIntentionFeature(true, "student loan", true);
                logger.info("studentLoanIntentDetection (True):\t " + gov + "\t" + dep);
            } else if (VocabularyTests.isJoinStart(language, depWord)) {
                IndexedWord dObjWord = sdg.getDependencyGraph().getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
                if (dObjWord != null && VocabularyTests.isCollege(language, dObjWord.word())) {
                    xpf.setIntentionFeature(true, "student loan", true);
                    logger.info("studentLoanIntentDetection (True):\t " + gov + "\t" + dep);
                }
            }
        }
    }

    public static void intentObjectDeclineDetection(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

        String govWord = gov.word().toLowerCase();
        String depWord = dep.word().toLowerCase();
        Languages language = sdg.getLanguage();
        String domainName = sdg.getDomainName();
        String subject = sdg.getSubject();
        XpSentenceResources xto = sdg.getXpTextObject();
        XpFeatureVector xpf = sdg.getFeatureSet();
        Map<String, String> negatedOpinionMap = sdg.getNegatedOpinionMap();
        //		System.out.println("intentObjectDeclineDetection fired with gov - " + govWord + " dep - " + depWord);
        if (VocabularyTests.isDecline(language, govWord) && XpIntention.isIntentObjectWord(language, depWord, domainName, subject, xto)) {
            if (!negatedOpinionMap.containsKey(govWord)) {
                Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                if (multiWordEntityMap.containsKey(dep)) {
                    xpf.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                } else {
                    xpf.setIntentionFeature(true, depWord, true);
                }
                logger.info("intentObjectDeclineDetection (True):\t " + gov + "\t" + dep);
            }
        } else if (VocabularyTests.isApprove(language, govWord) && XpIntention.isIntentObjectWord(language, depWord, domainName, subject, xto)) {
            if (negatedOpinionMap.containsKey(govWord)) {
                Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                if (multiWordEntityMap.containsKey(dep)) {
                    xpf.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                } else {
                    xpf.setIntentionFeature(true, depWord, true);
                }
                logger.info("intentObjectDeclineDetection (True):\t " + gov + "\t" + dep);
            }
        }
    }

    public static void intentObjectComparisonDetection(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg, boolean isAlreadyStrong) {

        //		String govWord = gov.word().toLowerCase();
        String depWord = dep.word().toLowerCase();
        String govPOS = gov.tag();
        Languages language = sdg.getLanguage();
        String domainName = sdg.getDomainName();
        String subject = sdg.getSubject();
        XpSentenceResources xto = sdg.getXpTextObject();
        XpFeatureVector xpf = sdg.getFeatureSet();
        //		Map<String, String> negatedOpinionMap = sdg.getNegatedOpinionMap();
        //		System.out.println("intentObjectComparisonDetection fired with gov - " + govWord + " dep - " + depWord);
        if (xpf.isQuestionFeature() && XpIntention.isIntentObjectWord(language, depWord, domainName, subject, xto)) {
            if (govPOS.matches("(JJR)|(RBR)")) {
                Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                if (multiWordEntityMap.containsKey(dep)) {
                    xpf.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                } else {
                    xpf.setIntentionFeature(true, depWord, true);
                }
                logger.info("intentObjectComparisonDetection (True):\t " + gov + "\t" + dep);
            } else {
                IndexedWord opinionObj = sdg.getDependencyGraph().getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
                if (opinionObj != null && opinionObj.tag().matches("(JJ[RS]*)|(RB[RS]*)")) {
                    Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                    if (multiWordEntityMap.containsKey(dep)) {
                        xpf.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                    } else {
                        xpf.setIntentionFeature(true, depWord, true);
                    }
                    logger.info("intentObjectComparisonDetection (True):\t " + gov + "\t" + dep);
                } else {
                    opinionObj = sdg.getDependencyGraph().getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT);
                    if (opinionObj != null && opinionObj.tag().matches("(JJ[RS]*)|(RB[RS]*)")) {
                        Map<IndexedWord, String> multiWordEntityMap = sdg.getMultiWordEntityMap();
                        if (multiWordEntityMap.containsKey(dep)) {
                            xpf.setIntentionFeature(true, multiWordEntityMap.get(dep), true);
                        } else {
                            xpf.setIntentionFeature(true, depWord, true);
                        }
                        logger.info("intentObjectComparisonDetection (True):\t " + gov + "\t" + dep);
                    }
                }
            }
        }
    }

    private static boolean isEntityOfCategoryDesired(Languages language, String entity, String desiredCategoryRegEx) {
        if (entity == null || desiredCategoryRegEx == null)
            return false;
        String rawCategory = XpOntology.getRawOntologyCategory(language, entity);
        //		System.out.println("inside isEntityOfCategoryDesired entity - " + entity + " raw category - " + rawCategory);
        if (rawCategory != null && rawCategory.matches(desiredCategoryRegEx))
            return true;
        return false;
    }

    private static boolean isNegated(Map<String, String> negatedOpinionMap, String word) {
        if (negatedOpinionMap != null && negatedOpinionMap.containsKey(word)) {
            //			logger.info("IntentionClassifier: verb " + word + " is negated");
            return true;
        }
        return false;
    }

//	private static boolean isHouse(String entity) {
//		if (entity == null)
//			return false;
//		boolean isEntityHouse = entity.toLowerCase().matches("(house)|(apartment)");
//		return isEntityHouse;
//	}

//	private static boolean isCollege(String entity) {
//		if (entity == null)
//			return false;
//		boolean isEntityHouse = entity.toLowerCase().matches("(college)|(university)|(school)");
//		return isEntityHouse;
//	}

}