/**
 *
 */
package  com.abzooba.xpresso.expressions.intention;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Koustuv Saha
 * Aug 10, 2015 3:02:27 PM
 * XpressoV3.0  IntentPhraseInfo
 */
public class IntentPhraseInfo {

    String intentCategory;
    Set<String> domainApplicable;

    public IntentPhraseInfo(String[] intentTuple) {

        if (intentTuple.length >= 3) {
            this.intentCategory = intentTuple[2];
            if (intentTuple.length > 3) {
                domainApplicable = new HashSet<String>();
                for (int i = 3; i < intentTuple.length; i++) {
                    domainApplicable.add(intentTuple[i]);
                }
            }
        }
    }

    public String getIntentCategory(String domainName) {
        if (domainApplicable == null || (domainApplicable != null && domainApplicable.contains(domainName))) {
            return this.intentCategory;
        }
        return null;
    }

}