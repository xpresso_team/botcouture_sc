/**
 *
 */
package com.abzooba.xpresso.expressions.sentiment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.aspect.domainontology.XpOntology;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.engine.core.XpSentenceResources;
import com.abzooba.xpresso.expressions.intention.IntentEngine;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.sdgraphtraversal.StanfordDependencyGraph;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.PosTags;
import com.abzooba.xpresso.textanalytics.ProcessString;
import com.abzooba.xpresso.textanalytics.VocabularyTests;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.UniversalEnglishGrammaticalRelations;
import org.slf4j.Logger;

/**
 * @author Koustuv Saha
 * 14-Mar-2014 10:42:37 am
 * SentimentAnalysis-RnD SentimentEngine
 */
public class SentimentEngine {

    // TODO if useful the following has to be language agnostic
    // public static String shiftSplitPat = " but|whereas ";

    protected static final Logger logger = XCLogger.getSpLogger();

    public static String preProcessString(String str) {
        str = str.toLowerCase().replaceAll(",", " ");
        str = str.replaceAll("\\s+", " ");
        // System.out.println(str);
        //		str = CoreNLPController.lemmatizeToString(str);
        return str;
    }

    //	public static String sentimentEvaluation(String str, XpFeatureSet featureSet) {
    //		str = preProcessString(str);
    //		String sentiment = SentiParse.rulesPipeline(str, featureSet);
    //		//		String sentiment = SentimentRulesFeatureBased.rulesPipeline(str);
    //		return sentiment;
    //	}

    public static void auxilliarySentimentClassifier(Languages language, IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, SemanticGraph dependencyGraph, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, XpFeatureVector xpf) {

        String govWord = gov.value().toLowerCase();
        String depWord = dep.value();
        boolean isCouldCondition = false;
		/*They can be better*/
        if (VocabularyTests.isCan(language, depWord)) {
            // if (depWord.equalsIgnoreCase("can")) {
            IndexedWord govCopulaDep = dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.COPULA);
            if (govCopulaDep != null && govCopulaDep.word().equalsIgnoreCase("be")) {
                isCouldCondition = true;
            }
        }

        if (VocabularyTests.isCould(language, depWord) || isCouldCondition) {
            // if (depWord.equalsIgnoreCase("could") || isCouldCondition) {
            // String govWord = v1.toLowerCase();
            String govTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);

			/*The staff could not be better. Sometimes when "." exists the better is tagged as RBR*/
            boolean comparativeAdjectiveCondition = PosTags.isComparativeAdjective(language, gov.tag()) || PosTags.isComparativeAdverb(language, gov.tag());
            // boolean comparativeAdjectiveCondition = gov.tag().equals("JJR") || gov.tag().equals("RBR");

			/*The staff could not have been more helpful*/
            IndexedWord advmodDep = dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
            boolean comparativeAdverbCondition = advmodDep != null && PosTags.isAdverb(language, advmodDep.tag());
            // boolean comparativeAdverbCondition = advmodDep != null && advmodDep.tag().contains("RB");

            //			boolean comparativeAdverbCondition = advmodDep != null && advmodDep.tag().equals("RBR");

            if (SentimentFunctions.isPolarTag(govTag) && (comparativeAdjectiveCondition || comparativeAdverbCondition)) {

                //			if (SentimentFunctions.isPolarTag(govTag) && (gov.tag().equals("JJR")) {
                if (XpSentiment.POSITIVE_TAG.equals(govTag) || XpSentiment.UNCONDITIONAL_POSITIVE_TAG.equals(govTag)) {
                    xpf.setCouldPosFeature(true);
					/*This could be better*/
                    xpf.setSuggestionPresentFeature(true);
                } else if (XpSentiment.NEGATIVE_TAG.equals(govTag) || XpSentiment.UNCONDITIONAL_NEGATIVE_TAG.equals(govTag)) {
                    xpf.setCouldNegFeature(true);
                }

                String govSentiTag = SentimentFunctions.switchSentimentTag(govTag);
                logger.info("Aux: Fired Could: " + govWord + " " + govSentiTag);
                tagSentimentMap.put(govWord, govSentiTag);
            }
        } else if (VocabularyTests.isShould(language, depWord) || VocabularyTests.isMust(language, depWord)) {
            // } else if (depWord.equalsIgnoreCase("should") || depWord.equalsIgnoreCase("must")) {
			/*They should improve. They should make a note of this. It is suggestion. Incase not a sentiment word follows then Neutral, Else it is negative. Incase no sentiment but is negated, then too it is a negative sentiment*/

            String govTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
            String sentiment = null;
            boolean isShouldFeature = false;

            //			if (SentimentFunctions.isPolarTag(govTag) || dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NEGATION_MODIFIER) != null) {
            if (SentimentFunctions.isPolarTag(govTag) || !dependencyGraph.isNegatedVertex(gov)) {
                isShouldFeature = true;
            } else if (dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER) != null) {
                isShouldFeature = true;
            } else {
				/*They should have better staff | They should have more staff*/
                IndexedWord dobjDep = dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);

                if (dobjDep != null) {
                    if ((dependencyGraph.getChildWithReln(dobjDep, UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER) != null) || (dependencyGraph.getChildWithReln(dobjDep, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER) != null)) {
                        isShouldFeature = true;
                    }
                }
            }
            logger.info("Aux: Fired Should: " + govWord + " " + sentiment);
            if (isShouldFeature) {
                sentiment = XpSentiment.NEGATIVE_SENTIMENT;
                xpf.setShouldNegFeature(true);
                xpf.setSuggestionPresentFeature(true);
            } else {
                sentiment = XpSentiment.NEUTRAL_SENTIMENT;
            }

            tagSentimentMap.put(XpSentiment.NEED_ID, sentiment);
        }
        // TODO in Spanish no 'would' but a conditional mood.
		/*I would suggest/advise kind of sentences*/
        else if (depWord.equalsIgnoreCase("would")) {
            String govTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);

			/*The staff could not be better*/
            boolean comparativeAdjectiveCondition = gov.tag().equals("JJR");

			/*The staff could not have been more helpful*/
            IndexedWord advmodDep = dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
            boolean comparativeAdverbCondition = advmodDep != null && advmodDep.tag().equals("RBR");

            if (SentimentFunctions.isPolarTag(govTag)) {
                if (comparativeAdjectiveCondition || comparativeAdverbCondition) {

                    //			if (SentimentFunctions.isPolarTag(govTag) && (gov.tag().equals("JJR")) {
                    //				if (SentimentLexicon.POSITIVE_TAG.equals(govTag) || SentimentLexicon.UNCONDITIONAL_POSITIVE_TAG.equals(govTag)) {
                    //					xpf.setCouldPosFeature(true);
                    //					/*This could be better*/
                    //					xpf.setSuggestionPresentFeature(true);
                    //				} else if (SentimentLexicon.NEGATIVE_TAG.equals(govTag) || SentimentLexicon.UNCONDITIONAL_NEGATIVE_TAG.equals(govTag)) {
                    //					xpf.setCouldNegFeature(true);
                    //				}

                    //				String govSentiTag = SentimentFunctions.switchSentimentTag(govTag);
                    logger.info("Aux: Fired Would (Case1): " + govWord);
                    tagSentimentMap.put(govWord, XpSentiment.NON_SENTI_TAG);
                } else if (gov.tag().equals("JJ")) {
                    logger.info("Aux: Fired Would (Case2): " + govWord);
                    tagSentimentMap.put(govWord, SentimentFunctions.switchSentimentTag(govWord));
                }
            }
            //			else if (gov.tag().matches("VB[A-Z]*")) {
            //				List<IndexedWord> ifNodes = dependencyGraph.getAllNodesByWordPattern("if");
            //				ifNodes.forEach(ifNode -> {
            //
            //				});
            //			}

        }
    }

    public static void markerSentimentClassifier(Languages language, IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, SemanticGraph dependencyGraph, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, XpFeatureVector xpf) {

        String depWord = dep.value().toLowerCase();
        String depSentiTag = XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap);

        if (VocabularyTests.isIf(language, depWord)) {
            String govWord = gov.value().toLowerCase();
            String govTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);

			/*If the service was better, If the service was more good*/
            if (SentimentFunctions.isPolarTag(govTag)) {

                IndexedWord advmodDep = dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
                boolean comparativeAdverbCondition = advmodDep != null && PosTags.isComparativeAdverb(language, advmodDep.tag());

                if ((PosTags.isComparativeAdjective(language, gov.tag()) || comparativeAdverbCondition)) {
                    tagSentimentMap.put(govWord, XpSentiment.NON_SENTI_TAG);
                    logger.info("Marker: Fired If: " + govWord);
                }
            } else {
                IndexedWord xcompDep = dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT);
                if (xcompDep != null) {
					/*If this is going to improve*/
                    String xcompDepWord = xcompDep.word().toLowerCase();
                    if (SentimentFunctions.isPolarFlexibleTag(XpSentiment.getLexTag(language, xcompDep.word(), wordPOSmap, tagSentimentMap))) {
                        tagSentimentMap.put(xcompDepWord, XpSentiment.NON_SENTI_TAG);
                        logger.info("Marker: Fired if, xcomp:" + xcompDepWord);
                    }
                }
            }
        } else if (XpSentiment.SHIFTER_TAG.equals(depSentiTag)) {
			/*No I don't like -> In this case, the first No is not a shifter*/
            tagSentimentMap.put(depWord, XpSentiment.NON_SENTI_TAG);
            logger.info("Marker: Fired Nullifying Dependent Shifter:\t" + depWord);
        }
    }

    //	public static void nsubjBasedSentimentClassifier(Languages language, IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, SemanticGraph dependencyGraph, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, XpFeatureVector xpf, String domainName) {
    public static void nsubjBasedSentimentClassifier(Languages language, IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg) {

        XpSentenceResources xto = sdg.getXpTextObject();

        Map<String, String> tagSentimentMap = xto.getTagSentimentMap();
        XpFeatureVector xpf = sdg.getFeatureSet();
        SemanticGraph dependencyGraph = sdg.getDependencyGraph();
        Map<String, String> wordPOSmap = xto.getWordPOSmap();

        String govWord = gov.word().toLowerCase();
        String depWord = dep.word().toLowerCase();

        String depDirectAspect = XpOntology.getOntologyCategory(language, sdg.getDomainName(), depWord, xto);
        logger.info("Direct Aspect: " + depDirectAspect);
        if (depDirectAspect != null && depDirectAspect.equals("STAFF") && (govWord.equals("interesting"))) {
			/*interesting classifier*/
            tagSentimentMap.put("interesting", XpSentiment.NEGATIVE_TAG);
        } else if (VocabularyTests.isShifterSubject(language, depWord)) {
			/*Nobody loves the place*/
            logger.info("Nsubj Based ShiftedPronoun (Shifter Nullified):\t" + depWord);
            tagSentimentMap.put(depWord, XpSentiment.NON_SENTI_TAG);
            xpf.setNsubjShiftPronounSubjectFeature(true);
        } else if (depWord.equals("one")) {
            //			System.out.println("I am here");
            IndexedWord negDep = dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.NEGATION_MODIFIER);
            if (negDep != null) {
                logger.info("Nsubj Based ShiftedPronoun: (No one) (Shifter Nullified):\t" + depWord + "\t" + negDep);
                tagSentimentMap.put(negDep.word(), XpSentiment.NON_SENTI_TAG);
                tagSentimentMap.put("no one", XpSentiment.NON_SENTI_TAG);
                xpf.setNsubjShiftPronounSubjectFeature(true);
            }
        } else {
            String govSentiTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
            if (XpSentiment.NEED_TAG.equals(govSentiTag) || VocabularyTests.isRare(language, govWord)) {
				/*rare classifier*/
				/*Good restaurants are needed. Good Restaurants are rare*/
                IndexedWord amodDep = dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER);
                if (amodDep != null) {
                    String amodDepWord = amodDep.value().toLowerCase();
                    String amodDepTag = XpSentiment.getLexTag(language, amodDepWord, wordPOSmap, tagSentimentMap);
                    if (SentimentFunctions.isPolarTag(amodDepTag)) {
                        tagSentimentMap.put(amodDepWord, SentimentFunctions.switchSentimentTag(amodDepTag));
                        logger.info("NsubjBased: Fired Rare Based Sentiment: " + amodDepWord);
                    }
                }
            } else if (XpSentiment.NON_POSSESSION_TAG.equals(govSentiTag)) {
				/*Crisis was avoided*/
                String depSentiTag = XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap);
                if (SentimentFunctions.isPolarTag(depSentiTag)) {
                    tagSentimentMap.put(depWord, SentimentFunctions.switchSentimentTag(depSentiTag));
                    logger.info("NsubjBased: NPOSSN on Sentiment (NSUBJ): " + depWord + "\t" + govWord);
                }
            }
        }
    }

    //	public static String needClassifier(Languages language, IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, SemanticGraph dependencyGraph, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, XpFeatureVector xpf, String subject, Map<String, String> negatedOpinionHash) {

    public static String needClassifier(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg) {

        String returnSentiment = null;
        String govWord = gov.word().toLowerCase();
        //		System.out.println("Need Classifier Called with : " + govWord + "::::" + dep.value().toLowerCase());
        XpFeatureVector xpf = sdg.getFeatureSet();
        SemanticGraph dependencyGraph = sdg.getDependencyGraph();
        Languages language = sdg.getLanguage();
        XpSentenceResources xto = sdg.getXpTextObject();
        Map<String, String> wordPOSmap = xto.getWordPOSmap();
        Map<String, String> tagSentimentMap = xto.getTagSentimentMap();
        String subject = sdg.getSubject();
        Map<String, String> negatedOpinionMap = sdg.getNegatedOpinionMap();

        if (XpSentiment.NEED_TAG.equals(XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap))) {
            String depWord = dep.value().toLowerCase();
            logger.info("Fired needClassifier: " + govWord + " " + depWord);
            xpf.setNeedFeature(true);
			/*Incase the first person exists, like We need more information, these kind of things should be neutral. But for cases like, I hope that they improve, this should be like normal needClassification*/
            IndexedWord nsubjDep = dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT);
            if (nsubjDep != null && !VocabularyTests.isHope(language, govWord)) {
                String nsubjDepWord = nsubjDep.word().toLowerCase();
				/*This case should be excluded for: I wish to close my account | The counter example is I wish to open an account. So better it is neutral*/
                //				if ((nsubjDepWord.equalsIgnoreCase("we") || nsubjDepWord.equalsIgnoreCase("i")) && !govWord.equals("wish")) {
                if (VocabularyTests.isFirstPersonSubject(language, nsubjDepWord)) {
                    returnSentiment = XpSentiment.NEUTRAL_SENTIMENT;
                    tagSentimentMap.put(XpSentiment.NEED_ID, returnSentiment);
                    logger.info("(NEED) First Person Case: Sentiment Neutralized: " + govWord + " " + depWord);
                    return returnSentiment;
                }

				/*To ignore cases like, My child needs the toy*/
                IndexedWord possDep = dependencyGraph.getChildWithReln(nsubjDep, UniversalEnglishGrammaticalRelations.POSSESSION_MODIFIER);

                if (possDep != null) {
                    String possDepWord = possDep.word();
                    if (VocabularyTests.isFirstPersonPossessive(language, possDepWord)) {
                        returnSentiment = XpSentiment.NEUTRAL_SENTIMENT;
                        tagSentimentMap.put(XpSentiment.NEED_ID, returnSentiment);
                        logger.info("(NEED) First Person (Possessive) Case: Sentiment Neutralized: " + govWord + " " + depWord);
                        return returnSentiment;
                    }
                }
            }

			/*If it is hope with a verb, it should be made neutral: Hope your son enjoyed it. Hope they improve.*/
            if (VocabularyTests.isHope(language, govWord) && PosTags.isMainVerb(language, dep.tag())) {
                returnSentiment = XpSentiment.NEUTRAL_SENTIMENT;
                tagSentimentMap.put(XpSentiment.NEED_ID, returnSentiment);
                logger.info("(NEED) First Person (Possessive) Case: Sentiment Neutralized: " + govWord + " " + depWord);
                return returnSentiment;
            }

            if (subject != null && depWord.equalsIgnoreCase(subject)) {
                returnSentiment = XpSentiment.POSITIVE_SENTIMENT;
            } else {
                returnSentiment = XpSentiment.NEGATIVE_SENTIMENT;
            }

            boolean isNegated = false;
            if (negatedOpinionMap != null && (negatedOpinionMap.containsKey(govWord) || negatedOpinionMap.containsKey(depWord))) {
                isNegated = true;
            } else if (dependencyGraph.isNegatedVertex(gov)) {
                isNegated = true;
            }
            //			else if (dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NEGATION_MODIFIER) != null) {
            //				isNegated = true;
            //			}

            //			IndexedWord advmodDep = dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
            //		if (dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NEGATION_MODIFIER) != null) {

            if (isNegated) {
                returnSentiment = SentimentFunctions.switchSentiment(returnSentiment);
                //				System.out.println("Sentiment switched to: " + returnSentiment);
            }

            if (returnSentiment != null) {

                if (returnSentiment.equals(XpSentiment.POSITIVE_SENTIMENT)) {
                    xpf.setNeedPosFeature(true);
                } else if (returnSentiment.equals(XpSentiment.NEGATIVE_SENTIMENT)) {
                    xpf.setNeedNegFeature(true);
                }
                xpf.setSuggestionPresentFeature(true);
                //				System.out.println("Suggestion Present: " + xpf.isSuggestionPresentFeature());
                if (tagSentimentMap != null) {
                    tagSentimentMap.put(XpSentiment.NEED_ID, returnSentiment);

                    String tagFromSentiment = SentimentFunctions.getTagFromSentiment(returnSentiment);
                    tagSentimentMap.put(govWord, tagFromSentiment);
                    if (PosTags.isMainVerb(language, dep.tag())) {
                        tagSentimentMap.put(depWord, tagFromSentiment);
                    }
                }
            }
        }
        //		System.out.println("Returning return Sentiment from NeedClassifier: " + returnSentiment);
        //		System.out.println("Tag Sentiemment Map: " + tagSentimentMap);
        logger.info("Sentiment From NeedClassifier: " + returnSentiment);
        return returnSentiment;

    }

    public static boolean possessionClassifier(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation, StanfordDependencyGraph sdg) {
        String govWord = gov.word().toLowerCase();
        String depWord = dep.word().toLowerCase();

        Languages language = sdg.getLanguage();

        logger.info("Possession classifer govWord : " + govWord + " depWord : " + depWord);
        if (VocabularyTests.isHavePastTense(language, govWord) || VocabularyTests.isHavePastTense(language, depWord)) {
            return false;
        }

        XpSentenceResources xto = sdg.getXpTextObject();
        Map<String, String> wordPOSmap = xto.getWordPOSmap();
        Map<String, String> tagSentimentMap = xto.getTagSentimentMap();
        XpFeatureVector xpf = xto.getXpf();

        String govTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
        String depTag = XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap);
        //		boolean isPossessionPresent = SentimentLexicon.POSSESSION_TAG.equals(gov_tag) || SentimentLexicon.POSSESSION_TAG.equals(dep_tag) || SentimentLexicon.NON_POSSESSION_TAG.equals(gov_tag) || SentimentLexicon.NON_POSSESSION_TAG.equals(dep_tag);
        //		boolean isPossessionPresent = false;
        String sentiment = null;

		/*It captures I want to buy this, but not I want to get rid of this. This should not include past tense*/

        if (XpSentiment.POSSESSION_TAG.equals(govTag) && !PosTags.isPastVerb(language, gov.tag())) {
			/*I want iphone || I want to buy*/
            if (PosTags.isProperNoun(language, dep.tag()) || XpSentiment.POSSESSION_TAG.equals(depTag)) {
                //				if (dep.tag().contains("NN") || SentimentLexicon.POSSESSION_TAG.equals(depTag)) {
                //				if (dep.tag().contains("NN") || SentimentLexicon.POSSESSION_TAG.equals(depTag) || SentimentLexicon.NON_POSSESSION_TAG.equals(depTag)) {
				/*I want nothing with this bank*/
                if (XpSentiment.SHIFTER_TAG.equals(depTag)) {
                    sentiment = XpSentiment.NEGATIVE_SENTIMENT;
                } else {
                    sentiment = XpSentiment.POSITIVE_SENTIMENT;

                    logger.info("Intention Classification in Possession (Case 1):\t" + gov + "\t" + dep);
                    if (!xpf.isIntentionFeature()) {
                        IntentEngine.intentionClassification(gov, dep, edgeRelation, sdg, false);
                    } //					xpf.setIntentionFeature(true, depWord, false);

                }
            } else if (XpSentiment.NON_POSSESSION_TAG.equals(depTag)) {
				/*I want to leave iphone*/
                sentiment = XpSentiment.NEGATIVE_SENTIMENT;
            } else {
                IndexedWord dobjDep = sdg.getDependencyGraph().getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
                if (dobjDep != null) {

                    String dobjDepSentiTag = XpSentiment.getLexTag(language, dobjDep.word(), wordPOSmap, tagSentimentMap);
                    if (PosTags.isNoun(language, dobjDep.tag())) {
						/*I want to have iphone*/
                        String dobjDepWord = dobjDep.word().toLowerCase();
                        logger.info("Intention Classification in Possession (Case 2):\t" + gov + "\t" + dobjDepWord);
                        //							xpf.setIntentionFeature(true, dobjDepWord, true);
                        if (!xpf.isIntentionFeature()) {
                            //							IntentEngine.intentionClassification(gov, dobjDep, edgeRelation, dependencyGraph, true, xpf, domainName, subject, negatedOpinionHash, tagAspectMap, tagSentimentMap, wordPOSmap, language, multiWordEntityMap);
                            IntentEngine.intentionClassification(gov, dobjDep, edgeRelation, sdg, true);
                        }
                        sentiment = XpSentiment.POSITIVE_SENTIMENT;
                    } else if (dobjDepSentiTag != null) {
                        if (XpSentiment.NON_POSSESSION_TAG.equals(dobjDepSentiTag) || XpSentiment.SHIFTER_TAG.equals(dobjDepSentiTag)) {
							/*I want to get rid of iphone || I want nothing from this bank*/
                            sentiment = XpSentiment.NEGATIVE_SENTIMENT;
                        }
                    }
                }
            }
        } else if (XpSentiment.POSSESSION_TAG.equals(depTag) && !SentimentFunctions.isCombinationPolar(depTag, govTag) && !SentimentFunctions.isPolarTag(govTag)) {
            //			else if (SentimentLexicon.POSSESSION_TAG.equals(depTag) || SentimentLexicon.NON_POSSESSION_TAG.equals(depTag)) {
            //			xpf.setIntentionFeature(true);
			/*It should not contain expensive buy*/
            sentiment = XpSentiment.POSITIVE_SENTIMENT;
        } else if (XpSentiment.NON_POSSESSION_TAG.equals(govTag)) {
			/*Avoid the crisis*/
            String depSentiTag = XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap);
            if (SentimentFunctions.isPolarTag(depSentiTag)) {
                //				tagSentimentMap.put(depWord, SentimentFunctions.switchSentimentTag(depSentiTag));
                sentiment = SentimentFunctions.getSentimentFromTag(depSentiTag);
                logger.info("Possession Classifier: NPOSSN on Sentiment: " + depWord + "\t" + govWord);
            }
        }

        if (VocabularyTests.isHavePresentTense(language, govWord)) {
            if (XpSentiment.PPI_TAG.equals(depTag)) {
                sentiment = XpSentiment.POSITIVE_SENTIMENT;
                logger.info("Possession Classifier has/have PPI:\t" + depWord);
            } else if (XpSentiment.NPI_TAG.equals(depTag)) {
                sentiment = XpSentiment.NEGATIVE_SENTIMENT;
                logger.info("Possession Classifier has/have NPI:\t" + depWord);
            }
        }
        boolean isGovWordNegated = sdg.getNegatedOpinionMap().containsKey(govWord);
        boolean isDepWordNegated = sdg.getNegatedOpinionMap().containsKey(depWord);
        if (isGovWordNegated || isDepWordNegated) {
            logger.info("isGovWordNegated : " + isGovWordNegated + " isDepWordNegated : " + isDepWordNegated);
            sentiment = SentimentFunctions.switchSentiment(sentiment);
        }
        if (SentimentFunctions.isPolarSentiment(sentiment)) {
            logger.info("Possession Classifier: " + govWord + "\t" + depWord + "\t" + sentiment);
            tagSentimentMap.put(XpSentiment.POSSN_ID, sentiment);
            return true;
        }
        return false;
    }

    //	public static String determineNeedBasedSentiment(Languages language, SemanticGraph dependencyGraph, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, XpFeatureVector xpf) {
    //		String returnSentiment = null;
    //		if (dependencyGraph != null) {
    //
    //			List<SemanticGraphEdge> sortedEdgesList = dependencyGraph.edgeListSorted();
    //			Collections.sort(sortedEdgesList, new DependencyRelationSorter());
    //
    //			Set<SemanticGraphEdge> checkedEdges = new HashSet<SemanticGraphEdge>();
    //			//			Collection<GrammaticalRelation> prepRelations = EnglishGrammaticalRelations.getPreps();
    //			Collection<GrammaticalRelation> prepRelations = UniversalEnglishGrammaticalRelations.getNmods();
    //
    //			for (SemanticGraphEdge edge : sortedEdgesList) {
    //				if (checkedEdges.contains(edge)) {
    //					continue;
    //				}
    //				checkedEdges.add(edge);
    //				IndexedWord gov = edge.getGovernor();
    //				IndexedWord dep = edge.getDependent();
    //				GrammaticalRelation rel = edge.getRelation();
    //
    //				//				if (rel.equals(UniversalEnglishGrammaticalRelations.ADJECTIVAL_COMPLEMENT) || rel.equals(UniversalEnglishGrammaticalRelations.CLAUSAL_COMPLEMENT) || rel.equals(UniversalEnglishGrammaticalRelations.DIRECT_OBJECT) || rel.equals(UniversalEnglishGrammaticalRelations.PREPOSITIONAL_OBJECT) || rel.equals(UniversalEnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT)) {
    //				//					returnSentiment = needClassifier(gov, dep, rel, dependencyGraph, tagSentimentMap, xpf, null, null);
    //				//				}
    //
    //				//				if (rel.equals(UniversalEnglishGrammaticalRelations.ADJECTIVAL_COMPLEMENT) || rel.equals(UniversalEnglishGrammaticalRelations.CLAUSAL_COMPLEMENT) || rel.equals(UniversalEnglishGrammaticalRelations.PREPOSITIONAL_OBJECT) || rel.equals(UniversalEnglishGrammaticalRelations.DIRECT_OBJECT)) {
    //
    //				//				if (rel.equals(UniversalEnglishGrammaticalRelations.ADJECTIVAL_COMPLEMENT) || rel.equals(UniversalEnglishGrammaticalRelations.CLAUSAL_COMPLEMENT) || rel.equals(UniversalEnglishGrammaticalRelations.DIRECT_OBJECT) || prepRelations.contains(rel) || rel.equals(UniversalEnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT)) {
    //				if (rel.equals(UniversalEnglishGrammaticalRelations.CLAUSAL_COMPLEMENT) || rel.equals(UniversalEnglishGrammaticalRelations.DIRECT_OBJECT) || prepRelations.contains(rel) || rel.equals(UniversalEnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT)) {
    //					returnSentiment = needClassifier(language, gov, dep, rel, dependencyGraph, wordPOSmap, tagSentimentMap, xpf, null, null);
    //				} else if (rel.equals(UniversalEnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT)) {
    //					IndexedWord dobjDep = dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
    //					/* They need to have good employees: Employees: Need to have*/
    //					if (dobjDep != null) {
    //						checkedEdges.add(dependencyGraph.getEdge(dep, dobjDep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT));
    //						returnSentiment = needClassifier(language, gov, dobjDep, rel, dependencyGraph, wordPOSmap, tagSentimentMap, xpf, null, null);
    //					}
    //				} else if (rel.equals(UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT) || rel.equals(UniversalEnglishGrammaticalRelations.NOMINAL_PASSIVE_SUBJECT)) {
    //					nsubjBasedSentimentClassifier(language, gov, dep, rel, dependencyGraph, wordPOSmap, tagSentimentMap, xpf);
    //				} else if (rel.equals(UniversalEnglishGrammaticalRelations.AUX_MODIFIER)) {
    //					auxilliarySentimentClassifier(language, gov, dep, rel, dependencyGraph, wordPOSmap, tagSentimentMap, xpf);
    //				} else if (rel.equals(UniversalEnglishGrammaticalRelations.MARKER)) {
    //					markerSentimentClassifier(language, gov, dep, rel, dependencyGraph, wordPOSmap, tagSentimentMap, xpf);
    //				} else if (rel.equals(UniversalEnglishGrammaticalRelations.COMPOUND_MODIFIER)) {
    //					if (dep.word().toLowerCase().equals("killer")) {
    //						tagSentimentMap.put("killer", XpSentiment.POSITIVE_TAG);
    //					}
    //				}
    //
    //			}
    //		}
    //		return returnSentiment;
    //	}

    //	public static HashMap<Integer, String> syntacticSentimentEvaluation(List<String> tokenList, HashMap<String, String> wordLemmaMap) {
    public static String sentimentEvaluation(Languages language, List<String> tokenList, XpSentenceResources xto, boolean considerMLsentiment) {
        if (tokenList != null) {
            String sentiment;
            // System.out.println("sentimentEvaluation, language: " + language);
            sentiment = SentimentParser.rulesPipeline(language, tokenList, xto.getWordPOSmap(), xto.getTagSentimentMap(), xto.getXpf(), considerMLsentiment);

            //		String sentiment = SentimentRulesFeatureBased.rulesPipeline(tokenList, tagSentimentMap);
            return sentiment;
        } else {
            // System.out.println("sentimentEvaluation: empty tokenList");
            return XpSentiment.NEUTRAL_SENTIMENT;
        }
    }

    //	public static String sentimentEvaluation(Languages language, List<String> tokenList, SemanticGraph dependencyGraph, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, XpFeatureVector xpf, boolean considerMLsentiment, boolean considerNeedSentiment) {
    //		String sentiment = null;
    //		if (considerNeedSentiment) {
    //			if (tagSentimentMap == null) {
    //				tagSentimentMap = new HashMap<String, String>();
    //			}
    //
    //			if (dependencyGraph != null) {
    //				sentiment = determineNeedBasedSentiment(language, dependencyGraph, wordPOSmap, tagSentimentMap, xpf);
    //			}
    //		}
    //		if (sentiment == null) {
    //			if (tokenList != null) {
    //				sentiment = SentimentParser.rulesPipeline(language, tokenList, wordPOSmap, tagSentimentMap, xpf, considerMLsentiment);
    //				//		String sentiment = SentimentRulesFeatureBased.rulesPipeline(tokenList, tagSentimentMap);
    //				return sentiment;
    //			} else {
    //				return XpSentiment.NEUTRAL_SENTIMENT;
    //			}
    //		}
    //		return sentiment;
    //
    //	}

    public static Map<List<String>, String> sentimentEvaluationAdvanced(Languages language, List<String> tokenList, Map<String, String> tagSentimentMap, XpFeatureVector featureSet) {

        List<List<String>> splitTokenList = new ArrayList<List<String>>();
        int prevIndex = 0;

        //if first word is "but" we need not check for it and split
        for (int i = 1; i < tokenList.size(); i++) {
            String currTok = tokenList.get(i);
            if (VocabularyTests.isBut(language, currTok)) {
                splitTokenList.add(tokenList.subList(prevIndex, i));
                prevIndex = i;
            }

        }
        splitTokenList.add(tokenList.subList(prevIndex, tokenList.size()));
        Map<List<String>, String> snippetSentiMap = SentimentParser.rulesPipelineAdvanced(language, splitTokenList, tagSentimentMap, featureSet);
        //		Map<List<String>, String> snippetSentiMap = SentimentRulesFeatureBased.rulesPipelineAdvanced(splitTokenList, tagSentimentMap);
        return snippetSentiMap;
    }

    public static String sentimentEvaluation(Languages language, String str, XpSentenceResources xto, boolean considerMLsentiment) {
        //		str = preProcessString(str);
        // System.out.println("sentimentEvaluation with str " + str);
        List<String> tokenList = ProcessString.tokenizeString(language, str);
        return sentimentEvaluation(language, tokenList, xto, considerMLsentiment);
        //		String sentiment = SentimentParser.rulesPipeline(tokenList, tagSentimentMap, featureSet);
        //		String sentiment = SentimentRulesFeatureBased.rulesPipeline(tokenList, tagSentimentMap);
        //		return sentiment;
    }

    //	public static String sentimentEvaluation(String str, HashMap<String, String> wordLemmaMap) {
    //		List<String> tokenList = ProcessString.tokenizeString(str);
    //		String sentiment = SentimentRules.rulesPipeline(tokenList, wordLemmaMap);
    //		return sentiment;
    //	}

    //	public static LinkedHashMap<String, String> sentimentPhraseWise(String str) {
    //		str = preProcessString(str);
    //		return SentimentParser.sentiTagParseToMap(str);
    //		//		return SentimentRulesFeatureBased.sentiTagParseToMap(str);
    //	}

    /**
     * Returns the contextual sentiment tag of the opinionWord associated with entity in the given domain name
     * @author Koustuv Saha
     * 04-Mar-2015
     * @param entity
     * @param opinionWord
     * @param domainName
     * @return domainContextualSentiment
     *
     */
    //	public static String getDomainContextualSentimentTag(String entity, String opinionWord, String domainName) {
    //		return getDomainContextualSentimentTag(Languages.EN, entity, opinionWord, domainName);
    //	}

    //	public static String getDomainContextualSentimentTag(Languages lang, String entity, String opinionWord, String domainName, String subject, XpTextObject xto) {
    //		// System.out.println("map: " + XpOntology.domainNameIdxMap);
    //		// System.out.println(domainName);
    //		return XpOntology.getDomainContextualSentiment(lang, entity, opinionWord, domainName, subject, xto);
    //	}

    //	public static void setDomainContextualSentiment(String entity, String opinionWord, String domainName) {
    //		//		return XpOntology.getDomainContextualSentiment(entity, opinionWord, domainName);
    //		return XpOntology.setDomainContextualSentiment(entity, opinionWord, domainName);
    //	}

    public static boolean getDomainNonSentimentTag(Languages lang, String word, String domainName) {

        return XpOntology.isDomainNonSentimentPhrase(lang, word, domainName);
        //		return XpOntology.domainConceptsArr[XpOntology.domainNameIdxMap.get(domainName)].checkDomainNonSentiment(word);
    }

    //	public static void init() throws IOException {
    //		XpSentiment.init();
    //	}

    //	public static HashMap<String, String> findSyntacticSentiment(String str, XpFeatureSet featureSet) {
    //		HashMap<String, String> snippetSentiMap = new HashMap<String, String>();
    //		// str = preProcessString(str);
    //		String[] shiftSplitArr = str.split(shiftSplitPat);
    //		String[] sentiArr = new String[shiftSplitArr.length];
    //
    //		for (int i = 0; i < shiftSplitArr.length; i++) {
    //			// sentiArr[i] = FireRules.rulesPipeline(shiftSplitArr[i]);
    //			sentiArr[i] = sentimentEvaluation(shiftSplitArr[i], featureSet);
    //		}
    //
    //		// for (int i = 0; i < shiftSplitArr.length; i++) {
    //		// System.out.println(shiftSplitArr[i] + ">> " + sentiArr[i]);
    //		// }
    //
    //		for (int i = 1; i < sentiArr.length; i++) {
    //			if (sentiArr[i - 1] != null) {
    //				if (SentiParse.isOppSentiment(sentiArr[i - 1], sentiArr[i])) {
    //					continue;
    //				} else if (sentiArr[i].equalsIgnoreCase("POSITIVE") || sentiArr[i].equalsIgnoreCase("NEGATIVE")) {
    //					if (!SentiParse.isPolarSentiment(sentiArr[i - 1])) {
    //						sentiArr[i - 1] = SentiParse.switchSentiment(sentiArr[i]);
    //					}
    //				} else if (sentiArr[i - 1].equalsIgnoreCase("POSITIVE") || sentiArr[i - 1].equalsIgnoreCase("NEGATIVE")) {
    //					if (!SentiParse.isPolarSentiment(sentiArr[i])) {
    //						sentiArr[i] = SentiParse.switchSentiment(sentiArr[i - 1]);
    //					}
    //				}
    //			}
    //		}
    //
    //		for (int i = 0; i < shiftSplitArr.length; i++) {
    //			// System.out.println(shiftSplitArr[i] + ">> " + sentiArr[i]);
    //			snippetSentiMap.put(shiftSplitArr[i].trim(), sentiArr[i]);
    //		}
    //		return snippetSentiMap;
    //
    //	}
    //
    public static void main(String[] args) throws Exception {
        Languages language = Languages.EN;
        XpEngine.init(true);
        //		XPconfig.init(propsFile);

        // XPconfig.loadLibraryLocations(propsFile);
        //
        // init();

        // String str =
        // "Erection is very strong and long, and absolutely no side effects for me.";

        //		String str = "I don't want to have it";
        //		String str = "It doesn't fit to my country Portugal it means that it doesn't work with phone app.";
        //		String str = "It fits in my pocket";
        //		String str = "He loved it and did not want to leave it for a second.";
        //		String str = "When I found the Rescue Bots,I brought Boulder home and my son fell nstantly in love with it.";
        //		String str = "not know if it will remain at that price or if it is available for less elsewhere, this toy is not worth $30 in my opinion	My_Little_Pony	Overall	Overall	I do not know if it will remain at that price or if it is available for less elsewhere, this toy is not worth $30 in my opinion";

        //		String str = "Checkout takes atleast 15 minutes";
        String str = "naaasty bathroom";
        //		String str = "I can not pee in peace";
        System.out.println(str);
        //		String str = "Unfortunately the sound effects do  not work consistently - seems like the wiring in the toy is bad.";
        //		String str = "I expected cheap materials, but these were really well constructed, and they have been played with since buying them.";
        //		String str = "I probably would not recommend this to someone that is using it just as it is, but food was cold";

        double startTime = System.currentTimeMillis();
        //		String sentiment = sentimentEvaluation(str);
        //		System.out.println(sentiment);
        //		System.out.println(System.currentTimeMillis() - startTime + " ms");
        //		System.out.println(sentimentPhraseWise(str));
        //
        // Logger logger = LoggerFactory.getLogger(SentimentEngine.class);
        // logger.info(sentimentPhraseWise(str).toString());

        XpFeatureVector featureSet = new XpFeatureVector();
        //		findSyntacticSentiment(str, featureSet);

        System.out.println("-------------New Style-----------");
        List<String> tokenList = new ArrayList<String>();
        tokenList = ProcessString.tokenizeString(language, str);
        HashMap<String, String> wordLemmaMap = new HashMap<String, String>();
        for (String tok : tokenList) {
            String lemma = CoreNLPController.lemmatizeToString(language, tok);
            wordLemmaMap.put(tok, lemma);
        }
        System.out.println(tokenList);
        startTime = System.currentTimeMillis();
        HashMap<String, String> tagSentimentMap = new HashMap<String, String>();
        //		System.out.println(sentimentEvaluation(tokenList, tagSentimentMap, featureSet, true));
        //		System.out.println(syntacticSentimentEvaluation(tokenList, wordLemmaMap));
        System.out.println(System.currentTimeMillis() - startTime + " ms");

        startTime = System.currentTimeMillis();
        System.out.println("Sending " + tokenList + " and " + wordLemmaMap);
        System.out.println(sentimentEvaluationAdvanced(language, tokenList, tagSentimentMap, featureSet));
        System.out.println(System.currentTimeMillis() - startTime + " ms");

    }

}