/**
 * @author Alix Melchy Jun 12, 2015 11:51:53 AM
 */
package  com.abzooba.xpresso.expressions.sentiment.languages;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.expressions.sentiment.SentimentLexicon;
import com.abzooba.xpresso.expressions.sentiment.SentimentPhraseInfo;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;

public class SentimentLexiconEN extends SentimentLexicon {

    protected Map<String, String> phrasalVerbsMap = new HashMap<String, String>();

    public SentimentLexiconEN() {
        this.language = Languages.EN;
        try {
            loadTSVtoMap(XpConfig.getPhrasalVerbs(language), phrasalVerbsMap);
            init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* (non-Javadoc)
     * @see com.abzooba.xpresso.expressions.sentiment.SentLexicon#addSwitcher(java.util.Map)
     */
    @Override
    protected void addSwitcher(Map<String, SentimentPhraseInfo> basic_kbMap) {
        SentimentPhraseInfo phraseObj = new SentimentPhraseInfo();
        phraseObj.setLexTag(XpSentiment.SWITCHER_TAG);
        basic_kbMap.put("but", phraseObj);
    }

	/* (non-Javadoc)
	 * @see com.abzooba.xpresso.expressions.sentiment.SentLexicon#lookUpString(java.lang.String)
	 */
    //	@Override
    //	protected String lookUpString(String str) {
    //		String tag = basicLexicalMap.get(str);
    //		if (tag == null) {
    //			String[] strArr = str.split(" ");
    //			StringBuilder lemma = new StringBuilder();
    //			for (String word : strArr) {
    //				lemma.append(CoreNLPController.lemmatizeToString(word)).append(" ");
    //			}
    //			String lemmaStr = lemma.toString().trim();
    //			if (lemmaStr.length() > 0) {
    //				tag = lemmatizedLexicalMap.get(lemmaStr);
    //			}
    //		}
    //		return tag;
    //	}

    //	/* (non-Javadoc)
    //	 * @see com.abzooba.xpresso.expressions.sentiment.SentLexicon#getLexTag(java.lang.String, java.util.Map)
    //	 */
    //	@Override
    //	protected String getLexTag(String str, Map<String, String> wordPOSmap) {
    //		if (greetingsSalutationSet.contains(str)) {
    //			return SentimentLexicon.NON_SENTI_TAG;
    //		}
    //
    //		String tag = lookUpString(str);
    //		if (tag == null) {
    //
    //			if (!str.contains(" ")) {
    //				if (wordPOSmap == null || (wordPOSmap != null && !("NNP").equals(wordPOSmap.get(str)))) {
    //					String spellSuggestedStr = LuceneSpell.correctSpellingWord(str);
    //					if (spellSuggestedStr != null) {
    //						tag = lookUpString(spellSuggestedStr);
    //					}
    //				}
    //			}
    //			if (tag == null) {
    //				tag = phrasalVerbsMap.get(str);
    //			}
    //			if (tag == null) {
    //				tag = idiomsMap.get(str);
    //			}
    //
    //		}
    //		return tag;
    //	}

	/* (non-Javadoc)
	 * @see com.abzooba.xpresso.expressions.sentiment.SentLexicon#isNP(java.lang.String)
	 */
    //	@Override
    //	protected boolean isNP(String tag) {
    //		return ("NNP").equals(tag);
    //	}

    /**
     * @author Koustuv Saha
     * Jul 7, 2015
     * @return
     *
     */
    public Map<String, String> getPhrasalVerbMap() {
        // TODO Auto-generated method stub
        return phrasalVerbsMap;
    }

    public String getLexTag(String str, Map<String, String> wordPOSmap) {
        String sentiTag = super.getLexTag(str, wordPOSmap);
        if (sentiTag == null) {
            sentiTag = phrasalVerbsMap.get(str);
        }

        return sentiTag;
    }

}