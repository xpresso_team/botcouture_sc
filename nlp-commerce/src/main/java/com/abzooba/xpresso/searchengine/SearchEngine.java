/**
 *
 */
package  com.abzooba.xpresso.searchengine;

/**
 * @author Koustuv Saha
 * 18-Oct-2014 1:51:29 pm
 * XpressoV2.7  SearchEngine
 */
public interface SearchEngine {

    public enum SEARCH_ENGINE {
        BING_API, BING_CRAWL, GOOGLE_API, GOOGLE_CRAWL
    };

    public int getHitCount();

}