/**
 *
 */
package  com.abzooba.xpresso.searchengine;

import com.abzooba.xpresso.searchengine.SearchEngine.SEARCH_ENGINE;

/**
 * @author Koustuv Saha
 * 23-Feb-2015 9:57:14 pm
 * XpressoV2.7  HitCounterThread
 */
public class SearchEngineHitCounter {

    public static int getHitCount(String phrase, String searchElement, SEARCH_ENGINE engine_name) {
        phrase = phrase.toLowerCase();
        if (searchElement != null) {
            searchElement = searchElement.toLowerCase();
        }
        Integer hitCount = null;
        String query = null;
        SearchEngine se = null;
        switch (engine_name) {
            case BING_API:
                query = BingSearchAPI.constructQuery(phrase, searchElement, 10);
                se = new BingSearchAPI(query);
                break;
            case BING_CRAWL:
                break;
            case GOOGLE_API:
                break;
            case GOOGLE_CRAWL:
                query = GoogleSearchCrawler.constructQuery(phrase, searchElement, 10);
                se = new GoogleSearchCrawler(query);
                break;
        }

        if (se != null) {
            hitCount = se.getHitCount();
        }
        if (hitCount == null) {
            hitCount = 0;
        }
        //		System.out.println(phrase + "\t" + searchElement + "\t" + hitCount);
        return hitCount;
    }

}