/**
 *
 */
package  com.abzooba.xpresso.searchengine;

import java.util.List;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.expressions.emotion.EmotionLexicon;
import com.abzooba.xpresso.searchengine.SearchEngine.SEARCH_ENGINE;

/**
 * @author Koustuv Saha
 * 23-Feb-2015 6:09:42 pm
 * XpressoV2.7  PMICalculator
 */
public class PmiCalculator {

    int seedWordsComputed;

    //	private static final String phraseScoreFileName = "data/phraseSeedHits.txt";

    public static void init() throws Exception {
        // bingUrl = "https://www.bing.com/search?q=";
        //		ACCOUNT_KEY = "BLv+2ANZgreuLwtiP4AcMmPNoCxVRT4z1uF3Hy0QI/M";
        BingSearchAPI.init();

        //		FileIO.read_file(phraseScoreFileName, phraseScoresList);
        //		for (String phraseScore : phraseScoresList) {
        //			String[] phraseScoreArr = phraseScore.split("\t");
        //			if (phraseScoreArr.length == 2) {
        //				//				phraseLogORscoreMap.put(phraseScoreArr[0], Double.parseDouble(phraseScoreArr[1]));
        //				phraseSeedHitsMap.put(phraseScoreArr[0], Integer.parseInt(phraseScoreArr[1]));
        //
        //			}
        //		}
    }

    public static void exit() {
        //		FileIO.write_file(phraseLogORscoreMap, phraseScoreFileName, false, XpConfig.TAB_DELIMITER);
        //		FileIO.write_file(phraseSeedHitsMap, phraseScoreFileName, false, XpConfig.TAB_DELIMITER);
        BingSearchAPI.writeMap();
    }

    public PmiCalculator() {
        seedWordsComputed = 0;
    }

    private double getPMIvalue(String phrase, SEARCH_ENGINE engine_name, List<String> seedArr) throws Exception {
        double categoryPMI = 0;

        int loopCount = (XpConfig.SEARCH_ENGINE_MAX_SEED < seedArr.size()) ? XpConfig.SEARCH_ENGINE_MAX_SEED : seedArr.size();

        //		for (String element : loopCount) {
        //		for (int i = 0; i < loopCount; i++) {
        //			String element = seedArr.get(i);
        //			new HitCounterThread(phrase, null, engine_name).start();
        //			new HitCounterThread(element, null, engine_name).start();
        //			new HitCounterThread(phrase, element, engine_name).start();
        //		}
        //		while (this.seedWordsComputed != loopCount) {
        //			//			System.out.println(this.seedWordsComputed + "\t" + loopCount);
        //			Thread.sleep(100);
        //		}

        for (int i = 0; i < loopCount; i++) {
            String element = seedArr.get(i);
            int phraseScore = SearchEngineHitCounter.getHitCount(phrase, null, engine_name);
            int elementScore = SearchEngineHitCounter.getHitCount(element, null, engine_name);
            long comparisionScore = SearchEngineHitCounter.getHitCount(phrase, element, engine_name);

            double seedPMI;
            if (comparisionScore == 0 || phraseScore == 0 || elementScore == 0) {
                seedPMI = 0;
            } else {
                seedPMI = Math.log10((double) comparisionScore) / Math.log10(((double) phraseScore * (double) elementScore));
            }

            categoryPMI += seedPMI;

            //			Random rand = new Random();
            //			int randNum = rand.nextInt((20 - 6) + 1) + 5;
            //			Thread.sleep(randNum);
        }
        categoryPMI = categoryPMI / loopCount;
        //		System.out.println("*****PMI for " + phrase + " :-> " + categoryPMI);
        //		categoryPMI = categoryPMI * 2;
        categoryPMI = 2 / (1 + Math.exp((-1) * 10 * categoryPMI));
        //		System.out.println("*****Sigmoid PMI for " + phrase + " :-> " + categoryPMI);
        return categoryPMI;
    }

    public double getPMI(Languages lang, String phrase, String category, SEARCH_ENGINE engine_name) {
        //	public static Double getLogORRatio(String phrase, String category, SEARCH_ENGINE engine_name) {

        Double logORscore = null;

        //		Double logORscore = phraseLogORscoreMap.get(phrase);

        //			System.out.println("Phrase not found: " + phrase);
        logORscore = 0.0;
        double totalCount = 0;
        try {
            totalCount = getPMIvalue(phrase, engine_name, EmotionLexicon.getEmotionSeedVector(lang, category));
            //			totalCount = getPMI(phrase, engine_name, EmotionLexicon.getEmotionSeedVector(category));

            //				totalPosCount = getComparisionScore(phrase, engine_name, posSeedArr);
            //				totalNegCount = getComparisionScore(phrase, engine_name, negSeedArr);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        logORscore = (double) totalCount;
        //			return (double) totalCount;

        //			logORscore = Math.log10((double) (totalPosCount + 1) / (double) (totalNegCount + 1));
        //		phraseLogORscoreMap.put(phrase, logORscore);
        //		}
        return logORscore;
    }
}