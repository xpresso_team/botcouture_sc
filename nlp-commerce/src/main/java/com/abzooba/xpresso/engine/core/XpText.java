/**
 *
 */
package com.abzooba.xpresso.engine.core;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.abzooba.xcommerce.core.XCLogger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.languages.XpTextInterfaceEN;
import com.abzooba.xpresso.engine.core.languages.XpTextInterfaceSP;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.sdgraphtraversal.ConstituencyTree;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.LuceneSpell;
import com.abzooba.xpresso.textanalytics.ProcessString;

//import ch.qos.logback.classic.Logger;
import edu.stanford.nlp.ling.CoreAnnotations.MentionsAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;
import edu.stanford.nlp.util.CoreMap;
import org.slf4j.Logger;

/**
 * @author Koustuv Saha
 * 10-Mar-2014 3:24:48 pm
 * XpressoV2 XPtext
 */
public class XpText {

	protected static Logger logger;
	private static final TregexPattern tgrepPattern = TregexPattern.compile("S");

	protected final String original_text;
	protected String normalized_text;
	public boolean isMicro = false;

	protected Annotation document;

	//	Adding a language switch to choose functions relevant to language
	protected Languages language;
	protected Class<?> languageSpecificInterface;

	protected static void init() {
		logger = XCLogger.getSpLogger();
	}

	/**
	 * @param reviewStr
	 */
	public XpText(String reviewStr) {
		this(Languages.EN, reviewStr, false);
	}

	/**
	 * @param language
	 * @param reviewStr
	 * @param isMicro
	 */
	public XpText(Languages language, String reviewStr, boolean isMicro) {
		this.original_text = reviewStr.replaceAll("\t", " ");
		this.isMicro = isMicro;
		this.setLanguage(language);
		this.initialProcess();
	}


	protected XpText(String reviewStr, boolean isMicro) {
		this(Languages.EN, reviewStr, isMicro);
	}

	protected void setLanguage(Languages language) {
		this.language = language;
		switch (language) {
			case EN:
				this.languageSpecificInterface = XpTextInterfaceEN.class;
				break;
			case SP:
				this.languageSpecificInterface = XpTextInterfaceSP.class;
				break;
		}
	}

	protected void preProcess() {
		this.normalized_text = this.original_text;
		this.normalized_text = this.normalized_text.trim();
		this.normalized_text = ProcessString.removeUrls(normalized_text);
		this.normalized_text = ProcessString.removeTwitterUsernames(normalized_text);
		this.normalized_text = this.normalized_text.replaceAll("’", "'");

		this.normalized_text = this.normalized_text.replaceAll("\\\\'", "'");

		/*should not hit for 9/10*/
		this.normalized_text = this.normalized_text.replaceAll("(\\w{2,})/(\\w{2,})", "$1 or $2");
		/* If "....." exists in sentence, then replace it with . and a space */
		this.normalized_text = this.normalized_text.replaceAll("\\.\\.+", ". ");
		/* If no space exists after a ".". Then add a space */
		this.normalized_text = this.normalized_text.replaceAll("\\.([A-Z][^\\.])", ". $1");
		/* omit the single letters at the end of a sentence*/
		this.normalized_text = this.normalized_text.replaceAll("\\s\\S\\.", ".");
		this.normalized_text = this.normalized_text.replaceAll("\\s\\S\\!", "!");

		//		this.normalized_text = this.normalized_text.replaceAll("\\s\\S\\Z", "");
		//any single letter at the end of the sentence should be removed. "hello q" should be "hello"
		//this.normalized_text = this.normalized_text.replaceAll("\\s\\S\\.$", ".");
		/* If a lowercaps letter after . and " " then replace with capitalized version */
		this.normalized_text = this.normalized_text.replaceAll("\\.([a-z][^\\.])", ". " + "$1".toUpperCase());
		/* Multiple occurences of same punctuation character is replaced by one. This leads to wrong parsing .!?\\-*/
		this.normalized_text = this.normalized_text.replaceAll("(\\?|\\*|\\$|\\!|\\-)+", "$1");
		//		this.normalized_text = this.normalized_text.replaceAll("\\?+", "?").replaceAll("\\*+", "*").replaceAll("\\$+", "\\$").replaceAll("\\!+", "!");
		this.normalized_text = this.normalized_text.replaceAll("\\.([a-z][^\\.])", ". " + "$1".toUpperCase());
		try {
			String methodName = "preProcess";
			Method method = languageSpecificInterface.getMethod(methodName, String.class);
			this.normalized_text = (String) method.invoke(null, this.normalized_text);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		/* Parking Lot - Crowded with reckless and rude drivers */
		//		this.normalized_text = this.normalized_text.replaceAll(" - ", " is ");
		this.normalized_text = ProcessString.removeHypertextTags(normalized_text);
		this.normalized_text = this.normalized_text.replaceAll("\\s+", " ");

		//		this.normalized_text = this.normalized_text.replaceAll("TMobile", "Tmobile").replaceAll("T Mobile", "Tmobile").replaceAll("T-Mobile", "Tmobile").replaceAll("iphone", "iPhone").replaceAll("hone6 plus", "hone6 Plus").replaceAll("hone 6 plus", "hone6 Plus");
		this.normalized_text = ProcessString.resolveReplacePatterns(this.normalized_text);
		this.normalized_text = ProcessString.resolveApostrophe(this.language, this.normalized_text);
		logger.info("Normalized Text :\t" + this.normalized_text);

	}

	public Languages getLanguage() {
		return this.language;
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * Annotates the document
	 */
	protected void annotateDocument() {
		document = new Annotation(this.normalized_text);
		CoreNLPController.getPipeline(language).annotate(document);
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param sentence
	 * @return dependency_graph
	 *
	 */
	public SemanticGraph getDependencyGraph(CoreMap sentence) {
		SemanticGraph dependency_graph = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
		return dependency_graph;
	}

	public ConstituencyTree getConstituencyTree() {
		ConstituencyTree tree = null;
		List<CoreMap> sentencesList = getSentences();

		try {
			JSONObject parseTreeJSON = getParseTree();
			String key;
			String treeStr;
			Iterator<?> keys = parseTreeJSON.keys();
			int sentenceIdx = 0;
			while (keys.hasNext()) {
				key = (String) keys.next();
				treeStr = (String) parseTreeJSON.get(key);
				tree = new ConstituencyTree(sentencesList.get(sentenceIdx), treeStr);
				sentenceIdx += 1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return tree;
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @return dependency_graph as json
	 *
	 */
	protected JSONObject getDependencyGraph() {
		Map<String, List<String>> dependencyParseMap = new LinkedHashMap<String, List<String>>();
		List<CoreMap> sentencesList = this.document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentencesList) {
			SemanticGraph dependencyGraph = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
			//			String depGraphString = dependencyGraph.toString(SemanticGraph.OutputFormat.LIST).replaceAll("\n", "\t");
			List<String> depGraphList = new ArrayList<String>();
			if (dependencyGraph != null) {
				for (TypedDependency dep : dependencyGraph.typedDependencies()) {
					depGraphList.add(dep.toString());
				}
			}

			dependencyParseMap.put(sentence.toString(), depGraphList);
		}
		return new JSONObject(dependencyParseMap);
	}

	private void initialProcess() {
		this.preProcess();
		double tac = System.currentTimeMillis();
		this.annotateDocument();
		double tic = System.currentTimeMillis();
		logger.info("Annotation completed in " + (tic - tac) + " ms");
	}

	public List<CoreMap> getSentences() {
		List<CoreMap> sentencesList = this.document.get(SentencesAnnotation.class);
		return sentencesList;
	}

	public boolean checkForbiddenSentence(CoreMap sentence) {
		boolean isForbidden = false;
		if (sentence.toString().matches("[\\.!]")) {
			isForbidden = true;
		}
		return isForbidden;
	}

	@SuppressWarnings("unchecked")
	protected List<String> getTokens(String domainName, CoreMap sentence, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, XpFeatureVector featureSet) {
		List<String> tokenList = null;
		try {
			String methodName = "getTokens";
			Method method = languageSpecificInterface.getMethod(methodName, String.class, CoreMap.class, Map.class, Map.class, XpFeatureVector.class);
			tokenList = (List<String>) method.invoke(null, domainName, sentence, wordPOSmap, tagSentimentMap, featureSet);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		return tokenList;
	}

	/**
	 * The method retrieves the sentiment corresponding to the sentence as given by the Stanford parsers
	 * In case of Spanish, the sentiment is null.
	 * @return map containing the sentence and the corresponding sentiment given by the stanford parser
	 */
	protected Map<String, String> getStanfordSentiment() {
		List<CoreMap> sentences = this.getSentences();
		Map<String, String> sentenceSentimentMap = new LinkedHashMap<String, String>();
		for (CoreMap sentence : sentences) {
			String sentiment = null;
			if (language == Languages.EN) {
				sentiment = sentence.get(SentimentCoreAnnotations.SentimentClass.class);
			}
			sentenceSentimentMap.put(sentence.toString(), sentiment);

		}
		return sentenceSentimentMap;
	}

	protected String getStanfordSentiment(CoreMap sentence) {
		if (language == Languages.EN) {
			return sentence.get(SentimentCoreAnnotations.SentimentClass.class);
		} else {
			return null;
		}
	}

	/**
	 * The method gets the words and the corresponding POS tags from the sentences
	 * @return JSON string of a map containing the sentence as key and the word-POS pair as value pair
	 */
	protected JSONObject getPOS() {
		Map<String, String> posMap = new LinkedHashMap<String, String>();
		List<CoreMap> sentencesList = this.getSentences();
		for (CoreMap sentence : sentencesList) {
			StringBuilder sb = new StringBuilder();
			List<CoreLabel> coreLabelList = sentence.get(TokensAnnotation.class);
			//			logger.info("coreLabelList : " + coreLabelList.toString());
			for (CoreLabel coreLabel : coreLabelList) {
				//				logger.info("coreLabel Tag : " + coreLabel.tag());
				sb.append(coreLabel.word()).append("/").append(coreLabel.tag()).append(" ");
			}
			posMap.put(sentence.toString(), sb.toString().trim());
		}
		return new JSONObject(posMap);
	}

	/**
	 * The method processes a sentence(in stanford custom data structure) and returns the map of named entities
	 * extracted from the sentence.
	 * @param sentence text(in stanford custom data structure) from where named entities are extracted.
	 * @return map of named entities extracted from the sentence
	 */
	protected static Map<String, String> getNERsentence(CoreMap sentence) {
		Map<String, String> entitiesNERmap = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
		List<CoreLabel> coreLabelList = sentence.get(TokensAnnotation.class);
		String previousNER = null;
		String previousEntity = null;
		for (CoreLabel coreLabel : coreLabelList) {

			String currNER = coreLabel.ner();

			if (currNER.equals(previousNER)) {
				previousEntity += " " + coreLabel.word();
			} else {
				if (previousEntity != null) {
					if (!previousNER.equals("O")) {
						entitiesNERmap.put(previousEntity, previousNER);
					}
				}
				previousEntity = coreLabel.word();
				previousNER = currNER;
			}
		}
		if (previousEntity != null) {
			if (!previousNER.equals("O")) {
				entitiesNERmap.put(previousEntity, previousNER);
			}
		}
		return entitiesNERmap;
	}

	protected static List<CoreMap> getEntityMentions(CoreMap sentence) {
		List<CoreMap> entityMentions = sentence.get(MentionsAnnotation.class);
		return entityMentions;
	}

	/**
	 * The method computes and returns a map of named entities from a list of sentences(in stanford custom data structure)
	 * @return string containing the map of the named entities extracted from the list of the sentences
	 */
	public JSONObject getNER() {

		return new JSONObject(getNerMap());
	}

	private Map<String, Map<String, String>> getNerMap() {
		List<CoreMap> sentencesList = this.getSentences();
		Map<String, Map<String, String>> sentenceNERmap = new LinkedHashMap<String, Map<String, String>>();
		for (CoreMap sentence : sentencesList) {
			Map<String, String> entitiesNERmap = getNERsentence(sentence);
			sentenceNERmap.put(sentence.toString(), entitiesNERmap);
		}
		return sentenceNERmap;
	}

	public Map<String, String> getAllNerMap() {
		List<CoreMap> sentencesList = this.getSentences();
		Map<String, String> allNerMap = new LinkedHashMap<String, String>();
		for (CoreMap sentence : sentencesList) {
			Map<String, String> entitiesNERmap = getNERsentence(sentence);
			allNerMap.putAll(entitiesNERmap);
		}
		return allNerMap;
	}

	protected JSONObject getLemmas() {
		//		CAUTION: this function returns null lemma in Spanish (no lemmatization with CoreNLP)
		Map<String, String> lemmaMap = new LinkedHashMap<String, String>();
		List<CoreMap> sentencesList = this.getSentences();
		for (CoreMap sentence : sentencesList) {
			StringBuilder sb = new StringBuilder();
			List<CoreLabel> coreLabelList = sentence.get(TokensAnnotation.class);
			for (CoreLabel coreLabel : coreLabelList) {
				sb.append(coreLabel.word()).append("/").append(coreLabel.lemma()).append(" ");
			}
			lemmaMap.put(sentence.toString(), sb.toString().trim());
		}

		return new JSONObject(lemmaMap);
	}

	public JSONObject getParseTree() {
		Map<String, String> parseMap = new LinkedHashMap<String, String>();
		List<CoreMap> sentencesList = this.document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentencesList) {
			Tree parseTree = sentence.get(TreeAnnotation.class);
			parseMap.put(sentence.toString(), parseTree.toString());
		}
		return new JSONObject(parseMap);
	}

	public Tree getParseTree(CoreMap sentence) {
		Tree parseTree = sentence.get(TreeAnnotation.class);
		return parseTree;
	}

	public List<CoreLabel> getTokens(CoreMap sentence) {
		List<CoreLabel> coreLabels = sentence.get(TokensAnnotation.class);
		return coreLabels;
	}

	public JSONObject getSpellCorrectedText() {

		List<CoreMap> sentencesList = this.getSentences();
		Map<String, String> spellCorrectedSentenceMap = new LinkedHashMap<String, String>();

		StringBuilder sb = new StringBuilder();
		for (CoreMap sentence : sentencesList) {
			List<CoreLabel> coreLabelList = sentence.get(TokensAnnotation.class);
			for (CoreLabel token : coreLabelList) {
				String actualWord = token.word();
				String correctedWord = LuceneSpell.correctSpellingWord(language, actualWord);
				if (correctedWord != null) {
					sb.append(correctedWord);
				} else {
					sb.append(actualWord);
				}
				sb.append(" ");
			}
			spellCorrectedSentenceMap.put(sentence.toString(), sb.toString().trim());
		}

		return new JSONObject(spellCorrectedSentenceMap);

	}

	private void addToSubSnippets(String subSnippet, List<String> subSnippetsList) {
		subSnippet = subSnippet.trim();
		if (subSnippet.length() > 0 && !subSnippet.matches("\\W+")) {
			subSnippetsList.add(subSnippet);
		}
	}

	protected List<String> getSubSnippets(Tree parseTree, CoreMap sentence) {
		List<String> subSnippetsList = new ArrayList<String>();

		String snippet = sentence.toString();
		int snippetHashCode = ProcessString.calculateStringHashCode(snippet);

		String remSubSnippet = snippet;
		TregexMatcher m_tree = tgrepPattern.matcher(parseTree);
		boolean isChecked = false;
		int counter = 0;
		while (m_tree.find()) {
			counter++;
			Tree subTree = m_tree.getMatch();
			String currentStr = Sentence.listToString(subTree.yield());

			/* To avoid putting in double counted same entire sentence */
			if (!isChecked && (snippetHashCode == ProcessString.calculateStringHashCode(currentStr))) {
				remSubSnippet = currentStr;
				isChecked = true;
			}

			/*
			 * To avoid double entry of the sentences like, Not sure I would
			 * return unless they offered some kind of reconciliation.
			 */
			else if (remSubSnippet.contains(currentStr)) {
				remSubSnippet = remSubSnippet.replace(currentStr, "");
				this.addToSubSnippets(currentStr, subSnippetsList);
			}
		}
		remSubSnippet = remSubSnippet.replaceAll("\\s+", " ").replaceAll(" (\\.|,|;|:|\\?)", "$1").trim();

		if (counter == 0) {
			this.addToSubSnippets(remSubSnippet, subSnippetsList);
			return subSnippetsList;
		}
		this.addToSubSnippets(remSubSnippet, subSnippetsList);
		logger.info("SubSnippets List: " + subSnippetsList);
		return subSnippetsList;
	}

	/**
	 * @author Koustuv Saha
	 * Oct 8, 2015
	 * @return
	 *
	 */
	protected JSONObject getExpression() {
		return null;
	}

	/**
	 * @author Koustuv Saha
	 * Oct 8, 2015
	 * @return
	 *
	 */
	protected JSONObject getSentiment() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @author Koustuv Saha
	 * Oct 8, 2015
	 * @return
	 *
	 */
	protected JSONObject getEmotion() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @author Koustuv Saha
	 * Oct 8, 2015
	 * @return
	 *
	 */
	protected JSONObject getIntention() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @author Koustuv Saha
	 * Oct 8, 2015
	 * @return
	 *
	 */
	protected JSONObject getDocumentCategory() {
		// TODO Auto-generated method stub
		return null;
	}

	protected JSONObject getTopic() {
		// TODO Auto-generated method stub
		return null;
	}

	protected JSONObject getExprTrend() {
		// TODO Auto-generated method stub
		return null;
	}

	protected JSONObject getAspect() {
		// TODO Auto-generated method stub
		return null;
	}

	public Annotation getDocument() {
		return document;
	}
}