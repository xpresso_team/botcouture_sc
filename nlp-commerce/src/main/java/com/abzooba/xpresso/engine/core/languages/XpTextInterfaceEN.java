/**
 * @author Alix Melchy Jun 2, 2015 6:40:16 PM
 */
package  com.abzooba.xpresso.engine.core.languages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.ProcessString;

import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.Americanize;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.util.CoreMap;

public class XpTextInterfaceEN {

    public static String preProcess(String original_text) {
        String normalized_text = original_text;

        ////		this.normalized_text = this.normalized_text.replaceAll("(\\w+)/(\\w+)", "$1 or $2");
        //		this.normalized_text = this.original_text;
        //		if (isMicro) {
        //			this.normalized_text = MicroTextPreProcessor.constructSentence(this.normalized_text);
        //		}
        //		//		this.normalized_text = this.normalized_text.replaceAll("(\\w+)/(\\w+)", "$1 or $2");
        //		/*should not hit for 9/10*/
        //		this.normalized_text = this.normalized_text.replaceAll("(\\w{2,})/(\\w{2,})", "$1 or $2");
        //		/* If "....." exists in sentence, then replace it with . and a space */
        //		//		this.normalized_text = this.normalized_text.replaceAll("\\.\\.+", ". ");
        //		/* If no space exists after a ".". Then add a space */
        //		//		this.normalized_text = this.normalized_text.replaceAll("\\.([A-Z][^\\.])", ". $1");
        //		/*
        //		 * If a lowercaps letter after . and " " then replace with capitalized
        //		 * version
        //		 */
        //		//		this.normalized_text = this.normalized_text.replaceAll("\\.([a-z][^\\.])", ". " + "$1".toUpperCase());
        //		//		this.normalized_text = this.normalized_text.replaceAll("[^\\x00-\\x7F]", "");
        //		this.normalized_text = this.normalized_text.replaceAll("\\?+", "?").replaceAll("\\*+", "*").replaceAll("\\$+", "\\$");
        //
        //		this.normalized_text = this.normalized_text.replaceAll("\\.([a-z][^\\.])", ". " + "$1".toUpperCase());
        //		this.normalized_text = this.normalized_text.replaceAll("[^\\x00-\\x7F]", "");
        //		/* Parking Lot - Crowded with reckless and rude drivers */
        //		//		this.normalized_text = this.normalized_text.replaceAll(" - ", " is ");
        //		//		this.normalized_text = this.normalized_text.replaceAll(" NO ", " no ");
        //		//		this.normalized_text = this.normalized_text.replaceAll("([^,]) but ", "$1, but ");
        //		//		this.normalized_text = this.normalized_text.replaceAll("\\s+", " ");
        //
        //		//		this.normalized_text = this.normalized_text.replaceAll("TMobile", "Tmobile").replaceAll("T Mobile", "Tmobile").replaceAll("T-Mobile", "Tmobile").replaceAll("iphone", "iPhone").replaceAll("hone6 plus", "hone6 Plus").replaceAll("hone 6 plus", "hone6 Plus");
        //		//		this.normalized_text = ProcessString.resolveReplacePatterns(this.normalized_text);
        //		//		this.normalized_text = ProcessString.resolveAppostrophie(this.normalized_text);
        //		//		this.normalized_text = this.original_text;
        //		//

        //		normalized_text = normalized_text.replaceAll("(\\w+)/(\\w+)", "$1 or $2");
        //		/* If "....." exists in sentence, then replace it with . and a space */
        //		normalized_text = normalized_text.replaceAll("\\.\\.+", ". ");
        //		/* If no space exists after a ".". Then add a space */
        //		normalized_text = normalized_text.replaceAll("\\.([A-Z][^\\.])", ". $1");
        //		/*
        //		 * If a lowercaps letter after . and " " then replace with capitalized
        //		 * version
        //		 */
        //		normalized_text = normalized_text.replaceAll("\\.([a-z][^\\.])", ". " + "$1".toUpperCase());
        normalized_text = normalized_text.replaceAll("[^\\x00-\\x7F]", "");
        //		/* Parking Lot - Crowded with reckless and rude drivers */
        //		normalized_text = normalized_text.replaceAll(" - ", " is ");
        normalized_text = normalized_text.replaceAll(" NO ", " no ");
        normalized_text = normalized_text.replaceAll("([^,]) but ", "$1, but ");

        List<String> initialTokensList = ProcessString.tokenizeString(Languages.EN, normalized_text);
        for (String token : initialTokensList) {
            String americanizedToken = Americanize.americanize(token);
            if (!americanizedToken.equals(token)) {
                normalized_text = normalized_text.replaceAll("\\b" + token + "\\b", americanizedToken);
            }

        }

        //		normalized_text = Americanize.americanize(normalized_text);

        //		normalized_text = normalized_text.replaceAll("\\s+", " ");
        //
        //		//		normalized_text = normalized_text.replaceAll("TMobile", "Tmobile").replaceAll("T Mobile", "Tmobile").replaceAll("T-Mobile", "Tmobile").replaceAll("iphone", "iPhone").replaceAll("hone6 plus", "hone6 Plus").replaceAll("hone 6 plus", "hone6 Plus");
        //
        //		normalized_text = ProcessString.resolveReplacePatterns(normalized_text);
        //		normalized_text = ProcessString.resolveApostrophe(normalized_text);

        return normalized_text;

    }

    //	protected void annotateDocument(Annotation document) {
    //		CoreNLPController.getPipeline().annotate(document);
    //	}

    public static SemanticGraph getDependencyGraph(CoreMap sentence) {

        // SemanticGraph dependency_graph =
        // sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
        SemanticGraph dependency_graph = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
        return dependency_graph;

    }

	/*	The method getDependencyGraph() should call getDependencyGraph(sentence); as for typed dependencies, to be examined to find equivalent in Spanish case.*/

    public static void featurizePOSbased(String pos, XpFeatureVector featureSet) {

        switch (pos) {
            case ("JJ"):
                featureSet.setAdjFeature(true);
                break;
            case ("JJR"):
                featureSet.setCompAdjFeature(true);
                break;
            case ("JJS"):
                featureSet.setSupAdjFeature(true);
                break;
            case ("RB"):
                featureSet.setAdvbFeature(true);
                break;
            case ("RBR"):
                featureSet.setCompAdvbFeature(true);
                break;
            case ("RBS"):
                featureSet.setSupAdvbFeature(true);
                break;
        }
    }

    public static List<String> getTokens(String domainName, CoreMap sentence, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, XpFeatureVector featureSet) {

        List<String> tokenList = new ArrayList<String>();

        int fwCount = 0;
        List<CoreLabel> coreLabelList = sentence.get(TokensAnnotation.class);

        for (CoreLabel token : coreLabelList) {

            // String tokenStr = token.toString();
            String tokenWord = token.word();
            if ((tokenWord.length() > 2) && (!tokenWord.matches("\\W+")) && (tokenWord.toUpperCase()).equals(tokenWord)) {
                featureSet.setCapitalizedPresentFeature(true);
                // System.out.println("Capitalized Word: " + tokenWord);
            }

            String word = tokenWord.toLowerCase();
            String lemma = token.lemma();
            CoreNLPController.addToLemmaMap(Languages.EN, word, lemma.toLowerCase());

            if (!word.matches("\\W+")) {

                // System.out.println("Before Word: " + word);
                word = word.replaceAll("\\.", "");
                String pos = token.tag();
                if (pos.startsWith("FW")) {
                    fwCount += 1;
                }
                featurizePOSbased(pos, featureSet);
                //				if (SentimentEngine.getDomainNonSentimentTag(Languages.EN, word.toLowerCase(), domainName)) {
                //					tagSentimentMap.put(word.toLowerCase(), XpSentiment.NON_SENTI_TAG);
                //				}
                wordPOSmap.put(word, pos);
                tokenList.add(word);
            }
            //			else if (word.equals(".")) {
            //				featureSet.setPeriodPresentFeature(true);
            //			}
        }

        // if (fwCount >= 2) {
        int length = tokenList.size();
        double percentageFW = 0;
        if (length > 0) {
            percentageFW = (double) ((double) fwCount / (double) tokenList.size());
        }
        if (percentageFW > 0.5) {
            return null;
        }
        return tokenList;
    }

}