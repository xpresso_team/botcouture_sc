/**
 *
 */
package com.abzooba.xpresso.engine.core;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

import com.abzooba.xcommerce.core.XCLogger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.aspect.domainontology.DomainKnowledge;
import com.abzooba.xpresso.aspect.domainontology.OntologyUtils;
import com.abzooba.xpresso.aspect.domainontology.XpOntology;
import com.abzooba.xpresso.aspect.entityextraction.ontologybuilder.XpOntologyBuilder;
import com.abzooba.xpresso.aspect.wordvector.FuzzyEntityRecognizer;
import com.abzooba.xpresso.aspect.wordvector.WordVectorAspect;
import com.abzooba.xpresso.aspect.wordvector.WordVectorBottomUp;
import com.abzooba.xpresso.document.DocumentClassifier;
import com.abzooba.xpresso.document.JobDescriptionClassifier;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Annotations;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.controller.XpCacheWriterThread;
import com.abzooba.xpresso.engine.controller.XpFileWriterThread;
import com.abzooba.xpresso.expressions.emotion.EmotionVector;
import com.abzooba.xpresso.expressions.intention.XpIntention;
import com.abzooba.xpresso.expressions.sentiment.SWNsentiment;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.machinelearning.XpClassifierML;
import com.abzooba.xpresso.machinelearning.XpClassifierML.ALGORITHM;
import com.abzooba.xpresso.machinelearning.XpClassifierML.CLASSIFICATION_TYPE;
import com.abzooba.xpresso.realtime.KeywordRelevanceComputation;
import com.abzooba.xpresso.realtime.TrendInfoManipulation;
import com.abzooba.xpresso.realtime.TrendManipulationThread;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.LuceneSpell;
import com.abzooba.xpresso.textanalytics.ProcessString;
import com.abzooba.xpresso.textanalytics.microtext.HashTagProcessor;
import com.abzooba.xpresso.textanalytics.microtext.MicroTextPreProcessor;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;
import com.abzooba.xpresso.textanalytics.wordvector.WordVectorDB;
import com.abzooba.xpresso.utils.DatabaseFunctions;
import com.abzooba.xpresso.utils.FileIO;
import com.abzooba.xpresso.utils.GenericIO;
import org.slf4j.Logger;


/**
 * @author Koustuv Saha 
 * 10-Mar-2014 2:27:11 pm 
 * XpressoV2 XPengine
 */
public class XpEngine {

	private static boolean interactiveMode = false;
	private static boolean mlMode = false;

	private static Map<Languages, XpClassifierML> xpSentiClassifierMap = new ConcurrentHashMap<Languages, XpClassifierML>();
	private static Map<Languages, XpClassifierML> xpStmtClassifierMap = new ConcurrentHashMap<Languages, XpClassifierML>();
	private static WordVectorDB wordVectorDB = new WordVectorDB();
	private static Logger logger;

	//	private static Semaphore AVAILABLE_LOCK;
	private static String CURRENT_DATE_STR = null;

	// private static WekaStmtClassifierML classifierStmtML;

	// private static List<Triple> rawTriples;

	// private static Stack<Triple> rawTriples;

	private static String outputFileName;

	public static DatabaseFunctions dbFunction;

	public static void setOutputFileName(String fileName) {
		outputFileName = fileName;
	}

	public static String getOutputFileName() {
		return outputFileName;
	}

	public static XpClassifierML getXpSentiClassifier(Languages language) {
		// synchronized (XpSentiClassifier) {
		return xpSentiClassifierMap.get(language);
		// }
	}

	public static XpClassifierML getXpStmtClassifier(Languages language) {
		// synchronized (XpStmtClassifier) {
		return xpStmtClassifierMap.get(language);
		// }
	}

	public static WordVectorDB getWordVectorDbApp() {
		return wordVectorDB;
	}

	// public static void addToTriples(Triple t) {
	// synchronized (rawTriples) {
	// // if either it is nonTrainingMode or it is in Interactive Mode it
	// // should add it to triples
	// // if (!getTrainingMode() || getInteractiveMode()) {
	// if (mlMode) {
	// // rawTriples.add(t);
	//
	// t.mismatchFix();
	// rawTriples.push(t);
	// // if (rawTriples.size() >= 1000) {
	// if (rawTriples.size() >= 2000) {
	// // System.out.println("Writing to triples File " +
	// // rawTriples.size());
	// // XpParser.write_triples(rawTriples, outputFileName, true);
	// write_triples(outputFileName, true);
	// // flushTriples();
	// // System.out.println("Flushed: " + rawTriples.size());
	// }
	// }
	// }
	// }
	//
	// public static void addToTriplesB(List<Triple> triplesList) {
	// synchronized (rawTriples) {
	// // if either it is nonTrainingMode or it is in Interactive Mode it
	// // should add it to triples
	// // if (!getTrainingMode() || getInteractiveMode()) {
	// if (mlMode) {
	// // rawTriples.add(t);
	//
	// // t.mismatchFix();
	// // rawTriples.push(t);
	// for (Triple t : triplesList) {
	// rawTriples.push(t);
	// }
	// // if (rawTriples.size() >= 1000) {
	// if (rawTriples.size() >= 5000) {
	// // System.out.println("Writing to triples File " +
	// // rawTriples.size());
	// // XpParser.write_triples(rawTriples, outputFileName, true);
	// write_triples(outputFileName, true);
	// // flushTriples();
	// // System.out.println("Flushed: " + rawTriples.size());
	// }
	// }
	// }
	// }

	public static void addToTriples(List<XpDistill> triplesList) {
		//System.out.println("Add to triples before if -----");
		if (mlMode) {
			//System.out.println("Add to triples -----");
			//			FileIO.write_fileNIO(triplesList, asynchFileChannel, true);

			XpFileWriterThread xpFW = new XpFileWriterThread(triplesList, outputFileName);
			xpFW.start();

		}
	}

	// public static Stack<Triple> getTriples() {
	// synchronized (rawTriples) {
	// return rawTriples;
	// }
	// }
	//
	// public static void write_triples(String outputFileName, boolean
	// writeFileFlag) {
	// synchronized (rawTriples) {
	//
	// List<String> triples_strList = new ArrayList<String>();
	//
	// int size = rawTriples.size();
	// // for (Triple t : triplesList) {
	// for (int i = 0; i < size; i++) {
	// Triple t = rawTriples.pop();
	//
	// String t_str = t.toString();
	//
	// System.out.println(t_str);
	// if (t_str != null) {
	// triples_strList.add(t_str);
	// }
	// }
	// if (writeFileFlag && outputFileName != null) {
	// FileIO.write_file(triples_strList, outputFileName, true);
	// }
	//
	// }
	// }

	// protected static boolean getTrainingMode() {
	// return trainingMode;
	// }
	//
	protected static boolean getInteractiveMode() {
		return interactiveMode;
	}

	public static void setInteractiveMode(boolean iMode) {
		interactiveMode = iMode;
	}

	public static String getCurrentDateStr() {
		return CURRENT_DATE_STR;
	}

	// public static boolean getMachineLearningMode() {
	// if (!getTrainingMode() && !getInteractiveMode()) {
	// return true;
	// }
	// return false;
	// }

	public static boolean getMachineLearningMode() {
		return mlMode;
	}

	public static void init(boolean readModelMode, String... varargs) {
		try {
			init(varargs);
			if (!XpClassifierML.IS_BASE_CLASS) {
				initializeML(readModelMode);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void init(String... varargs) {

		double startTime = System.currentTimeMillis();

		try {
			XpConfig.init(varargs);
			logger = XCLogger.getSpLogger();
			XpCacheWriterThread.init();
			XpText.init();
			class XpLoaderThread extends Thread {
				boolean isComplete = false;

				@Override
				public void run() {
					try {

						double time0 = System.currentTimeMillis();
						double time1 = System.currentTimeMillis();
						/*---------------*/
						if (XpConfig.LOAD_XP_IO) {
							time0 = System.currentTimeMillis();
							GenericIO.init();
							ProcessString.init();
							time1 = System.currentTimeMillis();
							System.out.println("GenericIO, ProcessString Loaded: " + (time1 - time0) / 1000 + " seconds");
						}

						/*---------------*/
						if (XpConfig.LOAD_LUCENE) {
							time0 = time1;
							LuceneSpell.init();
							time1 = System.currentTimeMillis();
							System.out.println("LuceneSpell Loaded: " + (time1 - time0) / 1000 + " seconds");
						}

						/*---------------*/
						if (XpConfig.LOAD_ONTOLOGY_UTILS) {
							time0 = time1;
							OntologyUtils.init();
							time1 = System.currentTimeMillis();
							System.out.println("Xpresso Ontology Utils Loaded: " + (time1 - time0) / 1000 + " seconds");
						}

						/*---------------*/
						if (XpConfig.LOAD_SWN_SENTIMENT) {
							time0 = time1;
							SWNsentiment.init();
							time1 = System.currentTimeMillis();
							System.out.println("SentiWordNet Loaded: " + (time1 - time0) / 1000 + " seconds");
						}

						/*---------------*/
						if (XpConfig.LOAD_XP_EMOTION) {
							time0 = time1;
							EmotionVector.init();
							time1 = System.currentTimeMillis();
							System.out.println("Emotion Utils Loaded: " + (time1 - time0) / 1000 + " seconds");
						}

						/*---------------*/
						if (XpConfig.LOAD_XP_INTENTION) {
							time0 = time1;
							XpIntention.init();
							time1 = System.currentTimeMillis();
							System.out.println("XpIntention Loaded: " + (time1 - time0) / 1000 + " seconds");
						}
						isComplete = true;

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						isComplete = true;
					}
				}
			}

			//			AVAILABLE_LOCK = new Semaphore(XpConfig.MAX_THREADS);

			SimpleDateFormat sdf = new SimpleDateFormat(XpConfig.DATE_FORMAT);
			//			SimpleDateFormat sdf = new SimpleDateFormat(XpConfigSP.DATE_FORMAT);
			CURRENT_DATE_STR = sdf.format(Calendar.getInstance().getTime());

			//			LogFormatter.init(interactiveMode);

			//			Logger logger = LogFormatter.getLogger();
			//			logger.setLevel(XpConfig.LOGGER_LEVEL);

			XpLoaderThread xlt = new XpLoaderThread();
			xlt.start();
			//			dbFunction = new DatabaseFunctions(XpConfig.DB_NAME, XpConfig.DB_USER, XpConfig.DB_PASS);

			/*---------------*/
			double time0 = System.currentTimeMillis();
			CoreNLPController.init();
			double time1 = System.currentTimeMillis();
			System.out.println("CoreNLPController Loaded: " + (time1 - time0) / 1000 + " seconds");

			/*---------------*/
			if (XpConfig.LOAD_XP_SENTIMENT) {
				time0 = time1;
				XpSentiment.init();
				time1 = System.currentTimeMillis();
				System.out.println("SentimentEngine Loaded: " + (time1 - time0) / 1000 + " seconds");
			}

			/*---------------*/
			if (XpConfig.LOAD_XP_ONTOLOGY) {
				time0 = time1;
				DomainKnowledge.init();
				XpOntology.init();
				time1 = System.currentTimeMillis();
				System.out.println("Xpresso Ontology Loaded: " + (time1 - time0) / 1000 + " seconds");
			}

			/*---------------*/
			if (XpConfig.LOAD_WV_UTILS) {
				time0 = time1;
				WordVector.init();
				WordVectorDB.init();
				time1 = System.currentTimeMillis();
				System.out.println("WordVector Utils Loaded: " + (time1 - time0) / 1000 + " seconds");
			}

			/*---------------*/

			/*---------------*/
			if (XpConfig.LOAD_DOC_CLASSIFIER) {
				time0 = time1;
				DocumentClassifier.init(XpConfig.IS_WORDVECTOR_DOCUMENT_LOAD);
				JobDescriptionClassifier.init();
				time1 = System.currentTimeMillis();
				System.out.println("DocumentClassifier Loaded: " + (time1 - time0) / 1000 + " seconds");
			}

			/*---------------*/
			if (XpConfig.LOAD_MICROTEXT_PROCESSOR) {
				time0 = time1;
				MicroTextPreProcessor.init();
				time1 = System.currentTimeMillis();
				System.out.println("MicroTextProcessor Loaded: " + (time1 - time0) / 1000 + " seconds");
			}

			/*---------------*/
			if (XpConfig.LOAD_HASHTAG_PROCESSOR) {
				time0 = time1;
				HashTagProcessor.init();
				//			IntentEngine.init();
				time1 = System.currentTimeMillis();
				System.out.println("HashTagProcessor Loaded: " + (time1 - time0) / 1000 + " seconds");
			}

			/*---------------*/
			if (XpConfig.LOAD_WV_ASPECT_MATCH) {
				System.out.println("WordVectorAspectMatch Type: " + XpConfig.IS_WORDVECTOR_ASPECTS_MATCH);
				time0 = time1;
				if (XpConfig.IS_WORDVECTOR_ASPECTS_MATCH <= 2 || XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 5 || XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 8 || XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 9) {
					WordVectorAspect.init();
					time1 = System.currentTimeMillis();
					System.out.println("WordVectorAspectMaximalMatch Loaded: " + (time1 - time0) / 1000 + " seconds");
				}
			}

			/*---------------*/
			if (XpConfig.LOAD_WV_BOTTOMUP_MATCH) {
				time0 = time1;
				if (XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 8 || XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 10) {
					WordVectorBottomUp.init();
					time1 = System.currentTimeMillis();
					System.out.println("WordVectorBottomUpMatch Loaded: " + (time1 - time0) / 1000 + " seconds");
				}
			}

			//			if (XpConfig.IS_GLOVE_ASPECTS_MATCH >= 5) {
			//				GoogleW2V.init();
			//				time1 = System.currentTimeMillis();
			//				System.out.println("GoogleW2V Loaded: " + (time1 - time0) / 1000 + " seconds");
			//	
			//			}

			/*---------------*/
			if (XpConfig.LOAD_FUZZY_ENTITY_RECOGNIZER) {
				if (XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 3 || XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 4 || XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 6 || XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 7) {
					/*---------------*/
					time0 = System.currentTimeMillis();
					FuzzyEntityRecognizer.init(false);
					time1 = System.currentTimeMillis();
					System.out.println("FuzzyEntityRecognizer Loaded: " + (time1 - time0) / 1000 + " seconds");
				}
			}

			/*---------------*/
			if (XpConfig.LOAD_TREND_DETECTION) {
				time0 = System.currentTimeMillis();
				KeywordRelevanceComputation.init();
				TrendInfoManipulation.init();
				time1 = System.currentTimeMillis();
				System.out.println("TrendDetection Module Loaded: " + (time1 - time0) / 1000 + " seconds");
			}

			//			FullContactAPI.init();

			//TmobileLexicon.init();
			//			try {
			//				RuleBase.init();
			//			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e1) {
			//				// TODO Auto-generated catch block
			//				e1.printStackTrace();
			//			}
			//			System.out.println("Tmobile neccessities loaded");

			while (!xlt.isComplete) {
				Thread.sleep(1000);
			}
			//			dbFunction = new DatabaseFunctions();
			//			DatabaseFunctions.init();

			/*Real Time Trend Updater*/
			//			RealTimeTrendUpdaterThread rt = new RealTimeTrendUpdaterThread();
			//			rt.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Time Taken to execute the Initialization: " + (System.currentTimeMillis() - startTime) / 1000 + " seconds");
	}

	private static void initializeML(boolean readModelMode) {
		if (XpConfig.LOAD_ML_CLASSIFIER) {
			XpClassifierML.init(readModelMode);
			for (Languages language : XpConfig.LANGUAGES) {
				xpSentiClassifierMap.put(language, new XpClassifierML(language, CLASSIFICATION_TYPE.SENTIMENT, ALGORITHM.RANDOMFOREST, readModelMode));
				xpStmtClassifierMap.put(language, new XpClassifierML(language, CLASSIFICATION_TYPE.STATEMENT, ALGORITHM.RANDOMFOREST, readModelMode));
				System.out.println("Machine Learning Classifiers Loaded for " + language);
			}
			mlMode = true;
		}
	}

	// public String distillPostJSON(String annotation, String domainName,
	// String subject, String reviewStr) throws InterruptedException {
	//
	// // String outString = "";
	// AVAILABLE_LOCK.acquire();
	// String outString = null;
	// try {
	// XpLanguageProcessor review = new XpLanguageProcessor("0", domainName,
	// subject, reviewStr);
	// if (review != null) {
	// List<XpDistill> reviewTriples = review.processPipeline();
	//
	// Map<String, JSONObject> netMap = new LinkedHashMap<String, JSONObject>();
	// for (XpDistill t : reviewTriples) {
	//
	// System.out.println(t.toString());
	// String snippet = t.reviewSnippet;
	// JSONObject distillJSON = netMap.get(snippet);
	// if (distillJSON == null) {
	// distillJSON = new JSONObject();
	// JSONArray aboutArr = new JSONArray();
	// distillJSON.put("About", aboutArr);
	// }
	// JSONArray aboutMapArr = distillJSON.getJSONArray("About");
	// JSONObject aboutObj = new JSONObject();
	// String indicative_snippet;
	// if (t.entityClass.equals("Overall")) {
	// indicative_snippet = t.opinion;
	// } else {
	// indicative_snippet = snippet;
	// }
	//
	// // distillJSON.put("Indicative Snippet",
	// // indicative_snippet);
	// aboutObj.put("Entity", t.entity);
	// aboutObj.put("Aspect", t.entityClass);
	// aboutObj.put("Sentiment", t.opinionSentiment);
	// aboutObj.put("StatementType", t.statementType);
	// aboutObj.put("Indicative Snippet", indicative_snippet);
	// aboutMapArr.put(aboutObj);
	// distillJSON.put("About", aboutMapArr);
	// netMap.put(snippet, distillJSON);
	// }
	//
	// JSONObject netJSON = new JSONObject(netMap);
	// outString = netJSON.toString();
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// if (outString == null) {
	// outString = "";
	// }
	// AVAILABLE_LOCK.release();
	// return outString;
	// }

	//	public String distillPostJSON(String annotation, String domainName, String subject, String reviewStr, boolean isHistoricalDemo) throws InterruptedException {
	//		AVAILABLE_LOCK.acquire();
	//		String outString = null;
	//		if (domainName != null) {
	//			domainName = domainName.toLowerCase();
	//		}
	//		try {
	//			XpText review = null;
	//			switch (annotation.toUpperCase()) {
	//				case ("RESUME"):
	//					ResumeIR resume = new ResumeIR("0", reviewStr, null);
	//					outString = resume.getResumeJSON();
	//					break;
	//				case ("MICRO"):
	//					//					MicroTextProcessor reviewMT = new MicroTextProcessor("0", domainName, subject, reviewStr);
	//					XpExpression reviewMT = new XpExpression(isHistoricalDemo, "0", domainName, subject, reviewStr, true);
	//					outString = reviewMT.getExpression();
	//					break;
	//				case ("SENTI"):
	//					XpExpression reviewLP = new XpExpression(isHistoricalDemo, "0", domainName, subject, reviewStr);
	//					outString = reviewLP.getExpression();
	//					break;
	//				case ("POS"):
	//					review = new XpText(reviewStr);
	//					outString = review.getPOS();
	//					break;
	//				case ("LEMMA"):
	//					review = new XpText(reviewStr);
	//					outString = review.getLemmas();
	//					break;
	//				case ("PARSE"):
	//					review = new XpText(reviewStr);
	//					outString = review.getParseTree();
	//					break;
	//				case ("DEPPARSE"):
	//					review = new XpText(reviewStr);
	//					outString = review.getDependencyGraph();
	//					break;
	//				case ("NER"):
	//					review = new XpText(reviewStr);
	//					outString = review.getNER();
	//					break;
	//				case ("EMO"):
	//					XpEmotion reviewEmo = new XpEmotion(domainName, subject, reviewStr);
	//					outString = reviewEmo.getEmotion();
	//					break;
	//				case ("TMOBILE"):
	//					UseCaseResolution usr = new UseCaseResolution(reviewStr);
	//					outString = usr.resolveUseCases().toString();
	//					break;
	//				default:
	//					outString = "Invalid Annotation";
	//			}
	//
	//			// }
	//
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//		if (outString == null) {
	//			outString = "";
	//		}
	//		AVAILABLE_LOCK.release();
	//		return outString;
	//	}

	public String distillPostJSON(Annotations annotation, String domainName, String subject, String source, String reviewStr, boolean isHistoricalDemo, boolean isTrendDetection) throws InterruptedException {
		return distillPostJSON(Languages.EN, annotation, domainName, subject, source, reviewStr, isHistoricalDemo, isTrendDetection);
	}

	public String distillPostJSON(Languages language, Annotations annotation, String domainName, String subject, String source, String reviewStr, boolean isHistoricalDemo, boolean isTrendDetection) throws InterruptedException {
		//		System.out.println("Available Lock Before Acquire: " + AVAILABLE_LOCK.availablePermits());
		//		AVAILABLE_LOCK.acquire();
		//		boolean isAcquire = AVAILABLE_LOCK.tryAcquire(1000, TimeUnit.SECONDS);
		//		XpGateway.init(language);
		//		String outString = null;
		JSONObject outputJSON = null;
		String distillJSON = null;
		if (domainName != null) {
			domainName = domainName.toLowerCase();
		}
		try {
			XpText review = null;
			boolean isMicro = ("twitter".equals(source)) ? true : false;
			switch (annotation) {
				case SENTI:
					review = new XpExpression(language, annotation, isHistoricalDemo, "0", domainName, subject, reviewStr, isMicro);
					outputJSON = review.getSentiment();

					if (isTrendDetection) {
						TrendManipulationThread tmThread = new TrendManipulationThread((XpExpression) review);
						tmThread.run();
					}
					break;
				case SENTI_MP:
					review = new XpExpressionManPower(language, annotation, isHistoricalDemo, "0", domainName, subject, reviewStr, isMicro);
					outputJSON = review.getSentiment();

					if (isTrendDetection) {
						TrendManipulationThread tmThread = new TrendManipulationThread((XpExpression) review);
						tmThread.run();
					}
					break;
				case EMO:
					//					XpEmotion reviewEmo = new XpEmotion(domainName, subject, reviewStr, isMicro);
					review = new XpExpression(language, annotation, isHistoricalDemo, "0", domainName, subject, reviewStr, isMicro);
					outputJSON = review.getEmotion();
					if (isTrendDetection) {
						TrendManipulationThread tmThread = new TrendManipulationThread((XpExpression) review);
						tmThread.run();
					}
					break;
				case TOPIC:
					review = new XpExpression(language, annotation, isHistoricalDemo, "0", domainName, subject, reviewStr, isMicro);
					outputJSON = review.getTopic();
					break;
				case INTENT:
					review = new XpExpression(language, annotation, isHistoricalDemo, "0", domainName, subject, reviewStr, isMicro);
					outputJSON = review.getIntention();
					if (isTrendDetection) {
						TrendManipulationThread tmThread = new TrendManipulationThread((XpExpression) review);
						tmThread.run();
					}
					break;
				case EXPR:
					review = new XpExpression(language, annotation, isHistoricalDemo, "0", domainName, subject, reviewStr, isMicro);
					outputJSON = review.getExpression();
					if (isTrendDetection) {
						TrendManipulationThread tmThread = new TrendManipulationThread((XpExpression) review);
						tmThread.run();
					}
					break;
				case EXPR_TREND:
					review = new XpExpression(language, annotation, isHistoricalDemo, "0", domainName, subject, reviewStr, isMicro);
					if (isTrendDetection) {
						TrendManipulationThread tmThread = new TrendManipulationThread((XpExpression) review);
						tmThread.run();
					}
					outputJSON = review.getExprTrend();
					break;
				case ASPECT:
					review = new XpExpression(language, annotation, isHistoricalDemo, "0", domainName, subject, reviewStr, isMicro);
					outputJSON = review.getAspect();
					break;
				case DOCUMENT:
					review = new XpDocumentClassifier(language, reviewStr);
					outputJSON = review.getDocumentCategory();
					break;
				case POS:
					review = new XpText(language, reviewStr, isMicro);
					outputJSON = review.getPOS();
					break;
				case LEMMA:
					review = new XpText(reviewStr);
					outputJSON = review.getLemmas();
					break;
				case PARSE:
					review = new XpText(language, reviewStr, isMicro);
					outputJSON = review.getParseTree();
					break;
				case DEPPARSE:
					review = new XpText(language, reviewStr, isMicro);
					outputJSON = review.getDependencyGraph();
					break;
				case NER:
					review = new XpText(language, reviewStr, isMicro);
					outputJSON = review.getNER();
					break;
				case SPELL:
					review = new XpText(language, reviewStr, isMicro);
					outputJSON = review.getSpellCorrectedText();
					break;
				case WV_CATEGORY:
					outputJSON = (JSONObject) WordVectorBottomUp.getCategory(language, reviewStr, true, false, false);
					break;
				case WORD_VECTOR:
					List<?> wordVector = WordVector.getWordVector(language, reviewStr);
					System.out.println(wordVector.toString());
					outputJSON = new JSONObject();
					outputJSON.put("Word", reviewStr);
					outputJSON.put("WordVector", wordVector);
					break;
				case WORD_SIM:
					String words[] = reviewStr.split(",");
					if (words.length == 2) {
						List<?> vector1 = WordVector.getWordVector(language, words[0]);
						List<?> vector2 = WordVector.getWordVector(language, words[1]);
						double score = WordVector.cosSimilarity(vector1, vector2);
						System.out.println("Similarity between" + words[0] + "\t" + words[1] + ":\t" + score);
						outputJSON = new JSONObject();
						outputJSON.put("Words", reviewStr);
						outputJSON.put("Score", score);
					}
					break;
				case ENTITY_EXTRACTION:
					outputJSON = XpOntologyBuilder.getEntityExtracted(language, reviewStr, 0);
					break;
				case TREND:
					outputJSON = TrendInfoManipulation.prepareTrendJSON(domainName);
					break;

				default:
					outputJSON = new JSONObject("{\"About\":[\"Invalid Annotation\"]}");
			}
			if (outputJSON == null) {
				outputJSON = new JSONObject("{}");
			} else if (outputJSON.length() > 0) {
				outputJSON.put("Version", XpConfig.XPRESSO_VERSION);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//		System.out.println("Available Lock Before Release: " + AVAILABLE_LOCK.availablePermits());
		//		System.out.println("Available Lock Queue Length: " + AVAILABLE_LOCK.getQueueLength());
		//		System.out.println("Available Lock After Release: " + AVAILABLE_LOCK.availablePermits());
		if (outputJSON != null) {
			distillJSON = outputJSON.toString();
		}
		logger.info("Xpresso Distill JSON: \n" + distillJSON);
		//		AVAILABLE_LOCK.release();
		return distillJSON;

	}

	public static void main(String[] args) throws IOException, JSONException {
		//		String propsFile = "./config/xdt.properties";
		//		Languages language = Languages.EN;
		//		init(propsFile, true, true, false);

		//		String review = "they have a big section for homeware and clothes and have loads of great deals on always (asda's own coconut and rasin curry sauce 5p atm!)";
		//		String subject = "My_Little_Pony";

		// List<String> reviewList = new ArrayList<String>();
		Stack<String> reviewList = new Stack<String>();
		// FileIO.read_file("data/drugData.txt", reviewList);

		// FileIO.read_file("data/productData.txt", reviewList);
		// FileIO.read_file("data/hasbro_amazon2.txt", reviewList);
		// FileIO.read_file("data/hasbro_amazon3.txt", reviewList);
		// FileIO.read_file("data/hasbro_amazon2.txt", reviewList);
		// FileIO.read_file("data/hasbro_amazon2.txt", reviewList);
		// FileIO.read_file("data/facebook_data2014-04-22 07-40-19.txt",
		// reviewList);
		FileIO.read_file("data/aetna.csv", reviewList);

		// String last_rowFileName = "config/last_rowNo.txt";
		// PrintWriter pw = new PrintWriter(new FileWriter(last_rowFileName));

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String dateStr = sdf.format(cal.getTime());
		String outputFileName = "output/combinedTriples_" + dateStr + ".txt";
		String CustomerName = "WalMart";
		String outputJSONfileName = "output/" + CustomerName + "Values_" + dateStr + ".txt";

		double startTime = System.currentTimeMillis();

		// System.out.println("In Curator Style");

		// BufferedReader br = new BufferedReader(new
		// FileReader(last_rowFileName));
		// String strLine;
		//		Integer i = 0;
		// while ((strLine = br.readLine()) != null) {
		// i = Integer.parseInt(strLine);
		// }
		// br.close();
		try {
			//			XpParser.pipelineFlow(i.toString(), "senti", "retail", subject, review, false);

			// for (String line : reviewList) {
			// String[] strArr = line.split("\t");
			// String rvw = strArr[1];
			// String subj = strArr[5];
			// if (rvw.length() == 0) {
			// continue;
			// }
			// CuratorParser.pipelineFlow(i, subj, rvw);
			// System.out.println(++i);
			// }

			// write_triples(outputFileName, true);
//			XpJSON.writeJSON(outputFileName, outputJSONfileName);
			// pw.write("0");
			// pw.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(System.currentTimeMillis() - startTime + " ms");
			// write_triples(outputFileName, true);
//			XpJSON.writeJSON(outputFileName, outputJSONfileName);

			e.printStackTrace();
			// pw.write(i);
			// pw.close();
		}
		// CuratorParser.write_triples(subject, sentiTriples, "sentiment");

		System.out.println(System.currentTimeMillis() - startTime + " ms");
	}
}