/**
 *
 */
package com.abzooba.xpresso.engine.config;

import java.io.InputStream;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.abzooba.xpresso.utils.FileIO;

import ch.qos.logback.classic.Level;

/**
 * @author Koustuv Saha
 * 12-Mar-2014 1:56:46 pm
 * XpressoV2 XPconfig
 */
public class XpConfig {

	public static Properties XpressoProperties;

	public static final String TAB_DELIMITER = "\t";
	public static final String COMMA_DELIMITER = ",";

	//	TODO WORDNET for SP
	public static String WORDNET_LOC;
	public static String SENTI_WORDNET_FILE;

	public static String STORMPATH_APPLICATION_NAME;
	public static String STORMPATH_CREDENTIALS;
	public static String SUPER_USERS_FILE;
	public static String REPLACE_PATTERNS_FILE;
	public static String XPRESSO_DELIMITER;

	public static String LIB_DIR;
	public static String RESOURCES_DIR;
	public static String RESOURCES_STATIC_DIR;
	public static String MODELS_DIR;
	public static String DATA_DIR;
	public static String DOMAIN_NAME;
	public static String DOMAIN_NAMES_LIST;
	public static String DOMAIN_KB_DIR;
	//private static Map<Languages, String> DOMAIN_CONFIG_FILE = new ConcurrentHashMap<Languages, String>();
	public static String DOMAIN_CONFIG_FILE;
	private static Map<Languages, String> DOMAIN_CONCEPTS_FILE = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> DOMAIN_NONSENTIMENT_FILE = new ConcurrentHashMap<Languages, String>();
	public static Boolean IS_SUBJECT_MODE;
	public static String INPUT_DIR;
	public static String OUTPUT_DIR;
	public static String MeSH_DB_LOC;

	public static String WV_DATABASE_TYPES;
	public static Integer WV_DATABASE_USE;
	public static int DOCUMENT_CLASSIFY_APPROACH;

	// public static String DOMAIN_NON_SENTI_FILENAME;

	public static Boolean ENABLE_BING_SEARCH;

	public enum Languages {
		EN, SP
	}

	/*
	 * Add in exprAnnotations if aspect based sentiment analysis is required
	 */
	public enum Annotations {
		RESUME, SENTI, EMO, INTENT, EXPR, TOPIC, POS, LEMMA, PARSE, DEPPARSE, NER, SPELL, DOCUMENT, ASPECT, TMOBILE, EXPR_TREND, TREND, WORD_VECTOR, WV_CATEGORY, WORD_SIM, ENTITY_EXTRACTION, WALMART_EMAIL, SENTI_MP
	}

	public static Set<Annotations> exprAnnotations;
	public static Set<Languages> LANGUAGES;

	private static Map<Languages, String> STOPWORDS_FILE = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> DICTIONARY_FILE = new ConcurrentHashMap<Languages, String>();
	public static String CONCEPTS_FILENAME;
	public static String POSSIBLE_ASPECTS_FILENAME;
	//	public static String IMPLICIT_CONCEPTS_MAP;
	private static Map<Languages, String> IMPLICIT_ASPECT_LEXICON = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> CONTEXTUAL_SENTI_FILENAME = new ConcurrentHashMap<Languages, String>();
	//	private static Map<Languages, String> CONCEPTS_CLUSTER_FILENAME = new ConcurrentHashMap<Languages, String>();

	private static Map<Languages, String> PRONOUN_WORDS_FILE_LOCATION = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> WEAK_VERBS_FILE_LOCATION = new ConcurrentHashMap<Languages, String>();

	private static Map<Languages, String> POSSESSION_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> NON_POSSESSION_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> RESOURCES_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> CONSUME_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> NEED_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> DECREASE_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> INCREASE_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> INTENSIFIER_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> NEGATIVE_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> POSITIVE_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> UC_NEGATIVE_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> UC_POSITIVE_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> NPI_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> PPI_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> SHIFTER_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> NON_SENTI_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> POS_TAGS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> GREETINGS_SALUTATIONS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> ABSTRACT_CONCEPTS_FILE = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> INTENT_ACTION_FILE = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> INTENT_OBJECT_FILE = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> INTENT_RULE_FILE = new ConcurrentHashMap<Languages, String>();

	public static String EMOTICONS_FILE;
	private static Map<Languages, String> IDIOMS_FILE = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> PHRASAL_VERBS_FILE = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> EMOTION_KB_FILE = new ConcurrentHashMap<Languages, String>();

	private static Map<Languages, String> ABUSE_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> ADVOCATE_WORDS = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> SUGGESTION_WORDS = new ConcurrentHashMap<Languages, String>();

	public static String INPUT_REVIEW_FILENAME;
	public static Languages REVIEW_LANGUAGE;
	public static String REVIEW_DELIMITER;
	public static int REVIEW_FIELD;
	public static int MAX_REVIEW_LENGTH;
	public static String EXTRACTED_ENTITY_FILENAME;

	public static String DATE_FORMAT;
	public static Integer MAX_LOG_SIZE;
	public static Integer MAX_FILE_COUNT;
	public static Integer MAX_THREADS;

	public static Boolean USE_EXPANDED_DOMAINKB;
	public static Boolean STANFORD_USE_SRPARSER;
	public static Boolean STANFORD_USE_CASELESS;
	private static Map<Languages, String> STANFORD_ANNOTATORS = new ConcurrentHashMap<Languages, String>();
	public static Boolean STANFORD_SP_DEPPARSE;
	public static String STANFORD_SP_DEPPARSE_MODEL;

	public static String MICROTEXT_RESOURCE_PATH;
	public static String MICROTEXT_RESOURCE_FILES;
	public static String MICROTEXT_TAGGER_MODEL;
	public static String MICROTEXT_TAG_DICT;
	public static String CLUSTER_RESOURCE_NAME;

	private static Map<Languages, String> SUFFIX_FILE = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> SENTI_MODEL = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> STMT_MODEL = new ConcurrentHashMap<Languages, String>();
	private static Map<Languages, String> EMOTION_SEED_FILE = new ConcurrentHashMap<Languages, String>();

	/**
	 * graphdb server uri
	 */
	public static Boolean USE_NEO4J;
	public static String GRAPHDB_SERVER_URI = null;

	public static String UNDO_QUERY_FILE;
	public static String DOMAIN_GRAPH_FILE;
	public static boolean USE_GRAPH_DB;
	public static boolean USE_EMBEDDED_GRAPHDB;
	public static boolean UPLOAD_STATIC_LEXICON;
	public static boolean UNDO_PREVIOUS_GRAPH;
	public static boolean BUILD_DOMAIN_GRAPH;
	public static int IS_WORDVECTOR_ASPECTS_MATCH;
	public static boolean IS_WORDVECTOR_CATEGORIES_LOAD;
	public static boolean IS_WORDVECTOR_CATEGORIES_EN_LOAD;
	public static boolean IS_WORDVECTOR_CATEGORIES_SP_LOAD;
	public static boolean IS_WORDVECTOR_DOCUMENT_LOAD;
	public static boolean USE_LSH;

	public static String EMBEDDED_DB_PATH;

	public static int SEARCH_ENGINE_MAX_SEED;

	public static String WORD_VECTOR_FILE;
	//	public static boolean LOAD_GLOVE_STATUS;

	//	public static boolean EMOTION_GLOVE_FLAG;
	//	public static boolean GLOVE_VECTOR_READ;

	//enable trend in activator
	public static boolean ENABLE_TREND_ACTIVATOR;

	//xpengine init module
	public static boolean LOAD_XP_IO;
	public static boolean LOAD_LUCENE;
	public static boolean LOAD_XP_ONTOLOGY;
	public static boolean LOAD_ONTOLOGY_UTILS;
	public static boolean LOAD_XP_SENTIMENT;
	public static boolean LOAD_XP_INTENTION;
	public static boolean LOAD_XP_EMOTION;
	public static boolean LOAD_ML_CLASSIFIER;
	public static boolean LOAD_SWN_SENTIMENT;
	public static boolean LOAD_WALMART_UTILS;
	public static boolean LOAD_WV_UTILS;
	public static boolean LOAD_WV_ASPECT_MATCH;
	public static boolean LOAD_WV_BOTTOMUP_MATCH;
	public static boolean LOAD_DOC_CLASSIFIER;
	public static boolean LOAD_MICROTEXT_PROCESSOR;
	public static boolean LOAD_HASHTAG_PROCESSOR;
	public static boolean LOAD_FUZZY_ENTITY_RECOGNIZER;
	public static boolean LOAD_TREND_DETECTION;
	public static boolean LOAD_PHRASE_SCORE;

	private static String WORD_VECTOR_CLUSTERS_FILE;
	public static String QA_TAGGED_ASPECT_FILE;

	public static String MONGO_HOST;
	public static String MONGO_DB;
	public static String MONGO_COLLECTION;
	public static String MONGO_USERNAME;
	public static String MONGO_PASSWORD;
	public static Integer MONGO_PORT;

	public static boolean MONGO_WV_LOAD;
	public static boolean MONGO_WV_READ;
	public static boolean MONGO_CREATE_INDEX;
	public static String MONGO_FILE;

	public static Level LOGGER_LEVEL;

	public static String TMOBILE_KB_FILE;
	//	public static String REVIEW_SOURCE;

	public static String UNLISTED_ENTITY_FILE;
	public static String XPRESSO_VERSION;

	public static Boolean USE_CACHE;
	public static Integer MAX_CACHE_SIZE;

	public static String ENTITY_DOC_COUNT_FILE;
	public static boolean IDF_DOWNGRADE;
	public static double UP_FACTOR;
	public static int TREND_DETECTION_THREADS;
	public static int TREND_IDF_COMPUTE_METHOD;
	public static double DOWNFACTOR_POWER;
	public static double DECAY_FACTOR;
	public static double TREND_THRESHOLD;
	public static Integer MAX_TREND_TOPIC;
	public static boolean BATCH_RUN_KEYWORD_SIGNIFICANCE;
	public static Integer TREND_TYPE;
	public static boolean TREND_BATCH_RUN;
	public static Integer ENTITY_ABUNDANCE_TYPE;
	public static int TREND_REVIEW_COUNT;
	public static int MIN_TREND_TOPIC_COUNT;

	public static String DB_NAME;
	public static String DB_USER;
	public static String DB_PASS;

	// public static String TRAINING_INPUT_FILE;

	private static String resourceName(Languages language, String dir, String shortName) {
		String longName;
		Pattern stemPattern = Pattern.compile("([\\w|-]+)\\.(\\w+)");
		Matcher stemMatch = stemPattern.matcher(shortName);
		String stemStr = null;
		String fileExt = null;

		switch (language) {
			case SP:
				if (stemMatch.find()) {
					stemStr = stemMatch.group(1);
					fileExt = stemMatch.group(2);
				}
				//				longName = RSRC_DIR + "SP/" + stemStr + "-sp.txt";
				longName = dir + "/sp/" + stemStr + "-sp." + fileExt;
				break;
			default:
				longName = dir + "/en/" + shortName;
		}

		// System.out.println(longName);
		return longName;
	}

	private static String sharedResourceName(String directory, String shortName) {
		String longName = directory + "/" + shortName;
		return longName;
	}

	private static void addResources(String dir, String resourceShortFilename, Map<Languages, String> resourceMap) {
		for (Languages language : LANGUAGES) {
			resourceMap.put(language, resourceName(language, dir, resourceShortFilename));
		}
	}

	private static String modelProperty(Languages language, String modelType) {
		String propertyName = null;
		switch (modelType) {
			case ("senti"):
				switch (language) {
					case SP:
						propertyName = "ml.sp.senti.model.file";
						break;
					default:
						propertyName = "ml.senti.model.file";
				}
				break;
			case ("stmt"):
				switch (language) {
					case SP:
						propertyName = "ml.sp.stmt.model.file";
						break;
					default:
						propertyName = "ml.stmt.model.file";
				}
				break;
		}
		return propertyName;
	}

	private static void addModels(String modelType, Map<Languages, String> modelMap) {
		for (Languages language : LANGUAGES) {
			String modelFilename;
			switch (modelType) {
				case ("senti"):
					modelFilename = XpressoProperties.getProperty(modelProperty(language, modelType), "sentiModelFile.model");
					break;
				case ("stmt"):
					modelFilename = XpressoProperties.getProperty(modelProperty(language, modelType), "stmtModelFile.model");
					break;
				default:
					modelFilename = XpressoProperties.getProperty(modelProperty(language, modelType), "modelFile.model");
			}

			modelMap.put(language, modelFilename);
			//			modelMap.put(language, DATA_DIR + "/" + modelFilename);
		}
	}

	private static Languages encode(String languageName) {
		Languages languageCode;
		switch (languageName) {
			case ("spanish"):
				languageCode = Languages.SP;
				break;
			default:
				languageCode = Languages.EN;
		}
		return languageCode;
	}

	public static Languages convertLanguage(String language) {
		switch (language.toUpperCase()) {
			case "SP":
				return Languages.SP;
			case "ES":
				return Languages.SP;
			default:
				return Languages.EN;
		}
	}

	public static Annotations convertAnnotation(String annotation) {
		switch (annotation.toUpperCase()) {
			case ("RESUME"):
				return Annotations.RESUME;
			case ("SENTI"):
				return Annotations.SENTI;
			case ("SENTIMENT"):
				return Annotations.SENTI;
			case ("EMO"):
				return Annotations.EMO;
			case ("EMOTION"):
				return Annotations.EMO;
			case ("INTENT"):
				return Annotations.INTENT;
			case ("INTENTION"):
				return Annotations.INTENT;
			case ("TOPIC"):
				return Annotations.TOPIC;
			case ("EXPR"):
				return Annotations.EXPR;
			case ("EXPRESSION"):
				return Annotations.EXPR;
			case ("EXPR_TREND"):
				return Annotations.EXPR_TREND;
			case ("ASPECT"):
				return Annotations.ASPECT;
			case ("POS"):
				return Annotations.POS;
			case ("LEMMA"):
				return Annotations.LEMMA;
			case ("PARSE"):
				return Annotations.PARSE;
			case ("DEPPARSE"):
				return Annotations.DEPPARSE;
			case ("NER"):
				return Annotations.NER;
			case ("SPELL"):
				return Annotations.SPELL;
			case ("TMOBILE"):
				return Annotations.TMOBILE;
			case ("DOCUMENT"):
				return Annotations.DOCUMENT;
			case ("DOC"):
				return Annotations.DOCUMENT;
			case ("TREND"):
				return Annotations.TREND;
			case ("WV_CATEGORY"):
				return Annotations.WV_CATEGORY;
			case ("WORD_VECTOR"):
				return Annotations.WORD_VECTOR;
			case ("WORD_SIM"):
				return Annotations.WORD_SIM;
			case ("ENTITY_EXTRACTION"):
				return Annotations.ENTITY_EXTRACTION;
			case ("WALMART_EMAIL"):
				return Annotations.WALMART_EMAIL;
			case ("WM_EMAIL"):
				return Annotations.WALMART_EMAIL;
			case ("SENTI_MP"):
				return Annotations.SENTI_MP;
			default:
				return Annotations.SENTI;
		}
	}

	private static void getLanguages(String languageNames) {
		LANGUAGES = new LinkedHashSet<Languages>();
		String[] languageArr = languageNames.split(",");
		for (String lang : languageArr) {
			LANGUAGES.add(encode(lang));
		}
	}

	private static void processStanfordAnnotators(String annotators) {
		/*
		 * Input format: language1:annotation11,annotation12;language2:annotation21,annotation22,annotation23
		 */
		String[] annotatorsArr = annotators.split(";");
		for (String annot : annotatorsArr) {
			String[] langAnnotators = annot.split(":");
			//			TODO throw error or handle badly formatted input
			STANFORD_ANNOTATORS.put(encode(langAnnotators[0]), langAnnotators[1]);
		}
	}

	//	public static String getDomainConfigFile(Languages language) {
	//		// return DOMAIN_CONFIG_FILE.get(language);
	//		return DOMAIN_CONFIG_FILE;
	//	}

	public static String getDomainConceptsFile(Languages language) {
		return DOMAIN_CONCEPTS_FILE.get(language);
	}

	public static String getDomainNonSentimentFile(Languages language) {
		return DOMAIN_NONSENTIMENT_FILE.get(language);
	}

	public static String getImplicitAspectLexicon(Languages language) {
		return IMPLICIT_ASPECT_LEXICON.get(language);
	}

	public static String getContextualSentiFilename(Languages language) {
		return CONTEXTUAL_SENTI_FILENAME.get(language);
	}

	//	public static String getConceptsClusterFilename(Languages language) {
	//		return CONCEPTS_CLUSTER_FILENAME.get(language);
	//	}

	public static String getPossessionWords(Languages language) {
		return POSSESSION_WORDS.get(language);
	}

	public static String getNonPossessionWords(Languages language) {
		return NON_POSSESSION_WORDS.get(language);
	}

	public static String getConsumeWords(Languages language) {
		return CONSUME_WORDS.get(language);
	}

	public static String getResourcesWords(Languages language) {
		return RESOURCES_WORDS.get(language);
	}

	public static String getDecreaseWords(Languages language) {
		return DECREASE_WORDS.get(language);
	}

	public static String getIncreaseWords(Languages language) {
		return INCREASE_WORDS.get(language);
	}

	public static String getIntensifierWords(Languages language) {
		return INTENSIFIER_WORDS.get(language);
	}

	public static String getNPIWords(Languages language) {
		return NPI_WORDS.get(language);
	}

	public static String getPPIWords(Languages language) {
		return PPI_WORDS.get(language);
	}

	public static String getShifterWords(Languages language) {
		return SHIFTER_WORDS.get(language);
	}

	public static String getNegativeWords(Languages language) {
		return NEGATIVE_WORDS.get(language);
	}

	public static String getPositiveWords(Languages language) {
		return POSITIVE_WORDS.get(language);
	}

	public static String getUCNegativeWords(Languages language) {
		return UC_NEGATIVE_WORDS.get(language);
	}

	public static String getUCPositiveWords(Languages language) {
		return UC_POSITIVE_WORDS.get(language);
	}

	public static String getIntentActionWords(Languages language) {
		return INTENT_ACTION_FILE.get(language);
	}

	public static String getIntentObjectWords(Languages language) {
		return INTENT_OBJECT_FILE.get(language);
	}

	public static String getNeedWords(Languages language) {
		return NEED_WORDS.get(language);
	}

	public static String getNonSentiWords(Languages language) {
		return NON_SENTI_WORDS.get(language);
	}

	public static String getGreetingsSalutations(Languages language) {
		return GREETINGS_SALUTATIONS.get(language);
	}

	public static String getAbuseWords(Languages language) {
		return ABUSE_WORDS.get(language);
	}

	public static String getAdvocateWords(Languages language) {
		return ADVOCATE_WORDS.get(language);
	}

	public static String getSuggestionWords(Languages language) {
		return SUGGESTION_WORDS.get(language);
	}

	public static String getIdioms(Languages language) {
		return IDIOMS_FILE.get(language);
	}

	public static String getPhrasalVerbs(Languages language) {
		return PHRASAL_VERBS_FILE.get(language);
	}

	public static String getEmotionKB(Languages language) {
		return EMOTION_KB_FILE.get(language);
	}

	public static String getStopWords(Languages lang) {
		return STOPWORDS_FILE.get(lang);
	}

	public static String getAnnotators(Languages language) {
		return STANFORD_ANNOTATORS.get(language);
	}

	public static String getEmotionSeed(Languages language) {
		return EMOTION_SEED_FILE.get(language);
	}

	public static String getDictionary(Languages language) {
		return DICTIONARY_FILE.get(language);
	}

	public static String getSuffix(Languages language) {
		return SUFFIX_FILE.get(language);
	}

	public static String getPronounWords(Languages language) {
		return PRONOUN_WORDS_FILE_LOCATION.get(language);
	}

	public static String getWeakVerbs(Languages language) {
		return WEAK_VERBS_FILE_LOCATION.get(language);
	}

	public static String getAbstractConcepts(Languages language) {
		return ABSTRACT_CONCEPTS_FILE.get(language);
	}

	public static String getSentiModel(Languages language) {
		return SENTI_MODEL.get(language);
	}

	public static String getStmtModel(Languages language) {
		return STMT_MODEL.get(language);
	}

	public static String getIntentRules(Languages language) {
		return INTENT_RULE_FILE.get(language);
	}

	public static String getWordVectorFile(Languages language) {
		String filename = null;
		switch (language) {
			case EN:
				filename = WORD_VECTOR_FILE;
				break;
			case SP:
				filename = "sp_" + WORD_VECTOR_FILE;
				break;
		}

		return filename;
	}

	public static String getWordVectorClusterFile(Languages language) {
		String filename = null;
		switch (language) {
			case EN:
				filename = XpConfig.DOMAIN_KB_DIR + "/clusters/" + WORD_VECTOR_CLUSTERS_FILE;
				break;
			case SP:
				filename = XpConfig.DOMAIN_KB_DIR + "/clusters/" + "sp_" + WORD_VECTOR_CLUSTERS_FILE;
				break;
		}

		return filename;
	}

	private static void addAnnotations() {
		exprAnnotations = new HashSet<Annotations>();
		exprAnnotations.add(Annotations.SENTI);
		exprAnnotations.add(Annotations.SENTI_MP);
		exprAnnotations.add(Annotations.EXPR);
		exprAnnotations.add(Annotations.TOPIC);
		exprAnnotations.add(Annotations.ASPECT);
		exprAnnotations.add(Annotations.EXPR_TREND);
	}

	public static void init(String... varargs) throws Exception {
		addAnnotations();
		String popFile = null;
		if (varargs == null || varargs.length == 0 || varargs[0].length() == 0) {
			popFile = "config/xpressoConfig.properties";
		} else {
			popFile = varargs[0];
		}

		InputStream is = FileIO.getInputStreamFromFileName(popFile);
		XpressoProperties = new Properties();
		XpressoProperties.load(is);
		getLanguages(XpressoProperties.getProperty("language.names", "english"));

		STORMPATH_APPLICATION_NAME = XpressoProperties.getProperty("stormpath.application.name", "XpressoApi");
		if (STORMPATH_APPLICATION_NAME.equalsIgnoreCase("xpressoApi")) {
			STORMPATH_CREDENTIALS = "config/apiKey_xpresso.properties";
		} else {
			STORMPATH_CREDENTIALS = "config/apiKey_test.properties";
		}

		XPRESSO_DELIMITER = XpressoProperties.getProperty("xpresso.delimiter", "#&#abz#&#");

		LIB_DIR = XpressoProperties.getProperty("lib.dir", "lib");
		RESOURCES_DIR = XpressoProperties.getProperty("resources.dir", "resources");
		RESOURCES_STATIC_DIR = XpressoProperties.getProperty("resources.static.dir", "resources-static");
		DATA_DIR = XpressoProperties.getProperty("data.dir", "data");
		MODELS_DIR = XpressoProperties.getProperty("models.dir", "models");
		INPUT_DIR = XpressoProperties.getProperty("input.dir", "input");
		OUTPUT_DIR = XpressoProperties.getProperty("output.dir", "output");

		SUPER_USERS_FILE = sharedResourceName(RESOURCES_DIR, XpressoProperties.getProperty("super_users.file", "super_users.txt"));
		REPLACE_PATTERNS_FILE = sharedResourceName(RESOURCES_DIR, XpressoProperties.getProperty("replace.patterns.file", "preprocess-replace.txt"));
		WORDNET_LOC = sharedResourceName(RESOURCES_STATIC_DIR, XpressoProperties.getProperty("wordnet.dir", "WordNet-3.1"));
		SENTI_WORDNET_FILE = sharedResourceName(RESOURCES_STATIC_DIR, XpressoProperties.getProperty("senti.wordnet.file", "SentiWordNet_3.0.0_20130122.txt"));
		DOMAIN_NAMES_LIST = XpressoProperties.getProperty("domain.names", "hospitality");
		DOMAIN_KB_DIR = XpressoProperties.getProperty("domainKB.location", "./domainKB");
		DOMAIN_CONFIG_FILE = DOMAIN_KB_DIR + "/" + XpressoProperties.getProperty("domain.config.file", "domainConfig.json");
		addResources(DOMAIN_KB_DIR, XpressoProperties.getProperty("domain.concepts.kb", "conceptsKB.txt"), DOMAIN_CONCEPTS_FILE);
		addResources(DOMAIN_KB_DIR, XpressoProperties.getProperty("domain.nonsentiment.file", "domainNonSentiment.json"), DOMAIN_NONSENTIMENT_FILE);
		addResources(DOMAIN_KB_DIR, XpressoProperties.getProperty("implicit.aspects.words", "implicit-aspects-words.txt"), IMPLICIT_ASPECT_LEXICON);
		addResources(DOMAIN_KB_DIR, XpressoProperties.getProperty("intent.rule.file", "intent_rules.txt"), INTENT_RULE_FILE);
		IS_SUBJECT_MODE = Boolean.parseBoolean(XpressoProperties.getProperty("subject.mode", "false"));
		CONCEPTS_FILENAME = XpressoProperties.getProperty("concepts.file.name", "domainConcepts.txt");
		POSSIBLE_ASPECTS_FILENAME = XpressoProperties.getProperty("possible.aspects.file.name", "possibleAspects.txt");
		addResources(DOMAIN_KB_DIR, XpressoProperties.getProperty("contextual.sentiment.file.name", "contextualSentiment.txt"), CONTEXTUAL_SENTI_FILENAME);
		MeSH_DB_LOC = XpressoProperties.getProperty("mesh.db.location", "./domainKB/healthCare/conceptsMeSH.txt");
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("pronoun_words.file", "pronoun-words.txt"), PRONOUN_WORDS_FILE_LOCATION);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("weak_verbs.file", "weak-verbs.txt"), WEAK_VERBS_FILE_LOCATION);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("stop_words.file", "stopwords_refined.txt"), STOPWORDS_FILE);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("dictionary.file", "dict_words.txt"), DICTIONARY_FILE);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("resources.words", "resources-words.txt"), RESOURCES_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("consume.words", "consume-words.txt"), CONSUME_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("possession.words", "possession-words.txt"), POSSESSION_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("non.possession.words", "non-possession-words.txt"), NON_POSSESSION_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("need.words", "need-words.txt"), NEED_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("decrease.words", "decrease-words.txt"), DECREASE_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("increase.words", "increase-words.txt"), INCREASE_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("intensifier.words", "intensifier-words.txt"), INTENSIFIER_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("negative.words", "negative-words.txt"), NEGATIVE_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("positive.words", "positive-words.txt"), POSITIVE_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("uc.negative.words", "unconditional-negative-words.txt"), UC_NEGATIVE_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("uc.positive.words", "unconditional-positive-words.txt"), UC_POSITIVE_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("shifter.words", "shifter-words.txt"), SHIFTER_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("NPI.words", "NPI-words.txt"), NPI_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("PPI.words", "PPI-words.txt"), PPI_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("non_senti.words", "non-senti-words.txt"), NON_SENTI_WORDS);
		EMOTICONS_FILE = sharedResourceName(RESOURCES_DIR, XpressoProperties.getProperty("emoticons.file", "emoticons.txt"));
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("idioms.file", "idioms.txt"), IDIOMS_FILE);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("phrasal.verbs.file", "phrasal-verbs.txt"), PHRASAL_VERBS_FILE);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("emotion.kb.file", "NRC-emotions-lexicon-filtered.txt"), EMOTION_KB_FILE);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("pos.tags.file", "pos-tags.txt"), POS_TAGS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("greetings.salutations.file", "greetings-salutations.txt"), GREETINGS_SALUTATIONS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("abstract.concepts.file", "abstract-concepts.txt"), ABSTRACT_CONCEPTS_FILE);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("suffix.file", "suffix-list.txt"), SUFFIX_FILE);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("intent.action.file", "intent-action.txt"), INTENT_ACTION_FILE);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("intent.object.file", "intent-object.txt"), INTENT_OBJECT_FILE);
		UNLISTED_ENTITY_FILE = sharedResourceName(RESOURCES_DIR, XpressoProperties.getProperty("unlisted.entity.file", "unlisted_entities.txt"));
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("abuse.words", "abuse-words.txt"), ABUSE_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("advocate.words", "advocate-words.txt"), ADVOCATE_WORDS);
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("suggestion.words", "suggestion-words.txt"), SUGGESTION_WORDS);
		SEARCH_ENGINE_MAX_SEED = Integer.parseInt(XpressoProperties.getProperty("searchengine.max.seed", "3"));
		addModels("senti", SENTI_MODEL);
		addModels("stmt", STMT_MODEL);
		INPUT_REVIEW_FILENAME = INPUT_DIR + "/" + XpressoProperties.getProperty("input.review.file.name", "woven_data_latest.csv");
		REVIEW_LANGUAGE = convertLanguage(XpressoProperties.getProperty("review.language", "EN"));
		REVIEW_DELIMITER = XpressoProperties.getProperty("review.delimiter", "\\t");
		REVIEW_FIELD = Integer.parseInt(XpressoProperties.getProperty("review.field", "4"));
		MAX_REVIEW_LENGTH = Integer.parseInt(XpressoProperties.getProperty("max.review.length", "0"));
		EXTRACTED_ENTITY_FILENAME = OUTPUT_DIR + "/" + XpressoProperties.getProperty("extracted.entity.file.name", "extracted-entity.csv");
		GRAPHDB_SERVER_URI = XpressoProperties.getProperty("graphdb.server", "http://localhost:7474");
		USE_NEO4J = Boolean.parseBoolean(XpressoProperties.getProperty("use.neo4j", "false"));
		DB_NAME = XpressoProperties.getProperty("db.name", "dbSingapore");
		DB_USER = XpressoProperties.getProperty("db.username", "root");
		DB_PASS = XpressoProperties.getProperty("db.password", "123");
		// UNDO_QUERY_FILE = DOMAIN_KB_DIR + "/" + DOMAIN_NAME + "/" +
		// XpressoProperties.getProperty("undo.query.file", "undo_query.txt");
		// DOMAIN_GRAPH_FILE = DOMAIN_KB_DIR + "/" + DOMAIN_NAME + "/" +
		// XpressoProperties.getProperty("domain.graph.file",
		// "hospitality_domain_db.txt");
		UNDO_QUERY_FILE = DOMAIN_KB_DIR + "/" + DOMAIN_NAME + "/" + XpressoProperties.getProperty("undo.query.file", "undo_query.txt");
		DOMAIN_GRAPH_FILE = DOMAIN_KB_DIR + "/" + DOMAIN_NAME + "/" + XpressoProperties.getProperty("domain.graph.file", "domain_db.txt");
		USE_GRAPH_DB = Boolean.parseBoolean(XpressoProperties.getProperty("use.graphdb", "false"));
		USE_EMBEDDED_GRAPHDB = Boolean.parseBoolean(XpressoProperties.getProperty("use.embedded.graphdb", "false"));
		UPLOAD_STATIC_LEXICON = Boolean.parseBoolean(XpressoProperties.getProperty("upload.static.lexicon", "false"));
		UNDO_PREVIOUS_GRAPH = Boolean.parseBoolean(XpressoProperties.getProperty("undo.previous.graph", "false"));
		BUILD_DOMAIN_GRAPH = Boolean.parseBoolean(XpressoProperties.getProperty("build.domain.graph", "false"));
		EMBEDDED_DB_PATH = XpressoProperties.getProperty("embedded.graphdb", "C:\\Users\\Antarip Biswas\\Documents\\Neo4j\\core.graphdb");
		DOCUMENT_CLASSIFY_APPROACH = Integer.parseInt(XpressoProperties.getProperty("document.classify.approach", "0"));

		/**
		 * Enabling trend in activator
		 */
		ENABLE_TREND_ACTIVATOR = Boolean.parseBoolean(XpressoProperties.getProperty("enable.trend.activator", "false"));
		/**
		 * XpEngine init resources load
		 */
		LOAD_DOC_CLASSIFIER = Boolean.parseBoolean(XpressoProperties.getProperty("document.classifier.load", "true"));
		LOAD_FUZZY_ENTITY_RECOGNIZER = Boolean.parseBoolean(XpressoProperties.getProperty("fuzzy.entity.recognizer.load", "true"));
		LOAD_HASHTAG_PROCESSOR = Boolean.parseBoolean(XpressoProperties.getProperty("hashtag.processor.load", "true"));
		LOAD_MICROTEXT_PROCESSOR = Boolean.parseBoolean(XpressoProperties.getProperty("microtext.processor.load", "true"));
		LOAD_ML_CLASSIFIER = Boolean.parseBoolean(XpressoProperties.getProperty("xpml.classifier.load", "true"));
		LOAD_ONTOLOGY_UTILS = Boolean.parseBoolean(XpressoProperties.getProperty("ontology.utils.load", "true"));
		LOAD_PHRASE_SCORE = Boolean.parseBoolean(XpressoProperties.getProperty("phrase.score.load", "true"));
		LOAD_SWN_SENTIMENT = Boolean.parseBoolean(XpressoProperties.getProperty("swnsentiment.load", "true"));
		LOAD_TREND_DETECTION = Boolean.parseBoolean(XpressoProperties.getProperty("trend.detection.load", "true"));
		LOAD_WALMART_UTILS = Boolean.parseBoolean(XpressoProperties.getProperty("walmart.utils.load", "true"));
		LOAD_WV_ASPECT_MATCH = Boolean.parseBoolean(XpressoProperties.getProperty("wv.aspect.match.load", "true"));
		LOAD_WV_BOTTOMUP_MATCH = Boolean.parseBoolean(XpressoProperties.getProperty("wv.bottomup.match.load", "true"));
		LOAD_WV_UTILS = Boolean.parseBoolean(XpressoProperties.getProperty("wv.utils.load", "true"));
		LOAD_XP_EMOTION = Boolean.parseBoolean(XpressoProperties.getProperty("xpemotion.load", "true"));
		LOAD_XP_INTENTION = Boolean.parseBoolean(XpressoProperties.getProperty("xpintention.load", "true"));
		LOAD_XP_ONTOLOGY = Boolean.parseBoolean(XpressoProperties.getProperty("xpontology.load", "true"));
		LOAD_XP_SENTIMENT = Boolean.parseBoolean(XpressoProperties.getProperty("xpsentiment.load", "true"));
		LOAD_XP_IO = Boolean.parseBoolean(XpressoProperties.getProperty("xpio.load", "true"));
		LOAD_LUCENE = Boolean.parseBoolean(XpressoProperties.getProperty("lucene.load", "true"));

		/**
		 * glove vector resource file location load
		 */
		WORD_VECTOR_FILE = XpConfig.INPUT_DIR + "/" + XpressoProperties.getProperty("wv.vector.file", "glove_vector.txt");
		WORD_VECTOR_CLUSTERS_FILE = XpressoProperties.getProperty("wv.clusters.file", "AllEntitiesClustersBottomUp.txt");
		addResources(RESOURCES_DIR, XpressoProperties.getProperty("emotion.seed.file", "emotion_seed.txt"), EMOTION_SEED_FILE);
		QA_TAGGED_ASPECT_FILE = XpConfig.DOMAIN_KB_DIR + "/" + XpressoProperties.getProperty("qa.tagged.aspects.file", "qaTaggedAspects.tsv");

		MONGO_WV_READ = Boolean.parseBoolean(XpressoProperties.getProperty("mongo.wv.read", "false"));
		MONGO_HOST = XpressoProperties.getProperty("mongo.host", "localhost");
		MONGO_DB = XpressoProperties.getProperty("mongo.db", "mydb");
		MONGO_USERNAME = XpressoProperties.getProperty("mongo.username", "abzAdmin");
		MONGO_PASSWORD = XpressoProperties.getProperty("mongo.password", "Abz00ba1nc");
		MONGO_PORT = Integer.parseInt(XpressoProperties.getProperty("mongo.port", "27017"));
		MONGO_COLLECTION = XpressoProperties.getProperty("mongo.collection", "word2vecCollection");
		MONGO_FILE = XpressoProperties.getProperty("mongo.file", "input/word2vec/partsaa");
		MONGO_WV_LOAD = Boolean.parseBoolean(XpressoProperties.getProperty("mongo.wv.load", "false"));
		MONGO_CREATE_INDEX = Boolean.parseBoolean(XpressoProperties.getProperty("mongo.create.index", "false"));

		/**
		 * twitter resources load
		 */
		MICROTEXT_RESOURCE_PATH = sharedResourceName(RESOURCES_STATIC_DIR, XpressoProperties.getProperty("microtext.resource.path", "twitter"));
		MICROTEXT_RESOURCE_FILES = XpressoProperties.getProperty("microtext.resource.files", "proper_names,celebs,videogame,mobyplaces,family,female,male");
		MICROTEXT_TAGGER_MODEL = MICROTEXT_RESOURCE_PATH + "/" + XpressoProperties.getProperty("microtext.tagger.model", "model.ritter_ptb_alldata_fixed.20130723");
		MICROTEXT_TAG_DICT = MICROTEXT_RESOURCE_PATH + "/" + XpressoProperties.getProperty("microtext.tagger.dict", "tagdict.txt");
		CLUSTER_RESOURCE_NAME = MICROTEXT_RESOURCE_PATH + "/" + XpressoProperties.getProperty("cluster.resource.name", "50mpaths2");

		DATE_FORMAT = XpressoProperties.getProperty("date.format", "yyyyMMdd_HHmmss");
		MAX_THREADS = Integer.parseInt(XpressoProperties.getProperty("max.engine.threads", "10"));
		MAX_LOG_SIZE = Integer.parseInt(XpressoProperties.getProperty("log.max.file.size", "50000000"));
		MAX_FILE_COUNT = Integer.parseInt(XpressoProperties.getProperty("log.max.file.count", "50"));

		STANFORD_USE_SRPARSER = Boolean.parseBoolean(XpressoProperties.getProperty("stanford.use_sr_parser", "true"));
		STANFORD_USE_CASELESS = Boolean.parseBoolean(XpressoProperties.getProperty("stanford.use.caseless", "false"));
		STANFORD_SP_DEPPARSE = Boolean.parseBoolean(XpressoProperties.getProperty("stanford.sp.depparse", "true"));
		STANFORD_SP_DEPPARSE_MODEL = XpressoProperties.getProperty("stanford.sp.depparse.model", "nndep.spanish09.model.txt.gz");
		processStanfordAnnotators(XpressoProperties.getProperty("stanford.annotators", "english:tokenize,ssplit,pos,lemma,parse"));

		ENABLE_BING_SEARCH = Boolean.parseBoolean(XpressoProperties.getProperty("enable.bing.search", "false"));

		IS_WORDVECTOR_ASPECTS_MATCH = Integer.parseInt(XpressoProperties.getProperty("wv.aspects.match", "0"));

		IS_WORDVECTOR_CATEGORIES_LOAD = Boolean.parseBoolean(XpressoProperties.getProperty("load.wv.categories", "false"));
		IS_WORDVECTOR_CATEGORIES_EN_LOAD = Boolean.parseBoolean(XpressoProperties.getProperty("load.wv.categories.en", "false"));
		IS_WORDVECTOR_CATEGORIES_SP_LOAD = Boolean.parseBoolean(XpressoProperties.getProperty("load.wv.categories.sp", "false"));
		IS_WORDVECTOR_DOCUMENT_LOAD = Boolean.parseBoolean(XpressoProperties.getProperty("load.wv.document", "false"));
		USE_LSH = Boolean.parseBoolean(XpressoProperties.getProperty("use.lsh", "false"));

		TMOBILE_KB_FILE = DOMAIN_KB_DIR + "/" + XpressoProperties.getProperty("tmobile.conceptKB", "tmobile/tmobileconcepts.txt");
		XPRESSO_VERSION = XpressoProperties.getProperty("xpresso.version", "3.1.5");

		USE_CACHE = Boolean.parseBoolean(XpressoProperties.getProperty("use.cache", "true"));
		MAX_CACHE_SIZE = Integer.parseInt(XpressoProperties.getProperty("max.cache.size", "1000000"));

		ENTITY_DOC_COUNT_FILE = DATA_DIR + "/" + XpressoProperties.getProperty("entity.doc.count.file", "doc_entity_trend.txt");
		IDF_DOWNGRADE = Boolean.parseBoolean(XpressoProperties.getProperty("idf.downgrade", "false"));
		UP_FACTOR = Double.parseDouble(XpressoProperties.getProperty("up.factor", "1.0"));
		TREND_DETECTION_THREADS = Integer.parseInt(XpressoProperties.getProperty("trend.detection.threads", "10"));
		TREND_IDF_COMPUTE_METHOD = Integer.parseInt(XpressoProperties.getProperty("trend.idf.compute.method", "1"));
		DOWNFACTOR_POWER = Double.parseDouble(XpressoProperties.getProperty("downfactor.power", "1.0"));
		DECAY_FACTOR = Double.parseDouble(XpressoProperties.getProperty("decay.factor", "0.001"));
		TREND_THRESHOLD = Double.parseDouble(XpressoProperties.getProperty("trend.threshold", "0.5"));
		MAX_TREND_TOPIC = Integer.parseInt(XpressoProperties.getProperty("max.trend.topic", "25"));
		BATCH_RUN_KEYWORD_SIGNIFICANCE = Boolean.parseBoolean(XpressoProperties.getProperty("batch.run.keyword.significance", "false"));
		TREND_TYPE = Integer.parseInt(XpressoProperties.getProperty("trend.type", "1"));
		TREND_BATCH_RUN = Boolean.parseBoolean(XpressoProperties.getProperty("trend.batch.run", "false"));
		ENTITY_ABUNDANCE_TYPE = Integer.parseInt(XpressoProperties.getProperty("entity.abundance.type", "1"));
		TREND_REVIEW_COUNT = Integer.parseInt(XpressoProperties.getProperty("trend.review.count", "100"));
		MIN_TREND_TOPIC_COUNT = Integer.parseInt(XpressoProperties.getProperty("min.trend.topic.count", "10"));

		WV_DATABASE_TYPES = XpressoProperties.getProperty("wv.db.types", "mongo,memsql");
		WV_DATABASE_USE = Integer.parseInt(XpressoProperties.getProperty("wv.db.use", "0"));

		int loggerLevel = Integer.parseInt(XpressoProperties.getProperty("logger.level", "1"));
		switch (loggerLevel) {
			case (0):
				LOGGER_LEVEL = Level.OFF;
				break;
			case (1):
				LOGGER_LEVEL = Level.ALL;
				break;
			case (2):
				LOGGER_LEVEL = Level.ERROR;
				break;
			case (3):
				LOGGER_LEVEL = Level.WARN;
				break;
			default:
				LOGGER_LEVEL = Level.ALL;
		}
	}

}