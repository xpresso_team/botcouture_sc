/**
 * @author Alix Melchy Jun 2, 2015 6:48:43 PM
 */
package  com.abzooba.xpresso.engine.core.languages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.textanalytics.CoreNLPController;

import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.CoreMap;

public class XpTextInterfaceSP {

    public static String preProcess(String original_text) {
        String normalized_text;
        Pattern isWord = Pattern.compile("\\b\\s*$", Pattern.UNICODE_CHARACTER_CLASS);
        normalized_text = isWord.matcher(original_text).replaceAll(".");
        //
        //		normalized_text = normalized_text.replaceAll("(\\w+)/(\\w+)", "$1 or $2");
        //		/* If "....." exists in sentence, then replace it with . and a space */
        //		normalized_text = normalized_text.replaceAll("\\.\\.+", ". ");
        //		/* If no space exists after a ".". Then add a space */
        //		normalized_text = normalized_text.replaceAll("\\.([A-Z][^\\.])", ". $1");
        //		/*
        //		 * If a lowercaps letter after . and " " then replace with capitalized
        //		 * version
        //		 */
        //		normalized_text = normalized_text.replaceAll("\\.([a-z][^\\.])", ". " + "$1".toUpperCase());
        //		The following purges non-ASCII characters, which should not be done in Spanish
        //		normalized_text = normalized_text.replaceAll("[^\\x00-\\x7F]", "");
		/* Parking Lot - Crowded with reckless and rude drivers */
        //		normalized_text = normalized_text.replaceAll(" - ", " is "); possible replacements: es/estÃ¡
        normalized_text = normalized_text.replaceAll(" NO ", " no ");
        Pattern noCommaBeforePero = Pattern.compile("([^,]) pero ", Pattern.UNICODE_CHARACTER_CLASS);
        normalized_text = noCommaBeforePero.matcher(normalized_text).replaceAll("$1, pero ");
        //		normalized_text = normalized_text.replaceAll("\\s+", " ");

        //		normalized_text = normalized_text.replaceAll("TMobile", "Tmobile").replaceAll("T Mobile", "Tmobile").replaceAll("T-Mobile", "Tmobile").replaceAll("iphone", "iPhone").replaceAll("hone6 plus", "hone6 Plus").replaceAll("hone 6 plus", "hone6 Plus");

        //		normalized_text = ProcessString.resolveReplacePatterns(normalized_text);
        //		normalized_text = ProcessString.resolveApostrophe(normalized_text);
        //		List<String> initialTokensList = ProcessString.tokenizeString(Languages.SP, normalized_text);
        //		for (String token : initialTokensList) {
        //			if (token.startsWith("#")) {
        //				List<String> hashTagProcessedStr = HashTagProcessor.parseHashTag(Languages.SP, token);
        //				StringBuilder sb = new StringBuilder();
        //				if (hashTagProcessedStr != null) {
        //					for (String hashTagStr : hashTagProcessedStr) {
        //						sb.append(hashTagStr).append(" ");
        //					}
        //					String replaceStr = sb.toString().trim();
        //					normalized_text = normalized_text.replaceAll(token, replaceStr);
        //				}
        //			}
        //		}

        return normalized_text;
    }

    //	protected void annotateDocument(Annotation document) {
    //		CoreNLPController.getPipeline().annotate(document);
    ////		CoreNLPControllerSP.getPipeline().annotate(document);
    //	}

    //	public SemanticGraph getDependencyGraph(CoreMap sentence) {
    //
    //		SemanticGraph dependency_graph = something to replace the absent dependencyGraph in CoreNLP
    //		return dependency_graph;
    //
    //	}

    public static void featurizePOSbased(String pos, XpFeatureVector featureSet) {
        //		String posHead = pos.substring(0, Math.min(pos.length(), 3));
        //		Stanford simplifies AnCora PoS tags: impossible to identify comparatives and superlatives
        String posHead = pos.substring(0, Math.min(pos.length(), 2));
        switch (posHead) {
            //			case ("aqc"):
            //				featureSet.setCompAdjFeature(true);
            //				break;
            //			case ("aqs"):
            //				featureSet.setSupAdjFeature(true);
            //				break;
            //			case ("aqa"):case ("aqd"):
            case ("aq"):
                featureSet.setAdjFeature(true);
                break;
            case ("rg"):
                featureSet.setAdvbFeature(true);
                break;
        }

        //		if (pos.startsWith("aqc")) {
        //			featureSet.setCompAdjFeature(true);
        //		} else if (pos.startsWith("aqs")) {
        //			featureSet.setSupAdjFeature(true);
        //		} else if (pos.startsWith("aq")) {
        //			featureSet.setAdjFeature(true);
        //		} else if (pos.startsWith("rg")) {
        //			featureSet.setAdvbFeature(true);
        //		}
        //
    }

    public static List<String> getTokens(String domainName, CoreMap sentence, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, XpFeatureVector featureSet) {

        List<String> tokenList = new ArrayList<String>();

        int fwCount = 0;
        List<CoreLabel> coreLabelList = sentence.get(TokensAnnotation.class);

        for (CoreLabel token : coreLabelList) {

            String tokenWord = token.word();
            if ((tokenWord.length() > 2) && (!tokenWord.matches("\\W+")) && (tokenWord.toUpperCase()).equals(tokenWord)) {
                featureSet.setCapitalizedPresentFeature(true);
            }

            String word = tokenWord.toLowerCase();
            String lemma = CoreNLPController.lemmatizeToString(Languages.SP, word);
            //	String pos = token.get(PartOfSpeechAnnotation.class);
            String pos = token.tag();
            String key = token.word() + "/" + pos;
            //			String lemma = CoreNLPControllerSP.lemmatizeToString(word);
            //			String lemma = token.lemma();
            CoreNLPController.addToLemmaMap(Languages.SP, key, lemma.toLowerCase());
            //			CoreNLPControllerSP.addToLemmaMap(word, lemma.toLowerCase());

            if (!word.matches("\\W+")) {

                // System.out.println("Before Word: " + word);
                word = word.replaceAll("\\.", "");
                //String pos = token.tag();
                //In AnCora (Spanish PoS tags) no FW tag, its equivalent is nc00000
                if (pos.equals("nc00000")) {
                    fwCount += 1;
                }
                featurizePOSbased(pos, featureSet);
                //				if (SentimentEngine.getDomainNonSentimentTag(Languages.SP, word.toLowerCase(), domainName)) {
                //					tagSentimentMap.put(word.toLowerCase(), XpSentiment.NON_SENTI_TAG);
                //				}
                wordPOSmap.put(word, pos);
                tokenList.add(word);
            }
            //			else if (word.equals(".")) {
            //				featureSet.setPeriodPresentFeature(true);
            //			}
        }

        // if (fwCount >= 2) {
        int length = tokenList.size();
        double percentageFW = 0;
        if (length > 0) {
            percentageFW = (double) ((double) fwCount / (double) tokenList.size());
        }
        if (percentageFW > 0.5) {
            return null;
        }
        return tokenList;
    }

}