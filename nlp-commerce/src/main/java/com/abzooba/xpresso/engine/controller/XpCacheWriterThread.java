/**
 *
 */
package com.abzooba.xpresso.engine.controller;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.engine.config.XpConfig;
import org.slf4j.Logger;


/**
 * @author Koustuv Saha
 * Sep 6, 2015 8:54:50 AM
 * XpressoV3.0  XpCacheWriterThread
 * @param <K>
 * @param <V>
 */
public class XpCacheWriterThread<K, V> implements Runnable {

    Map<K, V> cacheMap;
    K key;
    V value;
    Set<K> nullHash;
    protected static final Logger logger = XCLogger.getSpLogger();

    private static ExecutorService executor;

    public static void init() {
        executor = Executors.newFixedThreadPool(20);

    }

    public XpCacheWriterThread(K key, V value, Map<K, V> cacheMap, Set<K> nullHash) {
        this.cacheMap = cacheMap;
        this.key = key;
        this.value = value;
        this.nullHash = nullHash;
        executor.execute(this);
    }

    public XpCacheWriterThread(K key, V value, Map<K, V> cacheMap) {
        this.cacheMap = cacheMap;
        this.key = key;
        this.value = value;
        executor.execute(this);
    }

    @Override
    public void run() {
        if (XpConfig.USE_CACHE) {

            if (value != null) {
                this.cacheMap.put(key, value);
            }
            //			else if (nullHash != null) {
            //				logger.info("Adding to nullHash: " + this.key);
            //				this.nullHash.add(this.key);
            //			}
            if (this.cacheMap.size() > XpConfig.MAX_CACHE_SIZE) {
                this.cacheMap.clear();
            }
            if (nullHash != null && nullHash.size() > XpConfig.MAX_CACHE_SIZE) {
                this.nullHash.clear();
            }
        }

        //		this.stop();

        //		this.
        //		if (!cacheMap.containsKey(key)) {
        //			cacheMap.put(key, value);
        //						if (cacheMap.size() > 1000000) {
        //							cacheMap.clear();
        //						}
        //		}
    }
}