/**
 *
 */
package  com.abzooba.xpresso.engine.core;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.document.DocumentClassifier;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;

/**
 * @author Koustuv Saha
 * Oct 8, 2015 7:13:49 PM
 * XpressoV3.0  XpDocumentClassifier
 */
public class XpDocumentClassifier extends XpText {

    public XpDocumentClassifier(Languages language, String reviewStr) {
        super(language, reviewStr, false);
    }

    protected JSONObject getDocumentCategory() {
        // TODO Auto-generated method stub
        JSONObject docJSON = new JSONObject();
        String[] category = DocumentClassifier.classifyDocument(this.normalized_text);
        //		String[] category = JobDescriptionClassifier.classifyDocument(this.normalized_text);
        try {
            docJSON.put("Document", this.normalized_text);
            if (category != null) {
                docJSON.put("Document Sub-Category", category[0]);
                docJSON.put("Document Category", category[1]);
            } else {
                docJSON.put("Document Sub-Category", "");
                docJSON.put("Document Category", "");
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return docJSON;
    }

}