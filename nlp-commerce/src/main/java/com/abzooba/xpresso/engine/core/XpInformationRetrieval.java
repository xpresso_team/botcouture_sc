/**
 *
 */
package com.abzooba.xpresso.engine.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.abzooba.xpresso.cls.ConceptsMiner;
import com.abzooba.xpresso.cls.ConceptsVector;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;

import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.util.CoreMap;

/**
 * @author Koustuv Saha
 * 13-Oct-2014 8:20:11 pm
 * XpressoV2.7  XpInformationRetrieval
 */
public class XpInformationRetrieval extends XpText {

    /**
     * @param reviewID
     * @param domainName
     * @param subject
     * @param reviewStr
     */

    private String docID = null;
    private Map<String, String> entitiesNERmap = null;
    private Set<String> conceptsSet = null;
    // private final Set<String> conceptsSet;

    private List<String> manualPhraseList = null;

    //	public XpInformationRetrieval(String reviewID, String domainName, String subject, String reviewStr) {
    //		super(reviewID, domainName, subject, reviewStr);
    //		this.conceptsSet = new HashSet<String>();
    //		entitiesNERmap = new HashMap<String, String>();
    //		this.setDocID(reviewID);
    //		this.processPipeline();
    //	}

    public XpInformationRetrieval(String reviewID, String reviewStr, List<String> manualPhraseList) {
        super(reviewStr);
        this.conceptsSet = new HashSet<String>();
        entitiesNERmap = new HashMap<String, String>();
        this.setDocID(reviewID);
        if (manualPhraseList == null) {
            this.processPipeline();
        } else {
            this.manualPhraseList = manualPhraseList;
            this.processPipelineManual();
        }

    }

    public String getDocID() {
        return docID;
    }

    public void setDocID(String docID) {
        this.docID = docID;
    }

    public Set<String> getConceptsSet() {
        return this.conceptsSet;
    }

    public Map<String, String> getEntitiesMap() {
        return this.entitiesNERmap;
    }

    private void processPipeline() {

        List<CoreMap> sentencesList = getSentences();
        for (CoreMap sentence : sentencesList) {
            String snippet = sentence.toString();
            XpFeatureVector featureSet = new XpFeatureVector();
            SemanticGraph semanticDepGraph = null;

            semanticDepGraph = this.getDependencyGraph(sentence);
            logger.info("Dependency Graph: " + semanticDepGraph.toString(SemanticGraph.OutputFormat.LIST));

            ConceptsMiner cmi = new ConceptsMiner(semanticDepGraph, null, snippet, featureSet);
            List<ConceptsVector> conceptsMinedList = cmi.mineConcepts();
            for (ConceptsVector cv : conceptsMinedList) {
                String currConcept = cv.getConcept();
                if (currConcept.contains(" NOUN") || currConcept.contains("NEG ")) {
                    continue;
                }
                currConcept = currConcept.replaceAll(" NOUN", "").replaceAll("NEG ", "");
                currConcept = currConcept.replaceAll("[^\\w\\s]", "");
                this.conceptsSet.add(currConcept.trim());
            }
            this.entitiesNERmap.putAll(getNERsentence(sentence));
        }
    }

    // private void processPipeline() {
    //
    // List<CoreMap> sentencesList = getSentences();
    // for (CoreMap sentence : sentencesList) {
    // String snippet = sentence.toString();
    // XpFeatureSet featureSet = new XpFeatureSet();
    // SemanticGraph semanticDepGraph = null;
    //
    // semanticDepGraph = this.getDependencyGraph(sentence);
    // logger.info( "Dependency Graph: " +
    // semanticDepGraph.toString(SemanticGraph.OutputFormat.LIST));
    //
    // ConceptsMiner cmi = new ConceptsMiner(semanticDepGraph, null, snippet,
    // featureSet);
    // List<ConceptsVector> conceptsMinedList = cmi.mineConcepts();
    // for (ConceptsVector cv : conceptsMinedList) {
    // String currConcept = cv.getConcept();
    // currConcept = currConcept.replaceAll(" NOUN", "").replaceAll("NEG ", "");
    // currConcept = currConcept.replaceAll("[^\\w\\s]", "");
    // this.conceptsSet.add(currConcept.trim());
    // }
    // System.out.println("sentence: " + sentence);
    // this.entitiesNERmap.putAll(this.getNERsentence(sentence));
    // }
    // computeDocumentMatrix();
    // }

    private void processPipelineManual() {

        this.conceptsSet.addAll(this.manualPhraseList);

        // for (String phrase : this.manualPhraseList) {
        // this.conceptsSet.add(phrase);
        // }
    }

}