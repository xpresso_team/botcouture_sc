/**
 * 
 */
package com.abzooba.xpresso.engine.core;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author Koustuv Saha
 * 09-Jul-2014 2:52:34 pm
 * XpressoV2.0.1  XpLexMatch
 */
public class XpLexMatch {

	/**
	 * Matches each sequence
	 * @param sequenceCollection
	 * @param text
	 * @return
	 */
	// public static boolean sequenceMatch(Collection<String>
	// sequenceCollection, String text) {
	public static String sequenceMatch(Collection<String> sequenceCollection, String text) {
		text = text.toLowerCase();

		for (String sequence : sequenceCollection) {
			if (text.matches(sequence)) {
				return sequence;
			}
		}
		return null;
	}

	public static String sequenceMatch(Map<String, String> sequenceMap, String text) {
		text = text.toLowerCase();

		if (sequenceMap != null) {
			for (Entry<String, String> entry : sequenceMap.entrySet()) {
				if (text.matches(".*\\b" + entry.getKey() + "\\b.*")) {
					return entry.getValue();
				}
			}
		}

		return null;
	}

	public static String sequenceContain(Collection<String> sequenceCollection, String text) {

		text = text.toLowerCase();

		for (String sequence : sequenceCollection) {
			if (text.contains(sequence)) {
				return sequence;
			}
		}
		return null;
	}

	public static boolean isCapitalized(String seq, String text) {

		String capSeq = seq.replaceAll("\\\\b", "").replaceAll("\\.", "").replaceAll("\\*", "").toUpperCase();
		// System.out.println("Capitalized seq: " + capSeq);
		if (text.contains(capSeq)) {
			return true;
		}
		return false;
	}

	// public static boolean sequenceContain(Collection<String>
	// sequenceCollection, String text) {
	// for (String sequence : sequenceCollection) {
	// if (text.contains(sequence)) {
	// return true;
	// }
	// }
	// return false;
	// }

	public static String sequenceContain(Map<String, String> sequenceMap, String text) {
		// for (String sequence : sequenceCollection) {
		// if (text.contains(sequence)) {
		// return true;
		// }
		// }

		if (sequenceMap != null) {
			for (Entry<String, String> entry : sequenceMap.entrySet()) {
				String sequence = entry.getKey();
				if (text.toLowerCase().contains(sequence)) {
					//				System.out.println("Matched: " + sequence + "\t" + text + "\tReturning: " + entry.getValue());
					return entry.getValue();
				}
			}
		}
		return null;
	}

	public static int sequenceStartsWith(Set<String> sequenceSet, String text) {
		int length = 0;
		if (sequenceSet != null) {
			for (String entry : sequenceSet) {
				/* Exhaustive search as the file is not ordered according to decreasing length. Issue was "hi" matched with "hiring better staff"*/
				//				if (text.startsWith(entry) && (entry.split(" ").length > length)) {
				if (text.matches("\\b" + entry + "\\b") && (entry.split(" ").length > length)) {
					//					System.out.println("Entry which was found: " + entry + " for " + text);
					length = entry.split(" ").length;
				}
			}
		}

		return length;
	}

	public static Map<Integer, String> sequenceStartsWith(Map<String, String> sequenceMap, String text) {
		String tag = null;
		int offset = 0;
		Map<Integer, String> answ = new HashMap<Integer, String>();
		if (sequenceMap != null) {
			for (Entry<String, String> entry : sequenceMap.entrySet()) {
				String sequence = entry.getKey();
				//				if (text.toLowerCase().startsWith(sequence) && (sequence.split(" ").length > offset)) {
				if (text.toLowerCase().matches("\\b" + sequence + "\\b") && (sequence.split(" ").length > offset)) {
					offset = sequence.split(" ").length;
					tag = entry.getValue();
				}
			}
		}
		answ.put(offset, tag);

		return answ;
	}

	public static void main(String[] args) {
		// String propsFile = "./config/xdt.properties";
		// XpEngine.init(propsFile);

		// String str = "best in anywhere";
		// boolean isPresent = sequenceMatch(SentiLex.getAdvocateHash(), str);
		// System.out.println(isPresent);

		// String sequence = "best\\b.*\\banywhere";
		String sequence = "best*anywhere";
		String regexSeq = sequence.replaceAll("\\*", "\\\\b\\.\\*\\\\b");
		System.out.println(regexSeq);
		regexSeq = ".*\\b" + regexSeq + "\\b.*";

		System.out.println(regexSeq);

		String str = "best in anywhere";
		boolean bool = str.matches(regexSeq);
		System.out.println(bool);

	}
}
