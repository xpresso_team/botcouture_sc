/**
 *
 */
package com.abzooba.xpresso.engine.core;

import java.util.List;
import java.util.Map;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;

import edu.stanford.nlp.util.CoreMap;
import org.slf4j.Logger;

/**
 * @author Koustuv Saha
 * Sep 28, 2015 3:17:46 PM
 * XpressoV3.0  XpTextObject
 */
public class XpSentenceResources {

    protected static final Logger logger = XCLogger.getSpLogger();

    private Map<String, String> wordPOSmap;
    private Map<String, String> nerMap;
    private Map<String, String> tagSentimentMap;
    private Map<String, String[]> tagAspectMap;
    private List<CoreMap> entityMentions;
    private XpFeatureVector xpf;

    public XpSentenceResources(CoreMap sentence, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, Map<String, String[]> tagAspectMap, XpFeatureVector xpf) {

        Map<String, String> nerMap = XpText.getNERsentence(sentence);
        List<CoreMap> entityMentions = XpText.getEntityMentions(sentence);

        logger.info("Entity Mentions for Sentence: " + entityMentions);
        logger.info("NER Map for Sentence: " + nerMap);

        this.wordPOSmap = wordPOSmap;
        this.nerMap = nerMap;
        this.tagSentimentMap = tagSentimentMap;
        this.tagAspectMap = tagAspectMap;
        this.entityMentions = entityMentions;
        this.xpf = xpf;
    }

    /**
     * @return the xpf
     */
    public XpFeatureVector getXpf() {
        return xpf;
    }

    /**
     * @param xpf the xpf to set
     */
    public void setXpf(XpFeatureVector xpf) {
        this.xpf = xpf;
    }

    /**
     * @return the wordPOSmap
     */
    public Map<String, String> getWordPOSmap() {
        return wordPOSmap;
    }

    /**
     * @param wordPOSmap the wordPOSmap to set
     */
    public void setWordPOSmap(Map<String, String> wordPOSmap) {
        this.wordPOSmap = wordPOSmap;
    }

    /**
     * @return the tagSentimentMap
     */
    public Map<String, String> getTagSentimentMap() {
        return tagSentimentMap;
    }

    /**
     * @param tagSentimentMap the tagSentimentMap to set
     */
    public void setTagSentimentMap(Map<String, String> tagSentimentMap) {
        this.tagSentimentMap = tagSentimentMap;
    }

    /**
     * @return the nerMap
     */
    public Map<String, String> getNerMap() {
        return nerMap;
    }

    /**
     * @param nerMap the nerMap to set
     */
    public void setNerMap(Map<String, String> nerMap) {
        this.nerMap = nerMap;
    }

    /**
     * @return the tagAspectMap
     */
    public Map<String, String[]> getTagAspectMap() {
        return tagAspectMap;
    }

    /**
     * @param tagAspectMap the tagAspectMap to set
     */
    public void setTagAspectMap(Map<String, String[]> tagAspectMap) {
        this.tagAspectMap = tagAspectMap;
    }

    /**
     * @return the entityMentions
     */
    public List<CoreMap> getEntityMentions() {
        return entityMentions;
    }

    /**
     * @param entityMentions the entityMentions to set
     */
    public void setEntityMentions(List<CoreMap> entityMentions) {
        this.entityMentions = entityMentions;
    }

}