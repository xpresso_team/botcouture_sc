/**
 *
 */
package com.abzooba.xpresso.engine.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Annotations;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.expressions.sentiment.SWNsentiment;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.expressions.statementtype.StatementTypeLexicon;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.sdgraphtraversal.ConstituencyTree;
import com.abzooba.xpresso.sdgraphtraversal.StanfordDependencyEdges;
import com.abzooba.xpresso.sdgraphtraversal.StanfordDependencyGraph;
import com.abzooba.xpresso.textanalytics.ProcessString;
import com.abzooba.xpresso.textanalytics.VocabularyTests;

import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

/**
 * @author vivek aditya
 *
 * 4:30:45 pm
 * 18-Feb-2016
 * TODO
 */
public class XpExpressionManPower extends XpExpression {

    /**
     * @param language
     * @param annotation
     * @param isHistoricalDemo
     * @param reviewID
     * @param domainName
     * @param subject
     * @param reviewStr
     * @param isMicro
     */
    public XpExpressionManPower(Languages language, Annotations annotation, boolean isHistoricalDemo, String reviewID, String domainName, String subject, String reviewStr, boolean isMicro) {
        super(language, annotation, isHistoricalDemo, reviewID, domainName, subject, reviewStr, isMicro);
    }

    public List<XpDistill> processPipeline() {
        return processPipeline(true);
    }

    protected JSONObject getSentiment() {
        List<XpDistill> reviewTriples = this.processPipeline();
        Map<String, JSONObject> netMap = new LinkedHashMap<String, JSONObject>();
        try {
            for (XpDistill t : reviewTriples) {
                String snippet = t.reviewSnippet;
                JSONObject distillJSON = netMap.get(snippet);
                JSONArray aboutMapArr;
                if (distillJSON == null) {
                    distillJSON = new JSONObject();
                    aboutMapArr = new JSONArray();
                } else {
                    aboutMapArr = distillJSON.getJSONArray("About");
                }

                JSONObject aboutObj = new JSONObject();
                String indicative_snippet;
                if (t.aspectCategory.equals(OVERALL_STR)) {
                    indicative_snippet = t.opinion;
                } else {
                    indicative_snippet = snippet;
                }
                if (this.isHistoricalDemo && !isHistoricalDemoValidSnippet(indicative_snippet, t)) {
                    continue;
                }
                String entityStr = t.entity.replaceAll("[^\\w\\s\\-]", "");
                if (t.statementType.equals("Advocacy") || t.statementType.equals("Opinion")) {
                    if (t.xpf.isCompAdjFeature()) {
                        if (t.opinionSentiment.equals("Negative")) {
                            t.opinionSentiment = "Positive";
                        } else {
                            if (t.opinionSentiment.equals("Positive")) {
                                t.opinionSentiment = "Negative";
                            }
                        }
                    }
                }
                aboutObj.put("Entity", entityStr);
                aboutObj.put("Aspect", t.aspectCategory);
                aboutObj.put("Sentiment", t.opinionSentiment);
                aboutObj.put("StatementType", t.statementType);
                aboutObj.put("Indicative Snippet", indicative_snippet);
                aboutMapArr.put(aboutObj);
                distillJSON.put("About", aboutMapArr);
                netMap.put(snippet, distillJSON);
            }

            JSONObject netJSON = new JSONObject(netMap);
            if (netJSON.length() > 0) {
                if (domainName != null) {
                    netJSON.put("Domain", this.domainName);
                } else {
                    netJSON.put("Domain", "Generic");
                }
            }
            return netJSON;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<XpDistill> processPipeline(boolean isSplittedSentence) {
        List<CoreMap> sentencesList = getSentences();
        XpFeatureVector xpf = null;
        XpSentenceResources xto = null;
        Map<String, String> wordPOSmap = null;
        if (!isSplittedSentence) {
            xpf = new XpFeatureVector();
            xpf.setStanfordSentimentFeature(this.getStanfordSentiment());
            xpf.setCountStopWordsFeature(ProcessString.countStopWords(language, this.normalized_text));
            wordPOSmap = new HashMap<String, String>();
        }

        for (CoreMap sentence : sentencesList) {
            String stanfordSentiment = (!this.isMicro) ? this.getStanfordSentiment(sentence) : null;
            String snippet = sentence.toString();
            if (isSplittedSentence) {
                wordPOSmap = new HashMap<String, String>();
                xpf = new XpFeatureVector();
            }
            if (!xpf.isActionInFuture() && VocabularyTests.goingToFutureTense(language, snippet))
                xpf.setIsActionInFuture(true);
            if (stanfordSentiment != null) {
                xpf.setStanfordSentimentFeature(stanfordSentiment);
                logger.info("Stanford Sentiment:\t" + stanfordSentiment);
            }
            Map<String, String> tagSentimentMap = new HashMap<String, String>();
            double tic = System.currentTimeMillis();
            List<String> tokenList = this.getTokens(domainName, sentence, wordPOSmap, tagSentimentMap, xpf);
            logger.info("getTokens duration: " + (System.currentTimeMillis() - tic) + " ms");
            if (tokenList == null) {
                continue;
            }
            logger.info("TokenList:\t" + tokenList.toString());
            this.emoticonFeaturize(snippet, xpf);
            this.linguisticFeaturize(tokenList, wordPOSmap, snippet, xpf);
            Tree parseTree = this.getParseTree(sentence);

            SemanticGraph semanticDepGraph = null;
            StanfordDependencyGraph sdg = null;

            Map<String, String[]> tagAspectMap = new HashMap<String, String[]>();
            xto = new XpSentenceResources(sentence, wordPOSmap, tagSentimentMap, tagAspectMap, xpf);
            //			xpf.setXpTextObject(xto);

            if ((language == Languages.EN) || ((language == Languages.SP) && XpConfig.STANFORD_SP_DEPPARSE)) {
                if (tokenList.size() < 80) {
                    semanticDepGraph = this.getDependencyGraph(sentence);
                    logger.info("Dependency Graph:\n" + semanticDepGraph.toString(SemanticGraph.OutputFormat.LIST));
                }
                sdg = new StanfordDependencyGraph(snippet, semanticDepGraph, xto, this);
            } else if (language == Languages.SP && !XpConfig.STANFORD_SP_DEPPARSE) {
                ConstituencyTree tree = null;
                List<SemanticGraphEdge> edges = null;
                if (tokenList.size() < 100) {
                    tree = this.getConstituencyTree();
                    edges = tree.extractEdges();
                }
                sdg = new StanfordDependencyEdges(snippet, tree, edges, xto, this);
                logger.info("Dependency Edges:\n" + sdg.getEdgesAsStr());
            }

            String snippetSentiment = null;
            String statementType = null;

            Map<String, Set<String>> entityOpinionMap = null;
            if (sdg != null) {
                entityOpinionMap = sdg.getEntityOpinionMap();
            }

            List<XpDistill> overallTriples = new ArrayList<XpDistill>();
            boolean needSentimentFlag = false;
            snippetSentiment = this.getNeedSentiment(tagSentimentMap);
            if (snippetSentiment != null) {
                if (!xpf.isGreetingsSalutationPresentFeature()) {
                    needSentimentFlag = true;
                }
            } else {
                snippetSentiment = getPossessionSentiment(tagSentimentMap);
            }

            logger.info(parseTree.toString());
			/*Start - Processing for Overall Triples*/
            if (snippetSentiment != null) {
                if (XpEngine.getMachineLearningMode()) {
                    statementType = XpEngine.getXpStmtClassifier(language).getPredictedClass(xpf);
                    logger.info("StatementType (Need Case): " + statementType);
                    if ((!statementType.equals(StatementTypeLexicon.SUGGESTION_STATEMENT) && snippetSentiment.equals(XpSentiment.NEGATIVE_SENTIMENT) || snippetSentiment.equals(XpSentiment.NEUTRAL_SENTIMENT))) {
                        statementType = StatementTypeLexicon.SUGGESTION_STATEMENT;
                    }
                }

                XpDistill overall_rawT = new XpDistill(reviewID, this.original_text, snippet, subject, OVERALL_STR, OVERALL_STR, snippet, snippetSentiment, snippetSentiment, statementType, xto);
                overallTriples.add(overall_rawT);

            } else {
                snippetSentiment = this.computeForOverallTriples(sentence, parseTree, semanticDepGraph, tokenList, overallTriples, snippet, xto, xpf, statementType);
            }
			/*End - Processing for Overall Triples*/
			/*This is being repetitive. Need to refactor this later*/
            if (XpEngine.getMachineLearningMode() && statementType == null) {
                //				System.out.println("processPipeline: l. 879");
                statementType = XpEngine.getXpStmtClassifier(language).getPredictedClass(xpf);
                //				System.out.println(featureSet.toStringTrue());
                logger.info("StatementType (Common Case): " + statementType);
            }
			/*Start - Processing for Aspect Based Triples - Dependency Parser Based*/
            if (entityOpinionMap != null) {
                String singleAspectPolarSentiment = null;
                if (entityOpinionMap.size() > 0) {
                    singleAspectPolarSentiment = this.loopOnOpinions(snippet, needSentimentFlag, entityOpinionMap, snippetSentiment, xto, xpf, statementType);
                }
				/*End - Processing for Aspect Based Triples - Dependency Parser Based*/
				/*
					 * This is when it would look for basic entity spotting. Eg:
					 * Food good. This returns a non-null dependency graph of size 0
				 */
                else {
					/*Great, Very good*/
                    if (entityOpinionMap.size() == 0) {
                        this.isDomainRelevantReview = true;
                    }
                    this.analyseBasicSentiment(snippet, snippetSentiment, statementType, overallTriples, tokenList, xto);
                }

                if (singleAspectPolarSentiment != null && overallTriples.size() == 1) {
					/* If the sentence has switched overall sentiment let the opinions have their own sentiments associated*/
                    this.addOverallTriples(overallTriples, singleAspectPolarSentiment, snippetSentiment, tokenList, sdg);
                } else {
                    this.addOverallTriples(overallTriples, null, snippetSentiment, tokenList, sdg);
                }
            }

            if (XpEngine.getInteractiveMode()) {
                logger.info("XP Feature Vector:\t" + xpf.toStringTrue());
            }
			/* call sdg.getFeatureSet() to get the featureSet vector */
            if (isSplittedSentence) {
                snippetFeatureMap.put(snippet, xpf);
            }
        }

        if (!isSplittedSentence) {
            double score = SWNsentiment.getSWNscore(wordPOSmap);
            xpf.setSentiWNSentimentFeature(score);
            snippetFeatureMap.put(this.normalized_text, xpf);
        }
        if (this.reviewTriples.size() == 0) {
            XpDistill basicT = new XpDistill(reviewID, this.original_text, this.normalized_text, subject, OVERALL_STR, OVERALL_STR, this.original_text, XpSentiment.NEUTRAL_SENTIMENT, XpSentiment.NEUTRAL_SENTIMENT, StatementTypeLexicon.OPINION_STATEMENT, xto);
            this.addOverallTriple(basicT, null, null);
            this.addToReviewTriples(basicT);
        }
        return this.reviewTriples;
    }

}