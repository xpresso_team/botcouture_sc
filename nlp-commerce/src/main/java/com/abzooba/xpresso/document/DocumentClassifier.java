/**
 *
 */
package com.abzooba.xpresso.document;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.aspect.domainontology.OntologyUtils;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Annotations;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.engine.core.XpExpression;
import com.abzooba.xpresso.textanalytics.ProcessString;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;
import com.abzooba.xpresso.utils.FileIO;
import com.abzooba.xpresso.utils.ThreadUtils;
import org.slf4j.Logger;


/**
 * @author Koustuv Saha
 * Oct 8, 2015 10:18:33 AM
 * XpressoV3.0  DocumentClassifier
 */
public class DocumentClassifier {

    //	private static final String semantriaCategoriesMappingFileName = "data/document/Categories-Mapping.txt";
    //	private static final String semantriaCategoriesHierarchayMappingFileName = "data/document/categories-Mapping-2nd.txt";
    //	private static final String semantriaCategoriesClustersFileName = "data/document/Semantria-Categories-Clusters.txt";

    private static final String documentClustersFileName = "data/document/Document Categories Clusters.txt";
    //	private static final String documentClustersFileName = "input/synaptika/Synaptika NL Search queries - Sheet4.tsv";
    private static final boolean useScalarMultiply = false;
    //	private static final String possibleCategoriesFileName = "data/document/possibleCategories.txt";

    //	private static List<String> semantriaCategoriesList;
    //	private static Map<String, String> categoriesMap = new HashMap<String, String>();

    private static Map<String, String> categoriesHierarchyMap = new HashMap<String, String>();
    private static Map<String, Integer> categoriesCountMap = new HashMap<String, Integer>();
    private static Map<String, List<String>> categoriesTokenMap = new HashMap<String, List<String>>();
    private static Map<String, Object> categoriesCentroidMap = new HashMap<String, Object>();

    //	private static Set<String> possibleCategoriesSet = new HashSet<String>();

    protected static final Logger logger = XCLogger.getSpLogger();

    class MongoUpdateThread implements Runnable {
        String line;

        public MongoUpdateThread(String line) {
            this.line = line;
        }

        /* (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            String[] strArr = line.split("\t");
            //			System.out.println("length of line: " + strArr.length);
            if (strArr.length > 2) {
                String categoryName = strArr[1];

                if (XpEngine.getWordVectorDbApp().getColumnObjectForKey(Languages.EN, categoryName, "centroid-vector") != null) {
                    //					System.out.println("Skipped " + categoryName);
                    return;
                }

                List<Double> clusterVector = null;

                int cardinality = strArr.length - 1;

                for (int i = 1; i < strArr.length; i++) {
                    String[] clusterWordArr = strArr[i].split(":");
                    if (clusterWordArr.length > 1) {
                        String clusterWord = clusterWordArr[0];
                        if (clusterVector == null) {
                            clusterVector = new ArrayList<Double>();
                        }

                        WordVector.addVectors(clusterVector, XpEngine.getWordVectorDbApp().getWordVectorForKey(Languages.EN, clusterWord));
                    }
                }
                WordVector.averageVector(clusterVector, cardinality);
                //				clusterCentroidMap.put(categoryName, clusterVector);
                XpEngine.getWordVectorDbApp().updateData(Languages.EN, categoryName, "centroid-vector", clusterVector);
                System.out.println("Loading " + categoryName);
            }

        }
    }

    private static void loadCategoriesToMongo(List<String> clusterFileLines) {

        //		List<String> clusterFileLines = new ArrayList<String>();
        //		FileIO.read_file(semantriaCategoriesClustersFileName, clusterFileLines);
        //		FileIO.read_file(documentClustersFileName, clusterFileLines);

        System.out.println("Entered LoadCategoriesToMongo");
        ExecutorService executor = Executors.newFixedThreadPool(20);

        DocumentClassifier dc = new DocumentClassifier();
        for (String currLine : clusterFileLines) {
            Runnable worker = dc.new MongoUpdateThread(currLine);
            executor.execute(worker);

        }
        ThreadUtils.shutdownAndAwaitTermination(executor);

    }

    public static void init(boolean isLoadToMongo) throws IOException {
        List<String> clusterFileLines = new ArrayList<String>();
        //		FileIO.read_file(semantriaCategoriesClustersFileName, clusterFileLines);
        FileIO.read_file(documentClustersFileName, clusterFileLines);
        for (String currLine : clusterFileLines) {
            String[] strArr = currLine.split("\t");
            if (strArr.length > 2) {
                String categoryName = strArr[1];
                //				System.out.println("Loading to CategorisHierarchy Map: " + categoryName + "\t" + strArr[0]);
                categoriesHierarchyMap.put(categoryName, strArr[0]);
            }

        }
        //		System.out.println("--------------------------------categoriesHierarchyMap size " + categoriesHierarchyMap.size());
        if (isLoadToMongo) {
            loadCategoriesToMongo(clusterFileLines);
        }

        //		List<String> categoriesMappingFileLines = new ArrayList<String>();
        //		FileIO.read_file(semantriaCategoriesMappingFileName, categoriesMappingFileLines);
        //
        //		for (String currLine : categoriesMappingFileLines) {
        //			String[] strArr = currLine.split("\t");
        //
        //			if (strArr.length == 2) {
        //				String categoryName = strArr[0];
        //				String parentCategory = strArr[1];
        //				categoriesMap.put(categoryName, parentCategory);
        //			}
        //		}

        //		FileIO.read_file(possibleCategoriesFileName, possibleCategoriesSet);

		/*For manPower deliverable-job categorization*/
        //		categoriesMappingFileLines.clear();
        //		FileIO.read_file(semantriaCategoriesHierarchayMappingFileName, categoriesMappingFileLines);
        //
        //		for (String currLine : categoriesMappingFileLines) {
        //			String[] strArr = currLine.split("\t");
        //
        //			if (strArr.length == 2) {
        //				String categoryName = strArr[0];
        //				String parentCategory = strArr[1];
        //				categoriesHierarchyMap.put(categoryName, parentCategory);
        //			}
        //		}
        //		BufferedReader br = new BufferedReader(new FileReader(possibleCategoriesFileName));
        //		String strLine = null;
        //		while ((strLine = br.readLine()) != null) {
        //			System.out.println(strLine);
        //			possibleCategoriesSet.add(strLine);
        //		}
        //		br.close();
        //		System.out.println("Categories Hierarchy Map - " + categoriesHierarchyMap);
    }

    private static Object getCategoryCentroid(String category) {
        Object centroidVector = categoriesCentroidMap.get(category);
        if (centroidVector == null) {
            centroidVector = XpEngine.getWordVectorDbApp().getColumnObjectForKey(Languages.EN, category, "centroid-vector");
            categoriesCentroidMap.put(category, centroidVector);
        }
        return centroidVector;
    }

    protected static String[] classifyDocument(Collection<String> tokensList) {
        List<Double> documentVector = null;
        List<String> token_cloud = new ArrayList<String>(); //debug 9 nov'15
        int relevantTokenCount = 0;

        for (String token : tokensList) {
            //			if (ProcessString.isStopWord(Languages.EN, token))
            if (OntologyUtils.isRejectEntity(Languages.EN, token)) {
                continue;
            }
            token = token.replaceAll("[^\\w\\s]", "");
            //			List<Double> tokenVector = XpEngine.getWordVectorDbApp().getWordVectorForKey(Languages.EN, token);
            List<Double> tokenVector = WordVector.getWordVector(Languages.EN, token);
            if (documentVector == null) {
                documentVector = new ArrayList<Double>();
            }
            //			System.out.println("Token vector for " + token + " : " + tokenVector);
            if (tokenVector != null) {

				/*---------------category count by token-------------------------*/
                double maxSim = 0;
                String maxSimCategory = null;
                for (Map.Entry<String, String> entry : categoriesHierarchyMap.entrySet()) {
                    String currCategory = entry.getKey(); //Sub Category from category-mapping
                    List<?> categoryCentroid = (List<?>) getCategoryCentroid(currCategory);

                    double currSimilarity = WordVector.cosSimilarity(tokenVector, categoryCentroid);
                    if (currSimilarity > 0 && currSimilarity > maxSim) {

                        maxSim = currSimilarity;
                        maxSimCategory = currCategory;
                    }
                }
                if (categoriesCountMap.containsKey(maxSimCategory))
                    categoriesCountMap.replace(maxSimCategory, categoriesCountMap.get(maxSimCategory) + 1);
                else
                    categoriesCountMap.put(maxSimCategory, 1);

                if (categoriesTokenMap.containsKey(maxSimCategory))
                    categoriesTokenMap.get(maxSimCategory).add(token);
                else {
                    List<String> tempToken = new ArrayList<String>();
                    tempToken.add(token);
                    categoriesTokenMap.put(maxSimCategory, tempToken);
                    //System.out.println("Token Map : " + token + " " + categoriesTokenMap.get(maxSimCategory));
                }

				/*------------------------------------------------------------*/

                WordVector.addVectors(documentVector, tokenVector);
                token_cloud.add(token); //debug 9 nov'15
                relevantTokenCount++;
            }
        }

        //		System.out.println("Token Count : " + relevantTokenCount);

        //		for (Map.Entry<String, Integer> entry : categoriesCountMap.entrySet()) {
        //			System.out.println("category : " + entry.getKey() + " and count : " + entry.getValue() + " Token List : " + categoriesTokenMap.get(entry.getKey()));
        //		}

        WordVector.averageVector(documentVector, relevantTokenCount);

        double maxSimilarity = 0;
        //		List<String> maxSimilarList = new ArrayList<String>();
        String maxSimilarCategory = null;
        //		for (String currCategory : semantriaCategoriesList) {
        for (Map.Entry<String, String> entry : categoriesHierarchyMap.entrySet()) {
            String currCategory = entry.getKey(); //Sub Category from category-mapping

            //			String parentCategory = categoriesHierarchyMap.get(categoriesMap.get(currCategory)); //possible(main) category from category-mapping-2nd.txt using auto-category from category-mapping
            //			if (parentCategory == null || parentCategory.equals("Others")) {
            //				continue;
            //			}

            //			System.out.println(possibleCategoriesSet);
            //			if (!possibleCategoriesSet.contains(entry.getValue())) {
            //				//				System.out.println("Skipped " + entry.getValue());
            //				continue;
            //			}
            //			List<?> categoryCentroid = (List<?>) XpEngine.getWordVectorDbApp().getColumnObjectForKey(Languages.EN, currCategory, "centroid-vector");

            List<?> categoryCentroid = (List<?>) getCategoryCentroid(currCategory);

            double currSimilarity = WordVector.cosSimilarity(documentVector, categoryCentroid);
            //			System.out.println("Curr Similarity " + currCategory + "\t" + currSimilarity);
            if (currSimilarity > maxSimilarity) {
                maxSimilarity = currSimilarity;
                maxSimilarCategory = currCategory;
            }
        }

        //		maxSimilarList.add(maxSimilarCategory);
        //		if (maxSimilarity > 0) {
        //			for (Map.Entry<String, String> entry : categoriesMap.entrySet()) {
        //				String currCategory = entry.getKey();
        //
        //				String parentCategory = categoriesHierarchyMap.get(categoriesMap.get(currCategory));
        //				if (parentCategory == null || parentCategory.equals("Others")) {
        //					continue;
        //				}
        //				List<?> categoryCentroid = (List<?>) getCategoryCentroid(currCategory);
        //				double currSimilarity = WordVector.cosSimilarity(documentVector, categoryCentroid);
        //				if (Math.abs(currSimilarity - maxSimilarity) <= 0.1) {
        //					maxSimilarList.add(currCategory);
        //				}
        //			}
        //		}

        String[] maxSimilarCategoryArr = new String[2];
        //		System.out.println("\nTOKEN LIST : " + token_cloud + "\n"); //debug 9 nov'15
        if (maxSimilarCategory != null) {
            //			System.out.println("max similarity score - " + maxSimilarity + " max similarity category : " + maxSimilarCategory);
            maxSimilarCategoryArr[0] = maxSimilarCategory;
            maxSimilarCategoryArr[1] = categoriesHierarchyMap.get(maxSimilarCategory);
        }
        return maxSimilarCategoryArr;

    }

    protected static String[] multiClassifyDocument(Map<String, Integer> tokenMap, boolean scalarMultiply) {
        List<Double> documentVector = null;
        List<String> token_cloud = new ArrayList<String>(); //debug 9 nov'15
        int relevantTokenCount = 0;
        DocumentInfo doc = new DocumentInfo();
        for (Entry<String, Integer> entry : tokenMap.entrySet()) {
            String token = entry.getKey();
            if (OntologyUtils.isRejectEntity(Languages.EN, token)) {
                continue;
            }
            token = token.replaceAll("[^\\w\\s]", "");
            List<Double> tokenVector = WordVector.getWordVector(Languages.EN, token);
            if (documentVector == null) {
                documentVector = new ArrayList<Double>();
            }
            //			System.out.println("Token vector for " + token + " : " + tokenVector);
            if (tokenVector != null) {
                doc.addEntity(token, 1);
                if (scalarMultiply)
                    WordVector.scalarMultiplyVectors(tokenVector, entry.getValue().intValue());
                WordVector.addVectors(documentVector, tokenVector);
                token_cloud.add(token); //debug 9 nov'15
                relevantTokenCount += entry.getValue().intValue();
            }
        }

        WordVector.averageVector(documentVector, relevantTokenCount);

        for (Map.Entry<String, String> entry : categoriesHierarchyMap.entrySet()) {
            String currCategory = entry.getKey(); //Sub Category from category-mapping
            List<?> categoryCentroid = (List<?>) getCategoryCentroid(currCategory);

            double currSimilarity = WordVector.cosSimilarity(documentVector, categoryCentroid);
            doc.addCategoryToSortedMap(currCategory, categoriesHierarchyMap.get(currCategory), currSimilarity);
        }
        doc.setDocumentVector(documentVector);
        //		doc.showCategoryPreference();
        String[] maxSimilarCategoryArr = new String[2];
        maxSimilarCategoryArr[0] = doc.categoryListToString(true);
        maxSimilarCategoryArr[1] = doc.categoryListToString(false);
        return maxSimilarCategoryArr;

    }

    protected static String[] sentenceWiseClassifyDocument(Set<Map<String, Integer>> sentenceTokenSet, boolean scalarMultiply, boolean useCategoryScore) {
        List<Double> sentenceVector = null;
        Map<String, Double> classificationMap = new HashMap<String, Double>();
        List<String> token_cloud = new ArrayList<String>(); //debug 9 nov'15
        int relevantTokenCount = 0;
        for (Map<String, Integer> entry : sentenceTokenSet) {
            sentenceVector = new ArrayList<Double>();
            for (Entry<String, Integer> pair : entry.entrySet()) {
                String token = pair.getKey().replaceAll("[^\\w\\s]", "");
                if (OntologyUtils.isRejectEntity(Languages.EN, token)) {
                    continue;
                }
                List<Double> tokenVector = WordVector.getWordVector(Languages.EN, token);
                //			System.out.println("Token vector for " + token + " : " + tokenVector);
                if (tokenVector != null) {
                    if (scalarMultiply)
                        WordVector.scalarMultiplyVectors(tokenVector, pair.getValue().intValue());
                    WordVector.addVectors(sentenceVector, tokenVector);
                    token_cloud.add(token); //debug 9 nov'15
                    if (scalarMultiply)
                        relevantTokenCount += pair.getValue().intValue();
                    else
                        relevantTokenCount += 1;
                }
            }

            WordVector.averageVector(sentenceVector, relevantTokenCount);
            String maxSimilarCategory = "";
            double maxSimilarity = 0.0;
            for (Map.Entry<String, String> categoryEntry : categoriesHierarchyMap.entrySet()) {
                String currCategory = categoryEntry.getKey(); //Sub Category from category-mapping
                List<?> categoryCentroid = (List<?>) getCategoryCentroid(currCategory);

                double currSimilarity = WordVector.cosSimilarity(sentenceVector, categoryCentroid);
                if (currSimilarity > 0 && currSimilarity > maxSimilarity) {
                    maxSimilarity = currSimilarity;
                    maxSimilarCategory = currCategory;
                }
            }
            Double categoryCount = classificationMap.get(maxSimilarCategory);
            if (maxSimilarity > 0) {
                if (categoryCount == null) {
                    if (useCategoryScore)
                        classificationMap.put(maxSimilarCategory, maxSimilarity);
                    else
                        classificationMap.put(maxSimilarCategory, 1.0);
                } else {
                    if (useCategoryScore)
                        classificationMap.put(maxSimilarCategory, categoryCount.doubleValue() + maxSimilarity);
                    else
                        classificationMap.put(maxSimilarCategory, categoryCount.doubleValue() + 1.0);
                }
            }
        }
        List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(classificationMap.entrySet());
        Collections.sort(list, new Comparator<Entry<String, Double>>() {
            @Override
            public int compare(Entry<String, Double> o1, Entry<String, Double> o2) {
                double value1 = o1.getValue().doubleValue();
                double value2 = o2.getValue().doubleValue();
                int res = 0;
                if (value1 > value2)
                    res = -1;
                else if (value1 < value2)
                    res = 1;
                return res;
            }
        });
        //		System.out.println("Entry - list : " + list);
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        double prevSimilarity = -1;
        for (int i = 0; i < list.size(); i++) {
            double currSimilarity = list.get(i).getValue().doubleValue();
            if (i > 0 && i < list.size()) {
                if (prevSimilarity == currSimilarity) {
                    String currentCatg = list.get(i).getKey();
                    sb1.append(currentCatg);
                    sb1.append(" ");
                    sb2.append(categoriesHierarchyMap.get(currentCatg));
                    sb2.append(" ");
                    prevSimilarity = currSimilarity;
                } else
                    break;
            } else {
                String currentCatg = list.get(i).getKey();
                sb1.append(currentCatg);
                sb1.append(" ");
                sb2.append(categoriesHierarchyMap.get(currentCatg));
                sb2.append(" ");
                prevSimilarity = currSimilarity;
            }

        }
        String[] maxSimilarCategoryArr = new String[2];
        maxSimilarCategoryArr[0] = sb1.toString();
        maxSimilarCategoryArr[1] = sb2.toString();
        return maxSimilarCategoryArr;

    }

    protected static String[] sentenceWiseCumulativeEntityClassifyDocument(Set<Map<String, Integer>> sentenceTokenSet, boolean scalarMultiply, boolean useCategoryScore, boolean multiClassify) {
        List<Double> sentenceVector = null;
        Map<String, Double> classificationMap = new HashMap<String, Double>();
        List<String> token_cloud = new ArrayList<String>(); //debug 9 nov'15
        int relevantTokenCount = 0;
        for (Map<String, Integer> entry : sentenceTokenSet) {
            sentenceVector = new ArrayList<Double>();
            for (Entry<String, Integer> pair : entry.entrySet()) {
                String token = pair.getKey().replaceAll("[^\\w\\s]", "");
                if (OntologyUtils.isRejectEntity(Languages.EN, token)) {
                    continue;
                }
                List<Double> tokenVector = WordVector.getWordVector(Languages.EN, token);
                //			System.out.println("Token vector for " + token + " : " + tokenVector);
                if (tokenVector != null) {
                    if (scalarMultiply)
                        WordVector.scalarMultiplyVectors(tokenVector, pair.getValue().intValue());
                    WordVector.addVectors(sentenceVector, tokenVector);
                    token_cloud.add(token); //debug 9 nov'15
                    if (scalarMultiply)
                        relevantTokenCount += pair.getValue().intValue();
                    else
                        relevantTokenCount += 1;
                }
            }

            WordVector.averageVector(sentenceVector, relevantTokenCount);
            String maxSimilarCategory = "";
            double maxSimilarity = 0.0;
            for (Map.Entry<String, String> categoryEntry : categoriesHierarchyMap.entrySet()) {
                String currCategory = categoryEntry.getKey(); //Sub Category from category-mapping
                List<?> categoryCentroid = (List<?>) getCategoryCentroid(currCategory);

                double currSimilarity = WordVector.cosSimilarity(sentenceVector, categoryCentroid);
                if (currSimilarity > 0 && currSimilarity > maxSimilarity) {
                    maxSimilarity = currSimilarity;
                    maxSimilarCategory = categoriesHierarchyMap.get(currCategory);
                }
            }
            Double categoryCount = classificationMap.get(maxSimilarCategory);
            if (maxSimilarity > 0) {
                if (categoryCount == null) {
                    if (useCategoryScore)
                        classificationMap.put(maxSimilarCategory, maxSimilarity);
                    else
                        classificationMap.put(maxSimilarCategory, 1.0);
                } else {
                    if (useCategoryScore)
                        classificationMap.put(maxSimilarCategory, categoryCount.doubleValue() + maxSimilarity);
                    else
                        classificationMap.put(maxSimilarCategory, categoryCount.doubleValue() + 1.0);
                }
            }
        }
        List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(classificationMap.entrySet());
        Collections.sort(list, new Comparator<Entry<String, Double>>() {
            @Override
            public int compare(Entry<String, Double> o1, Entry<String, Double> o2) {
                double value1 = o1.getValue().doubleValue();
                double value2 = o2.getValue().doubleValue();
                int res = 0;
                if (value1 > value2)
                    res = -1;
                else if (value1 < value2)
                    res = 1;
                return res;
            }
        });
        System.out.println("Sorted Entry - list : " + list);
        StringBuilder sb1 = new StringBuilder();
        if (!multiClassify) {
            double prevSimilarity = -1;
            for (int i = 0; i < list.size(); i++) {
                double currSimilarity = list.get(i).getValue().doubleValue();
                if (i > 0 && i < list.size()) {
                    if (prevSimilarity == currSimilarity) {
                        String currentCatg = list.get(i).getKey();
                        sb1.append(currentCatg);
                        sb1.append(" ");
                        prevSimilarity = currSimilarity;
                    } else
                        break;
                } else {
                    String currentCatg = list.get(i).getKey();
                    sb1.append(currentCatg);
                    sb1.append(" ");
                    prevSimilarity = currSimilarity;
                }

            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                if (i > 2)
                    break;
                String currentCatg = list.get(i).getKey();
                sb1.append(currentCatg);
                sb1.append(" ");
                sb1.append(list.get(i).getValue().doubleValue());
                sb1.append(" ");
            }

        }

        String[] maxSimilarCategoryArr = new String[2];
        maxSimilarCategoryArr[0] = sb1.toString();
        maxSimilarCategoryArr[1] = sb1.toString();
        return maxSimilarCategoryArr;

    }

    public static String[] classifyDocument(String documentText) {

        if (documentText.trim().startsWith("Victor Santiago"))
            return null;

        switch (XpConfig.DOCUMENT_CLASSIFY_APPROACH) {
            case 0:
                XpExpression xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                //			Set<String> entityList = xpE.processPipelineForEntities(true);
                Map<String, Integer> entityMap = xpE.getEntityMap();
                Set<String> entityList = entityMap.keySet();
                return classifyDocument(entityList);
            case 1:
                List<String> tokensList = ProcessString.tokenizeString(Languages.EN, documentText);
                return classifyDocument(tokensList);
            case 2:
                xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                entityMap = xpE.getEntityMap();
                return multiClassifyDocument(entityMap, useScalarMultiply);
            case 3:
                xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                entityMap = xpE.processPipelineForPOSbasedEntities(true);
                return multiClassifyDocument(entityMap, useScalarMultiply);
            case 4:
                xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                entityMap = xpE.processPipelineForPOSbasedEntities(true);
                return classifyDocument(entityMap.keySet());
            case 5:
                xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                Set<Map<String, Integer>> sentenceEntitySet = xpE.sentencePOSEntityExtraction(true);
                return sentenceWiseClassifyDocument(sentenceEntitySet, false, false);
            case 6:
                xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                sentenceEntitySet = xpE.sentencePOSEntityExtraction(true);
                return sentenceWiseClassifyDocument(sentenceEntitySet, false, true);
            case 7:
                xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                sentenceEntitySet = xpE.sentencePOSEntityExtraction(true);
                return sentenceWiseCumulativeEntityClassifyDocument(sentenceEntitySet, false, false, false);
            case 8:
                xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                sentenceEntitySet = xpE.sentencePOSEntityExtraction(true);
                return sentenceWiseCumulativeEntityClassifyDocument(sentenceEntitySet, false, true, false);
            case 9:
                xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                sentenceEntitySet = xpE.sentencePOSEntityExtraction(true);
                return sentenceWiseCumulativeEntityClassifyDocument(sentenceEntitySet, false, false, true);
            case 10:
                xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                sentenceEntitySet = xpE.sentencePOSEntityExtraction(true);
                return sentenceWiseCumulativeEntityClassifyDocument(sentenceEntitySet, false, true, true);
            case 11:
                String topic = extractTopic(documentText);
                if (topic != null)
                    xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", topic, true);
                else
                    xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                sentenceEntitySet = xpE.sentencePOSEntityExtraction(true);
                return sentenceWiseCumulativeEntityClassifyDocument(sentenceEntitySet, false, true, true);
            default:
                xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
                //			Set<String> entityList = xpE.processPipelineForEntities(true);
                entityMap = xpE.getEntityMap();
                entityList = entityMap.keySet();
                return classifyDocument(entityList);
        }
    }

    private static String extractTopic(String documentText) {
        String topic = null;
        String topicDelimiter = "|";
        int index = documentText.indexOf(topicDelimiter);
        if (index > 0 && index <= 100) {
            topic = documentText.substring(0, index);
        }
        return topic;
    }

    public static void main(String[] args) throws IOException {
        XpEngine.init(true);
        System.out.println(Arrays.toString(classifyDocument("interview shirt")));
        System.exit(1);

        List<String> inputFileLines = new ArrayList<String>();
        //		FileIO.read_file("input/document/examples2014.txt", inputFileLines);
        //		FileIO.read_file("input/document/datasets/20ng-test-all-terms.txt", inputFileLines);
        //		FileIO.read_file("input/synaptika/theiconic_mens_clothing.tsv", inputFileLines);
        FileIO.read_file("input/synaptika/Synaptika NL Search queries - Sheet1.tsv", inputFileLines);

        PrintWriter pw = new PrintWriter(new FileWriter("output/document-classification-" + XpEngine.getCurrentDateStr() + ".txt"));

        int i = 0;
        for (String inputLine : inputFileLines) {
            String[] strArr = inputLine.split("\t");
            //			if (strArr.length >= 3) {
            if (strArr.length >= 2) {
                //				String id = strArr[0];
                String id = strArr[0];
                //				String document = strArr[1];
                String document = strArr[1];
                String[] category = classifyDocument(document);
                pw.println(id + "\t" + document + "\t" + Arrays.toString(category));
                System.out.println(i);
                //				System.out.println(id + "\t" + document + "\t" + category);
            }
            if (i++ % 100 == 0) {
                pw.flush();
            }
        }
        pw.close();

    }

}