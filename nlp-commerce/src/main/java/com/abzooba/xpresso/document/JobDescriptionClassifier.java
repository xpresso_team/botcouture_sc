/**
 *
 */
package com.abzooba.xpresso.document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.abzooba.xpresso.engine.config.XpConfig.Annotations;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpExpression;
import com.abzooba.xpresso.textanalytics.ProcessString;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;
import com.abzooba.xpresso.utils.FileIO;

/**
 * @author Koustuv Saha
 * Nov 24, 2015 10:45:30 AM
 * XpressoV4.0  JobDescriptionClassifier
 */
public class JobDescriptionClassifier {

    private static final String resumeSeedFileName = "data/document/resume-seed.txt";
    private static final String entitiesMappingFileName = "data/document/entities-mapping.txt";

    private static Map<String, List<String>> categorySeedMap = new ConcurrentHashMap<String, List<String>>();
    private static Map<String, String> entitiesCategoryMap = new ConcurrentHashMap<String, String>();

    public static void init() {
        List<String> tempLines = new ArrayList<String>();
        FileIO.read_file(resumeSeedFileName, tempLines);

        for (String currLine : tempLines) {
            String[] strArr = currLine.split("\t");

            if (strArr.length >= 1) {
                List<String> seedList = new ArrayList<String>();
                String categoryName = strArr[0];
                for (int i = 1; i < strArr.length; i++) {
                    String seedWord = strArr[i];
                    seedList.add(seedWord);
                    categorySeedMap.put(categoryName, seedList);
                }
            }
        }
        tempLines.clear();
        FileIO.read_file(entitiesMappingFileName, tempLines);

        for (String currLine : tempLines) {
            String[] strArr = currLine.split("\t");

            if (strArr.length >= 3) {
                String entity = strArr[0];
                String category = strArr[2];
                if (category.trim().length() > 0) {
                    entitiesCategoryMap.put(entity, category);
                }
            }
        }
    }

    private static String[] classifyDocument(Set<String> tokensList, Map<String, String> allNerMap) {

        double maxSimilarity = 0;
        String maxSimilarCategoryStr = null;
        String maxSimilarSeed = null;
        //		for (String currCategory : semantriaCategoriesList) {

        Set<String> maxSimilarCategory = new HashSet<String>();
        System.out.println("Tokens List : " + tokensList);
        List<Double> documentVector = null;
        int relevantTokenCount = 0;
        for (String token : tokensList) {
            if (ProcessString.isStopWord(Languages.EN, token)) {
                continue;
            }
            token = token.toLowerCase();
            if (token.contains("job") || token.matches("(engineer[s]*)|(check)|(find)|(time[s]*)|(stor[yies]*)|(change[s]*)|(opportunit[iesy]*)") || token.contains("survey") || token.contains("trend")) {
                continue;
            } else if (token.contains("careerbrain") || token.matches("(question[s]*)"))
                continue;
            String nerCategory = allNerMap.get(token);
            if (nerCategory != null && nerCategory.equals("LOCATION") || token.matches("(cit[yies]*)|(location[s]*)|(state[s]*)")) {
                continue;
            }

            String directMatchCategory = entitiesCategoryMap.get(token);
            List<Double> tokenVector = WordVector.getWordVector(Languages.EN, token);
            if (documentVector == null) {
                documentVector = new ArrayList<Double>();
            }
            //			System.out.println("Token vector for " + token + " : " + tokenVector);
            if (tokenVector != null) {
                WordVector.addVectors(documentVector, tokenVector);
                relevantTokenCount++;
            }

            if (directMatchCategory != null) {
                //				maxSimilarCategory = directMatchCategory;
                maxSimilarCategory.add(directMatchCategory);
                maxSimilarSeed = directMatchCategory;
                maxSimilarity = 1;
                System.out.println("Direct Match of " + token + "\t" + directMatchCategory);
            }

        }
        WordVector.averageVector(documentVector, relevantTokenCount);

        if (maxSimilarity != 1) {
            for (Map.Entry<String, List<String>> entry : categorySeedMap.entrySet()) {
                String currCategory = entry.getKey();

                List<String> seedList = entry.getValue();
                for (String seedWord : seedList) {
                    //				double currSimilarity = WordVector.computeSimilarityBetweenWords(Languages.EN, token, seedWord);
                    //				double currSimilarity = WordVector.computeSimilarityBetweenWords(Languages.EN, token, seedWord);
                    double currSimilarity = WordVector.cosSimilarity(documentVector, WordVector.getWordVector(Languages.EN, seedWord));
                    //					System.out.println("Similarity between " + token + " and " + seedWord + ": " + currSimilarity);
                    if (currSimilarity > maxSimilarity) {
                        maxSimilarity = currSimilarity;
                        maxSimilarSeed = seedWord;
                        maxSimilarCategoryStr = currCategory;
                    }
                }
            }
        }
        if (maxSimilarCategoryStr != null) {
            System.out.println("max similarity - " + maxSimilarity);
            maxSimilarCategory.add(maxSimilarCategoryStr);
        }

        String[] maxSimilarCategoryArr = new String[2];

        if (maxSimilarCategory != null) {
            maxSimilarCategoryArr[0] = maxSimilarSeed;
            if (maxSimilarCategory.size() > 0) {
                maxSimilarCategoryArr[1] = maxSimilarCategory.toString();
            } else {
                maxSimilarCategoryArr[1] = "Others";
            }
        }
        //		else {
        //			maxSimilarCategoryArr[0] = "Others";
        //			maxSimilarCategoryArr[1] = "Others";
        //		}
        return maxSimilarCategoryArr;

    }

    public static String[] classifyDocument(String documentText) {

        XpExpression xpE = new XpExpression(Languages.EN, Annotations.SENTI, false, "", "", "", documentText, true);
        //		Set<String> entityList = ((XpExpression) xpE).processPipelineForEntities(true);
        Map<String, Integer> entityMap = xpE.getEntityMap();
        Set<String> entityList = entityMap.keySet();
        Map<String, String> nerMap = xpE.getAllNerMap();
        return classifyDocument(entityList, nerMap);
        //		List<String> tokensList = ProcessString.tokenizeString(Languages.EN, documentText);
        //		return classifyDocument(tokensList);
    }

}