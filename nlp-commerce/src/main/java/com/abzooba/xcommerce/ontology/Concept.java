/**
 * @author: Alix Melchy
 * Apr 8, 2016
 */
package com.abzooba.xcommerce.ontology;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.graphdb.traversal.Uniqueness;
import org.neo4j.helpers.collection.IterableWrapper;

public class Concept {
	static final String NAME = "name";
	private Node concept;
	
	Concept(Node concept) {
		this.concept = concept;
	}
	
	public Node getConcept() {
		return concept;
	}
	
	public String getConceptName() {
		return (String) concept.getProperty(NAME);
	}
	
	public List<String> getLabels() {
		Iterable<Label> labels = concept.getLabels();
		List<String> answ = new ArrayList<String>();
		labels.forEach(l -> {answ.add(l.name());});
		return answ;
	}
	
	@Override
	public int hashCode() {
		return concept.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		return o instanceof Concept && concept.equals(((Concept) o).getConcept());
	}
	
	@Override
	public String toString(){
		return getConceptName();
	}
	
	public Iterable<Concept> getParentConcept() {
		return getEnclosingConceptAtRemoteness(1);
	}
	
	public Iterable<Concept> getChildrenConcepts() {
		return getEnclosedConceptsAtRemoteness(1);
	}
	
	public Iterable<Concept> getEquivalentConcepts() {
		TraversalDescription travDesc = graphDB().traversalDescription()
				.breadthFirst()
				.relationships(RelationTypes.sameAs)
				.uniqueness(Uniqueness.NODE_LEVEL)
				.evaluator(Evaluators.atDepth(1))
				.evaluator(Evaluators.excludeStartPosition());
		return extractConceptFromPath(travDesc.traverse(concept));
	}
	
	public Iterable<Concept> getSubGraphWithRel(RelationTypes rel) {
		return getSubGraphWithRel(rel, Direction.BOTH);
	}
	
	public Iterable<Concept> getSubGraphWithRel(RelationTypes rel, Direction dir) {
		TraversalDescription travDesc = graphDB().traversalDescription()
				.breadthFirst()
				.relationships(rel, dir)
				.uniqueness(Uniqueness.NODE_LEVEL)
				.evaluator(Evaluators.excludeStartPosition());
		return extractConceptFromPath(travDesc.traverse(concept));
	}
	
	private GraphDatabaseService graphDB() {
		return concept.getGraphDatabase();
	}
	
	private Iterable<Concept> getEnclosingConceptAtRemoteness(int remoteness) {
		TraversalDescription travDesc = graphDB().traversalDescription()
				.breadthFirst()
				.relationships(RelationTypes.isA, Direction.OUTGOING)
				.uniqueness(Uniqueness.NODE_GLOBAL)
				.evaluator(Evaluators.toDepth(remoteness))
				.evaluator(Evaluators.excludeStartPosition());
		return extractConceptFromPath(travDesc.traverse(concept));
	}
	
	private Iterable<Concept> getEnclosedConceptsAtRemoteness(int remoteness) {
		TraversalDescription travDesc = graphDB().traversalDescription()
				.breadthFirst()
				.relationships(RelationTypes.isA, Direction.INCOMING)
				.uniqueness(Uniqueness.NODE_GLOBAL)
				.evaluator(Evaluators.toDepth(remoteness))
				.evaluator(Evaluators.excludeStartPosition());
		return extractConceptFromPath(travDesc.traverse(concept));
	}
	
	private IterableWrapper<Concept, Path> extractConceptFromPath(Traverser iterableToWrap) {
		return new IterableWrapper<Concept, Path>(iterableToWrap) {
			@Override
			protected Concept underlyingObjectToObject(Path path) {
				return new Concept(path.endNode());
			}
		};
	}

}
