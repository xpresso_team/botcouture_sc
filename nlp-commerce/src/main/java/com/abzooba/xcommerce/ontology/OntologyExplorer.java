/**
 * @author: Alix Melchy
 * Apr 8, 2016
 */
package com.abzooba.xcommerce.ontology;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.WordUtils;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;

public class OntologyExplorer {

	private static final String DB_PATH = XCConfig.NEO4J_DB_PATH;
	private static GraphDatabaseService graphDB;
	public static Logger logger = XCLogger.getSpLogger();

	public static void init() {
		graphDB = new GraphDatabaseFactory().newEmbeddedDatabase(new File(DB_PATH));
		registerShutdownHook(graphDB);
		countingTest();
	}

	private static void countingTest() {
		try (Transaction tx = graphDB.beginTx()) {
			Result result = graphDB.execute("match (n) return count(n)");
			while (result.hasNext()) {
				Map<String, Object> row = result.next();
				for (Entry<String, Object> column : row.entrySet())
					logger.info("graphDB initialized with " + column.getValue() + " nodes");
			}
			tx.success();
		}
	}

	public static List<String> findSimilarConcept(String conceptName) {
		Concept concept = findConcept(conceptName);
		logger.info("conceptName: " + conceptName + "\tconcept null: " + (concept == null));
		if (concept != null) {
			try (Transaction tx = graphDB.beginTx()) {
				Iterable<Concept> similarConcepts = concept.getEquivalentConcepts();

				List<String> answ = new ArrayList<String>();
				similarConcepts.forEach(c -> {
					answ.add(c.toString());
				});
				tx.success();
				return answ;
			}
		} else
			return null;
	}

	public static Map<String, String> findSubcategories(String conceptName) {
		Concept concept = findConcept(conceptName);
		if (concept != null) {
			try (Transaction tx = graphDB.beginTx()) {
				Iterable<Concept> subConcepts = concept.getChildrenConcepts();

				Map<String, String> answ = new HashMap<String, String>();
				subConcepts.forEach(c -> {
					updateMap(c, answ);
				});
				tx.success();
				return answ;
			}
		} else
			return null;
	}

	public static Map<String, String> findRecommendations(String labelStr, String conceptName) {
		logger.info("Looking for recommendations for " + conceptName);
		Concept concept = findConcept(labelStr, conceptName);
		if (concept != null) {
			try (Transaction tx = graphDB.beginTx()) {
				Iterable<Concept> recommendations = concept.getSubGraphWithRel(RelationTypes.goesWellWith, Direction.OUTGOING);

				Map<String, String> answ = new HashMap<String, String>();
				recommendations.forEach(c -> {
					updateMap(c, answ);
				});
				tx.success();
				return answ;
			}
		} else
			return null;
	}

	private static Concept findConcept(String conceptName) {
		Node node = null;
		Map<String, Object> params = new HashMap<String, Object>();
		logger.info("conceptName initially: " + conceptName);
		conceptName = conceptName.replaceAll("s\\b", "");
		logger.info("conceptName after replacement: " + conceptName);
		params.put("name", "(?i)" + conceptName + "(s)*");
		String query = "MATCH (n) WHERE n.name =~ {name} RETURN n, n.name";
		try (Transaction tx = graphDB.beginTx(); Result result = graphDB.execute(query, params)) {
			int i = 1;
			while (result.hasNext()) {
				Map<String, Object> row = result.next();
				node = (Node) row.get("n");
				String name = (String) row.get("n.name");
				logger.info(query);
				System.out.print(i++);
				logger.info("\t" + node + "\t" + name);
			}
			tx.success();
		}
		if (node != null)
			return new Concept(node);
		else
			return null;
	}

	private static Concept findConcept(String labelStr, String conceptName) {
		labelStr = WordUtils.capitalizeFully(labelStr);
		Node node = null;
		Map<String, Object> params = new HashMap<String, Object>();
		logger.info("conceptName initially: " + conceptName);
		conceptName = conceptName.replaceAll("s\\b", "");
		logger.info("conceptName after replacement: " + conceptName);
		params.put("name", "(?i)" + conceptName + "(s)*");
		String query = "MATCH (n: `" + labelStr + "`) WHERE n.name =~ {name} RETURN n, n.name";
		try (Transaction tx = graphDB.beginTx(); Result result = graphDB.execute(query, params)) {
			int i = 1;
			while (result.hasNext()) {
				Map<String, Object> row = result.next();
				node = (Node) row.get("n");
				String name = (String) row.get("n.name");
				logger.info(query);
				System.out.print(i++);
				logger.info("\t" + node + "\t" + name);
			}
			tx.success();
		}
		if (node != null)
			return new Concept(node);
		else
			return null;
	}

	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running application).
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}

	private static void updateMap(Concept concept, Map<String, String> labelMap) {
		List<String> labelNames = concept.getLabels();
		for (String label : labelNames) {
			String value = labelMap.get(label);
			if (value == null)
				labelMap.put(label, concept.toString());
			else
				labelMap.put(label, value + ", " + concept.toString());
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		init();
		String conceptName = "Garment";
		findConcept(conceptName);

		conceptName = "Trousers";
		List<String> similar = findSimilarConcept(conceptName);
		logger.info(similar.toString());
	}

}
