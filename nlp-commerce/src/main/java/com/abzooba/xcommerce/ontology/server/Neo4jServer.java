/**
 * @author: Alix Melchy
 * Apr 26, 2016
 */
package com.abzooba.xcommerce.ontology.server;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.nlp.LoadNLPFiles;
import org.apache.commons.lang.WordUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.slf4j.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

public class Neo4jServer {

	private static String SERVER_ROOT_URI;
	private static String USRNAME;
	private static String PWD;
	private static WebTarget target;

	public static final String GARMENT_MATERIAL_LABEL = "FabricMaterial";

	private static Logger logger = XCLogger.getSpLogger();


	public static void init() {
		SERVER_ROOT_URI = XCConfig.NEO4J_SERVER;
		USRNAME = XCConfig.NEO4J_USRNAME;
		PWD = XCConfig.NEO4J_PWD;
		if (databaseIsRunning()) {
			logger.info("Neo4jServer up and running");
			String queryStr = "MATCH (n) RETURN count(n)";
			sendTransactionalCypherQuery(queryStr);
		}
	}

	private static boolean databaseIsRunning() {
		Client client = ClientBuilder.newClient();
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(USRNAME, PWD);
		client.register(feature);
		target = client.target(SERVER_ROOT_URI);
		Response response = target.request().header("application/xml", "true").get();
		String entity = response.readEntity(String.class);
		logger.info(String.format("GET from URI " + SERVER_ROOT_URI + "\tstatus code [%d], returned data: " + System.getProperty("line.separator") + "%s", response.getStatus(), entity));

		response.close();

		return response.getStatus() == 200;
	}

	/**
	 * @param  query
	 * @return String representation of the JSON query result
	 */
	private static String sendTransactionalCypherQuery(String query) {
		String txUri = "transaction/commit";
		String payload = "{\"statements\" : [ {\"statement\" : \"" + query + "\"} ]}";
		Response response = target.path(txUri).request(MediaType.APPLICATION_JSON).header("application/xml", "true").accept(MediaType.APPLICATION_JSON).post(Entity.entity(payload, MediaType.APPLICATION_JSON_TYPE));

		String entity = response.readEntity(String.class);
		logger.info(String.format("POST to URI " + SERVER_ROOT_URI + "\tstatus code [%d]", response.getStatus()));
		response.close();

		return entity;
	}

	private static List<String> extractRowValuesForSingleColumn(String entity) {
		List<String> values = new ArrayList<String>();
		JSONObject entityJSON;
		try {
			entityJSON = new JSONObject(entity);
			JSONArray results = entityJSON.getJSONArray("results");
			for (int resultsIdx = 0; resultsIdx < results.length(); resultsIdx++) {
				JSONObject result = results.getJSONObject(resultsIdx);
				JSONArray data = result.getJSONArray("data");
				for (int dataIdx = 0; dataIdx < data.length(); dataIdx++) {
					JSONObject row = data.getJSONObject(dataIdx);
					JSONArray rowValue = row.getJSONArray("row");
					try {
						values.add(rowValue.get(0).toString().toLowerCase());
					}
					catch (NullPointerException e){
						logger.info("JSON Array has a null value");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return values;
	}

	private static Map<String, String> extractValuesForTwoColumns(String entity) {
		Map<String, String> answ = new HashMap<String, String>();
		JSONObject entityJSON;
		try {
			entityJSON = new JSONObject(entity);
			JSONArray results = entityJSON.getJSONArray("results");
			for (int resultsIdx = 0; resultsIdx < results.length(); resultsIdx++) {
				JSONObject result = results.getJSONObject(resultsIdx);
				JSONArray data = result.getJSONArray("data");
				for (int dataIdx = 0; dataIdx < data.length(); dataIdx++) {
					JSONObject row = data.getJSONObject(dataIdx);
					JSONArray rowValue = row.getJSONArray("row");
					JSONArray keys = rowValue.getJSONArray(0);
					String value = (String) rowValue.get(1);
					if (value != null) {
						if (keys != null && keys.length() > 0) {
							for (int idx = 0; idx < keys.length(); idx++) {
								addToMap(answ, keys.getString(idx), value.toLowerCase());
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return answ;
	}

	private static void addToMap(Map<String, String> map, String key, String value) {
		if (map.containsKey(key)) {
			String temp = map.get(key);
			map.put(key, temp + ", " + value);
		} else {
			map.put(key, value);
		}
	}

	public static List<String> findParents(String conceptName) {
		conceptName = conceptName.replaceAll("s\\b", "");
		StringBuilder sb = new StringBuilder();
		sb.append("MATCH (n) -[:isA]-> (p) WHERE n.name =~ '(?i)" + conceptName + "(s)*' ");
		sb.append("RETURN p.name");
		String query = sb.toString();
		String entity = sendTransactionalCypherQuery(query);
		return extractRowValuesForSingleColumn(entity);
	}

	public static List<String> findSiblings(String conceptName) {
		conceptName = conceptName.replaceAll("s\\b", "");
		StringBuilder sb = new StringBuilder();
		sb.append("MATCH (n) -[:isA]-> (p) <-[:isA]- (q) WHERE n.name =~ '(?i)" + conceptName + "(s)*' ");
		sb.append("RETURN q.name");
		String query = sb.toString();
		String entity = sendTransactionalCypherQuery(query);
		return extractRowValuesForSingleColumn(entity);
	}

	public static List<String> findSimilarConcept(String conceptName) {
		conceptName = conceptName.replaceAll("s\\b", "");
		StringBuilder sb = new StringBuilder();
		sb.append("MATCH (n) -[:sameAs]- (p) WHERE n.name =~ '(?i)" + conceptName + "(s)*' ");
		sb.append("RETURN p.name");
		String query = sb.toString();
		String entity = sendTransactionalCypherQuery(query);
		return extractRowValuesForSingleColumn(entity);
	}

	public static Set<String> findAllConcepts() {
		Set<String> uniqueConcepts = new LinkedHashSet<String>();
		String[] queries = { "MATCH (m)-[:isA]->(n) RETURN m.name,n.name", "MATCH (m)-[:sameAs]->(n) RETURN m.name", "MATCH (m)-[:similarTo]->(n) RETURN m.name" };
		for (int i = 0; i < queries.length; i++) {
			String entity = sendTransactionalCypherQuery(queries[i]);
			List<String> tempList = extractRowValuesForSingleColumn(entity);
			uniqueConcepts.addAll(tempList);
		}
		return uniqueConcepts;
	}

	public static Map<String, String> findRecommendations(String labelStr, String conceptName) {
		StringBuilder sb = new StringBuilder();
		conceptName = conceptName.replaceAll("s\\b", "");
		if (labelStr.equalsIgnoreCase("details"))
			sb.append("MATCH (n) -[:isA*0..1]-> (i) -[:goesWellWith*1..2]-> (p)");
		else {
			labelStr = WordUtils.capitalizeFully(labelStr);
			sb.append("MATCH (n:`" + labelStr + "`) -[:goesWellWith*1..2]-> (p)");
		}
		sb.append("WHERE n.name =~ '(?i)" + conceptName + "(s)*' ");
		sb.append("RETURN labels(p), p.name");
		String query = sb.toString();
		String entity = sendTransactionalCypherQuery(query);

		return extractValuesForTwoColumns(entity);
	}

	public static List<String> returnOccasionTerms() {
		String query = "MATCH (n:`Occasion`) RETURN n.name";
		String entity = sendTransactionalCypherQuery(query);
		return extractRowValuesForSingleColumn(entity);
	}

	public static List<String> returnColorTerms() {
		String query = "MATCH (n:`Colours`) RETURN n.name";
		String entity = sendTransactionalCypherQuery(query);
		return extractRowValuesForSingleColumn(entity);
	}

	public static List<String> returnImplicitWomen() {
		String query = "MATCH (n:`ImplicitWomen`) RETURN n.name";
		String entity = sendTransactionalCypherQuery(query);
		return extractRowValuesForSingleColumn(entity);
	}

	public static List<String> returnImplicitMen() {
		String query = "MATCH (n:`ImplicitMen`) RETURN n.name";
		String entity = sendTransactionalCypherQuery(query);
		return extractRowValuesForSingleColumn(entity);
	}

	public static List<String> notAllowedLevensthein() {
		String query = "MATCH (n:`NotAllowedLevensthein`) RETURN n.name";
		String entity = sendTransactionalCypherQuery(query);
		return extractRowValuesForSingleColumn(entity);
	}

	public static void updateColorTerms() {
		// removing all color nodes except the central node (Colours)
		String queryPrune = "MATCH (n:`Colours` {name: 'Colours'}) <-[:isA]- (p) DETACH DELETE p";
		logger.info(queryPrune);
		String entity = sendTransactionalCypherQuery(queryPrune);
		logger.info("Pruned color nodes: " + entity);

		ArrayList<String> colorSet = LoadNLPFiles.getListOfColors();

		String queryStart = "MATCH (n:`Colours` {name: 'Colours'}) ";

		for (String currColor : colorSet) {
			String[] colorArr = currColor.split("\t");
			String colorName = colorArr[6].replaceAll("'", "");
			String queryEnd = "CREATE (p: `Colours`) -[:isA]-> (n) SET p.name = '" + colorName + "' ";
			String queryAdd = queryStart + queryEnd;
			logger.info(queryAdd);
			entity = sendTransactionalCypherQuery(queryAdd);
			logger.info("Added color: " + entity);
		}
		logger.info("Added colors from " + XCConfig.COLORRGB_FILE);
	}

	public static List<String> returnMaterialNames() {
		String query = "MATCH (n:`FabricMaterial`) RETURN n.name";
		String entity = sendTransactionalCypherQuery(query);
		return extractRowValuesForSingleColumn(entity);
	}
	public static List<String> returnBrandNames() {
		String query = "MATCH (n:`Brand`) RETURN n.name";
		String entity = sendTransactionalCypherQuery(query);
		return extractRowValuesForSingleColumn(entity);
	}
	public static void main(String[] args) {

		XCConfig.init();
		XCLogger.init();
		logger = XCLogger.getSpLogger();
		SERVER_ROOT_URI = XCConfig.NEO4J_SERVER;
		USRNAME = XCConfig.NEO4J_USRNAME;
		PWD = XCConfig.NEO4J_PWD;
		/* UPDATING COLOR NODES */
		if (databaseIsRunning()) {
			updateColorTerms();
		}
	}
}
