package com.abzooba.xcommerce.utils;

/**
 * Created by mayank on 19/4/17.
 */
public class Interval {

    private int start;
    private int end;

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }
}
