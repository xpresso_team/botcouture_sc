/**
 * 
 */
package com.abzooba.xcommerce.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * @author Vivek Aditya
 * Apr 26, 2016 12:51:36 PM
 * Synaptica  WordVectorUtils
 */
public class WordVectorUtils {
	public static String getClusterString(String str) {
		Document doc;
		String cluster = null;
		try {
			doc = Jsoup.connect("http://xpresso.abzooba.com:9999/" + str).timeout(30 * 1000).get();
			cluster = doc.body().text().replaceAll(" ", "\t");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return (cluster);

	}

	public static List<String> getCluster(String str, double threshold) {
		List<String> clusterPhrases = null;
		String clusterStr = getClusterString(str);
		if (clusterStr != null) {
			String[] clusterArr = clusterStr.split("\t");
			if (clusterArr.length > 0) {
				for (String currCluster : clusterArr) {
					String[] currStrArr = currCluster.split(":");
					if (currStrArr.length == 2) {
						double similarity = Double.parseDouble(currStrArr[1]);
						if (similarity > threshold) {
							if (clusterPhrases == null) {
								clusterPhrases = new ArrayList<String>();
							}
							clusterPhrases.add(currStrArr[0]);
						}
					}
				}
			}
		}
		return clusterPhrases;
	}
}
