package com.abzooba.xcommerce.utils;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.engine.config.XpConfig.Annotations;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpExpression;
import com.abzooba.xpresso.utils.FileIO;
import com.amazonaws.util.json.JSONArray;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;
import edu.stanford.nlp.util.CoreMap;

/**
 * @author Sudhanshu Kumar
 * Mar 31, 2016 12:10:00 AM
 * Parse the details field of product to get some featured tag, if present any
 */

public class DetailsParser {

	public static StanfordCoreNLP pipeline;
	public static Logger logger = XCLogger.getSpLogger();
	public static Set<String> genericStopWords = null;

	public static void init() {
		genericStopWords = new HashSet<String>();
		//		FileIO.read_file(XCConfig.processCommaSeparatedFileNames(query.getDomainName(), XCConfig.STOPWORDS_FILES), genericStopWords);
		if (XCConfig.GENERIC_STOPWORDS_FILES == null) {
			XCConfig.GENERIC_STOPWORDS_FILES = "resources/xcfashion-genericStopWords.txt";
		}
		FileIO.read_file(XCConfig.GENERIC_STOPWORDS_FILES, genericStopWords);
	}

	public static void main(String[] args) {

		XCEngine.init();
		Scanner s = new Scanner(System.in);
		String line = "";
		while (true) {
			logger.info("Enter the query");
			line = s.nextLine();
			if (line != null && line.trim() != null) {
				try {
					logger.info("Tag List - " + extractTags(line));
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				s.close();
				break;
			}
		}
	}

	public static List<String> extractTags(String queryStr) {
		TregexPattern tgrepPatternVP = TregexPattern.compile("(VP [<` RB |<` RBS | <` RBS])");
		TregexPattern tgrepPatternNP = TregexPattern.compile("(NP [<` NN | <` NNS | <` NNP | <` NNPS])");

		XpExpression xp = new XpExpression(Languages.EN, Annotations.EXPR, false, null, null, null, queryStr, false);
		List<CoreMap> sentencesList = xp.getSentences();
		Map<String, String> nerMap = xp.getAllNerMap();
		logger.info("NER map - " + nerMap);
		List<String> tempTagList = new ArrayList<String>();
		List<String> tagList = new ArrayList<String>();
		for (CoreMap sentence : sentencesList) {
			Tree parseTree = xp.getParseTree(sentence);

			TregexMatcher m_tree = tgrepPatternNP.matcher(parseTree);

			while (m_tree.find()) {
				Tree subTreeNP = m_tree.getMatch();
				String currentStr = Sentence.listToString(subTreeNP.yield());
				tempTagList.add(currentStr);
			}

			m_tree = tgrepPatternVP.matcher(parseTree);

			while (m_tree.find()) {
				Tree subTreeVP = m_tree.getMatch();
				String currentStr = Sentence.listToString(subTreeVP.yield());
				tempTagList.add(currentStr);
			}
		}

		for (String tempTag : tempTagList) {
			tempTag = tempTag.toLowerCase();
			boolean isNer = false;
			for (Entry<String, String> entry : nerMap.entrySet()) {
				String ner = entry.getKey().toLowerCase();
				if (tempTag.equalsIgnoreCase(ner) || ner.contains(tempTag) || tempTag.contains(ner)) {
					String nerType = entry.getValue();
					if (!nerType.equalsIgnoreCase("PERCENT")) {
						isNer = true;
						break;
					}
				}
			}
			if (!isNer)
				tagList.add(tempTag);
		}

		return tagList;
	}

	public static JSONArray tagsFromDetails(String details) {
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		String text = "The Back PU Shift Dress by Dorothy Perkins is the perfect way to bring a stylish '60s cut into your work wardrobe. Style yours with patent pumps. Our model is wearing a size 8 dress. She usually takes a Standard AU8 size, is 5’7” (170cm) tall and has a 64cm waist.- Length: 84cm - Straight fit - Medium-weight woven fabric - Black finish - Nude faux -leather piping - Twin flap hip pockets - Exposed metal zip fastening on the back - Matte satin lining Warm machine wash. Low iron. Length: 84cm Material: 75% Polyester, 19% Viscose & 6% Elastane";
		Annotation document = new Annotation(text);
		pipeline.annotate(document);

		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		for (CoreMap sentence : sentences) {
			// this is the parse tree of the current sentence
			Tree tree = sentence.get(TreeAnnotation.class);
			logger.info("sentence : " + sentence + "\n");
			for (Tree subtree : tree) {
				if (subtree.label().value().equals("NP") && subtree.size() < 10 && subtree.size() > 4) {
					String currentStr = Sentence.listToString(subtree.yield());
					logger.info("CurrentStr: " + currentStr);
				}
			}
			logger.info("==========================================\n");
		}
		return null;
	}

	public static JSONArray attributesFromDetails(String details) {

		String text = "The Back PU Shift Dress by Dorothy Perkins is the perfect way to bring a stylish '60s cut into your work wardrobe. Style yours with patent pumps. Our model is wearing a size 8 dress. She usually takes a Standard AU8 size, is 5’7” (170cm) tall and has a 64cm waist. - Length: 84cm - Straight fit - Medium-weight woven fabric - Black finish - Nude faux -leather piping - Twin flap hip pockets - Exposed metal zip fastening on the back - Matte satin lining Warm machine wash. Low iron. Length: 84cm Material: 75% Polyester, 19% Viscose & 6% Elastane";
		BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
		iterator.setText(text);
		int start = iterator.first();
		for (int end = iterator.next(); end != BreakIterator.DONE; start = end, end = iterator.next()) {
			String singleSentence = text.substring(start, end);
			logger.info(singleSentence);
			if (singleSentence.contains("-")) {
				String[] features = singleSentence.split("-");
				for (String feature : features) {
					logger.info("====== " + feature);
				}
			}
		}

		return null;
	}
}
