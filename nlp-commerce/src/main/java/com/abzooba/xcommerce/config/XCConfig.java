package com.abzooba.xcommerce.config;

import com.abzooba.xcommerce.core.XCLogger;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class XCConfig {
	public static Logger logger;
	public static String XCOMMERCE_DELIMITER = null;
	public static ClientConfiguration configuration = null;
	public static Region US_EAST1 = null;
	// Dynamodb Related
	public static boolean CREATE_FLAG = false;
	public static boolean DELETE_FLAG = false;
	public static String DYNAMODB_DATA_FILE = null;
	public static int maxReviewCount = -1;
	public static Boolean uploadSearchDocumentsFlag = null;
	public static String SEARCH_TYPE = null;
	public static int BATCH_SIZE = 2;
	public static int MAX_SEARCH_HITS = 10;
	public static int MAX_SUGGESTED_HITS = 10;
	public static int MIN_SEARCH_HITS = 10;
	public static String COLORRGB_FILE = null;
	public static String PRODUCTNAMEANDBRAND_FILE = null;
	public static String RESOURCES_DIR = null;
	public static String DOMAIN_KB_DIR = null;
	public static String DOMAINKB_SPREADSHEET_ID = null;
	public static String DOMAINKB_RANGE = null;
	public static String XC_HIERARCHY_SPREADSHEET_ID = null;
	public static String XC_HIERARCHY_RANGE = null;
	public static String XC_HIERARCHY_XC_CATEGORY_MAP_RANGE = null;
	public static String SUBCATEGORY_FILE = null;
	public static String CATEGORY_FILE = null;
	public static String CATEGORY_DELIM = null;
	public static Boolean SUBCATEGORY_WORD_VECTORS = null;
	public static Boolean TEST_THEMATIC = null;
	public static Boolean BUILD_PRODUCT_CATALOG = null;
	public static String DEFAULT_DATA_DOMAIN = null;
	public static String NEO4J_DB_PATH = null;
	public static String NEO4J_SERVER = null;
	public static String NEO4J_SERVER_BOLT = null;
	public static String NEO4J_USRNAME = null;
	public static String NEO4J_PWD = null;
	public static String WORDNET_DIR = null;

	public static String PRICE_QUALIFIER_FILE = null;
	public static String AMBIGUOUS_PRICE_QUALIFIER_FILE = null;
	public static String NUMBER_MULTIPLIER_FILE = null;
	/**
	 * Price half interval length for equality case:
	 * searching between price  x (100 - delta) / 100 and price x (100 + delta) / 100
	 * delta is a percentage to adapt to different prices
	 *
	 */
	public static int PRICE_DELTA;
	public static String WORD_VECTOR_SERVER_URL = null;

	public static String XPRESSO_CONFIGURATION_FILE = null;

	public static String WORD_LIST_FILE = null;
	public static String BIGRAM_LIST_FILE = null;

	public static String STOPWORDS_FILES = null;
	public static String GENERIC_STOPWORDS_FILES = null;
	public static String KEEPWORDS_FILES = null;

	public static String DOMAIN_CONFIG_PATH = null;

	public static Boolean IS_WORDVECTOR_CATEGORIES_LOAD = null;
	public static String CURRENCY_FILE = null;
	public static String TEXT_NUMBER_FILE = null;

	public static Boolean DATA_DRIVEN_ACTIVE = null;

	public static String CONCEPTNET_DATABASE = null;
	public static String CONCEPTNET_API = null;
	public static String CONCEPTNET_NEO4J = null;

	/*
	 * Determines how many top subcategories are picked
	 */
	public static int NB_TOP_CATEGORIES;

	/*
	 * Testing related
	 */
	public static String SRC = null;
	public static String RANGE = null;
	public static String SPREADSHEET_ID = null;
	public static Integer QUERY_ID = 0;
	public static Integer MAX_LINES_PROCESS = 0;
	public static Integer MAX_ITEMS_PROCESS = 0;
	public static Integer NO_OF_THREAD = 0;
	public static Boolean IS_OUTPUT_NEED = false;
	public static String OUTPUT_FILENAME = null;
	public static List<String> OUTPUT_FILE_LINES = null;
	public static String CURRENT_DATE;
	public static Boolean WEB_TEST = null;
	public static Boolean HAS_COLUMN_NAME = false;
	public static String SIZE_LIST_FILE = null;

	// Redis related
	public static String REDIS_SERVER = null;
	public static String REDIS_BRAND_PATH = null;

    // Entity extraction using data driven
    public static String RASA_SERVER = null;
    public static String RASA_TOKEN = null;

    public static String ES_URL = null;
	public static String ES_URL_RULE = null;

	public static String RESET_PREFIX = null;
	public static String RESET_BRAND = null;
	public static String RESET_ES = null;
	public static String RESET_GENDER = null;


	/**
	 * Initializes configurable variable using configuration file.
	 * Optional input of configuration file directory.
	 * @param values (configuration files directory)
	 */
	public static void init(String... values) {
		logger = XCLogger.getSpLogger();
		configuration = new ClientConfiguration();
		configuration.setProtocol(Protocol.HTTPS);
		configuration.setMaxErrorRetry(2);
		configuration.setConnectionTimeout(5 * 60 * 1000);
		configuration.setClientExecutionTimeout(5 * 60 * 1000);
		configuration.setSocketTimeout(5 * 60 * 1000);
		configuration.setRequestTimeout(5 * 60 * 1000);
		US_EAST1 = Region.getRegion(Regions.US_EAST_1);
		String propFile = "config/xcommerceConfig.properties";
		Properties prop = new Properties();
		try {
			InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFile);
			if (inputStream != null)
				prop.load(inputStream);
			else
				logger.error("Error in loading properties file : " + propFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		RESOURCES_DIR = prop.getProperty("resources.dir", "resources") + "/";
		DOMAIN_KB_DIR = prop.getProperty("domainkb.dir", "knowledgebase") + "/";
		maxReviewCount = Integer.parseInt(prop.getProperty("max_review_count", "5"));
		uploadSearchDocumentsFlag = Boolean.valueOf(prop.getProperty("upload_search_documents", "false"));
		BATCH_SIZE = Integer.parseInt(prop.getProperty("batch_size", "2"));
		SEARCH_TYPE = prop.getProperty("search_type", "structured");
		MAX_SEARCH_HITS = Integer.parseInt(prop.getProperty("max_search_hits", "10"));
		MAX_SUGGESTED_HITS = Integer.parseInt(prop.getProperty("max_suggested_hits", "10"));

		//If structured search hits is less than min_search_hits, it will append results from simple search
		MIN_SEARCH_HITS = Integer.parseInt(prop.getProperty("min_search_hits", "10"));
		COLORRGB_FILE = RESOURCES_DIR + prop.getProperty("colorrgb.file", "colorRGB.tsv");

		PRODUCTNAMEANDBRAND_FILE = prop.getProperty("productnameandbrand.file", "productCatalog.tsv");
		SUBCATEGORY_FILE = prop.getProperty("subcategory.file", "sub-categories.txt");
		CATEGORY_FILE = prop.getProperty("category.file", "categories.txt");
		CATEGORY_DELIM = prop.getProperty("category.delim", "\t");
		SUBCATEGORY_WORD_VECTORS = Boolean.parseBoolean(prop.getProperty("subcategory.wv", "false"));
		TEST_THEMATIC = Boolean.parseBoolean(prop.getProperty("test.thematic", "false"));
		BUILD_PRODUCT_CATALOG = Boolean.parseBoolean(prop.getProperty("build.product.catalog", "false"));
		DEFAULT_DATA_DOMAIN = prop.getProperty("default.data.domain", "xcfashion");

		NEO4J_DB_PATH = DOMAIN_KB_DIR + prop.getProperty("neo4j.db.path", "default.graphdb");
		NEO4J_SERVER = prop.getProperty("neo4j.server", "http://107.20.68.187:7474/db/data/");
		NEO4J_SERVER_BOLT = prop.getProperty("neo4j.server.bolt", "bolt://34.207.113.57:7687");
		NEO4J_USRNAME = prop.getProperty("neo4j.usrname", "neo4j");
		NEO4J_PWD = prop.getProperty("neo4j.pwd", "abz00ba1nc");
		PRICE_QUALIFIER_FILE = RESOURCES_DIR + prop.getProperty("price.qualifiers.file", "price-qualifiers.txt");
		AMBIGUOUS_PRICE_QUALIFIER_FILE = RESOURCES_DIR + prop.getProperty("ambiguous.price.qualifiers.file", "ambiguous-price-qualifiers.txt");
		NUMBER_MULTIPLIER_FILE = RESOURCES_DIR + prop.getProperty("number.multipliers.file", "number-multiplier.txt");
		PRICE_DELTA = Integer.parseInt(prop.getProperty("price.delta", "10"));
		WORD_VECTOR_SERVER_URL = prop.getProperty("wv.server.url", "xpresso.abzooba.com:9999");
		WORDNET_DIR = prop.getProperty("wordnet.dir", "input/resources-static/WordNet-3.0");
		XPRESSO_CONFIGURATION_FILE = prop.getProperty("xpresso.config.path", "config/xpressoConfig.properties");

		WORD_LIST_FILE = RESOURCES_DIR + prop.getProperty("word_list.file", "american-english");
		BIGRAM_LIST_FILE = RESOURCES_DIR + prop.getProperty("bigram_list.file", "bigram.txt");

		STOPWORDS_FILES = prop.getProperty("stopwords.files", "xcfashion-stopWords.txt");
		GENERIC_STOPWORDS_FILES = RESOURCES_DIR + prop.getProperty("genericstopwords.files", "xcfashion-genericStopWords.txt");
		KEEPWORDS_FILES = prop.getProperty("keepwords.files", "xcfashion-keepWords.txt");

		DOMAINKB_SPREADSHEET_ID = prop.getProperty("DOMAINKB_SPREADSHEET_ID", "1txJgaC7zpyZlG6w1s0DXaGun-j_9Jl_vsv2zEE8pJzY");
		DOMAINKB_RANGE = prop.getProperty("DOMAINKB_RANGE", "Total!A1:AQ1016");
		DOMAIN_CONFIG_PATH = DOMAIN_KB_DIR + prop.getProperty("domainconfig.path", "domainConfig.json");

		// Add XC_HIERARCHY related details
		XC_HIERARCHY_SPREADSHEET_ID = prop.getProperty("XC_HIERARCHY_SPREADSHEET_ID", "1y24-uioPd8CKR88PMHSduvudTAapfupM71hygtfsXww");
		XC_HIERARCHY_RANGE = prop.getProperty("XC_HIERARCHY_RANGE", "With_H3-H4!A2:Y347");
		XC_HIERARCHY_XC_CATEGORY_MAP_RANGE = prop.getProperty("XC_HIERARCHY_XC_CATEGORY_MAP", "NLP_map!A2:B100");

		IS_WORDVECTOR_CATEGORIES_LOAD = Boolean.parseBoolean(prop.getProperty("mongo.wv.load", "false"));
		CURRENCY_FILE = RESOURCES_DIR + prop.getProperty("currency.file", "currency-list.txt");
		TEXT_NUMBER_FILE = RESOURCES_DIR + prop.getProperty("text.number.file", "text-number-list.txt");

		CONCEPTNET_DATABASE = prop.getProperty("conceptnet.db", "rest.api");
		CONCEPTNET_API = prop.getProperty("conceptnet.api", "http://107.20.68.187:81/data/5.4/");
		CONCEPTNET_NEO4J = prop.getProperty("conceptnet.neo4j", "bolt://107.20.68.187");
		XCOMMERCE_DELIMITER = prop.getProperty("XCOMMERCE_DELIMITER", "#&#abz#&#");

		DATA_DRIVEN_ACTIVE = Boolean.parseBoolean(prop.getProperty("data.driven.active", "false"));

		NB_TOP_CATEGORIES = Integer.parseInt(prop.getProperty("nb.top.categories", "1"));
		SIZE_LIST_FILE = RESOURCES_DIR + prop.getProperty("size.list.file", "sizeList.txt");
		/*
		 * Dynamodb related
		 */
		CREATE_FLAG = Boolean.parseBoolean(prop.getProperty("CREATE_FLAG", "false"));
		DELETE_FLAG = Boolean.parseBoolean(prop.getProperty("DELETE_FLAG", "false"));
		DYNAMODB_DATA_FILE = prop.getProperty("DYNAMODB_DATA_FILE ", "");

		// Redis related
		REDIS_SERVER = prop.getProperty("redis.server.url", "http://34.207.113.57:9001");
		REDIS_BRAND_PATH = prop.getProperty("redis.brand.path", "/v1/redis/getBrands");

        //Entity Extraction using rasa
        RASA_SERVER = prop.getProperty("rasa.server.url");
        RASA_TOKEN = prop.getProperty("rasa.token");

        //Elastic search url
        ES_URL = prop.getProperty("es.url");
		ES_URL_RULE = prop.getProperty("es.url.rule");

		//Reset keys
		RESET_PREFIX = prop.getProperty("reset.prefix");
		RESET_BRAND = prop.getProperty("reset.brand");
		RESET_ES = prop.getProperty("reset.es");
		RESET_GENDER = prop.getProperty("reset.gender");
	}

	public static String processCommaSeparatedFileNames(String domainName, String fileNames) {
		String answ = null;
		String[] fileNameArr = fileNames.split(",");
		for (String name : fileNameArr) {
			if (domainName.equals(name.split("-")[0])) {
				answ = RESOURCES_DIR + name;
				break;
			}
		}
		return answ;
	}
}
