package com.abzooba.xcommerce.domain.xcfashion;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.utils.SpreadSheetReader;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mayank on 2/5/17.
 */
public class LoadXCHierarchy {

    private static Logger logger;
    private static List<XCHierarchy> xcHierarchyList = new ArrayList<>();
    private static Multimap<String,String> nlpMapping = HashMultimap.create();
    private static List<String> specialClass =  Arrays.asList("plus","maternity", "petite","big and tall");

    public static void init()
    {
        logger = XCLogger.getSpLogger();
        loadXCHierarchy();
        loadNLPMapping();;
    }

    private static void loadXCHierarchy()
    {
        List<List<Object>> lines = null;
        try {
            lines = SpreadSheetReader.getTestData(XCConfig.XC_HIERARCHY_SPREADSHEET_ID, XCConfig.XC_HIERARCHY_RANGE);
            if(lines != null && lines.size() > 0)
            {
                for(List line:lines)
                {
                    List h4_level = new ArrayList();
                    List h4_level_lemmas = new ArrayList();
                    String h1_level = line.size() > 0 ? ((String) line.get(0)).toLowerCase() : null;
                    String h2_level = line.size() > 1 ? ((String) line.get(1)).toLowerCase() : null;
                    String h3_level = line.size() > 2 ? ((String) line.get(2)).toLowerCase() : null;

                    if(line.size() > 3) {
                        for (int i=3; i< line.size();i++) {
                            String h4 = ((String) line.get(i)).toLowerCase();
                            h4_level.add(h4);
                            String lemma = CoreNLPController.lemmatizeToString(h4);
                            h4_level_lemmas.add(lemma != null ? lemma : h4);
                        }
                    }
                    XCHierarchy xcHierarchy = new XCHierarchy(h1_level, h2_level, h3_level, h4_level,
                            CoreNLPController.lemmatizeToString(h1_level) != null ? CoreNLPController.lemmatizeToString(h1_level) : h1_level,
                            CoreNLPController.lemmatizeToString(h2_level) != null ? CoreNLPController.lemmatizeToString(h2_level) : h2_level,
                            h3_level != null ? (CoreNLPController.lemmatizeToString(h3_level) != null ? CoreNLPController.lemmatizeToString(h3_level) : h3_level) : null,
                            h4_level_lemmas);
                    xcHierarchyList.add(xcHierarchy);
                }
            }
        } catch (IOException e) {
            logger.error("Error in opening spreadsheet for xc_hierarchy");
        }
    }

    private static void loadNLPMapping()
    {
        List<List<Object>> lines = null;
        try {
            lines = SpreadSheetReader.getTestData(XCConfig.XC_HIERARCHY_SPREADSHEET_ID, XCConfig.XC_HIERARCHY_XC_CATEGORY_MAP_RANGE);
            if(lines != null && lines.size() > 0)
            {
                for(List line:lines)
                {
                    if(line.size() == 2)
                    {
                        nlpMapping.put(((String) line.get(1)).toLowerCase(),((String) line.get(0)).toLowerCase());
                    }
                }
            }
        } catch (IOException e) {
            logger.error("Error in opening spreadsheet for nlp xc_category mapping to xc_hierarchy");
        }
    }

    public static List<XCHierarchy> getXCHierarchyList() {
        return xcHierarchyList;
    }

    public static Multimap<String, String> getNlpMapping() {
        return nlpMapping;
    }

    public static List<String> getSpecialClass() {
        return specialClass;
    }

    public static void setSpecialClass(List<String> specialClass) {
        LoadXCHierarchy.specialClass = specialClass;
    }
}
