package com.abzooba.xcommerce.domain;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCEntity;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.core.XCQuery;
import com.abzooba.xcommerce.search.SearchUtils;

/**
 * @author Alix Melchy Sep 1, 2016
 *
 */
public class IntentUtils {

	private static Logger logger = XCLogger.getSpLogger();

	/* Non-instantiable class */
	private IntentUtils() {
		throw new AssertionError();
	}

	/* Clarification node for structured search api */
	public static JSONObject getClarification(Map<String, String[]> filterMap, Map<String, String[]> relaxedMap) {
		JSONObject clarificationJson = new JSONObject();
		try {
			if (filterMap != null && relaxedMap != null) {

				JSONObject oldNewPair = null;
				for (Map.Entry<String, String[]> entry : filterMap.entrySet()) {
					oldNewPair = new JSONObject();
					oldNewPair.put("Old", Arrays.asList(filterMap.get(entry.getKey())));
					oldNewPair.put("New", Arrays.asList(relaxedMap.get(entry.getKey())));
					clarificationJson.put(entry.getKey(), oldNewPair);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return clarificationJson;
	}

	/**
	 * Generates the domain specific query intent associated with entity.
	 * 
	 * @param domain
	 * @param entity
	 * @param query
	 */
	public static void processIntent(String domain, XCEntity entity, XCQuery query) {
		switch (domain) {
			case "xcfashion":
				xcFashionProcess(entity, query);
				break;
			case "xcelectronics":
				xcElectronicsProcess(entity, query);
				break;
		}
	}

	private static void xcFashionProcess1(XCEntity entity, XCQuery query) {
		try {
			if (query.getIntent().has(SearchUtils.COLOR_KEY)) {
				if (entity.getAttribute(SearchUtils.COLOR_KEY) != null) {
				}
			} else {
				if (entity.getAttribute(SearchUtils.COLOR_KEY) != null)
					query.getIntent().put(SearchUtils.COLOR_KEY, entity.getAttribute(SearchUtils.COLOR_KEY));
			}

			if (query.getIntent().has(SearchUtils.GENDER_KEY)) {
				if (entity.getAttribute(SearchUtils.GENDER_KEY) != null)
					((JSONArray) query.getIntent().get(SearchUtils.GENDER_KEY)).put(entity.getAttribute(SearchUtils.GENDER_KEY));
			} else {
				if (entity.getAttribute(SearchUtils.GENDER_KEY) != null)
					query.getIntent().put(SearchUtils.GENDER_KEY, new JSONArray().put(entity.getAttribute(SearchUtils.GENDER_KEY)));
			}

			if (query.getIntent().has(SearchUtils.GENERIC_KEY)) {
				if (entity.getAttribute(SearchUtils.GENERIC_KEY) != null)
					((JSONArray) query.getIntent().get(SearchUtils.GENERIC_KEY)).put(entity.getAttribute(SearchUtils.GENERIC_KEY));
			} else {
				if (entity.getAttribute(SearchUtils.GENERIC_KEY) != null)
					query.getIntent().put(SearchUtils.GENERIC_KEY, new JSONArray().put(entity.getAttribute(SearchUtils.GENERIC_KEY)));
			}

			if (query.getIntent().has(SearchUtils.ITEM_KEY)) {
				((JSONArray) query.getIntent().get(SearchUtils.ITEM_KEY)).put(entity.getEntityStr());
			} else {
				query.getIntent().put(SearchUtils.ITEM_KEY, new JSONArray().put(entity.getEntityStr()));
			}

			if (query.getIntent().has(SearchUtils.PRICE_NUMERIC_KEY)) {
				((JSONArray) query.getIntent().get(SearchUtils.PRICE_NUMERIC_KEY)).put(entity.getAttribute(SearchUtils.PRICE_NUMERIC_KEY));
			} else {
				query.getIntent().put(SearchUtils.PRICE_NUMERIC_KEY, new JSONArray().put(entity.getAttribute(SearchUtils.PRICE_NUMERIC_KEY)));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Adding intent and clarification node.
	 * Clarification node contains updated values of filters after relaxation during the start search process.
	 */
	private static void xcFashionProcess(XCEntity entity, XCQuery query) {
		JSONObject intentJson = new JSONObject();
		JSONObject clarificationJson = new JSONObject();
		Map<String, Set<String>> filterMap;
		Map<String, Set<String>> relaxedMap;

		try {
			if (entity.getStructuredSearchResults().size() > 0) {

				JSONObject oldNewPair = new JSONObject();
				oldNewPair.put("Old", entity.getEntityStr());
				oldNewPair.put("New", entity.getRelaxedEntityStr());
				clarificationJson.put("entity", oldNewPair);
				intentJson.put("entity", entity.getEntityStr());

				filterMap = entity.getAttributeMap();
				relaxedMap = entity.relaxationMap;
				if (filterMap != null) {

					oldNewPair = new JSONObject();
					oldNewPair.put("Old", new JSONArray(filterMap.get(SearchUtils.GENDER_KEY)));
					oldNewPair.put("New", new JSONArray(relaxedMap.get(SearchUtils.GENDER_KEY)));
					clarificationJson.put("gender", oldNewPair);
					intentJson.put("gender", new JSONArray(filterMap.get(SearchUtils.GENDER_KEY)));

					oldNewPair = new JSONObject();
					oldNewPair.put("Old", entity.getConsumerSubCategory());
					oldNewPair.put("New", entity.getConsumerSubCategory());
					clarificationJson.put("subcategory", oldNewPair);
					intentJson.put("subcategory", entity.getConsumerSubCategory());

					if (entity.getMainEntityList() != null && !entity.getMainEntityList().isEmpty()) {
						oldNewPair = new JSONObject();
						oldNewPair.put("Old", new JSONArray(entity.getMainEntityList()));
						oldNewPair.put("New", new JSONArray(entity.getMainEntityList()));
						clarificationJson.put("xc_category", oldNewPair);
						intentJson.put("xc_category", new JSONArray(entity.getMainEntityList()));
					} else {
						oldNewPair = new JSONObject();
						oldNewPair.put("Old", new JSONArray(filterMap.get(SearchUtils.XC_CATEGORY_KEY)));
						oldNewPair.put("New", new JSONArray(relaxedMap.get(SearchUtils.XC_CATEGORY_KEY)));
						clarificationJson.put("xc_category", oldNewPair);
						intentJson.put("xc_category", new JSONArray(filterMap.get(SearchUtils.XC_CATEGORY_KEY)));
					}

					oldNewPair = new JSONObject();
					oldNewPair.put("Old", new JSONArray(filterMap.get(SearchUtils.BRAND_KEY)));
					oldNewPair.put("New", new JSONArray(relaxedMap.get(SearchUtils.BRAND_KEY)));
					clarificationJson.put("brand", oldNewPair);
					intentJson.put("brand", new JSONArray(filterMap.get(SearchUtils.BRAND_KEY)));

					oldNewPair = new JSONObject();
					oldNewPair.put("Old", new JSONArray(filterMap.get(SearchUtils.COLOR_KEY)));
					oldNewPair.put("New", new JSONArray(relaxedMap.get(SearchUtils.COLOR_KEY)));
					clarificationJson.put("colorsavailable", oldNewPair);
					intentJson.put("colorsavailable", new JSONArray(filterMap.get(SearchUtils.COLOR_KEY)));

					oldNewPair = new JSONObject();
					oldNewPair.put("Old", new JSONArray(filterMap.get(SearchUtils.PRICE_NUMERIC_KEY)));
					oldNewPair.put("New", new JSONArray(relaxedMap.get(SearchUtils.PRICE_NUMERIC_KEY)));
					clarificationJson.put("price", oldNewPair);
					intentJson.put("price", new JSONArray(filterMap.get(SearchUtils.PRICE_NUMERIC_KEY)));

					oldNewPair = new JSONObject();
					oldNewPair.put("Old", new JSONArray(filterMap.get(SearchUtils.SIZE)));
					oldNewPair.put("New", new JSONArray(relaxedMap.get(SearchUtils.SIZE)));
					clarificationJson.put("size", oldNewPair);
					intentJson.put("size", new JSONArray(filterMap.get(SearchUtils.SIZE)));

					oldNewPair = new JSONObject();
					oldNewPair.put("Old", new JSONArray(filterMap.get(SearchUtils.GENERIC_KEY)));
					oldNewPair.put("New", new JSONArray(relaxedMap.get(SearchUtils.GENERIC_KEY)));
					clarificationJson.put("details", oldNewPair);
					intentJson.put("details", new JSONArray(filterMap.get(SearchUtils.GENERIC_KEY)));

					oldNewPair = new JSONObject();
					oldNewPair.put("Old", new JSONArray(filterMap.get(SearchUtils.FEATURE_KEY)));
					oldNewPair.put("New", new JSONArray(relaxedMap.get(SearchUtils.FEATURE_KEY)));
					clarificationJson.put("feature", oldNewPair);
					intentJson.put("feature", new JSONArray(filterMap.get(SearchUtils.FEATURE_KEY)));
				}

				/*query can be null when called from IntentAPI for structureSearch call*/
				if (query != null) {
					query.setClarification(clarificationJson);
					query.setIntent(intentJson);
					logger.info(query.getIntent().toString());
				}
				entity.setIntent(intentJson);
				entity.setClarification(clarificationJson);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void xcElectronicsProcess(XCEntity entity, XCQuery query) {
		try {
			if (entity.getAttribute(SearchUtils.BRAND_KEY) != null) {
				query.getIntent().put(SearchUtils.BRAND_KEY, entity.getAttribute(SearchUtils.BRAND_KEY));
			}
			if (entity.getAttribute(SearchUtils.SCREEN_TYPE) != null) {
				query.getIntent().put(SearchUtils.SCREEN_TYPE, entity.getAttribute(SearchUtils.SCREEN_TYPE));
			}
			if (entity.getAttribute(SearchUtils.DISPLAY_TYPE) != null) {
				query.getIntent().put(SearchUtils.DISPLAY_TYPE, entity.getAttribute(SearchUtils.DISPLAY_TYPE));
			}
			if (entity.getAttribute(SearchUtils.PRICE_NUMERIC_KEY) != null) {
				query.getIntent().put(SearchUtils.PRICE_NUMERIC_KEY, entity.getAttribute(SearchUtils.PRICE_NUMERIC_KEY));
			}
			if (entity.getAttribute(SearchUtils.PRODUCT_NAME_KEY) != null) {
				query.getIntent().put(SearchUtils.PRODUCT_NAME_KEY, entity.getAttribute(SearchUtils.PRODUCT_NAME_KEY));
			}
			if (entity.getAttribute(SearchUtils.WIFI_ENABLED) != null) {
				query.getIntent().put(SearchUtils.WIFI_ENABLED, entity.getAttribute(SearchUtils.WIFI_ENABLED));
			}
			if (entity.getAttribute(SearchUtils.SMART_TV) != null) {
				query.getIntent().put(SearchUtils.SMART_TV, entity.getAttribute(SearchUtils.SMART_TV));
			}
			if (entity.getAttribute(SearchUtils.NAME) != null) {
				query.getIntent().put(SearchUtils.NAME, entity.getAttribute(SearchUtils.NAME));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
