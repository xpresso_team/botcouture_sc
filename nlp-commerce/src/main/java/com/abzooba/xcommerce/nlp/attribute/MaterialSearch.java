/**
 * @author: Alix Melchy
 * May 13, 2016
 */
package com.abzooba.xcommerce.nlp.attribute;

import java.util.HashSet;
import java.util.Set;

import com.abzooba.xcommerce.core.XCEntity;
import com.abzooba.xcommerce.ontology.server.Neo4jServer;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xpresso.engine.core.XpLexMatch;

public class MaterialSearch {
	private static Set<String> materialSet = new HashSet<String>();

	public static void init() {
		materialSet.addAll(Neo4jServer.returnMaterialNames());
	}

	private static String checkForMaterial(String str) {
		return XpLexMatch.sequenceContain(materialSet, str);
	}

	public static boolean spotMaterial(String str, XCEntity entity) {
		String queryStrLower = str.toLowerCase();
		String material = checkForMaterial(queryStrLower);
		if (material != null) {
			if (entity != null) {
				String entityStr = entity.getEntityStr();
				entity.setEntityStr(entityStr.replaceAll("\\b" + material + "\\b", "").trim());
				entity.setAttribute(SearchUtils.FEATURE_KEY, material);
			}
			return true;
		}
		return false;
	}

}
