package com.abzooba.xcommerce.nlp.attribute;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.entity.ContentType;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;

/**
 * Created by mayank on 18/5/17.
 */
public class RedisBrand {
	public static final String REDIS_BRAND_URL = XCConfig.REDIS_SERVER + XCConfig.REDIS_BRAND_PATH;
	public static Logger logger = XCLogger.getSpLogger();
	private Set<String> brands = new HashSet<>();

	public Set<String> getBrandList() {
		URL obj = null;
		HttpURLConnection con = null;
		try {
			obj = new URL(REDIS_BRAND_URL);
		} catch (MalformedURLException e1) {
			logger.error("Url is not correct : " + REDIS_BRAND_URL + "\n" + e1.getMessage());
		}
		try {
			if (obj != null) {
				con = (HttpURLConnection) obj.openConnection();
				if (con != null) {
					con.setRequestMethod("GET");
					con.setRequestProperty("Content-Type", ContentType.APPLICATION_JSON.getMimeType() + ";charset=utf-8");
					con.setRequestProperty("Accept", ContentType.APPLICATION_JSON.getMimeType());

					int statusCode = con.getResponseCode();
					logger.info("Response Code:\t" + statusCode);

					if (statusCode >= 400 && statusCode < 500) {
						logger.error("Request Exception:\t" + con.getContent().toString());
					} else if (statusCode >= 500 && statusCode < 600) {
						logger.error("Internal Server Error. Please try again as this might be a transient error condition.");
					} else {
						BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
						String inputLine;

						while ((inputLine = in.readLine()) != null) {
							JSONObject jsonObject = new JSONObject(inputLine);
							if (jsonObject.has("size") && (Integer.parseInt(jsonObject.get("size").toString())) > 0) {
								if (jsonObject.has("brand")) {
									JSONArray jsonArray = jsonObject.getJSONArray("brand");
									if (jsonArray != null) {
										int len = jsonArray.length();
										for (int i = 0; i < len; i++) {
											brands.add(jsonArray.get(i).toString().toLowerCase());
										}
									}
								}

							}

						}
						in.close();
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error occured in getting brands from redis\n" + e.getMessage());
		} finally {
			if (con != null)
				con.disconnect();
		}
		return brands;
	}
}