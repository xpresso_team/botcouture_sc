package com.abzooba.xcommerce.nlp;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.utils.FileIO;
import org.slf4j.Logger;

import java.awt.*;
import java.util.*;

/**
 * Created by mayank on 3/4/17.
 */
public class LoadNLPFiles {

    private static Logger logger;
    private static String FILEPATH = "resources/";
    private static ArrayList<String> listOfColors = new ArrayList<>();
    private static ArrayList<String> priceQualifierList = new ArrayList<>();
    private static ArrayList<String> ambiguousPriceQualifierList = new ArrayList<>();
    private static ArrayList<String> sizeList = new ArrayList<>();
    private static ArrayList<String> listOfDicWords = new ArrayList<>();
    private static ArrayList<String> listBigramFileEntires = new ArrayList<>();
    private static Stack<String> numberMultiplierStack = new Stack<>();
    private static Stack<String> textNumberStack = new Stack<>();
    private static Stack<String> currencyStack = new Stack<>();
    private static ArrayList<String> domainConfigPath = new ArrayList<>();
    private static Set<String> productList = new HashSet<String>();

    public static void init()
    {
        XCLogger.init();
        logger = XCLogger.getSpLogger();

        // load colors
        FileIO.read_file(XCConfig.COLORRGB_FILE, listOfColors);
        if((listOfColors == null) || (listOfColors.size() == 0))
            logger.error("Error in loading colors file : "+XCConfig.COLORRGB_FILE);

        // load price qualifiers
        FileIO.read_file(XCConfig.PRICE_QUALIFIER_FILE, priceQualifierList);
        if((priceQualifierList == null) || (priceQualifierList.size() == 0))
            logger.error("Error in loading price qualifier : "+XCConfig.PRICE_QUALIFIER_FILE);

        FileIO.read_file(XCConfig.AMBIGUOUS_PRICE_QUALIFIER_FILE, ambiguousPriceQualifierList);
        if((ambiguousPriceQualifierList == null) || (ambiguousPriceQualifierList.size() == 0))
            logger.error("Error in loading ambiguous price qualifiers : "+XCConfig.AMBIGUOUS_PRICE_QUALIFIER_FILE);


        // load size list
        FileIO.read_file(XCConfig.SIZE_LIST_FILE, sizeList);
        if((sizeList == null) || (sizeList.size() == 0))
            logger.error("Error in loading size file : "+XCConfig.SIZE_LIST_FILE);

        // load dictionary words for spell checker
        FileIO.read_file(XCConfig.WORD_LIST_FILE, listOfDicWords);
        if((listOfDicWords == null) || (listOfDicWords.size() == 0))
            logger.error("Error in loading dictionary file : "+XCConfig.WORD_LIST_FILE);

        // load bigrams
        FileIO.read_file(XCConfig.BIGRAM_LIST_FILE, listBigramFileEntires);
        if((listBigramFileEntires == null) || (listBigramFileEntires.size() == 0))
            logger.error("Error in loading bigram file : "+XCConfig.BIGRAM_LIST_FILE);

        // load number multiplier file
        FileIO.read_file(XCConfig.NUMBER_MULTIPLIER_FILE, numberMultiplierStack);
        if((numberMultiplierStack == null) || (numberMultiplierStack.size() == 0))
            logger.error("Error in loading number multiplier file : "+XCConfig.NUMBER_MULTIPLIER_FILE);

        // load text number file
        FileIO.read_file(XCConfig.TEXT_NUMBER_FILE, textNumberStack);
        if((textNumberStack == null) || (textNumberStack.size() == 0))
            logger.error("Error in loading text number file : "+XCConfig.TEXT_NUMBER_FILE);

        // load currency file
        FileIO.read_file(XCConfig.CURRENCY_FILE, currencyStack);
        if((currencyStack == null) || (currencyStack.size() == 0))
            logger.error("Error in loading currency file : "+XCConfig.CURRENCY_FILE);

        // load domain config path
        FileIO.read_file(XCConfig.DOMAIN_CONFIG_PATH, domainConfigPath);
        if((domainConfigPath == null) || (domainConfigPath.size() == 0))
            logger.error("Error in loading doamin config path file : "+XCConfig.DOMAIN_CONFIG_PATH);

        // load brand list
        //FileIO.read_file(FILEPATH + XCConfig.DEFAULT_DATA_DOMAIN + "-brand.tsv", productList);
        //productList.addAll(Neo4jServer.returnBrandNames());
        //if((productList == null) || (productList.size() == 0))
        //    logger.error("Error in loading product list from file : "+XCConfig.DEFAULT_DATA_DOMAIN + "-brand.tsv");
    }

    public static ArrayList<String> getListOfColors() {
        return listOfColors;
    }

    public static ArrayList<String> getPriceQualifierList() {
        return priceQualifierList;
    }

    public static ArrayList<String> getAmbiguousPriceQualifierList() {
        return ambiguousPriceQualifierList;
    }

    public static ArrayList<String> getSizeList() {
        return sizeList;
    }

    public static ArrayList<String> getListOfDicWords() {
        return listOfDicWords;
    }

    public static ArrayList<String> getListBigramFileEntires() {
        return listBigramFileEntires;
    }

    public static Map<String, String> getPriceQualifierMap() {
        return parseQualifierFile(priceQualifierList);
    }

    public static Map<String, String> getAmbiguousPriceQualifierMap() {
        return parseQualifierFile(ambiguousPriceQualifierList);
    }

    public static Stack<String> getNumberMultiplierStack() {
        return numberMultiplierStack;
    }

    public static Stack<String> getTextNumberStack() {
        return textNumberStack;
    }

    public static Stack<String> getCurrencyStack() {
        return currencyStack;
    }

    public static ArrayList<String> getDomainConfigPath() {
        return domainConfigPath;
    }

    public static Set<String> getProductList() { return productList;    }

    private static HashMap<String, String> parseQualifierFile(ArrayList<String> qualifierList) {
        HashMap<String, String> qualifierMap = new HashMap<>();
        for (String line : qualifierList) {
            String[] splitArr = line.split("\t");
            if (splitArr.length == 2) {
                qualifierMap.put(splitArr[0], splitArr[1]);
            }
        }
        return qualifierMap;
    }

    public static HashMap<String,String[]> getTextNumberMap() {
        Stack<String> tempStack = textNumberStack;
        HashMap<String,String[]> numberTextMap = new HashMap<>();
        while (!tempStack.isEmpty()) {
            String line = tempStack.pop();
            String[] lineArr = line.split("\t");
            if (lineArr.length != 3)
                continue;
            String[] numberArr = { lineArr[1], lineArr[2] };
            numberTextMap.put(lineArr[0], numberArr);
        }
        return numberTextMap;
    }

    public static HashMap<String,String> getNumberMultiplierMap() {
        Stack<String> tempStack = numberMultiplierStack;
        HashMap<String,String> multiplierMap = new HashMap<>();
        while (!tempStack.isEmpty()) {
            String line = tempStack.pop();
            String[] lineArr = line.split("\t");
            if (lineArr.length != 2)
                continue;
            multiplierMap.put(lineArr[0], lineArr[1]);
        }
        return multiplierMap;
    }

    public static Set<String> getCuurencySet() {
        Stack<String> tempStack = currencyStack;
        Set<String> currencySet = new HashSet<>();
        while (!tempStack.isEmpty()) {
            String tempStr = tempStack.pop();
            String[] strArr = tempStr.split("\t");
            if (strArr.length != 2)
                continue;
            currencySet.add(strArr[0]);
            currencySet.add(strArr[1]);
        }
        return currencySet;
    }

    public static Map<String,Color> getColorObjectMap() {
        Map<String,Color> colorMap = new HashMap<>();
        for (int i = 1; i < listOfColors.size(); i++) {
            String currLine = listOfColors.get(i);
            String[] strArr = currLine.split("\t");
            if (strArr.length > 1) {
                String colorStr = strArr[0].toLowerCase();
                String hexCode = strArr[1];
                if (hexCode.length() > 0) {
                    Color color = Color.decode(hexCode);
                    colorMap.put(colorStr, color);
                }
            }
        }
        return colorMap;
    }
}
