package com.abzooba.xcommerce.nlp.attribute;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCEntity;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.ontology.server.Neo4jServer;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xcommerce.utils.WhitespaceCheck;

/**
 * @author Sudhanshu Kumar
 * Apr 1, 2016 11:10:12 AM
 * synaptica  SearchByProductName
 * used to extract the list of products and corresponding brands relevant to query.
 * One counter has been set to 50 in getProductList function, in case query returns more than 50 results eg, dress.
 */
public class SearchByProductName {

	private static String FILEPATH = "resources/";
	private static Logger logger;
	public static Set<String> brandList = null;
	public static Set<String> brandListFromNeo4j = new HashSet<String>();

	public static Logger getLogger() {
		return logger;
	}

	/**
	 * Initializing the Class. Creates and loads the brand names from the product catalog
	 */
	public static void init() {
		logger = XCLogger.getSpLogger();
		RedisBrand redisBrand = new RedisBrand();
		try {
			brandList = redisBrand.getBrandList();
		} catch (Exception e) {
			logger.error("Error in reading brands from redis");
		}
		if ((brandList == null) || !(brandList.size() > 0)) {
			brandListFromNeo4j.addAll(Neo4jServer.returnBrandNames());
			brandList.addAll(brandListFromNeo4j);
		}
	}

	/**
	 * 
	 * @param spEntity - SpEntity whose attribute map has to be updated.
	 * @return - True, if brand name found, else false
	 */
	public static boolean searchByProductName(XCEntity spEntity) {
		//		return false;
		logger.info("In SearchByProductName -> searchByProductName");
		String entityStr = spEntity.getQuery().getOriginalQuery().toLowerCase();
		/* "do you have it by haute curry?" */
		entityStr = entityStr.replace("?", "");
		logger.info("Query str: " + entityStr + "\tentity str: " + spEntity.getEntityStr());

		String matchingProduct = getMatchingProduct(entityStr);

		while (matchingProduct != null && entityStr.toLowerCase().contains(matchingProduct.toLowerCase())) {
			logger.info("Query String for brand search : " + entityStr);
			spEntity.setAttribute(SearchUtils.BRAND_KEY, matchingProduct);
			logger.info("Brand found : " + spEntity.getAttribute(SearchUtils.BRAND_KEY));
			String newEntityStr = "";
			if (WhitespaceCheck.containsWhitespace(matchingProduct)) {
				String enStr = spEntity.getEntityStr().toLowerCase();
				for (String str : matchingProduct.toLowerCase().split("\\s")) {
					enStr = enStr.replaceAll("\\b" + str + "\\b", "");
				}
				newEntityStr = enStr;
			} else
				newEntityStr = spEntity.getEntityStr().toLowerCase().replaceAll("\\b" + matchingProduct.toLowerCase() + "\\b", "");
			if (!newEntityStr.isEmpty()) {
				spEntity.setEntityStr(newEntityStr);
			} else {
				spEntity.setAttribute(SearchUtils.XC_CATEGORY_KEY, SearchUtils.ALL_CATEGORIES);
				spEntity.setEntityStr(SearchUtils.ALL_CATEGORIES);
			}
			logger.info("String after brand removal : " + spEntity.getEntityStr());
			/* 
			 * Issue: with multi-word brands, the following cleaning is ineffective
			 * e.g. 'studio 8'
			 * */
			Set<String> genericAttributes = spEntity.getAttribute(SearchUtils.GENERIC_KEY);
			String[] matchingProductParts = matchingProduct.toLowerCase().split(" ");
			for (String matchingProd : matchingProductParts) {
				logger.info("matchingProd: " + matchingProd);
				if (genericAttributes != null && genericAttributes.contains(matchingProd.toLowerCase())) {
					logger.info("Removing from generic attributes brand name: " + matchingProd.toLowerCase());
					genericAttributes.remove(matchingProd.toLowerCase());
					spEntity.setAttribute(SearchUtils.GENERIC_KEY, genericAttributes);
				}
				String newMultiWordEntity = spEntity.getMultiWordEntity().toLowerCase().replaceAll("\\b" + matchingProd.toLowerCase() + "\\b", "");
				if (newMultiWordEntity != null)
					spEntity.setMultiWordEntity(newMultiWordEntity, true);
			}
			logger.info("Multi-word entity after brand removal: " + spEntity.getMultiWordEntity());
			if (entityStr.trim().isEmpty() || entityStr.trim() == null)
				spEntity.setEntityStr(matchingProduct);

			entityStr = entityStr.replaceAll("\\b" + matchingProduct.toLowerCase() + "\\b", "");
			matchingProduct = getMatchingProduct(entityStr);
		}
		if (spEntity.getAttribute(SearchUtils.BRAND_KEY) != null)
			return true;
		else {
			logger.info("Brand found : " + null);
			return false;
		}

	}

	/**
	 * Screens entity that are exactly a brand name.
	 * @param entity
	 * @return whether the entity is rejected as being a brand name
	 */
	public static boolean isRejectedByBrand(String entity) {
		//		return false;
		logger.info("In SearchByProductName -> isRejectedByBrand");
		String brandMatch = getMatchingProduct(entity);
		return brandMatch != null && brandMatch.equals(entity.toLowerCase());
	}

	/**
	 * 
	 * @param query - Input query
	 * @return - Brand name
	 */
	public static String getMatchingProduct(String query) {
		logger.info("In SearchByProductName -> getMatchingProduct");
		String finalMatch = null;
		for (String line : brandList) {
			String commonWords = getCommonWords(query, line);
			if (commonWords.trim().length() == line.trim().length()) {
				finalMatch = line.toLowerCase();
				break;
			}
		}
		return finalMatch;
	}

	/**
	 * 
	 * @param query - Input query from which branch name has to be identified
	 * @param line - Brand name from the databse to be matched
	 * @return - common words from the two inputs
	 */
	private static String getCommonWords(String query, String line) {
		if (query != null && line != null) {

			query = query.toLowerCase().replaceAll("\"", "");
			line = line.toLowerCase().replaceAll("\"", "");

			String[] arr1 = query.split("[\\s]");
			String[] arr2 = line.split("[\\s]");
			Set<String> commonWords = new HashSet<String>();
			for (int i = 0; i < arr1.length; i++) {
				for (int j = 0; j < arr2.length; j++) {
					if (arr1[i].equalsIgnoreCase(arr2[j])) {
						commonWords.add(arr1[i]);
					} else {
						/* Calvin Klein's hipster briefs */
						String word = arr1[i].split("'")[0];
						if (word.equalsIgnoreCase(arr2[j]))
							commonWords.add(word);
					}
				}
			}

			StringBuilder sb = new StringBuilder();
			for (String item : commonWords) {
				sb.append(item + " ");
			}
			return sb.toString();

		}
		return null;
	}

	/**
	 * 
	 * @param query - Query from which brand name has to be identified.
	 * @return - Query after removing redundant and stop words.
	 */
	public static String removeStopWords(String query) {

		String[] stopwords = { "i", "the", "you", "some", "simple", "a", "want", "plain", "black", "in", "at", "for", "under", "above", "between", "of", "and", "by", "color", "over", "below", "with", "less", "more", "than", "top" };
		ArrayList<String> wordsList = new ArrayList<String>();

		query = query.trim().replaceAll("\\s+", " ");
		String[] words = query.split(" ");

		for (String word : words) {
			wordsList.add(word);
		}

		for (int j = 0; j < stopwords.length; j++) {
			if (wordsList.contains(stopwords[j])) {
				wordsList.remove(stopwords[j]);
			}
		}
		String queryResult = "";
		for (String str : wordsList) {
			queryResult = queryResult + " " + str;
		}
		queryResult = queryResult.trim();
		return queryResult;
	}
}