package com.abzooba.xcommerce.nlp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.nlp.attribute.SearchByProductName;
import com.abzooba.xcommerce.nlp.domain.DomainCategoryMapper;

/**
 * @author Sudhanshu Kumar Aug 11, 2016
 */

public class SpellChecker {

	public static Logger logger;

	public class DamerauLevenshteinDistance {
		/**
		 * Calculate the Damerau-Levenshtein Distance (edit distance)
		 * between two strings.
		 *
		 * @param source Source input string
		 * @param target Target input string
		 * @return The number of substitutions it would take
		 * to make the source string identical to the target
		 * string
		 */
		public int calculate(String source, String target) {
			//If both strings are empty, I'm of the opinion that
			//this is an error (technically the distance is zero).
			assert (!(source.isEmpty() && target.isEmpty()));

			//If the source string is empty, the distance is the
			//length of the target string.
			if (source.isEmpty()) {
				return target.length();
			}

			//If the target string is empty, the distance is the
			//length of the source string.
			if (target.isEmpty()) {
				return source.length();
			}

			//Delegate the calculation to the method that produces the matrix
			//and distance, but then only return the distance
			return calculateAndReturnFullResult(source, target).getDistance();
		}

		/**
		 * Perform the distance calculation, but also return the
		 * resulting matrix and distance.
		 *
		 * @param source Source input string
		 * @param target Target input string
		 * @return A simple object with the matrix and distance
		 */
		public DameauLevenshteinDistanceResult calculateAndReturnFullResult(String source, String target) {

			//If both strings are empty, I'm of the opinion that
			//this is an error (technically the distance is zero).
			assert (!(source.isEmpty() && target.isEmpty()));

			//We are going to construct a matrix of distances
			int[][] distanceMatrix = new int[source.length() + 1][target.length() + 1];

			//We need indexers from 0 to the length of the source string.
			//This sequential set of numbers will be the row "headers"
			//in the matrix.
			for (int sourceIndex = 0; sourceIndex <= source.length(); sourceIndex++) {

				//Set the value of the first cell in the row
				//equivalent to the current value of the iterator
				distanceMatrix[sourceIndex][0] = sourceIndex;
			}

			//We need indexers from 0 to the length of the target string.
			//This sequential set of numbers will be the
			//column "headers" in the matrix.
			for (int targetIndex = 0; targetIndex <= target.length(); targetIndex++) {

				//Set the value of the first cell in the column
				//equivalent to the current value of the iterator
				distanceMatrix[0][targetIndex] = targetIndex;
			}

			//We'll use this to add a penalty
			//to some operations.
			int cost = 0;

			//Iterate over all characters in the source
			//string.
			for (int sourceIndex = 1; sourceIndex <= source.length(); sourceIndex++) {

				//Iterate over all characters in the target
				//string.
				for (int targetIndex = 1; targetIndex <= target.length(); targetIndex++) {

					//If the current characters in both strings are equal
					if (source.charAt(sourceIndex - 1) == target.charAt(targetIndex - 1)) {
						//There is no penalty.
						cost = 0;
					} else {
						//Not equal, there is a penalty.
						cost = 1;
					}

					//We want to find the current distance by determining
					//the shortest path to a match (hence the 'minimum'
					//calculation on distances).
					distanceMatrix[sourceIndex][targetIndex] = minimum(
					//Character match between current character in
					//source string and next character in target
					distanceMatrix[sourceIndex - 1][targetIndex] + 1,
					//Character match between next character in
					//source string and current character in target
					distanceMatrix[sourceIndex][targetIndex - 1] + 1,
					//No match, at current, add cumulative penalty
					distanceMatrix[sourceIndex - 1][targetIndex - 1] + cost);

					//We don't want to do the next series of calculations on
					//the first pass because we would get an index out of bounds
					//exception.
					if (sourceIndex == 1 || targetIndex == 1) {
						continue;
					}

					//transposition check (if the current and previous
					//character are switched around (e.g.: t[se]t and t[es]t)...
					if (source.charAt(sourceIndex - 1) == target.charAt(targetIndex - 2) && source.charAt(sourceIndex - 2) == target.charAt(targetIndex - 1)) {

						//What's the minimum cost between the current distance
						//and a transposition.
						distanceMatrix[sourceIndex][targetIndex] = minimum(
						//Current cost
						distanceMatrix[sourceIndex][targetIndex],
						//Transposition
						distanceMatrix[sourceIndex - 2][targetIndex - 2] + cost);
					}
				}
			}

			//Return the matrix and distance as the result
			return new DameauLevenshteinDistanceResult(distanceMatrix);
		}

		/**
		 * Calculate the minimum value from an array of values.
		 *
		 * @param values Array of values.
		 * @return minimum value of the provided set.
		 */
		private int minimum(int... values) {

			//Hopefully, everything should be smaller
			//than the max int value!
			int currentMinimum = Integer.MAX_VALUE;

			//Iterate over all provided values
			for (int value : values) {

				//Take the minimum value between the current
				//minimum and the current value of the
				//iteration
				currentMinimum = Math.min(value, currentMinimum);
			}

			//return the minimum value.
			return currentMinimum;
		}

		/**
		 * Simple container for the result of the Dameau-Levenshtein
		 * Distance calculation
		 */
		public class DameauLevenshteinDistanceResult {

			//Distance matrix
			private int[][] distanceMatrix;

			/**
			 * Instantiate the object with the resulting distance matrix
			 *
			 * @param distanceMatrix Matrix of distances between edits
			 */
			public DameauLevenshteinDistanceResult(int[][] distanceMatrix) {
				this.distanceMatrix = distanceMatrix;
			}

			/**
			 * Get the Distance Matrix
			 *
			 * @return Matrix of edit distances
			 */
			public int[][] getDistanceMatrix() {
				return distanceMatrix;
			}

			/**
			 * Get the Edit Distance
			 *
			 * @return number of changes to make before
			 * both strings are identical
			 */
			public int getDistance() {
				return distanceMatrix[distanceMatrix.length - 1][distanceMatrix[0].length - 1];
			}

			/**
			 * Get a string representation of this class
			 *
			 * @return A friendly display of the distance and matrix
			 */
			@Override
			public String toString() {

				StringBuilder sb = new StringBuilder();

				sb.append(String.format("Distance: %s \n", this.getDistance()));
				sb.append("Matrix: \n\n");

				for (int i = 0; i < this.distanceMatrix.length; i++) {

					sb.append("| ");

					for (int j = 0; j < this.distanceMatrix[0].length; j++) {

						sb.append(String.format("\t%s", this.distanceMatrix[i][j]));
					}

					sb.append(" |\n");
				}

				return sb.toString();
			}
		}
	}

	public static ArrayList<String> listCategories = new ArrayList<>();
	public static ArrayList<String> listDicWords = new ArrayList<>();
	public static HashMap<String, HashMap<String, Integer>> hmBigrams = new HashMap<String, HashMap<String, Integer>>();
	private static ArrayList<String> sizeList = new ArrayList<>();

	public static void init() {
		logger = XCLogger.getSpLogger();
		sizeList = LoadNLPFiles.getSizeList();
		listDicWords = LoadNLPFiles.getListOfDicWords();
		listDicWords.replaceAll(l -> l.toLowerCase());
		loadBigrams();
		loadCategories();
		listCategories.replaceAll(l -> l.toLowerCase());
	}

	private static void loadCategories() {
		try {
			ArrayList<String> tmplistCategories = DomainCategoryMapper.getAllSubCategories();
			listCategories = (ArrayList<String>) tmplistCategories.parallelStream().distinct().collect(Collectors.toList());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void loadBigrams() {
		// format => count word1 word2
		ArrayList<String> listBigramFileEntires = LoadNLPFiles.getListBigramFileEntires();
		String[] tokens;
		HashMap<String, HashMap<String, Integer>> hmTemp1 = new HashMap<String, HashMap<String, Integer>>();
		for (String sCurrentLine : listBigramFileEntires) {
			tokens = sCurrentLine.split("\\t");
			HashMap<String, Integer> hmTemp2 = new HashMap<String, Integer>();

			if (hmTemp1.containsKey(tokens[1])) {
				hmTemp2 = hmTemp1.get(tokens[1]);
				hmTemp2.put(tokens[2], Integer.parseInt(tokens[0]));
				hmTemp1.put(tokens[1], hmTemp2);
			} else {
				hmTemp2.put(tokens[2], Integer.parseInt(tokens[0]));
				hmTemp1.put(tokens[1], hmTemp2);
			}
		}
	}

	public String checkMySpelling(String query) {
		try {
			if (query.trim().length() < 1)
				return query;
			String spellCheckedQueryWord = "";
			String[] lQueryWord = query.split(" ");
			for (int i = 0; i < lQueryWord.length; i++) {
				String queryToken = lQueryWord[i].trim();
				if (i < lQueryWord.length - 1 && validate_words(queryToken) && validate_words(lQueryWord[i + 1].trim())) {
					if (listCategories.contains(queryToken + lQueryWord[i + 1].trim())) {
						spellCheckedQueryWord += " " + queryToken + lQueryWord[i + 1].trim();
						i++;
						continue;
					}
				}
				if (validate_words(queryToken.trim())) {
					spellCheckedQueryWord += " " + queryToken;
				} else {
					String sTmp = checkSpellingSingleToken(queryToken);
					if (!sTmp.equals(queryToken))
						spellCheckedQueryWord += " " + checkSpellingSingleToken(queryToken); //to add here
					else {
						String prevQueryToken, postQueryToken;
						if (i == 0 && i == lQueryWord.length - 1) {
							prevQueryToken = null;
							postQueryToken = null;
						} else if (i == 0) {
							prevQueryToken = null;
							postQueryToken = lQueryWord[i + 1];
						} else if (i == lQueryWord.length - 1) {
							prevQueryToken = lQueryWord[i - 1];
							postQueryToken = null;
						} else {
							prevQueryToken = lQueryWord[i - 1];
							postQueryToken = lQueryWord[i + 1];
						}

						String[] aQueryTokens = { prevQueryToken, queryToken, postQueryToken };
						String tsSpellChecked = checkSpellingSecond(aQueryTokens);
						spellCheckedQueryWord += " " + tsSpellChecked;
					}
				}
			}

			return spellCheckedQueryWord.trim();
		} catch (Exception e) {
			e.printStackTrace();
			return query;
		}
	}

	private String checkSpellingSecond(String[] queryToken) {
		ArrayList<String> listConfusionSetDamerau = generateConfusionSetDamerau(queryToken[1]);
		double maxScore = 0.0, tmpScore;
		String maxScored = null;
		if (listConfusionSetDamerau != null) {
			//to add categories
			ArrayList<String> common = new ArrayList<String>(listConfusionSetDamerau);
			common.retainAll(listCategories);
			if (common.parallelStream().distinct().collect(Collectors.toList()).size() == 1)
				return common.get(0);
			if (queryToken[0] == null) {
				for (String a : listConfusionSetDamerau) {
					String[] b = { a, queryToken[2] };
					tmpScore = getProbScore(b);

					if (tmpScore > maxScore) {
						maxScored = a;
					}
				}

			} else if (queryToken[2] == null) {
				for (String a : listConfusionSetDamerau) {
					String[] b = { queryToken[0], a };
					tmpScore = getProbScore(b);
					if (tmpScore > maxScore) {
						maxScored = a;
					}
				}
			} else {
				for (String a : listConfusionSetDamerau) {
					String[] b = { a, queryToken[2] };
					String[] ts = { queryToken[0], a };
					tmpScore = getProbScore(b) + getProbScore(ts);
					if (tmpScore > maxScore) {
						maxScored = a;
					}
				}
			}
		}
		if (maxScored != null)
			return maxScored;
		return queryToken[1];

	}

	private ArrayList<String> generateConfusionSetDamerau(String queryToken) {
		ArrayList<String> liTmp = new ArrayList<>();
		DamerauLevenshteinDistance distanceCalc = new DamerauLevenshteinDistance();
		for (String stoken : listDicWords) {

			if (distanceCalc.calculate(stoken, queryToken) < 2)
				liTmp.add(stoken);
		}
		return liTmp;
	}

	public ArrayList<String> generateConfusionSetDamerau(String queryToken, Set<String> candidates) {
		ArrayList<String> liTmp = new ArrayList<>();
		DamerauLevenshteinDistance distanceCalc = new DamerauLevenshteinDistance();
		for (String stoken : candidates) {

			if (distanceCalc.calculate(stoken, queryToken) < 2)
				liTmp.add(stoken);
		}
		return liTmp;
	}

	private String checkSpellingSingleToken(String queryToken) {
		double maxScore = 0.0, tmpScore;
		String[] maxScoreSet = {};
		for (String[] a : generateConfusionSet(queryToken)) {
			tmpScore = getProbScore(a);
			if (tmpScore > maxScore) {
				maxScoreSet = a;
			}
		}
		if (maxScoreSet.length > 1) {
			return maxScoreSet[0] + " " + maxScoreSet[1];
		}
		return queryToken;

	}

	private double getProbScore(String[] a) {
		if (hmBigrams.containsKey(a[0])) {
			HashMap<String, Integer> hmTemp2 = new HashMap<String, Integer>();
			hmTemp2 = hmBigrams.get(a[0]);
			if (hmTemp2.containsKey(a[1])) {
				return (double) hmTemp2.get(a[1]) / hmTemp2.values().stream().mapToInt(Integer::intValue).sum();
			}
			return 0;

		}

		return 0;
	}

	public ArrayList<String[]> generateConfusionSet(String queryWord) {
		ArrayList<String[]> listConfusionSet = new ArrayList<String[]>();
		for (int i = 1; i < queryWord.length(); i++) {
			String word1 = queryWord.substring(0, i);
			String word2 = queryWord.substring(i);
			if (validate_words(word1, word2)) {
				String[] arrayStrTmp = { word1, word2 };
				listConfusionSet.add(arrayStrTmp);
			}
		}
		return listConfusionSet;
	}

	private boolean validate_words(String word1, String word2) {
		return listDicWords.contains(word1) && listDicWords.contains(word2);
	}

	private boolean validate_words(String word1) {
		return listDicWords.contains(word1) || listDicWords.contains(word1.toLowerCase()) || word1.matches("[0-9]*") || SearchByProductName.isRejectedByBrand(word1) || sizeList.contains(word1.toLowerCase());
	}

	public static String getSoundexCode(String wordStr, boolean censusOption, int lengthOption) {

		if (censusOption) {
			lengthOption = 4;
		}
		int soundExLen = lengthOption;

		if (soundExLen > 10) {
			soundExLen = 10;
		}
		if (soundExLen < 4) {
			soundExLen = 4;
		}

		if (wordStr == null || wordStr.equals("")) {
			return null;
		}

		wordStr = wordStr.replaceAll("[^\\s\\w]+", " "); // replace non-chars w space
		wordStr = wordStr.toUpperCase();

		/**
		 * Enhancements
		 */

		if (!censusOption) {

			/* v1.0e: GH at begining of word has G-sound (e.g., ghost)
			*/
			wordStr = wordStr.replace("^(GH)", "G"); // Chng leadng GH to G

			wordStr = wordStr.replace("(DG)", "G"); // Change DG to G
			wordStr = wordStr.replace("(GH)", "H"); // Change GH to H
			wordStr = wordStr.replace("(GN)", "N"); // Change GN to N
			wordStr = wordStr.replace("(KN)", "N"); // Change KN to N
			wordStr = wordStr.replace("(PH)", "F"); // Change PH to F
			wordStr = wordStr.replace("MP([STZ])", "M$1"); // MP if fllwd by ST|Z
			wordStr = wordStr.replace("^(PS)", "S"); // Chng leadng PS to S
			wordStr = wordStr.replace("^(PF)", "F"); // Chng leadng PF to F
			wordStr = wordStr.replace("(MB)", "M"); // Chng MB to M
			wordStr = wordStr.replace("(TCH)", "CH"); // Chng TCH to CH
		}

		/**
		 * Original Soundex
		 */

		/* The above improvements may
		 * have changed this first letter
		*/
		char firstChar = wordStr.charAt(0);

		/* in case 1st letter is
		 * an H or W and we're in
		 * CensusOption = 1
		*/
		if (firstChar == 'H' || firstChar == 'W') {
			wordStr = "-" + wordStr.substring(1);
		}

		/* In properly done census
		 * SoundEx the H and W will
		 * be squezed out before
		 * performing the test
		 * for adjacent digits
		 * (this differs from how
		 * 'real' vowels are handled)
		*/
		if (censusOption) {
			wordStr = wordStr.replace("[HW]", ".");
		}

		/**
		 *  Begin Classic SoundEx
		 */
		wordStr = wordStr.replaceAll("[AEIOUYHW]", "0");
		wordStr = wordStr.replaceAll("[BPFV]", "1");
		wordStr = wordStr.replaceAll("[CSGJKQXZ]", "2");
		wordStr = wordStr.replaceAll("[DT]", "3");
		wordStr = wordStr.replaceAll("[L]", "4");
		wordStr = wordStr.replaceAll("[MN]", "5");
		wordStr = wordStr.replaceAll("[R]", "6");

		/* Properly done census:
		 * squeeze H and W out
		 * before doing adjacent
		 * digit removal.
		*/
		if (censusOption) {
			wordStr = wordStr.replace("[\\.]+", "");
		}

		/* Remove extra equal adjacent digits
		*/
		int WSLen = wordStr.length();
		StringBuilder sb = new StringBuilder();
		// removed v10c djr:  TmpStr = "-";  /* rplcng skipped first char */

		for (int i = 0; i < WSLen; i++) {
			char currChar = wordStr.charAt(i);
			if (i + 1 >= WSLen || (i + 1 < WSLen && currChar != wordStr.charAt(i + 1)))
				sb.append(currChar);
			else if (i + 1 < WSLen && currChar == wordStr.charAt(i + 1))
				i++;
		}
		wordStr = sb.toString();
		logger.info("word string after cleaning up adjacent digits - " + wordStr);

		wordStr = wordStr.substring(1); /* Drop first letter code   */
		wordStr = wordStr.replaceAll("[\\s]+", ""); /* remove spaces            */
		wordStr = wordStr.replaceAll("[0]+", ""); /* remove zeros             */
		wordStr += "0000000000"; /* pad with zeros on right  */

		wordStr = firstChar + wordStr; /* Add first letter of word */

		wordStr = wordStr.substring(0, soundExLen); /* size to taste     */

		return wordStr;

	}

	public String MSSpellCheck(String query) {

		HttpClient httpclient = HttpClients.createDefault();

		try {
			URIBuilder builder = new URIBuilder("https://api.cognitive.microsoft.com/bing/v5.0/spellcheck/");

			builder.setParameter("mode", "proof");
			builder.setParameter("mkt", "en-us");

			URI uri = builder.build();
			HttpPost request = new HttpPost(uri);

			// timeout on response
			final RequestConfig params = RequestConfig.custom().setConnectTimeout(3000).setSocketTimeout(3000).build();
			request.setConfig(params);

			request.setHeader("Content-Type", "application/x-www-form-urlencoded");
			//			request.setHeader("Ocp-Apim-Subscription-Key", "a494937c9ff84035b7346f675de6bb59");
			request.setHeader("Ocp-Apim-Subscription-Key", "3ca1c82fd1e84a6f9ecf87771c2475a6");

			// Request body
			StringEntity reqEntity = new StringEntity("Text=" + query);
			request.setEntity(reqEntity);

			HttpResponse response = httpclient.execute(request);
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				JSONObject jObj = new JSONObject(EntityUtils.toString(entity));
				logger.info("Entity : " + jObj.toString());
				JSONArray jArr = jObj.getJSONArray("flaggedTokens");
				String spellCorrected = query;

				for (int i = 0; i < jArr.length(); i++) {
					JSONObject tkn = jArr.getJSONObject(i);
					String token = tkn.getString("token");
					int offset = Integer.parseInt(tkn.getString("offset"));
					String suggestion = tkn.getJSONArray("suggestions").getJSONObject(0).getString("suggestion");
					// TODO: Replace with offset, otherwise all matching sub string will be replaced
					spellCorrected = spellCorrected.substring(0, offset) + spellCorrected.substring(offset).replaceFirst(token, suggestion);
					listDicWords.add(suggestion);
					//					FileIO.write_file(query + "\t" + token + "\t" + suggestion, "resources/bingSpellCorrected.tsv", true);
				}
				return spellCorrected;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main1(String args[]) {
		logger.info("dsd");
		XCConfig.init();
		DamerauLevenshteinDistance distanceCalc = new SpellChecker().new DamerauLevenshteinDistance();
		String distOne = "shart";
		try (BufferedReader br = new BufferedReader(new FileReader(XCConfig.WORD_LIST_FILE))) {
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {

				if (distanceCalc.calculate(distOne, sCurrentLine) < 2)
					logger.info(sCurrentLine);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		String originalQuery = "thats good";
		System.out.println("Original Query : " + originalQuery);
		SpellChecker spChk = new SpellChecker();
		System.out.println("MS Spell Corrected query : " + spChk.MSSpellCheck(originalQuery));
	}

}
