package com.abzooba.xcommerce.nlp.attribute;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCEntity;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.nlp.ColorSimilarity;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.textanalytics.JWIController;
import com.abzooba.xpresso.textanalytics.PosTags;

import edu.mit.jwi.item.POS;
import edu.stanford.nlp.ling.IndexedWord;

/**
 * 
 * @author Sudhanshu Kumar
 * Spots color from the passed string. If color exists, sets the entity's attribute map.
 */
public class ColorSearch {

	public static Logger logger = XCLogger.getSpLogger();

	/**
	 * 
	 * @param str - Color String
	 * @param posTag - If pos tag is adjective or noun, it will be dealt separately
	 * @param entity - SpEntity for which color is identified and set
	 * @return - True if color exists in the str, else false
	 */
	public static boolean spotColor(String str, String posTag, XCEntity entity) {
		str = str.replaceAll("[^a-zA-Z\\s]", "");
		logger.info("String for color search : " + str);
		return checkForColor(str, posTag, entity);

	}

	private static boolean checkForColor(String str, String posTag, XCEntity entity) {
		String color = checkForColor(str);
		Set<String> adjectifiedColors = null;

		if (color == null && (PosTags.isAdjective(Languages.EN, posTag) || PosTags.isNoun(Languages.EN, posTag))) {
			adjectifiedColors = checkForAdjectifiedColor(str);
		}

		if (entity != null) {

			if (adjectifiedColors != null) {
				logger.info("Color Identified : " + adjectifiedColors);
				entity.setAttribute(SearchUtils.COLOR_KEY, adjectifiedColors);
				entity.setEntityStr(entity.getEntityStr().replaceAll("\\b" + str + "\\b", "").trim());
			} else if (color != null) {
				logger.info("Color Identified : " + color);
				entity.setAttribute(SearchUtils.COLOR_KEY, color);
				entity.setEntityStr(entity.getEntityStr().replaceAll("\\b" + color + "\\b", "").trim());
			}
			entity.setEntityStr(entity.getEntityStr().replaceAll("\\bcolor\\b", ""));
		}

		return color != null || adjectifiedColors != null;
	}

	/**
	 *
	 * @param indexedWord - Color String
	 * @param entity - SpEntity for which color is identified and set
	 * @return - True if color exists in the str, else false
	 */
	public static boolean spotColor(IndexedWord indexedWord, XCEntity entity) {
		String str = indexedWord.word();
		String posTag = indexedWord.tag();
		str = str.replaceAll("[^a-zA-Z\\s]", "");
		logger.info("String for color search : " + str);
		return checkForColor(str, posTag, entity);

	}

	/**
	 * 
	 * @param str - String containing color, if it is a noun or adjective phrase
	 * @return - Set of color identified
	 */
	private static Set<String> checkForAdjectifiedColor(String str) {
		List<String> possibleColorList = JWIController.getSynonyms(str, POS.ADJECTIVE);
		Set<String> adjectifiedColors = null;
		if (possibleColorList != null) {
			for (String possibleColor : possibleColorList) {
				String color = checkForColor(possibleColor);
				if (color != null) {
					if (adjectifiedColors == null) {
						adjectifiedColors = new HashSet<String>();
					}
					adjectifiedColors.add(color);
				}
			}
		}
		return adjectifiedColors;
	}

	/**
	 * 
	 * @param str - String which may contain color (Direct check from color map)
	 * @return - Color found from the str
	 */
	public static String checkForColor(String str) {

		str = str.toLowerCase().trim();
		if (ColorSimilarity.COLOR_OBJECT_MAP.containsKey(str)) {
			return str;
		}

		String color = null;
		String color1 = null;
		boolean flag = false;

		for (String tempColor : ColorSimilarity.COLOR_OBJECT_MAP.keySet()) {
			String commonWords = getCommonWords(tempColor, str);
			if (commonWords.trim().length() == tempColor.trim().length()) {
				flag = true;
				color = tempColor;
				if (color1 == null)
					color1 = color;
			}
		}
		if (flag) {
			if (color.length() > color1.length()) {
				return color;
			} else {
				return color1;
			}
		} else {
			return null;
		}
	}

	/**
	 * Method to resolve cases like blue and Navy blue
	 * @param query - Color from the map to be matched against passed string
	 * @param line - Passed String
	 * @return
	 */
	private static String getCommonWords(String query, String line) {
		if (query != null && line != null) {

			query = query.toLowerCase().replaceAll("\"", "");
			line = line.toLowerCase().replaceAll("\"", "");

			String[] arr1 = query.split("[\\s]");
			String[] arr2 = line.split("[\\s]");
			Set<String> commonWords = new HashSet<String>();
			for (int i = 0; i < arr1.length; i++) {
				for (int j = 0; j < arr2.length; j++) {
					if (arr1[i].equalsIgnoreCase(arr2[j])) {
						commonWords.add(arr1[i]);
					}
				}
			}

			StringBuilder sb = new StringBuilder();
			for (String item : commonWords) {
				sb.append(item + " ");
			}
			return sb.toString();
		}
		return null;
	}
}
