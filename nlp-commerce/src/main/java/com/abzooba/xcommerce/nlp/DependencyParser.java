package com.abzooba.xcommerce.nlp;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import com.abzooba.xcommerce.core.XCQuery;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.GrammaticalRelation;

/**
 * Controls the dependency parsing used depending on the domain/customer.
 * Uses a forwarding architecture.
 * 
 * @author Alix Melchy Aug 31, 2016
 * @see com.abzooba.xcommerce.nlp.DependencyParse
 *
 */
public class DependencyParser implements DependencyParse {

	private DependencyParse domainSpecificDependencyParse;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public DependencyParser(String domain, XCQuery query, SemanticGraph dependency_graph) {
		try {
			Class c = Class.forName(getParserName(domain));
			Constructor constructor = c.getConstructor(XCQuery.class, SemanticGraph.class);
			this.domainSpecificDependencyParse = (DependencyParse) constructor.newInstance(query, dependency_graph);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private static String getParserName(String domain) {
		if (domain != null && domain.length() > 0)
			/*TODO: Remove this hard coded value */
			return "com.abzooba.xcommerce.domain." + domain.toLowerCase() + ".DependencyParse" + domain.substring(0, 3).toUpperCase() + domain.substring(3).toLowerCase();
		return "";
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#getEdgesAsStr()
	 */
	@Override
	public String getEdgesAsStr() {
		return domainSpecificDependencyParse.getEdgesAsStr();
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#getDependencyGraph()
	 */
	@Override
	public SemanticGraph getDependencyGraph() {
		return domainSpecificDependencyParse.getDependencyGraph();
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#getSortedEdges()
	 */
	@Override
	public List<SemanticGraphEdge> getSortedEdges() {
		return domainSpecificDependencyParse.getSortedEdges();
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#setSortedEdges()
	 */
	@Override
	public void setSortedEdges() {
		domainSpecificDependencyParse.setSortedEdges();

	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#getMultiWordEntityMap()
	 */
	@Override
	public Map<IndexedWord, String> getMultiWordEntityMap() {
		return domainSpecificDependencyParse.getMultiWordEntityMap();
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#getNERMap()
	 */
	@Override
	public Map<String, String> getNERMap() {
		return domainSpecificDependencyParse.getNERMap();
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#goeswith_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void goeswith_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.goeswith_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#ref_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void ref_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.ref_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#root_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void root_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.root_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#xsubj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void xsubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.xsubj_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#acomp_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void acomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.acomp_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#advcl_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void advcl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.advcl_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#discourse_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void discourse_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.discourse_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#possessive_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void possessive_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.possessive_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#number_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void number_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.number_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#predet_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void predet_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.predet_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#punct_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void punct_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.punct_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#prt_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void prt_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.prt_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#expl_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void expl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.expl_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#mwe_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void mwe_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.mwe_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#tmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void tmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.tmod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#npmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void npmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.npmod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#npadvmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void npadvmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.npadvmod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#nummod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void nummod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.nummod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#num_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void num_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.num_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#poss_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void poss_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.poss_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#case_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void case_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.case_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#acl_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void acl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.acl_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#advmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void advmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.advmod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#agent_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void agent_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.agent_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#amod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void amod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.amod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#quantmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void quantmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.quantmod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#appos_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void appos_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.appos_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#abbrev_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void abbrev_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.abbrev_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#aux_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void aux_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.aux_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#pcomp_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void pcomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.pcomp_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#csubjpass_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void csubjpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.csubjpass_fireRule(gov, v2, dep, edgeRelation);
    }

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#preconj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void preconj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.preconj_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#auxpass_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void auxpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.auxpass_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#cc_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void cc_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.cc_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#clausalSentiClassifier(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord)
	 */
	@Override
	public void clausalSentiClassifier(IndexedWord gov, IndexedWord dep) {
		domainSpecificDependencyParse.clausalSentiClassifier(gov, dep);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#ccomp_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void ccomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.ccomp_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#csubj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void csubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.csubj_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#conj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void conj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.conj_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#cop_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void cop_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.cop_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#dep_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void dep_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.dep_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#det_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void det_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.det_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#dobj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void dobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.dobj_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#iobj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void iobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.iobj_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#neg_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void neg_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.neg_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#name_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void name_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.name_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#compound_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void compound_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.compound_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#nn_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void nn_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.nn_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#relcl_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void relcl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.relcl_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#rcmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void rcmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.rcmod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#parataxis_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void parataxis_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.parataxis_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#nmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void nmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.nmod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#prep_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void prep_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.prep_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#prepc_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void prepc_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.prepc_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#pobj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void pobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.pobj_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#mark_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void mark_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.mark_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#vmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void vmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.vmod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#infmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void infmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.infmod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#partmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void partmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.partmod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#xcomp_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void xcomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.xcomp_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#prepSentiNullifier(java.lang.String)
	 */
	@Override
	public void prepSentiNullifier(String relation) {
		domainSpecificDependencyParse.prepSentiNullifier(relation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#nsubj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void nsubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.nsubj_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#nsubjpass_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void nsubjpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		domainSpecificDependencyParse.nsubjpass_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.nlp.DependencyParse#processDependencyGraph()
	 */
	@Override
	public void processDependencyGraph() {
		domainSpecificDependencyParse.processDependencyGraph();
	}

}
