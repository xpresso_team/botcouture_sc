/**
 * @author: Alix Melchy
 * Apr 12, 2016
 */
package com.abzooba.xcommerce.nlp.attribute;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCEntity;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.ontology.server.Neo4jServer;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xpresso.engine.core.XpLexMatch;

public class ThematicSearch {
	private static Logger logger;
	private static Map<String, Set<String>> themes;

	public static String SEASON_KEY = "details";
	public static String OCCASION_KEY = "details";
	public static String DEFAULT_KEY = "details";

	public static void init() {
		logger = XCLogger.getSpLogger();
		themes = new HashMap<String, Set<String>>();
		Set<String> seasons = Stream.of("spring", "summer", "fall", "autumn", "winter").collect(Collectors.toCollection(HashSet::new));
		themes.put("seasons", seasons);
		Set<String> occasions = new HashSet<String>();
		occasions.addAll(Neo4jServer.returnOccasionTerms());
		themes.put("occasions", occasions);
	}

	private static String getThemeKey(String theme) {
		switch (theme) {
			case "seasons":
				return SEASON_KEY;
			case "occasion":
				return OCCASION_KEY;
			default:
				return DEFAULT_KEY;
		}
	}

	public static String[] checkForTheme(String fullEntity) {
		for (String key : themes.keySet()) {
			String themeStr = XpLexMatch.sequenceContain(themes.get(key), fullEntity);
			if (themeStr != null) {
				String[] answ = { getThemeKey(key), themeStr };
				return answ;
			}
		}
		return null;
	}

	public static boolean spotTheme(String queryStr, XCEntity entity) {
		String[] theme = checkForTheme(queryStr.toLowerCase());
		if (theme != null) {
			String entityStr = entity.getEntityStr();
			String newEntityStr = entityStr.replaceAll("\\b" + theme[1] + "s*\\b", "").trim();
			entity.setEntityStr(newEntityStr);
			Map<String, String> recommendations = Neo4jServer.findRecommendations(theme[0], theme[1]);
			if (recommendations != null && !recommendations.isEmpty()) {
				logger.info("Found recommendations for " + theme[1] + "\t" + recommendations.toString());
				for (Entry<String, String> entry : recommendations.entrySet()) {
					if (entry.getKey().equalsIgnoreCase(Neo4jServer.GARMENT_MATERIAL_LABEL)) {
						entity.setAttribute(SearchUtils.FEATURE_KEY, entry.getValue());
						logger.info("Material recommendations: " + entry.getValue());
					} else {
						entity.setAttribute(SearchUtils.GENERIC_KEY, entry.getValue());
						logger.info("Generic recommendations: " + entry.getValue());
					}
				}
			} else {
				if (theme[0].equalsIgnoreCase(Neo4jServer.GARMENT_MATERIAL_LABEL)) {
					entity.setAttribute(SearchUtils.FEATURE_KEY, theme[1]);
					logger.info("Material recommendations: " + theme[1]);
				} else {
					entity.setAttribute(SearchUtils.GENERIC_KEY, theme[1]);
					logger.info("Generic recommendations: " + theme[1]);
				}
			}
			logger.info("Theme spotted:\t" + theme[0] + ": " + theme[1]);
			return true;
		}

		return false;
	}
}
