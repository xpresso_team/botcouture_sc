package com.abzooba.xcommerce.core;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.domain.IntentUtils;
import com.abzooba.xcommerce.nlp.DependencyParse;
import com.abzooba.xcommerce.nlp.DependencyParser;
import com.abzooba.xcommerce.nlp.attribute.GenderSearch;
import com.abzooba.xcommerce.nlp.attribute.SizeParser;
import com.abzooba.xcommerce.nlp.attribute.price.PriceParser;
import com.abzooba.xcommerce.search.SearchItem;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xpresso.engine.core.XpText;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import java.util.*;

/**
 * An instance of this class represents a query.
 * This instance holds the original query as well as its spell checked version,
 * the entities extracted from this query and the retrieved corresponding results.
 * 
 * @author Sudhanshu Kumar
 * 12-Apr-2014 10:00 AM
 * 
 */

public class XCQuery extends XpText {
	private String globalPriceStr;
	private String domainName;
	private String queryText;
	private String originalQuery;
	private String spellCorrectedQuery;
	private String clientName;
	private int pageNo;
	private int perPage;
	private List<SearchItem> simpleSearchResults;
	private List<SearchItem> structuredSearchResults;
	private Map<String, XCEntity> entityMap = null;
	private boolean hasAllCategories = false;
	private Set<String> uniqueEntitySet = null;
	private static Logger logger;
	private JSONObject intent;
	private JSONObject clarification;
	private IntentAPI intentObj;
	public boolean isChatBotQuery;
	String customReturnAttributes;
	private HashMap<CoreLabel, Double> sizeTokensWithConfidence;

	public static void init() {
		logger = XCLogger.getSpLogger();
	}

	public XCQuery(String originalQuery, String spellCorrectedQuery, String queryText, String clientName, String domainName, boolean isChatbotQuery, String customReturnAttributes, int pageNo, int perPage, IntentAPI... intentObj) {
		super(queryText);
		logger.info("In XCQuery -> XCQuery");
		this.spellCorrectedQuery = spellCorrectedQuery;
		this.queryText = queryText;
		this.pageNo = pageNo;
		this.perPage = perPage;
		this.domainName = domainName;
		this.clientName = clientName;
		this.originalQuery = originalQuery;
		this.entityMap = new HashMap<String, XCEntity>();
		this.uniqueEntitySet = new HashSet<String>();
		this.intent = new JSONObject();
		this.clarification = new JSONObject();
		this.isChatBotQuery = isChatbotQuery;
		this.customReturnAttributes = customReturnAttributes;
		if (intentObj.length > 0)
			this.intentObj = intentObj[0];
		this.parseQuery();
	}

	public XCQuery(String originalQuery, String spellCorrectedQuery, String queryText, String clientName, String domainName, boolean isChatbotQuery, String customReturnAttributes, String pageNo, String perPage, IntentAPI... intentObj) {
		this(originalQuery, spellCorrectedQuery, queryText, clientName, domainName, isChatbotQuery, customReturnAttributes, Integer.parseInt(pageNo), Integer.parseInt(perPage));
	}

	public XCQuery(String originalQuery, String spellCorrectedQuery, String queryText, String clientName, String domainName, boolean isChatbotQuery, String customReturnAttributes, IntentAPI... intentObj) {
		this(originalQuery, spellCorrectedQuery, queryText, clientName, domainName, isChatbotQuery, customReturnAttributes, 1, XCConfig.MAX_SEARCH_HITS, intentObj);
	}

	public IntentAPI getIntentObject() {
		return this.intentObj;
	}

	/**
	 * @return the set of unique entities
	 */
	public Set<String> getUniqueEntitySet() {
		return uniqueEntitySet;
	}

	/**
	 * @param uniqueEntitySet the set of unique entities to set
	 */
	public void setUniqueEntitySet(Set<String> uniqueEntitySet) {
		this.uniqueEntitySet = uniqueEntitySet;
	}

	/**
	 * @return the globalPriceStr
	 */
	public String getGlobalPriceStr() {
		return globalPriceStr;
	}

	/**
	 * @param globalPriceStr the globalPriceStr to set
	 */
	public void setGlobalPriceStr(String globalPriceStr) {
		this.globalPriceStr = globalPriceStr;
	}

	/**
	 * @return the normalized string of this query
	 */
	public String getQueryString() {
		return this.normalized_text;
	}

	public String getOriginalQuery() {
		return this.originalQuery;
	}

	/**
	 * @return the number of result pages to display
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * Sets the number of pages of results to display
	 * @param pageNo
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * @return the number of items per result page
	 */
	public int getPerPage() {
		return perPage;
	}

	/**
	 * Sets the number of items per result page
	 * @param perPage
	 */
	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}

	public String getClientName() {
		if (this.clientName.equals("") || this.clientName.equalsIgnoreCase("all") || this.clientName == null || this.clientName.isEmpty())
			this.clientName = "";
		return this.clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/**
	 * @return the domain (customer) name
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * Sets the domain (customer) name for this query
	 * @param domainName
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * This query text is the lemmatized version of the original query.
	 * @return this query text
	 */
	public String getQueryText() {
		return queryText;
	}

	/**
	 * Sets this query text, which is a processed version of the normalized query string.
	 * So far, holds the lemmatized version of the original query.
	 * @param queryText
	 */
	public void setQueryText(String queryText) {
		this.queryText = queryText;
	}

	/**
	 * @return spell corrected version of this query text
	 */
	public String getSpellCorrectedQuery() {
		return spellCorrectedQuery;
	}

	/**
	 * Sets this spell corrected version of this query text
	 * @param spellCorrectedQuery
	 */
	public void setSpellCorrectedQuery(String spellCorrectedQuery) {
		this.spellCorrectedQuery = spellCorrectedQuery;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @param intent
	 */
	public void setIntent(JSONObject intent) {
		this.intent = intent;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @return intent
	 */
	public JSONObject getIntent() {
		return intent;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @param clarification
	 */
	public void setClarification(JSONObject clarification) {
		this.clarification = clarification;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @return intent
	 */
	public JSONObject getClarification() {
		return clarification;
	}

	/**
	 * This entity map associates the identified entity string with its {@link com.abzooba.xcommerce.core.XCEntity#XCEntity(String, XCQuery, boolean)}
	 * @return entity map
	 */
	public Map<String, XCEntity> getEntityMap() {
		return entityMap;
	}

	/**
	 * Adds this spEntity with key entity to this entity map.
	 * This entity map associates the identified entity string with its {@link com.abzooba.xcommerce.core.XCEntity#XCEntity(String, XCQuery, boolean)}
	 * @param entity
	 * @param spEntity
	 */
	public void addToEntityMap(String entity, XCEntity spEntity) {
		this.entityMap.put(entity, spEntity);
	}

	/**
	 * Auxilliary method for com.abzooba.synaptica.core.SpQuery.removeDuplicatesFromMultiWordEntity()
	 * @return
	 */
	private Set<String> identifyDuplicatesFromMultiWordEntity() {
		Set<String> duplicates = new HashSet<String>();
		for (Map.Entry<String, XCEntity> entry : this.entityMap.entrySet()) {
			String multiWordEntity = entry.getValue().getMultiWordEntity();
			if (multiWordEntity != null) {
				for (String key : this.entityMap.keySet()) {
					/* "kurta pajama" */
					if (multiWordEntity.split(" ").length > 2) {
						logger.info("multiWordEntity length > 2 case:" + multiWordEntity);
						multiWordEntity = multiWordEntity.substring(multiWordEntity.lastIndexOf(' '));
					}
					if (!key.equals(entry.getKey()) && !multiWordEntity.equalsIgnoreCase(key) && multiWordEntity.contains(key)) {
						duplicates.add(key);
						entry.getValue().addAttribute(this.entityMap.get(key));
					}
				}
			}
		}

		return duplicates;
	}

	/**
	 * Removes entities the key of which is a part of a multiWordEntity of another entity
	 */
	private void removeDuplicatesFromMultiWordEntity() {
		Set<String> duplicates = identifyDuplicatesFromMultiWordEntity();
		for (String dup : duplicates)
			removeFromEntityMap(dup);
	}

	/**
	 * Removes the entry with key entity, or with a key containing entity, from the entity map.
	 * This entity map associates the identified entity string with its {@link com.abzooba.xcommerce.core.XCEntity#XCEntity(String, XCQuery, boolean)}
	 * @param entity
	 */
	public void removeFromEntityMap(String entity) {
		if (this.entityMap.containsKey(entity)) {
			logger.info("Removing entity with key: " + entity);
			this.entityMap.remove(entity);
		} else {
			for (String key : this.entityMap.keySet()) {
				if (key.contains(entity)) {
					logger.info("Removing entity with key: " + key + " containing entity string: " + entity);
					this.entityMap.remove(key);
					break;
				}
			}
			logger.info("No entity found for key: " + entity);
		}
	}

	/**
	 * Returns the instance of SpEntity associated with entityStr or with a key containing entityStr in this entity map.
	 * This entity map associates the identified entity string with its {@link com.abzooba.xcommerce.core.XCEntity#XCEntity(String, XCQuery, boolean)}
	 * @param entityStr
	 * @return spEntity corresponding to entityStr
	 */
	public XCEntity getEntity(String entityStr) {
		logger.info("In XCQuery -> getEntity");
		XCEntity returnEntity = this.entityMap.get(entityStr);
		if (returnEntity == null) {
			for (Map.Entry<String, XCEntity> entry : this.entityMap.entrySet()) {
				String currEntity = entry.getKey();
				if (currEntity.contains(entityStr)) {
					returnEntity = entry.getValue();
					break;
				}
			}
		}
		return returnEntity;
	}

	/**
	 * Adds these result lists to the respective structured and simple search result lists.
	 * @param currStructuredSearchResults
	 * @param currSimpleSearchResults
	 */
	public void addToResultItems(List<SearchItem> currStructuredSearchResults, List<SearchItem> currSimpleSearchResults) {
		if (currStructuredSearchResults != null) {
			if (this.structuredSearchResults == null) {
				this.structuredSearchResults = new ArrayList<SearchItem>();
			}
			this.structuredSearchResults.addAll(currStructuredSearchResults);
		}
		if (currSimpleSearchResults != null) {
			if (this.simpleSearchResults == null) {
				this.simpleSearchResults = new ArrayList<SearchItem>();
			}
			this.simpleSearchResults.addAll(currSimpleSearchResults);
		}
	}

	/**
	 * Adds this list of results to the structured search result
	 * @param currStructuredSearchResults
	 */
	public void addToResultItems(List<SearchItem> currStructuredSearchResults) {
		if (currStructuredSearchResults != null) {
			if (this.structuredSearchResults == null) {
				this.structuredSearchResults = new ArrayList<SearchItem>();
			}
			this.structuredSearchResults.addAll(currStructuredSearchResults);
		}
	}

	/**
	 * @return string representation of JSON of retrieved results for this query
	 */
	public String getSearchJSON() {
		JSONObject jsonObj = null;
		jsonObj = this.getSearchResults();
		try {
			if (jsonObj != null)
				return jsonObj.toString(2);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private JSONObject getSearchResults() {
		logger.info("Searching items containing " + "\t" + this.normalized_text);

		List<SearchItem> resultItems = null;
		if (this.structuredSearchResults != null) {
			logger.info("Final Results contain Structured Search Results " + this.structuredSearchResults.size());
			resultItems = this.structuredSearchResults;
		} else {
			System.out.println("Final Results contain Simple Search Results");
			resultItems = this.simpleSearchResults;
		}
		return XCJSON.processResultsToJSON(this.originalQuery, this.spellCorrectedQuery, this.queryText, this.domainName, resultItems, pageNo, perPage, this.intent, this.clarification);
	}

	private void parseQuery() {
		logger.info("In XCQuery -> parseQuery");
		List<CoreMap> sentencesList = this.getSentences();
		for (CoreMap sentence : sentencesList) {
			SemanticGraph dependencyGraph = this.getDependencyGraph(sentence);
			Tree parseTree = this.getParseTree(sentence);
			List<CoreLabel> tokens = this.getTokens(sentence);
			String qpStr = PriceParser.priceDetectionModuleExhaustive(parseTree, dependencyGraph, sentence.toString());
			logger.info("Price Details for Sentence : " + qpStr);
			this.setGlobalPriceStr(qpStr);

			HashMap<CoreLabel, Double> sizeMap = SizeParser.sizeDetectionModule(parseTree, dependencyGraph, sentence.toString());
			if (sizeMap.size() > 0) {
				this.setSizeTokensWithConfidence(sizeMap);
			}

			DependencyParse dp = new DependencyParser(domainName, this, dependencyGraph);
			dp.processDependencyGraph();
		}

		removeDuplicatesFromMultiWordEntity();

		for (XCEntity entity : this.entityMap.values()) {
			logger.info("Processing entity " + entity.getEntityStr());
			// To cater unisex products, we pass unisex as gender apart from user asked gender
			if (entity.getAttribute(SearchUtils.GENDER_KEY) != null) {
				entity.setAttribute(SearchUtils.GENDER_KEY, GenderSearch.UNISEX_TAG);
			}
			entity.processEntity(this);
			IntentUtils.processIntent(domainName, entity, this);
		}
	}

	/**
	 * Specifies if set to true that this query is over all categories in the catalog.
	 * E.g. 'I want something in blue': no specific entities is given, hence the search over all categories.
	 * @param hasAllCategories
	 */
	public void setHasAllCategories(boolean hasAllCategories) {
		this.hasAllCategories = hasAllCategories;
	}

	/**
	 * Specifies if set to true that this query is over all categories in the catalog.
	 * E.g. 'I want something in blue': no specific entities is given, hence the search over all categories.
	 * @return whether this query spans all categories
	 */
	public boolean getHasAllCategories() {
		return this.hasAllCategories;
	}

	public HashMap<CoreLabel, Double> getSizeTokensWithConfidence() {
		return sizeTokensWithConfidence;
	}

	public void setSizeTokensWithConfidence(HashMap<CoreLabel, Double> sizeTokensWithConfidence) {
		this.sizeTokensWithConfidence = sizeTokensWithConfidence;
	}
}
