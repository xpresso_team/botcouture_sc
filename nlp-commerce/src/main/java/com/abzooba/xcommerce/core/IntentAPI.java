package com.abzooba.xcommerce.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.domain.xcfashion.LoadXCHierarchy;
import com.abzooba.xcommerce.domain.xcfashion.XCHierarchy;
import com.abzooba.xcommerce.nlp.attribute.GenderSearch;
import com.abzooba.xcommerce.nlp.attribute.SearchByProductName;
import com.abzooba.xcommerce.ontology.server.Neo4jServer;
import com.abzooba.xcommerce.search.EsHttpSearch;
import com.abzooba.xcommerce.search.SearchController;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xcommerce.search.SemanticSearch;
import com.google.common.collect.Multimap;

import edu.stanford.nlp.ling.IndexedWord;

/**
 * @author Sudhanshu Kumar
 *
 * IntentAPI.java
 * 12:10PM 02 Nov, 2016
 */
public class IntentAPI {

	public static Logger logger;
	private Map<String, Set<String>> filterMap;
	private String entity;
	private Set<String> subCategories;
	public JSONArray intentJsonSet;
	private String query;

	private Map<String, ArrayList<Integer>> indexMap;

	public IntentAPI() {
		// TODO Auto-generated constructor stub
	}

	public static void init() {
		logger = XCLogger.getSpLogger();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		XCEngine.init();
		IntentAPI objIntentAPI = new IntentAPI();

		Scanner s = new Scanner(System.in);
		String line = null;
		Map<String, String[]> queryParameters = null;
		JSONObject intentJson = null;

		while (true) {
			logger.info("Enter the query");
			line = s.nextLine();
			if (line != null && line.trim() != null) {
				objIntentAPI = new IntentAPI();
				queryParameters = new HashMap<String, String[]>();
				queryParameters.put("query", new String[] { line.trim() });
				queryParameters.put("src", new String[] { "all" });

				intentJson = objIntentAPI.intentAPIForChatBot(queryParameters);
				logger.info(intentJson.toString());
			} else {
				s.close();
				break;
			}
		}

		//		Map<String, String[]> chatBotQuery = new HashMap<String, String[]>();
		//		chatBotQuery.put("entity", new String[] { "saree" });
		//		chatBotQuery.put("client_name", new String[] { "" });
		//		chatBotQuery.put("colorsavailable", new String[] { "" });
		//		chatBotQuery.put("subcategory", new String[] { "" });
		//		chatBotQuery.put("xc_category", new String[] { "" });
		//		chatBotQuery.put("xc_hierarchy_str", new String[] { "sarees" });
		//		chatBotQuery.put("details", new String[] { "" });
		//		chatBotQuery.put("gender", new String[] { "unisex", "women" });
		//		chatBotQuery.put("price", new String[] { "" });
		//		chatBotQuery.put("brand", new String[] { "" });
		//		chatBotQuery.put("size", new String[] { "" });
		//		objIntentAPI.structuredSearch(chatBotQuery);

	}

	public JSONObject structuredSearch(Map<String, String[]> chatBotQuery, String... customReturnAttributes) {
		//		chatBotQuery.put(SearchUtils.DOMAIN_KEY, new String[] { XCConfig.DEFAULT_DATA_DOMAIN });
		//		chatBotQuery.put(SearchUtils.PRODUCT_NAME_KEY, chatBotQuery.get(SearchUtils.ENTITY_KEY));
		SearchController objSearchController = new SearchController();
		JSONObject finalResult = null;
		finalResult = objSearchController.startSearch(chatBotQuery);
		try {
			logger.info(finalResult.getJSONArray("Results").length() + "");
			//			logger.info(finalResult.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return finalResult;
	}

	/*
	Semantic updating function for intentJson returned by Intent API
	 */
	SemanticSearch semanticJsonupdate = new SemanticSearch();

	public JSONObject semanticIntentAPIForChatBot(Map<String, String[]> query) {
		JSONObject intentJson = intentAPIForChatBot(query);
		try {
			return semanticJsonupdate.SemanticJsonUpdater(intentJson);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return intentJson;
	}

	/*
	 * Intent API
	 * */
	public JSONObject intentAPIForChatBot(Map<String, String[]> query) {
		//Resetting the corresponding lists for secret queries
		if (query.get("query")[0].startsWith(XCConfig.RESET_PREFIX)) {
			long startTime = System.nanoTime();
			if (query.get("query")[0].equals(XCConfig.RESET_PREFIX + XCConfig.RESET_BRAND)) {
				logger.info("Reloading the brandList");
				logger.info("Length of old brandList is " + SearchByProductName.brandList.size());
				SearchByProductName.brandList.removeAll(SearchByProductName.brandListFromNeo4j);
				SearchByProductName.brandListFromNeo4j.clear();
				SearchByProductName.brandListFromNeo4j.addAll(Neo4jServer.returnBrandNames());
				SearchByProductName.brandList.addAll(SearchByProductName.brandListFromNeo4j);
				logger.info("Length of new brandList is " + SearchByProductName.brandList.size());
				logger.info("Time taken to reset the brandList is " + (float) (System.nanoTime() - startTime) / 1000000000);
			}

			if (query.get("query")[0].equals(XCConfig.RESET_PREFIX + XCConfig.RESET_GENDER)) {
				logger.info("Reloading the implicit genders");
				logger.info("Size of old IMPLICIT_WOMEN_ITEMS " + GenderSearch.IMPLICIT_WOMEN_ITEMS.size());
				logger.info("Size of old IMPLICIT_MEN_ITEMS " + GenderSearch.IMPLICIT_MEN_ITEMS.size());
				logger.info("Size of old NOT_ALLOWED_LEVENSHTEIN " + GenderSearch.NOT_ALLOWED_LEVENSHTEIN.size());
				GenderSearch.IMPLICIT_MEN_ITEMS.clear();
				GenderSearch.IMPLICIT_WOMEN_ITEMS.clear();
				GenderSearch.NOT_ALLOWED_LEVENSHTEIN.clear();
				GenderSearch.IMPLICIT_MEN_ITEMS.addAll(Neo4jServer.returnImplicitMen());
				GenderSearch.IMPLICIT_WOMEN_ITEMS.addAll(Neo4jServer.returnImplicitWomen());
				GenderSearch.NOT_ALLOWED_LEVENSHTEIN.addAll(Neo4jServer.notAllowedLevensthein());
				logger.info("Size of new IMPLICIT_WOMEN_ITEMS " + GenderSearch.IMPLICIT_WOMEN_ITEMS.size());
				logger.info("Size of new IMPLICIT_MEN_ITEMS " + GenderSearch.IMPLICIT_MEN_ITEMS.size());
				logger.info("Size of new NOT_ALLOWED_LEVENSHTEIN " + GenderSearch.NOT_ALLOWED_LEVENSHTEIN.size());
				logger.info("Time taken to reset the implicit genders is " + (float) (System.nanoTime() - startTime) / 1000000000);
			}

			if (query.get("query")[0].equals(XCConfig.RESET_PREFIX + XCConfig.RESET_ES)) {
				logger.info("Clearing the es cache");
				EsHttpSearch.esCacheMap.clear();
				logger.info("Time taken to clear the es cache is " + (float) (System.nanoTime() - startTime) / 1000000000);
			}
		}
		XCEngine.getSearchResultsJSON(query, this);
		JSONObject intentJson = new JSONObject();
		try {
			if (this.intentJsonSet != null) {
				logger.info(intentJsonSet.toString());
				/* "show me some sports shoes for about 60 dollars"
				 * "show me red shirt for about 60 dollars".
				 *  They form two intent, one with correct entity, another with only price filter*/
				boolean intentJsonSelected = false;
				if (this.intentJsonSet.length() >= 2) {
					// && this.intentJsonSet.getJSONObject(this.intentJsonSet.length() - 1).get("entity").toString().trim().split(" ").length >= 2
					for (int i = 0; i < this.intentJsonSet.length(); i++) {
						if (this.intentJsonSet.getJSONObject(i).get("entity").toString().trim().split(" ").length >= 2) {
							intentJson = this.intentJsonSet.getJSONObject(i);
							intentJsonSelected = true;
							break;
						}
					}
				}

				if (!intentJsonSelected) {
					if (this.intentJsonSet.length() >= 2 && this.intentJsonSet.getJSONObject(0).get("entity").equals("anything") && !this.intentJsonSet.getJSONObject(1).get("entity").equals("anything")) {
						logger.info(this.intentJsonSet.getJSONObject(0).toString());
						logger.info(this.intentJsonSet.getJSONObject(1).toString());
						intentJson = this.intentJsonSet.getJSONObject(1);
					} else {
						intentJson = this.intentJsonSet.getJSONObject(0);
					}
				}
			} else {
				/*cases like any, any brand is parsed in this format from chatbot*/
				intentJson = new JSONObject("{\"entity\":\"anything\",\"price\":[],\"size\":[],\"subcategory\":[\"anything\"],\"features\":[],\"brand\":[],\"details\":[],\"colorsavailable\":[],\"gender\":[]}");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Comment out the below Semantic updation part to use semanticintentjsonforchatbot function above instead of intentJsonforchatbot function
		SemanticSearch semanticJson = new SemanticSearch();
		try {
			intentJson = semanticJson.SemanticJsonUpdaterBrand(intentJson);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return intentJson;
	}

	public void prepareIntentJson(XCEntity spEntity) {
		this.query = spEntity.getQuery().getQueryText();
		this.entity = spEntity.getEntityStr();
		this.filterMap = spEntity.getAttributeMap();
		this.subCategories = spEntity.getMainEntityList();
		if (this.intentJsonSet == null) {
			this.intentJsonSet = new JSONArray();
		}
		JSONObject intentJson = new JSONObject();

		try {
			intentJson.put("entity", this.entity);
			if (this.filterMap != null) {
				Set<String> details = this.filterMap.get(SearchUtils.GENERIC_KEY);
				Set<String> detailsNew = new HashSet<>();
				Set<String> detailsNewRepeat = new HashSet<>();
				String[] entityArray = intentJson.get("entity").toString().split(" ");
				if (details != null && !details.isEmpty()) {

					for (String detailWord : details) {
						String tempDetail = "";
						for (String temp : detailWord.split(" ")) {
							if (!tempDetail.contains(temp) && (!details.contains(temp) || detailWord.equalsIgnoreCase(temp))) {
								tempDetail = tempDetail + " " + temp;
							}
						}
						detailsNewRepeat.add(tempDetail.trim());
					}

					for (String detailWord : detailsNewRepeat) {
						detailWord = detailWord.trim();
						if (!intentJson.get("entity").toString().contains(detailWord)) {
							for (String entityWord : entityArray) {
								detailWord = detailWord.replace(entityWord, "");
							}
							detailsNew.add(detailWord.trim());
						}
					}

				}
				if (this.filterMap.get(SearchUtils.BRAND_KEY) != null) {
					for (String tempBrand : this.filterMap.get(SearchUtils.BRAND_KEY)) {
						String[] tempBrandWords = tempBrand.split(" ");
						for (String tempBrandWord : tempBrandWords) {
							detailsNew.remove(tempBrandWord);
						}
					}
				}
				intentJson.put("price", new JSONArray(this.filterMap.get(SearchUtils.PRICE_NUMERIC_KEY)));
				intentJson.put("size", new JSONArray(this.filterMap.get(SearchUtils.SIZE)));
				intentJson.put("xc_category", new JSONArray(this.filterMap.get(SearchUtils.XC_CATEGORY_KEY)));
				intentJson.put("features", new JSONArray(this.filterMap.get(SearchUtils.FEATURE_KEY)));
				intentJson.put("brand", new JSONArray(this.filterMap.get(SearchUtils.BRAND_KEY)));
				intentJson.put("details", new JSONArray(detailsNew));
				intentJson.put("colorsavailable", new JSONArray(this.filterMap.get(SearchUtils.COLOR_KEY)));
				/*Set implicit gender of items like skirt, etc*/
				if (this.filterMap.get(SearchUtils.GENDER_KEY) == null) {
					Set<String> gender = GenderSearch.getImplicitGender(this.entity);
					intentJson.put("gender", new JSONArray(gender));
				} else {
					intentJson.put("gender", new JSONArray(this.filterMap.get(SearchUtils.GENDER_KEY)));
				}
				List<String> xcHierarchies = getXCHierarchy(intentJson.get("gender"), this.filterMap.get(SearchUtils.XC_CATEGORY_KEY));
				intentJson.put("xc_hierarchy_str", new JSONArray(xcHierarchies));
			} else {
				/*cases like hello.show me some shirts overwrites correct intent with wrong ones*/
				return;
			}
			logger.info("Current Intent : " + intentJson.toString());
			this.intentJsonSet.put(intentJson);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<String> getXCHierarchy(Object gender, Set<String> xc_category) throws JSONException {
		Set<String> xcHierarchies = new HashSet<>();
		List<String> xcHierarchiesArray = new ArrayList();
		String h1_level = null;
		List<String> genders = new ArrayList<>();
		if (gender != null) {
			JSONArray x = new JSONArray();
			if (!gender.toString().contains("[") && !gender.toString().contains("]")) {
				x = new JSONArray("[" + gender.toString() + "]");
			} else {
				x = (JSONArray) gender;
			}
			if (x != null) {
				for (int i = 0; i < x.length(); i++) {
					genders.add(x.getString(i));
				}
			}
			if (genders.contains("men") && !genders.contains("women"))
				h1_level = "men";
			else if (!genders.contains("men") && genders.contains("women"))
				h1_level = "women";
		}
		if (xc_category != null && xc_category.size() > 0) {
			for (String category : xc_category) {
				if (category != null) {
					Collection<String> nlpMappings = getCategoryFromNLPMapping(category);
					for (String nlpMapping : nlpMappings) {
						String h2_level = null;
						String h3_level = null;
						if (nlpMapping.contains("|")) {
							h2_level = nlpMapping.split("\\|")[0].toLowerCase();
							h3_level = nlpMapping.split("\\|")[1].toLowerCase();
						} else
							h2_level = nlpMapping.toLowerCase();
						try {
							XCHierarchy xcHierarchy = new XCHierarchy(h1_level, h2_level, h3_level, null);

							List<XCHierarchy> xcHierarchyList = LoadXCHierarchy.getXCHierarchyList();

							for (XCHierarchy xcHierarchy1 : xcHierarchyList) {
								String matchXCHierarchy;
								if (xcHierarchy1.equals(xcHierarchy)) {
									matchXCHierarchy = xcHierarchy1.getH2_level();
									if (this.entity != null) {
										if ((xcHierarchy.getH3_level() == null) && (xcHierarchy1.getH3_level() != null) && ((xcHierarchy1.getH3_level().equals(this.entity)) || (xcHierarchy1.getH3_level_lemma().equals(this.entity)))) {
											matchXCHierarchy = matchXCHierarchy + "|" + xcHierarchy1.getH3_level();
										} else if (xcHierarchy1.getH4_level() != null && ((xcHierarchy1.getH4_level().contains(this.entity)) || xcHierarchy1.getH4_level_lemmas().contains(this.entity))) {
											if (xcHierarchy1.getH4_level_lemmas().contains(this.entity))
												matchXCHierarchy = matchXCHierarchy + "|" + xcHierarchy1.getH3_level() + "|" + xcHierarchy1.getH4_level().get(xcHierarchy1.getH4_level_lemmas().indexOf(this.entity));
											else
												matchXCHierarchy = matchXCHierarchy + "|" + xcHierarchy1.getH3_level() + "|" + this.entity;
										} else {
											if (xcHierarchy.getH3_level() != null)
												matchXCHierarchy = matchXCHierarchy + "|" + xcHierarchy.getH3_level();
										}
									}
									xcHierarchies.add(matchXCHierarchy);
								}
							}
						} catch (Exception e) {
							logger.error("Error in fetching xc_hierarchy" + e.getMessage());
						}
					}
				}
			}

			xcHierarchiesArray.addAll(xcHierarchies);

			if (null == checkIfQueryHasSpecialClass()) {
				List<String> specialClasses = LoadXCHierarchy.getSpecialClass();
				filterXCHierarchies(xcHierarchiesArray, specialClasses);
			} else {
				Set<String> matchClass = checkIfQueryHasSpecialClass();
				List<String> specialClasses = LoadXCHierarchy.getSpecialClass();
				List<String> modifiedClasses = new ArrayList<>();
				modifiedClasses.addAll(specialClasses);
				modifiedClasses.removeAll(matchClass);
				filterXCHierarchies(xcHierarchiesArray, modifiedClasses);
			}

			List<String> listOfOverlapHierarchies = new ArrayList<>();
			for (int i = 0; i < xcHierarchiesArray.size(); i++) {
				for (int j = 0; j < xcHierarchiesArray.size(); j++) {
					if (i != j) {
						if (xcHierarchiesArray.get(j).startsWith(xcHierarchiesArray.get(i)))
							listOfOverlapHierarchies.add(xcHierarchiesArray.get(i));
					}
				}
			}
			xcHierarchiesArray.removeAll(listOfOverlapHierarchies);
		}
		return xcHierarchiesArray;
	}

	private void filterXCHierarchies(List<String> xcHierarchies, List<String> specialClasses) {
		Iterator<String> iterator = xcHierarchies.iterator();
		while (iterator.hasNext()) {
			String xchierachy = iterator.next();
			for (String specialClass : specialClasses) {
				if (xchierachy.contains(specialClass)) {
					iterator.remove();
					break;
				}
			}
		}
	}

	private Set<String> checkIfQueryHasSpecialClass() {
		List<String> specialClasses = LoadXCHierarchy.getSpecialClass();
		Set<String> matchSpecialClasses = new HashSet<>();
		for (String specialClass : specialClasses) {
			if (this.query != null && this.query.contains(specialClass))
				matchSpecialClasses.add(specialClass);
		}
		return matchSpecialClasses;
	}

	private Collection<String> getCategoryFromNLPMapping(String category) {
		Collection<String> nlpMappings = new ArrayList<>();
		Multimap<String, String> nlpMapping = LoadXCHierarchy.getNlpMapping();
		if (nlpMapping != null && nlpMapping.size() > 0) {
			if (nlpMapping.containsKey(category.toLowerCase())) {
				nlpMappings = nlpMapping.get(category);
			}
			return nlpMappings;
		}
		nlpMappings.add(category);
		return nlpMappings;
	}

	public void setIndexMap(String entity, String attributeName, IndexedWord attribute) {
		JSONArray array = new JSONArray();
		JSONObject entitykeys = new JSONObject();
		JSONObject attributekeys = new JSONObject();
		JSONObject attributeValues = new JSONObject();

		try {
			attributeValues.put("start", attribute.beginPosition());
			attributeValues.put("end", attribute.endPosition());
			attributeValues.put("value", attribute.word());
			attributekeys.put(attributeName, attributeValues);
			entitykeys.put(entity, attributekeys);

			logger.info("============= " + entitykeys.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}
}
