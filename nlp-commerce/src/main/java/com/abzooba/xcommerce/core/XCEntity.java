package com.abzooba.xcommerce.core;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.domain.AttributeUtil;
import com.abzooba.xcommerce.nlp.attribute.SearchByProductName;
import com.abzooba.xcommerce.nlp.domain.DomainCategoryMapper;
import com.abzooba.xcommerce.nlp.domain.DomainKnowledgeBase;
import com.abzooba.xcommerce.search.EsHttpSearch;
import com.abzooba.xcommerce.search.SearchController;
import com.abzooba.xcommerce.search.SearchItem;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xcommerce.utils.DetailsParser;
import com.abzooba.xpresso.aspect.domainontology.OntologyUtils;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.utils.FileIO;
import edu.stanford.nlp.ling.CoreLabel;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import java.util.*;

/**
 * Each instance of this class represents a single entity (i.e. item in the query) with its attribute.
 * This instance also holds the retrieved items (from catalog) corresponding to its entity.
 *
 * @author Alix Melchy Sep 1, 2016
 *
 */
public class XCEntity {
	private static Logger logger = XCLogger.getSpLogger();
	private Set<String> mainEntityList;
	private Set<String> topEntityList;
	private String entityStr;
	private String relaxedEntityStr;
	private String domain;

	private String govWord;
	private String multiWordEntity;
	private List<String> wordVectorCategories = null;

	private JSONObject intent;
	private JSONObject clarification;
	private JSONArray consumerSubCategory;

	private List<SearchItem> simpleSearchResults;
	private List<SearchItem> structuredSearchResults;

	private boolean directSearchOnly = false;

	private Map<String, Double> skuConfidenceMap = new HashMap<String, Double>();
	private Map<String, Set<String>> attributeMap = new LinkedHashMap<String, Set<String>>();
	public Map<String, Set<String>> relaxationMap = new LinkedHashMap<String, Set<String>>();

	private XCQuery query;

	/**
	 * Constructor: creates an instance of this class with the entity string and the query
	 * the entity is extracted from. The boolean argument controls the search for semantically
	 * similar entities (no such search when set to true).
	 * @param entityStr
	 * @param query
	 * @param isChildEntity
	 */
	public XCEntity(String entityStr, XCQuery query, boolean isChildEntity) {
		this.entityStr = entityStr.toLowerCase();
		this.query = query;
		this.intent = new JSONObject();
		this.clarification = new JSONObject();
		this.simpleSearchResults = new ArrayList<SearchItem>();
		this.structuredSearchResults = new ArrayList<SearchItem>();
		/*If not childEntity, then find semantically similar entities*/
		if (!isChildEntity)
			this.getSemanticSimilarEntities();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Entity: " + entityStr);
		sb.append(" - govWord: " + govWord);
		sb.append(" - multiWordEntity: " + multiWordEntity);
		sb.append(" - mainEntityList: " + (mainEntityList == null ? "null" : mainEntityList.toString()));
		sb.append(" - topEntityList: ");
		sb.append((this.topEntityList == null) ? "null" : topEntityList.toString());
		return sb.toString();
	}

	/**
	 * Processes this entity: checks whether it is an acceptable one, extracts global parameters,
	 * and cleans this entity by searching for product name and cleaning its string representation.
	 * @param spQuery
	 *
	 */
	public void processEntity(XCQuery spQuery) {
		String nonLemmaEntityStr = this.entityStr;
		if (!this.entityStr.contains("t-shirt"))
			this.entityStr = CoreNLPController.lemmatizeToString(this.entityStr);
		this.entityStr = this.entityStr.replaceAll("[\\s]+", " ").toLowerCase();
		this.entityStr = this.entityStr.trim();
		SearchByProductName.searchByProductName(this);
		if (spQuery.getSizeTokensWithConfidence() != null && spQuery.getSizeTokensWithConfidence().size() > 0) {
			CoreLabel sizeParam = spQuery.getSizeTokensWithConfidence().keySet().iterator().next();
			this.setAttribute(SearchUtils.SIZE, sizeParam.word());
		}
		logger.info("entityStr after brand search: " + this.entityStr);
		this.setAttribute(SearchUtils.CLIENT_NAME, spQuery.getClientName());

		if (this.getAttribute(SearchUtils.SIZE) != null && this.getAttribute(SearchUtils.GENERIC_KEY) != null) {
			Set<String> detailKeys = this.getAttribute(SearchUtils.GENERIC_KEY);
			Set<String> sizeKeys = this.getAttribute(SearchUtils.SIZE);
			Set<String> overlappingKeys = new HashSet<>();
			for (String detailKey : detailKeys) {
				for (String sizeKey : sizeKeys) {
					List<String> detailKeySplit = Arrays.asList(detailKey.toLowerCase().split(" "));
					if (detailKey.contains(sizeKey) || detailKeySplit.contains("size"))
						overlappingKeys.add(detailKey);
				}
			}
			for (String overlapKey : overlappingKeys)
				this.getAttribute(SearchUtils.GENERIC_KEY).remove(overlapKey);
		}
		String price = this.query.getGlobalPriceStr();
		if (price != null) {
			AttributeUtil.disambiguateNumericAttributesAndPrice(this.getQuery().getDomainName(), this, price);
		}

		/* TODO: removing gender, brand or color keys from generic. eg: "men shirt" captures men in generic as well */
		if (this.getAttribute(SearchUtils.GENERIC_KEY) != null) {
			//			logger.info("=================== " + this.getAttribute(SearchUtils.GENERIC_KEY).toString());
			if (this.getAttribute(SearchUtils.GENDER_KEY) != null) {
				for (String gender : this.getAttribute(SearchUtils.GENDER_KEY)) {
					logger.info(gender);
					if (this.getAttribute(SearchUtils.GENERIC_KEY).contains(gender)) {
						logger.info("true");
						this.getAttribute(SearchUtils.GENERIC_KEY).remove(gender);
					}
				}
			}
			if (this.getAttribute(SearchUtils.COLOR_KEY) != null) {
				for (String color : this.getAttribute(SearchUtils.COLOR_KEY)) {
					if (this.getAttribute(SearchUtils.GENERIC_KEY).contains(color)) {
						this.getAttribute(SearchUtils.GENERIC_KEY).remove(color);
					}
				}
			}
			if (this.getAttribute(SearchUtils.BRAND_KEY) != null) {
				for (String brand : this.getAttribute(SearchUtils.BRAND_KEY)) {
					if (this.getAttribute(SearchUtils.GENERIC_KEY).contains(brand)) {
						this.getAttribute(SearchUtils.GENERIC_KEY).remove(brand);
					}
				}
			}
		}
		AttributeUtil.extractGlobalAttribute(this.getQuery().getDomainName(), this);

		// TODO: abstract following if conditions
		/* "loafers" when sent lemma version ie "loafer", it gets rejected because it cannot find any relevant category. Hence passing nonLemmaEntityStr */
		EsHttpSearch EsCategorySearch = new EsHttpSearch();
		if (nonLemmaEntityStr.contains(" ")) {
			logger.info("non lemma entity string: --" + nonLemmaEntityStr);
			if (nonLemmaEntityStr.split(" ")[0].trim().equalsIgnoreCase("")) {
				String[] queryArray = query.getQueryString().split(" ");
				for (int i = 0; i < queryArray.length; i++) {
					if (nonLemmaEntityStr.trim().equalsIgnoreCase(queryArray[i].trim())) {
						nonLemmaEntityStr = queryArray[i - 1] + nonLemmaEntityStr;
						logger.info("new non lemma entity string:" + nonLemmaEntityStr);
						break;
					}
				}
			}
			entityStr = entityStr.trim();
			String[] mainEntity = nonLemmaEntityStr.split(" ");
			entityStr = "";
			for (String entity : mainEntity) {
				/*the entity.trim().length() > 1 condition below is a quick fix for cases like where "salwar i" is being recognised as an entity*/
				if (entity.trim().length() > 2 && !this.hasAllCategoriesAttribute() && !entity.trim().equalsIgnoreCase(SearchUtils.COLOR_KEY) && !entity.trim().equalsIgnoreCase(SearchUtils.GENDER_KEY) && !entity.trim().equalsIgnoreCase(SearchUtils.GENERIC_KEY) && !entity.trim().matches("[0-9]+")) {
					entityStr = entityStr + " " + entity;
					//break;
				}
			}
			entityStr = entityStr.trim();
			/*
			This is for handling both the cases like "saree blouse" and "kalidar suit"
			Case 1: saree blouse
			both saree and blouse have category but saree blouse doesn't.
			Case 2: kalidar suit
			kalidar has no category but kalidar suit has category.
			To handle these cases instead of checking if each word has category or not to add into multiword entity, We are first removing all the
			color, price words and checking is the remaining string has a category. If it doesn't we are removing one word by one word and checking if the
			remaining string has category or not.
			 */
			if (!isRejectEntity(entityStr, query))
				entityStr = entityStr.trim();
			else {
				mainEntity = entityStr.split(" ");
				for (String entity : mainEntity) {
					if (!isRejectEntity(entityStr.replace(entity, ""), query)){
						entityStr = entityStr.replace(entity, "");
						break;
					}
				}
				entityStr = entityStr.trim();
			}
		} else {
			if (!this.hasAllCategoriesAttribute() && isRejectEntity(nonLemmaEntityStr, query) || entityStr.equalsIgnoreCase(SearchUtils.COLOR_KEY) || entityStr.equalsIgnoreCase(SearchUtils.GENDER_KEY) || entityStr.equalsIgnoreCase(SearchUtils.GENERIC_KEY)) {
				entityStr = "";
			}
		}

		if (!spQuery.getUniqueEntitySet().contains(entityStr)) {
			spQuery.getUniqueEntitySet().add(entityStr);
			if (entityStr != null && !entityStr.isEmpty()) {
				if (hasAllCategoriesAttribute()) {
					logger.info("All categories case");
					this.mainEntityList = new HashSet<String>();
					this.topEntityList = new HashSet<String>();
					this.mainEntityList.add(SearchUtils.ALL_CATEGORIES);
					this.topEntityList.add(SearchUtils.ALL_CATEGORIES);
				} else {
					this.wordVectorCategories = DomainKnowledgeBase.getWordVectorCategory(nonLemmaEntityStr, this.govWord, false);
					this.mainEntityList = DomainKnowledgeBase.getDomainCategory(this.query.getDomainName(), this.wordVectorCategories);
					/* lemmatizing in case no subcategories found */
					if (this.mainEntityList.isEmpty()) {
						this.entityStr = CoreNLPController.lemmatizeToString(this.entityStr);
						this.wordVectorCategories = DomainKnowledgeBase.getWordVectorCategory(this.entityStr, this.govWord, false);
						this.mainEntityList = DomainKnowledgeBase.getDomainCategory(this.query.getDomainName(), this.wordVectorCategories);
					}
					logger.info("multiWordEntity: " + multiWordEntity + "\tentityStr: " + entityStr);
					this.topEntityList = DomainKnowledgeBase.narrowDownSubCategoryList(((multiWordEntity != null) ? multiWordEntity : entityStr), mainEntityList);

					if (topEntityList != null && topEntityList.size() > 0)
						this.attributeMap.put(SearchUtils.XC_CATEGORY_KEY, topEntityList);
					else if (mainEntityList != null && mainEntityList.size() > 0)
						this.attributeMap.put(SearchUtils.XC_CATEGORY_KEY, mainEntityList);
					else
						logger.info("No Categories for\t" + this.entityStr);
				}
				logger.info("Main Entity: " + mainEntityList.toString());
				logger.info("Top Entity: " + ((topEntityList == null) ? "null" : topEntityList.toString()));

				logger.info("entityStr: " + entityStr);

				/*Getting subcategory from given category and client name*/
				if (this.getAttribute(SearchUtils.XC_CATEGORY_KEY) != null) {
					Set<String> xc_cat = this.getAttribute(SearchUtils.XC_CATEGORY_KEY);
					String[] categoryArray = xc_cat.toArray(new String[xc_cat.size()]);
					Map<String, String[]> genericAPIParam = new HashMap<String, String[]>();
					genericAPIParam.put("client_name", new String[] { spQuery.getClientName() });
					genericAPIParam.put("xc_category", categoryArray);
					IntentAPI objIntentAPI = new IntentAPI();
					//TODO: get subcategory specific to clients
				}

				if (!mainEntityList.isEmpty() || !topEntityList.isEmpty()) {
					this.cloudSearch(spQuery.customReturnAttributes);
					this.query.addToResultItems(this.structuredSearchResults, this.simpleSearchResults);
				} else {
					/*cases like "under 50 dollars" for intent takes "dollars" as entitystr but has no subcategories */
					if (this.getQuery().getIntentObject() != null) {
						this.mainEntityList = new HashSet<String>();
						this.topEntityList = new HashSet<String>();
						this.mainEntityList.add(SearchUtils.ALL_CATEGORIES);
						this.topEntityList.add(SearchUtils.ALL_CATEGORIES);
						this.entityStr = SearchUtils.ALL_CATEGORIES;
						this.cloudSearch(spQuery.customReturnAttributes);
						this.query.addToResultItems(this.structuredSearchResults, this.simpleSearchResults);
					}
				}
			}
		}
	}

	public JSONArray getConsumerSubCategory() {
		return this.consumerSubCategory;
	}

	/* used in Structure search. Since we don't call processEntity method in structureSearch API, this remains unchanged and needs to be set explicitly*/
	public void setConsumerSubCategory(JSONArray consumerSubCategory) {
		this.consumerSubCategory = consumerSubCategory;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @param intent
	 */
	public void setIntent(JSONObject intent) {
		this.intent = intent;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @return intent
	 */
	public JSONObject getIntent() {
		return intent;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @param clarification
	 */
	public void setClarification(JSONObject clarification) {
		this.clarification = clarification;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @return intent
	 */
	public JSONObject getClarification() {
		return clarification;
	}

	/**
	 * @return query of this object
	 */
	public XCQuery getQuery() {
		return query;
	}

	/**
	 * Sets this multiWordEntity; updates only to longer multiWordEntity
	 * @param multiWordEntity
	 */
	public void setMultiWordEntity(String multiWordEntity) {
		if (this.multiWordEntity == null || multiWordEntity.length() > this.multiWordEntity.length())
			this.multiWordEntity = multiWordEntity;
	}

	/**
	 * Sets this multiWordEntity; if toShorter, updates even in case of a shorter multiWordEntity
	 * @param multiWordEntity
	 * @param toShorter
	 */
	public void setMultiWordEntity(String multiWordEntity, boolean toShorter) {
		if (!toShorter)
			setMultiWordEntity(multiWordEntity);
		else
			this.multiWordEntity = multiWordEntity;
	}

	/**
	 * Sets this govWord
	 * @param govWord
	 */
	public void setGovWord(String govWord) {
		this.govWord = govWord;
	}

	/**
	 * Sets this directSearch; if set to true, no expansion of the initial search when retrieving
	 * relevant items from the catalog.
	 * @param value
	 */
	public void setDirectSearchOnly(boolean value) {
		this.directSearchOnly = value;
	}

	/**
	 * Adds the attribute value for this attribute key in the attribute map.
	 * Values are stored in a set.
	 * Updates this multi-entity when necessary.
	 * @param attributeKey
	 * @param attributeValue
	 */
	public void setAttribute(String attributeKey, String attributeValue) {
		if (attributeValue != null) {
			Set<String> valueSet = this.attributeMap.get(attributeKey);
			if (valueSet == null)
				valueSet = new HashSet<String>();

			// check if generic word is redundant or stop words such as need, how, etc.
			if (attributeKey.equals(SearchUtils.GENERIC_KEY)) {
				if (DetailsParser.genericStopWords.contains(attributeValue.toLowerCase())) {
					logger.info("Generic Key : " + attributeValue + " rejected.");
					return;
				}
				if (DetailsParser.genericStopWords.contains(CoreNLPController.lemmatizeToString(attributeValue.toLowerCase()))) {
					logger.info("Generic Key : " + attributeValue + " rejected.");
					return;
				}
			}
			valueSet.add(attributeValue);
			this.attributeMap.put(attributeKey, valueSet);

			if (attributeKey.equals(SearchUtils.GENERIC_KEY))
				setMultiWordEntity(this.entityStr);
			this.entityStr = this.entityStr.replaceAll("\\b" + attributeValue + "\\b", "");
			logger.info("Updated entity: " + this.toString());
			logger.info("Attribute key: " + attributeKey + "\tattributeValue: " + attributeValue + " new value: " + this.attributeMap.get(attributeKey));
		}
	}

	/**
	 * Adds this whole set of attribute values for this attribute key in the attribute map.
	 * @param attributeKey
	 * @param attributeValueSet
	 */
	public void setAttribute(String attributeKey, Set<String> attributeValueSet) {
		if (attributeValueSet != null) {
			Set<String> valueSet = this.attributeMap.get(attributeKey);
			if (valueSet == null)
				valueSet = new HashSet<String>();
			valueSet.addAll(attributeValueSet);
			this.attributeMap.put(attributeKey, valueSet);
			for (String attributeValue : attributeValueSet) {
				/*Remove any attribute like color, theme, etc. present in entity string except if it is in subcategory
				 * eg- query shirts have subcategory shirts which will make entity string empty*/
				String tempEntityStr = this.entityStr;
				this.entityStr = this.entityStr.replaceAll("\\b" + attributeValue + "\\b", "");
				if (this.entityStr.isEmpty() || this.entityStr == "")
					this.entityStr = tempEntityStr;
			}
		}
	}

	/**
	 * Adds to this attribute map the attributes and their value from that other entity.
	 * @param otherEntity
	 */
	public void addAttribute(XCEntity otherEntity) {
		Map<String, Set<String>> otherAttributeMap = otherEntity.getAttributeMap();
		for (Map.Entry<String, Set<String>> entry : otherAttributeMap.entrySet()) {
			String attributeKey = entry.getKey();
			Set<String> attributeValues = entry.getValue();
			Set<String> valueSet = this.attributeMap.get(attributeKey);
			if (valueSet == null)
				valueSet = new HashSet<String>();
			valueSet.addAll(attributeValues);
			this.attributeMap.put(attributeKey, valueSet);
		}
	}

	/**
	 * @param attributeKey
	 * @return whether this attribute is present or not
	 */
	public boolean isAttributePresent(String attributeKey) {
		return this.attributeMap.containsKey(attributeKey);
	}

	/**
	 * @return whether this entity has any attribute
	 */
	public boolean hasAttribute() {
		return !attributeMap.isEmpty();
	}

	/**
	 * @param attributeKey
	 * @return value set for this attribute
	 */
	public Set<String> getAttribute(String attributeKey) {
		return this.attributeMap.get(attributeKey);
	}

	/**
	 * Removes this attribute and its value set
	 * @param attributeKey
	 */
	public void removeAttribute(String attributeKey) {
		try {
			this.attributeMap.remove(attributeKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param entityStr
	 */
	public void setEntityStr(String entityStr) {
		this.entityStr = entityStr;
	}

	/**
	 * @return this entity string
	 */
	public String getEntityStr() {
		return entityStr;
	}

	/**
	 * @param entityStr
	 */
	public void setRelaxedEntityStr(String entityStr) {
		this.relaxedEntityStr = entityStr;
	}

	/**
	 * @return this relaxed entity string due to change in search controller
	 */
	public String getRelaxedEntityStr() {
		return relaxedEntityStr;
	}

	/*
	 * For APIs when we don't want to create SpQuery or doesn't need to go through the NLP pipeline
	 * SpEntity need to have domain it is querying for.
	 */
	public String getDomain() {
		if (this.getQuery() != null)
			return this.getQuery().getDomainName();
		else
			return this.domain;
	}

	public void setDomain(String domain) {
		if (this.getQuery() != null)
			this.domain = this.getQuery().getDomainName();
		else
			this.domain = domain;
	}

	/**
	 * @return this multi-word entity if it exists, otherwise this entity string
	 */
	public String getMultiWordEntity() {
		if (multiWordEntity == null)
			return entityStr;
		return multiWordEntity;
	}

	/**
	 * @return confidence map of retrieved items
	 */
	public Map<String, Double> getSkuConfidenceMap() {
		return skuConfidenceMap;
	}

	/**
	 * @return this attribute map
	 */
	public Map<String, Set<String>> getAttributeMap() {
		return attributeMap;
	}

	/**
	 * @return structured search results
	 */
	public List<SearchItem> getStructuredSearchResults() {
		return structuredSearchResults;
	}

	/**
	 * @return results from an unstructured search (no field specified)
	 */
	public List<SearchItem> getSimpleSearchResults() {
		return simpleSearchResults;
	}

	/**
	 * This entity can refer to all categories when no precise and definite entity is extracted
	 * from this query.
	 * @return whether the entity is not-defined, hence triggering a search across all categories
	 */
	public boolean hasAllCategoriesAttribute() {
		return entityStr.equals(SearchUtils.ALL_CATEGORIES);
	}

	/**
	 * @return whether to limit the search to the initial search without relaxing conditions
	 */
	public boolean getDirectSearchOnly() {
		return directSearchOnly;
	}

	/**
	 * Top subcategories are used to focus results.
	 * E.g. 'dress shirt' should only be searched amidst dress shirts or formal shirts.
	 * @return top matching subcategories
	 */
	public Set<String> getTopEntityList() {
		return topEntityList;
	}

	public void setTopEntityList(Set<String> topEntityList) {
		this.topEntityList = topEntityList;
	}

	/**
	 * @return the category of this entity using word vector embedding
	 */
	public List<String> getWordVectorCategories() {
		return wordVectorCategories;
	}

	/**
	 * @param wordVectorCategories
	 */
	public void setWordVectorCategories(List<String> wordVectorCategories) {
		this.wordVectorCategories = wordVectorCategories;
	}

	public String getGovWord() {
		return govWord;
	}

	/**
	 * @param simpleSearchResults
	 */
	public void setSimpleSearchResults(List<SearchItem> simpleSearchResults) {
		this.simpleSearchResults = simpleSearchResults;
	}

	/**
	 * @param structuredSearchResults
	 */
	public void setStructuredSearchResults(List<SearchItem> structuredSearchResults) {
		this.structuredSearchResults = structuredSearchResults;
	}

	/**
	 * @return the list of all the subcategories (catalog) relevant to this entity
	 */
	public Set<String> getMainEntityList() {
		return mainEntityList;
	}

	public void setMainEntityList(Set<String> mainEntityList) {
		this.mainEntityList = mainEntityList;
	}

	/**
	 * Retrieves semantically similar terms to this entity and stores it in this instance.
	 * Current implementation: based on small graph database
	 */
	private void getSemanticSimilarEntities() {
		if (this.mainEntityList != null) {
			for (String mainEntity : this.mainEntityList) {
				Set<String> similarEntities = DomainKnowledgeBase.getSimilarEntities(mainEntity);
				if (similarEntities != null) {
					logger.info("Semantically Similar Entities of " + mainEntity + "\t" + similarEntities);
					for (String similarEntity : similarEntities) {
						XCEntity e = new XCEntity(similarEntity, query, true);
						this.query.addToEntityMap(similarEntity, e);
					}
				}
			}
		}
	}

	/**
	 * Performs CloudSearch based on this entity and its attributes.
	 */
	public void cloudSearch(String... customReturnAttributes) {
		logger.info("CloudSearch searching for entity: " + this.getEntityStr() + "\tcategory: " + this.getMainEntityList() + "\tgender: " + this.getAttribute(SearchUtils.GENDER_KEY) + "\tcolor: " + this.getAttribute(SearchUtils.COLOR_KEY));
		SearchController objSearchController = new SearchController();
		objSearchController.getSearchResultsForEntity(this, customReturnAttributes);
		AttributeUtil.orderByAttribute(this.getQuery().getDomainName(), this);
	}

	/**
	 * Checks whether to reject this entity based on the domain of the query
	 * @param currentStr
	 * @param query
	 * @return
	 */
	public static boolean isRejectEntity(String currentStr, XCQuery query) {
		//TODO: rejecting explicit gender words like woman?
		String[] currentStrWordArray = currentStr.toLowerCase().trim().split(" ");
		logger.info("currentStr: " + currentStr);
		if (currentStr.isEmpty() || currentStr.equals("")) {
			logger.info(currentStr + " is reject for empty string");
			return true;
		}

		//rejecting if a number
		if (currentStr.trim().matches("\\d+")){
			logger.info(currentStr + " is reject for number");
			return true;
		}
		//TODO: initialize keepWords, stopWords in init() method
		Set<String> stopWords = new HashSet<String>();
		FileIO.read_file(XCConfig.processCommaSeparatedFileNames(query.getDomainName(), XCConfig.STOPWORDS_FILES), stopWords);
		for(String currentStrWord: currentStrWordArray) {
			if (stopWords.contains(currentStrWord.toLowerCase())) {
				logger.info(currentStr + " is reject for " + currentStrWord + " stop words");
				return true;
			}
			if (stopWords.contains(CoreNLPController.lemmatizeToString(currentStrWord.toLowerCase()))) {
				logger.info(currentStr + " is reject for " + currentStrWord + " stop words");
				return true;
			}
		}

		Set<String> keepWords = new HashSet<String>();
		FileIO.read_file(XCConfig.processCommaSeparatedFileNames(query.getDomainName(), XCConfig.KEEPWORDS_FILES), keepWords);
		/*if (keepWords.contains(currentStr.toLowerCase())) {
			logger.info(currentStr + " is not rejected for keep words");
			return false;
		}
		if (keepWords.contains(CoreNLPController.lemmatizeToString(currentStr.toLowerCase()))) {
			logger.info(currentStr + " is not rejected for keep words");
			return false;
		}*/

		int keepWordCount = 0;
		for (String currentStrWord: currentStrWordArray){
			if (keepWords.contains(currentStrWord)) {
				logger.info(currentStrWord + " is in keep words");
				keepWordCount = keepWordCount + 1;
			}
			else {
				if (keepWords.contains(CoreNLPController.lemmatizeToString(currentStrWord))) {
					logger.info(currentStrWord + " is in keep words");
					keepWordCount = keepWordCount + 1;
				}
			}
		}
		if (keepWordCount == currentStrWordArray.length){
			logger.info(currentStr + " is not reject for keep words");
			return false;
		}
		/*
		 * Rejecting past participle/adjective as entity
		 * NOTA: in domains where entities ending in "ed" exist (e.g. "bed"), necessary to put
		 * them in keepWords
		 */
		//In case of multi word entity checking if any of the words is rejectable
		for(String currentStrWord: currentStrWordArray) {

			if (currentStrWord.endsWith("ed") && !keepWords.contains(currentStrWord)) {
				logger.info(currentStr + " is reject for " + currentStrWord + " ending with ed");
				return true;
			}
			if (SearchByProductName.isRejectedByBrand(currentStrWord)) {
				logger.info(currentStr + " is reject for " + currentStrWord + " brand name");
				return true;
			}
			if (OntologyUtils.isRejectEntity(currentStrWord)) {
				logger.info(currentStr + " is reject by " + currentStrWord + " OntologyUtils");
				return true;
			}
		}

		//Proceeding with full word (same as before rules) .length > `1 kept because if it is single word entity then it's dealt above.
		if (currentStrWordArray.length > 1) {
			if (SearchByProductName.isRejectedByBrand(currentStr)) {
				logger.info(currentStr + " is reject for brand name");
				return true;
			}

			if (OntologyUtils.isRejectEntity(currentStr)) {
				logger.info(currentStr + " is reject by OntologyUtils");
				return true;
			}

		}
		if (AttributeUtil.isRejectByAttribute(query.getDomainName(), currentStr)) {
			logger.info(currentStr + " is reject for being other attribute value");
			return true;
		}
		//		Whether the entity is in a category relevant to the domain
		List<String> categories = DomainKnowledgeBase.getWordVectorCategory(currentStr, currentStr, false);
		if (categories == null || categories.size() == 0) {
			logger.info(currentStr + " is reject for not having any category");
			return true;
		}
		for (String category : categories) {
			if (DomainCategoryMapper.isCategoryOfDomain(query.getDomainName(), category)) {
				logger.info(currentStr + " is not rejected for having category: " + category);
				return false;
			}
		}

		return false;
	}

	public static boolean isRejectEntity(String currentStr, String queryDomainName) {
		//TODO: rejecting explicit gender words like woman?
		String[] currentStrWordArray = currentStr.toLowerCase().trim().split(" ");
		logger.info("currentStr: " + currentStr);
		if (currentStr.isEmpty() || currentStr.equals("")) {
			logger.info(currentStr + " is reject for empty string");
			return true;
		}

		//rejecting if a number
		if (currentStr.trim().matches("\\d+")){
			logger.info(currentStr + " is reject for number");
			return true;
		}
		//TODO: initialize keepWords, stopWords in init() method
		Set<String> stopWords = new HashSet<String>();
		FileIO.read_file(XCConfig.processCommaSeparatedFileNames(queryDomainName, XCConfig.STOPWORDS_FILES), stopWords);
		for(String currentStrWord: currentStrWordArray) {
			if (stopWords.contains(currentStrWord.toLowerCase())) {
				logger.info(currentStr + " is reject for " + currentStrWord + " stop words");
				return true;
			}
			if (stopWords.contains(CoreNLPController.lemmatizeToString(currentStrWord.toLowerCase()))) {
				logger.info(currentStr + " is reject for " + currentStrWord + " stop words");
				return true;
			}
		}

		Set<String> keepWords = new HashSet<String>();
		FileIO.read_file(XCConfig.processCommaSeparatedFileNames(queryDomainName, XCConfig.KEEPWORDS_FILES), keepWords);
		/*if (keepWords.contains(currentStr.toLowerCase())) {
			logger.info(currentStr + " is not rejected for keep words");
			return false;
		}
		if (keepWords.contains(CoreNLPController.lemmatizeToString(currentStr.toLowerCase()))) {
			logger.info(currentStr + " is not rejected for keep words");
			return false;
		}*/

		int keepWordCount = 0;
		for (String currentStrWord: currentStrWordArray){
			if (keepWords.contains(currentStrWord)) {
				logger.info(currentStrWord + " is in keep words");
				keepWordCount = keepWordCount + 1;
			}
			else {
				if (keepWords.contains(CoreNLPController.lemmatizeToString(currentStrWord))) {
					logger.info(currentStrWord + " is in keep words");
					keepWordCount = keepWordCount + 1;
				}
			}
		}
		if (keepWordCount == currentStrWordArray.length){
			logger.info(currentStr + " is not reject for keep words");
			return false;
		}
		/*
		 * Rejecting past participle/adjective as entity
		 * NOTA: in domains where entities ending in "ed" exist (e.g. "bed"), necessary to put
		 * them in keepWords
		 */
		//In case of multi word entity checking if any of the words is rejectable
		for(String currentStrWord: currentStrWordArray) {

			if (currentStrWord.endsWith("ed") && !keepWords.contains(currentStrWord)) {
				logger.info(currentStr + " is reject for " + currentStrWord + " ending with ed");
				return true;
			}
			if (SearchByProductName.isRejectedByBrand(currentStrWord)) {
				logger.info(currentStr + " is reject for " + currentStrWord + " brand name");
				return true;
			}
			if (OntologyUtils.isRejectEntity(currentStrWord)) {
				logger.info(currentStr + " is reject by " + currentStrWord + " OntologyUtils");
				return true;
			}
		}

		//Proceeding with full word (same as before rules) .length > `1 kept because if it is single word entity then it's dealt above.
		if (currentStrWordArray.length > 1) {
			if (SearchByProductName.isRejectedByBrand(currentStr)) {
				logger.info(currentStr + " is reject for brand name");
				return true;
			}

			if (OntologyUtils.isRejectEntity(currentStr)) {
				logger.info(currentStr + " is reject by OntologyUtils");
				return true;
			}

		}
		if (AttributeUtil.isRejectByAttribute(queryDomainName, currentStr)) {
			logger.info(currentStr + " is reject for being other attribute value");
			return true;
		}
		logger.info("The beauty entity is not rejected");
		return false;
	}

	public static void init() {

	}
}
