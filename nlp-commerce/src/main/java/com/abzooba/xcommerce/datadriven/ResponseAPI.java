package com.abzooba.xcommerce.datadriven;

import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCLogger;

/**
 * Created by mayank on 19/7/17.
 */
public class ResponseAPI {
	private Map<String, String> urlBuilderParams;
	private String server_url;
	private static Logger logger = XCLogger.getSpLogger();

	public ResponseAPI(Map<String, String> urlBuilderParams, String server_url) {
		this.urlBuilderParams = new HashMap<>();
		this.urlBuilderParams = urlBuilderParams;
		this.server_url = server_url;
	}

	public JSONObject getResponse() {
		URL url = null;
		try {
			URIBuilder uriBuilder = new URIBuilder(server_url);
			if (urlBuilderParams != null && urlBuilderParams.size() > 0) {
				for (String key : urlBuilderParams.keySet()) {
					if (!urlBuilderParams.get(key).isEmpty())
						uriBuilder.addParameter(key, urlBuilderParams.get(key));
				}
			}
			try {
				url = uriBuilder.build().toURL();
			} catch (MalformedURLException e1) {
				logger.error("Url is not correct : " + server_url + "\n" + e1.getMessage());
			}
		} catch (URISyntaxException e) {
			logger.error("Error in creating URI");
		}
		HttpURLConnection con = null;
		try {
			if (url != null) {
				con = (HttpURLConnection) url.openConnection();
				if (con != null) {
					con.setRequestMethod("GET");
					con.setRequestProperty("Content-Type", ContentType.APPLICATION_JSON.getMimeType() + ";charset=utf-8");
					con.setRequestProperty("Accept", ContentType.APPLICATION_JSON.getMimeType());
					con.setConnectTimeout(3000);
					int statusCode = con.getResponseCode();

					if (statusCode >= 400 && statusCode < 500) {
						logger.error("Request Exception:\t" + con.getContent().toString());
					} else if (statusCode >= 500 && statusCode < 600) {
						logger.error("Internal Server Error. Please try again as this might be a transient error condition.");
					} else {
						StringWriter writer = new StringWriter();
						IOUtils.copy(con.getInputStream(), writer, "utf-8");
						JSONObject jsonObject = new JSONObject(writer.toString());
						return jsonObject;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error occurred in fetching entities from data driven model : \n" + e.getMessage());
		} finally {
			if (con != null)
				con.disconnect();
		}
		return new JSONObject();
	}

	public static void main(String[] args) {
		Map<String, String> params = new HashMap<>();
		params.put("query", "lipstick");
		String server_url = "http://stgapi.xpresso.ai/nlpstgintent/";
		ResponseAPI apiCall = new ResponseAPI(params, server_url);
		System.out.println(apiCall.getResponse());
		params = new HashMap<>();
		params.put("q", "hello");
		params.put("token", "J6wOwW4dVEQfmCcW");
		server_url = "http://54.225.29.139:5004/parse";
		apiCall = new ResponseAPI(params, server_url);
		System.out.println(apiCall.getResponse());
	}
}
