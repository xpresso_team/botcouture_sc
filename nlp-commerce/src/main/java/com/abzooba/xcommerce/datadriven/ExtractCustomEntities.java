package com.abzooba.xcommerce.datadriven;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.IntentAPI;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 * Created by mayank on 18/7/17.
 */
public class ExtractCustomEntities {

	private String userQuery;
	private Object XCHierarchies;
	private static final String RASA_SERVER = XCConfig.RASA_SERVER;
	private static final String RASA_TOKEN = XCConfig.RASA_TOKEN;
	private static final String ES_URL = XCConfig.ES_URL;

	public ExtractCustomEntities(String sentence) {
		this.userQuery = sentence;
	}

	private String entity;

	public JSONObject getEntityJson() throws JSONException {
		JSONObject jsonObject = new JSONObject();
		Map<String, String> params = new HashMap<>();
		params.put("q", userQuery);
		params.put("token", RASA_TOKEN);
		ResponseAPI apiCall = new ResponseAPI(params, RASA_SERVER);
		JSONObject response = apiCall.getResponse();
		if (response.has(Constants.ENTITIES)) {
			Multimap<String, Object> mapOfEntity = HashMultimap.create();
			org.codehaus.jettison.json.JSONArray entities = null;
			try {
				entities = response.getJSONArray(Constants.ENTITIES);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			for (int i = 0; i < entities.length(); i++) {
				JSONObject entity = null;
				try {
					entity = entities.getJSONObject(i);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				try {
					if (entity.has(Constants.ENTITY) && entity.get(Constants.ENTITY) != null && entity.get(Constants.ENTITY) != "") {
						if (entity.get(Constants.ENTITY).equals("product")) {
							JSONObject categoryFromIntentAPI = getCategoryFromWordClusters(entity.get(Constants.VALUE).toString());

							mapOfEntity.put(Constants.ENTITY, entity.get(Constants.VALUE));

							if (categoryFromIntentAPI.has("syn-category")) {
								String xc_category = categoryFromIntentAPI.getString("syn-category");
								mapOfEntity.put("xc_category", xc_category.toLowerCase());
							} else
								mapOfEntity.put("xc_category", null);

						} else if (entity.get(Constants.ENTITY).equals(Constants.BRAND))
							mapOfEntity.put(Constants.BRAND, entity.get(Constants.VALUE));
						else if (entity.get(Constants.ENTITY).equals(Constants.GENDER))
							mapOfEntity.put(Constants.GENDER, entity.get(Constants.VALUE));
						else if (entity.get(Constants.ENTITY).equals("color"))
							mapOfEntity.put("colorsavailable", entity.get(Constants.VALUE));
						else if (entity.get(Constants.ENTITY).equals(Constants.PRODUCT_QUALIFIER))
							mapOfEntity.put("details", entity.get(Constants.VALUE));
						else if (entity.get(Constants.ENTITY).equals("size"))
							mapOfEntity.put("size", entity.get(Constants.VALUE));
						else if (entity.get(Constants.ENTITY).equals(Constants.PRICE_QUALIFIER))
							mapOfEntity.put(Constants.PRICE_QUALIFIER, entity.get(Constants.VALUE));
						else if (entity.get(Constants.ENTITY).equals(Constants.PRICE))
							mapOfEntity.put(Constants.PRICE, entity.get(Constants.VALUE));
						else if (entity.get(Constants.ENTITY).equals("priceLowerLimit"))
							mapOfEntity.put("priceLowerLimit", entity.get(Constants.VALUE));
						else if (entity.get(Constants.ENTITY).equals("priceUpperLimit"))
							mapOfEntity.put("priceUpperLimit", entity.get(Constants.VALUE));

					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			jsonObject = resolveEntities(mapOfEntity);
		}
		return jsonObject;
	}

	private JSONObject resolveEntities(Multimap<String, Object> mapOfEntity) throws JSONException {
		JSONObject jsonObject = new JSONObject();

		if (mapOfEntity.containsKey(Constants.ENTITY)) {
			Object entity = mapOfEntity.get(Constants.ENTITY).iterator().next();
			jsonObject.put(Constants.ENTITY, entity);
		} else
			jsonObject.put(Constants.ENTITY, "anything");

		if (mapOfEntity.containsKey("xc_category")) {
			String xc_category = (String) mapOfEntity.get("xc_category").iterator().next();
			Set<String> set_xc_category = new HashSet<>();
			set_xc_category.add(xc_category);
			Object gender = null;
			if (mapOfEntity.containsKey(Constants.GENDER))
				gender = mapOfEntity.get(Constants.GENDER).iterator().next();
			String entity = null;
			if (jsonObject.has(Constants.ENTITY))
				entity = jsonObject.getString(Constants.ENTITY);
			List<String> xcHierarchies = getXCHierarchies(gender, set_xc_category, entity);
			jsonObject.put("xc_hierarchy_str", xcHierarchies);
		} else
			jsonObject.put("xc_hierarchy_str", new ArrayList<>());

		if (mapOfEntity.containsKey(Constants.GENDER)) {
			Collection<Object> gender = mapOfEntity.get(Constants.GENDER);
			gender.add("unisex");
			jsonObject.put(Constants.GENDER, gender);
		} else
			jsonObject.put(Constants.GENDER, new ArrayList<>());

		if (mapOfEntity.containsKey(Constants.BRAND)) {
			Collection<Object> brands = mapOfEntity.get(Constants.BRAND);
			jsonObject.put(Constants.BRAND, brands);
		} else
			jsonObject.put(Constants.BRAND, new ArrayList<>());

		if (mapOfEntity.containsKey("colorsavailable")) {
			Collection<Object> colorsavailable = mapOfEntity.get("colorsavailable");
			jsonObject.put("colorsavailable", colorsavailable);
		} else
			jsonObject.put("colorsavailable", new ArrayList<>());

		if (mapOfEntity.containsKey("details")) {
			Collection<Object> details = mapOfEntity.get("details");
			jsonObject.put("details", details);
		} else
			jsonObject.put("details", new ArrayList<>());

		if (mapOfEntity.containsKey("size")) {
			Collection<Object> size = mapOfEntity.get("size");
			jsonObject.put("size", size);
		} else
			jsonObject.put("size", new ArrayList<>());

		if (mapOfEntity.containsKey("xc_category")) {
			Collection<Object> xc_category = mapOfEntity.get("xc_category");
			jsonObject.put("xc_category", xc_category);
		} else
			jsonObject.put("xc_category", new ArrayList<>());

		jsonObject.put("features", new ArrayList<>());

		String priceQualifier = null;
		if (mapOfEntity.containsKey(Constants.PRICE_QUALIFIER)) {
			priceQualifier = (String) mapOfEntity.get(Constants.PRICE_QUALIFIER).iterator().next();
		}
		if (mapOfEntity.containsKey(Constants.PRICE)) {
			String price = (String) mapOfEntity.get(Constants.PRICE).iterator().next();
			if (priceQualifier.trim().equals("<>")) {
				Integer pr = Integer.parseInt(price);
				Double lowerPrice = pr - (0.1 * pr);
				Double upperPrice = pr + (0.1 * pr);
				String finalPrice = priceQualifier + " " + lowerPrice.intValue() + " " + upperPrice.intValue();
				ArrayList<String> priceArray = new ArrayList();
				priceArray.add(finalPrice);
				jsonObject.put(Constants.PRICE, priceArray);
			} else {
				Integer pr = Integer.parseInt(price);
				String finalPrice = priceQualifier + " " + pr;
				ArrayList<String> priceArray = new ArrayList();
				priceArray.add(finalPrice);
				jsonObject.put(Constants.PRICE, priceArray);
			}
		} else if (mapOfEntity.containsKey("priceLowerLimit") && mapOfEntity.containsKey("priceUpperLimit")) {
			String finalPrice = "<>" + " " + mapOfEntity.get("priceLowerLimit") + " " + mapOfEntity.get("priceUpperLimit");
			ArrayList<String> priceArray = new ArrayList();
			priceArray.add(finalPrice);
			jsonObject.put(Constants.PRICE, priceArray);

		} else
			jsonObject.put(Constants.PRICE, new ArrayList<>());

		jsonObject.put("features", new ArrayList<>());

		return jsonObject;

	}

	public JSONObject getCategoryFromWordClusters(String userQuery) {
		Map<String, String> params = new HashMap<>();
		userQuery = userQuery.replaceAll(" ", "%20");
		String server_url = ES_URL + userQuery;
		ResponseAPI apiCall = new ResponseAPI(params, server_url);
		return apiCall.getResponse();
	}

	public List<String> getXCHierarchies(Object gender, Set<String> set_xc_category, String entity) {
		IntentAPI intentAPI = new IntentAPI();
		intentAPI.setEntity(entity);
		List<String> xcHierarchy = null;
		try {
			xcHierarchy = intentAPI.getXCHierarchy(gender, set_xc_category);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return xcHierarchy;
	}
}
