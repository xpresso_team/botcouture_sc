package com.abzooba.xcommerce.search;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;
import org.slf4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class EsHttpSearch {
    private static Logger logger = XCLogger.getSpLogger();
    public static HashMap<String, String> esCacheMap = new HashMap<>();
    public String ElasticSearchCategory(String name) {
        long startTime = System.nanoTime();
        try {
            String returnCategory = "";
            if (esCacheMap.containsKey(name)) {
                returnCategory = esCacheMap.get(name);
            }
            else {
                URL url = new URL(XCConfig.ES_URL_RULE + URLEncoder.encode(name.replace(" ", "_").replace("-", "_"), "UTF-8"));
                logger.info("URL for the get request is " + url);
                BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
                returnCategory = br.readLine().replace("\"", "");
                esCacheMap.put(name, returnCategory);
            }
            logger.info("Time taken to get category name from the proxy api for ElasticSearch is " + (float) (System.nanoTime() - startTime) / 1000000000);
            if (returnCategory.length() != 0)
                return returnCategory;
            else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
	/*public static void main(String[] args){
	    EsHttpSearch s = new EsHttpSearch();
	    while (1 == 1){
	        System.out.println("Enter your username: ");
	        Scanner scanner = new Scanner(System.in);
	        String username = scanner.nextLine();
	        System.out.println(s.ElasticSearchCategory(username));
	    }
	}*/
}