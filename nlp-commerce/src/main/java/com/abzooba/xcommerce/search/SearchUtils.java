/**
 * 
 */
package com.abzooba.xcommerce.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.abzooba.xcommerce.nlp.domain.XCDomain;

/**
 * @author Sudhanshu Kumar
 * May 18, 2016 9:16:43 AM
 * Synaptica  SearchUtils
 */
public class SearchUtils {
	private static HashMap<String, ArrayList<String>> returnAttributes = new HashMap<String, ArrayList<String>>();

	public static final String SKU_KEY = "sku";
	public static final String SCORE_KEY = "_score";
	public static final String CONFIDENCE_KEY = "confidence";
	public static final String SUGGESTED_KEY = "suggested";
	public static final String DOMAIN_KEY = "domain";
	public static final String ENTITY_KEY = "entity";

	/* 
	 * xcfashion
	 * 
	 * */
	public static final String ITEM_KEY = "item";
	public static final String GOV_WORD_KEY = "govword";
	public static final String PRODUCT_NAME_KEY = "productname";
	public static final String COLOR_KEY = "colorsavailable";
	public static final String BRAND_KEY = "brand";
	public static final String GENDER_KEY = "gender";
	public static final String XC_CATEGORY_KEY = "xc_category";
	public static final String SUBCATEGORY_KEY = "subcategory";
	public static final String PRICE_NUMERIC_KEY = "price";
	public static final String FEATURE_KEY = "features";
	public static final String GENERIC_KEY = "details";
	public static final String IMAGE_KEY = "image";
	public static final String CLIENT_NAME = "client_name";
	public static final String SIZE = "size";

	/* 
	 * This key word is used to retrieve items from any categories:
	 * e.g. "anything in red" 
	 * */
	public static final String ALL_CATEGORIES = "anything";

	public static final String ASCENDING_ORDER = "asc";
	public static final String DESCENDING_ORDER = "desc";

	/* 
	 * Costco
	 * 
	 * */
	public static final String SCREEN_SIZE = "screensizeclass";
	public static final String NAME = "resolution";
	public static final String SCREEN_TYPE = "screentype"; //	flat | curved
	public static final String SMART_TV = "smarttv";
	public static final String WIFI_ENABLED = "wifienabled";
	public static final String DISPLAY_TYPE = "displaytype"; //	lcd | led
	public static final String USB_INPUT = "inputsusb";
	public static final String HDMI_INPUT = "inputshdmi";

	public static void init(XCDomain[] domainObjectsArr) {
		for (XCDomain d : domainObjectsArr) {
			String[] returnAttributesArr = d.getSearchReturnAttributes().split(",");
			ArrayList<String> ret = new ArrayList<String>();
			for (String r : returnAttributesArr) {
				ret.add(r);
			}
			returnAttributes.put(d.getDomainName(), ret);
		}
	}

	public static List<String> getReturnAttributes(String domain) {
		return returnAttributes.get(domain);
	}

	public static void addToReturnAttributes(String returnAttr, String domain) {
		returnAttributes.get(domain).add(returnAttr);
	}
}
