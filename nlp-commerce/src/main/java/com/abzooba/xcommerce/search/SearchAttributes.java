package com.abzooba.xcommerce.search;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCEntity;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.nlp.attribute.price.PriceParser;

public class SearchAttributes implements Cloneable {

	private String queryStr = null;
	private String multiWordStr = null;

	private static Logger logger;
	private Map<String, String[]> filterAttributeMap = new LinkedHashMap<String, String[]>();
	private Map<String, Double> skuConfidenceMap;

	public static final String DIRECT_ATTRIBUTE = "direct";
	public static final String RANGE_ATTRIBUTE = "range";

	String priceSortOrder;
	String customReturnAttributes;

	public static final HashMap<String, String> QUERY_OPTIONS_MAP = new HashMap<String, String>();

	public static void init() {
		logger = XCLogger.getSpLogger();

		String value = "['colorsavailable^2','gender^1','subcategory^1.5','productname^2','brand^1','department','details','keywords','category']";
		QUERY_OPTIONS_MAP.put("fields", value);
	}

	public int getFilterAttributeCount() {
		return filterAttributeMap.size();
	}

	public SearchAttributes(String queryStr) {
		this.queryStr = queryStr;
		this.multiWordStr = queryStr;
	}

	public String getQueryString() {
		return queryStr;
	}

	/**
	 * @return the priceSortOrder
	 */
	public String getPriceSortOrder() {
		return priceSortOrder;
	}

	/**
	 * @return the skuConfidenceMap
	 */
	public Map<String, Double> getSkuConfidenceMap() {
		return skuConfidenceMap;
	}

	/**
	 * @return multi-word entity string in case it exists; the single-word entity string otherwise
	 */
	public String getMultiWordStr() {
		return multiWordStr;
	}

	/**
	 * @param priceSortOrder the priceSortOrder to set
	 */
	public void setPriceSortOrder(String priceSortOrder) {
		this.priceSortOrder = priceSortOrder;
	}

	public SearchAttributes(XCEntity spEntity, String... customReturnAttributes) {
		if (customReturnAttributes.length > 0 && customReturnAttributes[0] != null) {
			this.customReturnAttributes = customReturnAttributes[0];
		}
		this.queryStr = spEntity.getEntityStr();
		this.multiWordStr = spEntity.getMultiWordEntity();
		this.skuConfidenceMap = spEntity.getSkuConfidenceMap();
		Map<String, Set<String>> filterMap = spEntity.getAttributeMap();
		prepareFilterMap(filterMap);
		logger.info("filterAttributeMap:\t" + attributeMapToString()); // filterAttributeMap);
	}

	public SearchAttributes(String query, Map<String, Set<String>> filterMap) {
		this.queryStr = query;
		this.multiWordStr = query;
		prepareFilterMap(filterMap);
		logger.info("filterAttributeMap:\t" + attributeMapToString()); // filterAttributeMap);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Query: ");
		sb.append(queryStr);
		sb.append("\tfilter map: ");
		sb.append(attributeMapToString());

		return sb.toString();
	}

	private static String[] getPriceFilterArray(String... varargs) {
		return varargs;
	}

	public void prepareFilterMap(Map<String, Set<String>> filterMap) {
		for (Entry<String, Set<String>> entry : filterMap.entrySet()) {
			switch (entry.getKey()) {
				case SearchUtils.PRICE_NUMERIC_KEY:
					String priceQualifier = PriceParser.getPriceQualifier(entry.getValue());
					String[] priceValue = PriceParser.getPriceValue(entry.getValue()).split(" ");
					String[] filterArray = null;
					switch (priceQualifier) {
						case PriceParser.GREATER_SIGN:
							this.priceSortOrder = SearchUtils.ASCENDING_ORDER;
							filterArray = getPriceFilterArray(RANGE_ATTRIBUTE, priceQualifier, priceValue[0]);
							filterAttributeMap.put(SearchUtils.PRICE_NUMERIC_KEY, filterArray);
							break;
						case PriceParser.LESSER_SIGN:
							this.priceSortOrder = SearchUtils.DESCENDING_ORDER;
							filterArray = getPriceFilterArray(RANGE_ATTRIBUTE, priceQualifier, priceValue[0]);
							filterAttributeMap.put(SearchUtils.PRICE_NUMERIC_KEY, filterArray);
							break;
						case PriceParser.BETWEEN_SIGN:
							this.priceSortOrder = SearchUtils.DESCENDING_ORDER;
							if (priceValue.length == 2) {
								String priceMin = Math.min(Integer.parseInt(priceValue[0]), Integer.parseInt(priceValue[1])) + "";
								String priceMax = Math.max(Integer.parseInt(priceValue[0]), Integer.parseInt(priceValue[1])) + "";
								filterArray = getPriceFilterArray(RANGE_ATTRIBUTE, priceQualifier, priceMin, priceMax);
								filterAttributeMap.put(SearchUtils.PRICE_NUMERIC_KEY, filterArray);
							} else if (priceValue.length == 1) {
								filterArray = getPriceFilterArray(RANGE_ATTRIBUTE, "!" + priceQualifier, priceValue[0]);
								filterAttributeMap.put(SearchUtils.PRICE_NUMERIC_KEY, filterArray);
							}
							break;
						case PriceParser.EQUALS_SIGN:
							filterArray = getPriceFilterArray(RANGE_ATTRIBUTE, priceQualifier, priceValue[0]);
							filterAttributeMap.put(SearchUtils.PRICE_NUMERIC_KEY, filterArray);
							break;
					}
					break;
				default:
					/*replace levi's to levi\'s to escape in url passed */
					String valueStr = entry.getValue().toString().replaceAll("\\[\\]", "").replaceAll("\\'", "\\\\'");
					filterArray = new String[] { DIRECT_ATTRIBUTE, valueStr };
					filterAttributeMap.put(entry.getKey(), filterArray);
					filterAttributeMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { DIRECT_ATTRIBUTE, this.multiWordStr });
			}
		}
	}

	public void addFilter(String attribute, Object value, boolean isNot) {
		String valueStr = value.toString().replaceAll("\\[\\]", "");
		String[] filterArray = new String[3];
		filterArray[0] = DIRECT_ATTRIBUTE;
		filterArray[1] = valueStr;
		filterArray[2] = (isNot) ? "not" : null;
		filterAttributeMap.put(attribute, filterArray);
	}

	public String getFilterValue(String attribute) {
		String[] answ = filterAttributeMap.get(attribute);
		if (answ != null)
			return answ[1];
		return null;
	}

	/**
	 * The method composes the part of the URL string that is used to query the CloudSearch using
	 * the search attributes
	 * @param defaultOperator
	 * @return
	 */

	public String queryComposition(XCEntity spEntity, String defaultOperator, String... excludeAttributes) {
		StringBuilder sb = new StringBuilder();
		if (defaultOperator != null && queryStr != null) {
			spEntity.setRelaxedEntityStr(queryStr);
			try {
				sb.append("(");
				sb.append(defaultOperator);
				if (queryStr != null && !queryStr.equals(SearchUtils.ALL_CATEGORIES)) {
					sb.append(URLEncoder.encode(" ", "UTF-8"));
					sb.append("'");
					sb.append(URLEncoder.encode(queryStr, "UTF-8"));
					sb.append("'");
				}
				if (filterAttributeMap.size() > 0) {
					List<String> exAttributes = null;
					if (excludeAttributes != null) {
						exAttributes = new ArrayList<String>(Arrays.asList(excludeAttributes));
					}

					for (Entry<String, String[]> attribute : filterAttributeMap.entrySet()) {
						if (exAttributes != null && exAttributes.contains(attribute.getKey())) {
							spEntity.relaxationMap.put(attribute.getKey(), new HashSet<String>());
							continue;
						}
						String[] attributeValuesArr = attribute.getValue();
						spEntity.relaxationMap.put(attribute.getKey(), spEntity.getAttribute(attribute.getKey()));
						if (attributeValuesArr[0].equals(DIRECT_ATTRIBUTE)) {
							String tupleStr = directSearchGroupCompose(attribute.getKey(), attributeValuesArr);
							sb.append(tupleStr);
						} else if (attributeValuesArr[0].equals(RANGE_ATTRIBUTE)) {
							String tupleStr = rangeSearchGroupCompose(attribute.getKey(), attributeValuesArr);
							sb.append(tupleStr);
						}
					}
				}
				sb.append(")");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	/**
	 * The method composes the direct search group using the filter attribute key and value for the CloudSearch query
	 * @param key
	 * @param values
	 * @return
	 */
	private String directSearchGroupCompose(String key, String[] values) {
		if (values[1].isEmpty())
			return "";

		StringBuilder sb = new StringBuilder("(");
		if (values.length >= 3 && values[2] != null) {
			sb.append(values[2]);
			sb.append(" (");
		}
		if (values[1].contains(",")) {
			String[] splitValues = values[1].split(",");
			sb.append("or");
			for (String value : splitValues) {
				sb.append("(");
				sb.append(prepareField(key, value));
				sb.append(")");
			}
		} else {
			sb.append(prepareField(key, values[1]));
		}
		sb.append(")");
		return sb.toString();
	}

	/**
	 * The method composes the direct match tuple with the filter attribute key and value
	 * @param key
	 * @param value
	 * @return
	 */
	public String prepareField(String key, String value) {
		StringBuilder sb = new StringBuilder();

		sb.append("term");

		try {
			sb.append(URLEncoder.encode(" ", "UTF-8"));
			sb.append("field");
			sb.append(URLEncoder.encode("=", "UTF-8"));
			sb.append(key);
			sb.append(URLEncoder.encode(" ", "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		sb.append("'");
		try {
			value = value.replaceAll("\\[", "").replaceAll("\\]", "").trim();
			String filterTerm = URLEncoder.encode(value, "UTF-8");
			sb.append(filterTerm);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		sb.append("'");
		return sb.toString();
	}

	/**
	 * The method composes the range group using the range tuple generally composed of value and qualifier
	 * for the CloudSearch query
	 * @param key
	 * @param values
	 * @return
	 */
	public String rangeSearchGroupCompose(String key, String[] values) {
		if (values.length < 2)
			return "";
		StringBuilder sb = new StringBuilder("(range");
		try {
			sb.append(URLEncoder.encode(" ", "UTF-8"));
			sb.append("field");
			sb.append(URLEncoder.encode("=", "UTF-8"));
			sb.append(key);
			sb.append(URLEncoder.encode(" ", "UTF-8"));
			sb.append(rangeTuple(values));
			sb.append(")");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	/**
	 * The method composes the range tuple from the value and the qualifier for the CloudSearch query 
	 * @param values
	 * @return
	 */
	private String rangeTuple(String[] values) {
		StringBuilder sb = new StringBuilder();
		switch (values[1].trim()) {
			case "<":
				sb.append("{,");
				sb.append(values[2]);
				sb.append("]");
				break;
			case ">":
				sb.append("[");
				sb.append(values[2]);
				sb.append(",}");
				break;
			case "<>":
				sb.append("[");
				sb.append(values[2]);
				sb.append(",");
				sb.append(values[3]);
				sb.append("]");
				break;
			default:
				sb.append("[");
				sb.append(Math.max(0, Integer.parseInt(values[2]) * (100 - XCConfig.PRICE_DELTA) / 100));
				sb.append(",");
				sb.append(Integer.parseInt(values[2]) * (100 + XCConfig.PRICE_DELTA) / 100);
				sb.append("]");
		}
		return sb.toString();
	}

	/**
	 * The method converts the filter attribute map to string
	 * @return
	 */
	private String attributeMapToString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		for (Entry<String, String[]> entry : filterAttributeMap.entrySet()) {
			sb.append(entry.getKey());
			sb.append("=[");
			for (String str : entry.getValue()) {
				sb.append(str);
				sb.append(",");
			}
			if (entry.getValue().length > 0)
				sb.deleteCharAt(sb.length() - 1);
			sb.append("];");
		}
		if (!filterAttributeMap.isEmpty())
			sb.deleteCharAt(sb.length() - 1);
		sb.append("}");
		return sb.toString();
	}

	public void setQueryString(String string) {
		this.queryStr = string;
	}

}
