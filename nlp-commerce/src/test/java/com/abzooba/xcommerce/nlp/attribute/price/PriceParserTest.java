package com.abzooba.xcommerce.nlp.attribute.price;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.core.XpExpression;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

/**
 * Created by mayank on 21/2/17.
 */
public class PriceParserTest {
	@Test
	public void priceDetectionModuleExhaustive() throws Exception {
		XCEngine.init();

		String queryStr = "looking for a blue wallet under 100 bucks";
		assertEquals("< 100", getPriceString(queryStr));

		queryStr = "shirt between 50 and 100";
		assertEquals("<> 50 100", getPriceString(queryStr));

		queryStr = "red top for less than 100";
		assertEquals("< 100", getPriceString(queryStr));

		queryStr = "jeans not greater than 120$";
		assertEquals("< 120", getPriceString(queryStr));

		//		queryStr = "nighty in 70$";
		//		assertEquals("< 70", getPriceString(queryStr));

		queryStr = "nighty for 70$";
		assertEquals("<> 63 77", getPriceString(queryStr));

		queryStr = "my budget is below 1500$";
		assertEquals("< 1500", getPriceString(queryStr));

		queryStr = "show me those in the range of 5000 and 10000$";
		assertEquals("<> 5000 10000", getPriceString(queryStr));

		queryStr = "show me the cheaper ones like below 1000$";
		assertEquals("< 1000", getPriceString(queryStr));

		queryStr = "these look cheap, show me those above 1000$";
		assertEquals("> 1000", getPriceString(queryStr));

		queryStr = "I can't afford these.... show me the cheaper ones like below 1000$";
		assertEquals("< 1000", getPriceString(queryStr));

		queryStr = "those under 900$ are feasible for me";
		assertEquals("< 900", getPriceString(queryStr));

		queryStr = "show me those in between 500 to 1000$";
		assertEquals("<> 500 1000", getPriceString(queryStr));

		queryStr = "I need to buy a pair of peep toes but only under 1000$";
		assertEquals("< 1000", getPriceString(queryStr));

		queryStr = "show the stock you have above 2000$";
		assertEquals("> 2000", getPriceString(queryStr));

		queryStr = "i need a classy wallet under 1000$";
		assertEquals("< 1000", getPriceString(queryStr));

		queryStr = "do you keep windbreakers for women between 1000-1500 bucks?";
		assertEquals("<> 1000 1500", getPriceString(queryStr));

		queryStr = "these are too costly..do you have it below 1000$?";
		assertEquals("< 1000", getPriceString(queryStr));

		queryStr = "show me levi's denim skirts in red in size L under 1000$";
		assertEquals("< 1000", getPriceString(queryStr));

		queryStr = "I am out of cash so show me those below of 1000$";
		assertEquals("< 1000", getPriceString(queryStr));

		queryStr = "i think he will look dapper in black...keep it below 1500$";
		assertEquals("< 1500", getPriceString(queryStr));

		queryStr = "looking for a blue shawl for my grandmother above 500";
		assertEquals("> 500", getPriceString(queryStr));

		queryStr = "show the ones that fall under 2000$";
		assertEquals("< 2000", getPriceString(queryStr));

		queryStr = "something that is cheap...like below 2000$";
		assertEquals("< 2000", getPriceString(queryStr));

	}

	private String getPriceString(String queryStr) {
		XpExpression xp = new XpExpression(XpConfig.Languages.EN, XpConfig.Annotations.EXPR, false, null, null, null, queryStr, false);
		List<CoreMap> sentencesList = xp.getSentences();
		for (CoreMap sentence : sentencesList) {
			SemanticGraph dependencyGraph = xp.getDependencyGraph(sentence);
			Tree parseTree = xp.getParseTree(sentence);
			List<CoreLabel> tokens = xp.getTokens(sentence);
			return PriceParser.priceDetectionModuleExhaustive(parseTree, dependencyGraph, sentence.toString());
		}
		return null;
	}
}