package com.abzooba.xcommerce.nlp.attribute;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.abzooba.xcommerce.core.XCEngine;

/**
 * Created by mayank on 9/3/17.
 */
public class GenderSearchTest {
	@Test
	public void checkForGender() throws Exception {
		XCEngine.init();

		String[] matcher = GenderSearch.checkForGender("top for my sis");
		assertEquals("sis" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("watch for my grandma");
		assertEquals("grandma" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("Pants for my fiance under 45 $");
		assertEquals("fiance" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("show me shirts for my brother-in-law.");
		assertEquals("brother-in-law" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("Shoes for my sister");
		assertEquals("sister" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("looking for red shirt for my father-in-law");
		assertEquals("father-in-law" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("I am looking for jeans for my fat aunt");
		assertEquals("aunt" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("color for her should be black");
		assertEquals("her" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("looking for a sports tshirt for my dad");
		assertEquals("dad" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("floral skirts for my daughter");
		assertEquals("daughter" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("do you have a top for a small frame girl");
		assertEquals("girl" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("my friend's birthday is coming up, want to buy a dress for her.");
		assertEquals("her" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("I don't have any black joggers, do you have any for men?");
		assertEquals("men" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("I want to gift a blue shawl to my grandmother");
		assertEquals("grandmother" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("i need to gift a leather belt in black to my niece");
		assertEquals("niece" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("show me black pants as my dad loves to wear");
		assertEquals("dad" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("I need sneakers for my aunt");
		assertEquals("aunt" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("show me shrugs for my sister");
		assertEquals("sister" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("Do you have shirts for men");
		assertEquals("men" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("Do you have sports watches that I can buy for 1000$ for women");
		assertEquals("women" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("I need jackets for the winters for my uncle");
		assertEquals("uncle" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("I need sports shoes for my aunt");
		assertEquals("aunt" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("do you keep windbreakers for women between 1000-1500 bucks?");
		assertEquals("women" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("shoes by Puma for my daughter");
		assertEquals("daughter" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("was thinking to buy a Handbag from parfois for my wife");
		assertEquals("wife" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("show me some swimsuits for my girlfriend please");
		assertEquals("girlfriend" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("Michael kors tops for daughter");
		assertEquals("daughter" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("he only wears tommy hilfiger");
		assertEquals("he" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("can you show me a shirt by Park Avenue for men");
		assertEquals("men" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("want to purchase Sweater for men");
		assertEquals("men" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("show me some warm jackets preferably by Kappa for men");
		assertEquals("men" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("show me some coats for men");
		assertEquals("men" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("can you show me caps for men by nine west?");
		assertEquals("men" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("i need boxers by jockey for men");
		assertEquals("men" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("do you have shirts? I want to gift one to my brother-in-law");
		assertEquals("brother-in-law" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("looking for a blue shawl for my grandmother above 500");
		assertEquals("grandmother" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("show some tank tops for women in black color in size M");
		assertEquals("women" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("i am looking for a blue blazer for my brother");
		assertEquals("brother" + "men", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("was thinking to buy a Handbag from parfois or michael kors for my wife");
		assertEquals("wife" + "women", matcher[0] + matcher[1]);

		matcher = GenderSearch.checkForGender("my brother wants a stylish biker jacket");
		assertEquals("brother" + "men", matcher[0] + matcher[1]);

	}
}