package com.abzooba.xcommerce.nlp.attribute;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.abzooba.xcommerce.core.XCEngine;

/**
 * Created by mayank on 9/3/17.
 */
public class ColorSearchTest {
	@Test
	public void spotColor() throws Exception {
		XCEngine.init();
		assertEquals(true, ColorSearch.spotColor("reddish", "JJ", null));
	}

	@Test
	public void checkForColor() throws Exception {
		XCEngine.init();
		assertEquals("white", getColor("white top for my girlfriend"));
		assertEquals("magenta", getColor("magenta trouser for 100$"));
		assertEquals("black", getColor("I don't have any black joggers, do you have any for men?"));
		assertEquals("brown", getColor("show me in brown for a change"));
		assertEquals("ivory", getColor("looking for bridal gowns in ivory"));
		assertEquals("blue", getColor("I want to gift a blue shawl to my grandmother"));
		assertEquals("black", getColor("i need to gift a leather belt in black to my niece"));
		assertEquals("white", getColor("I don't have any white leggings ,do you have any?"));
		assertEquals("white", getColor("I would prefer it in white only."));
		assertEquals("black", getColor("I want black kitten heels"));
		assertEquals("black", getColor("show me black pants as my dad loves to wear"));
		assertEquals("red", getColor("show me levi's denim skirts in red in size L under 1000$"));
		assertEquals("black", getColor("can you help me get a really nice stilettos in black in size 7?"));
		assertEquals("green", getColor("show me green skirts"));
		assertEquals("black", getColor("i think he will look dapper in black...keep it below 1500$"));
		assertEquals("blue", getColor("looking for a blue shawl for my grandmother above 500"));
		assertEquals("black", getColor("show some tank tops for women in black color in size M"));
		assertEquals("black", getColor("I would like it by Kalt in black"));
		assertEquals("blue", getColor("i am looking for a blue blazer for my brother"));
		assertEquals("brown", getColor("i am going on a trip and need a rucksack in brown"));

	}

	private String getColor(String searchQuery) {
		String colorString = searchQuery.replaceAll("[^a-zA-Z\\s]", "");
		return ColorSearch.checkForColor(colorString);
	}
}