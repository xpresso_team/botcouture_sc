package com.abzooba.xcommerce.domain.xcfashion;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;
import org.junit.Test;
import org.slf4j.Logger;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by mayank on 2/5/17.
 */
public class LoadXCHierarchyTest {

    private static Logger logger;
    @Test
    public void loadXCHierarchy() throws Exception {
        XCLogger.init();
        XCConfig.init();
        logger = XCLogger.getSpLogger();
        LoadXCHierarchy.init();
        List<XCHierarchy> xcHierarchyList = LoadXCHierarchy.getXCHierarchyList();
        XCHierarchy xcHierarchy = new XCHierarchy(null,"jeans",null,null);
        logger.info(xcHierarchyList.get(xcHierarchyList.indexOf(xcHierarchy)).toString());
        assertEquals(true,xcHierarchyList.contains(xcHierarchy));
    }

}