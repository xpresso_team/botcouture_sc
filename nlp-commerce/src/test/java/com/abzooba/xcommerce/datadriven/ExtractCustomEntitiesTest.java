package com.abzooba.xcommerce.datadriven;

import com.abzooba.xcommerce.core.XCEngine;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

public class ExtractCustomEntitiesTest {

    @Test
    public void testGetEntityJson() throws Exception {
        XCEngine.init();
        ExtractCustomEntities extractEntities = new ExtractCustomEntities("Do you sell hair dryers, i desperately want one for around 500 rupees from philips");
        JSONObject entityJson = extractEntities.getEntityJson();
        System.out.println(entityJson);
    }
}