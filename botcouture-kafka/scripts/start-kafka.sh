#!/bin/bash

ZOOKEEPER_IP=${ZOOKEEPER_IP:-localhost:2181}
start-topic-creation.sh $ZOOKEEPER_IP > /topic_creation.log &

if [[ -n $ZOOKEEPER_IP ]]; then
    echo "zookeeper ip: $ZOOKEEPER_IP"
    if grep -q "^zookeeper.connect" $KAFKA_HOME/config/server.properties; then
    	echo "executing sed"
        sed -r -i "s/(zookeeper.connect)=(.*)/\1=$ZOOKEEPER_IP/g" $KAFKA_HOME/config/server.properties
    else
        echo -e "\n" >> $KAFKA_HOME/config/server.properties
        echo "zookeeper.connect=$ZOOKEEPER_IP" >> $KAFKA_HOME/config/server.properties
    fi
fi


if [[ -n $EXT_PORT ]]; then
    # NETWORK_GW=$(ip route show | grep default | cut -d ' ' -f3)
    MC_IP=$(ip addr show dev eth0 | grep global | awk '{print $2}' | cut -d/ -f1)
    # HOSTNAME=$(hostname)
    LISTENERS="PLAINTEXT://${MC_IP}:${EXT_PORT}"
    # if [[ $EXT_PORT != "9092" ]]; then
    #     LISTENERS="${LISTENERS},PLAINTEXT://${HOSTNAME}:9092"
    # fi
    echo -e "\n" >> $KAFKA_HOME/config/server.properties
    echo "listeners=${LISTENERS}" >> $KAFKA_HOME/config/server.properties
fi

echo -e "\n" >> $KAFKA_HOME/config/server.properties
echo "auto.create.topics.enable=false"

# # Set the external host and port
# if [ ! -z "$ADVERTISED_HOST" ]; then
#     echo "advertised host: $ADVERTISED_HOST"
#     if grep -q "^advertised.host.name" $KAFKA_HOME/config/server.properties; then
#         sed -r -i "s/#(advertised.host.name)=(.*)/\1=$ADVERTISED_HOST/g" $KAFKA_HOME/config/server.properties
#     else
#         echo -e "\n" >> $KAFKA_HOME/config/server.properties
#         echo "advertised.host.name=$ADVERTISED_HOST" >> $KAFKA_HOME/config/server.properties
#     fi
# fi
# if [ ! -z "$ADVERTISED_PORT" ]; then
#     echo "advertised port: $ADVERTISED_PORT"
#     if grep -q "^advertised.port" $KAFKA_HOME/config/server.properties; then
#         sed -r -i "s/#(advertised.port)=(.*)/\1=$ADVERTISED_PORT/g" $KAFKA_HOME/config/server.properties
#     else
#         echo -e "\n" >> $KAFKA_HOME/config/server.properties
#         echo "advertised.port=$ADVERTISED_PORT" >> $KAFKA_HOME/config/server.properties
#     fi
# fi


$KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties
