zk=$1

timeout_exceeded=false
count=0
step=5
TIMEOUT=100
brokers=""

while [[ -z $brokers ]];do
    echo "kafka not ready still"
    sleep $step
    
    count=$(expr $count + $step)
    if [[ $count -gt $TIMEOUT ]]; then
        timeout_exceeded=true
        break
    fi
    
    brokers=$($KAFKA_HOME/bin/zookeeper-shell.sh $zk <<< "ls /brokers/ids" | grep -E "\[[0-9](, [0-9])*\]")
    echo "brokers:>$brokers<"
done

if $timeout_exceeded; then
    echo "Not able to auto-create topic (waited for $TIMEOUT sec)"
    exit 1
fi

create-topic.sh $zk $TOPIC_NAME
wait
echo "... topic creation exits."