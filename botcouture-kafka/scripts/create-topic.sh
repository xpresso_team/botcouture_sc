ZK_IP=$1
TOPIC_NAME=$2

$KAFKA_HOME/bin/kafka-topics.sh --create --zookeeper $ZK_IP --replication-factor 1 --partitions 1 --topic $TOPIC_NAME
#  ... alter message retention duration on kafka server 
$KAFKA_HOME/bin/kafka-configs.sh --zookeeper $ZK_IP --entity-type topics --entity-name $TOPIC_NAME --alter --add-config retention.ms=259200000
$KAFKA_HOME/bin/kafka-configs.sh --zookeeper $ZK_IP --entity-type topics --entity-name $TOPIC_NAME --alter --add-config retention.bytes=1073741824
#  ... verify that the topic has been created
$KAFKA_HOME/bin/kafka-topics.sh --describe --zookeeper $ZK_IP
