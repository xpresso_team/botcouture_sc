import jsonschema
import falcon


def validate_format(json_body, generic_schema):
    """
    Validates the structure of json body. Raise and error if format does not match
    """

    try:
        jsonschema.validate(json_body, generic_schema)
    except jsonschema.exceptions.ValidationError as ve:
        raise falcon.HTTPError(falcon.HTTP_400, 'Invalid JSON Format',
                               'Provided JSON format is not supported')