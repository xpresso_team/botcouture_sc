__author__ = 'naveen'

import json
import os


def load_config():

    """
    Loads json configuration from the file.

    """

    config_file = os.environ.get("CONTROLLER_SERVICE_CONFIG_FILE")

    with open(config_file) as json_data_file:
        data = json.load(json_data_file)
    return data

config = load_config()
