import pylru
import copy

"""
Lru cache manager: manages all required interaction with cache
"""

lru_cache = None


def setup_cache(cache_size):
    global lru_cache
    lru_cache = pylru.lrucache(cache_size)


def get_cached_results(hash_key):
    """
    Get cached results for a query.
    :return:
    """
    if hash_key in lru_cache:
        copied_result = copy.deepcopy(lru_cache[hash_key])
        return copied_result
    return None


def insert_results_in_cache(hash_key, results):
    """
    insert results in the cache with the provided hash key
    :param hash_key: unique hash
    :param results: results which needs to be cached
    :return:
    """
    lru_cache[hash_key] = copy.deepcopy(results)