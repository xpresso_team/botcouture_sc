import logging

from app.utils import hashing
from app.caching import cache_manager


def get_cached_external_request(json_body):
    """
    Convert external request json body to hashable document
    :param json_body:
    :return: cached results, None if not cached
    """
    hashable_document = {
        "qtype": json_body["qtype"],
        "qcontext": json_body["qcontext"],
        "qmethod": json_body["qmethod"],
        "qparameter": json_body["qparameter"]
    }
    hash_key = hashing.generate_hash_from_dict(hashable_document)
    logging.info("Hash Key:["+hash_key+"]")
    cached_results = cache_manager.get_cached_results(hash_key)
    return cached_results, hash_key
