import math
from app import constants
from app.utils import generic


def parse_and_check(price_str, key):
    """
    Parse price and check if given key matches the price format
    :param price_str:  price format
    :param key: price format
    :return: True, if price matches, False otherwise
    """

    is_match = False
    # Check if type of key is valid
    price_key = 0
    if type(key) == str and generic.isfloat(key):
        price_key = float(key)
    elif type(key) == float:
        price_key = key
    elif type(key) == int:
        price_key = key
    elif type(key) == list and len(key) > 0 and generic.isfloat(key[0]):
        price_key = float(key[0])
    else:
        return is_match

    float_list_in_string = [float(s) for s in price_str.split() if generic.isfloat(s)]
    if constants.PRICE_BETWEEN_SIGN in price_str and len(float_list_in_string) >= 2:

        minima = float_list_in_string[0]
        maxima = float_list_in_string[1]

        if minima <= price_key <= maxima:
            is_match = True

    elif constants.PRICE_EQUAL_SIGN in price_str and len(float_list_in_string) >= 1:
        equal_val = float_list_in_string[0]
        if math.isclose(equal_val, price_key, rel_tol=1e-3):
            is_match = True

    elif constants.PRICE_GREATER_SIGN in price_str and len(float_list_in_string) >= 1:
        minima = float_list_in_string[0]
        if price_key >= minima:
            is_match = True

    elif constants.PRICE_LESSER_SIGN in price_str and len(float_list_in_string) >= 1:
        maxima = float_list_in_string[0]
        if price_key <= maxima:
            is_match = True

    return is_match