from app import constants
from app.filter import price_parser
from app.config import config


def match_document(doc1, doc2, ignore_list):
    """
    Match two documents and check if they have every value same
    :param doc1:  document which needs to matched
    :param doc2:  document which needs to matched
    :return: if documents are exact match
    """
    is_all_value_same = True
    for key, value in doc1.items():

        if not is_all_value_same:
            break

        if key not in doc2:
            continue

        # Empty value
        if not value:
            continue

        if key in ignore_list:
            continue

        if constants.PRICE_KEY in key:
            price_str = value
            if type(value) == list and len(value) > 0:
                price_str = value[0]

            is_all_value_same = is_all_value_same and price_parser.parse_and_check(price_str, doc2[key])
        elif constants.GENDER_KEY in key:
            gender_str = value
            if type(value) == list and len(value) > 0:
                gender_str = value[0]

            gender_list = gender_str.split(',')
            for each_val in gender_list:
                is_all_value_same = is_all_value_same and bool(each_val in doc2[key])
        elif type(value) is list:
            for each_val in value:
                is_all_value_same = is_all_value_same and bool(each_val in doc2[key])
        else:
            is_all_value_same = is_all_value_same and bool(value in doc2[key])

    return is_all_value_same


def filter_results(filter_parameter, results, ignore_keys=[]):
    """
    Filter the results
    :param filter_parameter:
    :return: filtered results
    """
    if results is None:
        return None

    if filter_parameter is None:
        return results

    filtered_result = []
    ignore_list = list(config["FILTER"]["IGNORE"]) + list(ignore_keys)
    for result in results:
        if match_document(filter_parameter, result, ignore_list):
            filtered_result.append(result)

    return filtered_result



