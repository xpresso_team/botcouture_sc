__author__ = 'naveen'

import falcon
import logging
import sys
import os

import app.routes as routes
from app.init import init
from app.config import config
from xpresso.ai.core.logging.xpr_log import XprLogger

"""
Controller is the central service which performs following tasks:

     1) Make sure data going back from here is correct and well formed
     2) Perform personalization or diversification when required
     3) Perform extra processing on the intermediate data when needed
     4) Log every possible API transaction.

Falcon based Rest API Framework is used.

This file instantiates the falcon API and
 list all currently supported resources.

 1) generic_redirects
 2) vision
 3) nlp
 4) nlp_customer_support
 5) collection
 6) datalayer
 7) emotions

"""

# Setting up logger
#base_logger = logging.getLogger()
#base_logger.setLevel(logging.INFO)

#ch = logging.StreamHandler(sys.stdout)
#ch.setLevel(logging.INFO)
#formatter = logging.Formatter('[%(asctime)s] [%(filename)s:%(lineno)d:%(funcName)s()] [%(levelname)s] - %(message)s')
#ch.setFormatter(formatter)
#base_logger.addHandler(ch)

project_root = os.getenv('ROOT_FOLDER')
config_file = os.path.join(project_root, 'config', 'xpr_log_stage.json')

logging = XprLogger(config_path=config_file)

logging.info("Initializing")

# falcon.API instances are callable WSGI apps
app = falcon.API()
routes.setup(app)

# Initialization done here
init()

# Setting up Cache
cache_opts = {
    'cache.type': 'file',
    'cache.data_dir': '/tmp/cache/data',
    'cache.lock_dir': '/tmp/cache/lock'
}

# Setup Swagger documentation
class SwaggerStaticResource(object):
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.content_type = 'application/json'
        with open(config["SWAGGER"]["JSON_FILE"], 'r') as f:
            resp.body = f.read()

app.add_route('/swagger_file', SwaggerStaticResource())

logging.info("------------ Server has started ------------------")
