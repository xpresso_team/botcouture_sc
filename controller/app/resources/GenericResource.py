__author__ = 'Naveen Sinha'

import json
import logging
import datetime

import falcon

from app.falcon_utils.json_parser import parse

from app.falcon_utils.json_vaidater import validate_format
from app.schema.generic_post_json import schema as generic_schema
import app.request_handlers.vision_handler as vision_handler
import app.request_handlers.nlp_handler as nlp_handler
import app.request_handlers.collection_handler as collection_handler
import app.request_handlers.datalayer_handler as datalayer_handler
import app.request_handlers.emotion_handler as emotion_handler
import app.request_handlers.nlp_customer_service_handler as nlp_customer_service_handler


class GenericResource(object):
    """
    This is used to request any supported APIs.
    It simply forwards the request to the corresponding
    request handler

    """

    def on_post(self, req, resp):
        start_time = datetime.datetime.now()

        json_body = parse(req)

        # Validate format
        validate_format(json_body, generic_schema)

        qtype = json_body['qtype']

        if "vision" == qtype:
            vision_handler.process(req, resp, json_body)
        elif "nlp" == qtype:
            if "structuredSearch" in json_body["qcontext"]:
                json_body["qcontext"] = "structuredSearch/"
                datalayer_handler.process(req, resp, json_body)
            elif "intent":
                json_body["qcontext"] = ""
                nlp_handler.process(req, resp, json_body)
        elif "collection" == qtype:
            collection_handler.process(req, resp, json_body)
        elif "datalayer" == qtype:
            datalayer_handler.process(req, resp, json_body)
        elif "emotion" == qtype:
            emotion_handler.process(req, resp, json_body)
        elif "nlp_cs" == qtype:
            nlp_customer_service_handler.process(req, resp, json_body)
        else:
            json_resp = {
                "description": "Provided qtype [" + qtype + "] is not supported yet",
                "title": "Non support qtype"
            }
            # Create a JSON representation of the resource
            resp.body = json.dumps(json_resp, ensure_ascii=True)

            # The following line can be omitted because 200 is the default
            # status returned by the framework, but it is included here to
            # illustrate how this may be overridden as needed.
            resp.status = falcon.HTTP_200

        end_time = datetime.datetime.now()
        logging.info("[" + str(json_body) + "][" + str((end_time - start_time).microseconds / 1000) + "]")
