__author__ = 'Naveen Sinha'

import json
import logging
import datetime

import requests
import falcon

from app.config import config


class NLPSearchResource(object):
    """
    This is used to request any supported APIs.
    It simply forwards the request to the corresponding
    request handler

    """

    def on_get(self, req, resp):
        start_time = datetime.datetime.now()

        json_resp = {}
        if req.get_param("query") and req.get_param("src"):
            try:
                intent_resp = requests.get(config["API"]["NLP"], params={"query": req.get_param("query")})
                intent_json = intent_resp.json()
                intent_json["client_name"] = req.get_param("src")
                del intent_json["xc_hierarchy_str"]
                for key, value in intent_json.items():
                    if isinstance(value, list):
                        intent_json[key] = ','.join(value)

                result_resp = requests.get(config["API"]["DATALAYER"] + "structuredSearch/", params=intent_json)
                json_resp = result_resp.json()
                json_resp["Query"] = req.get_param("query")
                json_resp["Results Count"] = ""
                json_resp["Suggested Results Count"] = ""
                json_resp["Direct Results Count"] = ""
                json_resp["Suggested Results"] = ""
                json_resp["Clarification"] = ""

            except:
                json_resp = {}

        else:
            json_resp = {
                "code": 400,
                "msg": "Please send src and query as parameter"
            }

        # Create a JSON representation of the resource
        resp.body = json.dumps(json_resp, ensure_ascii=True)

        # The following line can be omitted because 200 is the default
        # status returned by the framework, but it is included here to
        # illustrate how this may be overridden as needed.
        resp.status = falcon.HTTP_200

        end_time = datetime.datetime.now()
        logging.info("[][" + str((end_time - start_time).microseconds / 1000) + "]")
