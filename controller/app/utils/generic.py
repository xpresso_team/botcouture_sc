
def isfloat(value):
    """
    Checks if string can be converted to float
    :param value:
    :return:
    """
    try:
        float(value)
        return True
    except ValueError:
        return False
