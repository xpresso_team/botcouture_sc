import json
import hashlib


def generate_hash_from_dict(document):
    """
    Generate unique has from a dictionary object
    :param document: python dictionary for which hash needs to be generated
    :return: Hash of the document
    """
    hash_key = str(hashlib.sha1(json.dumps(document, sort_keys=True).encode('utf-8')).hexdigest())
    return hash_key
