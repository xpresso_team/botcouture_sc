import json
import itertools
import logging
from app.request_handlers.external_request import perform_generic_redirect
import app.constants as constants
import copy

"""
Read given json file and load the relaxation process
in the memory for future use
"""


class RelaxationManager:
    """
    Manager of
    1) Relaxation steps
    2) Relaxation attributes
    3) Relaxation logic

    Reads the relaxation json file and stores in memory.
    If there are k attributes present, then it will iterate
    by dropping 1 - k-1 attributes in stages. It will stop
    at the stage where there exist some results.
    """

    # Path to config file
    config_file = ""

    # Min Search Hits
    min_search_hits = 100

    # Attributes to consider while relaxing
    attributes_to_relax = []

    # Minimum no of attributes to consider at max
    min_attributes_to_consider = 1

    # This filter should not be ignored. This must be present
    hard_filter = []

    # It is better if this is present
    soft_filter = []

    # Attribute Name Map for sending back proper name
    attr_name_map = {}

    # Mandatory item for search parameter
    mandatory_field = []

    def init(self, config):
        """
        Read the relaxation json file and setup the data
        """
        self.config_file = config

        with open(self.config_file) as config_file:
            relaxation_data = json.load(config_file)

            if constants.JSON_MIN_SEARCH_HIT_KEY in relaxation_data:
                self.min_search_hits = relaxation_data[constants.JSON_MIN_SEARCH_HIT_KEY]

            if constants.ATTRIBUTE_TO_RELAX in relaxation_data:
                self.attributes_to_relax = relaxation_data[constants.ATTRIBUTE_TO_RELAX]

            if constants.MIN_ATTRIBUTES_TO_CONSIDER in relaxation_data:
                self.min_attributes_to_consider = relaxation_data[constants.MIN_ATTRIBUTES_TO_CONSIDER]

            if constants.SOFT_FILTER in relaxation_data:
                self.soft_filter = relaxation_data[constants.SOFT_FILTER]

            if constants.HARD_FILTER in relaxation_data:
                self.hard_filter = relaxation_data[constants.HARD_FILTER]
            if constants.ATTR_NAME_MAP in relaxation_data:
                self.attr_name_map = relaxation_data[constants.ATTR_NAME_MAP]

            if constants.MANDATORY_FIELD in relaxation_data:
                self.mandatory_field = relaxation_data[constants.MANDATORY_FIELD]

    def check_if_result_exist(self, ignore_set, final_parameter, parameter_body, search_uri):
        """
        Request downstream service to fetch the data for the current parameter
        :param final_parameter: parameters used to create get request
        :param parameter_body: BOdy of the parameters we got
        :param search_uri: URI path to hit to get results
        :return: response from the query
        """

        parameter_body["qparameter"] = copy.deepcopy(final_parameter)
        for item in ignore_set:
            if item in parameter_body["qparameter"]:
                del parameter_body["qparameter"][item]

        curr_response = {constants.RESULT_COUNT_KEY: 0}

        for field in self.soft_filter:
            parameter_body[constants.QPARAMETER][constants.PAGE_SIZE_KEY] = 0
            if field in parameter_body[constants.QPARAMETER]:
                del parameter_body[constants.QPARAMETER][field]
            curr_response = perform_generic_redirect(base_uri=search_uri,
                                                     json_body=parameter_body)

            if curr_response[constants.RESULT_COUNT_KEY] >= self.min_search_hits:
                break

        return curr_response[constants.RESULT_COUNT_KEY] >= self.min_search_hits

    def get_full_results_and_create_response(self,
                                             is_relaxed,
                                             attributes_set_to_relax,
                                             attributes_set_to_ignore,
                                             cur_param,
                                             final_parameter,
                                             parameter_body,
                                             search_uri,
                                             relaxation_attributes_present):
        """
        Get full results for the parameter and create a valid response.
        If relaxation is required, then create a relaxation response
        """

        final_response = {
            constants.RESULTS_KEY: [],
            constants.SUGGESTED_RESULT_KEY: []
        }
        logging.info("Is Relaxed:"+str(is_relaxed))
        if not is_relaxed:
            parameter_body[constants.QPARAMETER][constants.PAGE_SIZE_KEY] = 1000
            curr_response = perform_generic_redirect(base_uri=search_uri,
                                                     json_body=parameter_body)
            final_response[constants.RESULTS_KEY] = curr_response[constants.RESULTS_KEY]

            # If result is not present, then remove everything and do basic entity search
            if len(final_response[constants.RESULTS_KEY]) == 0:
                parameter_body[constants.QPARAMETER][constants.PAGE_SIZE_KEY] = 1000

                only_entity_parameter = {}
               
                for attr in self.mandatory_field :
                    if attr in parameter_body[constants.QPARAMETER]:
                        only_entity_parameter[attr] = parameter_body[constants.QPARAMETER][attr]

                parameter_body[constants.QPARAMETER] = only_entity_parameter
                cur_param = only_entity_parameter
                curr_response = perform_generic_redirect(base_uri=search_uri,
                                                         json_body=parameter_body)
                final_response[constants.SUGGESTED_RESULT_KEY] = curr_response[constants.RESULTS_KEY]

        clarification_node = {}
        for key, value in final_parameter.items():
            if key in cur_param:
                clarification_node[key] = {
                    "Old": value,
                    "New": cur_param[key]
                }
            else:
                clarification_node[key] = {
                    "Old": value,
                    "New": []
                }

        final_response["Clarification"] = clarification_node
        final_response["Intent"] = final_parameter
        final_response["Spell Corrected"] = False
        final_response["Query"] = final_parameter["entity"]
        final_response["Direct Results Count"] = len(final_response[constants.RESULTS_KEY])
        final_response["Suggested Results Count"] = len(final_response[constants.SUGGESTED_RESULT_KEY])
        final_response["Results Count"] = len(final_response[constants.RESULTS_KEY]) + len(
            final_response[constants.SUGGESTED_RESULT_KEY])
        final_response["is_relaxed_combination_exist"] = False
        final_response["relaxation_attributes"] = list(relaxation_attributes_present)

        if is_relaxed:
            final_response["is_relaxed_combination_exist"] = True
            final_response["relaxation"] = {
                constants.ENTITY_KEY: final_parameter[constants.ENTITY_KEY],
                "suggested_combination": []
            }

            for i, value in enumerate(attributes_set_to_relax):
                new_doc = {
                    "remove_attributes": attributes_set_to_ignore[i],
                    "combination": []
                }
                for attr in value:
                    new_doc["combination"].append(
                        {
                            "attribute_name": self.attr_name_map[attr],
                            "attribute_key": attr,
                            "attribute_value": final_parameter[attr]
                        }
                    )

                final_response["relaxation"]["suggested_combination"].append(new_doc)

        return final_response

    def process(self, search_uri, parameter_body):
        """
        Perform following steps

        1) Identify number of attributes present, k
        2) Iterate by dropping (1 -> k-1 attribute ) at a time
           and perform search for left over attributes
        3) Return each group for which result exists.
        """

        if "intent" not in parameter_body:
            return {}

        original_intent = parameter_body["intent"]
        final_parameter = {}
        relaxation_attributes_present = set()

        # Setup all xc_hierarchy, relaxation attributes
        # and ignore invalid values
        for key, value in original_intent.items():
            if constants.XC_HIERARCHY_KEY == key:
                """
                For xc_hierarchy, send all possible hierarchy from bottom to top
                """
                split_value = value.split('|')
                new_value = []
                for cur_val in split_value:
                    if len(new_value) == 0:
                        new_value.append(cur_val)
                    else:
                        new_value.append(new_value[-1] + "|" + cur_val)

                final_parameter[key] = ','.join(new_value)
            elif value:
                final_parameter[key] = value
                if key in self.attributes_to_relax:
                    relaxation_attributes_present.add(key)

        # Do direct search and check if results are found
        is_direct_result_exist = self.check_if_result_exist(set(), final_parameter, parameter_body, search_uri)
        logging.info("Is Direct Result Exist:" + str(is_direct_result_exist))

        if is_direct_result_exist:
            return self.get_full_results_and_create_response(False, [], [],
                                                             final_parameter,
                                                             final_parameter,
                                                             parameter_body,
                                                             search_uri,
                                                             relaxation_attributes_present)

        # No results found, perform relaxation process
        relaxation_attributes_count = len(relaxation_attributes_present)
        # Size of combination
        combination_size = relaxation_attributes_count - 1
        # Set of attributes that could be relaxed
        attributes_set_to_relax = []
        attributes_set_to_ignore = []
        while combination_size > 0 and len(attributes_set_to_relax) == 0:
            for combination_set in itertools.combinations(relaxation_attributes_present, combination_size):
                ignore_set = relaxation_attributes_present - set(combination_set)
                logging.info("Checking combination:[" + str(combination_set) + "][" + str(ignore_set) + "]")
                if self.check_if_result_exist(ignore_set, final_parameter, parameter_body, search_uri):
                    attributes_set_to_relax.append(combination_set)
                    attributes_set_to_ignore.append(list(ignore_set))

                    # self.check_if_result_exist(final_parameter, parameter_body, search_uri)
            combination_size -= 1

        return self.get_full_results_and_create_response(len(attributes_set_to_relax) > 0,
                                                         attributes_set_to_relax,
                                                         attributes_set_to_ignore,
                                                         final_parameter,
                                                         final_parameter,
                                                         parameter_body,
                                                         search_uri,
                                                         relaxation_attributes_present)

# Relaxation Manager object
relaxation_manager = RelaxationManager()
