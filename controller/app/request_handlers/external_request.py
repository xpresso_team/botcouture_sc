import requests
import logging

import falcon
import requests_cache

requests_cache.install_cache('external_requests')


def perform_generic_redirect(base_uri, json_body):
    """
    Perform external request to external API and returns
     the response
    :param base_uri: API base url to hit
    :param json_body: dict.contains query context, method and parameters
    :return: response from the redirect
    """
    qmethod = json_body["qmethod"]
    qcontext = json_body["qcontext"]
    qparameter = merge_array_json(json_body["qparameter"])
    api_url = base_uri + qcontext

    json_result = {
        "message": "API Request failed",
        "info": json_body["qmethod"] + "::" + json_body["qcontext"],
        "qtype": json_body["qtype"]
    }

    logging.info(api_url + ":" + qcontext + ":")
    logging.info("" + str(qparameter))
    if "GET" == qmethod:
        try:
            api_resp = requests.get(api_url, params=qparameter)
            logging.info(api_resp.url)
            json_result = api_resp.json()

        except Exception as ex:
            logging.error(" Error on external request[" +
                          api_url + "]:[" +
                          falcon.to_query_str(params=qparameter) + "]:[" + str(ex) + "]")

    elif "POST" == qmethod:
        try:
            api_resp = requests.post(api_url, data=qparameter)
            json_result = api_resp.json()
        except Exception as ex:
            logging.error(" Error on external request[" +
                          api_url + "]:[" +
                          qparameter + "]:[" + str(ex) + "]")

    return json_result


def merge_array_json(json_body):
    """ 
    Merge json array in a comma separate string
    """
    for key, value in json_body.items():
        if isinstance(value, list):
            json_body[key] = ','.join(value)
    return json_body
