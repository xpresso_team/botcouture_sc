import logging

__author__ = 'naveen'

import json
import falcon
import requests

from app.config import config


def process(req, resp, json_body):
    """
    Process the emotion based request.
    Query emotion api and sort the result if required.
    """

    api_url = config[config["EMOTION_HANDLER"]]["API"]
    param = {}

    # Trim the parameter to 256 characters . limitation of limit


    if "WIT" == config["EMOTION_HANDLER"]:
        api_url += "/message"
        param["access_token"] = config["WIT"]["TOKEN"]
        json_body["qparameter"]["query"] = json_body["qparameter"]["query"][-256:]
        param["q"] = json_body["qparameter"]["query"]

    elif "RASA" == config["EMOTION_HANDLER"]:
        api_url += "/parse"
        param["token"] = config["RASA"]["TOKEN"]
        param["q"] = json_body["qparameter"]["query"]

    logging.info(api_url)
    logging.info(param)
    response = requests.get(api_url, params=param)
    logging.info(response.url)
    json_resp = response.json()
    # Create a JSON representation of the resource
    resp.body = json.dumps(json_resp, ensure_ascii=True)

    # The following line can be omitted because 200 is the default
    # status returned by the framework, but it is included here to
    # illustrate how this may be overridden as needed.
    resp.status = falcon.HTTP_200
