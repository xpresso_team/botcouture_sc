__author__ = 'naveen'

import json
import falcon
import logging

from app.request_handlers.external_request import perform_generic_redirect
from app.request_handlers.personalization_handler import re_rank
from app.caching import cache_utils, cache_manager
from app.filter.generic_filter import filter_results
from app.config import config


def process(req, resp, json_body):
    """
    Process the vision based request.
    Query vision api and sort the result if required.
    """

    # Check if filtration is required
    cached_results, hash_key = cache_utils.get_cached_external_request(json_body)
    if cached_results is None:
        json_resp = perform_generic_redirect(base_uri=config["API"]["VISION"],
                                             json_body=json_body)

        cache_manager.insert_results_in_cache(hash_key, json_resp)

    else:
        logging.info("[CACHED]")
        json_resp = cached_results

    is_filtered_result_exist = False
    if "bot_vsi" in json_body["qcontext"]:
        is_filtered_result_exist = True
    elif "results" in json_resp:

        category_based_results = json_resp["results"]
        for category, result in category_based_results.items():
            if "gender_flag" in category or "tagged_img" in category or "upload_img" in category:
                continue
            # Filter results
            result["matches"] = filter_results(json_body["intent"], result["matches"], ['colorsavailable'])
            if not is_filtered_result_exist and len(result["matches"]) > 0:
                is_filtered_result_exist = True
            # Re-rank results
            result = re_rank(results=result["matches"], json_body=json_body)
            json_resp["results"][category]["matches"] = result

    if not is_filtered_result_exist:
        json_resp["status_code"] = -1
        json_resp["status_msg"] = "I could not find any products"
    # Create a JSON representation of the resource
    resp.body = json.dumps(json_resp, ensure_ascii=True)

    # The following line can be omitted because 200 is the default
    # status returned by the framework, but it is included here to
    # illustrate how this may be overridden as needed.
    resp.status = falcon.HTTP_200
