import datetime
import json
import requests
import logging
import requests_cache
import traceback

from app.config import config
from app import constants

requests_cache.install_cache('reranker')


# TODO Remove the need of this
# Because of different format that we get
# from different services, We will have to convert that into
# needed format
def check_if_array_format(results):
    if len(results) == 0:
        return False

    if (constants.XC_CATEGORY_KEY in results[0] and
            type(results[0][constants.XC_CATEGORY_KEY]) == str):
        return False
    else:
        return True


# TODO Remove the need of this
def change_to_str_format(results):
    for result in results:
        result[constants.XC_CATEGORY_KEY] = str(
            result[constants.XC_CATEGORY_KEY]) if constants.XC_CATEGORY_KEY in result else "0"
        result[constants.COLORS_AVAILABLE_KEY] = str(result[constants.COLORS_AVAILABLE_KEY]) if constants.COLORS_AVAILABLE_KEY in result else "0"
        result[constants.SIZE_KEY] = str(result[constants.SIZE_KEY]) if constants.SIZE_KEY in result else "0"
        result[constants.SCORE_KEY] = str(result[constants.SCORE_KEY]) if constants.SCORE_KEY in result else "0"
        result[constants.PRICE_KEY] = str(result[constants.PRICE_KEY]) if constants.PRICE_KEY in result else "0"


# TODO Remove the need of this
def change_to_array_format(results):
    for result in results:
        result[constants.XC_CATEGORY_KEY] = eval(
            result[constants.XC_CATEGORY_KEY]) if constants.XC_CATEGORY_KEY in result else []
        result[constants.COLORS_AVAILABLE_KEY] = eval(result[constants.COLORS_AVAILABLE_KEY]) if constants.COLORS_AVAILABLE_KEY in result else []
        result[constants.SIZE_KEY] = eval(result[constants.SIZE_KEY]) if constants.SIZE_KEY in result else []

        result[constants.SCORE_KEY] = str(result[constants.SCORE_KEY]) if constants.SCORE_KEY in result else "0"
        result[constants.PRICE_KEY] = str(result[constants.PRICE_KEY]) if constants.PRICE_KEY in result else ""


def re_rank(results, json_body):
    """
    Re ranks the services using personalization services
    :param results:
    :param json_body:
    :return:
    """

    if len(results) == 0:
        return results

    start_time = datetime.datetime.now()

    # TODO Remove the need of this
    # ranker service takes the data in format where arrays are converted to string
    # We have to convert the data accordingly
    is_result_array_format = check_if_array_format(results)

    if is_result_array_format:
        change_to_str_format(results)

    payload = {
        "is_diversify": True,
        "is_personalized": json_body["is_personalized"] if "is_personalized" in json_body else False,
        "psid": json_body["psid"],
        "page_id": json_body["page_id"],
        "channel_id": json_body["channel_id"],
        "Results": results
    }
    # logging.info(str(is_result_array_format))

    ignore_list = []
    for key, value in json_body["intent"].items():
        # If intent contains anything,
        # then ignore it during re ranking.
        # To be considered as hard filter
        if constants.XC_CATEGORY_KEY in key or constants.ENTITY_KEY in key or constants.SIZE_KEY in key:
            continue
        if value:
            ignore_list.append(key)

    payload["ignore"] = ignore_list

    headers = {
        'content-type': "application/json"
    }

    resp = requests.post(url=config["API"]["RE_RANKER_SERVICE"],
                         json=payload,
                         headers=headers)

    logging.info("Re ranking Time: [" + str(resp.elapsed.total_seconds()) + "]")
    logging.info(str(is_result_array_format))
    processed_results = []
    try:

        json_resp = resp.json()
        results = json_resp["Results"]
        if "Results" in json_resp:
            if is_result_array_format:
                results = json_resp["Results"]
                change_to_array_format(results)

            processed_results = results

    except:
        # traceback.print_exc()
        logging.warning("Invalid json format received from ranker service")
        if is_result_array_format:
            change_to_array_format(results)

        processed_results = results

    for result in processed_results:
        if constants.COLORS_AVAILABLE_KEY in result and not result[constants.COLORS_AVAILABLE_KEY]:
            del result[constants.COLORS_AVAILABLE_KEY]
        if constants.SIZE_KEY in result and not result[constants.SIZE_KEY]:
            del result[constants.SIZE_KEY]

    end_time = datetime.datetime.now()
    logging.info("Total ranking time: [" + str((end_time - start_time).microseconds / 1000) + "]")

    return processed_results
