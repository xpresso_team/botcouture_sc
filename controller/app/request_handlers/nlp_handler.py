__author__ = 'naveen'

import json
import falcon

from app.request_handlers.external_request import perform_generic_redirect
from app.request_handlers.personalization_handler import re_rank
from app.config import config


def process(req, resp, json_body):
    """
    Process the nlp based request.
    Query nlp api and sort the result if required.
    """

    json_resp = perform_generic_redirect(base_uri=config["API"]["NLP"],
                                         json_body=json_body)

    if "Results" in json_resp and len(json_resp["Results"]) > 0:
        json_resp["Results"] = re_rank(results=json_resp["Results"], json_body=json_body)
    if "Suggested Results" in json_resp and len(json_resp["Suggested Results"]) > 0:
        json_resp["Suggested Results"] = re_rank(results=json_resp["Suggested Results"], json_body=json_body)

    # Create a JSON representation of the resource
    resp.body = json.dumps(json_resp, ensure_ascii=True)

    # The following line can be omitted because 200 is the default
    # status returned by the framework, but it is included here to
    # illustrate how this may be overridden as needed.
    resp.status = falcon.HTTP_200
