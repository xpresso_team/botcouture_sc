"""
Constant variables and configuration
which is being used by relaxation module
"""
JSON_MIN_SEARCH_HIT_KEY = "min_search_hits"
JSON_STEPS = "steps"
ATTRIBUTE_TO_RELAX = "attributes_to_relax"
MIN_ATTRIBUTES_TO_CONSIDER = "min_attribute"
SOFT_FILTER = "soft_filter"
HARD_FILTER = "hard_filter"
ATTR_NAME_MAP = "attr_name_map"
MANDATORY_FIELD = "mandatory_field"

"""
Generic resource request
"""
QPARAMETER = "qparameter"


"""
Structured Search
"""
RESULTS_KEY = "Results"
RESULT_COUNT_KEY = "resultsCount"
PAGE_SIZE_KEY = "pageSize"
SUGGESTED_RESULT_KEY = "Suggested Results"

"""
Database
"""
XC_HIERARCHY_KEY = "xc_hierarchy_str"
XC_HIERARCHY_PIPE = "|"
PRICE_KEY = "price"
XC_CATEGORY_KEY = "xc_category"
COLORS_AVAILABLE_KEY = "colorsavailable"
SIZE_KEY = "size"
SCORE_KEY = "_score"
BRAND_KEY = "brand"
ENTITY_KEY = "entity"
GENDER_KEY = "gender"
PRODUCT_NAME_KEY = "productname"
CLIENT_NAME_KEY = "client_name"

"""
Price parser constants
"""
PRICE_GREATER_SIGN = ">"
PRICE_LESSER_SIGN = "<"
PRICE_BETWEEN_SIGN = "<>"
PRICE_EQUAL_SIGN = "="
