__author__ = 'naveen'

from app.resources.GenericResource import GenericResource
from app.resources.NLPSearchResource import NLPSearchResource


def setup(app):

    """
    Instantiate the supported resources
    """

    generic = GenericResource()
    app.add_route("/v1/generic/", generic)

    nlp = NLPSearchResource()
    app.add_route("/v1/search/", nlp)
