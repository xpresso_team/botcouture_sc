#!/usr/bin/env bash


if [ $# -eq 0 ]
  then
    echo "Give new version number as argument"
fi

NEW_VERSION=${1}
CURRENT_VERSION=`cat VERSION`

sed -i -e "s/${CURRENT_VERSION}/${NEW_VERSION}/g" VERSION
sed -i -e "s/${CURRENT_VERSION}/${NEW_VERSION}/g" dist/swagger.json
#sed -i -e "s/${CURRENT_VERSION}/${NEW_VERSION}/g" deploy/exp/Jenkinsfile
sed -i -e "s/${CURRENT_VERSION}/${NEW_VERSION}/g" deploy/stage/Jenkinsfile
