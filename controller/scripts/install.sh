#! /bin/bash

##################################
# Install falcon using Cython   #
##################################

# Install dependencies
sudo apt-get install build-essential python3-dev python3-pip

# Install cython for better performance
pip install cython

# Install ujson based on cython
pip install ujson

# Install falcon by compiling with cython
pip install --no-binary :all: falcon

# Install Gunicorn for production deployment
pip install gunicorn


