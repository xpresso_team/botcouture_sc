#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Run the application
export PYTHONPATH=${ROOT_FOLDER}
# python3 ${ROOT_FOLDER}/app/sparkStreaming.py

set -e

# #service nginx restart

# if [ "$1" = 'test-stage' ]; then
#     export CONTROLLER_SERVICE_CONFIG_FILE=./config/stg.json
#     pytest --cov=app --cov-report term-missing tests

# elif [ "$1" = 'stage' ]; then
export CONTROLLER_SERVICE_CONFIG_FILE=${ROOT_FOLDER}/config/stg.json
gunicorn app --env CONTROLLER_SERVICE_CONFIG_FILE=./config/stg.json \
    --workers 10 \
    --worker-class gthread \
    --threads 5 \
    --bind 0.0.0.0:8050

# elif [ "$1" = 'test-production' ]; then
#     export CONTROLLER_SERVICE_CONFIG_FILE=./config/prod.json
#     pytest --cov=app --cov-report term-missing tests

# elif [ "$1" = 'production' ]; then
#     gunicorn app --env CONTROLLER_SERVICE_CONFIG_FILE=./config/prod.json \
#     --workers $2 \
#     --worker-class gthread \
#     --threads $3 \
#     --bind 0.0.0.0:8050
    
# else
#     exec "$@"
# fi