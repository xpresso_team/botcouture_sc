
// @GENERATOR:play-routes-compiler
// @SOURCE:/opt/datalayer/conf/routes
// @DATE:Fri Oct 04 11:36:35 IST 2019

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:58
package ai.commerce.xpresso.v1.swagger.javascript {
  import ReverseRouteContext.empty

  // @LINE:58
  class ReverseSwaggerController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:58
    def getResource: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.swagger.SwaggerController.getResource",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "docs/swagger.json"})
        }
      """
    )
  
  }


}
