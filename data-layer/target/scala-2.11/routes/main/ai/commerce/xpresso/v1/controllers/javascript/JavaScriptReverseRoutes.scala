
// @GENERATOR:play-routes-compiler
// @SOURCE:/opt/datalayer/conf/routes
// @DATE:Fri Oct 04 11:36:35 IST 2019

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:9
package ai.commerce.xpresso.v1.controllers.javascript {
  import ReverseRouteContext.empty

  // @LINE:20
  class ReverseDynamoDBGet(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:20
    def getProduct: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.DynamoDBGet.getProduct",
      """
        function() {
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/catalog/product/"})
          }
        
        }
      """
    )
  
    // @LINE:26
    def scanAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.DynamoDBGet.scanAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/dynamodb/scan/"})
        }
      """
    )
  
  }

  // @LINE:15
  class ReverseCloudSearchGet(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:18
    def structuredSearchAPI: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.CloudSearchGet.structuredSearchAPI",
      """
        function() {
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/catalog/structuredSearch/"})
          }
        
        }
      """
    )
  
    // @LINE:15
    def genericAPI: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.CloudSearchGet.genericAPI",
      """
        function() {
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/catalog/genericAPI/"})
          }
        
        }
      """
    )
  
  }

  // @LINE:21
  class ReverseCatalogPut(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:21
    def insertBatch: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.CatalogPut.insertBatch",
      """
        function() {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/catalog/product/"})
        }
      """
    )
  
  }

  // @LINE:30
  class ReverseDynamoDBPost(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:30
    def updateProduct: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.DynamoDBPost.updateProduct",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/dynamodb/product/"})
        }
      """
    )
  
  }

  // @LINE:23
  class ReverseCatalogDelete(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:23
    def deleteProduct: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.CatalogDelete.deleteProduct",
      """
        function() {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/catalog/product/"})
        }
      """
    )
  
  }

  // @LINE:42
  class ReverseUtilityGet(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:42
    def getBrand: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.UtilityGet.getBrand",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/util/brand/"})
        }
      """
    )
  
  }

  // @LINE:55
  class ReversePromotionsGet(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:55
    def getTodaysPromotions: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.PromotionsGet.getTodaysPromotions",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/promotions/todayuser/"})
        }
      """
    )
  
  }

  // @LINE:39
  class ReverseCloudSearchPut(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:39
    def insertItem: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.CloudSearchPut.insertItem",
      """
        function() {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/cloudsearch/put/"})
        }
      """
    )
  
  }

  // @LINE:45
  class ReverseBrandsOnRedis(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:46
    def getRedisBrandList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.BrandsOnRedis.getRedisBrandList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/redis/getBrands"})
        }
      """
    )
  
    // @LINE:45
    def updateBrand: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.BrandsOnRedis.updateBrand",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/redis/updateBrands"})
        }
      """
    )
  
    // @LINE:47
    def updateDeltaUpdateForBrand: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.BrandsOnRedis.updateDeltaUpdateForBrand",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/redis/updateDeltaBrands"})
        }
      """
    )
  
  }

  // @LINE:29
  class ReverseDynamoDBPut(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:29
    def insertBatch: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.DynamoDBPut.insertBatch",
      """
        function() {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/dynamodb/product/"})
        }
      """
    )
  
  }

  // @LINE:22
  class ReverseCatalogPost(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:22
    def updateProduct: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.CatalogPost.updateProduct",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/catalog/product/"})
        }
      """
    )
  
  }

  // @LINE:38
  class ReverseCloudSearchDelete(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:38
    def deleteProduct: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.CloudSearchDelete.deleteProduct",
      """
        function() {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/cloudsearch/genericAPI/"})
        }
      """
    )
  
  }

  // @LINE:9
  class ReverseElasticSearchGet(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:10
    def structuredSearchAPI: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.ElasticSearchGet.structuredSearchAPI",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/elasticsearch/structuredSearch/"})
        }
      """
    )
  
    // @LINE:9
    def genericAPI: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.ElasticSearchGet.genericAPI",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/elasticsearch/genericAPI/"})
        }
      """
    )
  
  }

  // @LINE:31
  class ReverseDynamoDBDelete(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:31
    def deleteProduct: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.DynamoDBDelete.deleteProduct",
      """
        function() {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/dynamodb/product/"})
        }
      """
    )
  
  }

  // @LINE:50
  class ReverseCollectionsGet(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:50
    def l1: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.CollectionsGet.l1",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/collections/l1/"})
        }
      """
    )
  
    // @LINE:51
    def l2: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.CollectionsGet.l2",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/collections/l2/"})
        }
      """
    )
  
    // @LINE:52
    def l3: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "ai.commerce.xpresso.v1.controllers.CollectionsGet.l3",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "v1/collections/l3/"})
        }
      """
    )
  
  }


}
