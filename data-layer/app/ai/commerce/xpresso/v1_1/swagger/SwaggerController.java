package ai.commerce.xpresso.v1_1.swagger;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import play.Environment;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by naveen on 17/5/17.
 */
public class SwaggerController extends Controller {

    private final Environment env;

    @Inject
    public SwaggerController(final Environment env) {
        // the environment is used to access local files
        this.env = env;
    }
    /**
     * Return json resource file.
     *
     * @return
     */
    public Result getResource() {

        try (InputStream is = env.resourceAsStream("/swagger.json")) {
            final JsonNode json = Json.parse(is);
            return ok(json);
        } catch (IOException e) {
            return internalServerError("Something went wrong");
        }
    }
}
