package ai.commerce.xpresso.v1_1.size;

import ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB;
import ai.commerce.xpresso.v1_1.size.Size;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  Created by naveen on 8/6/17.
 *
 *  Singleton class
 *  Stores the size chart from the database.
 *
 *  Size chart contains following sizes:
 *    1) Standard Size
 *    2) US No ( Shoe & Apparel )
 *    3) Waist
 *    4) Bust
 *    5) Hip
 *    6) UK No ( Shoe )
 *    7) EU NO ( Shoe )
 *
 *
*/
public class SizeChart {
    /**
     * Private static instance
     */
    static private SizeChart mInstance = null;

    /**
     * Amazon db instance
     */
    private ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB mAmazonDynamoDB = null;


    /**
     * Size Chart Data
     * <size_key>:[ <type,size_letter/us no/waist/bust/hip/uk/eu> ]
     */
    private HashMap<String, List<ai.commerce.xpresso.v1_1.size.Size>> mSizeData = new HashMap<>();


    /**
     * Map that tells us what category falls under which higher category
     */
    private HashMap<String, List<String>> higherCategory = new HashMap<>();
    /**
     * Application configuration
     */
    private Configuration mConfiguration = null;

    private SizeChart(Configuration configuration) {
        mConfiguration = configuration;
    }

    /**
     * Get singleton instance
     */
    public static SizeChart getInstance(Configuration configuration){
        if( mInstance==null){
            mInstance = new SizeChart(configuration);
            mInstance.init(configuration);
        }
        return mInstance;
    }

    /**
     * Get singleton instance. Make sure you call {@link SizeChart#getInstance}
     */
    public static SizeChart getInstance(){
        return mInstance;
    }

    /**
     *
     * Converts sizes into a standard size.
     *
     * When a size is provided, it refers the size chart
     * and try to matches the size with value given in
     * size chart.
     *
     * Size match happens in following order:
     *   1) Letter Size ( String )
     *   2) Size No ( Min < Number <= Max )
     *   3) Chest Size ( Min < Number <= Max )
     *   4) Bust Size ( Min < Number <= Max )
     *   5) Hip Size ( Min < Number <= Max )
     *
     * Size parameter is a String, First we try to match the string.
     * If not mathed, we pick the first number from it, refer to size chart
     * and then fetches the standard size corresponding.
     *
     * @param originalSize original size
     * @return converted standard size
     */
    public String convertSize(String originalSize, String category) {

        String xcCategory = getHigherCategory(category);

        if( !mSizeData.containsKey(xcCategory)){
            return "";
        }
        // Check if number then compare it as a number
        boolean isDouble = false;
        try{
            double sizeD = Double.valueOf(originalSize);
            isDouble = true;
        }catch (NumberFormatException e){
            Logger.warn("Size is not a double. Check if string");
            isDouble = false;
        }

        String convertedSize = "";
        if( isDouble ){
            convertedSize = getNumberMatch(originalSize, xcCategory);
        }else{
            convertedSize = getLetterMatch(originalSize, xcCategory);
        }
        Logger.info("Best Match"+convertedSize);
        return convertedSize;

    }

    // Extract the number from the original size and search in given size chart
    private String getNumberMatch(String originalSize, String xcCategory) {
        String bestMatch = "";
        List<ai.commerce.xpresso.v1_1.size.Size> allSizes = mSizeData.get(xcCategory);
        // Extract double value from this string
        Pattern p = Pattern.compile("(\\d+\\.*\\d*)");
        Matcher m = p.matcher(originalSize);
        double extractedDouble = 0;
        while(m.find()) {
            extractedDouble = Double.parseDouble(m.group(1));
        }
        Logger.info("Extract double-"+extractedDouble+" from-"+originalSize );

        ai.commerce.xpresso.v1_1.size.Size match = doMatching(extractedDouble, allSizes);
        if( match!= null){
            bestMatch= match.mSizeLetter;
        }
        return bestMatch;
    }

    // Match every possible size to get the possible Match;
    public ai.commerce.xpresso.v1_1.size.Size doMatching(double extractedDouble, List<ai.commerce.xpresso.v1_1.size.Size> allSizes) {
        ai.commerce.xpresso.v1_1.size.Size match = null;
        // TODO This is two for loops. How can we make it better?
        for(String key : ai.commerce.xpresso.v1_1.size.Size.mSizeFields) {
            for(ai.commerce.xpresso.v1_1.size.Size size: allSizes) {
                try {
                    if (size.mData.has(key + "_min") &&
                            size.mData.getInt(key + "_min") <= extractedDouble &&
                            extractedDouble < size.mData.getInt(key + "_max")) {

                        match = size;
                        Logger.info("Matched for-" + key + "-" + extractedDouble);
                        break;
                    } else {
                        Logger.info("Did not match for -" + key + "-" + extractedDouble);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if( match!=null){
                break;
            }
        }
        return match;
    }

    // We will check the match for alpha sizes.
    // Count the prefix match count and assign string with maximum match
    private String getLetterMatch(String originalSize, String xcCategory) {
        originalSize= originalSize.replaceAll(" ","");
        originalSize= originalSize.replaceAll("-","");

        int maxCount = 0;
        String bestMatch = "";

        List<ai.commerce.xpresso.v1_1.size.Size> allSizes = mSizeData.get(xcCategory);
        for( ai.commerce.xpresso.v1_1.size.Size size: allSizes) {
            int matchCount = size.getMatchCount(originalSize);
            if( maxCount < matchCount){
                maxCount = matchCount;
                bestMatch = size.mSizeLetter;
            }
        }

        return bestMatch;
    }


    /**
     * Most of the category has a higher category in which
     * it's size can be included
     * @param category
     * @return
     */
    private String getHigherCategory(String category) {
        if(category.equalsIgnoreCase("footwear")){
            return "footwear";
        }
        return "apparel";
    }

    /**
     * /**
     * Convert Size to standar format .{@link SizeChart#convertSize }
     *
     * @param originalSize
     * @return
     */
    public String convertSize(String originalSize) {
        return convertSize(originalSize, "apparel");
    }


    /**
     * Fetch size chart from database
     * @param configuration configuration file
     */
    private void init(Configuration configuration){

        mAmazonDynamoDB = new ai.commerce.xpresso.v1_1.amazon.dynamodb.AmazonDynamoDB(configuration);

        String tableName = configuration.getString("datalayer.dynamodb.default_size_chart_table");
        Table table = null;
        try {
            table = mAmazonDynamoDB.mDynamoDB.getTable(tableName);
        } catch (IllegalArgumentException e) {
            Logger.info(e.getMessage());
            table = null;
        }

        if ( table == null){
            // Nothing can be done here. Size chart wont work.
            // Raise warning and exit.
            Logger.warn("Size chart could not be retrieved using tableName ["+tableName+"]");
            return;
        }

        ScanSpec scanSpec = new ScanSpec();

        try {
            ItemCollection<ScanOutcome> items = table.scan(scanSpec);
            Iterator<Item> iter = items.iterator();
            // Parse the db item into mSizeData
            while (iter.hasNext()) {
                Item item = iter.next();
                JSONObject itemJson = new JSONObject(item.toJSON());
                String key = itemJson.getString("xc_category");
                ai.commerce.xpresso.v1_1.size.Size newSize = new ai.commerce.xpresso.v1_1.size.Size(itemJson);
                if(mSizeData.containsKey(key)){
                    mSizeData.get(key).add(newSize);
                }else{
                    List<ai.commerce.xpresso.v1_1.size.Size> newSizeList = new ArrayList<>();
                    newSizeList.add(newSize);
                    mSizeData.put(key, newSizeList);
                }
            }
        } catch (Exception e) {
            Logger.error("Unable to scan the table:");
            Logger.error(e.getMessage());
        }
    }
}