package ai.commerce.xpresso.v1_1.controllers;

import ai.commerce.xpresso.v1_1.utils.ErrorHandler;
import ch.qos.logback.core.status.ErrorStatus;
import com.google.inject.Inject;
import io.swagger.annotations.*;
import org.apache.http.HttpStatus;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Logger;
import play.db.Database;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

/**
 * Created by naveen on 16/5/17.
 */

@Api(value = "Promotions GET", produces = "application/json", tags= {"promotions"})
public class PromotionsGet extends Controller {

    private Database mdb;

    @Inject
    public PromotionsGet(Database db) {
        this.mdb = db;
    }


    /**
     * Returns user Id for those users for whom
     * promotions needs to be send.
     *
     * Get list of all promotions who has start_date equal to current date.
     * Get list of all users from user table and send promotions for each user.
     *
     *
     * @return json result of all users
     */
    public Result getTodaysPromotions(){
        Connection connection =  mdb.getConnection();
        if(connection == null){
            return ok(ai.commerce.xpresso.v1_1.utils.ErrorHandler
                        .generateError(500, "fail", "database connection went wrong")
                        .toString())
                    .as("application/json");
        }

        int page = 0;
        int pageSize = 1000;
        final Set<Map.Entry<String, String[]>> entries = request().queryString().entrySet();

        for (Map.Entry<String, String[]> entry : entries) {
            if( entry.getKey().equalsIgnoreCase("page") &&
                    entry.getValue().length > 0){
                try {
                    page = Integer.parseInt(entry.getValue()[0]);
                }catch (NumberFormatException e){
                    Logger.warn(e.getMessage());
                }
            }else if( entry.getKey().equalsIgnoreCase("pageSize") &&
                    entry.getValue().length > 0){
                try {
                    pageSize = Integer.parseInt(entry.getValue()[0]);
                }catch (NumberFormatException e){
                    Logger.warn(e.getMessage());
                }
            }
            Logger.info(entry.getKey() + "-" + entry.getValue()[0]);
        }

        // Get all promotions
        JSONArray todaysPromotions = new JSONArray();
        try {
            PreparedStatement pStatement = connection.prepareStatement(
                    "SELECT * from promotions as p WHERE p.start_date = CURDATE()");

            ResultSet result = pStatement.executeQuery();
            while( result.next()){
                JSONObject promo = new JSONObject();
                promo.put("coupon_code", result.getString("coupon_code"));
                promo.put("coupon_name", result.getString("coupon_name"));
                promo.put("promotion_url", result.getString("promotion_url"));
                promo.put("promotion_image", result.getString("promotion_image"));
                promo.put("promotion_type", result.getString("promotion_type"));
                promo.put("start_date", result.getString("start_date"));
                promo.put("end_date", result.getString("end_date"));
                todaysPromotions.put(promo);
            }
        } catch (SQLException | JSONException e) {
            Logger.info(e.getMessage());
        }

        Logger.info(todaysPromotions.toString());

        JSONArray result = new JSONArray();
        if( todaysPromotions.length() > 0) {

            //Get all users
            try {
                StringBuilder query = new StringBuilder();
                query.append("SELECT * from user as u WHERE ")
                        .append(" u.channel_id ")
                        .append(" = ")
                        .append("'facebook'")
                        .append(" LIMIT ")
                        .append(page).append(",").append(pageSize);
                Logger.info(query.toString());
                PreparedStatement uStatement = connection.prepareStatement(query.toString());

                ResultSet resultStatement = uStatement.executeQuery();

                while (resultStatement.next()) {
                    JSONObject user = new JSONObject();
                    user.put("psid", resultStatement.getString("psid"));
                    user.put("channel_id", resultStatement.getString("channel_id"));
                    user.put("first_name", resultStatement.getString("first_name"));
                    user.put("last_name", resultStatement.getString("last_name"));
                    user.put("gender", resultStatement.getString("gender"));
                    user.put("promotions", todaysPromotions);
//                    Logger.info(user.toString());
                    result.put(user);
                }


            } catch (SQLException | JSONException e) {
                Logger.info(e.getMessage());
            }
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        JSONObject finalResults = new JSONObject();
        try {
            finalResults.put("Results",result);
            finalResults.put("ResultCount",result.length());
            finalResults.put("code",200);
            finalResults.put("status", "ok");
            finalResults.put("msg", "ok");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ok(finalResults.toString()).as("application/json");
    }
}