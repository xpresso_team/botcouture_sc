package ai.commerce.xpresso.v1_1.controllers;

import ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearch;
import ai.commerce.xpresso.v1_1.search.SearchItem;
import ai.commerce.xpresso.v1_1.search.XCCloudSearchResult;
import ai.commerce.xpresso.v1_1.utils.XCJSON;
import com.google.inject.Inject;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.*;

/**
 * Created by naveen on 1/5/17.
 *
 * Handles all GET request for the AWS CLoud Search.
 * Uses AWS SDK to create a structured query and perform search
 *
 * It has following api:
 *  1) Generic API
 */

public class CloudSearchDelete extends Controller {
    @Inject
    private Configuration mConfiguration;

    private AmazonCloudSearch mCloudSearch  ;

    private String FAIL_STATUS = "fail";

    @Inject
    public CloudSearchDelete(Configuration configuration){
        mCloudSearch = new AmazonCloudSearch(configuration);
    }
    /**
     * Performs search on multiple fields in cloud search and
     * return field can contain any fields or complete product details
     *
     * @return Results
     */

    public Result deleteProduct() {

        // Few search configurations.
        String[] returnField = {
                mConfiguration.getString("datalayer.cloudsearch.default_return_field")
        };
        boolean productDetails = false;

        String cloudSearchDomain = mConfiguration.getString("datalayer.cloudsearch.default_domain");

        // Map to store filtering parameter.
        // These parameters are used for filtering in cloudsearch reqeuest
        Map<String, Set<String>> filterMap = new HashMap<String, Set<String>>();

        // Iterating over provided parameters.
        // Identifies the requirement of request.
        // Fetch all results into a map
        final Set<Map.Entry<String, String[]>> entries = request().queryString().entrySet();
        for (Map.Entry<String, String[]> entry : entries) {
            if (entry.getKey().equals(mConfiguration.getString("datalayer.parameters.csdomain")) &&
                    entry.getValue().length > 0) {
                cloudSearchDomain = entry.getValue()[0];
            } else {
                Set<String> setValues = new HashSet<String>(Arrays.asList(entry.getValue()));
                filterMap.put(entry.getKey(), setValues);
            }
            Logger.info(entry.getKey() + "-" + entry.getValue()[0]);
        }

        XCCloudSearchResult xcCloudSearchResult = mCloudSearch.getSearchItems(returnField,
                cloudSearchDomain,
                filterMap);
        List<SearchItem> resultList = xcCloudSearchResult.getResults();


        JSONObject finalResult = new JSONObject();
        try {
            XCJSON objXCJSON = new XCJSON();
            JSONObject returnValues = objXCJSON.searchItemToJSON(resultList, productDetails);
            Logger.info(returnValues.toString());
            if( returnValues.has("xc_sku")){

                JSONArray matchedSKU = returnValues.getJSONArray("xc_sku");
                List<String> skuList = new ArrayList<>();
                for( int i =0 ; i < matchedSKU.length(); ++i){
                    skuList.add(matchedSKU.getString(i));
                }
                mCloudSearch.deleteItems(skuList);
            }
            finalResult.put("code",200);
            finalResult.put("status","ok");
            finalResult.put("msg","ok");
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.error(e.getMessage());
        }

        return ok(finalResult.toString()).as("application/json");
    }
}
