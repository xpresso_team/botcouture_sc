package ai.commerce.xpresso.v1_1.amazon.cloudsearch;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import ai.commerce.xpresso.v1_1.utils.XCLogger;
import org.slf4j.Logger;

public class PriceParser {

	public static final String LESS_ATTRIBUTE = "less";
	public static final String GREATER_ATTRIBUTE = "greater";
	public static final String BETWEEN_ATTRIBUTE = "between";
	public static final String LESSER_SIGN = "<";
	public static final String GREATER_SIGN = ">";
	public static final String BETWEEN_SIGN = "<>";
	public static final String EQUALS_SIGN = "=";

	private static Map<String, String> qualifierMap = new LinkedHashMap<String, String>();
	private static Map<String, String> ambiguousQualifierMap = new LinkedHashMap<String, String>();
	private static Map<String, String> multiplierMap = new LinkedHashMap<String, String>();
	private static Logger logger;

	private static Map<String, String[]> numberTextMap = new LinkedHashMap<String, String[]>();

	public static String[] getNumberForText(String text) {
		return numberTextMap.get(text);
	}

	public static void init() {
		logger = XCLogger.getSpLogger();

	}

	/**
	 * The method returns price qualifier tags from the identified price tags
	 * @param valueSet
	 * @return
	 */
	public static String getPriceQualifier(Set<String> valueSet) {
		for (String value : valueSet) {
			value = value.trim();
			if (value.startsWith(ai.commerce.xpresso.v1_1.amazon.cloudsearch.PriceParser.BETWEEN_SIGN))
				return ai.commerce.xpresso.v1_1.amazon.cloudsearch.PriceParser.BETWEEN_SIGN;
			else if (value.startsWith(ai.commerce.xpresso.v1_1.amazon.cloudsearch.PriceParser.LESSER_SIGN))
				return ai.commerce.xpresso.v1_1.amazon.cloudsearch.PriceParser.LESSER_SIGN;
			else if (value.startsWith(ai.commerce.xpresso.v1_1.amazon.cloudsearch.PriceParser.GREATER_SIGN))
				return ai.commerce.xpresso.v1_1.amazon.cloudsearch.PriceParser.GREATER_SIGN;
			else if (value.startsWith(ai.commerce.xpresso.v1_1.amazon.cloudsearch.PriceParser.EQUALS_SIGN))
				return ai.commerce.xpresso.v1_1.amazon.cloudsearch.PriceParser.EQUALS_SIGN;
		}
		return "";
	}

	public static String getPriceValue(Set<String> valueSet) {
		for (String value : valueSet) {
			value = value.trim().replaceAll("[^\\d\\.]+", " ");
			value = value.replaceAll("[\\s]+", " ");
			return value.trim();
		}
		return "";

	}

	/**
	 * The method checks if the given word is a price qualifier or not
	 * @param word
	 * @return
	 */
	public static String isPriceQualifier(String word) {
		word = word.toLowerCase();
		if (qualifierMap.containsKey(word))
			return qualifierMap.get(word);
		for (Entry<String, String> entry : qualifierMap.entrySet()) {
			if (word.matches(entry.getKey()))
				return entry.getValue();
		}
		return null;
	}

	public static String isAmbiguousPriceQualifier(String word) {
		word = word.toLowerCase();
		if (ambiguousQualifierMap.containsKey(word))
			return ambiguousQualifierMap.get(word);
		for (Entry<String, String> entry : ambiguousQualifierMap.entrySet()) {
			if (word.matches(entry.getKey()))
				return entry.getValue();
		}
		return null;
	}

}
