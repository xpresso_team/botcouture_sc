package ai.commerce.xpresso.v1.data;

import ai.commerce.xpresso.v1.amazon.cloudsearch.XCConfig;
import ai.commerce.xpresso.v1.amazon.dynamodb.AmazonDynamoDB;
import org.codehaus.jettison.json.JSONObject;
import play.Configuration;

/**
 * Created by naveen on 11/5/17.
 *
 * Constanst class to store data related constants
 */
public class Constants {

    public static final String EMPTY_STRING = "";
    public static final String SPACE_STRING = " ";

    public static final String PRICE_KEY = "price";
    public static final String PRICE_RANGE_KEYWORD = "<>";
    public static final String PRICE_LESS_THAN_KEYWORD = "<";
    public static final String PRICE_GREATER_THAN_KEYWORD = ">";



}