package ai.commerce.xpresso.v1.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import ai.commerce.xpresso.v1.search.SearchItem;
import ai.commerce.xpresso.v1.search.SearchUtils;

/**
 * Each instance of this class represents a single entity (i.e. item in the query) with its attribute.
 * This instance also holds the retrieved items (from catalog) corresponding to its entity.
 *
 * @author Alix Melchy Sep 1, 2016
 *
 */
public class XCEntity {
	private static Logger logger = XCLogger.getSpLogger();
	private Set<String> mainEntityList;
	private Set<String> topEntityList;
	private String entityStr;
	private String relaxedEntityStr;
	private String domain;

	private String govWord;
	private String multiWordEntity;
	private List<String> wordVectorCategories = null;

	private JSONObject intent;
	private JSONObject clarification;
	private JSONArray consumerSubCategory;

	private List<SearchItem> simpleSearchResults;
	private List<SearchItem> structuredSearchResults;

	private boolean directSearchOnly = false;

	private Map<String, Double> skuConfidenceMap = new HashMap<String, Double>();
	private Map<String, Set<String>> attributeMap = new LinkedHashMap<String, Set<String>>();
	public Map<String, Set<String>> relaxationMap = new LinkedHashMap<String, Set<String>>();

	private XCQuery query;

	/**
	 * Constructor: creates an instance of this class with the entity string and the query
	 * the entity is extracted from. The boolean argument controls the search for semantically
	 * similar entities (no such search when set to true).
	 * @param entityStr
	 * @param query
	 * @param isChildEntity
	 */
	public XCEntity(String entityStr, XCQuery query, boolean isChildEntity) {
		this.entityStr = entityStr.toLowerCase();
		this.query = query;
		this.intent = new JSONObject();
		this.clarification = new JSONObject();
		this.simpleSearchResults = new ArrayList<SearchItem>();
		this.structuredSearchResults = new ArrayList<SearchItem>();
		/*If not childEntity, then find semantically similar entities*/
		//		if (!isChildEntity)
		//			this.getSemanticSimilarEntities();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Entity: " + entityStr);
		sb.append(" - govWord: " + govWord);
		sb.append(" - multiWordEntity: " + multiWordEntity);
		sb.append(" - mainEntityList: " + (mainEntityList == null ? "null" : mainEntityList.toString()));
		sb.append(" - topEntityList: ");
		sb.append((this.topEntityList == null) ? "null" : topEntityList.toString());
		return sb.toString();
	}

	public JSONArray getConsumerSubCategory() {
		return this.consumerSubCategory;
	}

	/* used in Structure search. Since we don't call processEntity method in structureSearch API, this remains unchanged and needs to be set explicitly*/
	public void setConsumerSubCategory(JSONArray consumerSubCategory) {
		this.consumerSubCategory = consumerSubCategory;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @param intent
	 */
	public void setIntent(JSONObject intent) {
		this.intent = intent;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @return intent
	 */
	public JSONObject getIntent() {
		return intent;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @param clarification
	 */
	public void setClarification(JSONObject clarification) {
		this.clarification = clarification;
	}

	/**
	 * This intent is a JSON representing identified entities and attributes in this query.
	 * @return intent
	 */
	public JSONObject getClarification() {
		return clarification;
	}

	/**
	 * @return query of this object
	 */
	public XCQuery getQuery() {
		return query;
	}

	/**
	 * Sets this multiWordEntity; updates only to longer multiWordEntity
	 * @param multiWordEntity
	 */
	public void setMultiWordEntity(String multiWordEntity) {
		if (this.multiWordEntity == null || multiWordEntity.length() > this.multiWordEntity.length())
			this.multiWordEntity = multiWordEntity;
	}

	/**
	 * Sets this multiWordEntity; if toShorter, updates even in case of a shorter multiWordEntity
	 * @param multiWordEntity
	 * @param toShorter
	 */
	public void setMultiWordEntity(String multiWordEntity, boolean toShorter) {
		if (!toShorter)
			setMultiWordEntity(multiWordEntity);
		else
			this.multiWordEntity = multiWordEntity;
	}

	/**
	 * Sets this govWord
	 * @param govWord
	 */
	public void setGovWord(String govWord) {
		this.govWord = govWord;
	}

	/**
	 * Sets this directSearch; if set to true, no expansion of the initial search when retrieving
	 * relevant items from the catalog.
	 * @param value
	 */
	public void setDirectSearchOnly(boolean value) {
		this.directSearchOnly = value;
	}

	/**
	 * Adds the attribute value for this attribute key in the attribute map.
	 * Values are stored in a set.
	 * Updates this multi-entity when necessary.
	 * @param attributeKey
	 * @param attributeValue
	 */
	public void setAttribute(String attributeKey, String attributeValue) {
		if (attributeValue != null) {
			Set<String> valueSet = this.attributeMap.get(attributeKey);
			if (valueSet == null)
				valueSet = new HashSet<String>();
			valueSet.add(attributeValue);
			this.attributeMap.put(attributeKey, valueSet);

			if (attributeKey.equals(SearchUtils.GENERIC_KEY))
				setMultiWordEntity(this.entityStr);
			this.entityStr = this.entityStr.replaceAll("\\b" + attributeValue + "\\b", "");
			logger.info("Updated entity: " + this.toString());
			logger.info("Attribute key: " + attributeKey + "\tattributeValue: " + attributeValue + " new value: " + this.attributeMap.get(attributeKey));
		}
	}

	/**
	 * Adds this whole set of attribute values for this attribute key in the attribute map.
	 * @param attributeKey
	 * @param attributeValueSet
	 */
	public void setAttribute(String attributeKey, Set<String> attributeValueSet) {
		if (attributeValueSet != null) {
			Set<String> valueSet = this.attributeMap.get(attributeKey);
			if (valueSet == null)
				valueSet = new HashSet<String>();
			valueSet.addAll(attributeValueSet);
			this.attributeMap.put(attributeKey, valueSet);
			for (String attributeValue : attributeValueSet) {
				/*Remove any attribute like color, theme, etc. present in entity string except if it is in subcategory
				 * eg- query shirts have subcategory shirts which will make entity string empty*/
				String tempEntityStr = this.entityStr;
				this.entityStr = this.entityStr.replaceAll("\\b" + attributeValue + "\\b", "");
				if (this.entityStr.isEmpty() || this.entityStr == "")
					this.entityStr = tempEntityStr;
			}
		}
	}

	/**
	 * Adds to this attribute map the attributes and their value from that other entity.
	 * @param otherEntity
	 */
	public void addAttribute(XCEntity otherEntity) {
		Map<String, Set<String>> otherAttributeMap = otherEntity.getAttributeMap();
		for (Map.Entry<String, Set<String>> entry : otherAttributeMap.entrySet()) {
			String attributeKey = entry.getKey();
			Set<String> attributeValues = entry.getValue();
			Set<String> valueSet = this.attributeMap.get(attributeKey);
			if (valueSet == null)
				valueSet = new HashSet<String>();
			valueSet.addAll(attributeValues);
			this.attributeMap.put(attributeKey, valueSet);
		}
	}

	/**
	 * @return whether this entity has any attribute
	 */
	public boolean hasAttribute() {
		return !attributeMap.isEmpty();
	}

	/**
	 * @param attributeKey
	 * @return value set for this attribute
	 */
	public Set<String> getAttribute(String attributeKey) {
		return this.attributeMap.get(attributeKey);
	}

	/**
	 * @param entityStr
	 */
	public void setEntityStr(String entityStr) {
		this.entityStr = entityStr;
	}

	/**
	 * @return this entity string
	 */
	public String getEntityStr() {
		return entityStr;
	}

	/**
	 * @param entityStr
	 */
	public void setRelaxedEntityStr(String entityStr) {
		this.relaxedEntityStr = entityStr;
	}

	/**
	 * @return this relaxed entity string due to change in search controller
	 */
	public String getRelaxedEntityStr() {
		return relaxedEntityStr;
	}

	/*
	 * For APIs when we don't want to create SpQuery or doesn't need to go through the NLP pipeline
	 * SpEntity need to have domain it is querying for.
	 */
	public String getDomain() {
		if (this.getQuery() != null)
			return this.getQuery().getDomainName();
		else
			return this.domain;
	}

	public void setDomain(String domain) {
		if (this.getQuery() != null)
			this.domain = this.getQuery().getDomainName();
		else
			this.domain = domain;
	}

	/**
	 * @return this multi-word entity if it exists, otherwise this entity string
	 */
	public String getMultiWordEntity() {
		if (multiWordEntity == null)
			return entityStr;
		return multiWordEntity;
	}

	/**
	 * @return confidence map of retrieved items
	 */
	public Map<String, Double> getSkuConfidenceMap() {
		return skuConfidenceMap;
	}

	/**
	 * @return this attribute map
	 */
	public Map<String, Set<String>> getAttributeMap() {
		return attributeMap;
	}

	/**
	 * @return structured search results
	 */
	public List<SearchItem> getStructuredSearchResults() {
		return structuredSearchResults;
	}

	/**
	 * @return results from an unstructured search (no field specified)
	 */
	public List<SearchItem> getSimpleSearchResults() {
		return simpleSearchResults;
	}

	/**
	 * This entity can refer to all categories when no precise and definite entity is extracted
	 * from this query.
	 * @return whether the entity is not-defined, hence triggering a search across all categories
	 */
	public boolean hasAllCategoriesAttribute() {
		return entityStr.equals(SearchUtils.ALL_CATEGORIES);
	}

	/**
	 * @return whether to limit the search to the initial search without relaxing conditions
	 */
	public boolean getDirectSearchOnly() {
		return directSearchOnly;
	}

	/**
	 * Top subcategories are used to focus results.
	 * E.g. 'dress shirt' should only be searched amidst dress shirts or formal shirts.
	 * @return top matching subcategories
	 */
	public Set<String> getTopEntityList() {
		return topEntityList;
	}

	public void setTopEntityList(Set<String> topEntityList) {
		this.topEntityList = topEntityList;
	}

	/**
	 * @return the category of this entity using word vector embedding
	 */
	public List<String> getWordVectorCategories() {
		return wordVectorCategories;
	}

	/**
	 * @param wordVectorCategories
	 */
	public void setWordVectorCategories(List<String> wordVectorCategories) {
		this.wordVectorCategories = wordVectorCategories;
	}

	public String getGovWord() {
		return govWord;
	}

	/**
	 * @param simpleSearchResults
	 */
	public void setSimpleSearchResults(List<SearchItem> simpleSearchResults) {
		this.simpleSearchResults = simpleSearchResults;
	}

	/**
	 * @param structuredSearchResults
	 */
	public void setStructuredSearchResults(List<SearchItem> structuredSearchResults) {
		this.structuredSearchResults = structuredSearchResults;
	}

	/**
	 * @return the list of all the subcategories (catalog) relevant to this entity
	 */
	public Set<String> getMainEntityList() {
		return mainEntityList;
	}

	public void setMainEntityList(Set<String> mainEntityList) {
		this.mainEntityList = mainEntityList;
	}

	/**
	 * Retrieves semantically similar terms to this entity and stores it in this instance.
	 * Current implementation: based on small graph database
	 */
	//	private void getSemanticSimilarEntities() {
	//		if (this.mainEntityList != null) {
	//			for (String mainEntity : this.mainEntityList) {
	//				Set<String> similarEntities = DomainKnowledgeBase.getSimilarEntities(mainEntity);
	//				if (similarEntities != null) {
	//					logger.info("Semantically Similar Entities of " + mainEntity + "\t" + similarEntities);
	//					for (String similarEntity : similarEntities) {
	//						XCEntity e = new XCEntity(similarEntity, query, true);
	//						this.query.addToEntityMap(similarEntity, e);
	//					}
	//				}
	//			}
	//		}
	//	}

}
