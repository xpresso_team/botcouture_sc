package ai.commerce.xpresso.v1.search;

import java.util.*;
import java.util.Map.Entry;

import ai.commerce.xpresso.v1.amazon.cloudsearch.Hit;
import ai.commerce.xpresso.v1.amazon.cloudsearch.XCConfig;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * The class contains the results of search on the Amazon CloudSearch
 * @author antarip.biswas
 *
 */
public class SearchItem {

	/**
	 * score is the search relevance score obtained from the CloudSearch
	 */

	private Map<String, String> attributeMap = new LinkedHashMap<String, String>();

    public SearchItem(Hit amazonSearchResult) {
		Map<String, String> currentItemMap = amazonSearchResult.fields;

		for (Entry<String, String> entry : currentItemMap.entrySet()) {

			String keyStr = entry.getKey();
			String valueStr = entry.getValue();
            if( XCConfig.ENABLE_STANDARD_SIZE &&
                    keyStr.equalsIgnoreCase("standard_size")){
                this.attributeMap.put("size", valueStr);
            }else {
                this.attributeMap.put(keyStr, valueStr);
            }
		}
	}

	public SearchItem(Map<?, ?> dynamoDBSearchResult) {

		for (Entry<?, ?> entry : dynamoDBSearchResult.entrySet()) {
			String keyStr = (String) entry.getKey();
			String valueStr = ((AttributeValue) entry.getValue()).getS();

			//			String mappedKeyStr = SearchUtils.getMappedAttribute(keyStr);
			//			if (mappedKeyStr == null) {
			//				mappedKeyStr = keyStr;
			//			}
			//			this.attributeMap.put(mappedKeyStr, valueStr);
			this.attributeMap.put(keyStr, valueStr);
		}
	}

	public SearchItem() {
		// TODO Auto-generated constructor stub
	}

	public List<SearchItem> getSearchItemList(List<?> resultList, Boolean isSuggested) {
		List<SearchItem> searchItemList = new ArrayList<SearchItem>();
		getSearchItemList(null, searchItemList, 100, isSuggested, resultList);
		return searchItemList;
	}

	public static void getSearchItemList(Map<String, Double> skuConfidenceMap, List<SearchItem> resultSearchItemList, double confidenceFactor, Boolean isSuggested, List<?> resultList) {
		double netConfidence = confidenceFactor;
		if (resultList != null) {
			for (Object currResult : resultList) {
				SearchItem item = null;
				if (currResult instanceof Hit) {
					item = new SearchItem((Hit) currResult);
					//					System.out.println("Current Score: " + item.getAttributeValue(SearchUtils.SCORE_KEY));
					netConfidence = Double.parseDouble(item.getAttributeValue(SearchUtils.SCORE_KEY)) * confidenceFactor / 100;
				} else if (currResult instanceof Map<?, ?>) {
					item = new SearchItem((Map<?, ?>) currResult);
				}
				if (item != null) {
					String sku = item.getAttributeValue(SearchUtils.SKU_KEY);
					item.addAttribute(SearchUtils.CONFIDENCE_KEY, String.valueOf(netConfidence));
					item.addAttribute(SearchUtils.SUGGESTED_KEY, String.valueOf(isSuggested));

					if (sku != null && skuConfidenceMap != null) {
						Double existingConfidence = skuConfidenceMap.get(sku);
						if (existingConfidence == null || existingConfidence < netConfidence) {
							resultSearchItemList.add(item);
							skuConfidenceMap.put(sku, netConfidence);
						}
					} else {
						resultSearchItemList.add(item);
					}
				}
			}

		}
	}

	public void assignConfidence(double confidenceFactor) {
		double currConfidence = Double.parseDouble(this.getAttributeValue(SearchUtils.CONFIDENCE_KEY));
		double confidence = currConfidence * confidenceFactor / 100;
		this.addAttribute(SearchUtils.CONFIDENCE_KEY, String.valueOf(confidence));

		//		System.out.println("Color: " + this.getAttributeValue(SearchUtils.COLOR_KEY) + "\tInitial confidence: " + currConfidence + "\tFinal Confidence: " + confidence);
	}

	public String getAttributeValue(String attribute) {
		return this.attributeMap.get(attribute);
	}

	public void addAttribute(String attribute, String value) {
		this.attributeMap.put(attribute, value);
	}

	public Map<String, String> getAttributeMap() {
		return attributeMap;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SearchItem [attributeMap=" + attributeMap + "]";
	}
}
