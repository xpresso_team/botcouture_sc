var express = require('express');
var router = express.Router();
var tab3 = require('../controllers/engagement');

router.route('/getEngagement').post(tab3.getEngagement);

module.exports = router; 