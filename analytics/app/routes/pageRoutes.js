/**
 * Created by abzooba on 17/6/17.
 */
var path = require('path');
var database = require('../controllers/database');

module.exports = function(app) {
    app.use('/register',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'register.html'));
    });
    app.use('/overview',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'overview.html'));
    });
    app.use('/demography',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'demography.html'));
    });
    app.use('/activity',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'activity.html'));
    });
    app.use('/engagement',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'engagement.html'));
    });
    app.use('/top-messages',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'top-messages.html'));
    });
    app.use('/usertable',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'usertable.html'));
    });
    app.use('/sessiontable',function(req, res) {
        module.exports.userId=req.query.user_id;
        res.sendFile(path.join(__dirname, '../public/html', 'sessiontable.html'));
    });
    app.use('/retentiontable',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'retentiontable.html'));
    });
    app.use('/settings',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'settings.html'));
    });
    app.use('/sessiontranscriptvisualizer',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'chat_view.html'));
    });
    app.use('/messagefunnel',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'messageFunnel.html'));
    });
    app.use('/goalfunnels/sales_conversion',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'goalFunnels_salesconversion.html'));
    });
    app.use('/goalfunnels',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'goalFunnels.html'));
    });
    app.use('/broadcast',function(req, res) {
        res.sendFile(path.join(__dirname, '../public/html', 'broadcast.html'));
    });
    app.post('/updatedatabase',function(req, res) {
        var post = req.body;
        console.log(post)
        database.changeDatabase(post.database,function (err) {
            if(err){
                console.log(err);
            }
            res.redirect("/overview");
        })
    });
};
