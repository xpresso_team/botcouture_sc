/**
 * Created by abzooba on 28/9/17.
 */
var express = require('express');
var router = express.Router();
var tab10 = require('../controllers/goalFunnels');
var tab = require('../controllers/broadcast');

router.route('/getGoalFunnelsData').post(tab10.getGoalFunnelsData);
router.route('/broadcastmessage').post(tab.broadcastmessage);
router.route('/uploadImage').post(tab.uploadImage);

module.exports = router;
