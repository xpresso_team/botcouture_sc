var path = require('path');
var passport = require('passport');
var signupController = require('../controllers/signupController.js');
var session = require('express-session');
var bcrypt = require('bcrypt-nodejs') ;
var Model = require('../model/models.js')

module.exports = function(app) {

  app.get('/',function(req, res) {
    res.sendFile(path.join(__dirname, '../public/html', 'overview.html'));
  });

  // app.get('/signup', signupController.show);
  // app.post('/signup', signupController.signup);
  //
  //   app.post('/login',function (req,res) {
  //         var username = req.body.username ;
  //         var password = req.body.password ;
  //         var post = req.body;
  //
  //         Model.User.findOne({
  //             where: {
  //                 'username': username
  //             }
  //         }).then(function (user) {
  //             if (user == null) {
  //                 res.redirect('/');
  //             }
  //             var hashedPassword = bcrypt.hashSync(password, user.salt)
  //
  //             if (user.password === hashedPassword) {
  //                 req.session.user_id = post.username;
  //                 res.redirect('/overview');
  //             }
  //             res.redirect('/');
  //         })
  //     });
  //
  //    app.get('/logout', function (req, res) {
  //         delete req.session.user_id;
  //         res.redirect('/');
  //     });

      // app.use(pageAuthCheck);
        var tab1 = require('../routes/tab1Route');
        app.use('/tab1',  tab1);
        var tab2 = require('../routes/tab2Route');
        app.use('/tab2',   tab2);
        var tab3 = require('../routes/tab3Route');
        app.use('/tab3',   tab3);
        var tab4 = require('../routes/tab4Route');
        app.use('/tab4',   tab4);
        var tab6 = require('../routes/tab6Route');
        app.use('/tab6',  tab6);
        var tab7 = require('../routes/tab7Route');
        app.use('/tab7',  tab7);
        var tab8 = require('../routes/tab8Route');
        app.use('/tab8', tab8);
        var tab9 = require('../routes/tab9Route');
        app.use('/tab9', tab9);
        var tab10 = require('../routes/tab10Route');
        app.use('/tab10', tab10);
    };

    // function checkForResources(urlPath) {
    //     var extension = path.extname(urlPath);
    //     return ((extension === 'woff2') || (extension === '.css') || (extension === '.js') || (extension === '.png') || (extension === '.map') || (extension === '.jpg') || (extension === '.gif') || (extension === '.ttf'))
    // }

    // function pageAuthCheck(req, res, next) {
    //     if (req.session.user_id || req.path === "/" || req.path === "/login" || checkForResources(req.path) || req.path === "/register") {
    //         next()
    //     } else {
    //         res.redirect('/');
    //     }
    // }

