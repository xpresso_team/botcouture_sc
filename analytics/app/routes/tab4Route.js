var express = require('express');
var router = express.Router();
var tab4 = require('../controllers/demography');

router.route('/getGenderDistribution').post(tab4.getGenderDistribution);
router.route('/getLocationDistribution').post(tab4.getLocationDistribution);

module.exports = router; 