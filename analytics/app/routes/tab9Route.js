var express = require('express');
var router = express.Router();
var tab9 = require('../controllers/retentionTable');

router.route('/getNewUserRetentionTable').post(tab9.getNewUserRetentionTable);
router.route('/getExistingUserRetentionTable').post(tab9.getExistingUserRetentionTable);

module.exports = router;