var request = require('request');
var moment = require('moment');
var database = require('./database');
var winston = require('../../LoggerUtil.js');

/*Sends data for user report linechart */
exports.getUserCount = function(request, response) {
    //winston.info(request.body);
    var start = request.body.start ;
    var type = request.body.type;
    var end = request.body.end ;
    var gender = request.body.gender ;

    //console.log("getUserCount Tab2");
    winston.info("getUserCount Tab2");
    winston.info("start: " +start + " end: " + end + " type: " + type + " gender: " +gender);

    /* query1 gets engaged user count, query2 gets new user count, query3 gets total click count and query4 gets engaged session count */
    if(gender === "all") {
        var query1 = "SELECT COUNT(distinct user_id) as user,DATE(timestamp) as date from chatlog where timestamp >='" + start + "' and timestamp <='" + end + "' group by DATE(timestamp) order by DATE(timestamp);";
        var query2 = "SELECT COUNT(distinct user_id) as user,DATE(first_active_ts) as date from user where first_active_ts >='" + start + "' and first_active_ts <='" + end + "' group by DATE(first_active_ts) order by DATE(first_active_ts);";
        var query3 = "SELECT COUNT(*) as click,DATE(timestamp) as date from click where timestamp >='" + start + "' and timestamp <='" + end + "' group by DATE(timestamp) order by DATE(timestamp);";
        var query4 = "SELECT COUNT(distinct session_id) as session,DATE(timestamp) as date from chatlog where timestamp >='" + start + "' and timestamp <='" + end + "' group by DATE(timestamp) order by DATE(timestamp);";
    }
    else if(gender==='m' || gender === 'f'){
        var query1 = "SELECT COUNT(distinct c.user_id) as user,DATE(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where u.gender = '"+gender+"' and timestamp >='" + start + "' and timestamp <='" + end + "' group by DATE(timestamp) order by DATE(timestamp);";
        var query2 = "SELECT COUNT(distinct user_id) as user,DATE(first_active_ts) as date from user where gender = '"+gender+"' and first_active_ts >='" + start + "' and first_active_ts <='" + end + "' group by DATE(first_active_ts) order by DATE(first_active_ts);";
        var query3 = "SELECT COUNT(*) as click,DATE(timestamp) as date from click c join user u on c.user_id = u.user_id where gender = '"+gender+"' and timestamp >='" + start + "' and timestamp <='" + end + "' group by DATE(timestamp) order by DATE(timestamp);";
        var query4 = "SELECT COUNT(distinct session_id) as session,DATE(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where u.gender = '"+gender+"' and timestamp >='" + start + "' and timestamp <='" + end + "' group by DATE(timestamp) order by DATE(timestamp);";
    }
    else{
        var query1 = "SELECT COUNT(distinct c.user_id) as user,DATE(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where u.gender = '' and timestamp >='" + start + "' and timestamp <='" + end + "' group by DATE(timestamp) order by DATE(timestamp);";
        var query2 = "SELECT COUNT(distinct user_id) as user,DATE(first_active_ts) as date from user where gender = '' and first_active_ts >='" + start + "' and first_active_ts <='" + end + "' group by DATE(first_active_ts) order by DATE(first_active_ts);";
        var query3 = "SELECT COUNT(*) as click,DATE(timestamp) as date from click c join user u on c.user_id = u.user_id where gender = '' and timestamp >='" + start + "' and timestamp <='" + end + "' group by DATE(timestamp) order by DATE(timestamp);";
        var query4 = "SELECT COUNT(distinct session_id) as session,DATE(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where u.gender = '' and timestamp >='" + start + "' and timestamp <='" + end + "' group by DATE(timestamp) order by DATE(timestamp);";
    }

    var engagedUser = [];
    var newUser = [] ;
    var clickCount = [];
    var engagedSession = [];

    /*daywise, weekwise and monthwise arrays store the count of users aggregated on a daily, weekly and monthly basis respectively */
    var engagedUser = [],engagedUserDayWise = [],engagedUserWeekWise = [],engagedUserMonthWise = [];
    var newUser = [],newUserDayWise = [],newUserWeekWise = [],newUserMonthWise = [] ;
    var clickCount = [],clickCountDayWise = [],clickCountWeekWise = [],clickCountMonthWise = [];
    var engagedSession = [],engagedSessionDayWise = [],engagedSessionWeekWise = [],engagedSessionMonthWise = [];

    var startDate = moment(start);
    var endDate=moment(end);

    database.executeQuery(query1,function(err,data){
        winston.info("query1: " + query1);
        if(err){
            winston.error(err);
            return;
        }
        var i=0,j=0;
        while(startDate.isBefore(endDate)){
            var testDate = (j<data.rows.length)?(moment(data.rows[j].date).format('YYYY-MM-DD')):null;
            if(startDate.format('YYYY-MM-DD').localeCompare(testDate)==0){
                engagedUser[i] = data.rows[j].user;
                j++;
            }
            else
                engagedUser[i] = 0;

            i++;
            startDate.add(1,'days');
        }
        database.executeQuery(query2,function(err,data){
            winston.info("query2: " + query2);
            if(err){
                winston.error(err);
                return;
            }

            var startDate = moment(start);
            var endDate=moment(end);
            var i=0,j=0;

            while(startDate.isBefore(endDate)){
                var testDate = (j<data.rows.length)?(moment(data.rows[j].date).format('YYYY-MM-DD')):null;
                if(startDate.format('YYYY-MM-DD').localeCompare(testDate)==0){
                    newUser[i] = data.rows[j].user;
                    j++;
                }
                else
                    newUser[i] = 0;
                i++;
                startDate.add(1,'days');
            }

            database.executeQuery(query3,function(err,data){
                winston.info("query3: " + query3);
                if(err){
                    winston.error(err);
                    return;
                }

                var startDate = moment(start);
                var endDate=moment(end);
                var i=0,j=0;
                while(startDate.isBefore(endDate)){
                    var testDate = (j<data.rows.length)?(moment(data.rows[j].date).format('YYYY-MM-DD')):null;
                    if(startDate.format('YYYY-MM-DD').localeCompare(testDate)==0){
                        clickCount[i] = data.rows[j].click;
                        j++;
                    }
                    else
                        clickCount[i] = 0;

                    i++;
                    startDate.add(1,'days');
                }

                database.executeQuery(query4,function(err,data){
                    winston.info("query4: " + query4);
                    if(err){
                        winston.error(err);
                        return;
                    }

                    var startDate = moment(start);
                    var endDate=moment(end);
                    var i=0,j=0;
                    while(startDate.isBefore(endDate)){
                        var testDate = (j<data.rows.length)?(moment(data.rows[j].date).format('YYYY-MM-DD')):null;
                        if(startDate.format('YYYY-MM-DD').localeCompare(testDate)==0){
                            engagedSession[i] = data.rows[j].session;
                            j++;
                        }
                        else
                            engagedSession[i] = 0;
                        i++;
                        startDate.add(1,'days');
                    }
                    engagedUserDayWise=engagedUser;

                    for(var i=0,data=engagedUser.slice(0);i<engagedUser.length;i+=7){
                        for(var j=i+1;j<(i+7)&&j<engagedUser.length;j++){
                            data[i]+=data[j];
                        }
                        engagedUserWeekWise.push(data[i]);
                    }
                    engagedUserWeekWise.push(0);
                    for(var i=0,data=engagedUser.slice(0);i<engagedUser.length;i+=30){
                        for(var j=i+1;j<(i+30)&&j<engagedUser.length;j++){
                            data[i]+=data[j];
                        }
                        engagedUserMonthWise.push(data[i]);
                    }
                    engagedUserMonthWise.push(0);

                    newUserDayWise=newUser;

                    for(var i=0,data=newUser.slice(0);i<newUser.length;i+=7){
                        for(var j=i+1;j<(i+7)&&j<newUser.length;j++){
                            data[i]+=data[j];
                        }
                        newUserWeekWise.push(data[i]);
                    }
                    newUserWeekWise.push(0);
                    for(var i=0,data=newUser.slice(0);i<newUser.length;i+=30){
                        for(var j=i+1;j<(i+30)&&j<newUser.length;j++){
                            data[i]+=data[j];
                        }
                        newUserMonthWise.push(data[i]);
                    }
                    newUserMonthWise.push(0);

                    clickCountDayWise=clickCount;

                    for(var i=0,data=clickCount.slice(0);i<clickCount.length;i+=7){
                        for(var j=i+1;j<(i+7)&&j<clickCount.length;j++){
                            data[i]+=data[j];
                        }
                        clickCountWeekWise.push(data[i]);
                    }
                    clickCountWeekWise.push(0);
                    for(var i=0,data=clickCount.slice(0);i<clickCount.length;i+=30){
                        for(var j=i+1;j<(i+30)&&j<clickCount.length;j++){
                            data[i]+=data[j];
                        }
                        clickCountMonthWise.push(data[i]);
                    }
                    clickCountMonthWise.push(0);

                    engagedSessionDayWise=engagedSession;

                    for(var i=0,data=engagedSession.slice(0);i<engagedSession.length;i+=7){
                        for(var j=i+1;j<(i+7)&&j<engagedSession.length;j++){
                            data[i]+=data[j];
                        }
                        engagedSessionWeekWise.push(data[i]);
                    }
                    engagedSessionWeekWise.push(0);
                    for(var i=0,data=engagedSession.slice(0);i<engagedSession.length;i+=30){
                        for(var j=i+1;j<(i+30)&&j<engagedSession.length;j++){
                            data[i]+=data[j];
                        }
                        engagedSessionMonthWise.push(data[i]);
                    }
                    engagedSessionMonthWise.push(0);

                    var data = [{
                        name: 'Engaged User Count'
                    },
                        {
                            name: 'New User Count'
                        },
                        {
                            name: 'Total Click Count'
                        },
                        {
                            name: 'Engaged Session Count'
                        }];

                    switch(type){
                        case 'Day': data[0].data=engagedUserDayWise;
                            data[1].data=newUserDayWise;
                            data[2].data=clickCountDayWise;
                            data[3].data=engagedSessionDayWise;
                            break;
                        case 'Week': data[0].data=engagedUserWeekWise;
                            data[1].data=newUserWeekWise;
                            data[2].data=clickCountWeekWise;
                            data[3].data=engagedSessionWeekWise;
                            break;
                        case 'Month': data[0].data=engagedUserMonthWise;
                            data[1].data=newUserMonthWise;
                            data[2].data=clickCountMonthWise;
                            data[3].data=engagedSessionMonthWise;
                            break;
                        default:    data[0].data=engagedUserDayWise;
                            data[1].data=newUserDayWise;
                            data[2].data=clickCountDayWise;
                            data[3].data=engagedSessionDayWise;
                            break;
                    }

                    response.send(data) ;
                });
            }) ;
        });
    });
};

/* this function sends data for activity chart*/
exports.getActivity = function(request, response) {
    //winston.info(request.body);
    var start = request.body.start ;
    var end = request.body.end ;
    var type = request.body.type;
    var gender = request.body.gender ;

    //console.log("getActivity Tab2");
    winston.info("getActivity Tab2");
    winston.info("start: " +start + " end: " + end + " type: "+type + " gender: " + gender) ;

    /* query1 gets average session count, query2 gets time per session and query3 gets messages per session */
    if(gender=="all") {
        var query1 = "select date(timestamp) as date, count(distinct session_id)/count(distinct user_id) as avgSesCount from chatlog where timestamp >='" + start + "' and timestamp <='" + end + "'group by date(timestamp) order by date(timestamp) ;";
        var query2 = "select date as date,avg(diff)/100 as timePerSes from (select date(timestamp) as date,max(timestamp)-min(timestamp) as diff from chatlog where date(timestamp)>='" + start + "'and timestamp <='" + end + "'group by session_id) difftable group by date order by date ;" ;
        var query3 = "select date(timestamp) as date, count(*)/count(distinct session_id) as msgPerSes from chatlog where timestamp >='" + start + "'and timestamp <='" + end + "'group by date(timestamp) order by date(timestamp) ; ";
    }
    else if(gender==='m' || gender === 'f'){
        var query1 = "select date(timestamp) as date, count(distinct session_id)/count(distinct c.user_id) as avgSesCount from chatlog c join user u on c.user_id = u.user_id where gender = '"+gender+"' and timestamp >='" + start + "' and timestamp <='" + end + "'group by date(timestamp) order by date(timestamp) ;";
        var query2 = "select date as date,avg(diff)/100 as timePerSes from (select date(timestamp) as date, max(timestamp)-min(timestamp) as diff from chatlog c join user u on u.user_id = c.user_id where date(timestamp)>='"+start+"' and timestamp<='"+end+"' and gender = '"+gender+"' group by session_id) difftable group by date order by date ;"
        var query3 = "select date(timestamp) as date, count(*)/count(distinct session_id) as msgPerSes from chatlog c join user u on c.user_id = u.user_id where gender = '"+gender+"' and timestamp >='" + start + "'and timestamp <='" + end + "'group by date(timestamp) order by date(timestamp) ; ";
    }
    else{
        var query1 = "select date(timestamp) as date, count(distinct session_id)/count(distinct c.user_id) as avgSesCount from chatlog c join user u on c.user_id = u.user_id where gender = '' and timestamp >='" + start + "' and timestamp <='" + end + "'group by date(timestamp) order by date(timestamp) ;";
        var query2 = "select date as date, avg(diff)/100 as timePerSes from (select date(timestamp) as date, max(timestamp)-min(timestamp) as diff from chatlog c join user u on u.user_id = c.user_id where date(timestamp)>='"+start+"' and timestamp<='"+end+"' and gender = '' group by session_id) difftable group by date order by date ;"
        var query3 = "select date(timestamp) as date, count(*)/count(distinct session_id) as msgPerSes from chatlog c join user u on c.user_id = u.user_id where gender = '' and timestamp >='" + start + "'and timestamp <='" + end + "'group by date(timestamp) order by date(timestamp) ; ";
    }

    database.executeQuery(query1,function(err,data){
        winston.info("query1: " + query1) ;
        if(err){
            winston.error(err);
            return;
        }

        var startDate = moment(start);
        var endDate=moment(end);
        var i=0,j=0;
        var avgSesCountArr = [],avgSesCountArrDayWise = [],avgSesCountArrWeekWise = [],avgSesCountArrMonthWise = [] ;
        var timePerSesArr = [],timePerSesArrDayWise = [],timePerSesArrWeekWise = [],timePerSesArrMonthWise = [];
        var msgPerSesArr = [],msgPerSesArrDayWise = [],msgPerSesArrWeekWise = [],msgPerSesArrMonthWise = [] ;

        while(startDate.isBefore(endDate)){
            var testDate = (j<data.rows.length)?(moment(data.rows[j].date).format('YYYY-MM-DD')):null;
            if(startDate.format('YYYY-MM-DD').localeCompare(testDate)===0){
                avgSesCountArr[i] = data.rows[j].avgSesCount;
                j++;
            }
            else{
                avgSesCountArr[i] = 0;
            }
            i++;
            startDate.add(1,'days');
        }
        database.executeQuery(query2,function(err,data){
            winston.info("query2: " + query2) ;
            if(err){
                winston.error(err);
                return;
            }
            winston.info(data);
            var startDate = moment(start);
            var endDate=moment(end);
            var i=0,j=0;
            while(startDate.isBefore(endDate)){
                var testDate = (j<data.rows.length)?(moment(data.rows[j].date).format('YYYY-MM-DD')):null;
                if(startDate.format('YYYY-MM-DD').localeCompare(testDate)===0){
                    timePerSesArr[i] = data.rows[j].timePerSes;
                    j++;
                }
                else
                    timePerSesArr[i] = 0;

                i++;
                startDate.add(1,'days');
            }
            database.executeQuery(query3,function(err,data){
                winston.info("query3: " + query3) ;
                if(err){
                    winston.error(err);
                    return;
                }
                winston.info(data);
                var startDate = moment(start);
                var endDate=moment(end);
                var i=0,j=0;
                while(startDate.isBefore(endDate)){
                    var testDate = (j<data.rows.length)?(moment(data.rows[j].date).format('YYYY-MM-DD')):null;
                    if(startDate.format('YYYY-MM-DD').localeCompare(testDate)===0){
                        msgPerSesArr[i] = data.rows[j].msgPerSes;
                        j++;
                    }
                    else
                        msgPerSesArr[i] = 0;
                    i++;
                    startDate.add(1,'days');
                }
                avgSesCountArrDayWise=avgSesCountArr;

                for(var i=0,data=avgSesCountArr.slice(0);i<avgSesCountArr.length;i+=7){
                    for(var j=i+1;j<(i+7)&&j<avgSesCountArr.length;j++){
                        data[i]+=data[j];
                    }
                    avgSesCountArrWeekWise.push(data[i]);
                }
                avgSesCountArrWeekWise.push(0);
                for(var i=0,data=avgSesCountArr.slice(0);i<avgSesCountArr.length;i+=30){
                    for(var j=i+1;j<(i+30)&&j<avgSesCountArr.length;j++){
                        data[i]+=data[j];
                    }
                    avgSesCountArrMonthWise.push(data[i]);
                }
                avgSesCountArrMonthWise.push(0);

                timePerSesArrDayWise=timePerSesArr;

                for(var i=0,data=timePerSesArr.slice(0);i<timePerSesArr.length;i+=7){
                    for(var j=i+1;j<(i+7)&&j<timePerSesArr.length;j++){
                        data[i]+=data[j];
                    }
                    timePerSesArrWeekWise.push(data[i]);
                }
                timePerSesArrWeekWise.push(0);
                for(var i=0,data=timePerSesArr.slice(0);i<timePerSesArr.length;i+=30){
                    for(var j=i+1;j<(i+30)&&j<timePerSesArr.length;j++){
                        data[i]+=data[j];
                    }
                    timePerSesArrMonthWise.push(data[i]);
                }
                timePerSesArrMonthWise.push(0);

                msgPerSesArrDayWise=msgPerSesArr;

                for(var i=0,data=msgPerSesArr.slice(0);i<msgPerSesArr.length;i+=7){
                    for(var j=i+1;j<(i+7)&&j<msgPerSesArr.length;j++){
                        data[i]+=data[j];
                    }
                    msgPerSesArrWeekWise.push(data[i]);
                }
                msgPerSesArrWeekWise.push(0);
                for(var i=0,data=msgPerSesArr.slice(0);i<msgPerSesArr.length;i+=30){
                    for(var j=i+1;j<(i+30)&&j<msgPerSesArr.length;j++){
                        data[i]+=data[j];
                    }
                    msgPerSesArrMonthWise.push(data[i]);
                }
                msgPerSesArrMonthWise.push(0);

                var data = [{
                    name: 'Average Session Count',
                },
                    {
                        name: 'Time per Session',
                    },
                    {
                        name: 'Messages per Session',
                    }];
                switch(type){
                    case 'Day': data[0].data=avgSesCountArrDayWise;
                        data[1].data=timePerSesArrDayWise;
                        data[2].data=msgPerSesArrDayWise;
                        break;
                    case 'Week': data[0].data=avgSesCountArrWeekWise;
                        data[1].data=timePerSesArrWeekWise;
                        data[2].data=msgPerSesArrWeekWise;
                        break;
                    case 'Month': data[0].data=avgSesCountArrMonthWise;
                        data[1].data=timePerSesArrMonthWise;
                        data[2].data=msgPerSesArrMonthWise;
                        break;
                    default:    data[0].data=avgSesCountArrDayWise;
                        data[1].data=timePerSesArrDayWise;
                        data[2].data=msgPerSesArrDayWise;
                        break;
                }

                response.send(data) ;
            });
        });
    });
};

/*this function gets message count chart*/
exports.getMsgCount = function(request, response) {
    //winston.log(request.body);
    var start = request.body.start ;
    var end = request.body.end ;
    var type = request.body.type;
    var msgInType = request.body.messageInType ;
    var msgOutType = request.body.messageOutType ;
    var gender = request.body.gender ;

    //console.log("getMsgCount Tab2");
    winston.info("getMsgCount Tab2");

    //console.log("Message In Type: "+ msgInType);
    //console.log("Message Out Type: "+ msgOutType);

    /*query1 gets data for incoming messages filtered by the msg flag */
    winston.info("start: " + start + " end: " + end + " type: " + type + " msgInType: " + msgInType + " msgOutType: " + msgOutType + " msgInType: " + msgInType + " gender: " + gender) ;
    if(msgInType === "All" && gender==="all"){
        var query1 = "SELECT COUNT(*) as msgIn,DATE(timestamp) as date from chatlog where timestamp >='"+start+"' and timestamp <='"+end+"' and message_type = 'IN' group by DATE(timestamp) order by DATE(timestamp);" ;
    }
    else if(msgInType==="All" && (gender==='m' || gender ==='f')){
        var query1 = "SELECT COUNT(*) as msgIn,DATE(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where gender ='"+gender+"' and timestamp >='"+start+"' and timestamp <='"+end+"' and message_type = 'IN' group by DATE(timestamp) order by DATE(timestamp);" ;
    }
    else if(msgInType==="All" && gender === ''){
        var query1 = "SELECT COUNT(*) as msgIn,DATE(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where gender = '' and timestamp >='"+start+"' and timestamp <='"+end+"' and message_type = 'IN' group by DATE(timestamp) order by DATE(timestamp);"
    }
    else if(msgInType!=="All" && gender==="all"){
        var query1 = "SELECT COUNT(*) as msgIn,DATE(timestamp) as date from chatlog where timestamp >='"+start+"' and timestamp <='"+end+"' and message_type_flag = '"+msgInType+"' and message_type = 'IN' group by DATE(timestamp) order by DATE(timestamp);" ;
    }
    else if(msgInType!=="All" && (gender==='m' || gender === 'f')){
        var query1 = "select count(*) as msgIn, date(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where message_type_flag = '"+msgInType+"' and timestamp>='"+start+"' and timestamp<= '"+end+"' and gender = '"+gender+"' group by date(timestamp) order by date(timestamp);" ;
    }
    else{
        var query1 = "select count(*) as msgIn, date(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where message_type_flag = '"+msgInType+"' and timestamp>='"+start+"' and timestamp<= '"+end+"' and gender = '' group by date(timestamp) order by date(timestamp);" ;
    }


    /*query1 gets data for outgoing messages filtered by the msg flag */
    if(msgOutType === "All" && gender==="all"){
        var query2 = "SELECT COUNT(*) as msgOut,DATE(timestamp) as date from chatlog where timestamp >='"+start+"' and timestamp <='"+end+"' and (message_type = 'OB' or message_type = 'OH') group by DATE(timestamp) order by DATE(timestamp);" ;
    }
    else if(msgOutType === "All" && (gender==='m' || gender==='f')){
        var query2 = "SELECT COUNT(*) as msgOut,DATE(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where gender = '"+gender+"' and timestamp >='"+start+"' and timestamp <='"+end+"' and (message_type = 'OB' or message_type = 'OH') group by DATE(timestamp) order by DATE(timestamp);" ;
    }
    else if(msgOutType === "All" && (gender === '')){
        var query2 = "SELECT COUNT(*) as msgOut,DATE(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where gender = '' and timestamp >='"+start+"' and timestamp <='"+end+"' and (message_type = 'OB' or message_type = 'OH') group by DATE(timestamp) order by DATE(timestamp);" ;
    }
    else if(msgOutType !== "All" && gender==="all"){
        var query2 = "SELECT COUNT(*) as msgOut,DATE(timestamp) as date from chatlog where timestamp >='"+start+"' and timestamp <='"+end+"' and (message_type = 'OB' or message_type = 'OH') and message_type_flag = '"+msgOutType+"' group by DATE(timestamp) order by DATE(timestamp);" ;
    }
    else if(msgOutType !== "All" && (gender ==="m" || gender === "f")){
        var query2 = "SELECT COUNT(*) as msgOut,DATE(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where gender = '"+gender+"' and timestamp >='"+start+"' and timestamp <='"+end+"' and (message_type = 'OB' or message_type = 'OH') and message_type_flag = '"+msgOutType+"' group by DATE(timestamp) order by DATE(timestamp);" ;
    }
    else{
        var query2 = "SELECT COUNT(*) as msgOut,DATE(timestamp) as date from chatlog c join user u on c.user_id = u.user_id where gender ='' and timestamp >='"+start+"' and timestamp <='"+end+"' and message_type_flag ='"+msgOutType+"' and (message_type = 'OB' or message_type = 'OH') group by DATE(timestamp) order by DATE(timestamp);" ;
    }

    var msgInArr = [];
    var msgOutArr = [] ;
    var msgInArrDayWise=[],msgInArrWeekWise=[],msgInArrMonthWise=[];
    var msgOutArrDayWise=[],msgOutArrWeekWise=[],msgOutArrMonthWise=[];

    database.executeQuery(query1,function(err,data){
        winston.info("query1: " + query1) ;
        if(err){
            winston.log(err);
            return;
        }
        winston.log(data);
        var startDate = moment(start);
        var endDate=moment(end);
        var i=0,j=0;
        while(startDate.isBefore(endDate)){
            var testDate = (j<data.rows.length)?(moment(data.rows[j].date).format('YYYY-MM-DD')):null;
            if(startDate.format('YYYY-MM-DD').localeCompare(testDate)==0){
                msgInArr[i] = data.rows[j].msgIn;
                j++;
            }
            else{
                msgInArr[i] = 0;
            }
            i++;
            startDate.add(1,'days');
        }
        database.executeQuery(query2,function(err,data){
            winston.info("query2: " + query2) ;
            if(err){
                winston.log(err);
                return;
            }
            winston.log(data);
            var startDate = moment(start);
            var endDate=moment(end);
            var i=0,j=0;
            while(startDate.isBefore(endDate)){
                var testDate = (j<data.rows.length)?(moment(data.rows[j].date).format('YYYY-MM-DD')):null;
                if(startDate.format('YYYY-MM-DD').localeCompare(testDate)==0){
                    msgOutArr[i] = data.rows[j].msgOut;
                    j++;
                }
                else
                    msgOutArr[i] = 0;
                i++;
                startDate.add(1,'days');
            }

            msgInArrDayWise=msgInArr;

            for(var i=0,data=msgInArr.slice(0);i<msgInArr.length;i+=7){
                for(var j=i+1;j<(i+7)&&j<msgInArr.length;j++){
                    data[i]+=data[j];
                }
                msgInArrWeekWise.push(data[i]);
            }
            msgInArrWeekWise.push(0);
            for(var i=0,data=msgInArr.slice(0);i<msgInArr.length;i+=30){
                for(var j=i+1;j<(i+30)&&j<msgInArr.length;j++){
                    data[i]+=data[j];
                }
                msgInArrMonthWise.push(data[i]);
            }
            msgInArrMonthWise.push(0);

            msgOutArrDayWise=msgOutArr;

            for(var i=0,data=msgOutArr.slice(0);i<msgOutArr.length;i+=7){
                for(var j=i+1;j<(i+7)&&j<msgOutArr.length;j++){
                    data[i]+=data[j];
                }
                msgOutArrWeekWise.push(data[i]);
            }
            msgOutArrWeekWise.push(0);
            for(var i=0,data=msgOutArr.slice(0);i<msgOutArr.length;i+=30){
                for(var j=i+1;j<(i+30)&&j<msgOutArr.length;j++){
                    data[i]+=data[j];
                }
                msgOutArrMonthWise.push(data[i]);
            }
            msgOutArrMonthWise.push(0);

            var msgArr = [];
            msgArr.push(msgInArr);
            msgArr.push(msgOutArr);
            var data = [{
                name: 'Message In'
            },
                {
                    name: 'Message Out'
                }];
            switch(type){
                case 'Day': data[0].data=msgInArrDayWise;
                    data[1].data=msgOutArrDayWise;
                    break;
                case 'Week': data[0].data=msgInArrWeekWise;
                    data[1].data=msgOutArrWeekWise;
                    break;
                case 'Month': data[0].data=msgInArrMonthWise;
                    data[1].data=msgOutArrMonthWise;
                    break;
                default:    data[0].data=msgInArrDayWise;
                    data[1].data=msgOutArrDayWise;
                    break;
            }
            response.send(data) ;
        }) ;
    });
};
