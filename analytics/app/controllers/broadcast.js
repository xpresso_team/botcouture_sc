var request = require('request');
var moment = require('moment');
var winston = require('../../LoggerUtil.js');
var database = require('./database.js');
var fs = require('fs');
var FormData = require('form-data');
var https = require('https');
var configuration = require('../../config/index.js')(process.argv[2]);

//for uploading the image, saving to images folder and broadcasting the image to the bot
exports.uploadImage = function(req, resp) {
  var res = {};
  if (!req.files.hasOwnProperty('file')) {
    res.statusCode = 400;
    res.message = 'No file selected.Please select a file.';
    return resp.send(res);
  }

  // The name of the input field (i.e. "file") is used to retrieve the uploaded file
  var file = req.files.file;
  var filename = req.files.file.name;

  // Use the mv() method to place the file
  file.mv('./images/' + filename, function(err) {
    if (err) {
      res.statusCode = 500;
      res.message = err;
      return resp.send(res);
    }
    var query = 'select user_id from user';
    winston.info("Query for retrieving user  id for broadcasting image : " + query);
    database.executeQuery(query, function(err, data) {
      if (err) {
        winston.error(err);
        res.statusCode = 503;
        res.message = "Unable to connect to database";
        resp.send(res);
      }

      try {
        var id_notsent = [];
        var pagetoken = configuration.get('pageAccessToken');

        function sendimagemessage(callback) {
          const promises = data.rows.map(id => {
            var messageData = new FormData();
            messageData.append('recipient', '{id:' + id.user_id + '}');
            messageData.append('message', '{attachment :{type:"image", payload:{is_reusable: true}}}');
            messageData.append('filedata', fs.createReadStream('./images/' + filename));
            return broadcastimagemessage(id.user_id, messageData, filename, pagetoken, id_notsent);
          });
          Promise.all(promises).then(() => callback(id_notsent));
        }

        function sendimagestatus() {
          if (id_notsent.length == 0) {
            res.statusCode = 200;
            res.message = "Successfully sent generic image message to all recipients";
          } else {
            res.statusCode = 400;
            res.message = "Unable to send image message to all users. Message not sent to recipients : " + id_notsent.toString();
          };
          resp.send(res);
        }
        sendimagemessage(sendimagestatus);
      } catch (err) {
        winston.error(err);
        res.statusCode = 400;
        res.message = "Unable to send message";
        resp.send(res);
      }
    });
  });

  function broadcastimagemessage(recipientId, messageData, filename, pagetoken, id_notsent) {
    return new Promise((resolve, reject) => {
      var options = {
        method: 'post',
        host: 'graph.facebook.com',
        path: '/v2.6/me/messages?access_token=' + pagetoken,
        headers: messageData.getHeaders()
      };

      var request = https.request(options);
      messageData.pipe(request);
      request.on('error', function(error) {
        id_notsent.push(recipientId);
        winston.error(error);
        winston.info("Unable to send generic message to recipient %s", recipientId);
        resolve();
        return;
      });
      request.on('response', function(res) {
        if (res.statusMessage == "OK") {
          winston.info("Successfully sent generic message to recipient %s", recipientId);
        } else {
          id_notsent.push(recipientId);
          winston.info("Unable to send generic message to recipient %s", recipientId);
        }
        resolve();
        return;
      });
    });
  }
};


exports.broadcastmessage = function(req, resp) {
  var messageText = req.body.messageText;
  winston.info("Message : " + messageText);

  var query = 'select user_id from user'
  winston.info("Query for retrieving user  id : " + query);

  database.executeQuery(query, function(err, data) {

    var res = {};
    if (err) {
      winston.error(err);
      res.statusCode = 503;
      res.message = "Unable to connect to database";
      resp.send(res);
    }

    try {
      var id_notsent = [];
      var pagetoken = configuration.get('pageAccessToken');

      function sendmessage(callback) {
        const promises = data.rows.map(id => {
          var messageData = {
            recipient: {
              id: id.user_id
            },
            message: {
              text: messageText
            }
          };
          return callSendAPI(messageData, pagetoken, id_notsent);
        });
        Promise.all(promises).then(() => callback(id_notsent));
      }

      function sendstatus() {
        if (id_notsent.length == 0) {
          res.statusCode = 200;
          res.message = "Successfully sent generic message to all recipients";
        } else {
          res.statusCode = 400;
          res.message = "Unable to send message to all users. Message not sent to recipients : " + id_notsent.toString();
        };
        resp.send(res);
      }
      sendmessage(sendstatus);

    } catch (err) {
      winston.error(err);
      res.statusCode = 400;
      res.message = "Unable to send message";
      resp.send(res);
    }
  });

  function callSendAPI(messageData, pagetoken, id_notsent) {
    return new Promise((resolve, reject) => {
      request({
        uri: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {
          access_token: pagetoken,
        },
        method: 'POST',
        json: messageData
      }, function(error, response, body) {

        if (!error && response.statusCode == 200) {
          // winston.info(response);
          var recipientId = body.recipient_id;
          var messageId = body.message_id;
          winston.info("Successfully sent generic message with id %s to recipient %s", messageId, recipientId);
          resolve();
          return;
        } else {
          var recipientId = messageData.recipient.id;
          id_notsent.push(recipientId);
          winston.info("Unbale to send generic message to recipient %s", recipientId);
          // winston.info(response);
          winston.error(error);
          resolve();
          return;
        }
      });
    });
  };
};
