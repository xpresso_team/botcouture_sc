var request = require('request');
var moment = require('moment');
var database = require('./database');
var winston = require('../../LoggerUtil.js');
var fs = require('fs') ;

/*this function sends data of user information */
exports.getUserTable = function(request, response) {
    var start = request.body.start;
    var end = request.body.end;
    winston.info("In User Table") ;
    var query = "select temp.user_id as user_id,first_name,first_active_ts,locale as location,gender,count(distinct session_id) as session_count, count(if(message_type='IN',1,null)) as message_typed_count, count(if(message_type='OB',1,null)) as message_received_count, count(if(message_type_flag='IT',1,null)) as query_count, count(if(message_type_flag='IB',1,null)) as vision_query_count,avg(count) as avg_message_per_session,min(count) as min_message_per_session,max(count) as max_message_per_session, sum(diff) as total_online_time from (select user_id,count(message_type) as count, TIMESTAMPDIFF(SECOND,max(timestamp),min(timestamp))/60 as diff from chatlog group by session_id,user_id) as temp,chatlog,user where temp.user_id = chatlog.user_id and chatlog.user_id=user.user_id and date(chatlog.timestamp) between '"+start+"' and '"+end+"' group by user.user_id order by user.user_id;";
    database.executeQuery(query,function(err,data){
        winston.info("query: " + query) ;
        if(err){
            winston.error(err);
            return;
        }
        var i,data1=[];
        for(i=0;i<data.rows.length;i++){
            data1.push([
                data.rows[i].first_name,
                '<a href="../sessiontable?user_id='+data.rows[i].user_id+'">'+data.rows[i].user_id+'</a>',
                moment(data.rows[i].first_active_ts).format('YYYY-MM-DD, h:mm:ss a'),
                data.rows[i].session_count,
                data.rows[i].avg_message_per_session,
                data.rows[i].total_online_time,
                data.rows[i].min_message_per_session,
                data.rows[i].max_message_per_session,
                data.rows[i].message_typed_count,
                data.rows[i].message_received_count,
                data.rows[i].query_count,
                data.rows[i].vision_query_count,
                data.rows[i].location,
                data.rows[i].gender
            ]);
        }
        response.send(data1);
    });
};
