var request = require('request');
var moment = require('moment');
var database = require('./database');
var winston = require('../../LoggerUtil.js');

exports.getUserCount(function(request,response){

    var start = request.body.start;
    var end = request.body.end;

    var query = "SELECT COUNT(distinct user_id) as user,DATE(timestamp) as date from chatlog where timestamp >='" + start + "' and timestamp <='" + end + "' group by DATE(timestamp) order by DATE(timestamp);";
    database.executeQuery(query,function(err,data){
        if(err){
            winston.err(err+" IN getUserCount");
        }
        console.log(data);
        var dataset =[{
            label : "Engaged User Count",
            color : "#1ab394",
            bars : {
                show: true,
                align: "center",
                barWidth: 24 * 60 * 60 * 600,
                lineWidth:0
            }
        }]
    })
})