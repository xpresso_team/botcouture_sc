var request = require('request');
var moment = require('moment');
var database = require('./database');
var winston = require('../../LoggerUtil.js');
var HashMap = require('hashmap');

/*sends top incoming messages and their count */
exports.getMessageIn = function(request, response) {
  var start = request.body.start;
  var end = request.body.end;
  var gender = request.body.gender;

  //winston.info("getMessagesIn Tab6");
  winston.info("getMessagesIn Tab6");

  var msgType = request.body.msgInType;
  winston.info("msgType " + msgType);

  winston.info("gender:" + gender);

  if (gender === "all") {
    if (msgType !== 'All')
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog where message_type_flag = '" + msgType + "' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
    else
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog where message_type ='IN' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
  } else if (gender === "m" || gender === "f") {
    if (msgType !== 'All')
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog c join user u on u.user_id = c.user_id where gender = '" + gender + "' and message_type_flag = '" + msgType + "' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
    else
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog c join user u on u.user_id = c.user_id where gender = '" + gender + "' and message_type = 'IN' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
  } else {
    if (msgType !== 'All')
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog c join user u on u.user_id = c.user_id where gender = '' and message_type_flag = '" + msgType + "' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
    else
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog c join user u on u.user_id = c.user_id where gender = '' and message_type = 'IN' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
  }
  database.executeQuery(query, function(err, data) {
    winston.info("query: " + query);
    if (err) {
      winston.err(err);
      return;
    }
    var retData = new Array();
    winston.info('info', data);
    var i;

    if (msgType === "II") {

      for (i = 0; i < data.rows.length; i++) {
        var uri = encodeURI(data.rows[i].msgChat);
        var linkToimage = '<img src=' + data.rows[i].msgChat + ' alt = "Image" style = "max-width:128px; max-height:128px;">'
        retData.push([
          '<a href="../messageFunnel.html?msgChat=' + data.rows[i].msgChat + '">' + linkToimage + '</a>',
          data.rows[i].countMsg
        ])
      }
    } else {
      for (i = 0; i < data.rows.length; i++) {

        var msgtoput = data.rows[i].msgChat;
        if (msgtoput == "") {
          msgtoput = "Blank";
        }
        var uri = encodeURI(data.rows[i].msgChat);
        retData.push([
          // '<a href="../messagefunnel?msgChat=' + data.rows[i].msgChat + '">' + msgtoput + '</a>',
          '<a href="../messagefunnel?msgChat=' + uri + '">' + msgtoput + '</a>',
          data.rows[i].countMsg
        ])
      }
    }
    response.send(retData);
  });
};

/* sends outgoing messages and their count */
exports.getMessageOut = function(request, response) {
  var start = request.body.start;
  var end = request.body.end;
  var msgType = request.body.msgOutType;
  var gender = request.body.gender;

  winston.info("start: " + start + " end: " + end);

  winston.info("msgType " + msgType);

  winston.info("gender " + gender);

  winston.info("getMessageOut Tab6");

  if (gender === "all") {
    if (msgType !== 'All')
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog where message_type_flag = '" + msgType + "' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
    else
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog where message_type = 'OB' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
  } else if (gender === "m" || gender === "f") {
    if (msgType !== 'All')
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog c join user u on c.user_id = u.user_id where gender = '" + gender + "' and message_type_flag = '" + msgType + "' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
    else
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog c join user u on c.user_id = u.user_id where gender = '" + gender + "' and message_type = 'OB' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
  } else {
    if (msgType != 'All')
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog c join user u on c.user_id = u.user_id where gender = '' and message_type_flag = '" + msgType + "' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
    else
      var query = "select count(message_chat) as countMsg, message_chat as msgChat from chatlog c join user u on c.user_id = u.user_id where gender = '' and message_type = 'OB' and date(timestamp)>= '" + start + "' and date(timestamp)<= '" + end + "' group by message_chat order by count(message_chat) desc ;";
  }

  database.executeQuery(query, function(err, data) {
    winston.info("query: " + query);
    if (err) {
      winston.err(err);
      return;
    }

    var retData = new Array();
    var i;
    for (i = 0; i < data.rows.length; i++) {

      var msgtoput = data.rows[i].msgChat;
      if (msgtoput == "") {
        msgtoput = "Blank";
      }
			var uri = encodeURI(data.rows[i].msgChat);
      retData.push([
        // '<a href="../messagefunnel?msgChat=' + data.rows[i].msgChat + '">' + msgtoput + '</a>',
				'<a href="../messagefunnel?msgChat=' + uri + '">' + msgtoput + '</a>',
        data.rows[i].countMsg
      ]);
    }
    response.send(retData);
  });
};

/* message funnel data */
exports.getMessageFunnel = function(request, response) {
  var msg = request.body.msgChat;
  /*stores next message and it's count */
  var nextMsgMap = new HashMap();
  /*stores previous message and it's count */
  var prevMsgMap = new HashMap();
  winston.info(" current mesaage: " + msg);
  var query = "select b.message_chat as nextChat, b.product_response_list as nextList, c.message_chat as prevChat, c.product_response_list as prevList from chatlog a join chatlog b on a.message_number+1 = b.message_number and a.session_id = b.session_id join chatlog c on a.session_id = c.session_id and a.message_number-1 = c.message_number where a.message_chat = \"" + msg + "\" ;"
  database.executeQuery(query, function(err, data) {
    winston.info("queryfunnel: " + query);
    if (err) {
      winston.err(err);
      return;
    }
    var retData = [];
    var i;

    for (i = 0; i < data.rows.length; i++) {
      if (nextMsgMap.has(data.rows[i].nextChat)) {
        nextMsgMap.set(data.rows[i].nextChat, nextMsgMap.get(data.rows[i].nextChat) + 1)
      } else
        nextMsgMap.set(data.rows[i].nextChat, 1);
    }

    for (i = 0; i < data.rows.length; i++) {
      if (prevMsgMap.has(data.rows[i].prevChat))
        prevMsgMap.set(data.rows[i].prevChat, prevMsgMap.get(data.rows[i].prevChat) + 1)
      else
        prevMsgMap.set(data.rows[i].prevChat, 1);
    }

    var prevLen = prevMsgMap.count();
    var nextLen = nextMsgMap.count();

    var prevArray = []
    for (i = 0; i < prevLen; i++) {
      prevArray.push({
        "chat": prevMsgMap.keys()[i],
        "count": prevMsgMap.values()[i]
      });
    }
    /* sort decreasing according to count of prev messages */
    prevArray.sort(function(a, b) {
      return b.count - a.count;
    });

    var nextArray = []
    for (i = 0; i < nextLen; i++) {
      nextArray.push({
        "chat": nextMsgMap.keys()[i],
        "count": nextMsgMap.values()[i]
      });
    }
    /* sort decreasing according to count of next messages */
    nextArray.sort(function(a, b) {
      return b.count - a.count;
    });

    var currArray = []
    currArray.push({
      "chat": msg
    });

    /*converting to JSON*/
    var next = '{"nextArray":' + JSON.stringify(nextArray) + '}';
    var prev = '{"prevArray":' + JSON.stringify(prevArray) + '}';
    var curr = '{"currMsg":' + JSON.stringify(currArray) + '}';
    // winston.info(prev);
    // winston.info(next);

    var retJson = '{"messages":' + '[' + prev + ',' + next + ',' + curr + ']' + '}';
    // retJson = JSON.parse(retJson) ;
    // winston.info("next->"+JSON.stringify(retJson.messages[1].nextArray[0].count));

    winston.info("retjson" + retJson);
    response.send(retJson);

  });
}
