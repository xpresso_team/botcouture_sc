var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var sequelize = require('../../sequelize.js');
var Model = require('../model/models.js');

module.exports.show = function(req, res) {
    res.sendFile(path.join(__dirname, '../public/html', 'login.html'));
}

module.exports.signup = function(req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;
    var password2 = req.body.password2;

    if (!username || !password || !password2) {
        req.flash('error', "Please, fill in all the fields.")
        res.redirect('signup')
    }

    if (password !== password2) {
        req.flash('error', "Please, enter the same password twice.")
        res.redirect('signup')
    }

    var salt = bcrypt.genSaltSync(10)
    var hashedPassword = bcrypt.hashSync(password, salt)

    var newUser = {
        username: username,
        email: email,
        password: hashedPassword,
        salt: salt
    };

    // Model.User.sync({force: true}).then(function(){
    //     return Model.User.create(newUser).then(function() {
    //         res.redirect('/')
    //     }).catch(function(error) {
    //         req.flash('error', "Please, choose a different username.")
    //         res.redirect('/signup')
    //     })
    // });
//}
    Model.User.create(newUser).then(function () {
        res.redirect('/')
    }).catch(function (error) {
        req.flash('error', "Please, choose a different username.")
        res.redirect('/signup')
    });
}
