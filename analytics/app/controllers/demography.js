var request = require('request');
var moment = require('moment');
var database = require('./database');
var winston = require('../../LoggerUtil.js');

/*this function sends data for gender distribution pie chart*/
exports.getGenderDistribution = function(request, response) {
  //winston.info(request.body);
  var start = request.body.start;
  var end = request.body.end;
  /*is gender filter on include null or remove null */
  var genderNull = request.body.genderNull;

  winston.info("getGenderDistribution");
  var flag = 0;

  winston.info("start: " + start + " end: " + end + " genderNull: " + genderNull);

  /*query to get count of users of different genders*/
  /* flag=1 if filter selected to remove null */
  if (genderNull === 'yes')
    var query = "select count(distinct user.user_id) as genderCount, user.gender from chatlog join user on user.user_id = chatlog.user_id where date(timestamp) >='" + start + "' and date(timestamp) <='" + end + "' group by gender;";
  else {
    var query = "select count(distinct user.user_id) as genderCount, user.gender from chatlog join user on user.user_id = chatlog.user_id where gender <> '' and date(timestamp) >='" + start + "' and date(timestamp) <='" + end + "' group by gender;";
    flag = 1;
  }

  database.executeQuery(query, function(err, data) {
    winston.info(query);
    if (err) {
      winston.error(err);
      return;
    }
    try {
      if (data.rows == null) {
        winston.info('There is no data');
      }
      /* if flag = 0, send data for male, female and none */
      if (flag === 0) {
        // var male = (data.rows.length > 0) ? (data.rows[2].genderCount) : 100;
        // var female = (data.rows.length > 0) ? (data.rows[1].genderCount) : 0;
        var male = (data.rows.length > 2) ? (data.rows[2].genderCount) : 0;
        var female = (data.rows.length > 1) ? (data.rows[1].genderCount) : 0;
        var none = (data.rows.length > 0) ? (data.rows[0].genderCount) : 0;
        var malePercent = 100 * male / (male + female + none);
        var femalePercent = 100 * female / (male + female + none);
        var nonePercent = 100 * none / (male + female + none);

        var data = {
          name: 'Percentage',
          colorByPoint: true,
          data: [{
            name: 'Male',
            y: male,
            sliced: true,
            selected: true
          }, {
            name: 'Female',
            y: female
          }, {
            name: 'None',
            y: none
          }]
        };
      }

      /* if flag = 1, send data for male and female only */
      if (flag === 1) {
        var male = (data.rows.length > 1) ? (data.rows[1].genderCount) : 0;
        var female = (data.rows.length > 0) ? (data.rows[0].genderCount) : 0;

        if(male!=0 || female!=0)
        {
          var malePercent = 100 * male / (male + female);
          var femalePercent = 100 * female / (male + female);
        }

        var data = {
          name: 'Percentage',
          colorByPoint: true,
          data: [{
            name: 'Male',
            y: male,
            sliced: true,
            selected: true
          }, {
            name: 'Female',
            y: female
          }, ]
        };
      }

      response.send(data);
    } catch (err) {
      response.send({});
    }
  });
};

/* this function sends data for location distribution treemap of users */
exports.getLocationDistribution = function(request, response) {
  var start = request.body.start;
  var end = request.body.end;

  var incNull = request.body.incNull;
  winston.info("getLocationDistribution");

  var end = request.body.end;
  var locationNull = request.body.locationNull;
  var interval = moment(end).diff(moment(start), 'days');
  var prevStart = moment(start).subtract(interval, 'days').format('YYYY-MM-DD');
  winston.info("locationNull: " + locationNull);
  winston.info("prevStart: " + prevStart);
  //winston.info("getLocationDistribution");

  /*query to get count of users in each location and their respective changes to get gradient color of treemap */
  if (locationNull === 'yes') {
    var query = "select a.locale,a.count as new,b.count as prev,((a.count-b.count)*100)/b.count as percent from (select locale,count(locale) as count from user where date(first_active_ts) between '" + start + "' and '" + end + "' group by locale) as a left join (select locale,count(locale) as count from user where date(first_active_ts) between '" + prevStart + "' and '" + start + "' group by locale) as b on a.locale=b.locale order by percent;";
  } else {
    var query = "select a.locale,a.count as new,b.count as prev,((a.count-b.count)*100)/b.count as percent from (select locale,count(locale) as count from user where date(first_active_ts) between '" + start + "' and '" + end + "' group by locale) as a left join (select locale,count(locale) as count from user where date(first_active_ts) between '" + prevStart + "' and '" + start + "' group by locale) as b on a.locale=b.locale where a.locale <> '' order by percent;";
  }
  database.executeQuery(query, function(err, data) {
    winston.info("query: " + query);
    if (err) {
      winston.error(err);
      return;
    }
    winston.info("Location Distribution :" + JSON.stringify(data));
    var data1 = [];
    for (i = 0; i < data.rows.length; i++) {

      data1.push({
        name: (data.rows[i].locale === " ") ? "NULL" : data.rows[i].locale,
        value: data.rows[i].new,
        colorValue: i
      });
    };
    response.send(data1);
  });
};
