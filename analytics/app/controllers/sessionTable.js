var request = require('request');
var pageRoutes = require('../routes/pageRoutes.js');
var moment = require('moment');
var fs = require('fs');
var database = require('./database');
var winston = require('../../LoggerUtil.js');

/*this function sends data for information about each session */
exports.getSessionTable = function(request,response){
    var start = request.body.start ;
    var end  = request.body.end ;
    var gender = request.body.gender ;
    var userId = pageRoutes.userId;


    winston.info("start " + start) ;
    winston.info("end " + end) ;
    winston.info("gender " + gender) ;
    winston.info("getSessionTable Tab8");
    winston.info("UserId in Session Table :"+userId);

    if(userId===null||userId===undefined){
        if(gender==="all")
            var query = "select c.session_id as session_number, u.first_name as first_name, u.last_name as last_name, c.timestamp as session_start_ts, max(c.timestamp) as session_end_ts, count(if(c.message_type='IN',1,null)) as message_in_count, count(if(c.message_type_flag='INQ',1,null)) as nlp_query_count, count(if(c.message_type_flag='II',1,null)) as vision_query_count, count(if(c.message_type='OB' or c.message_type='OH',1,null)) as message_out_count, count(c.message_type) as total_messages from user u join chatlog c on u.user_id = c.user_id where date(c.timestamp)>='"+start+"' and date(c.timestamp)<='"+end+"' group by c.session_id order by timestamp;";
        else if(gender==='m' || gender === 'f')
            var query = "select c.session_id as session_number, u.first_name as first_name, u.last_name as last_name, c.timestamp as session_start_ts, max(c.timestamp) as session_end_ts, count(if(c.message_type='IN',1,null)) as message_in_count, count(if(c.message_type_flag='INQ',1,null)) as nlp_query_count, count(if(c.message_type_flag='II',1,null)) as vision_query_count, count(if(c.message_type='OB' or c.message_type='OH',1,null)) as message_out_count, count(c.message_type) as total_messages from user u join chatlog c on u.user_id = c.user_id where u.gender = '"+gender+"' and date(c.timestamp)>='"+start+"' and date(c.timestamp)<='"+end+"' group by c.session_id order by timestamp;";
        else
            var query = "select c.session_id as session_number, u.first_name as first_name, u.last_name as last_name, c.timestamp as session_start_ts, max(c.timestamp) as session_end_ts, count(if(c.message_type='IN',1,null)) as message_in_count, count(if(c.message_type_flag='INQ',1,null)) as nlp_query_count, count(if(c.message_type_flag='II',1,null)) as vision_query_count, count(if(c.message_type='OB' or c.message_type='OH',1,null)) as message_out_count, count(c.message_type) as total_messages from user u join chatlog c on u.user_id = c.user_id where u.gender = '' and date(c.timestamp)>='"+start+"' and date(c.timestamp)<='"+end+"' group by c.session_id order by timestamp;";
    }
    else{
        var query = "select c.session_id as session_number, u.first_name as first_name, u.last_name as last_name, c.timestamp as session_start_ts, max(c.timestamp) as session_end_ts, count(if(c.message_type='IN',1,null)) as message_in_count, count(if(c.message_type_flag='INQ',1,null)) as nlp_query_count, count(if(c.message_type_flag='II',1,null)) as vision_query_count, count(if(c.message_type='OB' or c.message_type='OH',1,null)) as message_out_count, count(c.message_type) as total_messages from user u join chatlog c on u.user_id = c.user_id where u.user_id ='"+userId+"' and date(c.timestamp)>='"+start+"' and date(c.timestamp)<='"+end+"' group by c.session_id order by timestamp;";
    }

    database.executeQuery(query,function(err,data){
        winston.info("query: " + query) ;
        if(err){
            winston.error(err);
            return ;
        }
        if(data.rows==null)
            winston.info("No Data Present");
        var data1 = [] ;
        var i;
        for(i=0;i<data.rows.length;i++){
            data1.push([
                data.rows[i].first_name,
                data.rows[i].last_name,
                '<a href="../sessiontranscriptvisualizer?session_id='+data.rows[i].session_number+'">'+moment(data.rows[i].session_start_ts).format('YYYY-MM-DD, h:mm:ss a')+'</a>',
                moment(data.rows[i].session_end_ts).format('YYYY-MM-DD, h:mm:ss a'),
                data.rows[i].message_in_count,
                data.rows[i].nlp_query_count,
                data.rows[i].vision_query_count,
                data.rows[i].message_out_count,
                data.rows[i].total_messages
            ])
        }
        response.send(data1) ;

    })

};

/*this function sends data for session transcript*/
exports.getSessionTranscript = function(request,response){
    var session_id = request.body.session_id;
    var query = "select user.profile_pic_link as user_profile_img, user.first_name as user_name, session_id as session_id,message_type as message_type,message_number as message_number,message_chat as message_chat, product_response_list as product_response_list, timestamp as timestamp from user join chatlog on user.user_id = chatlog.user_id where chatlog.session_id = '"+session_id+"' order by timestamp, message_number ;"
    winston.info("Session ID: " + session_id) ;
    database.executeQuery(query,function(err,data){
        winston.info("query: " + query) ;
        if(err){
            winston.error(err);
            return err;
        }
        var chat = [];
        for(var i =0;i<data.rows.length;i++){
            chat.push({
                timestamp:moment(data.rows[i].timestamp).format('dddd, MMMM Do YYYY, h:mm:ss a'),
                user_name:data.rows[i].user_name,
                user_profile_img:data.rows[i].user_profile_img,
                session_id:data.rows[i].session_id,
                message_number:data.rows[i].message_number,
                message_chat:data.rows[i].message_chat,
                message_type:data.rows[i].message_type,
                product_response_list:JSON.parse(data.rows[i].product_response_list)
            });
        }
        response.send('{"messages":'+JSON.stringify(chat)+'}');
    });
};
