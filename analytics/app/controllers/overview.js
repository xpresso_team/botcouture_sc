var request = require('request');
var moment = require('moment');
var database = require('./database');
var winston = require('../../LoggerUtil.js');

/* This function sends total User Count data */
exports.getTotalUsers = function(request, response) {
    //winston.info(request.body);

    /* Start date, end date and gender from frontend */
    var start = request.body.start;
    var end = request.body.end;
    var gender = request.body.gender ;

    /*To get percent change*/
    var interval = moment(end).diff(moment(start),'days');
    var prevStart = moment(start).subtract(interval,'days').format('YYYY-MM-DD');

    winston.info("getTotalUsers Tab1");
    winston.info("start: " + start + " end: " + end + " gender: " + gender + " interval: " + interval + " prevStart: " + prevStart) ;

    /* Query1 gets total user and query2 gets total user change */

    if(gender=="all") {
        var query1 = "SELECT count(distinct user_id) as count FROM chatlog where date(timestamp)>='" + start + "' and date(timestamp)<='" + end + "';";
        var query2 = "SELECT count(distinct user_id) as count FROM chatlog where date(timestamp)>='" + prevStart + "' and date(timestamp)<='" + start + "';";
    }
    else if(gender==='m' || gender==='f'){
        var query1 = "SELECT count(distinct c.user_id) as count FROM chatlog c join user u on c.user_id = u.user_id where u.gender = '"+gender+"' and date(timestamp)>='" + start + "' and date(timestamp)<='" + end + "';";
        var query2 = "SELECT count(distinct c.user_id) as count FROM chatlog c join user u on c.user_id = u.user_id where u.gender = '"+gender+"' and date(timestamp)>='" + prevStart + "' and date(timestamp)<='" + start + "';";
    }
    else{
        var query1 = "SELECT count(distinct c.user_id) as count FROM chatlog c join user u on c.user_id = u.user_id where u.gender = '' and date(timestamp)>='" + start + "' and date(timestamp)<='" + end + "';";
        var query2 = "SELECT count(distinct c.user_id) as count FROM chatlog c join user u on c.user_id = u.user_id where u.gender = '' and date(timestamp)>='" + prevStart + "' and date(timestamp)<='" + start + "';";
    }

    database.executeQuery(query1,function(err,data1) {
        winston.info("query1: " +query1) ;
        if(err){
            winston.error(err);
            return;
        }
        database.executeQuery(query2,function(err,data2) {
            winston.info("query2: " +query2);
            if(err){
                winston.error(err);
                return;
            }
            var percentChange = ((data1.rows[0].count-data2.rows[0].count)/data2.rows[0].count)*100;
            var data = [data1.rows[0].count,percentChange];
            response.send(data);
        });
    });
};

/* This function sends total Session Count data */
exports.getTotalSession = function(request, response) {
    //winston.info(request.body);
    var start = request.body.start;
    var end = request.body.end;
    var gender = request.body.gender ;

    var interval = moment(end).diff(moment(start),'days');
    var prevStart = moment(start).subtract(interval,'days').format('YYYY-MM-DD');

    winston.info("getTotalSession Tab1");
    winston.info("start: " + start + " end: " + end + " gender: " +gender + " interval: " +interval + " prevStart: " +prevStart);

    /* Query1 gets total session and query2 gets total session change */

    if(gender==="all") {
        var query1 = "SELECT COUNT(DISTINCT session_id) AS ses FROM chatlog where date(timestamp)>='" + start + "' and date(timestamp)<='" + end + "';";
        var query2 = "SELECT COUNT(DISTINCT session_id) AS ses FROM chatlog where date(timestamp)>='" + prevStart + "' and date(timestamp)<='" + start + "';";
    }
    else if(gender==='m' || gender==='f'){
        var query1 = "SELECT COUNT(DISTINCT c.session_id) AS ses FROM chatlog c join user u on u.user_id = c.user_id where u.gender = '"+gender+"' and date(timestamp)>='" + start + "' and date(timestamp)<='" + end + "';";
        var query2 = "SELECT COUNT(DISTINCT c.session_id) AS ses FROM chatlog c join user u on u.user_id = c.user_id where u.gender = '"+gender+"' and date(timestamp)>='" + prevStart + "' and date(timestamp)<='" + start + "';";
    }
    else{
        var query1 = "SELECT COUNT(DISTINCT c.session_id) AS ses FROM chatlog c join user u on u.user_id = c.user_id where u.gender = '' and date(timestamp)>='" + start + "' and date(timestamp)<='" + end + "';";
        var query2 = "SELECT COUNT(DISTINCT c.session_id) AS ses FROM chatlog c join user u on u.user_id = c.user_id where u.gender = '' and date(timestamp)>='" + prevStart + "' and date(timestamp)<='" + start + "';";
    }

    database.executeQuery(query1,function(err,data1) {
        winston.info("query1: " + query1);
        if(err){
            winston.error(err);
            return;
        }
        database.executeQuery(query2,function(err,data2) {
            winston.info("query2: " +query2) ;
            if(err){
                winston.error(err);
                return;
            }
            var percentChange = ((data1.rows[0].ses-data2.rows[0].ses)/data2.rows[0].ses)*100;
            var data = [data1.rows[0].ses,percentChange];
            response.send(data);
        });
    });
};

/* This function sends average session time data */

exports.getAvgSession = function(request, response) {
    winston.info(request.body);
    var start = request.body.start;
    var end = request.body.end;
    var gender = request.body.gender ;

    var interval = moment(end).diff(moment(start),'days');
    var prevStart = moment(start).subtract(interval,'days').format('YYYY-MM-DD');

    winston.info("getAvgSession Tab1");
    winston.info("start: " + start + " end: " + end + " gender: " +gender + " interval: " +interval + " prevStart: " +prevStart);

    /* Query1 gets average session time in minutes and query2 gets average session time change */

    if(gender=="all") {
        var query1 = "select avg(diff)/100 as avglen from (select max(timestamp)-min(timestamp) as diff from chatlog where date(timestamp)>='" + start + "' and date(timestamp)<='" + end + "'and session_id <> 'None' group by session_id) as avgses;";
        var query2 = "select avg(diff)/100 as avglen from (select max(timestamp)-min(timestamp) as diff from chatlog where date(timestamp)>='" + prevStart + "' and date(timestamp)<='" + start + "'and session_id <> 'None' group by session_id) as avgses;";
    }
    else if(gender==='m'||gender==='f'){
        var query1 = "select avg(diff)/100 as avglen from (select max(timestamp)-min(timestamp) as diff from chatlog c join user u on c.user_id = u.user_id where u.gender = '"+gender+"' and date(timestamp)>='" + start + "' and date(timestamp)<='" + end + "'and session_id <> 'None' group by session_id) as avgses;";
        var query2 = "select avg(diff)/100 as avglen from (select max(timestamp)-min(timestamp) as diff from chatlog c join user u on c.user_id = u.user_id where u.gender = '"+gender+"' and date(timestamp)>='" + prevStart + "' and date(timestamp)<='" + start + "'and session_id <> 'None' group by session_id) as avgses;";
    }
    else{
        var query1 = "select avg(diff)/100 as avglen from (select max(timestamp)-min(timestamp) as diff from chatlog c join user u on c.user_id = u.user_id where u.gender = '' and date(timestamp)>='" + start + "' and date(timestamp)<='" + end + "'and session_id <> 'None' group by session_id) as avgses;";
        var query2 = "select avg(diff)/100 as avglen from (select max(timestamp)-min(timestamp) as diff from chatlog c join user u on c.user_id = u.user_id where u.gender = '' and date(timestamp)>='" + prevStart + "' and date(timestamp)<='" + start + "'and session_id <> 'None' group by session_id) as avgses;";
    }
    database.executeQuery(query1,function(err,data1) {
        winston.info("query1: " +query1) ;
        if(err){
            winston.error(err);
            return;
        }
        database.executeQuery(query2,function(err,data2) {
            winston.info("query2: " +query2);
            if(err){
                winston.error(err);
                return;
            }
            var percentChange = ((data1.rows[0].avglen-data2.rows[0].avglen)/data2.rows[0].avglen)*100;
            var data = [data1.rows[0].avglen,percentChange];

            //console.log(data);
            response.send(data);
        });
    });
};

/* This function sends new User count data */

exports.getNewUsers = function(request, response) {
    var start = request.body.start;
    var end = request.body.end;
    var gender = request.body.gender ;
    var interval = moment(end).diff(moment(start),'days');
    var prevStart = moment(start).subtract(interval,'days').format('YYYY-MM-DD');


    winston.info("getNewUSers Tab1");
    winston.info("start: " + start + " end: " + end + " gender: " +gender + " interval: " +interval + " prevStart: " +prevStart);
    /* Query1 gets new user in the given interval and query2 gets new user change */

    if(gender=="all") {
        var query1 = "select count(distinct user_id) as newUsers from user where date(first_active_ts)>='" + start + "'and date(first_active_ts) <= '" + end + "' ;";

        // var query1 = "select count(distinct user_id) as newUsers from user where first_active_ts between'" + start + "'and'" + end + "' ;";

        var query2 = "select count(distinct user_id) as newUsers from user where date(first_active_ts)>='" + prevStart + "'and date(first_active_ts) <= '" + start + "' ;";
    }
    else if(gender==='m'||gender==='f'){
        var query1 = "select count(distinct user_id) as newUsers from user where gender = '"+gender+"' and date(first_active_ts)>='" + start + "'and date(first_active_ts) <= '" + end + "' ;";

        // var query1 = "select count(distinct user_id) as newUsers from user where gender = '"+gender+"' and first_active_ts between'" + start + "'and'" + end + "' ;";

        var query2 = "select count(distinct user_id) as newUsers from user where gender = '"+gender+"' and date(first_active_ts)>='" + prevStart + "'and date(first_active_ts) <= '" + start + "' ;";
    }
    else{
        var query1 = "select count(distinct user_id) as newUsers from user where gender = '' and date(first_active_ts)>='" + start + "'and date(first_active_ts) <= '" + end + "' ;";
        var query2 = "select count(distinct user_id) as newUsers from user where gender = '' and date(first_active_ts)>='" + prevStart + "'and date(first_active_ts) <= '" + start + "' ;";
    }
    database.executeQuery(query1,function(err,data1) {
        winston.info("query1: " + query1) ;
        if(err){
            winston.err(err);
            return;
        }
        database.executeQuery(query2,function(err,data2) {
            winston.info("query2: " +query2) ;
            if(err){
                winston.error(err);
                return;
            }
            var percentChange = ((data1.rows[0].newUsers-data2.rows[0].newUsers)/data2.rows[0].newUsers)*100;
            var data = [data1.rows[0].newUsers,percentChange];
            response.send(data);
        });
    });
};

/* This function sends data for drilldown pie chart of message type */

exports.getPieChartData = function(request, response) {
    //winston.info(request.body);
    var start = request.body.start;
    var end = request.body.end;
    var gender = request.body.gender ;

    winston.info("getPieChartData Tab1");
    //var query = "select count(*) as count from chatlog where date(timestamp)>='"+start+"' and date(timestamp)<='"+end+"' group by message_type_flag;";
    //var query = "select count(if(message_type_flag = 'IT',1,null)) as countIT, count(if(message_type_flag = 'IQR',1,null)) as countIQR, count(if(message_type_flag = 'IN',1,null)) as countIN, count(if(message_type_flag = 'II',1,null)) as countII, count(if(message_type_flag = 'INQ',1,null)) as countINQ, count(if(message_type_flag = 'OT',1,null)) as countOT, count(if(message_type_flag='ONR',1,null)) as countONR, count(if(message_type_flag='OVR')) as countOVR, count(if(message_type_flag='OQR')) as countOQR, count(if(message_type_flag='POS')) as countPOS from chatlog where date(timestamp)>='"+start+"' and date(timestamp) <= '"+end+"' ;"

    /* query to get messages of all flags (message_type_flag) */
    if(gender==="all")
        var query = "select count(if(message_type_flag = 'IT',1,null)) as countIT, count(if(message_type_flag = 'IQR',1,null)) as countIQR, count(if(message_type_flag = 'IN',1,null)) as countIN, count(if(message_type_flag = 'II',1,null)) as countII, count(if(message_type_flag = 'INQ',1,null)) as countINQ, count(if(message_type_flag='IB',1,null)) as countIB, count(if(message_type_flag = 'OT',1,null)) as countOT, count(if(message_type_flag='ONR',1,null)) as countONR, count(if(message_type_flag='OVR',1,null)) as countOVR, count(if(message_type_flag='OQR',1,null)) as countOQR, count(if(message_type_flag='POS',1,null)) as countPOS from chatlog where date(timestamp)>='"+start+"' and date(timestamp)<= '"+end+"' ;"
    else if(gender==='m'||gender==='f')
        var query = "select count(if(message_type_flag = 'IT',1,null)) as countIT, count(if(message_type_flag = 'IQR',1,null)) as countIQR, count(if(message_type_flag = 'IN',1,null)) as countIN, count(if(message_type_flag = 'II',1,null)) as countII, count(if(message_type_flag = 'INQ',1,null)) as countINQ, count(if(message_type_flag='IB',1,null)) as countIB, count(if(message_type_flag = 'OT',1,null)) as countOT, count(if(message_type_flag='ONR',1,null)) as countONR, count(if(message_type_flag='OVR',1,null)) as countOVR, count(if(message_type_flag='OQR',1,null)) as countOQR, count(if(message_type_flag='POS',1,null)) as countPOS from chatlog c join user u on c.user_id = u.user_id where u.gender = '"+gender+"' and date(timestamp)>='"+start+"' and date(timestamp) <= '"+end+"' ;"
    else
        var query = "select count(if(message_type_flag = 'IT',1,null)) as countIT, count(if(message_type_flag = 'IQR',1,null)) as countIQR, count(if(message_type_flag = 'IN',1,null)) as countIN, count(if(message_type_flag = 'II',1,null)) as countII, count(if(message_type_flag = 'INQ',1,null)) as countINQ, count(if(message_type_flag='IB',1,null)) as countIB, count(if(message_type_flag = 'OT',1,null)) as countOT, count(if(message_type_flag='ONR',1,null)) as countONR, count(if(message_type_flag='OVR',1,null)) as countOVR, count(if(message_type_flag='OQR',1,null)) as countOQR, count(if(message_type_flag='POS',1,null)) as countPOS from chatlog c join user u on c.user_id = u.user_id where u.gender = '' and date(timestamp)>='"+start+"' and date(timestamp) <= '"+end+"' ;"


    database.executeQuery(query,function(err,data) {
        winston.info("query: " + query) ;
        if(err){
            winston.error(err);
            return;
        }

        /* All incoing messages flag */
        var inText = (data.rows.length>0)?data.rows[0].countIT:100 ;
        var inQR = (data.rows.length>0)?data.rows[0].countIQR:0 ;
        var inEmoji = (data.rows.length>0)?data.rows[0].countIN:0 ;
        var inImage = (data.rows.length>0)?data.rows[0].countII:0 ;
        var inNLP = (data.rows.length>0)?data.rows[0].countINQ:0 ;
        var inIB = (data.rows.length>0)?data.rows[0].countIB:0 ;

        /* calculating incoming messages */
        var incoming = inText + inQR + inEmoji + inImage + inNLP + inIB;
        /* All outgoing messages flag */
        var outText = (data.rows.length>0)?data.rows[0].countOT : 0 ;
        var outNLP = (data.rows.length>0)?data.rows[0].countONR : 0 ;
        var outVision = (data.rows.length>0)?data.rows[0].countOVR : 0 ;
        var outQR = ((data.rows.length>0)?data.rows[0].countOQR : 0) ;
        var postback = ((data.rows.length>0)?data.rows[0].countPOS : 0) ;

        /* calculating outgoing messages */
        var outgoing = outText + outNLP + outQR + outVision + postback ;

        var outpercent = outgoing ;
        var inpercent = incoming ;

        var outTextPer = outText ;
        var outNlpPer = outNLP ;
        var outQRPer = outQR ;
        var postbackPer = postback;
        var outVisionPer = outVision ;

        var inTextPer = inText ;
        var inQRPer = inQR ;
        var inEmojiPer = inEmoji ;
        var inImagePer = inImage ;
        var inNLPPer = inNLP ;
        var inIBPer =  inIB ;

        var data = [{
            name: 'Outgoing',
            y: outpercent,
            drilldown: 'Outgoing'
            },{
            name: 'Incoming',
            y: inpercent,
            drilldown: 'Incoming'}];

        var drillData = [{
            name: 'Outgoing',
            id: 'Outgoing',
            data : [
                ['Out Text',outTextPer],
                ['Out NLP Response',outNlpPer],
                ['Out Quick Reply',outQRPer],
                ['Out Vision Result', outVisionPer],
                ['Postback',postbackPer]
            ]
        },
        {
            name: 'Incoming',
            id: 'Incoming',
            data : [
                ['In Text',inTextPer],
                ['In Quick Reply',inQRPer],
                ['In Emoji',inEmojiPer],
                ['In NLP Query',inNLPPer],
                ['Input Not Recognized', inIBPer],
                ['In Image Query',inImagePer]]
            }
        ];

        var retData = [];
        retData[0] = data ;
        retData[1] = drillData ;

        response.send(retData);
    });

};

/* Function to get user line chart */
exports.getLineChartData = function(request, response) {
    //winston.info(request.body);
    var start = request.body.start ;
    var type = request.body.type;
    var end = request.body.end ;
    var gender = request.body.gender ;

    winston.info("getLineChartData Tab1");

    /* gets engaged user count : number of distinct users who have sent or received a msg */
    if(gender==="all")
        var query = "SELECT count(distinct user_id) as usercount, DATE(timestamp) as date from chatlog where date(timestamp) >='"+start+"' and date(timestamp) <='"+end+"' group by DATE(timestamp) order by DATE(timestamp);" ;
    else if(gender==='m'||gender==='f')
        var query = "SELECT count(distinct c.user_id) as usercount, DATE(timestamp) as date from chatlog c join user u on u.user_id = c.user_id where u.gender = '"+gender+"' and date(timestamp) >='"+start+"' and date(timestamp) <='"+end+"' group by DATE(timestamp) order by DATE(timestamp);" ;
    else
        var query = "SELECT count(distinct c.user_id) as usercount, DATE(timestamp) as date from chatlog c join user u on u.user_id = c.user_id where u.gender = '' and date(timestamp) >='"+start+"' and date(timestamp) <='"+end+"' group by DATE(timestamp) order by DATE(timestamp);" ;

    winston.info("start: " + start + " end: " +end + " type: " +type + " gender: " + gender);

    database.executeQuery(query,function(err,data) {
        winston.info("query: " + query) ;
        if(err){
            winston.error(err);
            return;
        }

        var startDate = moment(start);
        var endDate=moment(end);
        var dataArray = [],dataArrayDayWise=[],dataArrayWeekWise=[],dataArrayMonthWise=[];
        var i=0,j=0;

        while(startDate.isBefore(endDate)){
            var testDate = (j<data.rows.length)?(moment(data.rows[j].date).format('YYYY-MM-DD')):null;
            if(startDate.format('YYYY-MM-DD').localeCompare(testDate)===0){
            dataArray[i] = data.rows[j].usercount;
            j++;
        }
        else
            dataArray[i] = 0;
            i++;
            startDate.add(1,'days');
        }

        /*daily data*/
        dataArrayDayWise=dataArray;

        /*to get weekly data*/
        for(var i=0,data=dataArray.slice(0);i<dataArray.length;i+=7){
            for(var j=i+1;j<(i+7)&&j<dataArray.length;j++){
                data[i]+=data[j];
            }
            dataArrayWeekWise.push(data[i]);
        }
        dataArrayWeekWise.push(0);

        /*to get monthly data*/
        for(var i=0,data=dataArray.slice(0);i<dataArray.length;i+=30){
            for(var j=i+1;j<(i+30)&&j<dataArray.length;j++){
                data[i]+=data[j];
            }
            dataArrayMonthWise.push(data[i]);
        }
        dataArrayMonthWise.push(0);

        var data = [{
        name: 'Engaged User Count'
        }];

        switch(type){
          case 'Day': data[0].data=dataArrayDayWise;
              break;
          case 'Week': data[0].data=dataArrayWeekWise;
              break;
          case 'Month': data[0].data=dataArrayMonthWise;
              break;
        }
        response.send(data);

  });
};
