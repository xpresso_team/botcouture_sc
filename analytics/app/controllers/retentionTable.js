var request = require('request');
var moment = require('moment');
var database = require('./database');
var winston = require('../../LoggerUtil.js');

exports.getNewUserRetentionTable = function(request,response){
	var start_date = moment(request.body.start).format('YYYY-MM-DD');
	var end_date = moment(request.body.end).format('YYYY-MM-DD');

	winston.info("START DATE ===>"+start_date);
	winston.info("END DATE ====>"+end_date);
    winston.info("In Retention Table New User Tab") ;

	var new_user_query = "select user_id as new_user_id, date(first_active_ts) as new_user_date from user where first_active_ts between '"+start_date+"' and '"+end_date+"' order by first_active_ts;";
	var total_user_query = "select distinct user_id as total_user_id, date(timestamp) as total_user_date from chatlog where timestamp between '"+start_date+"' and '"+end_date+"' order by user_id, timestamp;";
	database.executeQuery(new_user_query,function(err,data1){
		winston.info("new_user_query: " + new_user_query) ;
	    if(err){
			winston.error(err);
			return;
		}

		database.executeQuery(total_user_query,function(err,data2){
			winston.info("total_user_query: " + total_user_query) ;
		    if(err){
				winston.error(err);
				return;
			}
			var arr = new Array(moment(end_date).diff(moment(start_date),'days')+1),date;

			for(i=0,date=moment(start_date);i<=moment(end_date).diff(moment(start_date),'days');i++,date.add(1,'days')){
				arr[i]=[date.format('YYYY-MM-DD'),0,0,0,0,0,0,0,0];

			}

			for (i=0;i<data1.rows.length;i++){
				var l=0, r=data2.rows.length;
				var mid;
				while(l<=r){
					mid = Math.floor((l+r)/2);
					if(data2.rows[mid].total_user_id.localeCompare(data1.rows[i].new_user_id)==0)
						break;
					if(data2.rows[mid].total_user_id.localeCompare(data1.rows[i].new_user_id)<0)
						l=mid+1;
					if(data2.rows[mid].total_user_id.localeCompare(data1.rows[i].new_user_id)>0)
						r=mid-1
				}
				winston.info("Found @ "+mid+"  "+data2.rows[mid].total_user_id+"   "+moment(data2.rows[mid].total_user_date).format('YYYY-MM-DD')+"    "+data1.rows[i].new_user_id+"  "+moment(data1.rows[i].new_user_date).format('YYYY-MM-DD'));
				while((mid>0)&&(data2.rows[mid].total_user_id.localeCompare(data1.rows[i].new_user_id)==0)){
					//winston.info("This value is also present at :"+mid+" "+data2.rows[mid].total_user_id+"   "+moment(data2.rows[mid].total_user_date).format('YYYY-MM-DD'));
					mid--;
				}
				mid++;




				while((mid<data2.rows.length)&&(data2.rows[mid].total_user_id.localeCompare(data1.rows[i].new_user_id)==0)){
					var row = moment(data1.rows[i].new_user_date).diff(moment(start_date),'days');
					var col = moment(data2.rows[mid].total_user_date).diff(moment(data1.rows[i].new_user_date),'days')+1;

					if(col<=7)
						arr[row][col]++;
					winston.info("Current Row and Column :"+row+"  "+col+" and array value at row:col ");
					mid++;

				}
			}
            for(var i=0;i<arr.length;i++){
                for(var j=0;j<=7;j++){
                    if(j>=2) {
                        if(arr[i][1]!==0)arr[i][j] = (Math.floor((arr[i][j] / arr[i][1]) * 10000)/100)+'%';
                    }
                }
            }
			response.send(arr);

		})

	})

};

exports.getExistingUserRetentionTable = function(request,response){
    var start_date = moment(request.body.start).format('YYYY-MM-DD');
    var end_date = moment(request.body.end).format('YYYY-MM-DD');

    winston.info("START DATE ===>"+start_date);
    winston.info("END DATE ====>"+end_date);
    winston.info("In Retention Table Existing User ") ;

    var existing_user_query = "select total_user as existing_user_id, timestamp as existing_user_date from(select distinct date(first_active_ts) as first_active_ts, chatlog.user_id as total_user, date(timestamp) as timestamp from chatlog left join user on chatlog.user_id = user.user_id order by timestamp) as temp where timestamp>first_active_ts and timestamp between '"+start_date+"' and '"+end_date+"' order by timestamp;";
    var total_user_query = "select distinct user_id as total_user_id, date(timestamp) as total_user_date from chatlog where timestamp between '"+start_date+"' and '"+end_date+"' order by user_id, timestamp;";

    database.executeQuery(existing_user_query,function(err,data1){
        winston.info("existing_user_query: " + existing_user_query) ;
        if(err){
            winston.error(err);
            return;
        }
        //winston.info(data1);
        database.executeQuery(total_user_query,function(err,data2){
            winston.info("total_user_query: " + total_user_query) ;
            if(err){
                winston.error(err);
                return;
            }
            var arr = new Array(moment(end_date).diff(moment(start_date),'days')+1),date;

            for(i=0,date=moment(start_date);i<=moment(end_date).diff(moment(start_date),'days');i++,date.add(1,'days')){
                arr[i]=[date.format('YYYY-MM-DD'),0,0,0,0,0,0,0,0];

            }
            for (i=0;i<data1.rows.length;i++){


                var l=0, r=data2.rows.length;
                var mid;
                while(l<=r){
                    mid = Math.floor((l+r)/2);
                    if(data2.rows[mid].total_user_id.localeCompare(data1.rows[i].existing_user_id)==0)
                        break;
                    if(data2.rows[mid].total_user_id.localeCompare(data1.rows[i].existing_user_id)<0)
                        l=mid+1;
                    if(data2.rows[mid].total_user_id.localeCompare(data1.rows[i].existing_user_id)>0)
                        r=mid-1
                }
                winston.info("Found @ "+mid+"  "+data2.rows[mid].total_user_id+"   "+moment(data2.rows[mid].total_user_date).format('YYYY-MM-DD')+"    "+data1.rows[i].existing_user_id+"  "+moment(data1.rows[i].existing_user_date).format('YYYY-MM-DD'));
                while((mid>0)&&(data2.rows[mid].total_user_id.localeCompare(data1.rows[i].existing_user_id)==0)){
                    //winston.info("This value is also present at :"+mid+" "+data2.rows[mid].total_user_id+"   "+moment(data2.rows[mid].total_user_date).format('YYYY-MM-DD'));
                    mid--;
                }
                mid++;

                while((mid<data2.rows.length)&&(data2.rows[mid].total_user_id.localeCompare(data1.rows[i].existing_user_id)==0)){
                    var row = moment(data1.rows[i].existing_user_date).diff(moment(start_date),'days');
                    var col = moment(data2.rows[mid].total_user_date).diff(moment(data1.rows[i].existing_user_date),'days')+1;

                    if(col>0&&col<=7)
                        arr[row][col]++;
                    mid++;

                }
             }
            for(var i=0;i<arr.length;i++){
                for(var j=0;j<=7;j++){
                    if(j>=2) {
                        if(arr[i][j]!==0)arr[i][j] = (Math.floor((arr[i][j] / arr[i][1]) * 10000)/100)+'%';
                    }
                }
            }
            response.send(arr);

        })

    })

};
