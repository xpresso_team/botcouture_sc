// Load module
var mysql = require('mysql');
var configuration = require('../../config/index.js')(process.argv[2]);
var winston = require('../../LoggerUtil.js');

// Initialize pool
var pool = mysql.createPool({
    connectionLimit : configuration.get('connectionLimit'),
    host     : configuration.get('host'),
    port     : configuration.get('port'),
    user     : configuration.get('dbuser'),
    password : configuration.get('password'),
    database : configuration.get('database'),
    debug    : configuration.get('debug')
});

exports.changeDatabase = function (database,callback) {
	pool.end()
	pool = mysql.createPool({
		connectionLimit : configuration.get('connectionLimit'),
		host     : configuration.get('host'),
		port     : configuration.get('port'),
		user     : configuration.get('dbuser'),
		password : configuration.get('password'),
		database : database,
		debug    : configuration.get('debug')
	});
	callback()
}

exports.executeQuery = function(query,callback){
	try{
		pool.getConnection(function(err,connection){
			if (err) {
    			winston.error(err);
				if(connection!='undefined'){
          connection.release();
        }
				throw err;
			}
			connection.query(query,function(err,rows){
			   /* if(err && connection && connection !=="null" && connection !=="undefined"){
                    console.log("Null connection released in query : "+err);
                    connection.release();
                }*/
                connection.release();
				if(!err) {
						callback(null, {rows: rows});
				}
			});
			connection.on('error', function(err) {
				throw err;

			});
		});
	}catch(err){
		callback(err,null);
	}
};
