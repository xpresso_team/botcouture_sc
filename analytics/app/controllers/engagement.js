var request = require('request');
var moment = require('moment');
var database = require('./database');
var winston = require('../../LoggerUtil.js');

/* this function sends most active hours data */
exports.getEngagement = function(request,response){
	var start = request.body.start ;
	var end = request.body.end ;
	var gender = request.body.gender ;
	winston.info("Engagement Tab") ;
	winston.info("start: " + start + " end: " + end + " gender: " + gender) ;

	/* gets count of messages in every 30 min intervals */
	if(gender==="all")
	    var query = "select concat(date(timestamp),' ',sec_to_time(time_to_sec(timestamp)-time_to_sec(timestamp)%(30*60) + (30*60))) as new_ts, count(if(session_id,1,null)) as session_count from chatlog where date(timestamp) >= '"+start+"' and date(timestamp)<='"+end+"' group by time(new_ts) order by time(new_ts) ;";
	else if(gender==='m' || gender==='f')
        var query = "select concat(date(timestamp),' ',sec_to_time(time_to_sec(timestamp)-time_to_sec(timestamp)%(30*60) + (30*60))) as new_ts, count(if(session_id,1,null)) as session_count from chatlog c join user u on c.user_id = u.user_id where gender = '"+gender+"' and date(timestamp) >= '"+start+"' and date(timestamp)<='"+end+"' group by time(new_ts) order by time(new_ts) ;" ;
	else
		var query = "select concat(date(timestamp),' ',sec_to_time(time_to_sec(timestamp)-time_to_sec(timestamp)%(30*60) + (30*60))) as new_ts, count(if(session_id,1,null)) as session_count from chatlog c join user u on c.user_id = u.user_id where gender = '' and date(timestamp) >= '"+start+"' and date(timestamp)<='"+end+"' group by time(new_ts) order by time(new_ts) ;" ;

	/*finds the maximum count of messages in 30 min interval */
	function findMax(data){
		var i = 0;
		var max = data[0][1] ;
		for(i=1;i<data.length;i++){
			if(data[i][1]>max)
				max = data[i][1] ;
		}
		return max ;
	}	

	/*gets sum of messages within indices startIndex and endIndex*/
	function findSum(data,startIndex,endIndex){
		var i ;
		var sum = 0;
		for(i=startIndex;i<endIndex+1 ; i++)
			sum += data[i][1] ;
		return sum ;
	}

	/*sorts the average count messages in descending order*/
	function findMaxAvg(data,startArray,endArray){
		var i ;
		var maxAvg = 0;
		var avg = [];
		for(i=0;i<startArray.length;i++){
			avg.push([ data[startArray[i]][0], data[endArray[i]][0], findSum(data,startArray[i],endArray[i])/(endArray[i]-startArray[i]+1) ]) ;
		}

		avg.sort(function(a,b){
			if(a[2]===b[2])
				return 0 ;
			else 
				return (a[2] < b[2]) ? 1:-1 ;
		}) ;

		return avg ;		
	}

	/*function to find most active time */
	function findEngagement(data){
	    /* find maximum count */
		var max = findMax(data) ;

		/*thresholds set*/
		var threshold = 0.2 * max ;
		/*if count of message is less than 0.75*max, we will stop there and increase start and end*/
		var threshold2 = 0.75 * max ;
		winston.info("threshold " + threshold) ;
		winston.info("threshold2 " + threshold2) ;
		var i ;
		var start = 0;
		var end = 0;
		/*stores start indices*/
		var startArray = [] ;
		/*stores end indices*/
		var endArray = [] ;
		var avg = [] ;
		//startArray.push(0) ;
		while(end+1<data.length){

		    /*if consecutive counts are greater than threshold, we will stop, push start and end indices and update start and end*/
			if(Math.abs(data[end][1]-data[end+1][1]) > threshold){
				startArray.push(start) ;
				endArray.push(end) ;
				start = end + 1 ;
				end = start ; 
			}

			/*if end data is less than threshold2, we update start and end indices*/
			else if(data[end][1]<threshold2){
				winston.info("IN 2 data[start] " + data[start]) ;
				winston.info("IN 2 data[end] " + data[end]) ;
				start++ ;  
				end++ ;
			}

			else{
				winston.info("data start " + data[start]) ;
				winston.info("data end " + data[end]) ;	
				end ++ ;
			}
		
		}
		
		winston.info("startArray " + startArray) ;
		winston.info("endArray " + endArray) ;

		avg = findMaxAvg(data,startArray,endArray) ;
		winston.info("startTime endTime " + moment(avg[0][0]).subtract(30,'minutes').format('h:mm:ss ') +" "+ moment(avg[0][1]).format('h:mm:ss ') )  ;

		/* top nrows are sent to frontend */
		var nrows = 3; 

		return avg.slice(0,nrows) ;
	}

	database.executeQuery(query,function(err,data){
		winston.info("query: " + query) ;
	    var engageArray = [] ;
		if(err){
			winston.error(err) ;
			return ;
		}
		try{

			if(data.rows.length == 0){
				winston.info("No data available");
				//console.log("No data available");
			}

			var data1 = [] ;
			var i ;
			for(i=0;i<data.rows.length;i++){
				//data1.push(data.rows[i].new_ts), data1.push(data.rows[i].session_count)] ;
				data1.push( [ data.rows[i].new_ts, data.rows[i].session_count ] )
			}

			engageArray = [] ;

			if(data1.length>2) {
				engageArray = findEngagement(data1);
				winston.info("engageArray " + engageArray);
				//console.log(engageArray);
			}
			response.send(engageArray) ;
			//console.log(engageArray);
		}catch(err){
			response.send(engageArray);
		}
	})
};