var request = require('request');
var moment = require('moment');
var database = require('./database');
var winston = require('../../LoggerUtil.js');

exports.getGoalFunnelsData = function(request, response){
    // var start = request.body.start;
    // var end = request.body.end;

    winston.info("getFunnelData");

    code_search_action = "1_1%";
    code_filter = "1_2";
    code_complete_order = "1_3%";

    var q1 = 'select count(action_code) as search_action from action where action_code LIKE "' + code_search_action + '"';
    var q2 = 'select count(action_code) as filter_action from action where action_code LIKE "' + code_filter + '"';
    var q3 = 'select count(action_code) as complete_order from action where action_code LIKE "' + code_complete_order + '"';

    var query = 'select search_action, filter_action, complete_order from ('+q1+') a, ('+q2+') b, ('+q3+') c;';

    winston.info(query);

    database.executeQuery(query, function(err,data) {
        winston.info("query: " + query);
        if(err){
            winston.error("error: "+err);
            return;
        }

        winston.info("data: "+JSON.stringify(data.rows[0]));

        // funnelData = { 
        //     name: 'Count',
        //     data: []
        // };    

        // for (var key in data.rows[0]){
        //     funnelData.data.push(new Array(key, data.rows[0][key]));
        // }

        // winston.info("funnelData: "+JSON.stringify(funnelData));

        var funnelData = [];
        for(var key in data.rows[0]){
            funnelData.push(data.rows[0][key]);
        }
        winston.info("funnelData: "+funnelData);
        response.send(funnelData);
        // response.send(data.rows[0]);
    });

};