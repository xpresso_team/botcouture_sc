var forever = require('forever-monitor');
var fs = require('fs');
var nodemailer = require("nodemailer");
//var transport = nodemailer.createTransport('direct', { debug: false });
var winston = require('./LoggerUtil.js');

var child = new (forever.Monitor)('server.js', {
    max: 10,
    silent: false,
    outFile: 'logs/out',
    errFile: 'logs/err',
    append: true,
    args: [process.argv[2]],
    killTree: true,
    sourceDir: ''
});

child.on('start', function () {
    fs.writeFileSync('app/server.pid', child.childData.pid, 'utf8');
});

child.on('exit', function () {
    winston.log('Server has exited after 10 restarts');
});

/*child.on('restart', function() {
    fs.writeFileSync('server.pid', child.childData.pid, 'utf8');
    var msg = 'Forever restarting script for ' + child.times + ' time\nBot details\t' + child.childData.args.toString();
    transport.sendMail({
    from: "vivekadityapurella@gmail.com",
    to: "vivek.aditya@abzooba.com",
    subject: "Chatbot restarted at\t"+ new Date(),
    text: "Restart Mail",
    html: "<b>"+msg+"</b>"
    }, winston.error);

});
*/
child.start();
