var express = require('express');
var router = express.Router();
var tab2 = require('../controllers/activity');

router.route('/getUserCount').post(tab2.getUserCount);
router.route('/getActivity').post(tab2.getActivity);
router.route('/getMsgCount').post(tab2.getMsgCount);

module.exports = router;