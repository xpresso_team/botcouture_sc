var express = require('express');
var router = express.Router();

var tab6 = require('../controllers/topMessages');

router.route('/getMessageIn').post(tab6.getMessageIn);
router.route('/getMessageOut').post(tab6.getMessageOut);
router.route('/getMessageFunnel').post(tab6.getMessageFunnel);

module.exports = router;