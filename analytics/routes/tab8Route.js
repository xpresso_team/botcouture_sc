var express = require('express');
var router = express.Router();
var tab8 = require('../controllers/sessionTable');

router.route('/getSessionTable').post(tab8.getSessionTable);
router.route('/getSessionTranscript').post(tab8.getSessionTranscript);

module.exports = router;