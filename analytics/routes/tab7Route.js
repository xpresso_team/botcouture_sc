var express = require('express');
var router = express.Router();
var tab7 = require('../controllers/userTable');

router.route('/getUserTable').post(tab7.getUserTable);

module.exports = router;