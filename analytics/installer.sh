#!/usr/bin/env bash
sudo su
apt-get update -y
# curl
apt-get -y install curl
echo "[LOG] curl version installed"
curl -V
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
# node and npm
apt-get -y install nodejs
echo "[LOG] node version installed"
node -v
echo "[LOG] npm version installed"
npm -v
# git
apt-get -y install git
echo "[LOG] git version installed"
git --version
# wget
apt-get -y install wget
echo "[LOG] wget version installed"
wget --version
# unzip
apt-get -y install unzip
echo "[LOG] unzip version installed"
unzip -v
echo "[LOG] Cloning repo"
git clone git@bitbucket.org:rndabzooba/analyticsdashboard.git
echo "[LOG] Done cloning"
cd analyticsdashboard
git checkout release
mkdir logs
npm install
echo "[LOG] Installation done"
