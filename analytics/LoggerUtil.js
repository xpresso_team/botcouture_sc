'use strict'; //Basically it enables the Javascript strict mode
var winston = require('winston');
require('winston-loggly-bulk');

let logger = winston.createLogger({
    transports: [
        new winston.transports.File({ filename: 'logs.txt' }),
    ]
});

logger.add(new winston.transports.Console({format: winston.format.simple()}))

module.exports = logger;
