#! /bin/bash
## This script is used to setup the build environment. It is used setup the build and test environment. It performs
## either of these tasks
##   1. Install Linux Libraries
##   2. Setup environment
##   3. Download data required to perform build.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user

set -e

# Intalling nodejs
if command -v node & > /dev/null; then
  echo "NodeJS found. Skipping node installation"
else
  echo "NodeJS not found. Installing node"
  curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
  apt-get -y install nodejs
  echo "[LOG] node version installed"
  node -v
  echo "[LOG] npm version installed"
  npm -v 
fi

curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt-get -y install nodejs
echo "[LOG] node version installed"
node -v
echo "[LOG] npm version installed"
npm -v

# Installing unittest dependencies related packages for node
npm install --save-dev mocha
npm install --save-dev nyc
