#! /bin/bash
## This script is used to test the docker container once it is up and running

echo "Using newly created docker image to test the  build"


DOCKER_IMAGE_NAME=${1}
TAG=${2}
current_folder=${ROOT_FOLDER}/xprbuild/docker
# SK : need to change for node js
cmd="docker run ${DOCKER_IMAGE_NAME}:${TAG} npm test"
echo "Docker Test  command -> $cmd"
#exec $cmd

