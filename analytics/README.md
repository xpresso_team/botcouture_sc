# README #

## REPOSITORY ###

* Analytics Dashboard for ChatBot
* Version : 1.4.0.rc.1

## SOFTWARE REQUIREMENT ###

* Requires node(>=6.0.0) and npm

## SET UP BOTCOUTURE ANALYTICS USING DOCKER IMAGE TAR###
### Note : Step 1 and 2 are one time task and to be performed only if the docker image is not present

* Go to the directory containing botcouture_analytics.tar( path on Abzooba Kolkata server : /home/abzooba/botcouture_analytics/ )
* ``` $ sudo docker load < botcouture_analytics.tar```
* ``` $ sudo docker run -d -p 8082:8082 --network datalake --name botcouture_analytics --restart always botcouture_analytics:1.0.0 ```

## STOPPING THE SERVICE

* ``` $ sudo docker stop  botcouture_analytics ```

## STARTING THE STOPPED SERVICE

* ``` $ sudo docker start botcouture_analytics ``` 

## MORE INFORMATION ###
Read the [Wiki](https://bitbucket.org/rndabzooba/analyticsdashboard/wiki/Home) for more details.
