/**
 * Created by Srijan on 11/07/17.
 */
module.exports = {
  connectionLimit: 20,
  host: 'memsql',
  port: '3306',
  dbuser: 'root',
  password: 'pass',
  database: 'comcast_uat_database',
  debug: false,
  adPort: '8081'
}
