var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var underscore = require('underscore');
var setupPassport = require('./app/controllers/setupPassport.js');
var flash = require('connect-flash');
var path = require('path');
var fileUpload = require('express-fileupload');

var app = express();
app.use(flash());
app.use(fileUpload());
setupPassport(app);
var metricsRoutes = require('./app/routes/index.js');
var pageRoutes = require('./app/routes/pageRoutes.js');
var configuration = require('./config/index.js')(process.argv[2]);

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(session({secret: "secret"}));
metricsRoutes(app);
app.use(express.static('public'));
pageRoutes(app);

var server = app.listen(configuration.get('adPort'), function () {
    var host = server.address().address ;
    var port = server.address().port ;
    console.log("App listening at http://%s:%s", host, port);
});
